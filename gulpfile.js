var gulp = require('gulp');
var elixir = require('laravel-elixir');
var rename = require("gulp-rename");

/**
 * Copy any needed files.
 *
 * Do a 'gulp copyfiles' after bower updates
 */
gulp.task("copyfiles", function() {

    // Copy jQuery
    gulp.src("vendor/bower_dl/jquery/dist/jquery.js")
       .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("vendor/bower_dl/bootstrap/less/**")
        .pipe(gulp.dest("resources/assets/less/bootstrap"));


    // Copy Bootstrap
    gulp.src("vendor/bower_dl/bootstrap/dist/js/bootstrap.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));

    gulp.src("vendor/bower_dl/bootstrap/dist/fonts/**")
        .pipe(gulp.dest("public/assets/fonts"));


    // Copy FontAwesome
    gulp.src("vendor/bower_dl/font-awesome/less/**")
        .pipe(gulp.dest("resources/assets/less/font-awesome"));

    gulp.src("vendor/bower_dl/font-awesome/fonts/**")
        .pipe(gulp.dest("public/assets/fonts"));


    // Copy datatables
    gulp.src("vendor/bower_dl/datatables/media/js/jquery.dataTables.js")
        .pipe(gulp.dest('resources/assets/js/vendor/'));

    gulp.src("vendor/bower_dl/datatables/media/css/dataTables.bootstrap.css")
        .pipe(rename('dataTables.bootstrap.less'))
        .pipe(gulp.dest('resources/assets/less/others/'));

    gulp.src("vendor/bower_dl/datatables/media/dataTables.bootstrap.js")
        .pipe(gulp.dest('resources/assets/js/vendor/'));

    gulp.src("resources/assets/js/dataTables.french.lang.json")
        .pipe(gulp.dest('public/assets/js/vendor/'));

    gulp.src("vendor/bower_dl/datatables.net-buttons/js/dataTables.buttons.min.js")
        .pipe(gulp.dest('resources/assets/js/vendor/'));

    gulp.src("vendor/bower_dl/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")
        .pipe(gulp.dest('resources/assets/js/vendor/'));


    // Copy Chosen
    gulp.src("vendor/bower_dl/chosen/chosen.jquery.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));

    gulp.src("vendor/bower_dl/chosen/chosen.jquery.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));

    gulp.src("vendor/bower_dl/bootstrap-chosen/bootstrap-chosen-variables.less")
        .pipe(gulp.dest("resources/assets/less/chosen"));

    gulp.src("vendor/bower_dl/bootstrap-chosen/bootstrap-chosen.less")
        .pipe(gulp.dest("resources/assets/less/chosen"));

    gulp.src("vendor/bower_dl/bootstrap-chosen/chosen-sprite.png")
        .pipe(gulp.dest("public/assets/css"));
    gulp.src("vendor/bower_dl/bootstrap-chosen/chosen-sprite@2x.png")
        .pipe(gulp.dest("public/assets/css"));


    // Copy Fileinput
    gulp.src("vendor/bower_dl/bootstrap-fileinput/css/fileinput.css")
        .pipe(gulp.dest("public/assets/css"));

    gulp.src("vendor/bower_dl/bootstrap-fileinput/js/fileinput.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));


    // Copy bootstrap-tour
    gulp.src("vendor/bower_dl/bootstrap-tour/build/css/bootstrap-tour.min.css")
        .pipe(gulp.dest("public/assets/css"));

    gulp.src("vendor/bower_dl/bootstrap-tour/build/js/bootstrap-tour.min.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));

    // Copy Xeditable
     gulp.src("vendor/bower_dl/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js")
         .pipe(gulp.dest("resources/assets/js/vendor/"));
     gulp.src("vendor/bower_dl/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css")
         .pipe(gulp.dest("public/assets/css"));

    // Copy form validation
    gulp.src("vendor/bower_dl/form.validation/dist/css/formValidation.css")
        .pipe(gulp.dest("public/assets/css"));
    gulp.src("vendor/bower_dl/form.validation/dist/js/formValidation.min.js")
        .pipe(gulp.dest("resources/assets/js/vendor/"));
    gulp.src("vendor/bower_dl/form.validation/dist/js/framework/bootstrap.min.js")
        .pipe(rename("formValidation.bootstrap.min.js"))
        .pipe(gulp.dest("resources/assets/js/vendor/"));
    gulp.src("vendor/bower_dl/form.validation/dist/js/language/fr_FR.js")
        .pipe(rename("formValidation.fr.js"))
        .pipe(gulp.dest("resources/assets/js/vendor/"));

    // Copy anime.js
    gulp.src("vendor/bower_dl/animejs/anime.min.js")
      .pipe(gulp.dest("resources/assets/js/vendor/"));

});


/**
 * Default gulp is to run this elixir stuff
 */
elixir(function(mix) {
  // Combine scripts
  mix.scripts([
      'vendor/jquery.js',
      'vendor/bootstrap.js',
      'vendor/jquery.dataTables.js',
      'vendor/dataTables.bootstrap.js',
      'vendor/jquery.form.js',
      'vendor/chosen.jquery.js',
      'vendor/fileinput.js',
      'vendor/fileinput_locale_fr.js',
      'rating.js',
      'switcher.js',
      'admin.js',
    ],
    'public/assets/js/admin.js',
    'resources/assets/js'
  );

  mix.scripts([
      'vendor/jquery.js',
      'vendor/jquery.form.js',
      'vendor/bootstrap.js',
      'vendor/jquery.dataTables.js',
      'vendor/dataTables.bootstrap.js',
      'vendor/dataTables.buttons.min.js',
      'vendor/buttons.bootstrap.min.js',
      'vendor/chosen.jquery.js',
      'vendor/bootstrap-tour.min.js',
      'vendor/fileinput.js',
      'vendor/fileinput_locale_fr.js',
      'vendor/jquery.autogrow.js',
      'vendor/formValidation.min.js',
      'vendor/formValidation.bootstrap.min.js',
      'vendor/formValidation.fr.js',
      'vendor/anime.min.js',
      'rating.js',
      'kiviat.js',
      'rekasator.js',
      'tables.js',
      'forms.js',
      'couplage.js',
      'default.js',
    ],
    'public/assets/js/default.js',
    'resources/assets/js'
  );

/*
  mix.scripts('switcher.js',  'public/assets/js/switcher.js');
  mix.scripts('rating.js',    'public/assets/js/rating.js');
  mix.scripts('fileinput.js', 'public/assets/js/fileinput.js');
  mix.scripts('lecons.js',    'public/assets/js/lecons.js');
//  mix.scripts(['bootstrap-tour.min.js', 'kiviat.js'], 'public/assets/js/kiviat.js', 'resources/assets/js');
*/
//  mix.scripts('rekasator.js', 'public/assets/js/rekasator.js');
//  mix.scripts('default.js',   'public/assets/js/default.js');



  // Compile Less
//  mix.less('admin.less',   'public/assets/css/admin.css');
  mix.less('default.less', 'public/assets/css/default.css');

//  mix.less('switcher.less', 'public/assets/css/switcher.css');
//  mix.less('rating.less',   'public/assets/css/rating.css');
});
