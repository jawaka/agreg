<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouplagesForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('couplages', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });

        Schema::table('couplages_developpements', function(Blueprint $table) {
            $table->foreign('couplage_id')->references('id')->on('couplages')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->foreign('developpement_id')->references('id')->on('developpements')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('couplages', function(Blueprint $table) {
            $table->dropForeign('couplages_user_id_foreign');
        });

        Schema::table('couplages_developpements', function(Blueprint $table) {
            $table->dropForeign('couplages_developpements_couplage_id_foreign');
            $table->dropForeign('couplages_developpements_developpement_id_foreign');
        });
    }
}
