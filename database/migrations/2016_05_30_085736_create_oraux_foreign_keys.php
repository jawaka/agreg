<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrauxForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oraux_questions', function(Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('oraux_questions_types')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->foreign('oraux_type_id')->references('id')->on('oraux_types')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });

        Schema::table('oraux_retours', function(Blueprint $table) {
            $table->foreign('oraux_type_id')->references('id')->on('oraux_types')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });

        Schema::table('oraux_retours_reponses', function(Blueprint $table) {
            $table->foreign('question_id')->references('id')->on('oraux_questions')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->foreign('retour_id')->references('id')->on('oraux_retours')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oraux_questions', function(Blueprint $table) {
            $table->dropForeign('oraux_questions_type_id_foreign');
            $table->dropForeign('oraux_questions_oraux_type_id_foreign');
        });

        Schema::table('oraux_retours', function(Blueprint $table) {
            $table->dropForeign('oraux_retours_oraux_type_id_foreign');
            $table->dropForeign('oraux_retours_user_id_foreign');
        });

        Schema::table('oraux_retours_reponses', function(Blueprint $table) {
            $table->dropForeign('oraux_retours_reponses_question_id_foreign');
            $table->dropForeign('oraux_retours_reponses_retour_id_foreign');
        });
    }
}
