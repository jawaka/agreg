<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrauxQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oraux_questions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('oraux_type_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('label');
            $table->integer('ordre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oraux_questions');
    }
}
