<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferencesTable extends Migration {

	public function up()
	{
		Schema::create('references', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('titre');
			$table->string('auteurs');
			$table->string('cote');
			$table->string('ISBN');
		});
	}

	public function down()
	{
		Schema::drop('references');
	}
}