<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeconsTypesTable extends Migration {

	public function up()
	{
		Schema::create('lecons_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nom');
			$table->string('fnom');
		});
	}

	public function down()
	{
		Schema::drop('lecons_types');
	}
}