<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('lecons', function(Blueprint $table) {
			$table->foreign('type_id')->references('id')->on('lecons_types')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('versions', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('versions_fichiers', function(Blueprint $table) {
			$table->foreign('version_id')->references('id')->on('versions')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('versions_references', function(Blueprint $table) {
			$table->foreign('reference_id')->references('id')->on('references')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('versions_references', function(Blueprint $table) {
			$table->foreign('version_id')->references('id')->on('versions')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('recasages', function(Blueprint $table) {
			$table->foreign('developpement_id')->references('id')->on('developpements')
						->onDelete('restrict')
						->onUpdate('restrict');
			$table->foreign('lecon_id')->references('id')->on('lecons')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('recasages_votes', function(Blueprint $table) {
			$table->foreign('developpement_id')->references('id')->on('developpements')
						->onDelete('restrict')
						->onUpdate('restrict');
			$table->foreign('lecon_id')->references('id')->on('lecons')
						->onDelete('restrict')
						->onUpdate('restrict');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('role_users', function(Blueprint $table) {
			$table->foreign('role_id')->references('id')->on('roles')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('lecons_liens', function(Blueprint $table) {
			$table->foreign('parent_id')->references('id')->on('lecons')
						->onDelete('restrict')
						->onUpdate('restrict');

			$table->foreign('enfant_id')->references('id')->on('lecons')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('lecons', function(Blueprint $table) {
			$table->dropForeign('lecons_type_id_foreign');
		});
		Schema::table('versions', function(Blueprint $table) {
			$table->dropForeign('versions_user_id_foreign');
		});
		Schema::table('versions_fichiers', function(Blueprint $table) {
			$table->dropForeign('versions_fichiers_version_id_foreign');
		});
		Schema::table('versions_references', function(Blueprint $table) {
			$table->dropForeign('versions_references_reference_id_foreign');
		});
		Schema::table('versions_references', function(Blueprint $table) {
			$table->dropForeign('versions_references_version_id_foreign');
		});
		Schema::table('recasages', function(Blueprint $table) {
			$table->dropForeign('recasages_lecon_id_foreign');
			$table->dropForeign('recasages_developpement_id_foreign');
		});
		Schema::table('recasages_votes', function(Blueprint $table) {
			$table->dropForeign('recasages_votes_lecon_id_foreign');
			$table->dropForeign('recasages_votes_developpement_id_foreign');
			$table->dropForeign('recasages_votes_user_id_foreign');
		});
		Schema::table('role_users', function(Blueprint $table) {
			$table->dropForeign('role_users_user_id_foreign');
			$table->dropForeign('role_users_role_id_foreign');
		});
		Schema::table('lecons_liens', function(Blueprint $table) {
			$table->dropForeign('lecons_liens_parent_id_foreign');
			$table->dropForeign('lecons_liens_enfant_id_foreign');
		});
	}
}