<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVersionsTable extends Migration {

	public function up()
	{
		Schema::create('versions', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('remarque');
			$table->integer('versionable_id')->unsigned();
			$table->string('versionable_type');
			$table->integer('user_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('versions');
	}
}