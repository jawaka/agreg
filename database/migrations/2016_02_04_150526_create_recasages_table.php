<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecasagesTable extends Migration {

	public function up()
	{
		Schema::create('recasages', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('lecon_id')->unsigned();
			$table->integer('developpement_id')->unsigned();
			$table->integer('qualite');
		});
	}

	public function down()
	{
		Schema::drop('recasages');
	}
}