<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRowsInCouplagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('couplages', function (Blueprint $table) {
            $table->dropColumn(['nom']);
            $table->dropColumn(['votes']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('couplages', function (Blueprint $table) {
            $table->string('nom');
            $table->boolean('votes');
        });
    }
}
