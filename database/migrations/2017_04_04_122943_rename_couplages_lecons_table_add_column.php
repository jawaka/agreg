<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCouplagesLeconsTableAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('couplages_lecons_couvertes', 'couplages_lecons');
        Schema::table('couplages_lecons', function (Blueprint $table) {
            $table->integer('status')->default(3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('couplages_lecons', 'couplages_lecons_couvertes');
    }
}
