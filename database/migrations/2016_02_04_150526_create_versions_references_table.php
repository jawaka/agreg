<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVersionsReferencesTable extends Migration {

	public function up()
	{
		Schema::create('versions_references', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('reference_id')->unsigned();
			$table->integer('version_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('versions_references');
	}
}