<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVersionsFichiersTable extends Migration {

	public function up()
	{
		Schema::create('versions_fichiers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('version_id')->unsigned();
			$table->string('url');
		});
	}

	public function down()
	{
		Schema::drop('versions_fichiers');
	}
}