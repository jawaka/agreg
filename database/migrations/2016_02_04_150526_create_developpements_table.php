<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeveloppementsTable extends Migration {

	public function up()
	{
		Schema::create('developpements', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('nom');
			$table->text('details');
		});
	}

	public function down()
	{
		Schema::drop('developpements');
	}
}