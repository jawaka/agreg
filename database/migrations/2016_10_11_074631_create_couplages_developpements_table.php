<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouplagesDeveloppementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couplages_developpements', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('couplage_id')->unsigned();
            $table->integer('developpement_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('couplages_developpements');
    }
}
