<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecasagesVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recasages_votes', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('lecon_id')->unsigned();
            $table->integer('developpement_id')->unsigned();
            $table->integer('qualite');
            $table->integer('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recasages_votes');
    }
}
