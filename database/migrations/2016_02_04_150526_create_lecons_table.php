<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeconsTable extends Migration {

	public function up()
	{
		Schema::create('lecons', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('annee');
			$table->integer('numero');
			$table->integer('type_id')->unsigned();
			$table->string('nom');
			$table->text('rapport');
			$table->integer('options');
		});
	}

	public function down()
	{
		Schema::drop('lecons');
	}
}