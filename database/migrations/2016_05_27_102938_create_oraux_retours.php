<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrauxRetours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oraux_retours', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('oraux_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('annee');
            $table->boolean('anonyme');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oraux_retours');
    }
}
