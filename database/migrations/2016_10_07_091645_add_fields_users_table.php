<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->boolean('montrer_email')->after('permissions')->default(false);
            $table->integer('annee');
            $table->integer('option');
            $table->integer('resultat');
            $table->integer('classement');
            $table->text('biographie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn(array('montrer_email', 'annee', 'option', 'resultat', 'classement', 'biographie'));
        });
    }
}
