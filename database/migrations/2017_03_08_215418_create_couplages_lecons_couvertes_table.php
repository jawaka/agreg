<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouplagesLeconsCouvertesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couplages_lecons_couvertes', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('couplage_id')->unsigned();
            $table->integer('lecon_id')->unsigned();
        });

        Schema::table('couplages_lecons_couvertes', function (Blueprint $table) {
            $table->foreign('couplage_id')->references('id')->on('couplages')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->foreign('lecon_id')->references('id')->on('lecons')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('couplages_lecons_couvertes');
    }
}
