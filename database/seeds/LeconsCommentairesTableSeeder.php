<?php

use Illuminate\Database\Seeder;

class LeconsCommentairesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lecons_commentaires')->delete();
        
        \DB::table('lecons_commentaires')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-03-25 20:55:56',
                'updated_at' => '2017-03-25 20:55:56',
                'user_id' => 1,
                'lecon_id' => 785,
                'commentaire' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2017-03-25 20:56:32',
                'updated_at' => '2017-03-25 20:56:32',
                'user_id' => 1,
                'lecon_id' => 784,
                'commentaire' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2017-03-25 20:56:34',
                'updated_at' => '2017-03-25 21:15:57',
                'user_id' => 108,
                'lecon_id' => 663,
                'commentaire' => 'klkl
llm
',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2017-03-25 20:59:00',
                'updated_at' => '2017-03-25 20:59:00',
                'user_id' => 1,
                'lecon_id' => 778,
                'commentaire' => 'aze
azert',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2017-03-25 21:25:13',
                'updated_at' => '2017-03-25 21:25:13',
                'user_id' => 108,
                'lecon_id' => 683,
                'commentaire' => ',;,,
lm

$lm$',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2017-03-25 21:25:22',
                'updated_at' => '2017-03-25 21:25:22',
                'user_id' => 108,
                'lecon_id' => 684,
                'commentaire' => 'lk
l
',
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2017-03-27 14:35:57',
                'updated_at' => '2017-03-27 14:35:57',
                'user_id' => 134,
                'lecon_id' => 780,
                'commentaire' => '',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2017-03-27 14:54:03',
                'updated_at' => '2017-03-27 14:54:03',
                'user_id' => 134,
                'lecon_id' => 821,
                'commentaire' => '',
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2017-03-28 07:23:30',
                'updated_at' => '2017-03-28 07:26:49',
                'user_id' => 108,
                'lecon_id' => 666,
                'commentaire' => 'jk
ds
$dsds$
kljdKLjfkldsklfkdls
fdslfmds
fsdfdsfds
',
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2017-03-28 14:28:28',
                'updated_at' => '2017-03-28 14:28:28',
                'user_id' => 134,
                'lecon_id' => 811,
                'commentaire' => '',
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2017-03-28 14:32:22',
                'updated_at' => '2017-03-28 14:32:22',
                'user_id' => 134,
                'lecon_id' => 818,
                'commentaire' => '+ Hahn Banach analytique',
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2017-03-28 14:33:10',
                'updated_at' => '2017-03-28 14:33:10',
                'user_id' => 134,
                'lecon_id' => 823,
                'commentaire' => '',
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2017-03-28 14:34:37',
                'updated_at' => '2017-03-28 14:34:37',
                'user_id' => 134,
                'lecon_id' => 837,
                'commentaire' => '',
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2017-03-28 14:35:01',
                'updated_at' => '2017-03-28 14:35:04',
                'user_id' => 134,
                'lecon_id' => 838,
                'commentaire' => 'Théorème de Féjer',
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2017-03-28 14:35:42',
                'updated_at' => '2017-03-28 14:35:42',
                'user_id' => 134,
                'lecon_id' => 842,
                'commentaire' => 'Théorème de Féjer',
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2017-03-29 11:52:02',
                'updated_at' => '2017-03-29 11:52:02',
                'user_id' => 108,
                'lecon_id' => 664,
                'commentaire' => 'kjkm

klm
',
            ),
        ));
        
        
    }
}
