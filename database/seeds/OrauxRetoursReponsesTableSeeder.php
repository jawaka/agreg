<?php

use Illuminate\Database\Seeder;

class OrauxRetoursReponsesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oraux_retours_reponses')->delete();
        
        \DB::table('oraux_retours_reponses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'retour_id' => 1,
                'question_id' => 19,
                'reponse_texte' => '',
                'reponsable_id' => 557,
                'reponsable_type' => 'lecon',
            ),
            1 => 
            array (
                'id' => 2,
                'retour_id' => 1,
                'question_id' => 20,
                'reponse_texte' => '',
                'reponsable_id' => 616,
                'reponsable_type' => 'lecon',
            ),
            2 => 
            array (
                'id' => 3,
                'retour_id' => 1,
                'question_id' => 21,
                'reponse_texte' => '',
                'reponsable_id' => 34,
                'reponsable_type' => 'developpement',
            ),
            3 => 
            array (
                'id' => 636,
                'retour_id' => 65,
                'question_id' => 75,
                'reponse_texte' => '',
                'reponsable_id' => 150,
                'reponsable_type' => 'reference',
            ),
            4 => 
            array (
                'id' => 6,
                'retour_id' => 1,
                'question_id' => 24,
                'reponse_texte' => 'Les questions étaient de niveau moyen, mais le jury n\'était pas très attentif et blaguait beaucoup entre eux... Mais jury plutôt sympathique et enclin à aider.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            5 => 
            array (
                'id' => 7,
                'retour_id' => 1,
                'question_id' => 23,
                'reponse_texte' => 'A la suite du développement sur Kakutani : que peut on dire des sous groupes finis de GL2 ? Indication : on pourra utiliser le développement que vous venez de prouver... On se ramène aux sous groupes finis de O2. Puis comme sous-question : que peut on dire des sous groupes de SO2 ? On montre finalement qu\'ils sont cycliques et puis rapidement pour le cas de O2 on dit que ça fait le diédral.

Etant donné deux matrices J=(1 1, 0 1), K = (1 1, -1 0), montrer qu\'elles engendrent SL2(Z). Sous-question : est ce que les transvections engendrent SL2(Z) ? On adapte le pivot de Gauss pour montrer que oui, et en calculant J^n et JK on obtient toutes les transvections.

Des questions de topologie : pourquoi est ce que l\'application inverse est un homéo ? Réponse avec la formule de la comatrice. Est ce un difféomorphisme ? Je pars dans les calculs de la différentielle en A pendant que le jury essaye de me faire remarquer que comme tout est polynomial, ça marche tout seul, et que pour montrer que la différentielle est bijective, il suffit de remarquer que l\'application inverse est une involution.

Montrer que les matrices diagonalisables sont denses dans M_n(C). Pas trop eu le temps de finir la question, j\'ai expliqué avec les mains qu\'il faut perturber la matrice pour que le poly caract soit scindé à racines simples.

Une dernière question "pédagogique" : si vous enseignez cette partie à une classe, quels seraient les points délicats sur lesquels il faudrait insister ? Réponse : le lien avec la géométrie pour les petites dimensions, avec acquiescement du jury (notamment parce que j\'ai pas mal galéré pour les sous groupes de SO2...)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            6 => 
            array (
                'id' => 8,
                'retour_id' => 1,
                'question_id' => 26,
                'reponse_texte' => 'Pas de questions sur le plan, et le jury qui semblait pas trop concentré pendant les questions.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            7 => 
            array (
                'id' => 9,
                'retour_id' => 1,
                'question_id' => 27,
                'reponse_texte' => '16',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            8 => 
            array (
                'id' => 18,
                'retour_id' => 1,
                'question_id' => 27,
                'reponse_texte' => '16',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            9 => 
            array (
                'id' => 30,
                'retour_id' => 2,
                'question_id' => 37,
            'reponse_texte' => 'A14 (?) : de la théorie des jeux à 2 agents avec de la stat',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            10 => 
            array (
                'id' => 31,
                'retour_id' => 2,
                'question_id' => 38,
            'reponse_texte' => 'A64 (?) : un truc sur la loi exponentielle avec des martingales',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            11 => 
            array (
                'id' => 32,
                'retour_id' => 2,
                'question_id' => 39,
            'reponse_texte' => 'Mon texte était rigolo. On joue n tours d\'un jeu, on a un Adversaire qui joue des coups iid de loi inconnue à chaque tour, et on doit construire une stratégie qui maximise une fonction de gain (à chaque duo de coups joués (i,j) on associe r(i,j)∈R). L\'idée est d\'estimer la loi PA de l\'adversaire puis de jouer à chaque tour "le" coup qui rapporte le plus d\'après ce qu\'on sait de PA. Donc en fait à chaque tour ce qu\'on joue est totalement décidé par les observations précédentes, il n\'y a pas d\'aléatoire (sauf s\'il y a plusieurs solutions optimales mais on s\'en fout).',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            12 => 
            array (
                'id' => 33,
                'retour_id' => 2,
                'question_id' => 40,
            'reponse_texte' => 'Mes simulations illustraient un exemple de pierre-feuille-ciseau généralisé où l\'on gagnait un peu en cas d\'égalité, beaucoup si on jouait le coup (parmis m différents) qui battait celui de l\'adversaire (le m-ième coup bat le m+1-ième) mais zéro si on gagnait quoi que ce soit d\'autre ; j\'ai modélisé la convergence de l\'estimateur (m = 3 lol, trois marginales qui tendent vers des constantes égales aux PA(i) pour i∈{P,F,C}) et une stratégie non-optimale (pur accident : j\'avais pas compris la strat opti, du coup j\'ai modélisé un autre truc - une strat mixte un peu rigolotte). J\'ai expliqué ça au jury, j\'ai pas l\'impression que ça les ait trop choqués même si (dommage) j\'avais pas eu le temps de faire la vraie strat pour tenter uen comparaison. Il y avait pas mal de choses à dire sur mon modèle non opti en fait, c\'était plutôt sympa. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            13 => 
            array (
                'id' => 34,
                'retour_id' => 2,
                'question_id' => 42,
                'reponse_texte' => 'On m\'a demandé de préciser plusieurs trucs de ma présentation, puis de prouver un point que j\'avais laissé sans démo. On est passé à des questions sans rapport ensuite.

- Questions sur la LFGN. En fait, j\'avais utilisé une LFGN pour justifier une convergence à un moment du texte mais je m\'étais embrouillé, j\'avais fait n\'imp (le théorème s\'appliquait directement... mais j\'avais des notations hyper foireuses et il m\'a fallu du temps pour écrire des choses correctes). Du coup j\'ai eu des questions dessus, donner l\'énoncé complet et tout. Même chose pour le TCL ensuite, et pourquoi L2 est inclus dans L1. Après, comment utiliser le TCL pour construire un intervalle de confiance asymptotique (on a parlé de quantiles) et une dernière question 

- Dernière question pédagogique, comment introduire les chaines de Markov à des gens qui n\'y connaissent rien ? J\'ai dit que j\'expliquerait ça sur des exemples simples avec des schémas avec peu d\'états, j\'ai fait un petit dessin avec 3 états dont un qui renvoyait vers les autres et qu\'on ne visitait plus. Ca avait l\'air OK comme réponse, j\'ai vraiment pas eu le temps d\'en dire plus (ça a pris 30s à tout casser). ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            14 => 
            array (
                'id' => 35,
                'retour_id' => 2,
                'question_id' => 45,
            'reponse_texte' => 'J\'étais content de ma simulation et de ce que j\'avais à présenter, mais au tableau c\'était un peu le bordel et j\'ai pas toujours été très clair (quitte, cependant, à reprendre des trucs directement pour réexpliquer une fois que je me comprenais moi-même). Ca m\'a probablement pas mal coûté, ça et quelques bêtises, et globalement je m\'attends à une note franchement moyenne.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            15 => 
            array (
                'id' => 36,
                'retour_id' => 2,
                'question_id' => 41,
                'reponse_texte' => 'Essentiellement des questions faciles, qui survenaient parce que j\'avais raté un truc dans ma présentation. Jury qui aide assez.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            16 => 
            array (
                'id' => 37,
                'retour_id' => 2,
                'question_id' => 43,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            17 => 
            array (
                'id' => 38,
                'retour_id' => 2,
                'question_id' => 44,
                'reponse_texte' => '12',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            18 => 
            array (
                'id' => 39,
                'retour_id' => 3,
                'question_id' => 37,
            'reponse_texte' => 'De la merde (un modèle de croissance linéaire et division)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            19 => 
            array (
                'id' => 40,
                'retour_id' => 3,
                'question_id' => 38,
                'reponse_texte' => '    Pire',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            20 => 
            array (
                'id' => 41,
                'retour_id' => 3,
                'question_id' => 39,
                'reponse_texte' => 'Résumé du texte :
On étudie une fonction affine de pente 1, qui, à des temps aléatoires, est divisée par une quantité aléatoire (genre Xt=X0+t jusqu\'à t=T1, puis XT1=XT1−×U1 avec T1≥0 et 0≤U1≤1 aléatoire, et on continue à croître avec pente 1). Le but du texte est d\'estimer certaines caractéristiques des variables Ti et Ui.
/!\\ Il n\'y a aucune description des applications du modèle. Il est mentionné quelque chose comme « On peut penser à des applications en biologie, en gestion de stocks ou en informatique ». Je répète, ce modèle n\'est absolument pas motivé ! /!\\',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            21 => 
            array (
                'id' => 42,
                'retour_id' => 3,
                'question_id' => 40,
            'reponse_texte' => 'J\'ai un peu étudié le modèle, avec un peu plus de généralité. Le cas traité par le texte était T ou U aléatoire, et j\'ai traité T et U aléatoire. Les calculs, du coup, sont un peu plus tricky, et je crois que je les ai perdus à un moment (genre tous en même temps je veux dire, parce que sinon ils décrochent tous forcément quelques minutes pendant l\'exposé).
Du coup, j\'ai fait la connerie de parler de la loi conditionnelle L(X∣Y) de X sachant Y.* J\'étais pas super chaud sur le truc, ça s\'est vu, et j\'ai paniqué, et je me suis enfoncé. Du coup, on a passé un certain temps là-dessus, à traiter des exemples et tout.

* Pour ceux que ça intéresse, c\'est la mesure A↦E[1X∈A∣Y]. C\'est une mesure aléatoire. On est tous MDR. Je crois que si je leur avait dit ça dès le début ils auraient été contents.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            22 => 
            array (
                'id' => 43,
                'retour_id' => 3,
                'question_id' => 42,
                'reponse_texte' => 'Une question longue sur une connerie que j\'ai sortie, puis quelques questions de stats.

Après, les questions ont été orientées stats (puisque le texte est orienté probas), comment appliquez-vous la loi forte des grands nombres quand vous l\'avez écrite ici, donnez-moi un intervalle de confiance asymptotique pour tel truc (ok via TCL), est-ce qu\'il y a un intervalle de confiance exact (oui via Markov), comment calculer le α-quantile de la loi normale (là j\'ai buggé, mais ils m\'ont dit que j\'avais le droit de simuler un échantillon de 1000 gaussiennes, donc bon…).

On a fini sur la question lol : comment expliqueriez-vous le concept de chaîne de Markov à des élèves ? J\'ai parlé de marche aléatoire, j\'ai dit qu\'on pouvait la simuler « pour de vrai » avec une pièce, j\'ai dit que c\'était con parce que la marche n\'était ni apériodique ni récurrente forte, du coup elle n\'illustre aucun des théorèmes de convergence sympa. Ils ont eu l\'air heureux que j\'aie compris que c\'était un exemple de merde, ensuite j\'ai dit « On fait des ronds et des flèches » et ils étaient satisfaits en mode « C\'est nul mais de toute façon c\'était une question de merde ». Voilà.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            23 => 
            array (
                'id' => 44,
                'retour_id' => 3,
                'question_id' => 45,
            'reponse_texte' => 'Je sais qu\'il ne faut pas parler de choses qu\'on ne maîtrise pas. Mais en fait, j\'étais convaincu de savoir de quoi je parlais (à propos de la loi conditionnelle), du coup j\'aurais été pris en défaut de toute façon. Conclusion : quand on voit qu\'on coince, NE PAS S\'ENFONCER. Prendre son temps. PRENDRE SON TEMPS FICHTRE. Je pense que j\'aurais beaucoup plus facilement désamorcé le truc si j\'avais pris deux minutes, je serais passé pour un con mais un con temporaire. Sinon, j\'ai eu un tableau avec devant un écran, et de chaque côté de l\'écran, environ 20 cm pour écrire les paramètres de la modélisation. C\'est peu, ce qui m\'a obligé à descendre et remonter et descendre et remonter l\'écran en permanence, et c\'était relou.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            24 => 
            array (
                'id' => 45,
                'retour_id' => 3,
                'question_id' => 41,
            'reponse_texte' => 'Un peu cassant… parce que j\'ai sorti une connerie. Le mec qui posait les questions de stats (barbu, des lunettes, un peu vieux mais c\'est peut-être juste que ses cheveux étaient blancs) était sympa.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            25 => 
            array (
                'id' => 46,
                'retour_id' => 3,
                'question_id' => 43,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            26 => 
            array (
                'id' => 47,
                'retour_id' => 3,
                'question_id' => 44,
                'reponse_texte' => '13.5',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            27 => 
            array (
                'id' => 48,
                'retour_id' => 4,
                'question_id' => 37,
                'reponse_texte' => 'A03 - Estimation.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            28 => 
            array (
                'id' => 49,
                'retour_id' => 4,
                'question_id' => 38,
                'reponse_texte' => 'A05 - Estimation',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            29 => 
            array (
                'id' => 50,
                'retour_id' => 4,
                'question_id' => 39,
            'reponse_texte' => 'On disposait d\'un échantillon d\'individu ayant contracté l\'hépatite C. Une première variable $Z_i$ disait si l\'individu $i$ avait survécu ou non, puis on disposait d\'un certain nombre de variables $X_i^{(p)}$ explicatives. Le modèle linéaire gaussien n\'était pas le plus adapté car les $Z_i$ sont des Bernoulli, du coup on cherchait des coefficients $\\theta$ tels que $Z_i= 1_{\\theta_0 + \\theta_1 X_i^{(1)} + \\cdots + \\theta_p X_i^{(p)} + \\epsilon_i \\ge 0}. Les $\\epsilon_i$ sont supposés gaussiens centrés réduits. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            30 => 
            array (
                'id' => 51,
                'retour_id' => 4,
                'question_id' => 40,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            31 => 
            array (
                'id' => 52,
                'retour_id' => 4,
                'question_id' => 42,
            'reponse_texte' => 'Il y avait peu, voire pas de questions sur mon exposé. Mais des questions poursuivant l\'idée de mon exposé. J\'avais dit que mon deuxième théorème, qui établissait une convergence en loi vers une loi normale, permettait de créer un intervalle de confiance (plus exactement une région de confiance car on est en dimension grande). 

La première partie des questions concernaient l\'établissement de la région de confiance en dimension grande. J\'ai d\'abord donné l\'idée générale, puis on est rentré dans les détails. En fait, on avait en gros $\\sqrt(n) (\\theta(n)-\\theta^*)$, où $\\theta(n)$ était un estimateur de $\\theta^*$, qui convergeait en loi vers une loi normale centrée de matrice de covariance $J(n)$, qui dépendait de l\'estimateur. Du coup, après avoir montré comment on fonctionnait lorsque la matrice de covariance était l\'identité, puis une matrice déterministe, il fallait utiliser un lemme de Slutsky pour s\'en tirer. Mais il y avait ensuite d\'autres subtilités à prendre en compte, qui faisait qu\'il était préférable d\'estimer aussi $J(n)$. 
Ensuite, deuxième phase de question sur un moyen pour déterminer l\'estimateur du maximum de vraisemblance : il s\'agissait ainsi de discuter de la méthode de Newton-Raphson brièvement évoquée dans le texte. J\'ai expliqué comment cette méthode était une généralisation de Newton en dimension 1, et je crois que ça a plutôt satisfait le jury. 
Enfin, une dernière question last-minute sur une alternive possible à supposer les $\\varepsilon_i$ gaussiens. J\'ai dit qu\'on les choisissait gaussien pour faciliter les calculs, car on connaît bien les tables de la gaussienne et du $\\chi^2$. Il m\'a demandé si je ne voyais pas une autre loi possible. Il a fini par évoquer Logit, j\'ai dû avouer la vacuité de l\'intersection de cette loi avec mes connaissances. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            32 => 
            array (
                'id' => 53,
                'retour_id' => 4,
                'question_id' => 45,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            33 => 
            array (
                'id' => 54,
                'retour_id' => 4,
                'question_id' => 41,
                'reponse_texte' => 'Jury plutôt neutre et sympathique. Même s\'il n\'affichait pas ouvertement leur bonheur d\'assister à ces oraux, il n\'affichait pas le contraire, et ce malgré la présence de six auditeurs dans une petite salle où le rétroprojecteur est allumé à 15h par une température quasi-caniculaire. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            34 => 
            array (
                'id' => 55,
                'retour_id' => 4,
                'question_id' => 43,
            'reponse_texte' => 'Très négativement surpris par la présence d\'un demi-tableau. Certes, il était à craie, mais il n\'y avait de la place que pour 2 colonnes. J\'aurai bien aimé laisser mon plan, mais c\'était impossible dans ces conditions, et j\'avais à peine la place de terminer ma démonstration sans effacer le théorème. En plus, lorsque j\'allumai le rétro, il fallait descendre l\'écran blanc qui recouvrait 75% du tableau. Surpris aussi, et surtout, par la difficulté du texte. La première démonstration était vraiment mal rédigée (ça m\'aurait sans doute pris plus d\'une heure pour compléter les trous dans la démo). Les démonstrations suivantes, bien que lacunaires, étaient plus potables. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            35 => 
            array (
                'id' => 56,
                'retour_id' => 4,
                'question_id' => 44,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            36 => 
            array (
                'id' => 57,
                'retour_id' => 5,
                'question_id' => 37,
            'reponse_texte' => 'Variables gaussiennes, loi stationnaire, convergence (approximativement)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            37 => 
            array (
                'id' => 58,
                'retour_id' => 5,
                'question_id' => 38,
                'reponse_texte' => 'Quelque chose, pôlynomes, estimation.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            38 => 
            array (
                'id' => 59,
                'retour_id' => 5,
                'question_id' => 39,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            39 => 
            array (
                'id' => 60,
                'retour_id' => 5,
                'question_id' => 40,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            40 => 
            array (
                'id' => 61,
                'retour_id' => 5,
                'question_id' => 42,
                'reponse_texte' => 'Il n\'y a pas eu de questions sur le texte qui ne portaient pas sur mon exposé, mais le jury a utilisé des notations du texte que je n\'avais pas introduites.

Preuve de l\'existence d\'une mesure invariante sur la sphère (autre que celle qui était dans le texte).

Preuve de son unicité en dimension 1 (ils m\'ont vite suggéré d\'utiliser les séries de Fourier).

Comment prouver l\'existence d\'une mesure invariante par une application continue sur un espace métrique compact ?

Comment, dans un cadre plus général, vérifier que la loi limite qu\'on suppose être correcte l\'est efectivement ? (c\'est-à-dire parler de tests d\'adéquation à une loi) Quel est le principe du test de Kolmogorov-Smirnov ?

Pour le processus de Poisson sur R, quelle est la loi du premier saut après 1 ? Et du premier saut avant 1 ? Montrer que N_t/t converge presque-sûrement et donner sa limite.

Comment expliquer la différence entre la convergence p.s. et la convergence en loi à des élèves qui n\'en connaîtraient que les définitions ?',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            41 => 
            array (
                'id' => 62,
                'retour_id' => 5,
                'question_id' => 45,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            42 => 
            array (
                'id' => 63,
                'retour_id' => 5,
                'question_id' => 41,
                'reponse_texte' => 'Le niveau est difficile à évaluer, car ils ne me laissaient jamais réfléchir longtemps, ils me donnaient vite des indications. J\'ai trouvé le jury sympa ; ils m\'ont souri pendant mon exposé, ce qui n\'arrive pas toujours pendant l\'année. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            43 => 
            array (
                'id' => 64,
                'retour_id' => 5,
                'question_id' => 43,
                'reponse_texte' => 'L\'oral s\'est passé comme je l\'imaginais, à part que j\'ai été surprise par la possibilité de se déplacer librement pendant la préparation.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            44 => 
            array (
                'id' => 65,
                'retour_id' => 5,
                'question_id' => 44,
                'reponse_texte' => '18.5',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            45 => 
            array (
                'id' => 66,
                'retour_id' => 6,
                'question_id' => 46,
                'reponse_texte' => 'B90',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            46 => 
            array (
                'id' => 67,
                'retour_id' => 6,
                'question_id' => 47,
                'reponse_texte' => 'B87',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            47 => 
            array (
                'id' => 68,
                'retour_id' => 6,
                'question_id' => 48,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            48 => 
            array (
                'id' => 69,
                'retour_id' => 6,
                'question_id' => 49,
                'reponse_texte' => 'Je résume d\'abord mon plan puis les questions posées, j\'espère être assez clair, mais c\'est difficile.

----------------------------------------------------------------------------

I. Position du problème et modélisation

A. Croissance exponentielle et suite logistique

On s\'intéressait à l\'évolution d\'une population au cours du temps. On introduisait d\'abord le modèle à croissance de population constante

$$\\frac{du}{dt}=\\alpha u(t) $$

où u est le nombre d\'individus et \\alpha le taux de croissance.
Ceci mène à une solution exponentielle et donc non crédible.

On peut alors prendre en compte plusieurs phénomènes :

- la dépendance en âge, c\'est-à-dire que des nouveaux nés n\'auront pas le même taux de reproduction que de jeunes adultes par exemple
- la dépendance en temps, c\'est-à-dire que le taux de croissance peut dépendre du temps la reproduction est plus élevé au printemps.
- les limitations du milieu, c\'est-à-dire que si il y a trop de monde la population décroît car le milieu ne fournit pas assez de matériau pour subvenir au besoin de tous.

On retient la dernière considération ce qui mène à l\'équation logistique :
$$\\frac{du}{dt}=\\alpha u*(1-\\frac{u}{\\kappa} $$
où \\alpha garde la même interprétation et \\kappa est une population limite ou plutôt idéal.

On pose $ g : u\\mapsto \\alpha u*(1-\\frac{u}{\\kappa} )$

J\'ai ensuite écrit le théorème de Cauchy-Lipchitz proprement au tableau, et dis qu\'on pouvait l\'appliquer ici. Et on remarquer trois types de comportement :

- solutions constantes
- solutions croissantes comprises entre 0 et $\\kappa$
- solutions décroissantes plus grandes que $\\kappa$

Ainsi, notre interprétation de la constante $\\kappa$ est en adéquation avec l\'étude qualitative de l\'équation différentielle.
On pourrait prendre d\'autres types de profil pour g, par exemple  $ g : u\\mapsto \\alpha u*(1-\\frac{u}{\\kappa}^2$:
Ici nous ne prenons pas en compte, les déplacements de population, c\'est ce que nous allons faire.

B. Déplacement de population 
On fait l\'hypothèse que la population se répartie le long d\'une axe.
Je n\'ai pas établie l\'équation faute de temps, mais j\'ai dit que j\'étais prêt à en parler à la fin de l\'oral et qu\'il faudrait faire un bilan de flux sur une tranche [x,x+dx] comme dans le cas de l\'équation de la chaleur et utiliser une loi du type Fourier.
On est amené à l\'équation :

$$ \\frac{du}{dt}=\\nu \\frac{d^2 u}{dx^2} + g(u(x,t))$$

Même si je n\'ai pas établie l\'équation je donne l\'interprétation des termes, c\'est-à-dire que $\\nu \\frac{d^2 u}{dx^2}$ correspond à la diffusion et  $g(u(x,t))$ à la création de population localement, ce qui apparaît nettement quand on fait le bilan.

J\'ai précisé qu\'il fallait se fixer des conditions aux bords comme dans l\'autre équation parabolique que je connaissais : l\'équation de la chaleur. Ici on fixait la dérivée égale à 0 au bords ( on se plaçait sur un segment [-L,L] )


II. Différences finies

A. Présentation et consistance du schéma

J\'ai expliqué que l\'idée était de changer une équation différentielle en un problème discret et donc en un système linéaire à résoudre.
Là, j\'ai fait mes petits développements de Taylor pour montrer que le schéma choisi par le texte était d\'ordre 2 en espace et 1 en temps ( on traitait en implicite le terme diffusif et en explicite le terme de création de population.
(J\'ai essayé d\'être le plus clair possible vu le dernière oral que j\'ai présenté)
J\'ai précisé que les conditions aux bords était d\'ordre 2 en espace sans le démontrer.
Donc le schéma au total était bien d\'ordre 2 en espace et 1 en temps, donc consistant ce qui invitait à définir le schéma qui était dans le texte ( là j\'ai renvoyé au texte où tout était posé sans réécrire)
Il faut ensuite montrer que le schéma est bien définie, j\'ai donc montrer que la matrice M qui définissait le schéma était symétrique définie positive. Là, il fallait montrer qu\'une quantité du type $tV M V$ était strictement positive, si V était un vecteur non-nul.
On a donc montré que le schéma était consistant et bien défini, mais il faut encore montrer la stabilité.

B. Stabilité

On convient de dire qu\'un vecteur V est positif si toutes ses composantes sont positives.
Là, on montre que $$ MV \\ge 0 \\Rightarrow V \\ge 0 $$ par un argument sur le min des composantes de V.
Là, j\'avais préparé la démonstration pour montrer que cela impliquait que les termes du schéma était toujours compris entre 0 et 1, mais il m\'a dit qu\'il ne me restait plus que 10 minutes, donc j\'ai dit qu\'on allait passer aux applications numériques, mais qu\'on pourrait reparler de cela à la fin.


III. Applications numériques

A. Vitesse

On prend L=40, $\\nu =1$, $\\kappa=1$
Là, j\'ai présenté mon schéma avec une condition initiale qui était concentrée en zéros, du type 0.8* cos(2*\\pi x ) entre -10 et 10 et 0 sinon. L\'animation montrait qu\'il y avait diffusion sur les bords et au centre on allait rejoindre la population "idéal" $\\kappa$, puis ensuite qu\'il y avait diffusion quand 1 était atteint.
Ensuite, j\'ai présenté une animation avec la même condition initiale que précédemment et deux $\\nu$, on voyait que sur les bords la courbe avec le plus grand $\\nu$ "allait plus vite". On avait donc la bonne interprétation de $\\nu$ dans le bilan du début.
J\'ai dit que cela pouvait être utile pour calculer la vitesse de propagation du population d\'animaux sauvages, etc.

B. Ondes progressives

Le texte montrait qu\'il existait une onde se déplaçant en tanh(c(x-\\lambda*t)) pour des c et \\lambda convenables , j\'ai montré mes graphes avec encore une animation, où l\'on voyait la solution exacte et le schéma.
Là il m\'a dit qu\'il fallait conclure.

C. Ordre

J\'ai juste dit que mon dernier programme avait pas marché, mais que je voulais mettre en évidence l\'ordre du schéma à partir de l\'onde progressive qu\'on avait calculer, et que normalement on devrait se retrouver avec une cassure quand le pas de temps devenait assez petit, car comme l\'ordre est de 2 en espace, c\'est inutile de calculer au bout d\'une moment, car on ne gagne plus en précision.
',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        49 => 
                        array (
                            'id' => 70,
                            'retour_id' => 6,
                            'question_id' => 50,
                            'reponse_texte' => 'Questions :

Q : Qu\'est-ce qu\'il se passe si on traite le terme diffusif en explicite ?
R : En général, quand on traite un problème en implicite plutôt qu\'en explicite c\'est pour gagner en précision.

Q : Ah bon ?
R : Euh … ah bah oui non on gagne rien du tout les ordres restent les mêmes ici.

Q : Qu\'est-ce que vous feriez pour gagner un ordre en temps ici ?
R : On peut tenter d\'approcher la dérivée en temps par un truc du type

$$ u^{n+1}_j-u^{n-1}_j$$

Je sais pas si cela marche, mais on est passé à la suite, très vite.

Q : Et si on traite le terme de création de population (g(u(x,t)) en implicite qu\'est-ce qu\'il se passe ?
R : Il faut utiliser la méthode de Newton. J\'essaye de l\'expliquer, mais ça n\'a pas l\'air clair donc l\'examinateur me demande d\'écrire au tableau, ce que je fais et on passe à la suite.

Q : Si on oublie la dépendant en temps, qu\'est-ce qu\'il se passe ?
R : Il faut réécrire la matrice et en fait le problème n\'admet plus nécessairement une unique solution, ce qui se répercute dans le problème discret car la matrice n\'est plus définie positive.

Q : Vous avez dit le taux de croissance pouvait dépendre du temps, vous pouvez préciser ?
R : En fait il faut écrire un coefficient $\\alpha$ dépendant du temps, par exemple

$$ \\alpha (t) = \\alpha _0 ( 1+ cos(\\omega t))$$

On choisit le terme périodique, car l\'année se répète.


Q : À quelle condition une solution d\'une telle équation est-elle périodique ?
R : $\\int_0^T \\alpha(s) ds = 0 $ où $ T = \\frac{ 2 \\pi}{\\omega} $
',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        50 => 
                        array (
                            'id' => 71,
                            'retour_id' => 6,
                            'question_id' => 51,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        51 => 
                        array (
                            'id' => 72,
                            'retour_id' => 6,
                            'question_id' => 52,
                            'reponse_texte' => 'Attitude : assez cassant au début mais décontracté vers la fin.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        52 => 
                        array (
                            'id' => 73,
                            'retour_id' => 6,
                            'question_id' => 53,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        53 => 
                        array (
                            'id' => 74,
                            'retour_id' => 6,
                            'question_id' => 54,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        54 => 
                        array (
                            'id' => 75,
                            'retour_id' => 7,
                            'question_id' => 46,
                            'reponse_texte' => 'un truc sur les différences finies qui est une modélisation d\'un système physique',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        55 => 
                        array (
                            'id' => 76,
                            'retour_id' => 7,
                            'question_id' => 47,
                            'reponse_texte' => 'un truc sur les différences finies qui n\'est pas une modélisation d\'un système physique',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        56 => 
                        array (
                            'id' => 77,
                            'retour_id' => 7,
                            'question_id' => 48,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        57 => 
                        array (
                            'id' => 78,
                            'retour_id' => 7,
                            'question_id' => 49,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        58 => 
                        array (
                            'id' => 79,
                            'retour_id' => 7,
                            'question_id' => 50,
                            'reponse_texte' => 'quelle est la dérivée de l\'inverse de f^-1, d\'où ça sort ? et ensuite il s\'est rendormi. Comment on obtient le schéma, pourquoi telle fonction intégrale à paramètre est continue, croissante, que des conneries du genre.

J\'ai sorti 2 énormes conneries, ils sont revenus dessus et sinon que des \'ah mais vous avez dit ça mais comment vous le faites exactement ?\' Rien dont je n\'ai pas parlé, pourtant il y avait quelques trucs à dire, mais il y avait masse de calculs dont beaucoup d\'ellipses de ma part pour tenir dans le temps imparti.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        59 => 
                        array (
                            'id' => 80,
                            'retour_id' => 7,
                            'question_id' => 51,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        60 => 
                        array (
                            'id' => 81,
                            'retour_id' => 7,
                            'question_id' => 52,
                            'reponse_texte' => 'Le niveau des questions ? Très variées entre débile et moyen. Certains m\'ont totalement déstabilisées tellement c\'était con, mais j\'ai bien répondu. Un type dormait tout le temps et se réveillait de temps à autre pour dire des trucs bizarres puis se rendormait. Une femme qui a pas parlé, une question, j\'ai été trop rapide, elle m\'a regardé plus lentement, j\'ai réexpliqué, elle était contente et les autres très sympas.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        61 => 
                        array (
                            'id' => 82,
                            'retour_id' => 7,
                            'question_id' => 53,
                            'reponse_texte' => 'Très bizarre, déjà seulement 40 minutes, je croyais 45, mais bon, un peu dépassé ils ont rien dit, le mec qui dort fin voilà ... C\'est eux qui allume et éteignent le rétro qui est au centre du tableau, c\'est pas un rétro à côté ni un écran individuel pour les profs et la question finale qui m\'a laissé sans voix \'que garderiez vous si vous étiez professeur de terminal?\' et fin, je lui ai expliqué que bon tu peux leur faire faire le schéma et voir que ça converge, mais bon vu que TOUT le reste, pourquoi ça converge, pourquoi tu prends ce schéma, pourquoi c\'est intéressant, pourquoi la solution existe, fin je vois pas du tout l\'intérêt ... Enfin au moins j\'ai pas eu \'que faites vous si un élève vous frappe?\', peut être demain Bref, malade comme un chien, stressé comme pas possible et des stupidités incroyables, mais ils ont été très sympa, fin ceux qui parlaient et ils ont passé du temps à tout expliquer c\'était plutôt sympa et 4h c\'était limite !',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        62 => 
                        array (
                            'id' => 83,
                            'retour_id' => 7,
                            'question_id' => 54,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        63 => 
                        array (
                            'id' => 84,
                            'retour_id' => 8,
                            'question_id' => 46,
                            'reponse_texte' => 'B90',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        64 => 
                        array (
                            'id' => 85,
                            'retour_id' => 8,
                            'question_id' => 47,
                            'reponse_texte' => 'B87',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        65 => 
                        array (
                            'id' => 86,
                            'retour_id' => 8,
                            'question_id' => 48,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        66 => 
                        array (
                            'id' => 87,
                            'retour_id' => 8,
                            'question_id' => 49,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        67 => 
                        array (
                            'id' => 88,
                            'retour_id' => 8,
                            'question_id' => 50,
                        'reponse_texte' => 'Des questions sur l exposé (des trucs qu ils avaient pas compris et que j\'avais mal expliqué et des "fautes de frappe" au tableau (une matrice M au mauvais endroit et un t au lieu d un x)...) et de cours...

Le calcul que vous aviez fait on pouvait pas le faire autrement? (sur deux calculs que j\'avais fait) Pourquoi l\'avez vous fait ainsi? (pourquoi pas?...)

citez Cauchy Lipschitz et le lemme de sortie de tout compact.

Refaites une étude qualitative déjà faite, refaites une preuve

est ce que votre animation bouge? (oui elle bouge...)

Pourquoi avez vous choisi ce pas de temps? (parce qu\'il marche...)

Comment inverser la matrice? (tridiagonale donc je dis LU) Pas mieux ? (symétrique donc LU c est pareil que Cholesky) Pourquoi vous avez pas dit cholesky? (parce que c est pareil ... c est symétrique...)

Comment on élimine la condition sur dt? (je m embrouille un peu, mais finis par parler des schémas amonts et avaux, et lequel convient dans quel cas d\'équation au transport... Non ce qui les intéressait était sur y\'=lambda*y lequel convient..)

Qu\'est ce qui se passe si on part au dessus du seuil critique ? (aucun sens qualitatif pour le développement d\'une population, c\'est pour ca que c\'est un seuil critique... mais j\'explique que la population dépérit lentement)

Sur l\'équation sur tout R on a une solution, vous l\'avez tracé sur un segment quelle équation vérifie t elle? (je dis que c est presque l équation... "ah presque!"... On est en analyse numérique, c\'est évidemment une approximation que je ne peux pas représenter sur tout R, je la trace sur un segment du coup... "mais pourquoi on peut le faire?"... Parce que c\'est une tangente hyperbolique...) ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        68 => 
                        array (
                            'id' => 89,
                            'retour_id' => 8,
                            'question_id' => 51,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        69 => 
                        array (
                            'id' => 90,
                            'retour_id' => 8,
                            'question_id' => 52,
                        'reponse_texte' => 'Le niveau des questions était facile (soit je l\'avais déjà expliqué, soit c était du cours "citez Cauchy Lipschitz et le lemme de sortie de tout compact" soit c était des trucs qu ils avaient pas vu " tu aurais du inverser la matrice avant la boucle" (je l\'avais fait...)). Quelques autres hors du lot.

L\'attitude du jury : je l\'ai trouvé cassant, pas intéressé parce que je disais... ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        70 => 
                        array (
                            'id' => 91,
                            'retour_id' => 8,
                            'question_id' => 53,
                        'reponse_texte' => '    Même texte que charles, il l\'a très bien décrit. Honnêtement j\'ai eu l\'impression de ne jamais avoir aussi bien réussi l\'étude d\'un texte. J\'ai répondu correctement à toutes les questions, j\'avais des animations, des graphes, des études de célérité au delà du texte, tout tournait correctement... Sur la présentation : j\'arrive, ils voient mon nom et un dit "ah un rémi, bravo" (mon second nom), je dis que je ne l\'aime pas trop, il devient froid d\'un coup ("vous venez de perdre les points que vous auriez gagné")... Du coup je stresse un peu. J\'écris avec mon écriture habituelle (je sais...), présente le problème, donne des explications qualitatives, des interprétations, ils commencent à faire la gueule... Je panique encore un peu, et fais une faute d\'inattention (le t à la place du x sur la dérivée...). Ils le signalent, on continue. Ca me fait perdre du temps (ils n\'ont pas arrêté le chrono pour l\'interruption). Du coup je dois aller un peu plus vite, je me plante en mettant une matrice ou il faut pas (mais ma description orale ne laisse pas de doutes qu\'il fallait la mettre a coté, puis un max au lieu d\'un min "vous pouvez faire attention pour une fois et mettre un min?"), puis je décris mon animation. Je pense faire une bonne description qualitative. Sur la troisième partie je dois faire avec les mains faute de temps (c était un calcul, un système..) mais je montre mon animation, et j\'explique pourquoi c\'est bien. J\'ouvre en suggérant des études de célérité des ondes progressives observées dans le cas général. Un peu trouble par moments, quelques secousses et pas ma meilleure présentation, mais globalement j\'étais content. Visiblement pas eux. Ils sont revenus sur ce que j\'ai fait, m\'ont fait écrire tous les théorèmes (que j\'avais cité), se sont plaint que j\'aie fait les calculs d\'une façon et pas d\'une autre... Et au final je pense se fichaient totalement des descriptions que j\'avais faites. Je n\'ai aucune idée de la note qu\'ils veulent me donner et je suis vraiment dégoûté d\'avoir sué pour aller plus loin que le texte pour, semble t il, rien.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        71 => 
                        array (
                            'id' => 92,
                            'retour_id' => 8,
                            'question_id' => 54,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        72 => 
                        array (
                            'id' => 93,
                            'retour_id' => 9,
                            'question_id' => 55,
                            'reponse_texte' => 'Un truc sur le cardinal de groupes abéliens finis à ordre fixé',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        73 => 
                        array (
                            'id' => 94,
                            'retour_id' => 9,
                            'question_id' => 56,
                            'reponse_texte' => 'Un truc sur Lotka-Volterra',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        74 => 
                        array (
                            'id' => 95,
                            'retour_id' => 9,
                            'question_id' => 57,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        75 => 
                        array (
                            'id' => 96,
                            'retour_id' => 9,
                            'question_id' => 58,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        76 => 
                        array (
                            'id' => 97,
                            'retour_id' => 9,
                            'question_id' => 59,
                            'reponse_texte' => 'Pendant ma présentation j\'ai foiré mon calcul de complexité, du coup 90% DES PUTERELLES DE QUESTION ONT ETE SUR DES CALCULS DE COMPLEXITE DE SCHEISS.
Quelques questions sur des trucs que j\'ai dit un peu vite dans ma présentation (et qui étaient faux)

Donner la comlpexité du calcul de $\\prod_{k=1}^{+\\infty}\\frac{1}{1-X^k}~~mod~X^{e+1}$ et trouver que ça fait $O(e^2)$ (c\'est franchement super rigolo comme calcul, je vous conseille chaudement ça comme une activité à faire en famille le samedi soir pour égayer vos veillées funèbres)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        77 => 
                        array (
                            'id' => 98,
                            'retour_id' => 9,
                            'question_id' => 60,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        78 => 
                        array (
                            'id' => 99,
                            'retour_id' => 9,
                            'question_id' => 61,
                            'reponse_texte' => 'Le niveau était plutôt bas, j\'ai l\'impression que le jury a pris pitié quand il m\'a vu m\'empêtrer dans mes calculs de complexité du coup il m\'aidait bien.
Un des membres du jury avait une longue vue pour voir ce que j\'écrivais au tableau :D #cool #incongru #pirate
Pour une fois, un membre du jury hochait de la tête quand je disais des trucs. Ça doit être l\'attitude de jury la plus humaine que j\'aie vue pendants mes oraux.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        79 => 
                        array (
                            'id' => 100,
                            'retour_id' => 9,
                            'question_id' => 62,
                            'reponse_texte' => 'Tout s\'est passé aussi moyennement que prévu.
P\'tite cace-dédi à Michel qu\'a eu la présence d\'esprit de reposer le manuel de Sage dans la malle avant de passer en oral de manière à ce que je puisse commencer avec.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        80 => 
                        array (
                            'id' => 101,
                            'retour_id' => 9,
                            'question_id' => 63,
                            'reponse_texte' => '14.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        81 => 
                        array (
                            'id' => 102,
                            'retour_id' => 10,
                            'question_id' => 55,
                        'reponse_texte' => 'Cryptage à clef publique dans GLn(Fq)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        82 => 
                        array (
                            'id' => 103,
                            'retour_id' => 10,
                            'question_id' => 56,
                            'reponse_texte' => 'Des séries formelles, me rappelle plus trop..',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        83 => 
                        array (
                            'id' => 104,
                            'retour_id' => 10,
                            'question_id' => 57,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        84 => 
                        array (
                            'id' => 105,
                            'retour_id' => 10,
                            'question_id' => 58,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        85 => 
                        array (
                            'id' => 106,
                            'retour_id' => 10,
                            'question_id' => 59,
                        'reponse_texte' => 'Questions principalement sur l\'exposé, mais mon intro était assez solide et recouvrait déjà les thématiques adjacentes au texte (logarithme dans les corps finis, stratégie des cryptages à clef publique..)

un peu stressé et fatigué pendant ma présentation (commencer à 6h30 c\'est dur..), j\'ai fait une démo fausse dans mon exposé, du coup j\'ai du la refaire (heureusement, elle était correcte dans mes notes, juste fausse telle qu\'écrite au tableau).
On m\'a demandé de réfléchir aux points que j\'avais admis dans ma présentation, notamment le calcul d\'un polynome minimal, et une partie de l\'algo que j\'avais pas implémentée.
Pas vraiment d\'exercices, mais on a un peu discuté de réduction de Jordan dans les corps finis, notamment pour des questions d\'ordres de blocs de Jordan.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        86 => 
                        array (
                            'id' => 107,
                            'retour_id' => 10,
                            'question_id' => 60,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        87 => 
                        array (
                            'id' => 108,
                            'retour_id' => 10,
                            'question_id' => 61,
                            'reponse_texte' => 'les questions étaient relativement faciles, j\'y répondais généralement entre quelques phrases. Le jury était le même que celui de dosso, mention spécial au vieillard joufflu dont le nez rouge et les jumelles rétractables m\'ont fait craindre l\'arrêt cardiaque à chaque fois qu\'il esquissait un geste.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        88 => 
                        array (
                            'id' => 109,
                            'retour_id' => 10,
                            'question_id' => 62,
                            'reponse_texte' => 'oral assez sympathique, même si le tableau est assez dur à gérer vu qu\'on ne choisit pas trop comment / quand utiliser l\'ordinateur.

Tkt dosso jte lache pas buddy',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        89 => 
                        array (
                            'id' => 110,
                            'retour_id' => 10,
                            'question_id' => 63,
                            'reponse_texte' => '17.5',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        90 => 
                        array (
                            'id' => 111,
                            'retour_id' => 11,
                            'question_id' => 55,
                            'reponse_texte' => 'Multiplication rapide de polynômes en caractéristique 2.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        91 => 
                        array (
                            'id' => 112,
                            'retour_id' => 11,
                            'question_id' => 56,
                            'reponse_texte' => 'Équivalence de nuages de points.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        92 => 
                        array (
                            'id' => 113,
                            'retour_id' => 11,
                            'question_id' => 57,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        93 => 
                        array (
                            'id' => 114,
                            'retour_id' => 11,
                            'question_id' => 58,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        94 => 
                        array (
                            'id' => 115,
                            'retour_id' => 11,
                            'question_id' => 59,
                            'reponse_texte' => 'Pas mal de questions sur l\'exposé et mon informatique, quelques questions sur la partie du texte que je n\'ai pas eu le temps d\'aborder.

Réexpliquer un bout de mon informatique sur lequel j\'étais allé un peu vite.

Corriger une remarque où j\'avais indiqué une équivalence alors que seule une implication était vraie.

Justifier des calculs de complexité et quelques calculs que je n\'avais pas eu le temps de détailler.

Essayer de justifier un théorème que j\'avais laissé de côté (je n\'ai pas réussi faire quoi que ce soit là-dessus).

Comment construit-on une clôture algébrique de $\\mathbb{F}_2$ (évoquée dans mon exposé pour justifier le fait qu\'il n\'y a qu\'une racine $2^k$-ème de l\'unité dans un corps de caractéristique $2$) ?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        95 => 
                        array (
                            'id' => 116,
                            'retour_id' => 11,
                            'question_id' => 60,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        96 => 
                        array (
                            'id' => 117,
                            'retour_id' => 11,
                            'question_id' => 61,
                        'reponse_texte' => 'Questions plutôt faciles à mon avis. Jury plus agréable et (car ?) plus jeune que ceux d\'algèbre et d\'analyse.

',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        97 => 
                        array (
                            'id' => 118,
                            'retour_id' => 11,
                            'question_id' => 62,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        98 => 
                        array (
                            'id' => 119,
                            'retour_id' => 11,
                            'question_id' => 63,
                            'reponse_texte' => '17.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        99 => 
                        array (
                            'id' => 120,
                            'retour_id' => 12,
                            'question_id' => 55,
                            'reponse_texte' => 'C47 : Usinage de de courbes',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        100 => 
                        array (
                            'id' => 121,
                            'retour_id' => 12,
                            'question_id' => 56,
                        'reponse_texte' => 'C37 : un truc de cryptographie (cf Nicolas ?)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        101 => 
                        array (
                            'id' => 122,
                            'retour_id' => 12,
                            'question_id' => 57,
                            'reponse_texte' => 'On a une courbe

$$ \\alpha(t) = (x(t), y(t))$$

définie sur un segment. On a une fraiseuse avec laquelle on aimerait tracer la courbe. La fraiseuse est circulaire de rayon $r$ et passe par une courbe décalée à $\\alpha$ :

$$ \\beta = \\alpha + r \\vec{n} $$

où $n= (-y\', x\')/ ||\\alpha\'||$ est la normale. Pour avoir des trucs faciles à calculer on suppose que $||\\alpha\'||$ est polynomiale ainsi que $x\'$ et $y\'$. On en déduit une condition sur $x,y$.

De là il y a des problèmes d\'intersection qui apparaissent : si $r$ est trop grand (la fraise est trop grosse) on va manger trop de matière. Il faut donc regarder les points d\'auto-intersection de la courbe décalée. De même si on définie une courbe par morceaux il y a des risques d\'intersection entre deux courbes décalées. D\'où on cherche les points d\'intersection de deux courbes pour éviter les détériorations.

Pour tout ça on utilise en gros la multiplicité d\'un point d\'une courbe et le résultant avec lequel on arrive à détecter ces points.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        102 => 
                        array (
                            'id' => 123,
                            'retour_id' => 12,
                            'question_id' => 58,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        103 => 
                        array (
                            'id' => 124,
                            'retour_id' => 12,
                            'question_id' => 59,
                        'reponse_texte' => 'Pas vraiment des questions de mathématiques fondamentales. Surtout des questions pour voir si j\'arrive à mettre en contexte les mathématiques. Les détails des preuves étaient facultatives. Les questions étaient plutôt \'évasives\' (?)

Questions :

Q : éclairer un point du début

Q : Est ce qu\'il y a des courbes qui poseront forcément un problème ?
R : Oui, genre un point de rebroussement.

Q : Pour toute courbe le $r$ est minoré nan ?
R : Par le rayon de courbure

Q : Et la complexité de tout ça ?
R : Pgcd c\'est polynomial, division tout ça, ... multiplication de poly en $n\\ln(n)$ avec Transformée de Fourier Rapide. Résultant c\'est du déterminant ou par méthode du pgcd donc polynomial.

Q : Les courbes de Béziers vous connaissez ?
R : C\'est des courbes polynomiales qui interpolent le passage en des points de manière lisse $C^1$.

Q : Yep et c\'est quoi le degré de ces polynômes ?
R : Bah il y a quatre conditions donc de degré $4$.

Q : Est ce que vous vu la suite ?
R : nan ça partait trop en lattes + quelque chose de politiquement correct

Q : Si le déterminant d\'une matrice est très petit. Genre 0 normalement mais à cause des arrondis ça marche pas. Est ce que vous savez calculer un élément du noyau ? [j\'avais soulevé ce problème dans ma présentation]
R : pas trop

Q : [un truc chelou du genre : qu\'est ce qu\'on obtient avec la fraiseuse ? ils me demandaient des propriétés mathématiques] ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        104 => 
                        array (
                            'id' => 125,
                            'retour_id' => 12,
                            'question_id' => 60,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        105 => 
                        array (
                            'id' => 126,
                            'retour_id' => 12,
                            'question_id' => 61,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        106 => 
                        array (
                            'id' => 127,
                            'retour_id' => 12,
                            'question_id' => 62,
                            'reponse_texte' => 'L\'ordinateur et Sage ont été pas mal coopératifs. Je les remercie. Par contre la touche espace ... ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        107 => 
                        array (
                            'id' => 128,
                            'retour_id' => 12,
                            'question_id' => 63,
                            'reponse_texte' => '19.5',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        108 => 
                        array (
                            'id' => 129,
                            'retour_id' => 13,
                            'question_id' => 64,
                            'reponse_texte' => 'D88 - Théorie des jeux via satisfiabilité',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        109 => 
                        array (
                            'id' => 130,
                            'retour_id' => 13,
                            'question_id' => 65,
                            'reponse_texte' => 'D85 - Codes correcteurs',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        110 => 
                        array (
                            'id' => 131,
                            'retour_id' => 13,
                            'question_id' => 66,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        111 => 
                        array (
                            'id' => 132,
                            'retour_id' => 13,
                            'question_id' => 67,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        112 => 
                        array (
                            'id' => 133,
                            'retour_id' => 13,
                            'question_id' => 68,
                            'reponse_texte' => 'Beaucoup de questions sur l\'exposé, le modèle présenté et ses hypothèses. Sur le code aussi et les mécanismes derrière les primitives utilisées.

- List.hd et List.tl sont-elles des fonctions totales ?

- pourquoi telle ou telle hypothèse ? Quelles conséquences si on la change ?

- une remarque sur une erreur de style dans mon code (vu la taille du fichier que j\'ai pondu, ça n\'est pas étonnant d\'avoir quelques petites maladresses par-ci par-là)

Le texte portait sur la modélisation de procédés automatiques par un jeu : une machine doit réaliser une chose sans être mise en échec par les actions de son environnement. On voit ça comme un jeu à deux joueurs représenté par un graphe (les sommets où A joue et ceux où B joue plus les transitions entre les sommets selon les règles du jeu). On voit deux types de jeux : le joueur A veut atteindre l\'un des sommets d\'un ensemble donné, ou il veut l\'atteindre une infinité de fois.

- questions sur un exemple basé sur un problème de réseaux (l\'instanciation sous forme de jeu est un peu complexe) : on doit envoyer 9 messages d\'un noeud à un autre au travers d\'un petit réseau, sachant qu\'à chaque instant un seul lien peut être cassé et à chaque instant au plus un message transite sur chaque lien.

Pour calculer l\'ensemble des sommets d\'où A a une stratégie gagnante, on modélise le jeu par un ensemble de clauses de Horn dont on vérifie la satisfiabilité par un petit programme (exercice de programmation).

- est-on certain d\'avoir produit assez de clauses pour tout avoir ?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        113 => 
                        array (
                            'id' => 134,
                            'retour_id' => 13,
                            'question_id' => 69,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        114 => 
                        array (
                            'id' => 135,
                            'retour_id' => 13,
                            'question_id' => 70,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        115 => 
                        array (
                            'id' => 136,
                            'retour_id' => 13,
                            'question_id' => 71,
                        'reponse_texte' => 'Très petit tableau, j\'ai du demander à effacer une partie et j\'ai fini à l\'oral (pour les remarques du genre intérêt et faiblesses du modèle et l\'ouverture à la fin).',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        116 => 
                        array (
                            'id' => 137,
                            'retour_id' => 13,
                            'question_id' => 72,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        117 => 
                        array (
                            'id' => 138,
                            'retour_id' => 14,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 555,
                            'reponsable_type' => 'lecon',
                        ),
                        118 => 
                        array (
                            'id' => 139,
                            'retour_id' => 14,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 574,
                            'reponsable_type' => 'lecon',
                        ),
                        119 => 
                        array (
                            'id' => 140,
                            'retour_id' => 14,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 73,
                            'reponsable_type' => 'developpement',
                        ),
                        120 => 
                        array (
                            'id' => 158,
                            'retour_id' => 16,
                            'question_id' => 5,
                            'reponse_texte' => 'Les questions ont porté sur :
- la décomposition en éléments simples (comment calculer les coeffs, etc)
- le fait qu\'une conique soit rationnelle
- un peu sur le développement',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        121 => 
                        array (
                            'id' => 142,
                            'retour_id' => 14,
                            'question_id' => 5,
                            'reponse_texte' => 'Que des questions portant sur mon plan, mes développements, ce que j\'ai dis dans ma défense.

Suite au théorème de structure des groupes abéliens finis :
-qu\'en est-il de l\'unicité? (je ne prouve que l\'existence)
-le faire sur un exemple: Z/36Z x Z/45Z x Z/60Z

Par rapport à la transformée de Fourier discrète:
-qu\'est-ce que la Fast Fourier Transform?
-comment multiplier des polynômes avec?
-peut-on généraliser les caractères? (y a t\'il une théorie des caractères?)

Sur Sn :
-pouvez-vous donner un système minimal de générateurs?
-que dire en général sur le cardinal d\'un famille génératrice pour un groupe fini quelconque?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        122 => 
                        array (
                            'id' => 143,
                            'retour_id' => 14,
                            'question_id' => 6,
                            'reponse_texte' => 'Jury pas cassant mais pas sympathique non plus, aidant juste ce qu\'il faut.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        123 => 
                        array (
                            'id' => 144,
                            'retour_id' => 14,
                            'question_id' => 8,
                            'reponse_texte' => 'Oral qui s\'est plutôt bien passé, juste 2 surprises:
-on a des tableaux blancs à marqueur
-pas mal d\'aller-retours et de bruit pendant la préparation',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        124 => 
                        array (
                            'id' => 145,
                            'retour_id' => 14,
                            'question_id' => 9,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        125 => 
                        array (
                            'id' => 146,
                            'retour_id' => 15,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 625,
                            'reponsable_type' => 'lecon',
                        ),
                        126 => 
                        array (
                            'id' => 147,
                            'retour_id' => 15,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 607,
                            'reponsable_type' => 'lecon',
                        ),
                        127 => 
                        array (
                            'id' => 148,
                            'retour_id' => 15,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 18,
                            'reponsable_type' => 'developpement',
                        ),
                        128 => 
                        array (
                            'id' => 264,
                            'retour_id' => 30,
                            'question_id' => 14,
                            'reponse_texte' => 'quelques questions sur le plan et deux exos. 

- Une question sur le dev (le calcul de la transformée de $\\delta_0$, je l\'ai fait en le voyant comme une distrib - ça prend une ligne - et ils m\'ont demandé de le faire "directement", par un calcul de transformée pour une mesure du coup)

- Questions sur le plan : dans quels items du plan j\'utilisais le théorème de changement de variable (j\'explique vite fait le calcul de l\'intégrale de la gaussienne notamment) ; pourquoi j\'avais mentionné la marche aléatoire sur $\\mathbb{Z}^d$ (c\'était un exemple donné après l\'intégration des $1 /||x||^{\\alpha}$, je me suis embrouillé à pas trop savoir quoi dire et quoi survolé mais j\'ai finalement justifié la présence du truc (on intègre des trucs de la forme $1/(1-cos(x))$) ; est-ce que j\'avais un autre exemple de calcul des résidus en tête (hic, j\'ai donné que mon dev 1 comme exemple, je savais pas où en chercher d\'autre - ben j\'en avais pas, c\'est un peu con).  

- Exo 1 :

$$
I_n = \\int_0^n (1-x/n)^n \\ln(x) dx
$$

Donner la limite de $I_n$ (dire ce que c\'est puis le montrer). En déduire la valeur de $\\int_0^{\\infty} \\exp(-x) \\ln(x) dx$.

---

Que du bon calcul calculatoire de taupin. Le machin auquel on pense tend vers l\'exponentielle et donc, incroyable, la limite de $I_n$ c\'est l\'autre intégrale.

On commence par mettre une $\\mathbb{1}_{[0,n]}$ pour que l\'intégrale soit gérable, puis on fait de la CVD ; pour la domination, on peut passer par la concavité du log et hop. Pour calculer l\'intégrale ensuite il va falloir une écriture explicite de $I_n$ : on change de variable pour virer les $x/n$, on découpe ce qu\'il reste du log, ce qui fait poper un terme qui s\'intègre directement et un autre qu\'on peut calculer par IPP. On a arrêté l\'exo une fois que j\'avais amorcé l\'IPP, on m\'a vite fait demandé si je savais la limite de $\\ln(n) - \\sum_{k=1}^n 1/k$ qui intervient à la fin (houuuuu).

- Exo 2 : on prend $A$ une matrice symétrique. Deviner quel exo on va bien pouvoir faire avec dans cette leçon.

---

Bon, le vrai exo, c\'était évidemment d\'étudier $\\int_{\\mathbb{R}^n} \\exp(-(Ax,x)) dx$, et de la calculer quand $A$ est $S_n^{++}$. Voilà, voilà... ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        129 => 
                        array (
                            'id' => 150,
                            'retour_id' => 15,
                            'question_id' => 14,
                            'reponse_texte' => 'Que exercices sur le plan.

Une variable aléatoire discrète admettant des moments à tout ordre est-elle caractérisée par ses moments? (en rapport avec les séries génératrices)

Calculer $\\sum_{1}^{+ \\infty} \\frac{(-1)^n}{n}$

Si f est DSE en 0 avec un RCV de 1, est-elle DSE en 1/2? Quel est le RCV?

Connaissez-vous une autre démonstration du théorème de d\'Alembert que celle à partir du théorème de Liouville? (avec des outils plus simples)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        130 => 
                        array (
                            'id' => 151,
                            'retour_id' => 15,
                            'question_id' => 15,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        131 => 
                        array (
                            'id' => 152,
                            'retour_id' => 15,
                            'question_id' => 17,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        132 => 
                        array (
                            'id' => 153,
                            'retour_id' => 15,
                            'question_id' => 18,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        133 => 
                        array (
                            'id' => 155,
                            'retour_id' => 16,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 570,
                            'reponsable_type' => 'lecon',
                        ),
                        134 => 
                        array (
                            'id' => 156,
                            'retour_id' => 16,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 581,
                            'reponsable_type' => 'lecon',
                        ),
                        135 => 
                        array (
                            'id' => 157,
                            'retour_id' => 16,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 47,
                            'reponsable_type' => 'developpement',
                        ),
                        136 => 
                        array (
                            'id' => 159,
                            'retour_id' => 16,
                            'question_id' => 6,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        137 => 
                        array (
                            'id' => 160,
                            'retour_id' => 16,
                            'question_id' => 8,
                            'reponse_texte' => 'L\'oral s\'est assez bien passé, le jury était assez sympa. J\'ai mis le théorème de Lüroth avec des applications aux courbes rationnelles, d\'où la question sur les coniques.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        138 => 
                        array (
                            'id' => 161,
                            'retour_id' => 16,
                            'question_id' => 9,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        139 => 
                        array (
                            'id' => 173,
                            'retour_id' => 18,
                            'question_id' => 5,
                            'reponse_texte' => 'Questions sur le développement, notamment l\'importance du poids et de si la récurrence passe ou pas. Question facile, mais avoir les idées au clair, ce qui fut laborieusement le cas pour moi.

Questions sur la factorialité de A[X1,… XN], j\'ai parlé de l\'euclidiennité, la factorialité, etc. et de voir si cela passe à l\'anneau de polynômes.

Donner un exemple de polynôme à plusieurs variables qui admet un infinité de racines sans être nul.

Question : On se donne un polynôme de 3 variables sur R symétrique. Ses zéros forment une quadrique de révolution. ( petit bad )',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        140 => 
                        array (
                            'id' => 163,
                            'retour_id' => 17,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 634,
                            'reponsable_type' => 'lecon',
                        ),
                        141 => 
                        array (
                            'id' => 164,
                            'retour_id' => 17,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 613,
                            'reponsable_type' => 'lecon',
                        ),
                        142 => 
                        array (
                            'id' => 166,
                            'retour_id' => 17,
                            'question_id' => 14,
                            'reponse_texte' => 'J\'ai eu quelques questions sur le developpement, par exemple préciser le fait que l\'espace des mesures de proba sur $R^{d}$ est métrisable compact pour la convergence étroite. 

Sinon, deux exos : un sans rapport avec la leçon, et un sur les fonctions caractéristiques. 

Que dire d\'une v.a. réelle dont la fonction caractéristique vaut 1 en un $t \\textgreater 0$ ?
-\\textgreater La loi charge les 2k*Pi/t.
',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        143 => 
                        array (
                            'id' => 167,
                            'retour_id' => 17,
                            'question_id' => 15,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        144 => 
                        array (
                            'id' => 168,
                            'retour_id' => 17,
                            'question_id' => 17,
                        'reponse_texte' => 'L\'oral s\'est bien passé. J\'ai été surpris par le 1er exo qui n\'avait pas beaucoup de rapport avec la leçon (calcul de densité marginale).',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        145 => 
                        array (
                            'id' => 169,
                            'retour_id' => 17,
                            'question_id' => 18,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        146 => 
                        array (
                            'id' => 170,
                            'retour_id' => 18,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 572,
                            'reponsable_type' => 'lecon',
                        ),
                        147 => 
                        array (
                            'id' => 171,
                            'retour_id' => 18,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 577,
                            'reponsable_type' => 'lecon',
                        ),
                        148 => 
                        array (
                            'id' => 172,
                            'retour_id' => 18,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 69,
                            'reponsable_type' => 'developpement',
                        ),
                        149 => 
                        array (
                            'id' => 174,
                            'retour_id' => 18,
                            'question_id' => 6,
                            'reponse_texte' => 'Le jury n\'est pas cassant, mais assez fatigué. Il avait l\'air de pas vouloir être là … et ce dès mon arrivée dans la salle.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        150 => 
                        array (
                            'id' => 175,
                            'retour_id' => 18,
                            'question_id' => 8,
                            'reponse_texte' => 'L\'oral s\'est assez mal passé je pense, les questions sur le développement m\'ont un peu trop arrêté du coup on n\'a pas eu le temps de faire des trucs sympas.

Et ils n\'en avaient que faire des applications sympas que j\'avais mise ( détermination des polynômes de matrices invariants par similitude, algorithme de Faadeev, etc. ), sans doute parce que je me fais piéger sur les petites questions ...',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        151 => 
                        array (
                            'id' => 176,
                            'retour_id' => 18,
                            'question_id' => 9,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        152 => 
                        array (
                            'id' => 177,
                            'retour_id' => 19,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 588,
                            'reponsable_type' => 'lecon',
                        ),
                        153 => 
                        array (
                            'id' => 178,
                            'retour_id' => 19,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 580,
                            'reponsable_type' => 'lecon',
                        ),
                        154 => 
                        array (
                            'id' => 179,
                            'retour_id' => 19,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 5,
                            'reponsable_type' => 'developpement',
                        ),
                        155 => 
                        array (
                            'id' => 243,
                            'retour_id' => 27,
                            'question_id' => 5,
                            'reponse_texte' => '$A[X_1,\\ldots,X_n]$ est noethérien, certes. Mais peut-on borner le nombre de générateurs d\'un idéal ?
Début de réponse : si $A$ est un corps, l\'espace des polynômes homogènes de degré $d$ est de dimension $\\begin{pmatrix} n+d-1 \\\\ n-1 \\end{pmatrix}$, qui devient arbitrairement grand quand $d$ croît. 

Est-il alors possible de retirer des générateurs à l\'idéal engendré par les polynômes homogènes pour en borner le nombre ?
Je ne sais pas et on passe à autre chose.

Peut-on utiliser le résultant pour déterminer l\'intérieur des matrices diagonalisables dans $\\mathbb{C}$ ?
Oui, en exprimant la non-annulation du discriminant du polynôme caractéristique, mais ça n\'a rien à voir avec la leçon puisqu\'il n\'y a qu\'une indéterminée.

Que signifie la propriété universelle des anneaux de polynômes (exprimée en termes de morphisme dans mon plan) ?
Ça veut dire que $X_1,\\ldots,X_n$ engendrent $A[X_1,\\ldots,X_n]$ en tant que $A$-algèbre.

Comment démontrer le théorème de d\'Alembert-Gauß en utilisant les relations coefficients-racines ?
Lire Gourdon ou demander à Denis Serre (selon vos préférences).

Est-ce que $A[X_1,\\ldots,X_n]$ est principal ? Pourquoi ?
Si $n\\geqslant 2$, non. Si l\'idéal $(X_1,X_2)$ était engendré par un élément, celui-ci serait une constante pour des raisons de degré. C\'est impossible, encore pour des raisons de degré.

Est-ce que les polynômes à plusieurs indéterminées peuvent servir à résoudre des équations polynomiales, disons de degré $3$ ? Et de degré plus grand ?
Oui, il existe des expressions via les relations coefficients-racines. En degré $4$, ça marche encore, mais dès le degré $5$, ça foire.

Finalement, qu\'est-ce qui se cache derrière tout ça ?
La question de la résolubilité du groupe symétrique, et la théorie de Galois.

Comment enseigneriez-vous ceci à des élèves de niveau L3 ?
Réponse censurée : en faisant des rappels sur les polynômes à une indéterminée et en insistant sur la différence qu\'il y a entre le cas d\'une et de plusieurs indéterminées.
Réponse non censurée : en donnant les définitions et en démontrant les théorèmes, comme pour n\'importe quel autre sujet.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        156 => 
                        array (
                            'id' => 181,
                            'retour_id' => 19,
                            'question_id' => 5,
                        'reponse_texte' => 'sous quelles conditions sur la forme quadratique q a-t-on l\'égalité en F et son double orthogonal  (pour la forme q) ?

on considère la forme quadratique réelle qui a une matrice A associe Tr(A^2). Est elle dégénérée ? quel est son cône nilpotent ? (j\'ai donné des exemples qui montraient qu\'il n\'était pas vide, mais je n\'ai pas su le caractériser, le jury a vite abandonné cette question). quelle est sa signature ? (je n\'ai traité que la dimension 2, puis j\'ai dit que ça se généralisait mais le jury m\'a directement mis sur une autre question). 

Puis : comment enseigneriez-vous cette matière à des élèves de niveau L2 ? et comment l\'exposeriez vous à un public non spécialiste, par exemples des physiciens ? J\'ai répondu qu\'il fallait insister sur la représentation polynomiale, l\'interprétation géométrique avec les coniques, le jury a eu l\'air d\'approuver.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        157 => 
                        array (
                            'id' => 182,
                            'retour_id' => 19,
                            'question_id' => 6,
                            'reponse_texte' => ' jury qui ne donne pas d\'indices mais qui est assez réactif aux propositions',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        158 => 
                        array (
                            'id' => 183,
                            'retour_id' => 19,
                            'question_id' => 8,
                            'reponse_texte' => 'J\'ai eu une hésitation dans mon développement, parce que j\'ai écrit de manière trop dégueulasse et qu\'un a s\'est transformé en q, mais j\'ai su me rattraper.. le jury avait du mal à comprendre le développement, j\'ai du réexpliquer plusieurs points qui étaient pourtant assez simples. L\'un des 3 membres a été complètement muet, les autres n\'avaient pas l\'air de se sentir très concernés par le sujet..',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        159 => 
                        array (
                            'id' => 184,
                            'retour_id' => 19,
                            'question_id' => 9,
                            'reponse_texte' => '15',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        160 => 
                        array (
                            'id' => 185,
                            'retour_id' => 20,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 591,
                            'reponsable_type' => 'lecon',
                        ),
                        161 => 
                        array (
                            'id' => 186,
                            'retour_id' => 20,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 579,
                            'reponsable_type' => 'lecon',
                        ),
                        162 => 
                        array (
                            'id' => 187,
                            'retour_id' => 20,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 41,
                            'reponsable_type' => 'developpement',
                        ),
                        163 => 
                        array (
                            'id' => 189,
                            'retour_id' => 20,
                            'question_id' => 5,
                        'reponse_texte' => 'Montrer que les bissectrices d\'un triangle se croisent (Indication : utiliser l\'équation normale d\'une droite ???) --\\textgreater pas su faire :(

Montrer que les médianes se croisent.

Utilisation de Hahn-Banach ?

On prend deux sphères disjointes, $x_0$ un point de la première, $y_0$ le projeté de ce point sur la deuxième, $x_1$ le projeté de $y_0$ sur le première etc. Montrer que ça converge et dire vers quoi.

Comment motiveriez-vous la géométrie affine à une classe ?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        164 => 
                        array (
                            'id' => 190,
                            'retour_id' => 20,
                            'question_id' => 6,
                            'reponse_texte' => '

Le jury parlait bien mais n\'aidait presque pas.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        165 => 
                        array (
                            'id' => 191,
                            'retour_id' => 20,
                            'question_id' => 8,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        166 => 
                        array (
                            'id' => 192,
                            'retour_id' => 20,
                            'question_id' => 9,
                            'reponse_texte' => '16',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        167 => 
                        array (
                            'id' => 193,
                            'retour_id' => 21,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 620,
                            'reponsable_type' => 'lecon',
                        ),
                        168 => 
                        array (
                            'id' => 194,
                            'retour_id' => 21,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 614,
                            'reponsable_type' => 'lecon',
                        ),
                        169 => 
                        array (
                            'id' => 195,
                            'retour_id' => 21,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 90,
                            'reponsable_type' => 'developpement',
                        ),
                        170 => 
                        array (
                            'id' => 197,
                            'retour_id' => 21,
                            'question_id' => 14,
                            'reponse_texte' => 'f C^2 de R^+ dans R telle que f et f\'\' sont L^2, prouver que f\' est L^2

$||f||_p -\\textgreater ||f||_{+ infty}$ (sur un espace de mesure fini)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        171 => 
                        array (
                            'id' => 198,
                            'retour_id' => 21,
                            'question_id' => 15,
                            'reponse_texte' => 'Jury neutre dans son attitude qui fournissait quelques indications. ',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        172 => 
                        array (
                            'id' => 199,
                            'retour_id' => 21,
                            'question_id' => 17,
                            'reponse_texte' => 'Globalement bien passé je pense même si trois heures c\'est super court.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        173 => 
                        array (
                            'id' => 200,
                            'retour_id' => 21,
                            'question_id' => 18,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        174 => 
                        array (
                            'id' => 201,
                            'retour_id' => 22,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 620,
                            'reponsable_type' => 'lecon',
                        ),
                        175 => 
                        array (
                            'id' => 202,
                            'retour_id' => 22,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 617,
                            'reponsable_type' => 'lecon',
                        ),
                        176 => 
                        array (
                            'id' => 203,
                            'retour_id' => 22,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 90,
                            'reponsable_type' => 'developpement',
                        ),
                        177 => 
                        array (
                            'id' => 205,
                            'retour_id' => 22,
                            'question_id' => 14,
                            'reponse_texte' => 'Questions :

Q : a-t-on des inclusions entre les Lp si l\'espace est de mesure finie ? 
R : Oui, ils sont décroissants, petit temps pour le montrer.

Q : Que se passe-t-il si l\'espace est de mesure infinie ?
R : Là j\'ai dit que je savais qu\'il existait des contre-exemples de fonctions qui sont dans un Lp mais dans aucun autre Lq.

Et que si on se donnait trois indices on avait une inclusion du type précédent. J\'ai commencé à écrire au tableau, mais ils m\'ont arrêté pour la suite

Q : Quelles sont les fonctions f qui convolées à elle-même sont nulles ?
R : Là, j\'ai écrit la formule de la convolution, il m\'a dit "Calme-toi", j\'ai fait : "Ah ok", passons par Fourier.

Et là c\'est posé.

Q :  Vous avez écrit que la transformée de Fourier d\'une fonction L1 est continue, que dire de plus ?
R : Elle tend vers 0 au bord.

Q : Montrer le
R : Euh, bah la démonstration que je connais … euh … repose sur une astuce … Plouf Plouf … 

Q : Admettons le résultat. La transformée de Fourier va de L1 dans les fonctions continues qui tendent vers 0 aux bords, que dire de ce second espace.
R : c\'est un Banach pour la norme infinie.

Q : Vous avez dit que la transformée de Fourier est injective, est-elle surjective dans ces conditions ?
R : Là, je me suis dit que je m\'étais jamais posé cette question. Et j\'ai répondu que je pensais pas vu qu\'on introduisait L2 et S pour travailler sur Fourier en général, du coup qu\'il fallait trouver un contre-exemple ou montrer que les deux espaces étaient pas isomorphes.

Q : Comment fait-on cela ?
R : On peut regarder le caractère séparable, là manque de bol les deux sont séparables, enfin je crois.
( du coup il faudrait regarder le caractère réflexif des deux machins … je sais pas si on peut s\'en sortir )

Q : Passons à autre chose ? Que dire d\'une application continue bijective d\'un banach dans un banach ?
R : La réciproque est continue, par le théorème de l\'application ouverte.

Q : Ok petit con maintenant tu vas nous dire à quoi ça sert dans la vie les espaces Lp ?
R : Euh … plouf plouf … On peut introduire les espaces de Sobolev pour résoudre des équations différentielles plus générale, et même des E.D.P.

Q : Les espaces de Sobolev ? MAIS TU TE FOUS DE MA GUEULE ? Tu crois que tu vas les intéresser les petits cons d\'aujourd\'hui avec leur black berry, leur iphone et le Ternet ?
R : Euh …

Q : Quoi euh !
R : Bah …

Q : Allez dégage toi aussi t\'es un petit con !
R : Bonne journée.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        178 => 
                        array (
                            'id' => 206,
                            'retour_id' => 22,
                            'question_id' => 15,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        179 => 
                        array (
                            'id' => 207,
                            'retour_id' => 22,
                            'question_id' => 17,
                        'reponse_texte' => 'J\'étais pas très bien vu l\'oral de la veille, du coup j\'ai pas parlé de tout ce que j\'avais prévu dans cette leçon ( Sobolev, Stampaccia, Lax-Milgram) et j\'ai surtout bien veillé à la cohérence du plan surtout sur la construction des Lp. Et je me suis retrouvé à faire une leçon sur la convolution et la transformation de Fourier au final en gros ...',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        180 => 
                        array (
                            'id' => 208,
                            'retour_id' => 22,
                            'question_id' => 18,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        181 => 
                        array (
                            'id' => 209,
                            'retour_id' => 23,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 556,
                            'reponsable_type' => 'lecon',
                        ),
                        182 => 
                        array (
                            'id' => 210,
                            'retour_id' => 23,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 583,
                            'reponsable_type' => 'lecon',
                        ),
                        183 => 
                        array (
                            'id' => 212,
                            'retour_id' => 23,
                            'question_id' => 5,
                            'reponse_texte' => 'Jury : Est-ce que la représentation standard est toujours utile ?
Votre serviteur : Oui, mais autant on peut trouver la table de $\\mathfrak{S}_4$, autant c\'est moins évident pour les autres $\\mathfrak{S}_n$. D\'ailleurs, peut-être que ça ne donne qu\'un caractère en plus parfois ?* [Regarde pour la classe des transpositions.] Ah, pour $n\\neq 3$ ça donne bien toujours deux représentations. Et pour $n=3$ [Calculs.] le caractère tordu est le même.
* Ça donne toujours un caractère en plus, et parfois la torsion par la signature en donne un deuxième gratuit.

J (c\'est le vieux qui baragouine) : Est-qu\'il y a un élément d\'ordre 15 dans $\\mathfrak{S}_5$ ?
VS : Non parce que…
J : Regardez le cardinal de $\\mathfrak{S}_5$.
VS, un peu agacé : C\'est 120, donc ça ne nous aide pas. Par contre, on peut conclure parce que si on avait un élément d\'ordre 15, blablabla, d\'après la décomposition en produit de cycles, blablabla.
J : Alors quel est le premier $\\mathfrak{S}_n$ pour lequel on a un élément d\'ordre 15 ?
VS : $\\mathfrak{S}_8$ pour les mêmes raisons.

J : Si on regarde les isométries du carrés comme un sous-groupe de $\\mathfrak{S}_4$, qu\'est-ce qu\'on trouve ?
VS : Alors il y a telle symétrie qui donne telle permutation, telle rotation qui donne telle permutation…
J : Qu\'est-ce que vous êtes en train de construire comme groupe ?
VS : Le groupe diédral $D_4$ ?
J : Et est-ce qu\'on peut généraliser le résultat ?
VS : oui, $D_n$ est toujours inclus dans $\\mathfrak{S}_n$.

J : Que peut-on dire des représentations des groupes de cardinal $p^3$ ?
VS (je fais la version sans l\'hésitation, ça a pris un certain temps) : Soit le groupe est commutatif, et on connait ses représentations, soit $Z(G)$ est de cardinal $p$ (centre non trivial et quotient non cyclique). Ensuite, on peut regarder les représentations de dimension 1, qui se factorisent par le groupe dérivé. Je n\'ai pas été franchement plus loin.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        184 => 
                        array (
                            'id' => 213,
                            'retour_id' => 23,
                            'question_id' => 6,
                            'reponse_texte' => 'Plutôt faisable, pas facile mais pas franchement dur non plus. Les gens ont été plutôt cools, sauf un vieux qui marmonnait et qui ne faisait aucun effort d\'articulation. Mais les autres lui demandaient de se taire, donc ça allait.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        185 => 
                        array (
                            'id' => 214,
                            'retour_id' => 23,
                            'question_id' => 8,
                            'reponse_texte' => 'Pas du tout de questions sur le plan, ça m\'a étonné.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        186 => 
                        array (
                            'id' => 215,
                            'retour_id' => 23,
                            'question_id' => 9,
                            'reponse_texte' => '17.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        187 => 
                        array (
                            'id' => 216,
                            'retour_id' => 24,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 620,
                            'reponsable_type' => 'lecon',
                        ),
                        188 => 
                        array (
                            'id' => 217,
                            'retour_id' => 24,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 608,
                            'reponsable_type' => 'lecon',
                        ),
                        189 => 
                        array (
                            'id' => 218,
                            'retour_id' => 24,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 4,
                            'reponsable_type' => 'developpement',
                        ),
                        190 => 
                        array (
                            'id' => 220,
                            'retour_id' => 24,
                            'question_id' => 14,
                            'reponse_texte' => 'Questions de niveau moyen, même si j\'ai pas forcément très bien répondu.

Le jury était un peu relou, ils écoutaient pas vraiment, ils étaient mous, en gros j\'avais un peu l\'impression qu\'ils s\'en foutaient. Et un des mecs (J.-P. Barani) m\'a plus ou moins forcé à dire que le dual de $L^1$ n\'était pas $L^\\infty$. J\'ai pas trop insisté parce que c\'est un oral, mais j\'ai un peu la haine.

Et aussi, ils ont fait n\'importe quoi administrativement parlant, mais tout s\'est bien passé. Enfin un peu des branleurs quoi.

Jury : La solution que vous avez trouvé, que peut-on en dire à $t\\textgreater0$ fixé ?
Votre serviteur : Eh bien puisqu\'on a montré que $(t,x)\\mapsto u(t,x)$ est $C^\\infty$, en particulier $x\\mapsto u(t,x)$ est lisse aussi.
J : Et comment vous le montreriez ?
VS : Ben… Je ferais ça…
[C\'EST LE TIERS DE MON DÉVELOPPEMENT PENDARD, JE VIENS DE LE FAIRE, TU VEUX PAS ÉCOUTER UNE SECONDE ?]
J : Ah. Et si la dérivée en temps est double, qu\'est-ce qu\'il se passe ?
VS : C\'est l\'équation des ondes, ça ressemble plus à une équation de transport, il n\'y a pas régularisation.

Jury : Soit $f$ l\'indicatrice d\'un ensemble de mesure strictement positive. Sa transformée de Fourier est-elle intégrable ?
Votre Serviteur : Non, sinon elle serait la transformée de Fourier inverse de sa transformée de Fourier, donc continue.

J : Si $f$ et $f^{(n)}$ sont $L^p$, montrez que les $f^{(k)}$ sont bornées.
J\'en ai chié, mais Taylor, ce qui m\'a valu un petit « Ah, je comprends pourquoi vous n\'avez pas pris l\'autre leçon. » du jury. Garder son calme.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        191 => 
                        array (
                            'id' => 221,
                            'retour_id' => 24,
                            'question_id' => 15,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        192 => 
                        array (
                            'id' => 222,
                            'retour_id' => 24,
                            'question_id' => 17,
                            'reponse_texte' => 'Comme dit précédemment, jury un peu borné.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        193 => 
                        array (
                            'id' => 223,
                            'retour_id' => 24,
                            'question_id' => 18,
                            'reponse_texte' => '18',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        194 => 
                        array (
                            'id' => 224,
                            'retour_id' => 25,
                            'question_id' => 19,
                            'reponse_texte' => '',
                            'reponsable_id' => 608,
                            'reponsable_type' => 'lecon',
                        ),
                        195 => 
                        array (
                            'id' => 225,
                            'retour_id' => 25,
                            'question_id' => 20,
                            'reponse_texte' => '',
                            'reponsable_id' => 613,
                            'reponsable_type' => 'lecon',
                        ),
                        196 => 
                        array (
                            'id' => 226,
                            'retour_id' => 25,
                            'question_id' => 21,
                            'reponse_texte' => '',
                            'reponsable_id' => 8,
                            'reponsable_type' => 'developpement',
                        ),
                        197 => 
                        array (
                            'id' => 228,
                            'retour_id' => 25,
                            'question_id' => 23,
                        'reponse_texte' => 'Q: Développement limité à l\'ordre 2 implique-t-il dérivée seconde ? [non. Par exemple f(x)=sin(1/x)x^3]

Q: Avez-vous une idée de la preuve "f et f^(n+1) bornée implique f^(k) bornée pour k entre 1 et n"? [utiliser Taylor-Lagrange et isoler les termes f et f^(n+1). Le polynôme des autres termes sera uniformément borné, puis utiliser l\'équivalence des normes en dimension finie pour borner les coefficients]

Q: Montrer que f(x)=(1-cos(x))/x^2 est C^{infinie}. [Elle est développable en série entière]

Q: Historiquement, quel(s) problème(s) ont motivé l\'introduction de ces notions et quand ? [Sérieusement ?]',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        198 => 
                        array (
                            'id' => 229,
                            'retour_id' => 25,
                            'question_id' => 24,
                            'reponse_texte' => 'Jury plutôt coopératif.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        199 => 
                        array (
                            'id' => 230,
                            'retour_id' => 25,
                            'question_id' => 26,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        200 => 
                        array (
                            'id' => 231,
                            'retour_id' => 25,
                            'question_id' => 27,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        201 => 
                        array (
                            'id' => 232,
                            'retour_id' => 26,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 600,
                            'reponsable_type' => 'lecon',
                        ),
                        202 => 
                        array (
                            'id' => 233,
                            'retour_id' => 26,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 626,
                            'reponsable_type' => 'lecon',
                        ),
                        203 => 
                        array (
                            'id' => 234,
                            'retour_id' => 26,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 34,
                            'reponsable_type' => 'developpement',
                        ),
                        204 => 
                        array (
                            'id' => 236,
                            'retour_id' => 26,
                            'question_id' => 14,
                            'reponse_texte' => 'L\'exercice m\'a paru plutôt difficile. On a seulement traité les cas où K est une boule, puis le cas où K est connexe par arcs. J\'ai eu beaucoup d\'indications, mais le jury était très pressant dès que la démo n\'avançait pas, c\'était assez frustrant.

questions :

comment prouver rapidement le résultat du développement pour les groupes finis ? avez-vous une idée pour généraliser le résultat aux groupes compacts infinis ?

que pouvez-vous me dire sur le th. de représentation conforme ? (référence à la dernière remarque du plan qui parlait de points fixes d\'homographies)

pouvez-vous citer des applications du th. de picard en analyse fonctionnelle ?

un exercice :

Soit K un compact connexe de R^n, U un voisinage ouvert de K, f : U -\\textgreater R^n, C^1, tel que pour tout x dans K, la norme subordonnée de df_x est \\textlesser 1. Montrer que f admet un point fixe dans K.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        205 => 
                        array (
                            'id' => 237,
                            'retour_id' => 26,
                            'question_id' => 15,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        206 => 
                        array (
                            'id' => 238,
                            'retour_id' => 26,
                            'question_id' => 17,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        207 => 
                        array (
                            'id' => 239,
                            'retour_id' => 26,
                            'question_id' => 18,
                            'reponse_texte' => '12',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        208 => 
                        array (
                            'id' => 240,
                            'retour_id' => 27,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 572,
                            'reponsable_type' => 'lecon',
                        ),
                        209 => 
                        array (
                            'id' => 241,
                            'retour_id' => 27,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 593,
                            'reponsable_type' => 'lecon',
                        ),
                        210 => 
                        array (
                            'id' => 242,
                            'retour_id' => 27,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 46,
                            'reponsable_type' => 'developpement',
                        ),
                        211 => 
                        array (
                            'id' => 244,
                            'retour_id' => 27,
                            'question_id' => 6,
                            'reponse_texte' => 'Jury neutre, questions de niveau moyen à facile.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        212 => 
                        array (
                            'id' => 245,
                            'retour_id' => 27,
                            'question_id' => 8,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        213 => 
                        array (
                            'id' => 246,
                            'retour_id' => 27,
                            'question_id' => 9,
                            'reponse_texte' => '16.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        214 => 
                        array (
                            'id' => 247,
                            'retour_id' => 28,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 613,
                            'reponsable_type' => 'lecon',
                        ),
                        215 => 
                        array (
                            'id' => 248,
                            'retour_id' => 28,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 603,
                            'reponsable_type' => 'lecon',
                        ),
                        216 => 
                        array (
                            'id' => 250,
                            'retour_id' => 28,
                            'question_id' => 14,
                            'reponse_texte' => 'Il n\'y a pas eu beaucoup d\'exercices, ils étaient moyens sans indication, faciles avec.

Exo 1 : Sauriez-vous montrer que $u_n=\\sum_{k=0}^n\\frac{1}{k!}$ converge en n\'utilisant aucun outil théorique sur les séries ?

Indication : Considérer $v_n=u_n+\\frac{1}{n(n!)}$

Exo 2 : On considère $(u_n)$ une suite réelle positive telle que : $\\forall n,p\\in N, u_{n+p}\\leq u_n+u_p$. Montrer que $\\frac{u_n}{n}$ converge

Indication : Montrer que ça converge vers $inf \\frac{u_n}{n}$',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        217 => 
                        array (
                            'id' => 251,
                            'retour_id' => 28,
                            'question_id' => 15,
                        'reponse_texte' => 'J\'ai eu pas mal de questions sur mon plan, beaucoup de petites questions qui me faisaient approfondir des items de mon plan. Un des membres du jury (Torossian crois-je) est beaucoup revenu sur le fait que j\'avais parlé de suites de v.a réelles dans mon plan.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        218 => 
                        array (
                            'id' => 252,
                            'retour_id' => 28,
                            'question_id' => 17,
                            'reponse_texte' => 'L\'oral était pas terrible, je me suis planté en faisant Stirling ce qui est loin d\'être glorieux. Sinon le jury était neutre, il ne semblait ni emballé ni lassé.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        219 => 
                        array (
                            'id' => 253,
                            'retour_id' => 28,
                            'question_id' => 18,
                            'reponse_texte' => '12.75',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        220 => 
                        array (
                            'id' => 254,
                            'retour_id' => 29,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 567,
                            'reponsable_type' => 'lecon',
                        ),
                        221 => 
                        array (
                            'id' => 255,
                            'retour_id' => 29,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 556,
                            'reponsable_type' => 'lecon',
                        ),
                        222 => 
                        array (
                            'id' => 256,
                            'retour_id' => 29,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 68,
                            'reponsable_type' => 'developpement',
                        ),
                        223 => 
                        array (
                            'id' => 257,
                            'retour_id' => 29,
                            'question_id' => 5,
                        'reponse_texte' => 'Question assez faciles. Le jury était assez sympathique (Il y avait Caldero dedans).

Prouver qu\'un extension finie de Q ne contient qu\' un nombre fini de racines de l\'unité 

Prouver X^2 +X +1 est irréductible dans F_2, puis F_32

Irréductibilité des poly cyclotomiques sur F_p',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        224 => 
                        array (
                            'id' => 258,
                            'retour_id' => 29,
                            'question_id' => 6,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        225 => 
                        array (
                            'id' => 259,
                            'retour_id' => 29,
                            'question_id' => 8,
                            'reponse_texte' => 'Relativement bien, même si je me suis un peu embrouillé sur le ien entre irréductibilité dans Q[X] et dans Z[X].',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        226 => 
                        array (
                            'id' => 260,
                            'retour_id' => 29,
                            'question_id' => 9,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        227 => 
                        array (
                            'id' => 261,
                            'retour_id' => 30,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 621,
                            'reponsable_type' => 'lecon',
                        ),
                        228 => 
                        array (
                            'id' => 262,
                            'retour_id' => 30,
                            'question_id' => 11,
                            'reponse_texte' => '',
                            'reponsable_id' => 628,
                            'reponsable_type' => 'lecon',
                        ),
                        229 => 
                        array (
                            'id' => 263,
                            'retour_id' => 30,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 134,
                            'reponsable_type' => 'developpement',
                        ),
                        230 => 
                        array (
                            'id' => 265,
                            'retour_id' => 30,
                            'question_id' => 15,
                        'reponse_texte' => 'Quelques indications quand je mongolisais, et jury qui aide notamment à ne pas écrire de choses fausses (ils m\'ont corrigé immédiatement quelques erreurs type problème de signe / objets qui disparaissent mystérieusement d\'une ligne sur l\'autre / ...)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        231 => 
                        array (
                            'id' => 266,
                            'retour_id' => 30,
                            'question_id' => 17,
                            'reponse_texte' => '- C\'est vraiment pas beaucoup trop heures fichtre ! J\'avais une bonne idée de plan, quelques bons exemples mais il m\'en manquait des basiques pour illustrer certaines notions et mon plan était un peu vide. 

- Je suis tombé sur JP Barani, un colleur de ma prépa (qui ne m\'a pas reconnu) qui me terrifiait. Il s\'est montré plutôt sympa, finalement. 

- Globalement, je m\'attendais un peu à la douche (plan OK mais maigre, développements pas trop en confiance) mais ça s\'est plutôt bien passé. Dommage pour les trop nombreuses erreurs de mongol.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        232 => 
                        array (
                            'id' => 267,
                            'retour_id' => 30,
                            'question_id' => 18,
                            'reponse_texte' => '15',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        233 => 
                        array (
                            'id' => 268,
                            'retour_id' => 31,
                            'question_id' => 19,
                            'reponse_texte' => '',
                            'reponsable_id' => 625,
                            'reponsable_type' => 'lecon',
                        ),
                        234 => 
                        array (
                            'id' => 269,
                            'retour_id' => 31,
                            'question_id' => 20,
                            'reponse_texte' => '',
                            'reponsable_id' => 565,
                            'reponsable_type' => 'lecon',
                        ),
                        235 => 
                        array (
                            'id' => 271,
                            'retour_id' => 31,
                            'question_id' => 23,
                            'reponse_texte' => 'Sur le plan puis quelques exos.
- prouver le critère d\'Abel implique celui de Cauchy

- si $f$ est DSE de rayon de convergence $R\\textgreater0$ et si $r\\textlesserR$, calculer $\\displaystyle \\int_{0}^{2\\pi} \\left|\\ f\\left(r\\ e^{i\\theta}\\right) \\right|^{2} \\, \\mathrm{d}\\theta$

Dans le cas où $R\\textgreater1$ et les coefficients du DSE sont entiers, montrer que $f$ est un polynôme

- si $\\Sigma\\ a_{n}z^{n}$ a un rayon de convergence non nul, que peut-on dire de celui de $\\Sigma\\ \\frac{a_{n}z^{n}}{n!}$ ?

- époque et motivation des séries entières ?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        236 => 
                        array (
                            'id' => 272,
                            'retour_id' => 31,
                            'question_id' => 24,
                            'reponse_texte' => 'Jury très agréable et souriant.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        237 => 
                        array (
                            'id' => 273,
                            'retour_id' => 31,
                            'question_id' => 26,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        238 => 
                        array (
                            'id' => 274,
                            'retour_id' => 31,
                            'question_id' => 27,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        239 => 
                        array (
                            'id' => 275,
                            'retour_id' => 32,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 573,
                            'reponsable_type' => 'lecon',
                        ),
                        240 => 
                        array (
                            'id' => 276,
                            'retour_id' => 32,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 592,
                            'reponsable_type' => 'lecon',
                        ),
                        241 => 
                        array (
                            'id' => 277,
                            'retour_id' => 32,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 6,
                            'reponsable_type' => 'developpement',
                        ),
                        242 => 
                        array (
                            'id' => 384,
                            'retour_id' => 46,
                            'question_id' => 5,
                        'reponse_texte' => 'Des exercices et des questions sur le plan : il y avait 5 résultats dans mon plan qui étaient des dl classiques ils m\'ont demandé de donner les idées de démos des 3 que je n\'ai pas proposés (lemme de morse, réduction des formes quadratiques sur un corps fini et ss-groupes compacts de Gln).

- Montrer que l\'ensemble des classes de congruence des matrices symétriques est fini ssi K*/K*^2 est fini 
- Montrer que l\'ensemble des formes quadratiques de signature p,q est un ouvert
- Est ce que vous connaissez un résultat général sur la réduction de formes quadratiques ? (ils voulaient le théorème de Witt que je ne connaissais que de nom)
- Montrer que O(q) est un groupe. Dans le cas réel, quand ce groupe est-il compact ?


',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        243 => 
                        array (
                            'id' => 279,
                            'retour_id' => 32,
                            'question_id' => 5,
                            'reponse_texte' => 'Quelques explications sur le développement, des questions sur le plan, des exos.

Dans l\'ordre. Sur le développement quelques explications qui n\'étaient pas clair pour le jury.

Sur le plan : j\'ai mis que le corps des nombres algébriques était un corps sans mettre d\'où le résultant apparaît. Ils m\'ont donc demander d\'expliquer. (Sol : $Res_T ( P(X- T) , Q(X) )$

Sur le théorème de Bézout : Est ce que vous connaissez une généralisation ? J\'ai répondu qu\'on peut généraliser au plan projectif à l\'aide de la théorie de la multiplicativité des points d\'intersection et qu\'il y avait égalité dans l\'inégalité du théorème faible de Bézout. Ça leur a suffit.

Application : trouver les points d\'intersection de $X^2 + YX + Y^2 - 1 = 0$ et $2X^2 + Y^2 - .. = 0$ un truc du genre. Sol : on fait le résultant en $Y$ on solve en $X$ on $x = 1$ ou $-1$ et on en déduit les points d\'intersection.

Sur le discriminant (qui est dans le plan) : connaissez vous un lien entre le discriminant d\'un polynôme de degré $3$. On montre que le signe du résultant permet de dire s\'il y a 3 racines réelles. Je n\'ai pas eu à faire les calculs en entier.

Sur le discriminant. Calculer le discriminant de $P(X) = X^n + aX + b$. Sol : par la formule avec le reste de la division de $P$ par $P\'$. Pareil, ils ne m\'ont pas demandé de finir les calculs.

Est ce que vous connaissez un critère sur  $P \\in \\mathbb{Q}(X)$ pour que son groupe de Galois soit dans $\\mathfrak{U}_n$ + des questions sur le groupe de Galois. Je n\'ai pas fini la question.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        244 => 
                        array (
                            'id' => 280,
                            'retour_id' => 32,
                            'question_id' => 6,
                            'reponse_texte' => 'Le jury était plutôt sympa mais surtout enclin à éviter TOUT calcul. En gros je leur disais juste comment il fallait faire.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        245 => 
                        array (
                            'id' => 281,
                            'retour_id' => 32,
                            'question_id' => 8,
                        'reponse_texte' => 'gros stress au début parce que je savais pas quoi choisir comme sujet. Puis gros stress parce qu\'une fois la leçon choisie il y a un sardanapale qui a pris LE Apéry pour faire la leçon 142 ! et il n\'y en avait pas d\'autre dans toutes les autres malles. Au final j\'ai sorti les définitions de résultant de tête et c\'est (peut-être) passé parce que je n\'ai pas eu de questions reloues à ce niveau là.

Du bruit durant la préparation.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        246 => 
                        array (
                            'id' => 282,
                            'retour_id' => 32,
                            'question_id' => 9,
                            'reponse_texte' => '17.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        247 => 
                        array (
                            'id' => 283,
                            'retour_id' => 33,
                            'question_id' => 28,
                            'reponse_texte' => '',
                            'reponsable_id' => 642,
                            'reponsable_type' => 'lecon',
                        ),
                        248 => 
                        array (
                            'id' => 284,
                            'retour_id' => 33,
                            'question_id' => 29,
                            'reponse_texte' => '',
                            'reponsable_id' => 647,
                            'reponsable_type' => 'lecon',
                        ),
                        249 => 
                        array (
                            'id' => 285,
                            'retour_id' => 33,
                            'question_id' => 32,
                            'reponse_texte' => 'Uniquement des questions sur le plan et le développement, pas d\'exercice.

- Preuve de la complexité pour le calcul des bords ? Comment se sert-on des bords ? Sur un exemple ?

- Des questions sur comment (et pourquoi) le développement fonctionne

- D\'autres codes que Huffman ? Comment on chiffre et déchiffre avec Huffman ?

- KMP, quelle autre structure (automates) ?',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        250 => 
                        array (
                            'id' => 286,
                            'retour_id' => 33,
                            'question_id' => 35,
                        'reponse_texte' => 'Jury sur l\'offensive mais pas méchant non plus. Il m\'a coincé sur deux ou trois choses. Membres : Claude Marché et Judicaël Courant. Malheureusement je n\'ai pas retenu le nom de celui qui parlait le plus (90%)',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        251 => 
                        array (
                            'id' => 287,
                            'retour_id' => 33,
                            'question_id' => 34,
                        'reponse_texte' => 'Pas vraiment de surprises, juste une petite déception sur l\'absence de questions sur les lemmes admis dans le développement (qui sont en fait d\'autres développements).',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        252 => 
                        array (
                            'id' => 288,
                            'retour_id' => 33,
                            'question_id' => 36,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        253 => 
                        array (
                            'id' => 289,
                            'retour_id' => 34,
                            'question_id' => 10,
                            'reponse_texte' => '',
                            'reponsable_id' => 599,
                            'reponsable_type' => 'lecon',
                        ),
                        254 => 
                        array (
                            'id' => 290,
                            'retour_id' => 34,
                            'question_id' => 12,
                            'reponse_texte' => '',
                            'reponsable_id' => 127,
                            'reponsable_type' => 'developpement',
                        ),
                        255 => 
                        array (
                            'id' => 291,
                            'retour_id' => 34,
                            'question_id' => 14,
                            'reponse_texte' => 'Deux questions sur le développement, puis principalement des exos.

Pourquoi la fonction \\[ t\\mapsto \\left\\{\\begin{array}{rl}
\\frac{f(t)-f(s)}{t-s} \\esperluette \\mbox{si $s\\neq t$ } \\\\
f\'(t) \\esperluette \\mbox{si $s=t$}\\end{array}\\right. \\] est-elle continue (je n\'avais justifié que la continuité en $t$) ?

A-t-on vraiment \\[ D=\\bigcup_{n\\in \\mathbb{N}} F_n \\] ($D$ désignant l\'ensemble des fonctions de $[0,1]$ dans $\\mathbb{R}$ dérivables en au moins un point et \\[F_n=\\left\\lbrace f\\in \\mathscr{C}^0([0,1],\\mathbb{R} ), \\exists t\\in [0,1], \\forall s\\in [0,1], |f(t)-f(s)|\\leqslant n|t-s| \\right\\rbrace \\] ayant vocation a être un fermé d\'intérieur vide) ?
Non, on a seulement une inclusion, mais comme on montre que le membre de droite est d\'intérieur vide, celui de gauche l\'est aussi, et c\'est ce qu\'on cherche.

Un exemple d\'espace muni de deux distances dont l\'une est complète et pas l\'autre ?
$ \\left] -\\frac{\\pi}{2}, \\frac{\\pi}{2} \\right[ $ n\'est pas complet pour la distance induite par la valeur absolue de $\\mathbb{R}$, mais l\'est pour la distance définie par $d(x,y)=|\\tan(x)-\\tan(y)|$.

Un exemple d\'espace complet non normé ?
Un espace métrique complet qui n\'est pas un espace vectoriel, par exemple celui ci-dessus.

Comment construit-on $\\mathbb{R}$ et cela se généralise-t-il ?
En quotient l\'ensemble des suites de Cauchy de $\\mathbb{Q}$ par la relation d\'équivalence identifiant deux suites si leur différence tend vers $0$, en faisant bien attention à remplacer les $\\varepsilon$ par des $\\frac{1}{k}$ dans la définition de la convergence puisque les $\\varepsilon$ réels n\'existent pas encore. C\'est une des façons de compléter n\'importe quel espace métrique, sauf que mainteant les $\\varepsilon$ réels existent, donc c\'est moins subtil.

Y a-t-il une métrique qui rende $\\mathbb{Q}$ complet ?
Si on demande qu\'elle induise la topologie usuelle, non par théorème de Baire. Sinon, ...

Comment prouve-t-on le théorème de Cauchy-Lipschitz ?
Une solution au problème de Cauchy est un point fixe de l\'application \\[ \\varphi \\longmapsto \\left( t\\mapsto x_0+ \\int_{t_0}^t f(u,\\varphi(u)) \\mbox{d}u \\right) \\mbox{.}\\] Cette application possède une itérée contractante. Le reste est technique et ne relève pas du théorème du point fixe.

Le disque unité ouvert de $\\mathbb{C}$ muni de la topologie de la convergence uniforme sur tout compact (qui est métrisable via une exhaustion compacte) est-il complet ? Pourquoi ?
Oui, par théorème de Weierstrass, qui se prouve en utilisant la formule de Cauchy.

Soient $E$ l\'espace $\\mathscr{C}^1([0,1],\\mathbb{R})$ muni de la norme $\\Vert f\\Vert _E=\\Vert f\\Vert _\\infty +\\Vert f\'\\Vert _\\infty $ et $F$ l\'espace $\\mathscr{C}^0([0,1],\\mathbb{R})$ muni de la norme $\\Vert f\\Vert _F=\\Vert f\\Vert _\\infty $. On note $\\Phi$ l\'opérateur de dérivation de $E$ dans $F$. Montrer que $\\Phi(B_E(0,1))$ est d\'intérieur non vide.
$\\Phi$ est linéaire et $\\Vert \\Phi f\\Vert _F\\leqslant \\Vert f\\Vert _E$, donc $\\Phi$ est continue. Le résultat suit donc du théorème de l\'application ouverte.

Mouais, je veux bien que $F$ soit complet, c\'est dans votre plan. Mais pourquoi $E$ l\'est-il ?
Si $(f_n)_{n\\in\\mathbb{N}}$ est de Cauchy dans $E$, $(f_n\')_{n\\in \\mathbb{N}}$ l\'est aussi dans $F$ car $\\Vert f\' \\Vert _F \\leqslant \\Vert f\\Vert _E$ si $f\\in E$. Donc $(f_n\')_{n\\in \\mathbb{N}}$ converge uniformément, le reste est classique.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        256 => 
                        array (
                            'id' => 292,
                            'retour_id' => 34,
                            'question_id' => 15,
                            'reponse_texte' => 'Jury neutre, un des membres a clairement l\'air de s\'ennuyer. Questions de niveau moyen.',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        257 => 
                        array (
                            'id' => 293,
                            'retour_id' => 34,
                            'question_id' => 17,
                            'reponse_texte' => '',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        258 => 
                        array (
                            'id' => 294,
                            'retour_id' => 34,
                            'question_id' => 18,
                            'reponse_texte' => '13.25',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        259 => 
                        array (
                            'id' => 295,
                            'retour_id' => 35,
                            'question_id' => 1,
                            'reponse_texte' => '',
                            'reponsable_id' => 582,
                            'reponsable_type' => 'lecon',
                        ),
                        260 => 
                        array (
                            'id' => 296,
                            'retour_id' => 35,
                            'question_id' => 2,
                            'reponse_texte' => '',
                            'reponsable_id' => 569,
                            'reponsable_type' => 'lecon',
                        ),
                        261 => 
                        array (
                            'id' => 297,
                            'retour_id' => 35,
                            'question_id' => 3,
                            'reponse_texte' => '',
                            'reponsable_id' => 2,
                            'reponsable_type' => 'developpement',
                        ),
                        262 => 
                        array (
                            'id' => 299,
                            'retour_id' => 35,
                            'question_id' => 5,
                            'reponse_texte' => 'Quasiment que des questions sur le développement et le plan.

sur le développement, que je fais à partir de la décomposition de Dunford,

Pourquoi l\'exponentielle est un polynôme en la matrice ? Bah K[A] est fermé :3 

Ouais mais pourquoi c\'est fermé ? Donc bon c\'est un EV de dimension finie

Pourquoi l\'inverse est un polynôme en la matrice ? Bah on utilise un polynôme annulateur, il a voulu que je lui fasse le calcul de 2 lignes au tableau

Quel est le lien entre les valeurs propres de la matrice et celle de ça décomposition de Dunford ? En fait on cotrigonalise les deux matrices et vu que la nilpotente a des zéros sur la diagonale on conclut.



Sur le plan, comment on montre le lemme des noyaux, pourquoi votre deuxième développement est dans le sujet, c\'est vrai qu\'au début c\'est pas immédiat mais toute la démo repose sur une trigonalisation et un calcul que tu fait de la matrice trigonalisée, en quoi tr f^k =0 pour tout k est une caractérisation des matrices nilpotentes, démo que j\'ai lu dans le FGN juste avant de passer, et divers autres trucs',
                            'reponsable_id' => 0,
                            'reponsable_type' => '',
                        ),
                        263 => 
                        array (
                            'id' => 300,
                            'retour_id' => 35,
                            'question_id' => 6,
                        'reponse_texte' => 'Le jury était très sympa, question moyenne assez classiques sur TOUS les points du développement, du coup assez content parce que j\'étais au taquet =) Une question à la fin, un peu plus dur et des indications très bizarres \'quel est votre raisonnement mathématique préféré ?\' qui me fait me poser la terrible question, toi membre du jury es tu constructiviste ? En l\'occurrence il ne l\'était pas',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    264 => 
                    array (
                        'id' => 301,
                        'retour_id' => 35,
                        'question_id' => 8,
                        'reponse_texte' => 'ça s\'est plutôt bien passé, j\'ai répondu à quasiment toutes les questions, ils avaient l\'air contents. A la fin, l\'un m\'a demandé si je connaissais une caractérisation géométrique de la trigonalisabilité, j\'ai pas trouvé, il m\'a parlé de drapeau.

Le tableau était tout petit, et j\'avais des craies, clairement heureusement que je suis passé sur un développement court.

J\'ai dépassé sur la défense de plan, ils m\'ont arrêté à la seconde près sans me prévenir, mais vu qu\'il me restait juste à présenter mon 2e développement, il m\'a demandé de finir.

Et une remarque qui m\'a laissé perplexe \'Vous dîtes qu\'une matrice est nilpotente ssi tr f^k =0 pour tout k, mis c\'est faux, regardez pour In ça ne marche pas, beaucoup de candidats font l\'erreur\', je crois qu\'il a vite compris que c\'était n\'imp\' et qu\'il devait confondre avec une erreur classique, parce que In c\'est pas vraiment nilpotent, ou alors j\'ai vraiment rien compris ...',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    265 => 
                    array (
                        'id' => 302,
                        'retour_id' => 35,
                        'question_id' => 9,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    266 => 
                    array (
                        'id' => 303,
                        'retour_id' => 36,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 632,
                        'reponsable_type' => 'lecon',
                    ),
                    267 => 
                    array (
                        'id' => 304,
                        'retour_id' => 36,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 616,
                        'reponsable_type' => 'lecon',
                    ),
                    268 => 
                    array (
                        'id' => 305,
                        'retour_id' => 36,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 134,
                        'reponsable_type' => 'developpement',
                    ),
                    269 => 
                    array (
                        'id' => 306,
                        'retour_id' => 36,
                        'question_id' => 14,
                        'reponse_texte' => 'Une seule question sur le développement. Pas de questions sur le plan. Que des exos.

Q : Et comment on fait avec $y\'\' - y = T$ avec $T \\in S\'$ ?
R : on fait comme dans ce qui est proposé dans plan, c\'est-à-dire on convole avec la solution fondamentale.

Q : Calculer $\\widehat{H}$. (avec de l\'aide en particulier pour introduire $vp(1/x)$ qui n\'est pas dans le plan).
R : $ix \\widehat{H} = 1$ ok. $vp$ est l\'inverse de $x$ mais c\'est pas tout il y a aussi $\\delta$ qui convient et là j\'ai bloqué et on a changé.

Q [le prof de prépa se réveille] : exo sur la fonction nulle part dérivable qui fait intervenir une transformée de Fourier d\'une fonction. 
R : Je suis son raisonnement et on fini l\'exo sans faire tous les calculs et à chaque il me font grâce des vérifications du genre \'permutation intégrale série\'.

Q : et si vous deviez enseigner les distributions à une classe de 5ème ?
R : ahah non c\'est pas vrai cette question  mais j\'aurais bien aimé l\'avoir

Q : Si $T \\in E\'$ [NB : je l\'ai défini dans mon plan], alors $\\widehat{T}$ est $C^\\infty$ et  $T$ est sous polynomiale
R : oulah calm down cowboy ! pas eu le temps de finir et grosse galère ...
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    270 => 
                    array (
                        'id' => 307,
                        'retour_id' => 36,
                        'question_id' => 15,
                        'reponse_texte' => 'Le jury m\'aidait beaucoup pour que ça avance. Je n\'ai jamais eu le temps de bloquer plus que 30 secondes. Le jury passait soit à un autre exo soit me donnait un coup de pouce',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    271 => 
                    array (
                        'id' => 308,
                        'retour_id' => 36,
                        'question_id' => 17,
                        'reponse_texte' => 'J\'ai essayé de tendre le maximum de perches possible dans mon plan ou plutôt de préparer les questions sur lesquels ils allez me coincer dans le but d\'avoir des questions  faciles au début. Du genre :
- une fonction dans $D$ pas dans $S$, ou des trucs du genre ?
- comment on démontre que $S$ est complet ?
- $S$ est stable par Fourier ok. $D$ ?
- la topologie sur $D$ ?

Mais non rien de cela. Pas de questions de topologie difficiles. J\'avais juste introduit quelques notions de distributions + convolution.
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    272 => 
                    array (
                        'id' => 309,
                        'retour_id' => 36,
                        'question_id' => 18,
                        'reponse_texte' => '18.25',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    273 => 
                    array (
                        'id' => 310,
                        'retour_id' => 37,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 582,
                        'reponsable_type' => 'lecon',
                    ),
                    274 => 
                    array (
                        'id' => 311,
                        'retour_id' => 37,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 568,
                        'reponsable_type' => 'lecon',
                    ),
                    275 => 
                    array (
                        'id' => 312,
                        'retour_id' => 37,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 104,
                        'reponsable_type' => 'developpement',
                    ),
                    276 => 
                    array (
                        'id' => 314,
                        'retour_id' => 37,
                        'question_id' => 5,
                        'reponse_texte' => 'Petites questions sur quelques points du développement où j\'étais allé un peu vite. Quelques questions sur le plan, puis deux exos.

- Après Householder : comment on choisit $M$ et $N$ en pratique ? (j\'avais mis la décomposition $D-E-F$ dans le plan mais pas dit grand-chose dessus) On veut quoi comme bonnes propriétés sur $M$ ? Comment on fait pour savoir si effectivement la méthode va converger ? (ben, on ne regarde pas $\\rho (M^{-1}N)$ qui est horrible à calculer, donc on calcule juste des itérations de $x_n = M^{-1}(Nx_{n-1} +b)$ et on regarde si ça a l\'air de converger... là, je me suis dit que mon développement, il était vraiment très très très utile, mvoyez).

- D\'après le plan on peut cotrigonaliser $f$ et $g$ s\'ils commutent (et sont trigonalisables), est-ce qu\'on a une réciproque ? 

- $f$ trigo $\\Leftrightarrow$ $\\chi_f$ scindé ; et pour le polynôme annulateur ? Comment on prouve le cas $\\chi_f$, comment on modifie la preuve pour $\\mu_f$ ?

- Exo 1 : on prend (sic) la matrice $A$ suivante : 
$$
\\begin{matrix}
1 \\esperluette 1 \\esperluette -1 \\\\
0 \\esperluette 4 \\esperluette 1 \\\\
0 \\esperluette 0 \\esperluette 9
\\end{matrix}
$$
Calculer ses racines carrées. 
Bon, ben la matrice est clairement diagonalisable, on voit bien à quoi ses racines ressemblent. On prend $R$ une racine, elle commute à $A$ puisque $A=R^2$ est un polynôme en $R$, et on travaille là-dessus pour montrer que $R$ est forcément de la bonne forme ($R$ est encore trigonale, puisqu\'elle conserve les sous-espaces propres de $A$). J\'ai perdu beaucoup de temps sur cet exo bidon, c\'était pas beau à voir.  

- Exo 2 : on se donne $A$, $B$ telles que $A+\\lambda B$ soit nilpotente en au moins $n+1$ valeurs distinctes de $\\lambda$. Montrer que $A$ et $B$ sont nilpotentes.
On regarde le polynôme caractéristique de $\\chi_{A+\\lambda B}$ ; à part pour le degré $n$, les coefs sont des polynômes en $\\lambda$ qui ont trop de racines pour leur degré donc sont nulles. Après, il reste à évaluer en $0$ pour avoir la nilpotence de $A$, puis en factorisant $\\lambda$ on récupère $B$ nilpotente. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    277 => 
                    array (
                        'id' => 315,
                        'retour_id' => 37,
                        'question_id' => 6,
                        'reponse_texte' => 'Pas bien dur, des trucs d\'AL de petit taupin sur lesquels j\'étais pas complètement au point. On m\'a, là encore, pas mal aidé dès que je bloquais. Les trois membres du jury sont intervenus et étaient sympa.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    278 => 
                    array (
                        'id' => 316,
                        'retour_id' => 37,
                        'question_id' => 8,
                        'reponse_texte' => 'Les questions sur les méthodes itératives étaient beaucoup plus sympa que ce que je pensais, j\'y connais pas grand-chose au fond mais j\'avais de quoi répondre. Sinon, pas de surprise, pas trop de joie non plus parce que c\'était pas ouf.

Note : j\'avais Risler dans mon jury, c\'est dans son livre qu\'il y a l\'algorithme de Dunford effectif (que j\'avais détaillé dans le plan), j\'espère qu\'il a kiffé. 
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    279 => 
                    array (
                        'id' => 317,
                        'retour_id' => 37,
                        'question_id' => 9,
                        'reponse_texte' => '11.75',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    280 => 
                    array (
                        'id' => 318,
                        'retour_id' => 38,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 627,
                        'reponsable_type' => 'lecon',
                    ),
                    281 => 
                    array (
                        'id' => 319,
                        'retour_id' => 38,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 609,
                        'reponsable_type' => 'lecon',
                    ),
                    282 => 
                    array (
                        'id' => 320,
                        'retour_id' => 38,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 21,
                        'reponsable_type' => 'developpement',
                    ),
                    283 => 
                    array (
                        'id' => 362,
                        'retour_id' => 43,
                        'question_id' => 14,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    284 => 
                    array (
                        'id' => 322,
                        'retour_id' => 38,
                        'question_id' => 14,
                        'reponse_texte' => 'Quelques questions sur le développement, une ou deux sur le plan et surtout des exos.

ils ont voulu que j\'explique plus lentement le passage \'il existe epsilon tel que D_epsilon est inclus dans phi^-1(omega)\' pour ceux qui connaissent le développement
Vous avez dit que la différentielle étaitune similitude drecte, que vouliez vous dire ? Alors bon j\'explique tout un tas de trucs, en fait il voulait seulement que je dise que ça conserve les angles.
pourquoi une série entière de rayon de convergence fini a au moins un point singulier ?
Alors par l\'absurde, si tous les points sont réguliers t\'as autour de chaque point du cercle de convergence un ouvert où ta fonction se prolonge en une fonction holomorphe, t\'en extrait un sous recouvrement fini par compacité du cercle et ensuite gros dilemme parce que tu peux étendre ta série en une fonction holomorphe sur un disque plus grand mais est ce que ça veut dire que ta série entière a un rayon de convergence plus grand que prévu et bah ouais, suffit de regarder la formule de Cauchy sur un cercle plus grand où ta fonction est holomorphe et ... fin regarde la preuve de \'une fonction holomorphe est analytique c\'est la même idée\'
Que sont les biholomorphismes du plan ? Les fonctions affines. On suppose le biholomorphisme nul en 0, on regarde l\'inverse, on montre que 0 est un pôle de multiplicité 1 de l\'inverse car la dérivée en 0 de f n\'est pas nul, il faut pour le voir regarder la dérivée de la composée de la fonction avec sont inverse, bref on fait mumuse. on retire à l\'inverse la partie en 1/z, c\'est une fonction holomorphe, on montre qu\'elle est bornée, on utilise le théorème de Liouville et du coup elle est constante et même nulle et on finit par montrer que le biholomorphisme est une fonction affine et fichtre quand t\'as jamais vu ça de ta vie, t\'es content d\'en avoir fini. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    285 => 
                    array (
                        'id' => 323,
                        'retour_id' => 38,
                        'question_id' => 15,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    286 => 
                    array (
                        'id' => 324,
                        'retour_id' => 38,
                        'question_id' => 17,
                        'reponse_texte' => 'Plutôt bien passé je crois, une leçon pas évidente, j\'ai répondu aux questions et leurs questions remuaient beaucoup de notions de mon plan et étaient loin d\'être triviales. Ils étaient assez neutres, j\'ai un peu dépassé le temps imparti à la défense de plan mais ils n\'ont rien dit. J\'avais quelques craintes sur mon plan, basé sur l\'Amar Matheron qui démontre la formule de Cauchy en mode je suis un psychopathe, j\'aime les 1-formes, mais ils ne m\'en ont pas parlé, ils m\'ont posé quelques questions sur d\'autres points du plan, mais pas sur la partie trash.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    287 => 
                    array (
                        'id' => 325,
                        'retour_id' => 38,
                        'question_id' => 18,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    288 => 
                    array (
                        'id' => 326,
                        'retour_id' => 39,
                        'question_id' => 28,
                        'reponse_texte' => '',
                        'reponsable_id' => 654,
                        'reponsable_type' => 'lecon',
                    ),
                    289 => 
                    array (
                        'id' => 327,
                        'retour_id' => 39,
                        'question_id' => 29,
                        'reponse_texte' => '',
                        'reponsable_id' => 646,
                        'reponsable_type' => 'lecon',
                    ),
                    290 => 
                    array (
                        'id' => 328,
                        'retour_id' => 39,
                        'question_id' => 30,
                        'reponse_texte' => '',
                        'reponsable_id' => 193,
                        'reponsable_type' => 'developpement',
                    ),
                    291 => 
                    array (
                        'id' => 640,
                        'retour_id' => 66,
                        'question_id' => 76,
                        'reponse_texte' => '',
                        'reponsable_id' => 195,
                        'reponsable_type' => 'reference',
                    ),
                    292 => 
                    array (
                        'id' => 639,
                        'retour_id' => 66,
                        'question_id' => 76,
                        'reponse_texte' => '',
                        'reponsable_id' => 180,
                        'reponsable_type' => 'reference',
                    ),
                    293 => 
                    array (
                        'id' => 331,
                        'retour_id' => 39,
                        'question_id' => 32,
                        'reponse_texte' => 'Pas trop d\'exercices en info, donc surtout des questions sur des détails du plan ou des notions générales.
Le niveau était complètement naze, la plupart des questions étaient un niveau basique L3, et pourtant ça avait l\'air compliqué pour le jury... Un des 3 membres était un vieux grincheux qui comprenait pas grand chose et qui était du coup pas très sympathique...
Apparemment, ils ont pas digéré le fait que je parle pas des arbre rouges noirs et que je mette les arbres splay à la place, parce que pour eux c\'est beaucoup plus compliqué les arbres splays que les ARN, donc les questions visaient à me faire dire que c\'était de la merde mon développement...

Des détails sur mon développement :
- sur les différents cas que vous avez dessiné (zig, zig-zag, zig-zig), ça recouvre pas tout, c\'est beaucoup plus compliqué, non ? Les autres cas sont symétriques, tocard.
- comment ça se code splay(x,S) ? Du coup je montre comment ça s\'écrit de manière récursive, que c\'est pas trop compliqué à faire mais apparemment le but de la question était de montrer que "c\'est quand même trop compliqué à coder !" =\\textgreater WTF ?!!
- c\'est quoi les crédits dans la preuve ? J\'essaye de lui expliquer ce que c\'est l\'analyse amortie, mais apparemment il a pas l\'air de connaitre. J\'en vient même à lui citer l\'exemple du tableau dynamique pour essayer de lui faire comprendre, et à ce moment là un des autres membres du jury lui dit à l\'oreille un truc du style : "Oui oui, ça je connais, c\'est vrai ce qu\'il dit" -\\textgreater WTF ?!!!! Y\'a une leçon entière qui s\'appelle analyse de complexité, le jury est sensé s\'y connaitre, merde !!!
- Non mais je comprends pas, c\'est quoi un crédit ? Bah on peut voir ça comme des opérations élémentaires qu\'on a payé en avance pour analyser la complexité de manière plus fine...
- (Un autre membre) Et c\'est équilibré ces arbres ? Bah en pire cas non, mais si on les utilise sur un ensemble de données, ça tends à donner le meilleur arbre (style Huffman) pour ce set de données, et puis en moyenne ça se rééquilibre tout seul.
- Qu\'est ce qui se passe si on insère 1,2,3, ... ? Oui ca créé un peigne donc c\'est pas équilibré, mais il suffit d\'appeler une fois find(1,S) pour que l\'arbre se rééquilibre tout bien.
- Mais vous connaissez pas des arbres qui sont équilibrés tout le temps ? Bah les arbres rouges noirs, mais c\'est relou à utiliser. Je donne quand même les différentes contraintes que doit vérifier un arbre rouge noir, et j\'explique que c\'est ça qui est casse-pied à maintenir.

Questions sur le plan :
- (Retour au grincheux) Vous dites que la profondeur moyenne est O(log n), qu\'est ce que ça veut dire ? J\'explique que c\'est l\'ordre qui est pris de manière aléatoire, et comment on insère les éléments.
- Vous pouvez nous faire la preuve ? C\'est un développement en entier c**nnard ! Je commence à faire la preuve, en montrant que les racines sont équiprobables, puis qu\'à une racine fixée, les arbres gauches et droits sont uniquement déterminés par l\'ordre des éléments plus petits (resp plus grands) que la racine, et donc on aboutit à une inégalité de récurrence, et j\'ai pas le temps de la résoudre.
- Vous dites qu\'on peut supprimer dans une liste triée en O(1), comment on fait ça ? Bah on change les pointeurs de celui d\'avant et de celui d\'après.
- Mais c\'est quoi vos listes ?!! Bah des listes doublement chainées (je l\'avais écris dans le plan...)
- Et comment vous faites pour trouver le maximum d\'une liste triée en O(1) ?!! Mais c\'est une liste doublement chainée, boulet ! J\'ai un accès sur le début et la fin de la liste !
- Et puis, dans le cas d\'une liste normale, comment vous faites pour supprimer en O(1) ?! Toujours pareil, c\'est doublement chainée ! Là je commence à comprendre qu\'il a un blocage sur les listes, et qu\'il a pas l\'air très à l\'aise avec la notion de liste doublement chainée, donc je développe : bah j\'ai mis les complexités que dans le cas des listes doublement chainées, parce que elles ont une meilleure complexité que les listes pour toutes les requêtes, donc les listes simplement chainées n\'ont aucun intérêt algorithmique dans cette leçon.

Question pédagogique :
- Comment vous introduirez cette leçon à des lycéens ? Je leur parlerai de Google. Détaillez. Bah il faut indexer donc ça ressemble un peu à ma partie sur l\'indexation. Puis je me corrige en disant que l\'algo de Google est trop compliqué et que du coup je parlerai plutôt de la fonction recherche dans un document texte, qui utilise fortement l\'indexation.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    294 => 
                    array (
                        'id' => 332,
                        'retour_id' => 39,
                        'question_id' => 35,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    295 => 
                    array (
                        'id' => 333,
                        'retour_id' => 39,
                        'question_id' => 34,
                        'reponse_texte' => 'J\'ai volontairement pas choisi Machines de Turing parce que j\'avais peur de faire des trucs trop théoriques et trop compliqués, donc je me suis dis qu\'en prenant de l\'algo, ça devrait passer. Et beh non ! Jury avec un niveau de connaissance très très décevant, c\'était vraiment triste...
J\'ai pas du tout eu de question sur la partie d\'algorithmique du texte (1/3 de mon plan), et je soupçonne que c\'est parce que le jury n\'était pas non plus à l\'aise sur ces notions...

Du coup, au lieu de discuter sur toutes les pistes de notions un peu plus complexes que je proposais dans le plan, le jury a essayé de ramener les questions à un niveau prépa qu\'il maitrisait mieux, enfin du moins en apparence...

[Oui je suis un gros rageux, ils sont peut être pas si mauvais que ça en vrai...]
EDIT: Vu la note reçu, c\'était bien un gros rageux....',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    296 => 
                    array (
                        'id' => 334,
                        'retour_id' => 39,
                        'question_id' => 36,
                        'reponse_texte' => '5.25',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    297 => 
                    array (
                        'id' => 335,
                        'retour_id' => 40,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 597,
                        'reponsable_type' => 'lecon',
                    ),
                    298 => 
                    array (
                        'id' => 336,
                        'retour_id' => 40,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 606,
                        'reponsable_type' => 'lecon',
                    ),
                    299 => 
                    array (
                        'id' => 337,
                        'retour_id' => 40,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 115,
                        'reponsable_type' => 'developpement',
                    ),
                    300 => 
                    array (
                        'id' => 339,
                        'retour_id' => 40,
                        'question_id' => 14,
                        'reponse_texte' => 'Aucune question sur le plan, ni sur la preuve de Brouwer. Ils m\'ont demandé comment le généraliser à la dimension infinie.
Rien sur mon problème de Dirichlet préparé avec tant d\'amour...

Premier exo : peut on trouver une submersion du tore sur R? (une indicationa  facilité la tache)
Deuxieme exo : opérateurs compacts, montrer la compacité de l opérateur intégration partant de $C^0$ allant dans le bon espace, calculer son spectre
Troisieme exo  :X ferme borne d un banach tel que pour tout $\\epsilon$ il existe un sev de dimension finie tel que d(X, Feps)\\textlesser $\\epsilon$ montrer que X est compact
j ai galéré et on m a interrompu, hésité sur le processus de diagonalisation qu il proposait (plus grosse erreur de ma part a mon avis) et on a arrete...

Quatrième exo : f continue sur un compact avec d(f(x),f(y))\\textlesserd(x,y), montrer qu on a un unique pt fixe
une indication et pof...

Cinquième exo : montrer que  la décomposition polaire est un homéo, interrompu avant la fin... Je commencais à fatiguer.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    301 => 
                    array (
                        'id' => 340,
                        'retour_id' => 40,
                        'question_id' => 15,
                        'reponse_texte' => 'Les questions étaient d un niveau correct, le jury était intéressé, aidait, pas cassant pour un sou, et semblait mieux supporter les 40 degrés que moi.

Juste une n\'a pas vu que j\'avais parlé des opérateurs compacts (alors que c était dans le plan et ma défense).',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    302 => 
                    array (
                        'id' => 341,
                        'retour_id' => 40,
                        'question_id' => 17,
                        'reponse_texte' => 'Je suis content de la leçon et du développement, le jury était bien... 
Seule surprise : un tableau noir avec un bord pliant, et une prise derrière qui empechait de bien le caler... Du coup il bougeait quand j écrivais... Déjà que j\'ai une écriture médicale si on en rajoute...
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    303 => 
                    array (
                        'id' => 342,
                        'retour_id' => 40,
                        'question_id' => 18,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    304 => 
                    array (
                        'id' => 343,
                        'retour_id' => 41,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 609,
                        'reponsable_type' => 'lecon',
                    ),
                    305 => 
                    array (
                        'id' => 344,
                        'retour_id' => 41,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 601,
                        'reponsable_type' => 'lecon',
                    ),
                    306 => 
                    array (
                        'id' => 345,
                        'retour_id' => 41,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 120,
                        'reponsable_type' => 'developpement',
                    ),
                    307 => 
                    array (
                        'id' => 347,
                        'retour_id' => 41,
                        'question_id' => 14,
                        'reponse_texte' => 'Plutôt équilibré, il y avait des coquilles dans le plan du coup ils sont revenu dessus.
Q : comment je montre D\'Alembert-Gauss (qui était dans le plan)
R : par l\'absurde, le polynome atteint un min non nul et DL en ce min

Q : Extrema de "somme des i*x_i" sur la sphère. 
R : Extrema liés, bla bla ...(en fait il y en a pas besoin, la fonction est une forme linéaire qu\'on peut donc voir comme un produit scalaire et c\'est torché mais dans le cadre de la leçon c\'était ce qu\'ils attendaient)

Q : f holomorphe sur C ne s\'annulant pas sur le disque unité fermé et qui stabilise le cercle unité, que peut on en dire ?
R : Elle est constante en appliquant le principe du max à f et 1/f sur le disque unité (petite subtilité ici pour 1/f puisque il faut être défini sur un voisinage du disque fermé)

Q : Connaissez-vous des problèmes d\'extrema sur des espaces de fonction ?
R : Lax-Milgram (dans le plan) et application a une fonctionnelle obtenue à partir d\'un opérateur différentiel, le min est alors solution

Q : Un truc plus élémentaire ?
R : Des problèmes en rapport avec les courbes

Q : Par exemple, le plus simple ?
R : Le plus court chemin reliant 2 pts

Q : Comment vous faite ?
R : On prend A et B ...[début de formalisme coupé par ce que l\'oral allait se terminer, du coup heuristique]... je parle d\'intégrer le produit scalaire de la dérivée du chemin et d\'un vecteur unitaire colinéaire à AB, le boss accepte.

(je n\'avais pas parlé de ce genre de problèmes dans le plan, je pense du coup que c\'est attendu ou au moins le mentionner dans la défense)

Là les deux autres couillons me demandent de refaire l\'exo d\'holomorphie parce qu\'ils ne sont toujours pas convaincu (alors qu\'ils m\'ont fortement incité à considérer 1/f donc a priori ils ont la même solution que moi) ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    308 => 
                    array (
                        'id' => 348,
                        'retour_id' => 41,
                        'question_id' => 15,
                    'reponse_texte' => 'Questions faciles. En ce qui concerne le jury : un sympa, un raleur (les coquilles l\'ont énervé), un neutre (en mode big boss qui posait de vrais questions)',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    309 => 
                    array (
                        'id' => 349,
                        'retour_id' => 41,
                        'question_id' => 17,
                    'reponse_texte' => 'Surpris par le type qui a demandé a ses collègues "je suis pas entrain de me faire arnaqué là ?" après avoir entendu ma réponse à son exo (tjrs l\'holomorphie). J\'en conclut qu\'il ne faut pas se mettre trop de pression sur notre niveau, mais simplement ne pas faire d\'erreurs bêtes -_-\' (On le savait déjà, certes)',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    310 => 
                    array (
                        'id' => 350,
                        'retour_id' => 41,
                        'question_id' => 18,
                        'reponse_texte' => '13.75',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    311 => 
                    array (
                        'id' => 351,
                        'retour_id' => 42,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 594,
                        'reponsable_type' => 'lecon',
                    ),
                    312 => 
                    array (
                        'id' => 352,
                        'retour_id' => 42,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 586,
                        'reponsable_type' => 'lecon',
                    ),
                    313 => 
                    array (
                        'id' => 353,
                        'retour_id' => 42,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 128,
                        'reponsable_type' => 'developpement',
                    ),
                    314 => 
                    array (
                        'id' => 355,
                        'retour_id' => 42,
                        'question_id' => 5,
                        'reponse_texte' => 'exos sans lien direct

justifier des formules de bases',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    315 => 
                    array (
                        'id' => 356,
                        'retour_id' => 42,
                        'question_id' => 6,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    316 => 
                    array (
                        'id' => 357,
                        'retour_id' => 42,
                        'question_id' => 8,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    317 => 
                    array (
                        'id' => 358,
                        'retour_id' => 42,
                        'question_id' => 9,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    318 => 
                    array (
                        'id' => 359,
                        'retour_id' => 43,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 630,
                        'reponsable_type' => 'lecon',
                    ),
                    319 => 
                    array (
                        'id' => 360,
                        'retour_id' => 43,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 596,
                        'reponsable_type' => 'lecon',
                    ),
                    320 => 
                    array (
                        'id' => 361,
                        'retour_id' => 43,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 83,
                        'reponsable_type' => 'developpement',
                    ),
                    321 => 
                    array (
                        'id' => 363,
                        'retour_id' => 43,
                        'question_id' => 15,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    322 => 
                    array (
                        'id' => 364,
                        'retour_id' => 43,
                        'question_id' => 17,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    323 => 
                    array (
                        'id' => 365,
                        'retour_id' => 43,
                        'question_id' => 18,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    324 => 
                    array (
                        'id' => 366,
                        'retour_id' => 44,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 565,
                        'reponsable_type' => 'lecon',
                    ),
                    325 => 
                    array (
                        'id' => 367,
                        'retour_id' => 44,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 586,
                        'reponsable_type' => 'lecon',
                    ),
                    326 => 
                    array (
                        'id' => 368,
                        'retour_id' => 44,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 173,
                        'reponsable_type' => 'developpement',
                    ),
                    327 => 
                    array (
                        'id' => 370,
                        'retour_id' => 44,
                        'question_id' => 5,
                        'reponse_texte' => 'Exercices qui étaient pour la plupart en lien avec le développement développé. Aucune question sur le plan. Pas de question pédagogique. 

Prendre un générateur de $F_q^*$ ($q=p^n$) et donner son ordre, en déduire le degré du polynôme minimal sur $F_p$. Cette question avait pour but de me faire dire que, lorsque l\'existence de $F_q$ était établie, le résultat du développement, i.e. l\'existence d\'un polynôme irréductible de degré n sur $F_p$, devenait quasiment immédiat. 
Ensuite, un deuxième exo qui me demandait de décomposer $X^8+X$ en irréductibles sur $F_4$. 
Puis, un exo sur le morphisme de Frobenius F : déterminer noyaux et images des itérés $F^{°i}$, puis montrer que les itérés de F forment une base des automorphismes de corps de $F_q$, j\'ai juste eu le temps de montrer que c\'était une famille génératrice, puis il m\'a dit que la liberté se montrait par argument de cardinalité. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    328 => 
                    array (
                        'id' => 371,
                        'retour_id' => 44,
                        'question_id' => 6,
                        'reponse_texte' => 'Le niveau n\'était pas bien difficile, mais le jury, sympathique, mettait un gros rythme dans son interrogation, ne me laissant souvent que peu de temps pour réfléchir. Je me suis sans doute parfois un peu précipité pour donner certaines réponses, ce qui m\'a fait dire quelques bêtises. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    329 => 
                    array (
                        'id' => 372,
                        'retour_id' => 44,
                        'question_id' => 8,
                        'reponse_texte' => 'Très surpris par le deuxième exo. Lorsque j\'ai écris $F_4=\\{0,1,\\alpha,\\alpha +1\\}$, où $\\alpha$ est racine de $X^2+X+1$, le jury m\'a vite demandé si $\\alpha$ était racine de $X^8+X$. Après quelques instants de réflexion, j\'ai fini par dire que $\\alpha^4=\\alpha$ car $\\alpha \\in F_4$, et avant d\'avoir le temps de poursuivre mon raisonnement, le jury m\'a directement affirmé que $F_4$ était inclus dans $F_8$ puis a enchaîné sur la suite de l\'exercice. Or je me suis aperçu, à tête reposée à l\'issue de l\'oral, que c\'était complètement faux : durant tout l\'exercice, le jury a fait comme si 8 était puissance de 4. Alors, soit le jury est particulièrement machiavélique en me donnant des résultats faux, mais comme il a enchaîné directement sans me laisser le temps de réfléchir à ce qu\'il disait, et que les deux autres membres du jury n\'avaient pas l\'air au taquet sur l\'exo, je pense plutôt que le jury n\'a pas remarqué non plus son erreur. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    330 => 
                    array (
                        'id' => 373,
                        'retour_id' => 44,
                        'question_id' => 9,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    331 => 
                    array (
                        'id' => 374,
                        'retour_id' => 45,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 635,
                        'reponsable_type' => 'lecon',
                    ),
                    332 => 
                    array (
                        'id' => 375,
                        'retour_id' => 45,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 602,
                        'reponsable_type' => 'lecon',
                    ),
                    333 => 
                    array (
                        'id' => 376,
                        'retour_id' => 45,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 1,
                        'reponsable_type' => 'developpement',
                    ),
                    334 => 
                    array (
                        'id' => 400,
                        'retour_id' => 48,
                        'question_id' => 14,
                    'reponse_texte' => 'D\'abord des questions portant sur le plan (visiblement posées par quelqu\'un qui l\'avait mal lu, car les réponses étaient dedans), puis des exercices.

Pourquoi la famille $n\\mathbb{1}_{[0,1/n]}$, que j\'avais mise comme exemple, n\'est-elle pas uniformément intégrable ?

Justifier l\'inégalité de Hoeffding comme développement dans cette leçon (comme si je ne l\'avais pas fait dans ma défense du plan !) et donner une autre inégalité du même genre, mois fine (comme si la loi des grands nombres $L^2$ ne se trouvait pas dans mon plan juste avant Hoeffding !).

Comment montrer la formule de Stirling avec le théorème central limite ? (j\'avais mis la propriété dans mon plan).

Condition nécessaire et suffisante pour que $\\sum_{k=1}^n X_k$ converge en loi, où les $X_k$ sont indépendantes et $X_k$ a la loi $\\mathcal{N}(0,\\sigma^2)$.

Montrer que si les $X_k$ sont iid de loi de Cauchy, $\\frac{X_1+\\cdots+X_n}{n}$ converge en loi mais pas presque-sûrement (pour cette dernière propriété, j\'ai parlé de tribu asymptotique et ils m\'ont demandé le lemmme de Borel-Cantelli). J\'ai reçu des indications.

Si les $X_n$ sont indépendantes avec $X_n$ de loi $Ber(1/n)$, les $X_n$ convergent en probabilité, mais pas presque-sûrement.

Si les $\\epsilon_n$ sont iid avec $\\mathbb{P}(\\epsilon_n=1) = \\mathbb{P}(\\epsilon_n=-1)=1/2$ et $(a_n)_n$ est une suite de réels, donner une CNS pour que $\\sum_{k=1}^n\\epsilon_k a_k$ converge dans $L^2$, puis montrer que cette condition est aussi une condition de convergence dans $L^4$.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    335 => 
                    array (
                        'id' => 378,
                        'retour_id' => 45,
                        'question_id' => 14,
                        'reponse_texte' => 'D\'abord des exos, puis des questions sur mon plan, qui ont entraîné d\'autres exos. 

Quelques petites questions sur mon développement (pourquoi cette démonstration de la loi des GN nécessite de se placer dans $L^4$ ? donner l\'énoncé du lemme de Borel-Cantelli). Puis un exo portant sur la démonstration de la loi forte des GN dans le cas $L^2$, en me donnant une piste de départ sur l\'étude de la sous-suite $(S_{n^2})$ de la moyenne empirique $S_n$. 
Puis des questions sur le plan, notamment sur la démonstration du théorème de Lévy. J\'ai donné les grandes lignes d\'une démonstration passant par des sous-ensembles dont l\'adhérence contient les fonctions continues à support compact, puis on a fait la démonstration dans le cas de la dimension 1 en passant par les distributions. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    336 => 
                    array (
                        'id' => 379,
                        'retour_id' => 45,
                        'question_id' => 15,
                    'reponse_texte' => 'Un membre du jury avait déjà sévi dans l\'épisode "Pierre P et les espaces $L^p$". Il a récidivé en soufflant dès que j\'ai dit que je prenais la leçon de proba. Ensuite, durant les exos, qu\'il menait pour la plupart, il n\'a pas arrêté de dire que son exo permettait de faire des maths (sous-entendu plutôt que des probas). Il est allé jusqu\'à demander à son acolyte s\'il l\'autorisait à me poser une question portant sur une démonstration du thm de Lévy par les distributions tempérées. Il m\'a ensuite demandé mon approbation : "Connaissez-vous les distributions tempérées ?". Lorsque j\'ai répondu oui, il a ensuite été très souriant et content de pouvoir faire des maths. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    337 => 
                    array (
                        'id' => 380,
                        'retour_id' => 45,
                        'question_id' => 17,
                    'reponse_texte' => 'Surpris d\'avoir eu à faire un exo reposant essentiellement sur la transformée de Fourier dans $S\'(\\mathbb R)$, mais plutôt content de l\'avoir eu car les distributions faisaient partie de mes leçons préférées. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    338 => 
                    array (
                        'id' => 381,
                        'retour_id' => 45,
                        'question_id' => 18,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    339 => 
                    array (
                        'id' => 382,
                        'retour_id' => 46,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 588,
                        'reponsable_type' => 'lecon',
                    ),
                    340 => 
                    array (
                        'id' => 383,
                        'retour_id' => 46,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 581,
                        'reponsable_type' => 'lecon',
                    ),
                    341 => 
                    array (
                        'id' => 385,
                        'retour_id' => 46,
                        'question_id' => 6,
                        'reponse_texte' => ' Jury vraiment sympa qui s\'excuse quand il pose une question un peu bête, qui acquiesce dès que je pars dans la bonne direction et qui aide quand je bloque. L\'oral était principalement guidé par Tosel.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    342 => 
                    array (
                        'id' => 386,
                        'retour_id' => 46,
                        'question_id' => 8,
                        'reponse_texte' => 'Un oral qui ressemblait plus que d\'habitude à notre préparation, des résultats du plan à démontrer ou expliquer. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    343 => 
                    array (
                        'id' => 387,
                        'retour_id' => 46,
                        'question_id' => 9,
                        'reponse_texte' => '18',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    344 => 
                    array (
                        'id' => 388,
                        'retour_id' => 46,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 5,
                        'reponsable_type' => 'developpement',
                    ),
                    345 => 
                    array (
                        'id' => 393,
                        'retour_id' => 47,
                        'question_id' => 5,
                        'reponse_texte' => 'A noter, des questions sur le groupe symétrique que je n\'avais pas mis dans le plan.

Prouver le lemme que j\'avais admis dans mon développement (c\'est à dire le lemme du Goblot). Trois preuves différentes du fait que $\\Gamma$ (voir le Goblot, à nouveau) est un groupe.

La réciproque du théorème chinois est-elle vraie ? Le démontrer.

Montrer qu\'un sous-groupe fini de $SL_2(\\mathbb{R})$ est cyclique (avec des indications).

Où ai-je utilisé le groupe symétrique dans mon plan ? (dans l\'étude des groupes de Sylow)

Donner les p-Sylow du groupe symétrique d\'ordre 4.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    346 => 
                    array (
                        'id' => 390,
                        'retour_id' => 47,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 555,
                        'reponsable_type' => 'lecon',
                    ),
                    347 => 
                    array (
                        'id' => 391,
                        'retour_id' => 47,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 578,
                        'reponsable_type' => 'lecon',
                    ),
                    348 => 
                    array (
                        'id' => 392,
                        'retour_id' => 47,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 1,
                        'reponsable_type' => 'developpement',
                    ),
                    349 => 
                    array (
                        'id' => 394,
                        'retour_id' => 47,
                        'question_id' => 6,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    350 => 
                    array (
                        'id' => 395,
                        'retour_id' => 47,
                        'question_id' => 8,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    351 => 
                    array (
                        'id' => 396,
                        'retour_id' => 47,
                        'question_id' => 9,
                        'reponse_texte' => '15.75',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    352 => 
                    array (
                        'id' => 397,
                        'retour_id' => 48,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 635,
                        'reponsable_type' => 'lecon',
                    ),
                    353 => 
                    array (
                        'id' => 398,
                        'retour_id' => 48,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 597,
                        'reponsable_type' => 'lecon',
                    ),
                    354 => 
                    array (
                        'id' => 399,
                        'retour_id' => 48,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 79,
                        'reponsable_type' => 'developpement',
                    ),
                    355 => 
                    array (
                        'id' => 401,
                        'retour_id' => 48,
                        'question_id' => 15,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    356 => 
                    array (
                        'id' => 402,
                        'retour_id' => 48,
                        'question_id' => 17,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    357 => 
                    array (
                        'id' => 403,
                        'retour_id' => 48,
                        'question_id' => 18,
                        'reponse_texte' => '20',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    358 => 
                    array (
                        'id' => 408,
                        'retour_id' => 51,
                        'question_id' => 37,
                        'reponse_texte' => 'A73 : marches aléatoires, théorèmes limites, ...',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    359 => 
                    array (
                        'id' => 409,
                        'retour_id' => 51,
                        'question_id' => 38,
                        'reponse_texte' => 'A19 : variables de Bernoulli, tests statistiques, chaînes de Markov',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    360 => 
                    array (
                        'id' => 410,
                        'retour_id' => 51,
                        'question_id' => 39,
                    'reponse_texte' => 'On étudie l’évolution du capital d’un groupe d’entreprises. Pour cela, on s’intéressait uniquement aux variables ordonnées du $n$-uplet $(X_1,…,X_n)$. On utilisait alors des fonctions de $[0,n]$ affines par morceaux dont les pentes étaient les valeurs du $n$-uplet étudié. La loi des grands nombres et le théorème central limite donnaient deux résultats de convergence de ces fonctions, dont on déduisait le comportement asymptotique des capitaux (regroupement des capitaux autour de certaines valeurs).',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    361 => 
                    array (
                        'id' => 411,
                        'retour_id' => 51,
                        'question_id' => 40,
                    'reponse_texte' => 'Les notations et le modèle utilisés étaient plutôt rapides à introduire, à l’aide de représentations graphiques des fonctions étudiées. J’ai ensuite démontré un des théorèmes importants ainsi que le lemme qu’il utilisait, et évoqué à l’oral celui qui utilisait le théorème central limite. Mon premier code permettait simplement de représenter les fonctions associées à un $n$-uplet. J’ai également fait deux autres modélisations pour illustrer les deux résultats de convergence (vers l’état d’équilibre et les fluctuations autour de celui-ci). Pour terminer, j’ai donné une conclusion quant aux résultats obtenus et discuté de certaines des hypothèses faites.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    362 => 
                    array (
                        'id' => 412,
                        'retour_id' => 51,
                        'question_id' => 42,
                    'reponse_texte' => 'Ils ont commencé par quelques questions rapides sur ma démonstration (problèmes de notations entre autres).
Dans la démonstration, j’utilisais qu’on avait $X_k/k$ qui tendait presque surement vers 0 grâce à la loi des grands nombres (argument donné dans le texte). Ils m’ont demandé de détailler ce point et s’il existait un autre moyen de le montrer. Il fallait utiliser Borel-Cantelli.
Ils m’ont ensuite poser une autre question pour voir si j’avais bien compris ce qu’on en déduisait concernant les capitaux, sûrement parce que je n’avais pas été très clair sur ça pendant ma présentation.
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    363 => 
                    array (
                        'id' => 413,
                        'retour_id' => 51,
                        'question_id' => 45,
                    'reponse_texte' => 'Je pense que les éléments choisis pour la présentation étaient plutôt pertinents (simulations/preuves/discussions). Mais, je me suis pas mal embrouillé sur la démonstration. J’aurais dû plus la préparer pendant les 4 heures. Et durant la phase d’interprétation des résultats, ne pas hésiter à dire des choses qui pourraient paraître évidentes, histoire d’être sûr que le jury a bien compris que vous avez compris.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    364 => 
                    array (
                        'id' => 414,
                        'retour_id' => 51,
                        'question_id' => 41,
                        'reponse_texte' => 'Le jury était attentif durant ma présentation, ni cassant, ni souriant. Pendant la phase de questions, un des membres du jury monopolisait plus ou moins la parole. Les autres sont intervenus pour des questions banales ou pour donner des indications. Ils m’ont corrigé une fois aussi.
Le jury ne me laissait pas du tout réfléchir à leurs questions et ils donnaient très vite des indications, plutôt frustrant.
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    365 => 
                    array (
                        'id' => 415,
                        'retour_id' => 51,
                        'question_id' => 43,
                    'reponse_texte' => 'Il n’y a eu aucune réaction concernant mes simulations. D’ailleurs le vidéoproj projetait à moitié sur le tableau à craie (pas d’écran), le jury n’a pas dû voir grand-chose. Par contre, ils s’occupaient d’allumer le vidéoproj quand je leur demandais, c’était plutôt pratique. Autre point positif, le tableau était grand, et à craie.
Je n’ai pas eu le droit à une question finale sur les chaînes de Markov ou de statistique, probablement par manque de temps.
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    366 => 
                    array (
                        'id' => 416,
                        'retour_id' => 51,
                        'question_id' => 44,
                        'reponse_texte' => '16.5',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    367 => 
                    array (
                        'id' => 417,
                        'retour_id' => 52,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 710,
                        'reponsable_type' => 'lecon',
                    ),
                    368 => 
                    array (
                        'id' => 418,
                        'retour_id' => 52,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 723,
                        'reponsable_type' => 'lecon',
                    ),
                    369 => 
                    array (
                        'id' => 419,
                        'retour_id' => 52,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 51,
                        'reponsable_type' => 'developpement',
                    ),
                    370 => 
                    array (
                        'id' => 449,
                        'retour_id' => 54,
                        'question_id' => 14,
                    'reponse_texte' => 'J\'ai eue des questions sur le plan (notamment sur des propositions fausses que j\'ai corrigé à l\'oral) et sur la convergence de mes contre exemples de séries qui convergeaient ou non sur leur disque de convergence (trouvé dans le Hauchecorne) ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    371 => 
                    array (
                        'id' => 565,
                        'retour_id' => 63,
                        'question_id' => 14,
                        'reponse_texte' => 'Beaucoup de questions sur mon développement,
des exercices de convergence dominée et de dérivation sous l\'intégrale sur lesquelles j\'ai buggué comme un gros nul 
Déroutant, d\'autant plus que j\'avais bien préparé cette leçon et mis des exemples originaux sur lesquels je voulais être questionné. ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    372 => 
                    array (
                        'id' => 428,
                        'retour_id' => 52,
                        'question_id' => 14,
                        'reponse_texte' => 'Deux questions sur le développement pour voir en gros si je ne suis pas un tocard.

Soit $f$ continue avec $f(a)=0$. Montrer que l’ensemble des points $x$ tels que la suite $u_{n+1}=f(u_n)$ partant de $x$ converge vers a est un ouvert.
- On utilise la continuité des itérés de $f$.

On se place dans un Hilbert $H$ séparable. Montrer que si $(u_n)_n$ converge faiblement vers $u$, alors $(||u_n||)_n$ est bornée. 
- On utilise Banach-Steinhaus.
Soit $(e_k)_k$ une base hilbertienne de $H$. Montrer que $(u_n)_n$ converge faiblement vers $u$ si et seulement si pour tout $k$, $\\langle u_n,e_k\\rangle \\to \\langle u,e_k \\rangle$.
- Le sens direct est évident. Je me suis pas mal embrouillé dans les arguments et les notations, mais ça se fait plutôt bien avec Cauchy-Schwarz et une interversion de limite. On est passé à un autre exercice.

On se place dans $L^2 ([0,1])$. On note $e_k : x \\mapsto x^{1/k}$. Montrer que la famille $(e_k)_k$ est une famille totale.
- On montre que l’orthogonal de cette famille est nul. (Indication : introduire $F(z) = \\int_{0}^{1} f(x)x^z \\mathrm{d}x$) La fonction $F$ est holomorphe grâce au théorème d’holomorphie sous l’intégrale (J’ai galéré dans le critère de Riemann pour donner son domaine de définition). Avec le théorème des zéros isolés, $F = 0$. En particulier $F(k) = 0$ pour tout $k$ et on conclut avec le théorème de Weierstrass.
',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    373 => 
                    array (
                        'id' => 429,
                        'retour_id' => 52,
                        'question_id' => 15,
                        'reponse_texte' => 'Le jury était plutôt sympa, et donnait des indications quand je galérais mais me laissait aussi réfléchir. Ils n\'ont pas trop aimé que je bute sur le critère de Riemann par contre, normal !',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    374 => 
                    array (
                        'id' => 430,
                        'retour_id' => 52,
                        'question_id' => 17,
                        'reponse_texte' => 'Tableau blanc et grand.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    375 => 
                    array (
                        'id' => 431,
                        'retour_id' => 52,
                        'question_id' => 18,
                        'reponse_texte' => '19.75',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    376 => 
                    array (
                        'id' => 432,
                        'retour_id' => 53,
                        'question_id' => 1,
                        'reponse_texte' => '',
                        'reponsable_id' => 668,
                        'reponsable_type' => 'lecon',
                    ),
                    377 => 
                    array (
                        'id' => 433,
                        'retour_id' => 53,
                        'question_id' => 2,
                        'reponse_texte' => '',
                        'reponsable_id' => 683,
                        'reponsable_type' => 'lecon',
                    ),
                    378 => 
                    array (
                        'id' => 434,
                        'retour_id' => 53,
                        'question_id' => 3,
                        'reponse_texte' => '',
                        'reponsable_id' => 11,
                        'reponsable_type' => 'developpement',
                    ),
                    379 => 
                    array (
                        'id' => 438,
                        'retour_id' => 53,
                        'question_id' => 5,
                    'reponse_texte' => 'Une question sur le développement : pourquoi on a $\\mu_i ^2 = \\mu_i \' ^2 \\iff  \\mu_i = \\mu_i \'$ (j\'avais donné l\'argument à l\'oral) ? Pourquoi ces valeurs propres sont-elles bien positives ?

On considère la décomposition $\\nu : O_n(\\mathbb{R}) \\times T_n^+(\\mathbb{R}) \\to GL_n(\\mathbb{R})$ avec $T_n^+(\\mathbb{R})$ ensemble des matrices triangulaires supérieures à coefficients diagonaux strictement positifs. A quel résultat est-elle liée ? Orthonormalisation de Gram-Schmidt. Quelle est l\'expression de $\\mu^{-1} \\circ \\nu$ ? S\'écrit facilement.

Ensuite, plusieurs questions sur le plan, je ne me souviens pas de toutes, mais en tout cas : démonstration des isomorphismes exceptionnels (cf. Perrin), démonstration de la connexité de $GL_n(\\mathbb{C})$, cas de $\\mathbb{R}$.

',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    380 => 
                    array (
                        'id' => 439,
                        'retour_id' => 53,
                        'question_id' => 6,
                        'reponse_texte' => 'Plutôt sympa. Un des membres du jury n\'a pas dit un mot.
Un autre me demandait tout le temps de réexpliquer un argument alors que ça n\'avait pas l\'air de poser problème aux autres.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    381 => 
                    array (
                        'id' => 440,
                        'retour_id' => 53,
                        'question_id' => 8,
                        'reponse_texte' => '',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    382 => 
                    array (
                        'id' => 441,
                        'retour_id' => 53,
                        'question_id' => 9,
                        'reponse_texte' => '17',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    383 => 
                    array (
                        'id' => 442,
                        'retour_id' => 54,
                        'question_id' => 10,
                        'reponse_texte' => '',
                        'reponsable_id' => 736,
                        'reponsable_type' => 'lecon',
                    ),
                    384 => 
                    array (
                        'id' => 443,
                        'retour_id' => 54,
                        'question_id' => 11,
                        'reponse_texte' => '',
                        'reponsable_id' => 730,
                        'reponsable_type' => 'lecon',
                    ),
                    385 => 
                    array (
                        'id' => 444,
                        'retour_id' => 54,
                        'question_id' => 12,
                        'reponse_texte' => '',
                        'reponsable_id' => 128,
                        'reponsable_type' => 'developpement',
                    ),
                    386 => 
                    array (
                        'id' => 450,
                        'retour_id' => 54,
                        'question_id' => 15,
                    'reponse_texte' => 'Plutôt pas content mais pas méchants. Le jury a pas du tout apprécié l\'exemple du Gourdon d\'utilisation du théorème d\'Abel car il y a plus simple que ça pour le résoudre (et il me l\'on demandé en questions). Aussi, du au stress j\'ai mal définis les Ek dès le début de mon développement donc ils m\'ont arrété (et redemandé une définition plus rigoureuse en nommant mes partitions à la fin du développement). Aussi, ça a perturbé le jury que mes indices k et n soient inversés àla fin de ma preuve par rapport à l\'énoncé.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    387 => 
                    array (
                        'id' => 451,
                        'retour_id' => 54,
                        'question_id' => 17,
                    'reponse_texte' => 'Mon développement à duré trop longtemps (17 min avec leur intervention) mais ils me l\'on dit et m\'ont laisser quand même conclure (donc c\'est cool, ils étaient pas à la minute près). Sinon c\'est long 20 min de questions, ça ressemble beaucoup aux oraux de concours je trouve.
PS: j\'ai pas encore ma note (je pense que ça se voit)',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    388 => 
                    array (
                        'id' => 452,
                        'retour_id' => 54,
                        'question_id' => 18,
                        'reponse_texte' => '20',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    389 => 
                    array (
                        'id' => 453,
                        'retour_id' => 55,
                        'question_id' => 55,
                        'reponse_texte' => 'cryptosystème à  clef secrète',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    390 => 
                    array (
                        'id' => 454,
                        'retour_id' => 55,
                        'question_id' => 56,
                        'reponse_texte' => 'Un truc avec du résultant ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    391 => 
                    array (
                        'id' => 455,
                        'retour_id' => 55,
                        'question_id' => 57,
                    'reponse_texte' => 'on fait un cryptosysteme a clef secrète affine (en gros on code en faisant $M*(m +k)$ avec $m,k$ des scalaires étant le message et la clefs secrète et $M$ une matrice de permutation.) On regarde une attaque possible et du coup on code en faisant plusieir fois le codage précédent avec des $k$ différents, puis on choisit bien $M$.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    392 => 
                    array (
                        'id' => 456,
                        'retour_id' => 55,
                        'question_id' => 58,
                    'reponse_texte' => 'J\'ai fait un truc un peu original en partant d\'une phrase de l\'intro et ça a fait un flop total (en gros j\'ai essayer de partitionner mon message et de le coder morceau par morceaux mais finalement c\'est un peu plus rapide mais niveau sécurité ça change que dalle). Sinon j\'ai suivi le texte, j\'ai pas fait assez de maths à leur goût je pense.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    393 => 
                    array (
                        'id' => 457,
                        'retour_id' => 55,
                        'question_id' => 59,
                    'reponse_texte' => 'Je me suis un peu fait démonté même si ils essayaient d\'être gentils. Il faut savoir que comparer la complexité à RSA demande de bien comparer des valeurs comparables (ici prendre n (taille du message en nombre de bit) = N). Aussi, on doit savoir combien d\'opération un ordi fait par seconde (en ordre de grandeur)   ',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    394 => 
                    array (
                        'id' => 458,
                        'retour_id' => 55,
                        'question_id' => 60,
                        'reponse_texte' => 'Bien faire des maths ET de l\'info. Pas partir sur des trucs totalement originaux',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    395 => 
                    array (
                        'id' => 459,
                        'retour_id' => 55,
                        'question_id' => 61,
                    'reponse_texte' => 'En fait, ils ont rigolé entre eux pendant la présentation et ça m\'a pas mal perturbé. Je ne sais pas si ils rigolaient entre eux ou si c\'était àcause de ce que je disait et écrivait (peut être que j\'ai fait trop de fautes d\'orthographes au tableau)',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    396 => 
                    array (
                        'id' => 460,
                        'retour_id' => 55,
                        'question_id' => 62,
                        'reponse_texte' => 'Je pensais vraiment que ça serait plus facile que ça. Certaines parties du texte étaient dur à comprendre',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    397 => 
                    array (
                        'id' => 461,
                        'retour_id' => 55,
                        'question_id' => 63,
                        'reponse_texte' => '20',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    398 => 
                    array (
                        'id' => 462,
                        'retour_id' => 56,
                        'question_id' => 37,
                        'reponse_texte' => 'Problème d\'apparition d\'un mot donné dans une suite de lettres aléatoires. Tags : Chaines de Markov, Martingales, Temps d\'arret.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    399 => 
                    array (
                        'id' => 463,
                        'retour_id' => 56,
                        'question_id' => 38,
                        'reponse_texte' => 'Mécanique statistique. Tags : Chaine de Markov, Mesure invariante, convergence, vitesse de convergence.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    400 => 
                    array (
                        'id' => 464,
                        'retour_id' => 56,
                        'question_id' => 39,
                    'reponse_texte' => 'On sait qu\'un mot fixé apparait une infinité de fois dans une suite de lettres indépendantes uniformes sur l\'alphabet. Que dire de la fréquence d\'apparition d\'un mot par rapport à un autre ? Dans la 1ere partie, introduction d\'une chaine de Markov qui mesure le nombre de lettres en commun avec le mot voulu et qui donne la finitude du temps d\'apparition de ce mot. Puis un paragraphe sur comment réaliser la série génératrice de la loi d\'apparition je crois (pas traité). Enfin, une preuve de l’espérance du temps d\'apparition sur un alphabet binaire avec des martingales.',
                        'reponsable_id' => 0,
                        'reponsable_type' => '',
                    ),
                    401 => 
                    array (
                        'id' => 465,
                        'retour_id' => 56,
                        'question_id' => 40,
                        'reponse_texte' => 'J\'ai décidé de ne traiter que les parties sur la chaine de Markov et sur la preuve de l\'espérance du t.a. sans regarder la série génératrice. 
En termes de simulation : un programme qui détecte l\'apparition d\'un mot donné dans un autre mot. La réalisation de la chaine de Markov annoncée pour un mot donné, avec des mots donnés aléatoires, puis deux exemples particuliers extrêmes (qui se sont avérés etre les cas minimal et maximal du temps d\'atteinte). Enfin, un programme pour calculer la moyenne empirique du temps d\'apparition pour illustrer les résultats du texte.

Plan :
I) Autour de l\'apparition d\'un mot
Comparaison avec une géométrique, majoration de l\'espérance, infinité d\'apparition. (Prouvée)
II) Heuristique de la non-uniformité à l\'intérieur des mots à meme nombre de lettres
Introduction de la chaine de Markov, illustration via les simulations et comparaisons des matrices de transition des deux cas particuliers traités.
III) Esperance du temps d\'apparition
Preuve intégrale de la formule de l\'espérance via les martingales. (Prouvée) Puis illustration via le programme.',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        402 => 
        array (
            'id' => 466,
            'retour_id' => 56,
            'question_id' => 42,
            'reponse_texte' => 'Question du jury : 
-Comment montrer autrement le fait qu\'un mot apparait un nombre infini de fois ps (Borel Cantelli - j\'avais utilisé un argument type propriété de Markov à l\'oral)
-Vous avez parlé de théorèmes d\'arret en disant que c\'était une version déguisée du TCD. Quelles sont les hypothèses possibles ?
-Comment estimer les probabilités de transition à partir d\'une unique réalisation de la Chaine ? Quelle est l\'hypothèse qui assure que les états vont être visités (irréductibilité) ? Quel théorème justifie la convergence de votre estimateur (Théorème ergodique) ?
-Etant donné la chaine de Markov étudiée, que peut-on dire sur une mesure invariante (existence/unicité/convergence) -> quelles hypothèses pour quoi. Lien entre la mesure invariante et l’espérance du temps de retour.',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        403 => 
        array (
            'id' => 467,
            'retour_id' => 56,
            'question_id' => 45,
            'reponse_texte' => 'Je pense que j\'ai fait des bons choix : j\'ai prouvé peu de choses, j\'ai motivé la problématique de mon exposé, et j\'avais des simulations simples mais visuelles. Je pense que ça a pas mal plu au jury.',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        404 => 
        array (
            'id' => 468,
            'retour_id' => 56,
            'question_id' => 41,
        'reponse_texte' => 'Le jury dans l\'ensemble était plutôt sympathique durant l\'oral (j\'avais même un verre d\'eau qui m\'attendait sur la table). Tout le monde n\'avait pas le même temps de parole parmi le jury : deux hommes, deux femmes. Les deux hommes menaient la discussion, les femmes intervenaient peu (j\'ai eu une question de l\'une d\'elle je crois), mais étaient très attentives et réceptives aux réponses. L\'un des deux hommes était plus \'direct\' et interrogateur que l\'autre, mais il restait assez bienveillant je pense.
',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        405 => 
        array (
            'id' => 469,
            'retour_id' => 56,
            'question_id' => 43,
            'reponse_texte' => 'J\'ai eu la chance d\'avoir un tableau noir de taille respectable dont les volets extérieurs se rabattaient pour faire place à un tableau blanc : pratique pour les simulations.',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        406 => 
        array (
            'id' => 470,
            'retour_id' => 56,
            'question_id' => 44,
            'reponse_texte' => '20',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        407 => 
        array (
            'id' => 471,
            'retour_id' => 57,
            'question_id' => 1,
            'reponse_texte' => '',
            'reponsable_id' => 686,
            'reponsable_type' => 'lecon',
        ),
        408 => 
        array (
            'id' => 472,
            'retour_id' => 57,
            'question_id' => 2,
            'reponse_texte' => '',
            'reponsable_id' => 702,
            'reponsable_type' => 'lecon',
        ),
        409 => 
        array (
            'id' => 473,
            'retour_id' => 57,
            'question_id' => 3,
            'reponse_texte' => '',
            'reponsable_id' => 31,
            'reponsable_type' => 'developpement',
        ),
        410 => 
        array (
            'id' => 571,
            'retour_id' => 64,
            'question_id' => 5,
        'reponse_texte' => 'Questions sur le plan, ils m\'ont fait corrigé une bébé erreur. Et sur mes annexes (pivot de Gauss pour obtenir des équations de sev)
Detailler un point de mon développement.
Exos : 
- Prouver la formule de changement de base dans la base duale
- Montrer que tout endomorphisme d\'un C-ev de dimension finie admet un hyperplan stable (bizarre qu\'ils m\'aient demandé celui là vu mon plan qui mettait en avant pas mal d\'application de la transposition déjà)
- l\'exo classique sur les intersections d\'hyperplan (que j\'ai oublié de rajouter dans mon plan..)
- montrer à la main (sans les bases antéduales), que deux formes linéaires définissent le même hyperplan ssi elles sont proportionnelles avec un coeff non nul',
            'reponsable_id' => 0,
            'reponsable_type' => '',
        ),
        411 => 
        array (
            'id' => 478,
            'retour_id' => 57,
            'question_id' => 5,
        'reponse_texte' => 'Quelques questions sur mon développement (un point que j\'avais peu détaillé, notations). Les invariants de similitude sont les espaces ou les polynômes ?
-Les actions que vous donnez sont-elles à droite ou à gauche ? (je sais jamais)
-Vous avez centré votre exposé sur les invariants et les formes normales. Que peut-on dire de la structure des orbites ? Par exemple pour l\'action d\'équivalence, que peut-on dire des orbites de rang fixé ? (j\'ai parlé d\'adhérence et de semi-continuité) Est-ce que ces orbites sont des sous-variétés ? (Pour rg=0 et rg=n, c\'est trivial, sinon, je ne savais pas, et je leur ai dit - vu que le rang n\'est pas continu, je ne voyais pas comment procéder).
-Ex: si un sg additif $G$  est stable par multiplication à gauche et à droite par $M_n$, montrer que $G=\\{0\\}$ ou $G=M_n$. (Si non nul, on a une matrice dont un coeff est non nul, donc on a une matrice avec $a_{1,1}=1$ dans $G$ en permutant/dilatant, puis par pivot de Gauss, on tue les coeff sur la première ligne et la première colonne, et en multipliant par la matrice $E_{1,1}$, on obtient $E_{1,1}$, donc tous les $E_{i,j}$ par permutation, donc $M_n$. 
-A propos de réduction simultanée et d\'action diagonale (ce que j\'avais fait pour la conjugaison et la congruence), que se passe-t-il pour l\'action d\'équivalence ? Nb de classes, invariants, etc. (J\'ai un peu tâtonné, cherchant à réduire l\'un puis l\'autre sans trop de succès. Ils m\'ont dit de regarder le cas où la dim=1 -> On voit alors que on a soit (0,0), soit (1,0) ou (0,1), soit (1,x), $x\\neq 0$, et le rapport $x$ est un invariant. Retour à $n>1$, on veut regarder la classe de (Id,M), et on voit que c\'est (Id, P), où P est semblable à M)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            412 => 
            array (
                'id' => 479,
                'retour_id' => 57,
                'question_id' => 6,
                'reponse_texte' => 'Un peu sec tout au début quand j\'avais pris des mauvaises notations dans mon plan sur les invariants de similitude et que je confonde action à droite et à gauche, mais après, avec les exercices, ils sont vite devenus assez enthousiastes. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            413 => 
            array (
                'id' => 480,
                'retour_id' => 57,
                'question_id' => 8,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            414 => 
            array (
                'id' => 481,
                'retour_id' => 57,
                'question_id' => 9,
                'reponse_texte' => '18',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            415 => 
            array (
                'id' => 482,
                'retour_id' => 58,
                'question_id' => 10,
                'reponse_texte' => '',
                'reponsable_id' => 711,
                'reponsable_type' => 'lecon',
            ),
            416 => 
            array (
                'id' => 483,
                'retour_id' => 58,
                'question_id' => 11,
                'reponse_texte' => '',
                'reponsable_id' => 719,
                'reponsable_type' => 'lecon',
            ),
            417 => 
            array (
                'id' => 484,
                'retour_id' => 58,
                'question_id' => 12,
                'reponse_texte' => '',
                'reponsable_id' => 96,
                'reponsable_type' => 'developpement',
            ),
            418 => 
            array (
                'id' => 529,
                'retour_id' => 61,
                'question_id' => 14,
                'reponse_texte' => 'J\'ai été étonné car j\'ai eu beaucoup de questions sur mon développement et sur mon plan, mais presque aucun exercice. 

Sur mon développement (densité des polynômes orthogonaux) ils m\'ont demandé pourquoi il suffisait de montrer que $ \\int fx^n=0 \\forall n \\Rightarrow f=0$ pour avoir la densité, je me suis un peu embrouillé en parlant d\'abord des conséquences de Hahn Banach (la caractérisation des sous espaces denses par les formes linéaires), puis ils m\'ont rappelé qu\'on était dans un Hilbert et j\'ai fait la démo normale.

J\'ai eu des questions un peu bizarre, genre pourquoi $\\hat{f}=0 \\Rightarrow f=0$, alors que j\'avais dit trois fois qu\'à cet endroit là j\'utilisais l\'injectivité de la transformée de Fourier. Aussi dans le développement on se place sur un intervalle $I$ et ensuite on se ramène à une fonction sur $\\mathbb{R}$, et ils m\'ont demandé à quoi servait l\'intervalle $I$, pas trop compris...

Questions sur le plan :
-Pourquoi j\'ai rayé le Lemme du Riemann-Lebesgue dans mon plan? Parce que je me suis rendu compte que je l\'avais déjà mis avant sous une autre forme héhé
-Pourquoi si $f,g\\in L^1$ alors $f \\star g \\in L^1$. Meme chose avec $f\\in L^p, g\\in L^q$
-Vous avez dit qu\'on a pas besoin de la convergence dominé pour montrer le théorème d\'analycité sous l\'intégrale, pourquoi? Parce que voilà : paf recasage de la démo que j\'avais relu 5mn avant (cf Faraut, si je me trompe pas c\'est juste un théorème moins fort d\'interversion de somme et d\'intégrale). Hmm oui mais non, alors comment on caractérise une fonction analytique en terme de différentielle? Ben comme ça. Bon ok. Fin de la question
-Qu\'est ce qu\'il se passe dans le théorème de continuité si on intègre sur un segment? Ben ça marche tout le temps pour peu que la fonction soit continue en les deux variables
',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            419 => 
            array (
                'id' => 490,
                'retour_id' => 58,
                'question_id' => 14,
            'reponse_texte' => 'Défense bien passée, même si le jury était à moitié mort (je passe à 16h durant la dernière semaine d\'oral), l\'un avait les yeux fermés la plupart du temps. Durant le développement, j\'ai senti le jury très peu réceptif, et pas convaincu, du coup j\'ai perdu confiance, j\'ai réexpliqué des choses et perdu du temps inutilement : ils m\'ont demandé de conclure rapidement alors qu\'il me restait une partie du développement à montrer. J\'ai donné les étapes. Ils m\'ont demandé comme première question de détailler le dernier point (montrer que homéo implique difféo sur la réciproque).
-Ex: pouvez-vous appliquer ça (inversion locale) pour montrer que une fonction holomorphe est d\'image ouverte. J\'ai évacué le cas où la dérivée en un point est non nul, car on applique l\'inversion locale. Ensuite, j\'ai un peu galéré. Ils m\'ont orienté vers l\'écriture de la série entière en ce point. J\'ai introduit le premier coefficient non nul, et ils m\'ont dit de montrer qu\'alors on avait pas forcément un difféo, mais qu\'on avait en fait un difféo à la puissance $p$. On factorise le terme en $z^p$ puis il reste un terme qui est non nul en $0$, donc on a une détermination locale du logarithme et donc une puissance 1/p-ième.
-Ex: Montrer qu\'une fonction croissante de $[0,1]$ dans lui-même a un point fixe. J\'ai fait un dessin, posé $c=\\inf\\{x; f(x)\\leq x\\}$. Ils m\'ont demandé de dire des choses sur l\'ensemble considéré, j\'ai surtout parlé du fait qu\'il n\'était pas fermé, mais ils attendaient simplement qu\'il était non vide et minoré. Ensuite, j\'ai montré que si $c$ appartenait à l\'ensemble, c\'était fini. Puis j\'ai pris $(x_n)$ qui tend en décroissant, et je voulais montrer que $c$ était dans l\'ensemble. J\'ai un peu galéré, mais en regardant la bonne inégalité, on obtient le résultat.
-Ex: que dire de la différentielle autour d\'un point fixe d\'une fonction telle qu\'il existe un intervalle attractif ? Réponse donnée : norme $\\leq$1. Car sinon, on aurait une direction telle que la dérivée soit $>1+\\vareps$ et des histoires de stabilité.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            420 => 
            array (
                'id' => 491,
                'retour_id' => 58,
                'question_id' => 15,
            'reponse_texte' => 'Jury endormi au début (littéralement un qui luttait contre le sommeil pendant défense et développement), puis, allant de tout mou à très sec dans son attitude. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            421 => 
            array (
                'id' => 492,
                'retour_id' => 58,
                'question_id' => 17,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            422 => 
            array (
                'id' => 493,
                'retour_id' => 58,
                'question_id' => 18,
                'reponse_texte' => '19',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            423 => 
            array (
                'id' => 494,
                'retour_id' => 59,
                'question_id' => 55,
                'reponse_texte' => 'De la cryptographie avec de l\'algèbre linéaire sur des corps finis',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            424 => 
            array (
                'id' => 495,
                'retour_id' => 59,
                'question_id' => 56,
                'reponse_texte' => 'Un truc de combinatoire
',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            425 => 
            array (
                'id' => 496,
                'retour_id' => 59,
                'question_id' => 57,
                'reponse_texte' => 'A et B veulent créer un secret, pour cela ils choisissent un groupe G et un élément $\\mu$ de ce groupe. Ensuite A choisit un entier a, B choisit un entier b, A calcule $S_A=\\mu^a$ et B calcule $S_B=\\mu^b$, puis ils échangent leurs résultat, A calcule donc ensuite $S_B^a$ et B calcule $S_A^b$, ils connaissent donc le même secret $\\mu^{ab}$.
Ensuite le texte s\'intéresse à comment un méchant hackeur peut découvrir le secret. Dans tout le texte G était $GL_n(\\mathbb{F}_q)$. Il y avait trois grandes parties à part l\'introduction, deux d\'entre elle concernaient des méthodes d\'attaques du secret, et la troisième je ne l\'ai pas du tout traité car il y avait déjà de quoi faire, mais il y avait des matrices circulantes dedans et ça ne me tentait pas trop.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            426 => 
            array (
                'id' => 497,
                'retour_id' => 59,
                'question_id' => 58,
            'reponse_texte' => 'J\'ai redémontré quasiment toutes les affirmations du texte dans les deux première parties, et présenté des bouts de codes qui marchaient moyennement pour illustrer les méthodes d\'attaques décrites dans le texte. (environ une page et demi de code sur Sage)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            427 => 
            array (
                'id' => 498,
                'retour_id' => 59,
                'question_id' => 59,
            'reponse_texte' => 'Ils m\'ont posé beaucoup de questions sur mon exposé, j\'ai fait pas mal de démonstration donc je suis allé un peu vite et ils m\'ont demandé de reprendre plusieurs arguments, du coup ça paraissait facile vu que c\'était des trucs que j\'avais dit (ou pensé) pendant l\'exposé. Les questions plus générales ne m\'ont paru facile qu\'après coup, j\'ai hésité sur des questions un peu bêtes à cause de la fatigue. Le niveau des questions m\'a paru inégal, entre facile et moyen, j\'avais l\'impression de bien répondre puisque plusieurs membres du jury avaient une attitude très encourageante, par exemple des hochements de tête approbateurs ou des sourires quand je répondais vite. En revanche vers la fin ils m\'ont posé des questions de complexité, et ils m\'ont demandé comment j\'aurais fait pour aborder le problème de façon naïve avec telle ou telle donnée, et là j\'ai eu du mal',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            428 => 
            array (
                'id' => 499,
                'retour_id' => 59,
                'question_id' => 60,
                'reponse_texte' => 'J\'ai eu l\'impression de mal parler de ce que j\'avais fait comme code, en gros je disais à la fin d\'une partie "bon bah voilà ce que j\'ai fait" sans mettre vraiment en lien avec le texte. Pour le temps de préparation j\'ai trouvé que 4h c\'était assez large pour faire un truc bien et je met la plupart de mes erreurs sur le compte de la fatigue.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            429 => 
            array (
                'id' => 500,
                'retour_id' => 59,
                'question_id' => 61,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            430 => 
            array (
                'id' => 501,
                'retour_id' => 59,
                'question_id' => 62,
                'reponse_texte' => 'L\'oral en lui même s\'est passé à peu près comme je l\'imaginais, le jury était plutôt gentil, sauf quand j\'ai hésité sur des questions bêtes ils ont semblé s\'impatienter un peu mais je les comprends! ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            431 => 
            array (
                'id' => 502,
                'retour_id' => 59,
                'question_id' => 63,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            432 => 
            array (
                'id' => 503,
                'retour_id' => 60,
                'question_id' => 1,
                'reponse_texte' => '',
                'reponsable_id' => 699,
                'reponsable_type' => 'lecon',
            ),
            433 => 
            array (
                'id' => 504,
                'retour_id' => 60,
                'question_id' => 2,
                'reponse_texte' => '',
                'reponsable_id' => 674,
                'reponsable_type' => 'lecon',
            ),
            434 => 
            array (
                'id' => 505,
                'retour_id' => 60,
                'question_id' => 3,
                'reponse_texte' => '',
                'reponsable_id' => 120,
                'reponsable_type' => 'developpement',
            ),
            435 => 
            array (
                'id' => 510,
                'retour_id' => 60,
                'question_id' => 5,
            'reponse_texte' => 'Beaucoup de questions sur mon développement, effectivement il y avait des petites chose qui était pas hyper précises et auxquelles je n\'avais pas fait attention (par exemple lorsqu\'on montre qu\'un certain ensemble de formes quadratiques est compact, pour montrer la fermeture on utilise la caractérisation séquentielle, et ils ont beaucoup insisté pour préciser dans quel espace ça converge, pour quelle norme, etc...), mais j\'ai aussi l\'impression qu\'ils ont pas écouté grand chose à ce que j\'ai raconté pendant 15mn.

Ensuite autant de questions sur mon plan, un des membres du jury m\'a dit qu\'il le trouvait pas rigoureux parce que je me place au début dans un espace vectoriel sur un corps $K$ quelconque, et ensuite je passait allègrement de $K$ à $\\mathbb{R}$ et inversement sans forcément le préciser dans le plan, donc il m\'a fait reprendre une par une les propositions en me demandant pour quel corps c\'était vrai, et sinon qu\'est ce qu\'il se passait, etc..

Exercices : 
-Déterminer la signature de $q(x,y)=x^2+y^2+xy$ (ils m\'ont beaucoup embêté sur le changement de base que je faisais, j\'ai pas trop compris ce qu\'ils attendaient)
-Soit $q:M_n(\\mathbb{R})\\rightarrow \\mathbb{R}, M\\mapsto tr(M^2)$ Montrer que c\'est une forme quadratique et calculer sa signature (indice : considérer la restriction à $S_n(R)$)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            436 => 
            array (
                'id' => 511,
                'retour_id' => 60,
                'question_id' => 6,
                'reponse_texte' => 'Le membre du jury précédemment cité était assez cassant, il enchaînait les questions très vite, coupait la parole aussi bien à moi qu\'aux autres membres du jury. Les autres n\'ont pas dit grand chose, à part un qui m\'a donné des exercices à la fin.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            437 => 
            array (
                'id' => 512,
                'retour_id' => 60,
                'question_id' => 8,
                'reponse_texte' => 'Ça s\'est passé comme je l\'imaginais, le jury était un petit peu agressif mais il y en avait toujours un pour calmer le jeu.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            438 => 
            array (
                'id' => 513,
                'retour_id' => 60,
                'question_id' => 9,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            439 => 
            array (
                'id' => 522,
                'retour_id' => 61,
                'question_id' => 10,
                'reponse_texte' => '',
                'reponsable_id' => 733,
                'reponsable_type' => 'lecon',
            ),
            440 => 
            array (
                'id' => 523,
                'retour_id' => 61,
                'question_id' => 11,
                'reponse_texte' => '',
                'reponsable_id' => 746,
                'reponsable_type' => 'lecon',
            ),
            441 => 
            array (
                'id' => 524,
                'retour_id' => 61,
                'question_id' => 12,
                'reponse_texte' => '',
                'reponsable_id' => 64,
                'reponsable_type' => 'developpement',
            ),
            442 => 
            array (
                'id' => 530,
                'retour_id' => 61,
                'question_id' => 15,
            'reponse_texte' => 'Un des membres du Jury avait l\'air de comprendre tout ce que je disais, du coup quand les autres comprenaient pas une démonstration ils lui demandaient "tu as compris?" il disait oui et on passait à autre chose, c\'était un peu le boss final de l\'oral. Un autre m\'a un peu énervé (le prof de prépa je pense) parce qu\'il posait mal ses questions et je comprenais pas ce qu\'il voulait dire... Par exemple à un moment dans mon plan je parlais du lien entre la fonction gamma et la surface d\'une sphère, et donc j\'introduit une mesure (cf Faraut) définie en fonction de la mesure de Lebesgue (pour info, si $\\lambda$ est la mesure de lebesgue, la nouvelle mesure c\'est $ \\sigma(E)=\\lim\\limits_{\\epsilon\\rightarrow 0}\\frac{1}{\\epsilon}\\lambda(\\{ru | u\\in E, 1 \\leq r \\leq 1+\\epsilon\\})$). Je sais pas ce qu\'il a pas aimé la dedans, mais il trouvait cette mesure bizarre (alors que le boss avait l\'air d\'accord avec ce que je disais) et il m\'a parlé de ça pendant longtemps, ça a un peu cassé le rythme de l\'oral.

Quand le boss a enfin pu me poser des questions il avait l\'air très content, il souriait et je l\'ai même fait rigoler. Jury plutot agréable dans l\'ensemble, à aucun moment ils n\'ont cherché à me déstabiliser ou à me piéger.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            443 => 
            array (
                'id' => 531,
                'retour_id' => 61,
                'question_id' => 17,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            444 => 
            array (
                'id' => 532,
                'retour_id' => 61,
                'question_id' => 18,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            445 => 
            array (
                'id' => 553,
                'retour_id' => 62,
                'question_id' => 46,
                'reponse_texte' => 'EDO, EDP, différences finies, interpolation',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            446 => 
            array (
                'id' => 554,
                'retour_id' => 62,
                'question_id' => 47,
                'reponse_texte' => 'methodes de gradient, algebre lineaire',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            447 => 
            array (
                'id' => 555,
                'retour_id' => 62,
                'question_id' => 48,
            'reponse_texte' => 'C\'était sur les problèmes de contrôle : on prend un système différentiel (à une ou plusieurs variables) qui résulte d\'équations de la physique (on impose une force sur un système), qui vérifie des propriété d\'existence et unicité de solutions en fonction de la force et des conditions initiales. On fait le problème inverse : quelle force imposer pour arriver à un état final choisi en un temps donné.
(je viens de voir que c\'est principalement une réécriture du sujet du concours d\'entrée à l\'ENS Math C)',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            448 => 
            array (
                'id' => 556,
                'retour_id' => 62,
                'question_id' => 49,
                'reponse_texte' => '- prouver trois résultats du texte : un résultat de densité, un résultat d\'existence/unicité à l\'aide de formes linéaires, et un résultat de projection orthogonale
- numérique : du matriciel (pour mon probleme de systeme lineaire), de la chaleur (explicite et implicite), et j\'ai commencé mais non terminé le tracé des solutions au problème 

J\'ai suivi le texte mais pas trop, et rajouté des maths.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            449 => 
            array (
                'id' => 557,
                'retour_id' => 62,
                'question_id' => 50,
                'reponse_texte' => '- montrer comment approximer une dérivée d\'ordre n
grand
- montrer un autre résultat de densité (avec des polynomes qui n\'ont que des termes de degré pair)
- montrer comment vous avez codé la résolution numérique de la chaleur, en particulier les conditions de Neumann (je l\'ai fait comme un sauvage mais j\'ai expliqué ce qu\'il aurait fallu faire)
- là vous inversez la matrice plutot que résoudre le systeme, pourquoi ?
- j\'avais parlé de conditionnement dans mon exposé, c\'est quoi le conditionnement ? comment l\'avais vous calculé ? l\'exprimer pour la norme 2, comment calculer ce rayon spectral numériquement ? (puissance), formellement ? (on ne peut pas, par le pouvoir de l\'algèbre !)
- la classique : énoncer Cauchy Lipschitz',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            450 => 
            array (
                'id' => 558,
                'retour_id' => 62,
                'question_id' => 51,
                'reponse_texte' => 'je ne sais pas, être plus réactif en début des questions ?',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            451 => 
            array (
                'id' => 559,
                'retour_id' => 62,
                'question_id' => 52,
                'reponse_texte' => '2 muets
2 enthousiastes',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            452 => 
            array (
                'id' => 560,
                'retour_id' => 62,
                'question_id' => 53,
                'reponse_texte' => 'comme prévu',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            453 => 
            array (
                'id' => 561,
                'retour_id' => 62,
                'question_id' => 54,
                'reponse_texte' => '17',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            454 => 
            array (
                'id' => 562,
                'retour_id' => 63,
                'question_id' => 10,
                'reponse_texte' => '',
                'reponsable_id' => 714,
                'reponsable_type' => 'lecon',
            ),
            455 => 
            array (
                'id' => 563,
                'retour_id' => 63,
                'question_id' => 11,
                'reponse_texte' => '',
                'reponsable_id' => 729,
                'reponsable_type' => 'lecon',
            ),
            456 => 
            array (
                'id' => 564,
                'retour_id' => 63,
                'question_id' => 12,
                'reponse_texte' => '',
                'reponsable_id' => 4,
                'reponsable_type' => 'developpement',
            ),
            457 => 
            array (
                'id' => 566,
                'retour_id' => 63,
                'question_id' => 15,
                'reponse_texte' => 'Bizarre. Beaucoup de questions de bases.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            458 => 
            array (
                'id' => 567,
                'retour_id' => 63,
                'question_id' => 17,
                'reponse_texte' => 'Pas content ... J\'ai donné mauvaise impression au début et j\'ai jamais vraiment eu le temps de remonter.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            459 => 
            array (
                'id' => 568,
                'retour_id' => 63,
                'question_id' => 18,
                'reponse_texte' => '9.25',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            460 => 
            array (
                'id' => 569,
                'retour_id' => 64,
                'question_id' => 1,
                'reponse_texte' => '',
                'reponsable_id' => 695,
                'reponsable_type' => 'lecon',
            ),
            461 => 
            array (
                'id' => 570,
                'retour_id' => 64,
                'question_id' => 2,
                'reponse_texte' => '',
                'reponsable_id' => 671,
                'reponsable_type' => 'lecon',
            ),
            462 => 
            array (
                'id' => 572,
                'retour_id' => 64,
                'question_id' => 6,
                'reponse_texte' => 'Le plus neutre du monde possible.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            463 => 
            array (
                'id' => 573,
                'retour_id' => 64,
                'question_id' => 8,
            'reponse_texte' => 'Je voulais les titiller avec Hahn Banach (j\'avais révisé la preuve) mais ils en ont pas parlé. 
Bon oral cependant je pense !',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            464 => 
            array (
                'id' => 574,
                'retour_id' => 64,
                'question_id' => 9,
                'reponse_texte' => '16.25',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            465 => 
            array (
                'id' => 575,
                'retour_id' => 65,
                'question_id' => 19,
                'reponse_texte' => '',
                'reponsable_id' => 727,
                'reponsable_type' => 'lecon',
            ),
            466 => 
            array (
                'id' => 576,
                'retour_id' => 65,
                'question_id' => 20,
                'reponse_texte' => '',
                'reponsable_id' => 704,
                'reponsable_type' => 'lecon',
            ),
            467 => 
            array (
                'id' => 577,
                'retour_id' => 65,
                'question_id' => 21,
                'reponse_texte' => '',
                'reponsable_id' => 79,
                'reponsable_type' => 'developpement',
            ),
            468 => 
            array (
                'id' => 635,
                'retour_id' => 65,
                'question_id' => 75,
                'reponse_texte' => '',
                'reponsable_id' => 88,
                'reponsable_type' => 'reference',
            ),
            469 => 
            array (
                'id' => 634,
                'retour_id' => 65,
                'question_id' => 75,
                'reponse_texte' => '',
                'reponsable_id' => 146,
                'reponsable_type' => 'reference',
            ),
            470 => 
            array (
                'id' => 633,
                'retour_id' => 65,
                'question_id' => 75,
                'reponse_texte' => '',
                'reponsable_id' => 60,
                'reponsable_type' => 'reference',
            ),
            471 => 
            array (
                'id' => 632,
                'retour_id' => 65,
                'question_id' => 75,
                'reponse_texte' => '',
                'reponsable_id' => 19,
                'reponsable_type' => 'reference',
            ),
            472 => 
            array (
                'id' => 583,
                'retour_id' => 65,
                'question_id' => 23,
            'reponse_texte' => '- Beaucoup de questions sur le développement : des hypothèses que je n\'avais pas précisées, quel type de convergence on a pour $S_n$ (convergence p.s.), cette convergence peut-elle être obtenue avec la loi forte des grands nombres, et même "A quoi sert votre développement ?" (j\'ai comparé avec Bienaymé-Tchebytchev et parlé d\'intervalle de confiance).

- L\'ensemble des points de discontinuité d\'une fonction monotone est dénombrable. Quid de l\'ensemble des points de non-dérivabilité ?
Je n\'avais aucune idée de la réponse, j\'ai parlé de la fonction continue partout dérivable nulle part, en me disant que ça pouvait laisser penser que c\'était plus délicat. (En fait il semblerait qu\'elle soit dérivable presque partout)

- $f$ dérivable est croissante ssi $f\' >0$. Une condition plus faible ? (Heuu...) Est-ce qu\'on peut remplacer dérivable par dérivable à droite et dérivée à droite positive ? En fait, elle est localement croissante à droite, du coup OK.

- Existe-t-il une inégalité de convexité avec plus de 2 éléments ? (je l\'avais oublié dans le plan...) Quelles sont les hypothèses et la preuve ? J\'ai d\'abord dis par associativité c\'est facile, ça suffisait au jury sauf à la personne qui a posé la question, du coup on isole $\\lambda_1 x_1$, on factorise artificiellement par $(1 - \\lambda_1)$ l\'autre terme (on évacue le cas où ça vaut zéro avant) puis récurrence. Elle m\'a demandé plusieurs fois si j\'étais sur, ce qui m\'a un peu déstabilisé, alors que c\'était bon.

- Exercice : démontrer le théorème de Dini (toute suite de fonctions croissantes définies sur un segment convergeant vers $f$ continue  converge en fait uniformément). Je ne me souvenais plus de la preuve, je dis qu\'utiliser Heine pouvait être intéressant, je tente quelques trucs. Pas le temps de finir.
',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            473 => 
            array (
                'id' => 584,
                'retour_id' => 65,
                'question_id' => 24,
            'reponse_texte' => 'La dame qui m\'a acceuilli dans la salle a été un peu pénible pendant l\'oral, elle me demandait sans cesse les hypothèses et semblait agacée (ça se comprend car souvent j\'en oubliais ou je ne savais pas, par exemple la LGN) (Remarque : à la vue de la note, ils étaient effectivement agacés). Les deux autres membres du jruy étaient plutôt bienveillants et me laissaient réfléchir.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            474 => 
            array (
                'id' => 585,
                'retour_id' => 65,
                'question_id' => 26,
                'reponse_texte' => 'Tableau grand à feutres, la préparation ne dure pas trois heures le premier jour, car il faut le temps de comprendre l\'organisation et de se repérer, du coup il faut se préparer à ce genre de trucs.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            475 => 
            array (
                'id' => 586,
                'retour_id' => 65,
                'question_id' => 27,
                'reponse_texte' => '10.25',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            476 => 
            array (
                'id' => 592,
                'retour_id' => 66,
                'question_id' => 28,
                'reponse_texte' => '',
                'reponsable_id' => 769,
                'reponsable_type' => 'lecon',
            ),
            477 => 
            array (
                'id' => 593,
                'retour_id' => 66,
                'question_id' => 29,
                'reponse_texte' => '',
                'reponsable_id' => 766,
                'reponsable_type' => 'lecon',
            ),
            478 => 
            array (
                'id' => 594,
                'retour_id' => 66,
                'question_id' => 30,
                'reponse_texte' => '',
                'reponsable_id' => 217,
                'reponsable_type' => 'developpement',
            ),
            479 => 
            array (
                'id' => 638,
                'retour_id' => 66,
                'question_id' => 76,
                'reponse_texte' => '',
                'reponsable_id' => 217,
                'reponsable_type' => 'reference',
            ),
            480 => 
            array (
                'id' => 598,
                'retour_id' => 66,
                'question_id' => 32,
                'reponse_texte' => 'Quelques questions sur le développement :
- Une règle de conséquence que j\'avais oublié d\'appliquer, vite rectifié
- La logique de Hoare est-elle adaptée pour l\'automatisation (j\'avais un peu parlé de ça en conclusion du développement). Je dis que le problème est surtout de deviner certaines assertions, pour ça on a les weakest prediconditions (wp).
- Des exemples de wp ? Je parle de l\'assignation, on m\'a suggéré de regarder la séquence, et c\'est aussi facile.
- Est-ce que la logique de Hoare prouve la correction ? Non, seulement la correction partielle.

Sur le plan :
- Vous parlez d\'ensemble bien fondé mais uniquement dans le cas des fonctions récursives... Non en fait pour les algos itératifs c\'est pareil, juste un peu de maladresse dans mon plan. Exemple d\'ensemble bien fondé ? (j\'avais mis $\\mathbb{N}^2$ muni de l\'ordre lexicographique dans mon plan...) Un exemple de terminaison de programme itératif qui l\'utilise ? Je dis qu\'on peut réécrire un programme récursif que j\'avais donné (Binôme de Newton) en mode itératif, j\'ai un peu galéré alors que c\'était assez con...
- La terminaison de programme est un problème difficile (je donnais l\'exemple de la conjecture de Syracuse).  Est-ce que vous connaissez des problèmes dont la correction est arbitrairement difficile ? J\'avoue que le "arbitrairement" m\'a pas mal dérouté, j\'avais aucune idée d\'où il voulait m\'emmener, du coup on est passé à autre chose.
- Vous connaissez Dijkstra ? (sérieusement ?) Preuve de la correction ? J\'explique qu\'à l\'étape $i$ on a la longueur du plus court chemin passant par au plus $i$ sommets, ça n\'avait pas l\'air d\'avoir convaincu... Ils voulaient un invariant, c\'est assez délicat et eux même n\'étaient pas d\'accord... On y réfléchit un peu ensemble.

Exercices :
- On définit une fonction $f$ de la façon suivante :
$$f(x) = 
\\begin{cases} 
x - 10 & \\text{ si } x > 42 \\\\
f(f(x+11)) & \\text{ sinon}
\\end{cases}
$$
Montrer que $f$ termine. On me demande rapidement de calculer $f(42)$, $f(41)$, j\'ai montré que si $x \\leq 42$, $f(x) = 33$ en raisonnant par "tranches". A la fin, un des membres du jury m\'a aussi suggéré une récurrence forte, qui aurait été plus simple c\'est vraie...

- On prend une matrice $n \\times n$ triée par ligne et par colonne. Algorithme de recherche d\'un élément $x$ en temps linéaire ? Comment le prouver ? Là j\'ai expliqué quelle info on pouvait avoir en comparant $x$ à un élément de la matrice, mais il ne me restait plus beaucoup de temps.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            481 => 
            array (
                'id' => 599,
                'retour_id' => 66,
                'question_id' => 35,
            'reponse_texte' => 'Jury très sympathique, bienveillant et sollicitant. Il y avait Laurent Cheno (Inspecteur Général), Judicaël Courant, et une femme (celle qui m\'a acceuillie) dont j\'ai oublié le nom.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            482 => 
            array (
                'id' => 600,
                'retour_id' => 66,
                'question_id' => 34,
            'reponse_texte' => 'Je pense qu\'ils ont apprécié que je parle de logique de Hoare (c\'est assez suggéré dans le rapport) et surtout que je sois capable de discuter dessus après (j\'avais pas mal potassé aussi pendant la préparation).',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            483 => 
            array (
                'id' => 601,
                'retour_id' => 66,
                'question_id' => 36,
                'reponse_texte' => '20',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            484 => 
            array (
                'id' => 637,
                'retour_id' => 66,
                'question_id' => 31,
                'reponse_texte' => '',
                'reponsable_id' => 192,
                'reponsable_type' => 'developpement',
            ),
            485 => 
            array (
                'id' => 609,
                'retour_id' => 67,
                'question_id' => 64,
                'reponse_texte' => 'Triangulation parfaite',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            486 => 
            array (
                'id' => 610,
                'retour_id' => 67,
                'question_id' => 65,
                'reponse_texte' => 'Un truc sur les réseaux sociaux',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            487 => 
            array (
                'id' => 611,
                'retour_id' => 67,
                'question_id' => 66,
            'reponse_texte' => 'On s\'intéressait ici au problème de triangulation parfaite (le nom du sujet donné à la triangulation de Delaunay...). Une partie était plutôt théorique : on définissait mathématiquement une triangulation, celle de Delaunay et l\'équivalence entre Delaunay (tous les disques ouverts circonscrits aux triangles ne contiennent aucun point) et un critère d\'optimalité (on maximisait l\'ensemble des angles des triangles triés avec l\'ordre lexicographique). Dans la deuxième partie, on étudiait l\'aspect algorithmique avec l\'algorithme incrémental classique, où quand on insère un point, on supprime les triangles qui ne respectent pas la règle de Delaunay puis on retriangularise.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            488 => 
            array (
                'id' => 612,
                'retour_id' => 67,
                'question_id' => 67,
                'reponse_texte' => 'Mon plan :
Introduction
I] Définitions et étude mathématique
a) Triangulation
b) Triangulation optimale
c) Quelques propriétés (i.e. nombre d\'arètes et de triangles)

II] Aspect algorithmique
a) Borne inférieure
b) Algorithme incrémental
MODELISATION
c) Complexité (évaluation du nombre de triangles ajoutés et du nombre de tests d\'appartenance d\'un point au disque circonscrit)
d) Structure de données (là je parle de comment on peut faire en pratique pour supprimer un triangle, et plus généralement pour représenter une triangulation)
Par rapport au texte, j\'ai expliqué l\'existance et l\'unicité d\'une triangulation de Delaunay, j\'ai démontré la formule donnant le nombre d\'arêtes et de triangles dans une triangulation quelconque, je montre la borne inférieure en $n \\cdot \\text{log}(n)$ (l\'idée de l\'exemple était donné) et la complexité pire cas de l\'algo incrémental.
Ma dernière sous-partie n\'apparaissait pas dans le texte, j\'y parle d\'halfedge (une structure pour pouvoir représenter et parcourir une triangulation de façon compacte et simple).',
'reponsable_id' => 0,
'reponsable_type' => '',
),
489 => 
array (
'id' => 613,
'retour_id' => 67,
'question_id' => 68,
'reponse_texte' => '- Vous n\'avez pas traité exactement l\'exercice de modélisation". En effet, j\'avais commencé dans la bonne direction, mais j\'ai zappé le but de l\'exo de modélisation (on voulait un algo qui dit si un polygone est convexe). Du coup j\'explique un algo auquel j\'avais pensé (on regarde les orientations successives), on me fait demander la complexité (linéaire), alors qu\'une indication suggérait un algo quadratique. Du coup j\'exibe un contre exemple ou ce que je voulais faire ne marchait pas.
- Une question sur l\'utilisation des float en Caml (je faisais notamment une comparaison par rapport à 0.). Le but était de discuter sur les problèmes d\'arrondis, si on multiplie deux nombres petits on peut avoir un problème sur le représentation en machine, et quand on fait petit fois grand on peut avoir des soucis si le nombre trop petit à des erreurs d\'arrondis, amplifiée quand on multiplie. 
- Evidemment la question qui suit.... Comment on peut éviter ça ? Bah comme on ne fait pas que des opérations type multiplication/divisions, on peut représenter les entiers sur lesquels on travaille par des fractions, du coup on peut faire du calcul exact. L\'inconvénient est que pareil, quand on fait des opérations sur deux nombres rationnels, le dénominateur peut doubler de taille à chaque opération, et du coup sortir de la précision de l\'ordinateur si on fait trop d\'opérations. La personne du jury qui m\'a posé cette question était visiblement satisfaite.
- On discute longuement autour d\'un argument que j\'ai proposé à l\'oral pour une preuve qui ne convenait pas ici (pour l\'unicité j\'utilisais un argument du type "Si on a deux solutions différentes, on peut s\'arranger pour transformer l\'une vers l\'autre" alors que ce n\'était pas possible ici). Du coup ici, il fallait utiliser l\'autre caractérisation de Delaunay sur les angles triés dont je n\'ai pas parlé, qui là fournit un critère d\'optimisation. J\'ai eu une autre question du même genre mais je ne m\'en souviens plus.
- Dans l\'exemple pour la complexité pire cas, on place des points sur une parabole. Vous devinez la question... "Bah pourquoiiiii ?" Là j\'ai dit que ce qui était important, c\'est d\'avoir une courbe telle que la triangulation de Delaunay permettait d\'obtenir la liste des éléments triés facilement, et du coup comme on sait que le tri c\'est au moins $n \\cdot \\text{log}(n)$, c\'est gagné.

* Ensuite : "Mais pourquoi c\'est au moins $n \\cdot \\text{log}(n)$ le tri?" Déjà c\'est le tri par comparaison, ensuite le truc classique : arbre de décision, lien feuille/ordre des éléments, la hauteur c\'est le pire cas de l\'algo, et le lien entre hauteur et feuilles nous donne le résultat.
- Un des jurys voulait que je réexplique les halfedges, et surtout comment on obtient les triangles. Je dis que voir les somets d\'un triangle c\'est facile si on a une halfedge de ce triangle, par contre comment on la trouve... j\'y avais pas pensé. Du coup je dis que j\'avais vu cette structure dans un stage, et que je voulais montrer les idées essentielles.
',
'reponsable_id' => 0,
'reponsable_type' => '',
),
490 => 
array (
'id' => 614,
'retour_id' => 67,
'question_id' => 69,
'reponse_texte' => 'J\'aurai du faire plus attention à l\'exercice de modélisation demandé, surtout que ce n\'était pas bien dur et il y avait une indication ! Après c\'était le dernier jour et j\'étais pas mal fatigué. Mais des points vraiment perdus pour rien, alors que c\'était un sujet que je dominais suffisamment je pense...
Peut-être aussi faire une "vraie" démonstration mathématique, mais question temps j\'étais tout juste et je voulais pas raccourcir davantage.',
'reponsable_id' => 0,
'reponsable_type' => '',
),
491 => 
array (
'id' => 615,
'retour_id' => 67,
'question_id' => 70,
'reponse_texte' => 'Un des membres du jury monopolisait plus la parole, un autre dormait les 3/4 du temps, le troisième était grognon parce que je n\'ai pas fait de démonstration mathématique "pure et dure" (c\'est le seul dont je me souvienne du nom : Hubert Comon, ses thèmes de recherche sont réécriture et vérification... tu n\'étonnes qu\'il aime bien les démonstrations !). Enfin le dernier était sympathique et visiblement très content de mes réponses.',
'reponsable_id' => 0,
'reponsable_type' => '',
),
492 => 
array (
'id' => 616,
'retour_id' => 67,
'question_id' => 71,
'reponse_texte' => '',
'reponsable_id' => 0,
'reponsable_type' => '',
),
493 => 
array (
'id' => 617,
'retour_id' => 67,
'question_id' => 72,
'reponse_texte' => '15.25',
'reponsable_id' => 0,
'reponsable_type' => '',
),
494 => 
array (
'id' => 618,
'retour_id' => 68,
'question_id' => 1,
'reponse_texte' => '',
'reponsable_id' => 678,
'reponsable_type' => 'lecon',
),
495 => 
array (
'id' => 619,
'retour_id' => 68,
'question_id' => 2,
'reponse_texte' => '',
'reponsable_id' => 691,
'reponsable_type' => 'lecon',
),
496 => 
array (
'id' => 620,
'retour_id' => 68,
'question_id' => 3,
'reponse_texte' => '',
'reponsable_id' => 6,
'reponsable_type' => 'developpement',
),
497 => 
array (
'id' => 621,
'retour_id' => 68,
'question_id' => 5,
'reponse_texte' => 'Des questions sur le développement : préciser pourquoi vous prenait un corps de rupture de $Y^2-xY+1$ (pour ceux qui connaissent le développement), c\'était juste pour vérifier si j’avais compris ce que je faisais. Pourquoi Res(P ,Q) mod p = Res (P mod p, Q mod p), j’ai dit que c’était parce que je résultant passait aux morphismes mais pourquoi je ne savais pas exactement.
Le boss me dit : - C’est quoi un résultant ?
- Un déterminant, et le déterminant mod p c’est le déterminant des coeff mod p.
Le boss : ok ça me va (même si l’autre juré souhaitait un peu plus de détails je crois)

Questions sur le plan :
- Pourquoi $X^3+X+1$ est irréductible sur Q(i) ?
- Le polynôme est irréductible sur Q car pas de racine (s’il a une racine, on vérifie les hypothèse de divisibilité comme d’hab), puis on dessine la tour d’extension et comme P est de degré 3 et Q(i)/Q de degré 2 c’est bon (en détaillant un peu plus).

- Connaissez-vous un générateur de Q($\\sqrt 2, \\sqrt 3$) ?
- Oui $\\sqrt 2 + \\sqrt 3$ et je dois donner la preuve.

- Pouvez-vous montrer qu’il existe un polynôme irréductible de tout degré dans Fq ?
- Elément primitif car Fq* est cyclique puis le polynôme minimal d’un générateur convient.

- Montrer que $X^4+1$ est réductible sur tout Fp
Ça je m’en souvenais plus alors que c’était écrit dans le  Perrin, que j’avais utilisé pour quasiment tout le plan, je m’en suis voulu. J’ai un peu galéré, j’ai essayé pour p=2, c’est Frobenius, puis pour p>2 il fallait juste trouver un racine dans Fp² donc un élément d’ordre 8 dans Fp²* qui existe car c’est un groupe cyclique d’ordre (p+1)(p-1), qui est divisible par 8.
- Et comme $X^4+1=(X^2+1)-2X^2$ ça vous dit quoi au niveau de la réciprocité quadratique ?
- 2 est un carré (je l’avais relu le matin quand j’avais revu mon développement)

Le boss reprend la main et ne la relâche plus :
- On revient à Q($\\sqrt 2, \\sqrt 3$)/Q, pouvez-vous me donner tous les générateurs ?
- Euh… j’ai écrit un élément $a+b \\sqrt 2 + c \\sqrt 3 + d \\sqrt 6$. Bon $a$ n’engendre pas grand-chose et on ne peut pas avoir deux des coefficients parmi $b,c,d$ qui sont nuls.
Le boss : oui donc par exemple $b \\sqrt 2 + c \\sqrt 3$
J’ai dessiné la tour d’extensions, dit qu’il faudrait montrer que son polynôme minimal est de degré 4.
Le boss me parle de groupe de Galois, je baragaouine quelques trucs que je savais dessus et ça lui convient.

Le boss demande si ses collègues ont d’autres questions.
Un juré : Une dernière question, comment vous montrer que 1/x est constructible, si x l’est ?
- Alors c’est le théorème de Thalès, je fais un dessin, mais pas le bon (j’ai dessiné le droite reliant 1 à 1 et x à x ce qui n’est pas top…). Je leur dit que ça ne va pas trop ça. Les types sourient, le boss fait une petite blague (« oui ça c’est juste une règle de 3 »). Ils me disent que c’était pas la bonne droite à considérer. Ah oui il faut juste relier 1 à x, et voilà.',
'reponsable_id' => 0,
'reponsable_type' => '',
),
498 => 
array (
'id' => 622,
'retour_id' => 68,
'question_id' => 6,
'reponse_texte' => '3 types, un boss et deux autres. Le boss menait la plupart du temps la discussion mais les autres étaient aussi actifs (contrairement aux autres oraux). Jury dynamique et sympa (l’oral s’est bien déroulé, ça doit aider).',
'reponsable_id' => 0,
'reponsable_type' => '',
),
499 => 
array (
'id' => 623,
'retour_id' => 68,
'question_id' => 8,
'reponse_texte' => 'Je pensais qu\'on me titillerait plus sur la réciprocité quadratique et le résultant mais en fait non (tant mieux).
En fait on a presque plus parlé de polynômes irréductibles que d\'extensions de corps (bon ok c\'est très lié). A part la dernière question, rien sur les nombres constructibles.

C\'était mon dernier oral et j\'avais été juste niveau temps lors du premier, donc là j\'ai essayé d\'être efficace pendant la préparation pour pouvoir relire mes développements.',
'reponsable_id' => 0,
'reponsable_type' => '',
),
));
        \DB::table('oraux_retours_reponses')->insert(array (
            0 => 
            array (
                'id' => 624,
                'retour_id' => 68,
                'question_id' => 9,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            1 => 
            array (
                'id' => 625,
                'retour_id' => 69,
                'question_id' => 10,
                'reponse_texte' => '',
                'reponsable_id' => 732,
                'reponsable_type' => 'lecon',
            ),
            2 => 
            array (
                'id' => 626,
                'retour_id' => 69,
                'question_id' => 11,
                'reponse_texte' => '',
                'reponsable_id' => 716,
                'reponsable_type' => 'lecon',
            ),
            3 => 
            array (
                'id' => 627,
                'retour_id' => 69,
                'question_id' => 12,
                'reponse_texte' => '',
                'reponsable_id' => 65,
                'reponsable_type' => 'developpement',
            ),
            4 => 
            array (
                'id' => 628,
                'retour_id' => 69,
                'question_id' => 14,
                'reponse_texte' => 'Questions sur le développement :
Ils m\'ont fait corriger les quelques erreurs que j\'avais écrites.
Détailler Fubini, préciser la définition du produit de convolution, pourquoi a-t-on convergence dans $L^p$ de $f*h_n$ vers $f$ (avec $h_n$ approximation de l’unité), puis pourquoi a-t-on la continuité de l’opérateur de translation (densité des $C_c$), enfin pourquoi si on converge dans $L^p$ on a une suite extraite qui converge pp.

Le probabiliste se réveille :
- Qu’est-ce que ce corollaire d’extraction dit au niveau des v.a.
Je n’ai pas tout de suite très bien compris ce qu’il voulait me faire dire, il m’a demandé alors les implications des différents modes de cv de va et j’ai répondu.
- Que peut-on dire en l’infini de la transformée de Fourier de $f$ ? Riemann Lebesgue.
- Est-ce vrai pour la transformée de Fourier d’une mesure ? Non avec les Dirac
- Quand est-ce vrai ? Là j’ai dit que je ne savais pas précisément, ça marche si on est absolument continue par rapport à Lebesgue (oui bon d’accord…) puis j’ai parlé du théorème de Lévy mais c’est pas vraiment ce qu’il attendait.
Le probabiliste se rendort.

Exercice : Calculer $\\int_0^\\infty \\frac{1}{1+x^n}dx$
J’ai dit qu’on pouvait utiliser des résidus (j’avais mis cette intégrale avec $n=4$ dans mon plan), puis ils m’ont fait chercher un contour, j’ai un peu galéré parce qu’on ne pouvait pas prendre le demi cercle supérieur comme je l’avais mis dans le plan, il fallait réduire en un domaine plus petit (type camembert) avec le bon angle, que j’ai galéré à trouver (alors que c’était juste $2 \\pi /n$…).

Exercice : le probabiliste se réveille à nouveau. On prend deux va normales centrées réduites indépendantes $X$ et $Y$, calculer la loi de $X/Y$.  J’ai dit qu’il fallait calculer $E[f(X/Y)]$ pour $f$ borélienne positive quelconque, faire un changement de variables. Je me suis un peu embrouillé avec l’intégrale, je ne savais plus si j’intégrer sur $\\mathbb R$ ou $\\mathbb R^2$, bref c’était pas très joli à voir, surtout que j’ai fini par écrire le changement de variables en oubliant de déterminant du jacobien...le boss me dit alors « et c’est tout », et je réponds, genre j’y avais pensé, non il faut le déterminant du jacobien. Pas eu le temps de finir.
',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            5 => 
            array (
                'id' => 629,
                'retour_id' => 69,
                'question_id' => 15,
                'reponse_texte' => 'Trois profs : un boss, une dame, un probabiliste.
Le boss menait la discussion, le probabiliste posait les questions de probas (eh oui !), la dame n\'a rien dit.
Jury très neutre.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            6 => 
            array (
                'id' => 630,
                'retour_id' => 69,
                'question_id' => 17,
            'reponse_texte' => 'Très stressé au début de l’oral (c’était mon premier), donc des erreurs sur le développement qu’ils m’ont fait corriger (il manquait une intégrale puis il y en avait une qui n’avait pas lieu d’être).
3h c\'est court !! J\'ai été un peu pris par le temps, donc je n\'ai pas eu le temps de relire mes développements...ce qui m\'aurait éviter plusieurs erreurs.
Suivez les conseils de Danthony !!!! J\'ai eu toutes la ribambelle de questions sur des preuves de convolution, j\'étais bien content de savoir y répondre avec les bons arguments dans l\'ordre. ',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            7 => 
            array (
                'id' => 631,
                'retour_id' => 69,
                'question_id' => 18,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            8 => 
            array (
                'id' => 641,
                'retour_id' => 70,
                'question_id' => 37,
                'reponse_texte' => 'Le texte parlait de nombres premiers dont on essayait de modéliser la répartition avec des Bernoulli.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            9 => 
            array (
                'id' => 642,
                'retour_id' => 70,
                'question_id' => 38,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            10 => 
            array (
                'id' => 643,
                'retour_id' => 70,
                'question_id' => 39,
            'reponse_texte' => 'On étudie la répartition des nombres premiers avec des Bernoulli indépendantes de paramètres $1/\\log (n)$. C\'était très mal expliqué dans le texte pourquoi on prenait ça. En fait, cela provenait de l\'équivalent entre le nombre d\'entiers premiers inférieurs à $x$ et $\\sum_{n=3}^x \\frac{1}{\\log (n)}$. Puis on testait si ce modèle est réaliste. En regardait s\'il donnait bien le caractère infini de l\'ensemble des nombres premiers (ok par Borel-Cantelli), s\'il donnait également quelques théorèmes de convergence (avec des martingales). Puis il était question de tester si l\'hypothèse d\'indépendance était pertinente et on voyait que non via une statistique un peu sortie du chapeau. Enfin, on testait l\'hypothèse de Riemann sur la répartition des nombres premiers et là ça collait à nouveau bien.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            11 => 
            array (
                'id' => 644,
                'retour_id' => 70,
                'question_id' => 40,
                'reponse_texte' => 'Mon plan :

1. Présentation du modèle probabiliste des nombres premiers
2. Premiers résultats.
3. Test de l\'hypothèse d\'indépendance.

Au niveau des simulations j\'ai présenté plusieurs graphiques qui montrait l\'adéquation entre le modèle probabiliste et le modèle théorique des nombres premiers. J\'ai aussi effectué un test, donc en gros j\'avais juste un résultat qui apparaissait.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            12 => 
            array (
                'id' => 645,
                'retour_id' => 70,
                'question_id' => 42,
                'reponse_texte' => 'Une question sur un calcul de variance qui était dégueulasse ! Dans le texte il était écrit qu\'elle valait 1, je l\'ai écrit rapidement au tableau mais ça n\'avait pas d\'utilité pour la suite. J\'aurais mieux fait de ne pas en parler, parce qu\'on a passé 10 minutes à calculer cette foutue variance !

Quelques questions sur le modèle et sa motivation, que je n\'avais pas bien expliqué, car à vrai dire je n\'avais pas tout saisi (finalement ça sert à quoi de modéliser l\'ensemble des nombres premiers par des Bernoulli ?)

Aucune question sur mes simulations. Je pense qu\'elles étaient pertinentes et représentaient bien ce qu\'il se passait.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            13 => 
            array (
                'id' => 646,
                'retour_id' => 70,
                'question_id' => 45,
                'reponse_texte' => 'J\'aurais dû beaucoup mieux motiver l\'exposé, je suis resté au niveau du texte donc en détaillant peu, ça m\'a valu beaucoup de questions pas très intéressantes...',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            14 => 
            array (
                'id' => 647,
                'retour_id' => 70,
                'question_id' => 41,
            'reponse_texte' => 'Deux hommes et deux femmes. Le premier type a géré quasiment tout l\'oral tout seul, une des deux dames intervenait de temps en temps (mais très rapidement). Le deuxième type a allumé le vidéoprojecteur quand j\'en avais besoin (et c\'est tout). La deuxième dame n\'a rien dit (d\'ailleurs je ne suis même plus sûr que c\'était une dame...).
Le type qui posait toutes les questions était assez nerveux, il me laissait très peu de temps pour réfléchir.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            15 => 
            array (
                'id' => 648,
                'retour_id' => 70,
                'question_id' => 43,
                'reponse_texte' => 'Mon ordi a bugué un moment mais un type est venu assez rapidement pour régler le problème.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            16 => 
            array (
                'id' => 649,
                'retour_id' => 70,
                'question_id' => 44,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            17 => 
            array (
                'id' => 650,
                'retour_id' => 71,
                'question_id' => 1,
                'reponse_texte' => '',
                'reponsable_id' => 671,
                'reponsable_type' => 'lecon',
            ),
            18 => 
            array (
                'id' => 651,
                'retour_id' => 71,
                'question_id' => 2,
                'reponse_texte' => '',
                'reponsable_id' => 687,
                'reponsable_type' => 'lecon',
            ),
            19 => 
            array (
                'id' => 652,
                'retour_id' => 71,
                'question_id' => 3,
                'reponse_texte' => '',
                'reponsable_id' => 24,
                'reponsable_type' => 'developpement',
            ),
            20 => 
            array (
                'id' => 661,
                'retour_id' => 72,
                'question_id' => 73,
                'reponse_texte' => '',
                'reponsable_id' => 129,
                'reponsable_type' => 'reference',
            ),
            21 => 
            array (
                'id' => 654,
                'retour_id' => 71,
                'question_id' => 5,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            22 => 
            array (
                'id' => 655,
                'retour_id' => 71,
                'question_id' => 6,
                'reponse_texte' => 'Gentil, met à l\'aise et est dans l\'optique d\'une discussion critique sur le plan.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            23 => 
            array (
                'id' => 656,
                'retour_id' => 71,
                'question_id' => 8,
            'reponse_texte' => 'Le temps de préparation est très court (2h30 en fait) !!! Je prévoyais de mettre les tables de caractères de groupes classiques en annexe mais n\'ai eu le temps que de dresser celle de S4 à la va vite... Je l\'ai signalé pendant ma défense de plan. Le jury m\'a demandé à l\'oral : "Si vous aviez fait les tables de caractères de H8 et D4, que pourriez-vous en dire ?" ce qui m\'a permis de parler à l\'oral de ce que je n\'avais pas eu le temps d\'écrire.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            24 => 
            array (
                'id' => 657,
                'retour_id' => 71,
                'question_id' => 9,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            25 => 
            array (
                'id' => 658,
                'retour_id' => 72,
                'question_id' => 1,
                'reponse_texte' => '',
                'reponsable_id' => 671,
                'reponsable_type' => 'lecon',
            ),
            26 => 
            array (
                'id' => 659,
                'retour_id' => 72,
                'question_id' => 2,
                'reponse_texte' => '',
                'reponsable_id' => 687,
                'reponsable_type' => 'lecon',
            ),
            27 => 
            array (
                'id' => 660,
                'retour_id' => 72,
                'question_id' => 3,
                'reponse_texte' => '',
                'reponsable_id' => 24,
                'reponsable_type' => 'developpement',
            ),
            28 => 
            array (
                'id' => 662,
                'retour_id' => 72,
                'question_id' => 5,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            29 => 
            array (
                'id' => 663,
                'retour_id' => 72,
                'question_id' => 6,
                'reponse_texte' => 'Gentil, met à l\'aise et est dans l\'optique d\'une discussion critique sur le plan. Un membre du jury n\'a pas beaucoup parlé...',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            30 => 
            array (
                'id' => 664,
                'retour_id' => 72,
                'question_id' => 8,
            'reponse_texte' => 'Le temps de préparation est très court (2h30 en fait) !!! Je prévoyais de mettre les tables de caractères de groupes classiques en annexe mais n\'ai eu le temps que de dresser celle de S4 à la va vite... Je l\'ai signalé pendant ma défense de plan. Le jury m\'a demandé à l\'oral : "Si vous aviez fait les tables de caractères de H8 et D4, que pourriez-vous en dire ?" ce qui m\'a permis de parler à l\'oral de ce que je n\'avais pas eu le temps d\'écrire.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            31 => 
            array (
                'id' => 665,
                'retour_id' => 72,
                'question_id' => 9,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            32 => 
            array (
                'id' => 666,
                'retour_id' => 73,
                'question_id' => 10,
                'reponse_texte' => '',
                'reponsable_id' => 748,
                'reponsable_type' => 'lecon',
            ),
            33 => 
            array (
                'id' => 667,
                'retour_id' => 73,
                'question_id' => 11,
                'reponse_texte' => '',
                'reponsable_id' => 716,
                'reponsable_type' => 'lecon',
            ),
            34 => 
            array (
                'id' => 668,
                'retour_id' => 73,
                'question_id' => 74,
                'reponse_texte' => '',
                'reponsable_id' => 123,
                'reponsable_type' => 'reference',
            ),
            35 => 
            array (
                'id' => 669,
                'retour_id' => 73,
                'question_id' => 14,
                'reponse_texte' => 'Le développement que j\'ai choisi est la ruine de joueur. Je me suis emmêlé les pinceaux à un endroit dans une erreur de calcul. J\'ai admis une partie du résultat pour avoir le temps de faire la suite. A la fin du développement, le jury m\'a demandé de corriger rapidement mon erreur d\'étourderie, ce que j\'ai fait en prenant un peu de recul sur le tableau.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            36 => 
            array (
                'id' => 670,
                'retour_id' => 73,
                'question_id' => 15,
            'reponse_texte' => 'Un membre du jury ne devait pas aimer les probas car n\'a pas parlé. Les deux autres ont posé pas mal de questions et des exercices. On m\'a demandé de démontrer le résultat du développement de Poissonisation (évènements rares) avec des indications. Je pense avoir mené une bonne démarche en traitant un cas particulier plus facile pour en déduire le cas général mais ai buggé dans la formule de Taylor-Lagrange avec reste intégral à l\'ordre 2 ce qui je crois a beaucoup déplu au jury ... Il faut surtout ne pas dire de bêtises sur des choses de bases du programme de prépa !!!',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            37 => 
            array (
                'id' => 671,
                'retour_id' => 73,
                'question_id' => 17,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            38 => 
            array (
                'id' => 672,
                'retour_id' => 73,
                'question_id' => 18,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            39 => 
            array (
                'id' => 673,
                'retour_id' => 74,
                'question_id' => 37,
                'reponse_texte' => 'Evolution du nombres d\'espèces animales en compétition dans un milieu et la survie des plus adaptés.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            40 => 
            array (
                'id' => 674,
                'retour_id' => 74,
                'question_id' => 38,
                'reponse_texte' => '',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            41 => 
            array (
                'id' => 675,
                'retour_id' => 74,
                'question_id' => 39,
            'reponse_texte' => 'En gros à chaque mutation, une nouvelle espèce apparaît avec une valeur (nombre entre 0 et 1) d\'adaptation au milieu, et à chaque extinction, l\'espèce avec la valeur d\'adaptation la plus faible est éliminée. On veut savoir la répartition asymptotique de la population en fonction de leur valeur d\'adaptation. On se rend compte que les espèces les moins adaptées disparaissent tandis que les espèces de grande valeur d\'adaptation survivent.',
                'reponsable_id' => 0,
                'reponsable_type' => '',
            ),
            42 => 
            array (
                'id' => 676,
                'retour_id' => 74,
                'question_id' => 40,
                'reponse_texte' => 'Plan :
I) Présentation du modèle, extinction et mutation.
II) Etude de la répartition asymptotique e la population en fonction de la viabilité
III) Conclusion et critique du modèle.',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
43 => 
array (
    'id' => 677,
    'retour_id' => 74,
    'question_id' => 42,
    'reponse_texte' => 'Pour afficher ses simulations à l\'écran, je devais retourner le tableau blanc et demander au jury d\'activer le rétroprojecteur. Malheureusement, on ne voyait pas les légendes es figures ainsi que la valeur des différents paramètres. J\'ai du les lires à l\'oral pour le jury qui a demandé en fin d\'oral de revenir sur les simulations et de commenter les résultats. Notamment d\'expliquer pourquoi la convergence met du temps à s\'établir.

On a bien sûr le droit à la fameuse question : "Comment trouveriez-vous un estimateur de ce paramètre ? Comment en détermineriez-vous un intervalle de confiance (asymptotique) ?" Question classique d\'application de la loi des grands nombres et du théorème de la limite centrale.',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
44 => 
array (
    'id' => 678,
    'retour_id' => 74,
    'question_id' => 45,
    'reponse_texte' => '',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
45 => 
array (
    'id' => 679,
    'retour_id' => 74,
    'question_id' => 41,
    'reponse_texte' => '',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
46 => 
array (
    'id' => 680,
    'retour_id' => 74,
    'question_id' => 43,
    'reponse_texte' => 'Les 4 heures de préparations étaient suffisantes pour se préparer. On est bien guidé par les encadrants préparateurs pour mettre en route Scilab et éventuellement trouver les textes de données si les textes s\'y prêtent.',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
47 => 
array (
    'id' => 681,
    'retour_id' => 74,
    'question_id' => 44,
    'reponse_texte' => '',
    'reponsable_id' => 0,
    'reponsable_type' => '',
),
));
        
        
    }
}
