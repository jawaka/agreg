<?php

use Illuminate\Database\Seeder;

class OrauxQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oraux_questions')->delete();
        
        \DB::table('oraux_questions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'oraux_type_id' => 1,
                'type_id' => 2,
                'label' => 'Leçon choisie :',
                'ordre' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'oraux_type_id' => 1,
                'type_id' => 2,
                'label' => 'Autre leçon :',
                'ordre' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'oraux_type_id' => 1,
                'type_id' => 6,
            'label' => 'Développement choisi : (par le jury)',
                'ordre' => 3,
            ),
            3 => 
            array (
                'id' => 4,
                'oraux_type_id' => 1,
                'type_id' => 7,
            'label' => 'Autre(s) développement(s) proposé(s):',
                'ordre' => 4,
            ),
            4 => 
            array (
                'id' => 5,
                'oraux_type_id' => 1,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 6,
            ),
            5 => 
            array (
                'id' => 6,
                'oraux_type_id' => 1,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) ?',
                'ordre' => 7,
            ),
            6 => 
            array (
                'id' => 8,
                'oraux_type_id' => 1,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            7 => 
            array (
                'id' => 9,
                'oraux_type_id' => 1,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
            8 => 
            array (
                'id' => 10,
                'oraux_type_id' => 2,
                'type_id' => 3,
                'label' => 'Leçon choisie :',
                'ordre' => 1,
            ),
            9 => 
            array (
                'id' => 11,
                'oraux_type_id' => 2,
                'type_id' => 3,
                'label' => 'Autre leçon :',
                'ordre' => 2,
            ),
            10 => 
            array (
                'id' => 12,
                'oraux_type_id' => 2,
                'type_id' => 6,
            'label' => 'Développement choisi : (par le jury)',
                'ordre' => 3,
            ),
            11 => 
            array (
                'id' => 13,
                'oraux_type_id' => 2,
                'type_id' => 7,
            'label' => 'Autre(s) développement(s) proposé(s) :',
                'ordre' => 4,
            ),
            12 => 
            array (
                'id' => 14,
                'oraux_type_id' => 2,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 6,
            ),
            13 => 
            array (
                'id' => 15,
                'oraux_type_id' => 2,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) ? ',
                'ordre' => 7,
            ),
            14 => 
            array (
                'id' => 76,
                'oraux_type_id' => 4,
                'type_id' => 8,
                'label' => 'Liste des références utilisées pour le plan :',
                'ordre' => 5,
            ),
            15 => 
            array (
                'id' => 17,
                'oraux_type_id' => 2,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            16 => 
            array (
                'id' => 18,
                'oraux_type_id' => 2,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
            17 => 
            array (
                'id' => 19,
                'oraux_type_id' => 3,
                'type_id' => 4,
                'label' => 'Leçon choisie :',
                'ordre' => 1,
            ),
            18 => 
            array (
                'id' => 20,
                'oraux_type_id' => 3,
                'type_id' => 4,
                'label' => 'Autre leçon :',
                'ordre' => 2,
            ),
            19 => 
            array (
                'id' => 21,
                'oraux_type_id' => 3,
                'type_id' => 6,
            'label' => 'Développement choisi : (par le jury)',
                'ordre' => 3,
            ),
            20 => 
            array (
                'id' => 22,
                'oraux_type_id' => 3,
                'type_id' => 7,
            'label' => 'Autre(s) développement(s) proposé(s) :',
                'ordre' => 4,
            ),
            21 => 
            array (
                'id' => 23,
                'oraux_type_id' => 3,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 6,
            ),
            22 => 
            array (
                'id' => 24,
                'oraux_type_id' => 3,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) ?',
                'ordre' => 7,
            ),
            23 => 
            array (
                'id' => 75,
                'oraux_type_id' => 3,
                'type_id' => 8,
                'label' => 'Liste des références utilisées pour le plan :',
                'ordre' => 5,
            ),
            24 => 
            array (
                'id' => 26,
                'oraux_type_id' => 3,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            25 => 
            array (
                'id' => 27,
                'oraux_type_id' => 3,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
            26 => 
            array (
                'id' => 28,
                'oraux_type_id' => 4,
                'type_id' => 5,
                'label' => 'Leçon choisie :',
                'ordre' => 1,
            ),
            27 => 
            array (
                'id' => 29,
                'oraux_type_id' => 4,
                'type_id' => 5,
                'label' => 'Autre leçon :',
                'ordre' => 2,
            ),
            28 => 
            array (
                'id' => 30,
                'oraux_type_id' => 4,
                'type_id' => 6,
            'label' => 'Développement choisi : (par le jury)',
                'ordre' => 3,
            ),
            29 => 
            array (
                'id' => 31,
                'oraux_type_id' => 4,
                'type_id' => 7,
            'label' => 'Autre(s) développement(s) proposé(s) :',
                'ordre' => 4,
            ),
            30 => 
            array (
                'id' => 32,
                'oraux_type_id' => 4,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 6,
            ),
            31 => 
            array (
                'id' => 74,
                'oraux_type_id' => 2,
                'type_id' => 8,
                'label' => 'Liste des références utilisées pour le plan :',
                'ordre' => 5,
            ),
            32 => 
            array (
                'id' => 34,
                'oraux_type_id' => 4,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            33 => 
            array (
                'id' => 35,
                'oraux_type_id' => 4,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) ?',
                'ordre' => 7,
            ),
            34 => 
            array (
                'id' => 73,
                'oraux_type_id' => 1,
                'type_id' => 8,
                'label' => 'Liste des références utilisées pour le plan :',
                'ordre' => 5,
            ),
            35 => 
            array (
                'id' => 36,
                'oraux_type_id' => 4,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
            36 => 
            array (
                'id' => 37,
                'oraux_type_id' => 5,
                'type_id' => 1,
                'label' => 'Sujet du texte choisi :',
                'ordre' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'oraux_type_id' => 5,
                'type_id' => 1,
                'label' => 'Sujet de l\'autre texte :',
                'ordre' => 2,
            ),
            38 => 
            array (
                'id' => 39,
                'oraux_type_id' => 5,
                'type_id' => 1,
                'label' => 'Un petit résumé du texte :',
                'ordre' => 3,
            ),
            39 => 
            array (
                'id' => 40,
                'oraux_type_id' => 5,
                'type_id' => 1,
            'label' => 'Qu\'avez vous produit durant la préparation ? (plan, code, dessins, preuves, ...)',
                'ordre' => 4,
            ),
            40 => 
            array (
                'id' => 41,
                'oraux_type_id' => 5,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) durant les questions ?',
                'ordre' => 7,
            ),
            41 => 
            array (
                'id' => 46,
                'oraux_type_id' => 6,
                'type_id' => 1,
                'label' => 'Sujet du texte choisi :',
                'ordre' => 1,
            ),
            42 => 
            array (
                'id' => 42,
                'oraux_type_id' => 5,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 5,
            ),
            43 => 
            array (
                'id' => 43,
                'oraux_type_id' => 5,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            44 => 
            array (
                'id' => 44,
                'oraux_type_id' => 5,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
            45 => 
            array (
                'id' => 45,
                'oraux_type_id' => 5,
                'type_id' => 1,
            'label' => 'Suite à la présentation, qu\'est ce qui vous semblait améliorable ? (plan, gestion du temps, choix des résultats présentés, ...)',
                'ordre' => 6,
            ),
            46 => 
            array (
                'id' => 47,
                'oraux_type_id' => 6,
                'type_id' => 1,
                'label' => 'Sujet de l\'autre texte :',
                'ordre' => 2,
            ),
            47 => 
            array (
                'id' => 48,
                'oraux_type_id' => 6,
                'type_id' => 1,
                'label' => 'Un petit résumé du texte :',
                'ordre' => 3,
            ),
            48 => 
            array (
                'id' => 49,
                'oraux_type_id' => 6,
                'type_id' => 1,
            'label' => 'Qu\'avez vous produit durant la préparation ? (plan, code, dessins, preuves, ...) ',
                'ordre' => 4,
            ),
            49 => 
            array (
                'id' => 50,
                'oraux_type_id' => 6,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 5,
            ),
            50 => 
            array (
                'id' => 51,
                'oraux_type_id' => 6,
                'type_id' => 1,
            'label' => 'Suite à la présentation, qu\'est ce qui vous semblait améliorable ? (plan, gestion du temps, choix des résultats présentés, ...) ',
                'ordre' => 6,
            ),
            51 => 
            array (
                'id' => 52,
                'oraux_type_id' => 6,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) durant les questions ?',
                'ordre' => 7,
            ),
            52 => 
            array (
                'id' => 53,
                'oraux_type_id' => 6,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            53 => 
            array (
                'id' => 54,
                'oraux_type_id' => 6,
                'type_id' => 9,
                'label' => 'Note',
                'ordre' => 9,
            ),
            54 => 
            array (
                'id' => 55,
                'oraux_type_id' => 7,
                'type_id' => 1,
                'label' => 'Sujet du texte choisi : ',
                'ordre' => 1,
            ),
            55 => 
            array (
                'id' => 56,
                'oraux_type_id' => 7,
                'type_id' => 1,
                'label' => 'Sujet de l\'autre texte :',
                'ordre' => 2,
            ),
            56 => 
            array (
                'id' => 57,
                'oraux_type_id' => 7,
                'type_id' => 1,
                'label' => 'Un petit résumé du texte :',
                'ordre' => 3,
            ),
            57 => 
            array (
                'id' => 58,
                'oraux_type_id' => 7,
                'type_id' => 1,
            'label' => 'Qu\'avez vous produit durant la préparation ? (plan, code, dessins, preuves, ...) ',
                'ordre' => 4,
            ),
            58 => 
            array (
                'id' => 59,
                'oraux_type_id' => 7,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 5,
            ),
            59 => 
            array (
                'id' => 60,
                'oraux_type_id' => 7,
                'type_id' => 1,
            'label' => 'Suite à la présentation, qu\'est ce qui vous semblait améliorable ? (plan, gestion du temps, choix des résultats présentés, ...)',
                'ordre' => 6,
            ),
            60 => 
            array (
                'id' => 61,
                'oraux_type_id' => 7,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) durant les questions ?',
                'ordre' => 7,
            ),
            61 => 
            array (
                'id' => 62,
                'oraux_type_id' => 7,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            62 => 
            array (
                'id' => 63,
                'oraux_type_id' => 7,
                'type_id' => 9,
                'label' => 'Note obtenue : ',
                'ordre' => 9,
            ),
            63 => 
            array (
                'id' => 64,
                'oraux_type_id' => 8,
                'type_id' => 1,
                'label' => 'Sujet du texte choisi :',
                'ordre' => 1,
            ),
            64 => 
            array (
                'id' => 65,
                'oraux_type_id' => 8,
                'type_id' => 1,
                'label' => 'Sujet de l\'autre texte :',
                'ordre' => 2,
            ),
            65 => 
            array (
                'id' => 66,
                'oraux_type_id' => 8,
                'type_id' => 1,
                'label' => 'Un petit résumé du texte : ',
                'ordre' => 3,
            ),
            66 => 
            array (
                'id' => 67,
                'oraux_type_id' => 8,
                'type_id' => 1,
            'label' => 'Qu\'avez vous produit durant la préparation ? (plan, code, dessins, preuves, ...) ',
                'ordre' => 4,
            ),
            67 => 
            array (
                'id' => 68,
                'oraux_type_id' => 8,
                'type_id' => 1,
            'label' => 'Résumé de l\'échange avec le jury (questions/réponses/remarques) :',
                'ordre' => 5,
            ),
            68 => 
            array (
                'id' => 69,
                'oraux_type_id' => 8,
                'type_id' => 1,
            'label' => 'Suite à la présentation, qu\'est ce qui vous semblait améliorable ? (plan, gestion du temps, choix des résultats présentés, ...) ',
                'ordre' => 6,
            ),
            69 => 
            array (
                'id' => 70,
                'oraux_type_id' => 8,
                'type_id' => 1,
            'label' => 'Quelle a été l\'attitude du jury (muet/aide/cassant) durant les questions ? ',
                'ordre' => 7,
            ),
            70 => 
            array (
                'id' => 71,
                'oraux_type_id' => 8,
                'type_id' => 1,
                'label' => 'L\'oral s\'est-il passé comme vous l\'imaginiez ou avez-vous été surpris par certains points ? Cette question concerne aussi la préparation.',
                'ordre' => 8,
            ),
            71 => 
            array (
                'id' => 72,
                'oraux_type_id' => 8,
                'type_id' => 9,
                'label' => 'Note obtenue :',
                'ordre' => 9,
            ),
        ));
        
        
    }
}
