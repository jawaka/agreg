<?php

use Illuminate\Database\Seeder;

class VersionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('versions')->delete();
        
        \DB::table('versions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-05 07:47:35',
                'remarque' => 'Cetter version est algorithmique -> en faire un nouveau développement ?',
                'versionable_id' => 1,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-03-26 13:37:21',
                'remarque' => '',
                'versionable_id' => 2,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 15:58:42',
            'remarque' => 'Pour présenter ce développement, il faut savoir prouver que $\\mathfrak{g}$ est bien un sous-espace vectoriel de $\\mathcal{M}_{n}(\\mathbb{R})$.',
                'versionable_id' => 3,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:00:10',
                'remarque' => 'euh réf ??',
                'versionable_id' => 4,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:02:23',
                'remarque' => '',
                'versionable_id' => 5,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:49:42',
                'remarque' => '',
                'versionable_id' => 6,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:06:06',
                'remarque' => '',
                'versionable_id' => 7,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:14',
                'remarque' => '',
                'versionable_id' => 8,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:08:31',
                'remarque' => '',
                'versionable_id' => 9,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:08:49',
                'remarque' => '',
                'versionable_id' => 10,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:09:34',
                'remarque' => '',
                'versionable_id' => 11,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:10:22',
                'remarque' => '',
                'versionable_id' => 12,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:12:54',
                'remarque' => '',
                'versionable_id' => 13,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-08 21:38:32',
                'remarque' => 'je suis certain de l\'avoir vu autre part : FGN ou Szpirglas',
                'versionable_id' => 14,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:15:10',
                'remarque' => '',
                'versionable_id' => 15,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:16:31',
                'remarque' => '',
                'versionable_id' => 16,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:17:59',
            'remarque' => 'On sépare l\'étude par nombre premiers. Pour $\\omega = e^{2i\\pi/p}$ où $p$ est premier. On montre qu\'il est constructible avec le théorème de Wantzel. Pour créer la tour d\'extension quadratique on trouve un générateur du groupe des automorphismes de $Q(\\omega)$.
le recasage c\'est Mathieu D. bien sûr',
                'versionable_id' => 17,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:19:03',
                'remarque' => '',
                'versionable_id' => 18,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:20:04',
                'remarque' => '',
                'versionable_id' => 19,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            19 => 
            array (
                'id' => 290,
                'created_at' => '2016-04-06 16:21:02',
                'updated_at' => '2016-05-15 10:50:02',
                'remarque' => '',
                'versionable_id' => 20,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:21:36',
                'remarque' => '',
                'versionable_id' => 21,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:23:15',
                'remarque' => '',
                'versionable_id' => 22,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:23:38',
                'remarque' => '',
                'versionable_id' => 23,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:24:19',
                'remarque' => '',
                'versionable_id' => 24,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:25:46',
                'remarque' => '',
                'versionable_id' => 25,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-14 14:12:52',
                'remarque' => '',
                'versionable_id' => 26,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            26 => 
            array (
                'id' => 346,
                'created_at' => '2016-10-04 14:25:26',
                'updated_at' => '2016-10-04 14:25:26',
                'remarque' => '',
                'versionable_id' => 673,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            27 => 
            array (
                'id' => 347,
                'created_at' => '2016-10-04 14:25:41',
                'updated_at' => '2016-10-04 14:25:41',
                'remarque' => '',
                'versionable_id' => 674,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            28 => 
            array (
                'id' => 27,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:28:06',
                'remarque' => '',
                'versionable_id' => 27,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            29 => 
            array (
                'id' => 28,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:28:34',
                'remarque' => '',
                'versionable_id' => 28,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            30 => 
            array (
                'id' => 29,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:29:54',
                'remarque' => '',
                'versionable_id' => 29,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            31 => 
            array (
                'id' => 30,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-06 16:30:46',
                'remarque' => '',
                'versionable_id' => 30,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            32 => 
            array (
                'id' => 31,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:08',
                'remarque' => '',
                'versionable_id' => 31,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            33 => 
            array (
                'id' => 32,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:28:41',
                'remarque' => 'what c quoi cette réf ?!',
                'versionable_id' => 32,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            34 => 
            array (
                'id' => 33,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:29:04',
                'remarque' => '',
                'versionable_id' => 33,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            35 => 
            array (
                'id' => 34,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:29:33',
                'remarque' => '',
                'versionable_id' => 34,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            36 => 
            array (
                'id' => 35,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:30:37',
                'remarque' => '',
                'versionable_id' => 35,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            37 => 
            array (
                'id' => 36,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:31:16',
                'remarque' => '',
                'versionable_id' => 36,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            38 => 
            array (
                'id' => 37,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:32:01',
                'remarque' => '',
                'versionable_id' => 37,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            39 => 
            array (
                'id' => 38,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:28',
                'remarque' => '',
                'versionable_id' => 38,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            40 => 
            array (
                'id' => 39,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:33:09',
                'remarque' => '',
                'versionable_id' => 39,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            41 => 
            array (
                'id' => 40,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:33:42',
                'remarque' => '',
                'versionable_id' => 40,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            42 => 
            array (
                'id' => 41,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:34:20',
                'remarque' => '',
                'versionable_id' => 41,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            43 => 
            array (
                'id' => 42,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:34:48',
                'remarque' => '',
                'versionable_id' => 42,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            44 => 
            array (
                'id' => 43,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:35:57',
                'remarque' => 'Aussi appelé théorème de représentation conforme.',
                'versionable_id' => 43,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            45 => 
            array (
                'id' => 44,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:37:10',
                'remarque' => '',
                'versionable_id' => 44,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            46 => 
            array (
                'id' => 45,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:38:19',
                'remarque' => '',
                'versionable_id' => 45,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            47 => 
            array (
                'id' => 46,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:34',
                'remarque' => '',
                'versionable_id' => 46,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            48 => 
            array (
                'id' => 342,
                'created_at' => '2016-10-04 11:39:34',
                'updated_at' => '2016-10-04 11:39:34',
                'remarque' => '',
                'versionable_id' => 670,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            49 => 
            array (
                'id' => 343,
                'created_at' => '2016-10-04 11:42:57',
                'updated_at' => '2016-10-04 11:42:57',
                'remarque' => '',
                'versionable_id' => 669,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            50 => 
            array (
                'id' => 344,
                'created_at' => '2016-10-04 11:45:41',
                'updated_at' => '2016-10-04 11:45:41',
                'remarque' => '',
                'versionable_id' => 671,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            51 => 
            array (
                'id' => 345,
                'created_at' => '2016-10-04 14:25:09',
                'updated_at' => '2016-10-04 14:25:09',
                'remarque' => '',
                'versionable_id' => 672,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            52 => 
            array (
                'id' => 47,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:45:17',
                'remarque' => '',
                'versionable_id' => 47,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            53 => 
            array (
                'id' => 339,
                'created_at' => '2016-10-04 11:32:23',
                'updated_at' => '2016-10-04 11:32:23',
                'remarque' => '',
                'versionable_id' => 667,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            54 => 
            array (
                'id' => 340,
                'created_at' => '2016-10-04 11:34:40',
                'updated_at' => '2016-10-04 11:34:40',
                'remarque' => '',
                'versionable_id' => 668,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            55 => 
            array (
                'id' => 341,
                'created_at' => '2016-10-04 11:38:42',
                'updated_at' => '2016-10-04 11:38:42',
                'remarque' => '',
                'versionable_id' => 669,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            56 => 
            array (
                'id' => 48,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:41:29',
                'remarque' => '',
                'versionable_id' => 48,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            57 => 
            array (
                'id' => 49,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:42:00',
                'remarque' => '',
                'versionable_id' => 49,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            58 => 
            array (
                'id' => 50,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-11 20:42:32',
                'remarque' => 'un fgn je crois',
                'versionable_id' => 50,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            59 => 
            array (
                'id' => 51,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:00:46',
                'remarque' => '',
                'versionable_id' => 51,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            60 => 
            array (
                'id' => 52,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:01:58',
                'remarque' => 'Convolution.

réf : brézis en anglais',
                'versionable_id' => 52,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            61 => 
            array (
                'id' => 53,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-10-13 20:33:37',
                'remarque' => 'La preuve faite dans le livre de Queffelec n\'est pas optimale, mais c\'est à peu près la seule référence. L\'adaptation au cas où $f$ est seulement $\\mathcal{C}^{1}$ vient d\'une idée de D. Monclair.






',
                'versionable_id' => 53,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            62 => 
            array (
                'id' => 54,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:04:43',
                'remarque' => 'Il est aussi dans Gourdon mais il y est proposé une application différente.

euh fgn aussi je crois',
                'versionable_id' => 54,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            63 => 
            array (
                'id' => 55,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:05:24',
                'remarque' => 'samuel ??',
                'versionable_id' => 55,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            64 => 
            array (
                'id' => 56,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:50',
                'remarque' => '',
                'versionable_id' => 56,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            65 => 
            array (
                'id' => 57,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:07:10',
                'remarque' => '',
                'versionable_id' => 57,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            66 => 
            array (
                'id' => 58,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:07:56',
                'remarque' => '',
                'versionable_id' => 58,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            67 => 
            array (
                'id' => 59,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-17 09:51:20',
                'remarque' => '',
                'versionable_id' => 59,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            68 => 
            array (
                'id' => 60,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:10:27',
                'remarque' => 'réf : un cours ...',
                'versionable_id' => 60,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            69 => 
            array (
                'id' => 61,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:11:27',
                'remarque' => 'ramis deschamps odoux',
                'versionable_id' => 61,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            70 => 
            array (
                'id' => 62,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:12:21',
                'remarque' => '',
                'versionable_id' => 62,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            71 => 
            array (
                'id' => 63,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:12:47',
                'remarque' => '',
                'versionable_id' => 63,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            72 => 
            array (
                'id' => 64,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:13:08',
                'remarque' => 'On cherche à montrer que $F^\\bot = 0$. On utilisera la transformée de Fourier et le principe du prolongement analytique pour qu\'une telle fonction est nulle.
',
                'versionable_id' => 64,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            73 => 
            array (
                'id' => 65,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:14:40',
                'remarque' => '',
                'versionable_id' => 65,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            74 => 
            array (
                'id' => 66,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:15:23',
                'remarque' => '',
                'versionable_id' => 66,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            75 => 
            array (
                'id' => 67,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:16:30',
                'remarque' => '',
                'versionable_id' => 67,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            76 => 
            array (
                'id' => 68,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:17:16',
                'remarque' => 'réf c\'est un peu classique de théorie de galois',
                'versionable_id' => 68,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            77 => 
            array (
                'id' => 69,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:17:44',
                'remarque' => '',
                'versionable_id' => 69,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            78 => 
            array (
                'id' => 70,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:19:16',
                'remarque' => '',
                'versionable_id' => 70,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            79 => 
            array (
                'id' => 71,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:50:55',
                'remarque' => '',
                'versionable_id' => 71,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            80 => 
            array (
                'id' => 72,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:20:23',
                'remarque' => '',
                'versionable_id' => 72,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            81 => 
            array (
                'id' => 73,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:21:16',
                'remarque' => 'poly de jp serre',
                'versionable_id' => 73,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            82 => 
            array (
                'id' => 75,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:21:40',
                'remarque' => '',
                'versionable_id' => 75,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            83 => 
            array (
                'id' => 76,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:01',
                'remarque' => 'C\'est la méthode de Newton multiD.',
                'versionable_id' => 76,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            84 => 
            array (
                'id' => 77,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:22:42',
                'remarque' => '',
                'versionable_id' => 77,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            85 => 
            array (
                'id' => 78,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:23:06',
                'remarque' => '',
                'versionable_id' => 78,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            86 => 
            array (
                'id' => 79,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:08',
                'remarque' => '',
                'versionable_id' => 79,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            87 => 
            array (
                'id' => 80,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:23:58',
                'remarque' => '',
                'versionable_id' => 80,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            88 => 
            array (
                'id' => 81,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-12 21:24:35',
                'remarque' => 'peut être ouvrad 1',
                'versionable_id' => 81,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            89 => 
            array (
                'id' => 82,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:08:25',
                'remarque' => '',
                'versionable_id' => 82,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            90 => 
            array (
                'id' => 83,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:18',
                'remarque' => '',
                'versionable_id' => 83,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            91 => 
            array (
                'id' => 84,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:11:01',
                'remarque' => '',
                'versionable_id' => 84,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            92 => 
            array (
                'id' => 85,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:24',
                'remarque' => '',
                'versionable_id' => 85,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            93 => 
            array (
                'id' => 86,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:12:05',
                'remarque' => '',
                'versionable_id' => 86,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            94 => 
            array (
                'id' => 87,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:12:45',
                'remarque' => '',
                'versionable_id' => 87,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            95 => 
            array (
                'id' => 88,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:13:56',
                'remarque' => 'réf pas sûr',
                'versionable_id' => 88,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            96 => 
            array (
                'id' => 89,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:14:23',
                'remarque' => '',
                'versionable_id' => 89,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            97 => 
            array (
                'id' => 90,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:15:00',
                'remarque' => '',
                'versionable_id' => 90,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            98 => 
            array (
                'id' => 91,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:15:58',
                'remarque' => '',
                'versionable_id' => 91,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            99 => 
            array (
                'id' => 92,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:16:31',
                'remarque' => '',
                'versionable_id' => 92,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            100 => 
            array (
                'id' => 93,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:17:17',
                'remarque' => '',
                'versionable_id' => 93,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            101 => 
            array (
                'id' => 94,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:17:42',
                'remarque' => '',
                'versionable_id' => 94,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            102 => 
            array (
                'id' => 95,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:18:17',
                'remarque' => '',
                'versionable_id' => 95,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            103 => 
            array (
                'id' => 96,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:18:48',
                'remarque' => '',
                'versionable_id' => 96,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            104 => 
            array (
                'id' => 97,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:19:22',
                'remarque' => '',
                'versionable_id' => 97,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            105 => 
            array (
                'id' => 98,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:19:55',
                'remarque' => '',
                'versionable_id' => 98,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            106 => 
            array (
                'id' => 99,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:21:00',
                'remarque' => '',
                'versionable_id' => 99,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            107 => 
            array (
                'id' => 274,
                'created_at' => '2016-04-06 15:47:07',
                'updated_at' => '2016-04-06 15:47:07',
                'remarque' => '',
                'versionable_id' => 100,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            108 => 
            array (
                'id' => 101,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:21:38',
                'remarque' => '',
                'versionable_id' => 101,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            109 => 
            array (
                'id' => 102,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:23:12',
                'remarque' => '',
                'versionable_id' => 102,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            110 => 
            array (
                'id' => 103,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:23:37',
                'remarque' => 'un fgn',
                'versionable_id' => 103,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            111 => 
            array (
                'id' => 104,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:30',
                'remarque' => 'La plus grosse partie de la preuve consiste à créer une bonne norme d\'opérateur, ce qui se fait en trigonalisant $A$.
',
                'versionable_id' => 104,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            112 => 
            array (
                'id' => 105,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:25:10',
                'remarque' => 'réf : RMSt',
                'versionable_id' => 105,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            113 => 
            array (
                'id' => 106,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:25:31',
                'remarque' => '',
                'versionable_id' => 106,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            114 => 
            array (
                'id' => 107,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:26:04',
                'remarque' => '',
                'versionable_id' => 107,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            115 => 
            array (
                'id' => 108,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:26:38',
                'remarque' => 'chambert loir',
                'versionable_id' => 108,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            116 => 
            array (
                'id' => 109,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:27:04',
                'remarque' => '',
                'versionable_id' => 109,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            117 => 
            array (
                'id' => 110,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-13 20:27:49',
                'remarque' => '',
                'versionable_id' => 110,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            118 => 
            array (
                'id' => 111,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:36',
                'remarque' => '',
                'versionable_id' => 111,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            119 => 
            array (
                'id' => 112,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:05:00',
                'remarque' => '',
                'versionable_id' => 112,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            120 => 
            array (
                'id' => 113,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:06:21',
                'remarque' => '',
                'versionable_id' => 113,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            121 => 
            array (
                'id' => 114,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:06:57',
                'remarque' => '',
                'versionable_id' => 114,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            122 => 
            array (
                'id' => 115,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:49',
                'remarque' => '',
                'versionable_id' => 115,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            123 => 
            array (
                'id' => 116,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:51:55',
                'remarque' => 'Par la théorie des $p$-Sylow + du dénombrement.
c\'est fait dans Szpirglas
',
                'versionable_id' => 116,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            124 => 
            array (
                'id' => 117,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:02',
                'remarque' => 'L\'équation fonctionnelle admise au début de ce développement peut être démontrée grâce à la formule sommatoire de Poisson.',
                'versionable_id' => 117,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            125 => 
            array (
                'id' => 118,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:06',
                'remarque' => '',
                'versionable_id' => 118,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            126 => 
            array (
                'id' => 119,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:19',
            'remarque' => 'Ce développement fournit un exemple de fonction continue et $2 \\pi$-périodique dont la série de Fourier diverge en $0$; L\'existence de telles fonctions peut aussi être prouvée (de manière non constructive) en utilisant Banach-Steinhaus.',
                'versionable_id' => 119,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            127 => 
            array (
                'id' => 120,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:09:52',
                'remarque' => '',
                'versionable_id' => 120,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            128 => 
            array (
                'id' => 121,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:24',
                'remarque' => '',
                'versionable_id' => 121,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            129 => 
            array (
                'id' => 122,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:30',
                'remarque' => 'Tient facilement en 10 min en écrivant avec les deux mains.',
                'versionable_id' => 122,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            130 => 
            array (
                'id' => 123,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:36',
            'remarque' => 'La preuve trouvée dans le Zuily, reprise ici, est fausse. Certains éléments peuvent être gardés, mais $\\mathcal{C}^{\\infty}(\\mathbb{R},S\'(\\mathbb{R}^{n}))$ ne s\'injecte pas dans $S\'(\\mathbb{R}^{n+1})$.',
                'versionable_id' => 123,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            131 => 
            array (
                'id' => 124,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:43',
                'remarque' => 'Le résultant entre les deux polynômes fait le lien entre les deux courbes.
C\'est fait dans Saux-Picard mais c\'est pas corrigé ...
',
                'versionable_id' => 124,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            132 => 
            array (
                'id' => 125,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:52:56',
                'remarque' => '',
                'versionable_id' => 125,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            133 => 
            array (
                'id' => 126,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:53:02',
                'remarque' => 'La version présente ici est un peu différente de celle proposée dans "Invitation aux formes quadratiques", car on ne regarde pas les prolongements d\'isométries, mais les simplifications possibles dans les isomorphismes d\'espaces quadratiques. Ces deux points de vue sont équivalents, mais la rédaction change un peu.















Ce développement rentre dans des leçons sur la congruence matricielle, quitte à le reformuler dans ces termes (ce qui peut changer la démonstration).',
                'versionable_id' => 126,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            134 => 
            array (
                'id' => 127,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:13:43',
                'remarque' => '',
                'versionable_id' => 127,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            135 => 
            array (
                'id' => 128,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:14:08',
                'remarque' => '',
                'versionable_id' => 128,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            136 => 
            array (
                'id' => 129,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:16:21',
                'remarque' => '',
                'versionable_id' => 129,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            137 => 
            array (
                'id' => 130,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:17:28',
            'remarque' => 'par récurrence sur le nombre de générateurs. Pour $L = K(a,b)$ on cherche un élément primitif de la forme $\\gamma = a + cb$ pour un bon choix de $c$.
pas de recasabilité mais facile',
                'versionable_id' => 130,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            138 => 
            array (
                'id' => 131,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:17:48',
                'remarque' => 'arithmétique de base + lemme chinois
',
                'versionable_id' => 131,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            139 => 
            array (
                'id' => 132,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:18:18',
                'remarque' => 'Beppo Levi + théorème des résidus + convergence dominée
',
                'versionable_id' => 132,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            140 => 
            array (
                'id' => 133,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:18:39',
                'remarque' => '',
                'versionable_id' => 133,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            141 => 
            array (
                'id' => 134,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:18:57',
            'remarque' => 'Transformée de Fourier dans $S\'(\\mathbb{R})$ + calcul intégrale par la formule des résidus + convolution.
',
                'versionable_id' => 134,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            142 => 
            array (
                'id' => 135,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:19:20',
                'remarque' => '',
                'versionable_id' => 135,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            143 => 
            array (
                'id' => 136,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:19:40',
                'remarque' => '',
                'versionable_id' => 136,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            144 => 
            array (
                'id' => 137,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:20:08',
                'remarque' => '',
                'versionable_id' => 137,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            145 => 
            array (
                'id' => 138,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:20:43',
                'remarque' => 'Recasage un peu abusif',
                'versionable_id' => 138,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            146 => 
            array (
                'id' => 139,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:22:26',
                'remarque' => '',
                'versionable_id' => 139,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            147 => 
            array (
                'id' => 140,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:22:55',
                'remarque' => 'je crois que c\'est fait dans peyré',
                'versionable_id' => 140,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            148 => 
            array (
                'id' => 141,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:23:29',
                'remarque' => '',
                'versionable_id' => 141,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            149 => 
            array (
                'id' => 142,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:23:59',
                'remarque' => '',
                'versionable_id' => 142,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            150 => 
            array (
                'id' => 143,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:24:19',
                'remarque' => 'Ressemble fort à un autre développement intitulé Bairavabilité.',
                'versionable_id' => 143,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            151 => 
            array (
                'id' => 144,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:24:45',
                'remarque' => '',
                'versionable_id' => 144,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            152 => 
            array (
                'id' => 145,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:25:17',
                'remarque' => '',
                'versionable_id' => 145,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            153 => 
            array (
                'id' => 146,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:25:39',
                'remarque' => '',
                'versionable_id' => 146,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            154 => 
            array (
                'id' => 147,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:25:56',
            'remarque' => 'C\'est un poil différent du développement des générateurs de $O(E)$ ...',
                'versionable_id' => 147,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            155 => 
            array (
                'id' => 148,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:26:33',
                'remarque' => '',
                'versionable_id' => 148,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            156 => 
            array (
                'id' => 149,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:26:56',
                'remarque' => '',
                'versionable_id' => 149,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            157 => 
            array (
                'id' => 150,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:27:32',
                'remarque' => '',
                'versionable_id' => 150,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            158 => 
            array (
                'id' => 151,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-16 08:28:30',
                'remarque' => '',
                'versionable_id' => 151,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            159 => 
            array (
                'id' => 152,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:17:56',
                'remarque' => '',
                'versionable_id' => 152,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            160 => 
            array (
                'id' => 153,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:19:23',
                'remarque' => 'réf FGN algèbre d\'habitude',
                'versionable_id' => 153,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            161 => 
            array (
                'id' => 154,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:19:42',
                'remarque' => '',
                'versionable_id' => 154,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            162 => 
            array (
                'id' => 155,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:20:09',
                'remarque' => '',
                'versionable_id' => 155,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            163 => 
            array (
                'id' => 156,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:20:47',
                'remarque' => '',
                'versionable_id' => 156,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            164 => 
            array (
                'id' => 157,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:21:42',
                'remarque' => '',
                'versionable_id' => 157,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            165 => 
            array (
                'id' => 158,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:53:07',
                'remarque' => '',
                'versionable_id' => 158,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            166 => 
            array (
                'id' => 159,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-05-15 10:53:13',
            'remarque' => 'Pas de référence pour le théorème de Kakutani. Il y en a sans doute pour le corollaire sur les sous-groupes compacts de $GL_{n}(\\mathbb{R})$.',
                'versionable_id' => 159,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            167 => 
            array (
                'id' => 160,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:23:11',
                'remarque' => '',
                'versionable_id' => 160,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            168 => 
            array (
                'id' => 161,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:23:45',
                'remarque' => 'fgn ?',
                'versionable_id' => 161,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            169 => 
            array (
                'id' => 162,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:24:29',
                'remarque' => '',
                'versionable_id' => 162,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            170 => 
            array (
                'id' => 163,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:25:01',
                'remarque' => '',
                'versionable_id' => 163,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            171 => 
            array (
                'id' => 246,
                'created_at' => '2016-03-29 15:01:46',
                'updated_at' => '2016-03-29 15:01:46',
                'remarque' => '',
                'versionable_id' => 164,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            172 => 
            array (
                'id' => 165,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:25:37',
                'remarque' => '',
                'versionable_id' => 165,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            173 => 
            array (
                'id' => 166,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:26:11',
                'remarque' => '',
                'versionable_id' => 166,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            174 => 
            array (
                'id' => 167,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-21 11:48:45',
                'remarque' => '',
                'versionable_id' => 167,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            175 => 
            array (
                'id' => 169,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-03-30 12:18:31',
                'remarque' => '',
                'versionable_id' => 169,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            176 => 
            array (
                'id' => 245,
                'created_at' => '2016-03-27 20:25:16',
                'updated_at' => '2016-03-27 20:25:16',
                'remarque' => '',
                'versionable_id' => 121,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            177 => 
            array (
                'id' => 258,
                'created_at' => '2016-03-30 11:55:32',
                'updated_at' => '2016-03-30 11:55:32',
                'remarque' => '',
                'versionable_id' => 171,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            178 => 
            array (
                'id' => 172,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:28:32',
                'remarque' => '',
                'versionable_id' => 172,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            179 => 
            array (
                'id' => 223,
                'created_at' => '2016-03-27 19:46:43',
                'updated_at' => '2016-03-27 19:46:43',
                'remarque' => '',
                'versionable_id' => 61,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            180 => 
            array (
                'id' => 174,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-03-30 12:21:01',
                'remarque' => '',
                'versionable_id' => 174,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            181 => 
            array (
                'id' => 273,
                'created_at' => '2016-04-06 15:46:08',
                'updated_at' => '2016-04-06 15:46:08',
                'remarque' => '',
                'versionable_id' => 176,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            182 => 
            array (
                'id' => 247,
                'created_at' => '2016-03-29 15:03:15',
                'updated_at' => '2016-03-29 15:03:15',
                'remarque' => '',
                'versionable_id' => 177,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            183 => 
            array (
                'id' => 235,
                'created_at' => '2016-03-27 20:09:34',
                'updated_at' => '2016-03-27 20:09:34',
                'remarque' => '',
                'versionable_id' => 21,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            184 => 
            array (
                'id' => 242,
                'created_at' => '2016-03-27 20:18:21',
                'updated_at' => '2016-03-27 20:18:21',
                'remarque' => '',
                'versionable_id' => 120,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            185 => 
            array (
                'id' => 272,
                'created_at' => '2016-04-06 15:45:03',
                'updated_at' => '2016-04-06 15:45:28',
                'remarque' => 'réf : fgn je sais pas lequel',
                'versionable_id' => 180,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            186 => 
            array (
                'id' => 181,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:31:05',
                'remarque' => '',
                'versionable_id' => 181,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            187 => 
            array (
                'id' => 182,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:31:48',
                'remarque' => '',
                'versionable_id' => 182,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            188 => 
            array (
                'id' => 183,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:34:44',
                'remarque' => 'et davenport ?
Analytic Methods for Diophantine Equations and Diophantine Inequalities
978-0521605830',
                'versionable_id' => 183,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            189 => 
            array (
                'id' => 184,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:35:07',
                'remarque' => '',
                'versionable_id' => 184,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            190 => 
            array (
                'id' => 185,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:35:32',
                'remarque' => '#craquage',
                'versionable_id' => 185,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            191 => 
            array (
                'id' => 186,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:35:54',
                'remarque' => '',
                'versionable_id' => 186,
                'versionable_type' => 'developpement',
                'user_id' => 108,
            ),
            192 => 
            array (
                'id' => 187,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:37:10',
                'remarque' => '',
                'versionable_id' => 187,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            193 => 
            array (
                'id' => 188,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:37:30',
                'remarque' => '',
                'versionable_id' => 188,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            194 => 
            array (
                'id' => 189,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:37:44',
                'remarque' => '',
                'versionable_id' => 189,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            195 => 
            array (
                'id' => 190,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:38:01',
                'remarque' => '',
                'versionable_id' => 190,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            196 => 
            array (
                'id' => 191,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:38:14',
                'remarque' => '',
                'versionable_id' => 191,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            197 => 
            array (
                'id' => 192,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:38:29',
                'remarque' => '',
                'versionable_id' => 192,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            198 => 
            array (
                'id' => 193,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:38:46',
                'remarque' => '',
                'versionable_id' => 193,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            199 => 
            array (
                'id' => 194,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:38:55',
                'remarque' => '',
                'versionable_id' => 194,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            200 => 
            array (
                'id' => 195,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:39:05',
                'remarque' => '',
                'versionable_id' => 195,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            201 => 
            array (
                'id' => 196,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:39:16',
                'remarque' => '',
                'versionable_id' => 196,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            202 => 
            array (
                'id' => 197,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:39:26',
                'remarque' => '',
                'versionable_id' => 197,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            203 => 
            array (
                'id' => 198,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:39:40',
                'remarque' => '',
                'versionable_id' => 198,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            204 => 
            array (
                'id' => 199,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:39:49',
                'remarque' => '',
                'versionable_id' => 199,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            205 => 
            array (
                'id' => 200,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:01',
                'remarque' => '',
                'versionable_id' => 200,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            206 => 
            array (
                'id' => 201,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:10',
                'remarque' => '',
                'versionable_id' => 201,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            207 => 
            array (
                'id' => 202,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:18',
                'remarque' => '',
                'versionable_id' => 202,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            208 => 
            array (
                'id' => 203,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:26',
                'remarque' => '',
                'versionable_id' => 203,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            209 => 
            array (
                'id' => 204,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:33',
                'remarque' => '',
                'versionable_id' => 204,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            210 => 
            array (
                'id' => 205,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:41',
                'remarque' => '',
                'versionable_id' => 205,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            211 => 
            array (
                'id' => 206,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:50',
                'remarque' => '',
                'versionable_id' => 206,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            212 => 
            array (
                'id' => 207,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:40:59',
                'remarque' => '',
                'versionable_id' => 207,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            213 => 
            array (
                'id' => 208,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:41:09',
                'remarque' => '',
                'versionable_id' => 208,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            214 => 
            array (
                'id' => 209,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:41:18',
                'remarque' => '',
                'versionable_id' => 209,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            215 => 
            array (
                'id' => 210,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:41:28',
                'remarque' => '',
                'versionable_id' => 210,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            216 => 
            array (
                'id' => 211,
                'created_at' => '2016-03-17 09:49:54',
                'updated_at' => '2016-04-18 16:41:35',
                'remarque' => '',
                'versionable_id' => 211,
                'versionable_type' => 'developpement',
                'user_id' => 111,
            ),
            217 => 
            array (
                'id' => 212,
                'created_at' => '2016-03-24 17:29:42',
                'updated_at' => '2016-03-26 13:33:06',
                'remarque' => '',
                'versionable_id' => 89,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            218 => 
            array (
                'id' => 213,
                'created_at' => '2016-03-24 17:36:12',
                'updated_at' => '2016-03-26 13:36:11',
                'remarque' => 'pas sûr pour la 2eme ref',
                'versionable_id' => 212,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            219 => 
            array (
                'id' => 214,
                'created_at' => '2016-03-24 17:38:26',
                'updated_at' => '2016-03-27 19:27:06',
                'remarque' => 'réf RMS ?',
                'versionable_id' => 105,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            220 => 
            array (
                'id' => 215,
                'created_at' => '2016-03-24 17:40:35',
                'updated_at' => '2016-03-27 19:27:39',
                'remarque' => 'énoncé dans Saux Picart mais pas de preuve',
                'versionable_id' => 124,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            221 => 
            array (
                'id' => 216,
                'created_at' => '2016-03-24 17:43:58',
                'updated_at' => '2016-03-27 19:28:12',
                'remarque' => 'internet',
                'versionable_id' => 48,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            222 => 
            array (
                'id' => 217,
                'created_at' => '2016-03-24 17:52:27',
                'updated_at' => '2016-03-27 19:29:00',
                'remarque' => '',
                'versionable_id' => 85,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            223 => 
            array (
                'id' => 218,
                'created_at' => '2016-03-27 19:30:12',
                'updated_at' => '2016-03-27 19:30:12',
                'remarque' => '',
                'versionable_id' => 60,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            224 => 
            array (
                'id' => 219,
                'created_at' => '2016-03-27 19:32:25',
                'updated_at' => '2016-03-27 19:32:25',
                'remarque' => '',
                'versionable_id' => 4,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            225 => 
            array (
                'id' => 220,
                'created_at' => '2016-03-27 19:36:58',
                'updated_at' => '2016-03-27 19:36:58',
                'remarque' => '',
                'versionable_id' => 30,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            226 => 
            array (
                'id' => 221,
                'created_at' => '2016-03-27 19:43:31',
                'updated_at' => '2016-03-27 19:43:31',
                'remarque' => '',
                'versionable_id' => 181,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            227 => 
            array (
                'id' => 222,
                'created_at' => '2016-03-27 19:44:42',
                'updated_at' => '2016-03-27 19:44:42',
                'remarque' => '',
                'versionable_id' => 173,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            228 => 
            array (
                'id' => 224,
                'created_at' => '2016-03-27 19:48:31',
                'updated_at' => '2016-03-27 19:48:31',
                'remarque' => '',
                'versionable_id' => 88,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            229 => 
            array (
                'id' => 225,
                'created_at' => '2016-03-27 19:49:22',
                'updated_at' => '2016-03-27 19:49:22',
                'remarque' => '',
                'versionable_id' => 7,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            230 => 
            array (
                'id' => 226,
                'created_at' => '2016-03-27 19:50:12',
                'updated_at' => '2016-03-27 19:50:12',
                'remarque' => '',
                'versionable_id' => 153,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            231 => 
            array (
                'id' => 227,
                'created_at' => '2016-03-27 19:51:38',
                'updated_at' => '2016-03-27 19:51:38',
                'remarque' => '',
                'versionable_id' => 12,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            232 => 
            array (
                'id' => 228,
                'created_at' => '2016-03-27 19:53:08',
                'updated_at' => '2016-03-27 19:53:08',
                'remarque' => '',
                'versionable_id' => 51,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            233 => 
            array (
                'id' => 229,
                'created_at' => '2016-03-27 19:58:15',
                'updated_at' => '2016-03-27 19:58:15',
                'remarque' => '',
                'versionable_id' => 31,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            234 => 
            array (
                'id' => 230,
                'created_at' => '2016-03-27 19:59:23',
                'updated_at' => '2016-03-27 19:59:23',
                'remarque' => '',
                'versionable_id' => 20,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            235 => 
            array (
                'id' => 231,
                'created_at' => '2016-03-27 20:01:02',
                'updated_at' => '2016-03-27 20:01:02',
                'remarque' => '',
                'versionable_id' => 17,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            236 => 
            array (
                'id' => 232,
                'created_at' => '2016-03-27 20:04:56',
                'updated_at' => '2016-03-27 20:04:56',
                'remarque' => '',
                'versionable_id' => 213,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            237 => 
            array (
                'id' => 233,
                'created_at' => '2016-03-27 20:06:21',
                'updated_at' => '2016-03-27 20:06:21',
                'remarque' => '',
                'versionable_id' => 15,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            238 => 
            array (
                'id' => 234,
                'created_at' => '2016-03-27 20:07:39',
                'updated_at' => '2016-03-27 20:07:39',
                'remarque' => '',
                'versionable_id' => 178,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            239 => 
            array (
                'id' => 236,
                'created_at' => '2016-03-27 20:12:02',
                'updated_at' => '2016-03-27 20:12:02',
                'remarque' => '',
                'versionable_id' => 53,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            240 => 
            array (
                'id' => 237,
                'created_at' => '2016-03-27 20:13:46',
                'updated_at' => '2016-03-27 20:13:46',
                'remarque' => '',
                'versionable_id' => 168,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            241 => 
            array (
                'id' => 238,
                'created_at' => '2016-03-27 20:14:32',
                'updated_at' => '2016-03-27 20:14:32',
                'remarque' => '',
                'versionable_id' => 79,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            242 => 
            array (
                'id' => 239,
                'created_at' => '2016-03-27 20:15:45',
                'updated_at' => '2016-03-27 20:15:45',
                'remarque' => '',
                'versionable_id' => 147,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            243 => 
            array (
                'id' => 240,
                'created_at' => '2016-03-27 20:16:24',
                'updated_at' => '2016-03-27 20:16:24',
                'remarque' => '',
                'versionable_id' => 104,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            244 => 
            array (
                'id' => 241,
                'created_at' => '2016-03-27 20:17:21',
                'updated_at' => '2016-03-27 20:17:21',
                'remarque' => '',
                'versionable_id' => 179,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            245 => 
            array (
                'id' => 243,
                'created_at' => '2016-03-27 20:19:54',
                'updated_at' => '2016-03-27 20:19:54',
                'remarque' => '',
                'versionable_id' => 34,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            246 => 
            array (
                'id' => 244,
                'created_at' => '2016-03-27 20:20:51',
                'updated_at' => '2016-03-27 20:20:51',
                'remarque' => '',
                'versionable_id' => 170,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            247 => 
            array (
                'id' => 248,
                'created_at' => '2016-03-29 15:03:59',
                'updated_at' => '2016-03-29 15:03:59',
                'remarque' => '',
                'versionable_id' => 143,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            248 => 
            array (
                'id' => 249,
                'created_at' => '2016-03-29 15:06:12',
                'updated_at' => '2016-03-29 15:06:12',
                'remarque' => '',
                'versionable_id' => 8,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            249 => 
            array (
                'id' => 250,
                'created_at' => '2016-03-29 15:06:49',
                'updated_at' => '2016-03-29 15:06:49',
                'remarque' => '',
                'versionable_id' => 107,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            250 => 
            array (
                'id' => 251,
                'created_at' => '2016-03-29 15:07:25',
                'updated_at' => '2016-03-29 15:07:25',
                'remarque' => '',
                'versionable_id' => 129,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            251 => 
            array (
                'id' => 252,
                'created_at' => '2016-03-29 15:08:02',
                'updated_at' => '2016-03-29 15:08:02',
                'remarque' => '',
                'versionable_id' => 127,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            252 => 
            array (
                'id' => 253,
                'created_at' => '2016-03-29 15:10:01',
                'updated_at' => '2016-03-29 15:10:01',
                'remarque' => '',
                'versionable_id' => 214,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            253 => 
            array (
                'id' => 254,
                'created_at' => '2016-03-29 17:41:13',
                'updated_at' => '2016-03-29 17:41:13',
                'remarque' => '',
                'versionable_id' => 64,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            254 => 
            array (
                'id' => 255,
                'created_at' => '2016-03-29 17:42:55',
                'updated_at' => '2016-03-29 17:42:55',
                'remarque' => '',
                'versionable_id' => 152,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            255 => 
            array (
                'id' => 256,
                'created_at' => '2016-03-29 17:43:39',
                'updated_at' => '2016-03-29 17:43:39',
                'remarque' => '',
                'versionable_id' => 158,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            256 => 
            array (
                'id' => 271,
                'created_at' => '2016-04-06 12:53:55',
                'updated_at' => '2016-04-06 12:53:55',
                'remarque' => '',
                'versionable_id' => 215,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            257 => 
            array (
                'id' => 259,
                'created_at' => '2016-03-30 11:56:35',
                'updated_at' => '2016-03-30 11:56:35',
                'remarque' => '',
                'versionable_id' => 97,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            258 => 
            array (
                'id' => 260,
                'created_at' => '2016-03-30 11:57:34',
                'updated_at' => '2016-03-30 11:57:34',
                'remarque' => '',
                'versionable_id' => 13,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            259 => 
            array (
                'id' => 261,
                'created_at' => '2016-03-30 11:59:08',
                'updated_at' => '2016-03-30 11:59:08',
                'remarque' => '',
                'versionable_id' => 142,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            260 => 
            array (
                'id' => 262,
                'created_at' => '2016-03-30 11:59:57',
                'updated_at' => '2016-03-30 11:59:57',
                'remarque' => '',
                'versionable_id' => 5,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            261 => 
            array (
                'id' => 263,
                'created_at' => '2016-03-30 12:01:13',
                'updated_at' => '2016-03-30 12:01:44',
                'remarque' => 'fgn je crois',
                'versionable_id' => 16,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            262 => 
            array (
                'id' => 264,
                'created_at' => '2016-03-30 12:03:37',
                'updated_at' => '2016-03-30 12:03:37',
                'remarque' => '',
                'versionable_id' => 150,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            263 => 
            array (
                'id' => 265,
                'created_at' => '2016-03-30 12:04:32',
                'updated_at' => '2016-03-30 12:04:32',
                'remarque' => '',
                'versionable_id' => 134,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            264 => 
            array (
                'id' => 266,
                'created_at' => '2016-03-30 12:05:19',
                'updated_at' => '2016-03-30 12:05:19',
                'remarque' => '',
                'versionable_id' => 69,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            265 => 
            array (
                'id' => 267,
                'created_at' => '2016-03-30 12:06:19',
                'updated_at' => '2016-03-30 12:06:19',
                'remarque' => '',
                'versionable_id' => 24,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            266 => 
            array (
                'id' => 268,
                'created_at' => '2016-03-30 12:07:08',
                'updated_at' => '2016-03-30 12:07:08',
                'remarque' => '',
                'versionable_id' => 25,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            267 => 
            array (
                'id' => 269,
                'created_at' => '2016-03-30 12:08:02',
                'updated_at' => '2016-03-30 12:08:02',
                'remarque' => '',
                'versionable_id' => 162,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            268 => 
            array (
                'id' => 270,
                'created_at' => '2016-03-30 12:08:43',
                'updated_at' => '2016-03-30 12:08:43',
                'remarque' => '',
                'versionable_id' => 175,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            269 => 
            array (
                'id' => 275,
                'created_at' => '2016-04-06 15:54:31',
                'updated_at' => '2016-04-06 15:54:31',
                'remarque' => '',
                'versionable_id' => 1,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            270 => 
            array (
                'id' => 276,
                'created_at' => '2016-04-06 15:57:38',
                'updated_at' => '2016-05-15 10:53:22',
                'remarque' => '',
                'versionable_id' => 3,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            271 => 
            array (
                'id' => 277,
                'created_at' => '2016-04-06 16:00:19',
                'updated_at' => '2016-04-06 16:00:19',
                'remarque' => '',
                'versionable_id' => 4,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            272 => 
            array (
                'id' => 278,
                'created_at' => '2016-04-06 16:03:01',
                'updated_at' => '2016-05-15 10:53:31',
                'remarque' => '',
                'versionable_id' => 5,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            273 => 
            array (
                'id' => 279,
                'created_at' => '2016-04-06 16:03:40',
                'updated_at' => '2016-04-06 16:03:40',
                'remarque' => '',
                'versionable_id' => 5,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            274 => 
            array (
                'id' => 280,
                'created_at' => '2016-04-06 16:04:58',
                'updated_at' => '2016-04-06 16:04:58',
                'remarque' => '',
                'versionable_id' => 6,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            275 => 
            array (
                'id' => 281,
                'created_at' => '2016-04-06 16:06:18',
                'updated_at' => '2016-04-06 16:06:18',
                'remarque' => '',
                'versionable_id' => 7,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            276 => 
            array (
                'id' => 282,
                'created_at' => '2016-04-06 16:07:37',
                'updated_at' => '2016-04-06 16:07:37',
                'remarque' => '',
                'versionable_id' => 8,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            277 => 
            array (
                'id' => 283,
                'created_at' => '2016-04-06 16:10:42',
                'updated_at' => '2016-05-15 10:53:36',
                'remarque' => '',
                'versionable_id' => 12,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            278 => 
            array (
                'id' => 284,
                'created_at' => '2016-04-06 16:11:37',
                'updated_at' => '2016-04-06 16:11:37',
                'remarque' => '',
                'versionable_id' => 12,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            279 => 
            array (
                'id' => 285,
                'created_at' => '2016-04-06 16:13:23',
                'updated_at' => '2016-04-06 16:13:23',
                'remarque' => '',
                'versionable_id' => 13,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            280 => 
            array (
                'id' => 286,
                'created_at' => '2016-04-06 16:15:19',
                'updated_at' => '2016-04-06 16:15:19',
                'remarque' => '',
                'versionable_id' => 15,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            281 => 
            array (
                'id' => 287,
                'created_at' => '2016-04-06 16:16:40',
                'updated_at' => '2016-05-15 10:53:40',
                'remarque' => '',
                'versionable_id' => 16,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            282 => 
            array (
                'id' => 288,
                'created_at' => '2016-04-06 16:18:10',
                'updated_at' => '2016-05-15 10:53:43',
                'remarque' => '',
                'versionable_id' => 17,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            283 => 
            array (
                'id' => 289,
                'created_at' => '2016-04-06 16:19:20',
                'updated_at' => '2016-05-15 10:53:47',
                'remarque' => '',
                'versionable_id' => 18,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            284 => 
            array (
                'id' => 291,
                'created_at' => '2016-04-06 16:22:03',
                'updated_at' => '2016-04-06 16:22:03',
                'remarque' => '',
                'versionable_id' => 21,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            285 => 
            array (
                'id' => 292,
                'created_at' => '2016-04-06 16:24:27',
                'updated_at' => '2016-04-06 16:24:27',
                'remarque' => '',
                'versionable_id' => 24,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            286 => 
            array (
                'id' => 293,
                'created_at' => '2016-04-06 16:24:56',
                'updated_at' => '2016-04-06 16:24:56',
                'remarque' => '',
                'versionable_id' => 24,
                'versionable_type' => 'developpement',
                'user_id' => 110,
            ),
            287 => 
            array (
                'id' => 294,
                'created_at' => '2016-04-06 16:25:55',
                'updated_at' => '2016-05-15 10:54:02',
                'remarque' => '',
                'versionable_id' => 25,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            288 => 
            array (
                'id' => 295,
                'created_at' => '2016-04-06 16:26:20',
                'updated_at' => '2016-04-06 16:26:20',
                'remarque' => '',
                'versionable_id' => 25,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            289 => 
            array (
                'id' => 296,
                'created_at' => '2016-04-06 16:27:26',
                'updated_at' => '2016-05-15 10:54:06',
                'remarque' => '',
                'versionable_id' => 26,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            290 => 
            array (
                'id' => 297,
                'created_at' => '2016-04-06 16:28:56',
                'updated_at' => '2016-04-06 16:28:56',
                'remarque' => '',
                'versionable_id' => 28,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            291 => 
            array (
                'id' => 298,
                'created_at' => '2016-04-06 16:30:16',
                'updated_at' => '2016-04-06 16:30:16',
                'remarque' => '',
                'versionable_id' => 29,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            292 => 
            array (
                'id' => 300,
                'created_at' => '2016-04-11 20:30:46',
                'updated_at' => '2016-04-11 20:30:46',
                'remarque' => '',
                'versionable_id' => 35,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            293 => 
            array (
                'id' => 301,
                'created_at' => '2016-04-11 20:36:07',
                'updated_at' => '2016-05-15 10:54:12',
                'remarque' => '',
                'versionable_id' => 43,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            294 => 
            array (
                'id' => 302,
                'created_at' => '2016-04-11 20:37:34',
                'updated_at' => '2016-04-11 20:37:34',
                'remarque' => '',
                'versionable_id' => 44,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            295 => 
            array (
                'id' => 303,
                'created_at' => '2016-04-11 20:39:11',
                'updated_at' => '2016-04-11 20:39:11',
                'remarque' => '',
                'versionable_id' => 46,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            296 => 
            array (
                'id' => 304,
                'created_at' => '2016-04-11 20:41:03',
                'updated_at' => '2016-04-11 20:41:03',
                'remarque' => '',
                'versionable_id' => 47,
                'versionable_type' => 'developpement',
                'user_id' => 106,
            ),
            297 => 
            array (
                'id' => 305,
                'created_at' => '2016-04-12 21:02:08',
                'updated_at' => '2016-04-12 21:02:08',
                'remarque' => '',
                'versionable_id' => 52,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            298 => 
            array (
                'id' => 338,
                'created_at' => '2016-10-04 11:27:43',
                'updated_at' => '2016-10-04 11:27:43',
                'remarque' => '',
                'versionable_id' => 666,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            299 => 
            array (
                'id' => 306,
                'created_at' => '2016-04-12 21:03:50',
                'updated_at' => '2016-04-12 21:03:50',
                'remarque' => '',
                'versionable_id' => 53,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            300 => 
            array (
                'id' => 307,
                'created_at' => '2016-04-12 21:06:13',
                'updated_at' => '2016-04-12 21:06:13',
                'remarque' => '',
                'versionable_id' => 56,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            301 => 
            array (
                'id' => 308,
                'created_at' => '2016-04-12 21:08:08',
                'updated_at' => '2016-04-12 21:08:08',
                'remarque' => '',
                'versionable_id' => 58,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            302 => 
            array (
                'id' => 309,
                'created_at' => '2016-04-12 21:09:32',
                'updated_at' => '2016-04-12 21:09:32',
                'remarque' => '',
                'versionable_id' => 59,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            303 => 
            array (
                'id' => 310,
                'created_at' => '2016-04-12 21:11:36',
                'updated_at' => '2016-04-12 21:11:36',
                'remarque' => '',
                'versionable_id' => 61,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            304 => 
            array (
                'id' => 311,
                'created_at' => '2016-04-12 21:14:10',
                'updated_at' => '2016-04-12 21:14:10',
                'remarque' => '',
                'versionable_id' => 65,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            305 => 
            array (
                'id' => 312,
                'created_at' => '2016-04-12 21:15:31',
                'updated_at' => '2016-04-12 21:15:31',
                'remarque' => '',
                'versionable_id' => 66,
                'versionable_type' => 'developpement',
                'user_id' => 105,
            ),
            306 => 
            array (
                'id' => 313,
                'created_at' => '2016-04-13 20:09:20',
                'updated_at' => '2016-04-13 20:09:20',
                'remarque' => '',
                'versionable_id' => 83,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            307 => 
            array (
                'id' => 314,
                'created_at' => '2016-04-13 20:10:27',
                'updated_at' => '2016-05-15 10:54:16',
                'remarque' => '',
                'versionable_id' => 84,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            308 => 
            array (
                'id' => 315,
                'created_at' => '2016-04-13 20:12:58',
                'updated_at' => '2016-05-15 10:54:21',
                'remarque' => '',
                'versionable_id' => 87,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            309 => 
            array (
                'id' => 316,
                'created_at' => '2016-04-13 20:15:13',
                'updated_at' => '2016-05-15 10:54:25',
                'remarque' => '',
                'versionable_id' => 90,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            310 => 
            array (
                'id' => 317,
                'created_at' => '2016-04-13 20:20:19',
                'updated_at' => '2016-05-15 10:54:31',
                'remarque' => '',
                'versionable_id' => 98,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            311 => 
            array (
                'id' => 318,
                'created_at' => '2016-04-13 20:21:56',
                'updated_at' => '2016-04-13 20:21:56',
                'remarque' => '',
                'versionable_id' => 101,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            312 => 
            array (
                'id' => 337,
                'created_at' => '2016-10-04 11:26:13',
                'updated_at' => '2016-10-04 11:26:13',
                'remarque' => '',
                'versionable_id' => 665,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            313 => 
            array (
                'id' => 319,
                'created_at' => '2016-04-13 20:24:19',
                'updated_at' => '2016-04-13 20:24:19',
                'remarque' => '',
                'versionable_id' => 104,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            314 => 
            array (
                'id' => 320,
                'created_at' => '2016-04-16 08:10:00',
                'updated_at' => '2016-05-15 10:54:36',
                'remarque' => '',
                'versionable_id' => 120,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            315 => 
            array (
                'id' => 334,
                'created_at' => '2016-10-04 11:12:05',
                'updated_at' => '2016-10-04 11:12:05',
                'remarque' => '',
                'versionable_id' => 663,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            316 => 
            array (
                'id' => 321,
                'created_at' => '2016-04-16 08:14:36',
                'updated_at' => '2016-05-15 10:54:41',
                'remarque' => '',
                'versionable_id' => 129,
                'versionable_type' => 'developpement',
                'user_id' => 112,
            ),
            317 => 
            array (
                'id' => 322,
                'created_at' => '2016-04-16 08:15:27',
                'updated_at' => '2016-04-16 08:15:27',
                'remarque' => '',
                'versionable_id' => 129,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            318 => 
            array (
                'id' => 323,
                'created_at' => '2016-04-16 08:16:30',
                'updated_at' => '2016-04-16 08:16:30',
                'remarque' => '',
                'versionable_id' => 129,
                'versionable_type' => 'developpement',
                'user_id' => 104,
            ),
            319 => 
            array (
                'id' => 335,
                'created_at' => '2016-10-04 11:22:02',
                'updated_at' => '2016-10-04 11:22:02',
                'remarque' => '',
                'versionable_id' => 664,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            320 => 
            array (
                'id' => 324,
                'created_at' => '2016-04-16 08:27:41',
                'updated_at' => '2016-04-16 08:27:41',
                'remarque' => '',
                'versionable_id' => 150,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            321 => 
            array (
                'id' => 325,
                'created_at' => '2016-04-18 16:18:17',
                'updated_at' => '2016-04-18 16:18:17',
                'remarque' => '',
                'versionable_id' => 152,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            322 => 
            array (
                'id' => 336,
                'created_at' => '2016-10-04 11:22:19',
                'updated_at' => '2016-10-04 11:22:19',
                'remarque' => '',
                'versionable_id' => 663,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            323 => 
            array (
                'id' => 326,
                'created_at' => '2016-04-21 11:17:10',
                'updated_at' => '2016-04-21 11:17:10',
                'remarque' => '',
                'versionable_id' => 2,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            324 => 
            array (
                'id' => 327,
                'created_at' => '2016-04-21 11:25:48',
                'updated_at' => '2016-04-21 11:26:07',
                'remarque' => 'Version un poil différente des autres.',
                'versionable_id' => 53,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            325 => 
            array (
                'id' => 328,
                'created_at' => '2016-04-21 11:27:05',
                'updated_at' => '2016-04-21 11:27:05',
                'remarque' => '',
                'versionable_id' => 106,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            326 => 
            array (
                'id' => 329,
                'created_at' => '2016-04-21 11:29:27',
                'updated_at' => '2016-04-21 11:29:27',
                'remarque' => '',
                'versionable_id' => 8,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            327 => 
            array (
                'id' => 330,
                'created_at' => '2016-04-21 11:36:11',
                'updated_at' => '2016-04-21 11:36:11',
                'remarque' => '',
                'versionable_id' => 64,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            328 => 
            array (
                'id' => 331,
                'created_at' => '2016-04-21 11:41:38',
                'updated_at' => '2016-04-21 11:41:38',
                'remarque' => '',
                'versionable_id' => 216,
                'versionable_type' => 'developpement',
                'user_id' => 109,
            ),
            329 => 
            array (
                'id' => 348,
                'created_at' => '2016-10-04 14:25:56',
                'updated_at' => '2016-10-04 14:25:56',
                'remarque' => '',
                'versionable_id' => 675,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            330 => 
            array (
                'id' => 349,
                'created_at' => '2016-10-04 14:26:11',
                'updated_at' => '2016-10-04 14:26:11',
                'remarque' => '',
                'versionable_id' => 676,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            331 => 
            array (
                'id' => 350,
                'created_at' => '2016-10-04 14:26:23',
                'updated_at' => '2016-10-04 14:26:23',
                'remarque' => '',
                'versionable_id' => 677,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            332 => 
            array (
                'id' => 351,
                'created_at' => '2016-10-04 14:26:36',
                'updated_at' => '2016-10-04 14:26:36',
                'remarque' => '',
                'versionable_id' => 678,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            333 => 
            array (
                'id' => 352,
                'created_at' => '2016-10-04 14:26:52',
                'updated_at' => '2016-10-04 14:26:52',
                'remarque' => '',
                'versionable_id' => 679,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            334 => 
            array (
                'id' => 353,
                'created_at' => '2016-10-04 14:27:05',
                'updated_at' => '2016-10-04 14:27:05',
                'remarque' => '',
                'versionable_id' => 680,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            335 => 
            array (
                'id' => 354,
                'created_at' => '2016-10-04 14:27:18',
                'updated_at' => '2016-10-04 14:27:18',
                'remarque' => '',
                'versionable_id' => 681,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            336 => 
            array (
                'id' => 355,
                'created_at' => '2016-10-06 14:52:13',
                'updated_at' => '2016-10-06 14:52:13',
                'remarque' => '',
                'versionable_id' => 682,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            337 => 
            array (
                'id' => 356,
                'created_at' => '2016-10-06 14:52:53',
                'updated_at' => '2016-10-06 14:52:53',
                'remarque' => '',
                'versionable_id' => 683,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            338 => 
            array (
                'id' => 357,
                'created_at' => '2016-10-06 14:53:20',
                'updated_at' => '2016-10-06 14:53:20',
                'remarque' => '',
                'versionable_id' => 684,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            339 => 
            array (
                'id' => 358,
                'created_at' => '2016-10-06 14:53:35',
                'updated_at' => '2016-10-06 14:53:35',
                'remarque' => '',
                'versionable_id' => 685,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            340 => 
            array (
                'id' => 359,
                'created_at' => '2016-10-06 14:53:48',
                'updated_at' => '2016-10-06 14:53:48',
                'remarque' => '',
                'versionable_id' => 686,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            341 => 
            array (
                'id' => 360,
                'created_at' => '2016-10-06 14:54:01',
                'updated_at' => '2016-10-06 14:54:01',
                'remarque' => '',
                'versionable_id' => 687,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            342 => 
            array (
                'id' => 361,
                'created_at' => '2016-10-06 14:54:15',
                'updated_at' => '2016-10-06 14:54:15',
                'remarque' => '',
                'versionable_id' => 688,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            343 => 
            array (
                'id' => 362,
                'created_at' => '2016-10-06 14:54:29',
                'updated_at' => '2016-10-06 14:54:29',
                'remarque' => '',
                'versionable_id' => 689,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            344 => 
            array (
                'id' => 363,
                'created_at' => '2016-10-06 14:54:41',
                'updated_at' => '2016-10-06 14:54:41',
                'remarque' => '',
                'versionable_id' => 690,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            345 => 
            array (
                'id' => 364,
                'created_at' => '2016-10-06 14:54:56',
                'updated_at' => '2016-10-06 14:54:56',
                'remarque' => '',
                'versionable_id' => 691,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            346 => 
            array (
                'id' => 365,
                'created_at' => '2016-10-06 15:41:52',
                'updated_at' => '2016-10-06 15:41:52',
                'remarque' => '',
                'versionable_id' => 692,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            347 => 
            array (
                'id' => 366,
                'created_at' => '2016-10-06 15:42:06',
                'updated_at' => '2016-10-06 15:42:06',
                'remarque' => '',
                'versionable_id' => 693,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            348 => 
            array (
                'id' => 367,
                'created_at' => '2016-10-06 15:42:21',
                'updated_at' => '2016-10-06 15:42:21',
                'remarque' => '',
                'versionable_id' => 694,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            349 => 
            array (
                'id' => 368,
                'created_at' => '2016-10-06 15:42:34',
                'updated_at' => '2016-10-06 15:42:34',
                'remarque' => '',
                'versionable_id' => 695,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            350 => 
            array (
                'id' => 369,
                'created_at' => '2016-10-06 15:42:50',
                'updated_at' => '2016-10-06 15:42:50',
                'remarque' => '',
                'versionable_id' => 696,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            351 => 
            array (
                'id' => 370,
                'created_at' => '2016-10-06 15:43:10',
                'updated_at' => '2016-10-06 15:43:10',
                'remarque' => '',
                'versionable_id' => 697,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            352 => 
            array (
                'id' => 371,
                'created_at' => '2016-10-06 15:43:25',
                'updated_at' => '2016-10-06 15:43:25',
                'remarque' => '',
                'versionable_id' => 698,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            353 => 
            array (
                'id' => 372,
                'created_at' => '2016-10-06 15:43:41',
                'updated_at' => '2016-10-06 15:43:41',
                'remarque' => '',
                'versionable_id' => 699,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            354 => 
            array (
                'id' => 373,
                'created_at' => '2016-10-06 15:43:54',
                'updated_at' => '2016-10-06 15:43:54',
                'remarque' => '',
                'versionable_id' => 700,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            355 => 
            array (
                'id' => 374,
                'created_at' => '2016-10-06 15:44:13',
                'updated_at' => '2016-10-06 15:44:13',
                'remarque' => '',
                'versionable_id' => 701,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            356 => 
            array (
                'id' => 375,
                'created_at' => '2016-10-06 15:54:48',
                'updated_at' => '2016-10-06 15:54:48',
                'remarque' => '',
                'versionable_id' => 702,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            357 => 
            array (
                'id' => 376,
                'created_at' => '2016-10-06 15:55:04',
                'updated_at' => '2016-10-06 15:55:04',
                'remarque' => '',
                'versionable_id' => 703,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            358 => 
            array (
                'id' => 377,
                'created_at' => '2016-10-06 15:55:17',
                'updated_at' => '2016-10-06 15:55:17',
                'remarque' => '',
                'versionable_id' => 704,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            359 => 
            array (
                'id' => 378,
                'created_at' => '2016-10-06 15:55:35',
                'updated_at' => '2016-10-06 15:55:35',
                'remarque' => '',
                'versionable_id' => 705,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            360 => 
            array (
                'id' => 379,
                'created_at' => '2016-10-06 15:55:45',
                'updated_at' => '2016-10-06 15:55:45',
                'remarque' => '',
                'versionable_id' => 705,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            361 => 
            array (
                'id' => 380,
                'created_at' => '2016-10-06 15:55:59',
                'updated_at' => '2016-10-06 15:55:59',
                'remarque' => '',
                'versionable_id' => 706,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            362 => 
            array (
                'id' => 381,
                'created_at' => '2016-10-06 15:56:13',
                'updated_at' => '2016-10-06 15:56:13',
                'remarque' => '',
                'versionable_id' => 707,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            363 => 
            array (
                'id' => 382,
                'created_at' => '2016-10-06 15:56:25',
                'updated_at' => '2016-10-06 15:56:25',
                'remarque' => '',
                'versionable_id' => 708,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            364 => 
            array (
                'id' => 383,
                'created_at' => '2016-10-06 15:56:38',
                'updated_at' => '2016-10-06 15:56:38',
                'remarque' => '',
                'versionable_id' => 709,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            365 => 
            array (
                'id' => 384,
                'created_at' => '2016-10-07 14:37:50',
                'updated_at' => '2016-10-07 14:37:50',
                'remarque' => '',
                'versionable_id' => 710,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            366 => 
            array (
                'id' => 385,
                'created_at' => '2016-10-07 14:38:03',
                'updated_at' => '2016-10-07 14:38:03',
                'remarque' => '',
                'versionable_id' => 711,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            367 => 
            array (
                'id' => 386,
                'created_at' => '2016-10-07 14:38:15',
                'updated_at' => '2016-10-07 14:38:15',
                'remarque' => '',
                'versionable_id' => 712,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            368 => 
            array (
                'id' => 387,
                'created_at' => '2016-10-07 14:38:27',
                'updated_at' => '2016-10-07 14:38:27',
                'remarque' => '',
                'versionable_id' => 713,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            369 => 
            array (
                'id' => 388,
                'created_at' => '2016-10-07 14:38:41',
                'updated_at' => '2016-10-07 14:38:41',
                'remarque' => '',
                'versionable_id' => 714,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            370 => 
            array (
                'id' => 389,
                'created_at' => '2016-10-07 14:38:53',
                'updated_at' => '2016-10-07 14:38:53',
                'remarque' => '',
                'versionable_id' => 715,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            371 => 
            array (
                'id' => 390,
                'created_at' => '2016-10-07 14:39:05',
                'updated_at' => '2016-10-07 14:39:05',
                'remarque' => '',
                'versionable_id' => 716,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            372 => 
            array (
                'id' => 391,
                'created_at' => '2016-10-07 14:39:18',
                'updated_at' => '2016-10-07 14:39:18',
                'remarque' => '',
                'versionable_id' => 717,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            373 => 
            array (
                'id' => 392,
                'created_at' => '2016-10-07 14:39:31',
                'updated_at' => '2016-10-07 14:39:31',
                'remarque' => '',
                'versionable_id' => 718,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            374 => 
            array (
                'id' => 393,
                'created_at' => '2016-10-07 14:39:39',
                'updated_at' => '2016-10-07 14:39:39',
                'remarque' => '',
                'versionable_id' => 718,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            375 => 
            array (
                'id' => 394,
                'created_at' => '2016-10-07 14:39:53',
                'updated_at' => '2016-10-07 14:39:53',
                'remarque' => '',
                'versionable_id' => 719,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            376 => 
            array (
                'id' => 395,
                'created_at' => '2016-10-07 14:40:07',
                'updated_at' => '2016-10-07 14:40:07',
                'remarque' => '',
                'versionable_id' => 720,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            377 => 
            array (
                'id' => 396,
                'created_at' => '2016-10-10 12:27:37',
                'updated_at' => '2016-10-10 12:27:37',
                'remarque' => '',
                'versionable_id' => 721,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            378 => 
            array (
                'id' => 397,
                'created_at' => '2016-10-10 12:27:48',
                'updated_at' => '2016-10-10 12:27:48',
                'remarque' => '',
                'versionable_id' => 721,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            379 => 
            array (
                'id' => 398,
                'created_at' => '2016-10-10 12:28:07',
                'updated_at' => '2016-10-10 12:28:07',
                'remarque' => '',
                'versionable_id' => 722,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            380 => 
            array (
                'id' => 399,
                'created_at' => '2016-10-10 12:28:28',
                'updated_at' => '2016-10-10 12:28:28',
                'remarque' => '',
                'versionable_id' => 723,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            381 => 
            array (
                'id' => 400,
                'created_at' => '2016-10-10 12:28:40',
                'updated_at' => '2016-10-10 12:28:40',
                'remarque' => '',
                'versionable_id' => 724,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            382 => 
            array (
                'id' => 401,
                'created_at' => '2016-10-10 12:28:52',
                'updated_at' => '2016-10-10 12:28:52',
                'remarque' => '',
                'versionable_id' => 725,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            383 => 
            array (
                'id' => 402,
                'created_at' => '2016-10-10 12:29:42',
                'updated_at' => '2016-10-10 12:29:42',
                'remarque' => '',
                'versionable_id' => 726,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            384 => 
            array (
                'id' => 403,
                'created_at' => '2016-10-10 12:29:53',
                'updated_at' => '2016-10-10 12:29:53',
                'remarque' => '',
                'versionable_id' => 727,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            385 => 
            array (
                'id' => 404,
                'created_at' => '2016-10-10 12:30:09',
                'updated_at' => '2016-10-10 12:30:09',
                'remarque' => '',
                'versionable_id' => 728,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            386 => 
            array (
                'id' => 405,
                'created_at' => '2016-10-10 12:30:23',
                'updated_at' => '2016-10-10 12:30:23',
                'remarque' => '',
                'versionable_id' => 729,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            387 => 
            array (
                'id' => 406,
                'created_at' => '2016-10-10 12:30:35',
                'updated_at' => '2016-10-10 12:30:35',
                'remarque' => '',
                'versionable_id' => 730,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            388 => 
            array (
                'id' => 407,
                'created_at' => '2016-10-10 12:30:48',
                'updated_at' => '2016-10-10 12:30:48',
                'remarque' => '',
                'versionable_id' => 731,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            389 => 
            array (
                'id' => 408,
                'created_at' => '2016-10-10 12:31:01',
                'updated_at' => '2016-10-10 12:31:01',
                'remarque' => '',
                'versionable_id' => 732,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            390 => 
            array (
                'id' => 409,
                'created_at' => '2016-10-10 12:31:15',
                'updated_at' => '2016-10-10 12:31:15',
                'remarque' => '',
                'versionable_id' => 733,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            391 => 
            array (
                'id' => 410,
                'created_at' => '2016-10-10 12:31:27',
                'updated_at' => '2016-10-10 12:31:27',
                'remarque' => '',
                'versionable_id' => 734,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            392 => 
            array (
                'id' => 411,
                'created_at' => '2016-10-10 12:31:46',
                'updated_at' => '2016-10-10 12:31:46',
                'remarque' => '',
                'versionable_id' => 771,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            393 => 
            array (
                'id' => 412,
                'created_at' => '2016-10-10 15:48:58',
                'updated_at' => '2016-10-10 15:48:58',
                'remarque' => '',
                'versionable_id' => 736,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            394 => 
            array (
                'id' => 413,
                'created_at' => '2016-10-10 15:49:08',
                'updated_at' => '2016-10-10 15:49:08',
                'remarque' => '',
                'versionable_id' => 736,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            395 => 
            array (
                'id' => 414,
                'created_at' => '2016-10-10 15:49:26',
                'updated_at' => '2016-10-10 15:49:26',
                'remarque' => '',
                'versionable_id' => 737,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            396 => 
            array (
                'id' => 415,
                'created_at' => '2016-10-10 15:49:41',
                'updated_at' => '2016-10-10 15:49:41',
                'remarque' => '',
                'versionable_id' => 738,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            397 => 
            array (
                'id' => 416,
                'created_at' => '2016-10-10 15:50:11',
                'updated_at' => '2016-10-10 15:50:11',
                'remarque' => '',
                'versionable_id' => 739,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            398 => 
            array (
                'id' => 417,
                'created_at' => '2016-10-10 15:50:24',
                'updated_at' => '2016-10-10 15:50:24',
                'remarque' => '',
                'versionable_id' => 740,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            399 => 
            array (
                'id' => 418,
                'created_at' => '2016-10-10 15:50:44',
                'updated_at' => '2016-10-10 15:50:44',
                'remarque' => '',
                'versionable_id' => 741,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            400 => 
            array (
                'id' => 419,
                'created_at' => '2016-10-10 15:51:01',
                'updated_at' => '2016-10-10 15:51:01',
                'remarque' => '',
                'versionable_id' => 742,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            401 => 
            array (
                'id' => 420,
                'created_at' => '2016-10-10 15:52:22',
                'updated_at' => '2016-10-10 15:52:22',
                'remarque' => '',
                'versionable_id' => 744,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            402 => 
            array (
                'id' => 421,
                'created_at' => '2016-10-10 15:52:34',
                'updated_at' => '2016-10-10 15:52:34',
                'remarque' => '',
                'versionable_id' => 745,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            403 => 
            array (
                'id' => 422,
                'created_at' => '2016-10-10 15:52:48',
                'updated_at' => '2016-10-10 15:52:48',
                'remarque' => '',
                'versionable_id' => 746,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            404 => 
            array (
                'id' => 423,
                'created_at' => '2016-10-10 15:53:00',
                'updated_at' => '2016-10-10 15:53:00',
                'remarque' => '',
                'versionable_id' => 747,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            405 => 
            array (
                'id' => 424,
                'created_at' => '2016-10-10 15:53:10',
                'updated_at' => '2016-10-10 15:53:10',
                'remarque' => '',
                'versionable_id' => 748,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            406 => 
            array (
                'id' => 425,
                'created_at' => '2016-10-11 13:20:18',
                'updated_at' => '2016-10-11 13:20:18',
                'remarque' => '',
                'versionable_id' => 552,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            407 => 
            array (
                'id' => 426,
                'created_at' => '2016-10-12 08:05:53',
                'updated_at' => '2016-10-12 08:05:53',
                'remarque' => '',
                'versionable_id' => 553,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            408 => 
            array (
                'id' => 427,
                'created_at' => '2016-10-12 08:07:04',
                'updated_at' => '2016-10-12 08:07:04',
                'remarque' => '',
                'versionable_id' => 554,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            409 => 
            array (
                'id' => 428,
                'created_at' => '2016-10-12 08:09:29',
                'updated_at' => '2016-10-12 08:09:29',
                'remarque' => '',
                'versionable_id' => 555,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            410 => 
            array (
                'id' => 429,
                'created_at' => '2016-10-12 08:10:04',
                'updated_at' => '2016-10-12 08:10:04',
                'remarque' => '',
                'versionable_id' => 556,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            411 => 
            array (
                'id' => 430,
                'created_at' => '2016-10-12 08:10:37',
                'updated_at' => '2016-10-12 08:10:37',
                'remarque' => '',
                'versionable_id' => 557,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            412 => 
            array (
                'id' => 431,
                'created_at' => '2016-10-12 08:11:04',
                'updated_at' => '2016-10-12 08:11:04',
                'remarque' => '',
                'versionable_id' => 558,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            413 => 
            array (
                'id' => 432,
                'created_at' => '2016-10-12 08:11:37',
                'updated_at' => '2016-10-12 08:11:37',
                'remarque' => '',
                'versionable_id' => 559,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            414 => 
            array (
                'id' => 433,
                'created_at' => '2016-10-12 08:11:59',
                'updated_at' => '2016-10-12 08:11:59',
                'remarque' => '',
                'versionable_id' => 560,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            415 => 
            array (
                'id' => 434,
                'created_at' => '2016-10-12 08:12:23',
                'updated_at' => '2016-10-12 08:12:23',
                'remarque' => '',
                'versionable_id' => 561,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            416 => 
            array (
                'id' => 435,
                'created_at' => '2016-10-12 08:12:41',
                'updated_at' => '2016-10-12 08:12:41',
                'remarque' => '',
                'versionable_id' => 562,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            417 => 
            array (
                'id' => 436,
                'created_at' => '2016-10-12 08:13:31',
                'updated_at' => '2016-10-12 08:13:31',
                'remarque' => '',
                'versionable_id' => 563,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            418 => 
            array (
                'id' => 437,
                'created_at' => '2016-10-12 08:13:54',
                'updated_at' => '2016-10-12 08:13:54',
                'remarque' => '',
                'versionable_id' => 564,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            419 => 
            array (
                'id' => 438,
                'created_at' => '2016-10-12 12:53:50',
                'updated_at' => '2016-10-12 12:53:50',
                'remarque' => '',
                'versionable_id' => 565,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            420 => 
            array (
                'id' => 439,
                'created_at' => '2016-10-12 12:54:22',
                'updated_at' => '2016-10-12 12:54:22',
                'remarque' => '',
                'versionable_id' => 566,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            421 => 
            array (
                'id' => 440,
                'created_at' => '2016-10-12 12:54:43',
                'updated_at' => '2016-10-12 12:54:43',
                'remarque' => '',
                'versionable_id' => 567,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            422 => 
            array (
                'id' => 441,
                'created_at' => '2016-10-12 12:55:04',
                'updated_at' => '2016-10-12 12:55:04',
                'remarque' => '',
                'versionable_id' => 568,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            423 => 
            array (
                'id' => 442,
                'created_at' => '2016-10-12 12:55:24',
                'updated_at' => '2016-10-12 12:55:24',
                'remarque' => '',
                'versionable_id' => 569,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            424 => 
            array (
                'id' => 443,
                'created_at' => '2016-10-12 12:57:53',
                'updated_at' => '2016-10-12 12:57:53',
                'remarque' => '',
                'versionable_id' => 570,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            425 => 
            array (
                'id' => 444,
                'created_at' => '2016-10-12 12:58:26',
                'updated_at' => '2016-10-12 12:58:26',
                'remarque' => '',
                'versionable_id' => 571,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            426 => 
            array (
                'id' => 445,
                'created_at' => '2016-10-12 12:58:49',
                'updated_at' => '2016-10-12 12:58:49',
                'remarque' => '',
                'versionable_id' => 572,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            427 => 
            array (
                'id' => 446,
                'created_at' => '2016-10-12 12:59:11',
                'updated_at' => '2016-10-12 12:59:11',
                'remarque' => '',
                'versionable_id' => 573,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            428 => 
            array (
                'id' => 447,
                'created_at' => '2016-10-12 12:59:30',
                'updated_at' => '2016-10-12 12:59:30',
                'remarque' => '',
                'versionable_id' => 574,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            429 => 
            array (
                'id' => 448,
                'created_at' => '2016-10-12 12:59:51',
                'updated_at' => '2016-10-12 12:59:51',
                'remarque' => '',
                'versionable_id' => 575,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            430 => 
            array (
                'id' => 449,
                'created_at' => '2016-10-12 13:00:11',
                'updated_at' => '2016-10-12 13:00:11',
                'remarque' => '',
                'versionable_id' => 576,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            431 => 
            array (
                'id' => 450,
                'created_at' => '2016-10-12 13:00:32',
                'updated_at' => '2016-10-12 13:00:32',
                'remarque' => '',
                'versionable_id' => 577,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            432 => 
            array (
                'id' => 451,
                'created_at' => '2016-10-12 13:00:51',
                'updated_at' => '2016-10-12 13:00:51',
                'remarque' => '',
                'versionable_id' => 578,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            433 => 
            array (
                'id' => 452,
                'created_at' => '2016-10-12 13:01:12',
                'updated_at' => '2016-10-12 13:01:12',
                'remarque' => '',
                'versionable_id' => 579,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            434 => 
            array (
                'id' => 453,
                'created_at' => '2016-10-12 13:01:35',
                'updated_at' => '2016-10-12 13:01:35',
                'remarque' => '',
                'versionable_id' => 580,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            435 => 
            array (
                'id' => 454,
                'created_at' => '2016-10-12 13:01:55',
                'updated_at' => '2016-10-12 13:01:55',
                'remarque' => '',
                'versionable_id' => 581,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            436 => 
            array (
                'id' => 455,
                'created_at' => '2016-10-12 13:02:13',
                'updated_at' => '2016-10-12 13:02:13',
                'remarque' => '',
                'versionable_id' => 582,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            437 => 
            array (
                'id' => 456,
                'created_at' => '2016-10-12 13:02:32',
                'updated_at' => '2016-10-12 13:02:32',
                'remarque' => '',
                'versionable_id' => 583,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            438 => 
            array (
                'id' => 457,
                'created_at' => '2016-10-12 13:02:55',
                'updated_at' => '2016-10-12 13:02:55',
                'remarque' => '',
                'versionable_id' => 584,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            439 => 
            array (
                'id' => 458,
                'created_at' => '2016-10-12 13:03:22',
                'updated_at' => '2016-10-12 13:03:22',
                'remarque' => '',
                'versionable_id' => 585,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            440 => 
            array (
                'id' => 459,
                'created_at' => '2016-10-12 13:03:48',
                'updated_at' => '2016-10-12 13:03:48',
                'remarque' => '',
                'versionable_id' => 586,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            441 => 
            array (
                'id' => 460,
                'created_at' => '2016-10-12 13:04:22',
                'updated_at' => '2016-10-12 13:04:22',
                'remarque' => '',
                'versionable_id' => 587,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            442 => 
            array (
                'id' => 461,
                'created_at' => '2016-10-12 13:04:42',
                'updated_at' => '2016-10-12 13:04:42',
                'remarque' => '',
                'versionable_id' => 588,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            443 => 
            array (
                'id' => 462,
                'created_at' => '2016-10-12 13:05:07',
                'updated_at' => '2016-10-12 13:05:07',
                'remarque' => '',
                'versionable_id' => 589,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            444 => 
            array (
                'id' => 463,
                'created_at' => '2016-10-12 13:05:25',
                'updated_at' => '2016-10-12 13:05:25',
                'remarque' => '',
                'versionable_id' => 590,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            445 => 
            array (
                'id' => 464,
                'created_at' => '2016-10-12 13:05:44',
                'updated_at' => '2016-10-12 13:05:44',
                'remarque' => '',
                'versionable_id' => 591,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            446 => 
            array (
                'id' => 465,
                'created_at' => '2016-10-12 13:06:05',
                'updated_at' => '2016-10-12 13:06:05',
                'remarque' => '',
                'versionable_id' => 592,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            447 => 
            array (
                'id' => 466,
                'created_at' => '2016-10-12 13:06:38',
                'updated_at' => '2016-10-12 13:06:38',
                'remarque' => '',
                'versionable_id' => 593,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            448 => 
            array (
                'id' => 467,
                'created_at' => '2016-10-12 13:07:01',
                'updated_at' => '2016-10-12 13:07:01',
                'remarque' => '',
                'versionable_id' => 594,
                'versionable_type' => 'lecon',
                'user_id' => 102,
            ),
            449 => 
            array (
                'id' => 468,
                'created_at' => '2016-10-14 13:17:44',
                'updated_at' => '2016-10-14 13:17:44',
                'remarque' => '',
                'versionable_id' => 218,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            450 => 
            array (
                'id' => 469,
                'created_at' => '2016-10-14 13:36:37',
                'updated_at' => '2016-10-14 13:36:37',
                'remarque' => 'ça vient de quelque part mais d\'où ?',
                'versionable_id' => 219,
                'versionable_type' => 'developpement',
                'user_id' => 102,
            ),
            451 => 
            array (
                'id' => 471,
                'created_at' => '2016-10-28 17:27:59',
                'updated_at' => '2016-10-28 17:27:59',
                'remarque' => '',
                'versionable_id' => 220,
                'versionable_type' => 'developpement',
                'user_id' => 137,
            ),
            452 => 
            array (
                'id' => 472,
                'created_at' => '2016-12-11 08:33:16',
                'updated_at' => '2016-12-11 08:33:16',
                'remarque' => '',
                'versionable_id' => 222,
                'versionable_type' => 'developpement',
                'user_id' => 146,
            ),
            453 => 
            array (
                'id' => 473,
                'created_at' => '2016-12-12 18:42:16',
                'updated_at' => '2016-12-12 18:42:16',
                'remarque' => '',
                'versionable_id' => 749,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            454 => 
            array (
                'id' => 474,
                'created_at' => '2016-12-12 18:43:24',
                'updated_at' => '2016-12-12 18:43:24',
                'remarque' => 'Oral blanc',
                'versionable_id' => 749,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            455 => 
            array (
                'id' => 475,
                'created_at' => '2016-12-12 18:45:39',
                'updated_at' => '2016-12-12 18:45:39',
                'remarque' => 'Plan brouillon...',
                'versionable_id' => 750,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            456 => 
            array (
                'id' => 476,
                'created_at' => '2016-12-12 18:52:35',
                'updated_at' => '2016-12-12 18:52:35',
                'remarque' => '',
                'versionable_id' => 751,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            457 => 
            array (
                'id' => 477,
                'created_at' => '2016-12-12 18:53:51',
                'updated_at' => '2016-12-12 18:53:51',
                'remarque' => '',
                'versionable_id' => 752,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            458 => 
            array (
                'id' => 478,
                'created_at' => '2016-12-12 18:56:23',
                'updated_at' => '2016-12-12 18:56:23',
                'remarque' => '[Admin] N\'a même pas utilisé un Crochemore, une hérésie !',
                'versionable_id' => 753,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            459 => 
            array (
                'id' => 479,
                'created_at' => '2016-12-12 18:57:52',
                'updated_at' => '2016-12-12 18:57:52',
                'remarque' => '',
                'versionable_id' => 754,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            460 => 
            array (
                'id' => 480,
                'created_at' => '2016-12-12 18:59:19',
                'updated_at' => '2016-12-12 18:59:19',
                'remarque' => '',
                'versionable_id' => 755,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            461 => 
            array (
                'id' => 481,
                'created_at' => '2016-12-12 19:00:36',
                'updated_at' => '2016-12-12 19:00:36',
                'remarque' => '',
                'versionable_id' => 756,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            462 => 
            array (
                'id' => 482,
                'created_at' => '2016-12-12 19:02:09',
                'updated_at' => '2016-12-12 19:02:09',
                'remarque' => '',
                'versionable_id' => 757,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            463 => 
            array (
                'id' => 483,
                'created_at' => '2016-12-12 22:21:04',
                'updated_at' => '2016-12-12 22:21:04',
                'remarque' => '',
                'versionable_id' => 773,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            464 => 
            array (
                'id' => 484,
                'created_at' => '2016-12-12 22:22:21',
                'updated_at' => '2016-12-12 22:22:21',
                'remarque' => '',
                'versionable_id' => 758,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            465 => 
            array (
                'id' => 485,
                'created_at' => '2016-12-12 22:24:07',
                'updated_at' => '2016-12-12 22:24:07',
                'remarque' => '',
                'versionable_id' => 759,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            466 => 
            array (
                'id' => 486,
                'created_at' => '2016-12-12 22:25:45',
                'updated_at' => '2016-12-12 22:25:45',
                'remarque' => '',
                'versionable_id' => 760,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            467 => 
            array (
                'id' => 487,
                'created_at' => '2016-12-12 22:27:50',
                'updated_at' => '2016-12-12 22:27:50',
                'remarque' => '',
                'versionable_id' => 761,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            468 => 
            array (
                'id' => 488,
                'created_at' => '2016-12-12 22:28:51',
                'updated_at' => '2016-12-12 22:28:51',
                'remarque' => 'Oral blanc',
                'versionable_id' => 761,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            469 => 
            array (
                'id' => 489,
                'created_at' => '2016-12-12 22:30:18',
                'updated_at' => '2016-12-12 22:30:18',
                'remarque' => '',
                'versionable_id' => 762,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            470 => 
            array (
                'id' => 490,
                'created_at' => '2016-12-12 22:31:53',
                'updated_at' => '2016-12-12 22:31:53',
                'remarque' => '',
                'versionable_id' => 763,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            471 => 
            array (
                'id' => 491,
                'created_at' => '2016-12-12 22:34:57',
                'updated_at' => '2016-12-12 22:34:57',
                'remarque' => '',
                'versionable_id' => 764,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            472 => 
            array (
                'id' => 492,
                'created_at' => '2016-12-12 22:36:08',
                'updated_at' => '2016-12-12 22:36:08',
                'remarque' => '',
                'versionable_id' => 774,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            473 => 
            array (
                'id' => 493,
                'created_at' => '2016-12-12 22:38:11',
                'updated_at' => '2016-12-12 22:38:11',
                'remarque' => '',
                'versionable_id' => 765,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            474 => 
            array (
                'id' => 494,
                'created_at' => '2016-12-12 22:40:00',
                'updated_at' => '2016-12-12 22:40:00',
                'remarque' => '',
                'versionable_id' => 766,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            475 => 
            array (
                'id' => 495,
                'created_at' => '2016-12-12 22:42:27',
                'updated_at' => '2016-12-12 22:42:27',
                'remarque' => 'Oral blanc',
                'versionable_id' => 766,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            476 => 
            array (
                'id' => 496,
                'created_at' => '2016-12-12 22:55:36',
                'updated_at' => '2016-12-12 22:55:36',
                'remarque' => '',
                'versionable_id' => 767,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            477 => 
            array (
                'id' => 497,
                'created_at' => '2016-12-12 22:56:47',
                'updated_at' => '2016-12-12 22:56:47',
                'remarque' => 'Oral blanc',
                'versionable_id' => 767,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            478 => 
            array (
                'id' => 498,
                'created_at' => '2016-12-12 22:57:21',
                'updated_at' => '2016-12-12 22:57:21',
                'remarque' => '',
                'versionable_id' => 768,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            479 => 
            array (
                'id' => 499,
                'created_at' => '2016-12-12 23:01:43',
                'updated_at' => '2016-12-12 23:01:43',
                'remarque' => '',
                'versionable_id' => 769,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            480 => 
            array (
                'id' => 500,
                'created_at' => '2016-12-12 23:02:14',
                'updated_at' => '2016-12-12 23:02:14',
                'remarque' => '',
                'versionable_id' => 770,
                'versionable_type' => 'lecon',
                'user_id' => 135,
            ),
            481 => 
            array (
                'id' => 501,
                'created_at' => '2017-01-10 10:13:04',
                'updated_at' => '2017-01-10 10:13:04',
                'remarque' => '',
                'versionable_id' => 638,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            482 => 
            array (
                'id' => 502,
                'created_at' => '2017-01-10 10:14:49',
                'updated_at' => '2017-01-10 10:14:49',
                'remarque' => '',
                'versionable_id' => 639,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            483 => 
            array (
                'id' => 503,
                'created_at' => '2017-01-10 10:17:08',
                'updated_at' => '2017-01-10 10:17:08',
                'remarque' => '',
                'versionable_id' => 640,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            484 => 
            array (
                'id' => 504,
                'created_at' => '2017-01-10 10:19:21',
                'updated_at' => '2017-01-10 10:19:21',
                'remarque' => '',
                'versionable_id' => 641,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            485 => 
            array (
                'id' => 505,
                'created_at' => '2017-01-10 10:20:38',
                'updated_at' => '2017-01-10 10:20:38',
                'remarque' => '',
                'versionable_id' => 642,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            486 => 
            array (
                'id' => 506,
                'created_at' => '2017-01-10 11:16:47',
                'updated_at' => '2017-01-10 11:16:47',
                'remarque' => '',
                'versionable_id' => 643,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            487 => 
            array (
                'id' => 507,
                'created_at' => '2017-01-10 11:19:14',
                'updated_at' => '2017-01-10 11:19:14',
                'remarque' => '',
                'versionable_id' => 644,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            488 => 
            array (
                'id' => 508,
                'created_at' => '2017-01-10 11:21:58',
                'updated_at' => '2017-01-10 11:23:37',
                'remarque' => 'Leçon d\'une qualité assez faible.',
                'versionable_id' => 645,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            489 => 
            array (
                'id' => 509,
                'created_at' => '2017-01-10 11:25:10',
                'updated_at' => '2017-01-10 11:25:10',
                'remarque' => '',
                'versionable_id' => 646,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            490 => 
            array (
                'id' => 510,
                'created_at' => '2017-01-10 11:26:36',
                'updated_at' => '2017-01-10 11:27:15',
            'remarque' => '(Probablement) Oral blanc',
                'versionable_id' => 646,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            491 => 
            array (
                'id' => 511,
                'created_at' => '2017-01-10 11:27:56',
                'updated_at' => '2017-01-10 11:27:56',
                'remarque' => '',
                'versionable_id' => 647,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            492 => 
            array (
                'id' => 512,
                'created_at' => '2017-01-10 11:30:56',
                'updated_at' => '2017-01-10 11:30:56',
                'remarque' => '',
                'versionable_id' => 647,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            493 => 
            array (
                'id' => 513,
                'created_at' => '2017-01-10 11:32:57',
                'updated_at' => '2017-01-10 11:32:57',
                'remarque' => '',
                'versionable_id' => 648,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            494 => 
            array (
                'id' => 514,
                'created_at' => '2017-01-10 11:33:48',
                'updated_at' => '2017-01-10 11:34:26',
                'remarque' => 'Oral blanc',
                'versionable_id' => 649,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            495 => 
            array (
                'id' => 515,
                'created_at' => '2017-01-10 11:34:50',
                'updated_at' => '2017-01-10 11:34:50',
                'remarque' => 'Oral blanc',
                'versionable_id' => 649,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            496 => 
            array (
                'id' => 516,
                'created_at' => '2017-01-10 11:35:39',
                'updated_at' => '2017-01-10 11:35:39',
                'remarque' => '',
                'versionable_id' => 650,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            497 => 
            array (
                'id' => 517,
                'created_at' => '2017-01-10 11:36:49',
                'updated_at' => '2017-01-10 11:36:49',
                'remarque' => '',
                'versionable_id' => 651,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            498 => 
            array (
                'id' => 518,
                'created_at' => '2017-01-10 11:37:18',
                'updated_at' => '2017-01-10 11:37:18',
                'remarque' => 'Oral blanc',
                'versionable_id' => 651,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            499 => 
            array (
                'id' => 519,
                'created_at' => '2017-01-10 11:38:01',
                'updated_at' => '2017-01-10 11:38:01',
                'remarque' => '',
                'versionable_id' => 652,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
        ));
        \DB::table('versions')->insert(array (
            0 => 
            array (
                'id' => 520,
                'created_at' => '2017-01-10 11:39:22',
                'updated_at' => '2017-01-10 11:39:22',
                'remarque' => 'Plan trop long et trop complet pour être réalisable en temps limité.',
                'versionable_id' => 653,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            1 => 
            array (
                'id' => 521,
                'created_at' => '2017-01-10 11:40:04',
                'updated_at' => '2017-01-10 11:40:04',
                'remarque' => '',
                'versionable_id' => 654,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            2 => 
            array (
                'id' => 522,
                'created_at' => '2017-01-10 11:41:00',
                'updated_at' => '2017-01-10 11:41:00',
                'remarque' => '',
                'versionable_id' => 656,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            3 => 
            array (
                'id' => 523,
                'created_at' => '2017-01-10 11:41:59',
                'updated_at' => '2017-01-10 11:41:59',
                'remarque' => '',
                'versionable_id' => 657,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            4 => 
            array (
                'id' => 524,
                'created_at' => '2017-01-10 11:42:47',
                'updated_at' => '2017-01-10 11:42:47',
                'remarque' => '',
                'versionable_id' => 658,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            5 => 
            array (
                'id' => 525,
                'created_at' => '2017-01-10 11:43:44',
                'updated_at' => '2017-01-10 11:43:44',
                'remarque' => '',
                'versionable_id' => 659,
                'versionable_type' => 'lecon',
                'user_id' => 111,
            ),
            6 => 
            array (
                'id' => 526,
                'created_at' => '2017-01-10 11:46:19',
                'updated_at' => '2017-01-10 11:46:19',
                'remarque' => '',
                'versionable_id' => 660,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            7 => 
            array (
                'id' => 527,
                'created_at' => '2017-01-10 11:47:09',
                'updated_at' => '2017-01-10 11:47:09',
                'remarque' => '',
                'versionable_id' => 661,
                'versionable_type' => 'lecon',
                'user_id' => 154,
            ),
            8 => 
            array (
                'id' => 528,
                'created_at' => '2017-01-15 11:57:03',
                'updated_at' => '2017-01-15 11:57:03',
                'remarque' => '',
                'versionable_id' => 107,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            9 => 
            array (
                'id' => 529,
                'created_at' => '2017-01-15 11:59:15',
                'updated_at' => '2017-01-15 11:59:15',
                'remarque' => '',
                'versionable_id' => 828,
                'versionable_type' => 'lecon',
                'user_id' => 156,
            ),
            10 => 
            array (
                'id' => 530,
                'created_at' => '2017-01-18 20:25:31',
                'updated_at' => '2017-01-18 20:25:31',
                'remarque' => '',
                'versionable_id' => 175,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            11 => 
            array (
                'id' => 531,
                'created_at' => '2017-01-18 20:26:27',
                'updated_at' => '2017-01-18 20:26:27',
                'remarque' => '',
                'versionable_id' => 127,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            12 => 
            array (
                'id' => 532,
                'created_at' => '2017-01-18 20:27:03',
                'updated_at' => '2017-01-18 20:27:03',
                'remarque' => '',
                'versionable_id' => 64,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            13 => 
            array (
                'id' => 533,
                'created_at' => '2017-01-18 20:28:05',
                'updated_at' => '2017-01-18 20:28:05',
                'remarque' => '',
                'versionable_id' => 120,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            14 => 
            array (
                'id' => 534,
                'created_at' => '2017-01-18 20:28:44',
                'updated_at' => '2017-01-18 20:28:44',
                'remarque' => '',
                'versionable_id' => 98,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            15 => 
            array (
                'id' => 535,
                'created_at' => '2017-01-18 20:32:28',
                'updated_at' => '2017-01-18 20:32:28',
                'remarque' => '',
                'versionable_id' => 223,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            16 => 
            array (
                'id' => 536,
                'created_at' => '2017-01-18 20:33:23',
                'updated_at' => '2017-01-18 20:33:23',
                'remarque' => '',
                'versionable_id' => 14,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            17 => 
            array (
                'id' => 537,
                'created_at' => '2017-01-18 20:34:39',
                'updated_at' => '2017-01-18 20:34:39',
                'remarque' => '',
                'versionable_id' => 69,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            18 => 
            array (
                'id' => 538,
                'created_at' => '2017-01-18 20:35:35',
                'updated_at' => '2017-01-18 20:35:35',
                'remarque' => '',
                'versionable_id' => 106,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            19 => 
            array (
                'id' => 539,
                'created_at' => '2017-01-31 12:19:29',
                'updated_at' => '2017-01-31 12:19:29',
                'remarque' => '',
                'versionable_id' => 10,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            20 => 
            array (
                'id' => 540,
                'created_at' => '2017-01-31 12:22:41',
                'updated_at' => '2017-01-31 12:22:41',
                'remarque' => '',
                'versionable_id' => 224,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            21 => 
            array (
                'id' => 541,
                'created_at' => '2017-01-31 12:23:47',
                'updated_at' => '2017-01-31 12:23:47',
                'remarque' => '',
                'versionable_id' => 101,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            22 => 
            array (
                'id' => 542,
                'created_at' => '2017-01-31 12:26:40',
                'updated_at' => '2017-01-31 12:26:40',
                'remarque' => '',
                'versionable_id' => 84,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            23 => 
            array (
                'id' => 544,
                'created_at' => '2017-02-02 15:35:42',
                'updated_at' => '2017-02-02 15:35:42',
                'remarque' => '',
                'versionable_id' => 776,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            24 => 
            array (
                'id' => 545,
                'created_at' => '2017-02-02 15:36:33',
                'updated_at' => '2017-02-02 15:36:33',
                'remarque' => '',
                'versionable_id' => 780,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            25 => 
            array (
                'id' => 546,
                'created_at' => '2017-02-02 15:42:14',
                'updated_at' => '2017-02-02 15:42:14',
                'remarque' => '',
                'versionable_id' => 781,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            26 => 
            array (
                'id' => 547,
                'created_at' => '2017-02-02 15:49:19',
                'updated_at' => '2017-02-02 15:49:19',
                'remarque' => '',
                'versionable_id' => 782,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            27 => 
            array (
                'id' => 548,
                'created_at' => '2017-02-02 15:50:01',
                'updated_at' => '2017-02-02 15:50:01',
                'remarque' => '',
                'versionable_id' => 783,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            28 => 
            array (
                'id' => 549,
                'created_at' => '2017-02-02 15:50:53',
                'updated_at' => '2017-02-02 15:50:53',
                'remarque' => '',
                'versionable_id' => 784,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            29 => 
            array (
                'id' => 550,
                'created_at' => '2017-02-02 15:53:39',
                'updated_at' => '2017-02-02 15:53:39',
                'remarque' => '',
                'versionable_id' => 786,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            30 => 
            array (
                'id' => 551,
                'created_at' => '2017-02-02 15:54:23',
                'updated_at' => '2017-02-02 15:54:23',
                'remarque' => '',
                'versionable_id' => 787,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            31 => 
            array (
                'id' => 552,
                'created_at' => '2017-02-02 15:55:11',
                'updated_at' => '2017-02-02 15:55:11',
                'remarque' => '',
                'versionable_id' => 788,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            32 => 
            array (
                'id' => 553,
                'created_at' => '2017-02-02 15:56:39',
                'updated_at' => '2017-02-02 15:56:39',
                'remarque' => '',
                'versionable_id' => 790,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            33 => 
            array (
                'id' => 554,
                'created_at' => '2017-02-02 15:57:16',
                'updated_at' => '2017-02-02 15:57:16',
                'remarque' => '',
                'versionable_id' => 792,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            34 => 
            array (
                'id' => 555,
                'created_at' => '2017-02-02 16:07:57',
                'updated_at' => '2017-02-02 16:07:57',
                'remarque' => '',
                'versionable_id' => 794,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            35 => 
            array (
                'id' => 556,
                'created_at' => '2017-02-02 16:08:45',
                'updated_at' => '2017-02-02 16:08:45',
                'remarque' => '',
                'versionable_id' => 795,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            36 => 
            array (
                'id' => 557,
                'created_at' => '2017-02-02 16:12:23',
                'updated_at' => '2017-02-02 16:12:23',
                'remarque' => '',
                'versionable_id' => 796,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            37 => 
            array (
                'id' => 558,
                'created_at' => '2017-02-02 16:12:59',
                'updated_at' => '2017-02-02 16:12:59',
                'remarque' => '',
                'versionable_id' => 800,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            38 => 
            array (
                'id' => 559,
                'created_at' => '2017-02-02 16:13:37',
                'updated_at' => '2017-02-02 16:13:37',
                'remarque' => '',
                'versionable_id' => 801,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            39 => 
            array (
                'id' => 560,
                'created_at' => '2017-02-02 16:14:41',
                'updated_at' => '2017-02-02 16:14:41',
                'remarque' => '',
                'versionable_id' => 802,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            40 => 
            array (
                'id' => 561,
                'created_at' => '2017-02-02 16:15:50',
                'updated_at' => '2017-02-02 16:15:50',
                'remarque' => '',
                'versionable_id' => 803,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            41 => 
            array (
                'id' => 562,
                'created_at' => '2017-02-02 16:16:41',
                'updated_at' => '2017-02-02 16:16:41',
                'remarque' => '',
                'versionable_id' => 807,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            42 => 
            array (
                'id' => 563,
                'created_at' => '2017-02-02 16:17:38',
                'updated_at' => '2017-02-02 16:17:38',
                'remarque' => '',
                'versionable_id' => 809,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            43 => 
            array (
                'id' => 564,
                'created_at' => '2017-02-02 16:19:45',
                'updated_at' => '2017-02-02 16:19:45',
                'remarque' => '',
                'versionable_id' => 811,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            44 => 
            array (
                'id' => 565,
                'created_at' => '2017-02-02 16:20:21',
                'updated_at' => '2017-02-02 16:20:21',
                'remarque' => '',
                'versionable_id' => 816,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            45 => 
            array (
                'id' => 566,
                'created_at' => '2017-02-02 16:21:06',
                'updated_at' => '2017-02-02 16:21:06',
                'remarque' => '',
                'versionable_id' => 818,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            46 => 
            array (
                'id' => 567,
                'created_at' => '2017-02-02 16:23:03',
                'updated_at' => '2017-02-02 16:23:03',
                'remarque' => '',
                'versionable_id' => 822,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            47 => 
            array (
                'id' => 568,
                'created_at' => '2017-02-02 16:24:13',
                'updated_at' => '2017-02-02 16:24:13',
                'remarque' => '',
                'versionable_id' => 823,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            48 => 
            array (
                'id' => 569,
                'created_at' => '2017-02-02 16:25:05',
                'updated_at' => '2017-02-02 16:25:05',
                'remarque' => '',
                'versionable_id' => 824,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            49 => 
            array (
                'id' => 570,
                'created_at' => '2017-02-02 16:26:43',
                'updated_at' => '2017-02-02 16:26:43',
                'remarque' => '',
                'versionable_id' => 826,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            50 => 
            array (
                'id' => 571,
                'created_at' => '2017-02-02 16:33:31',
                'updated_at' => '2017-02-02 16:33:31',
                'remarque' => '',
                'versionable_id' => 828,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            51 => 
            array (
                'id' => 572,
                'created_at' => '2017-02-02 16:34:16',
                'updated_at' => '2017-02-02 16:34:16',
                'remarque' => '',
                'versionable_id' => 832,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            52 => 
            array (
                'id' => 573,
                'created_at' => '2017-02-02 16:34:57',
                'updated_at' => '2017-02-02 16:34:57',
                'remarque' => '',
                'versionable_id' => 833,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            53 => 
            array (
                'id' => 574,
                'created_at' => '2017-02-02 16:35:26',
                'updated_at' => '2017-02-02 16:35:50',
                'remarque' => 'Oral blanc',
                'versionable_id' => 833,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            54 => 
            array (
                'id' => 575,
                'created_at' => '2017-02-02 16:36:08',
                'updated_at' => '2017-02-02 16:36:08',
                'remarque' => '',
                'versionable_id' => 838,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            55 => 
            array (
                'id' => 576,
                'created_at' => '2017-02-02 16:37:41',
                'updated_at' => '2017-02-02 16:37:41',
                'remarque' => '',
                'versionable_id' => 839,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            56 => 
            array (
                'id' => 577,
                'created_at' => '2017-02-02 16:38:12',
                'updated_at' => '2017-02-02 16:38:12',
                'remarque' => '',
                'versionable_id' => 841,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            57 => 
            array (
                'id' => 578,
                'created_at' => '2017-02-02 16:43:31',
                'updated_at' => '2017-02-02 16:43:31',
                'remarque' => '',
                'versionable_id' => 845,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            58 => 
            array (
                'id' => 579,
                'created_at' => '2017-02-02 16:44:07',
                'updated_at' => '2017-02-02 16:44:07',
                'remarque' => '',
                'versionable_id' => 849,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            59 => 
            array (
                'id' => 580,
                'created_at' => '2017-02-02 16:44:51',
                'updated_at' => '2017-02-02 16:44:51',
                'remarque' => '',
                'versionable_id' => 862,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            60 => 
            array (
                'id' => 581,
                'created_at' => '2017-02-02 16:45:29',
                'updated_at' => '2017-02-02 16:45:29',
                'remarque' => '',
                'versionable_id' => 868,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            61 => 
            array (
                'id' => 582,
                'created_at' => '2017-02-03 11:57:23',
                'updated_at' => '2017-02-03 11:57:23',
                'remarque' => '',
                'versionable_id' => 70,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            62 => 
            array (
                'id' => 583,
                'created_at' => '2017-02-03 15:14:02',
                'updated_at' => '2017-02-03 15:14:02',
                'remarque' => '',
                'versionable_id' => 225,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            63 => 
            array (
                'id' => 584,
                'created_at' => '2017-02-06 10:18:52',
                'updated_at' => '2017-02-06 10:18:52',
                'remarque' => '',
                'versionable_id' => 60,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            64 => 
            array (
                'id' => 585,
                'created_at' => '2017-02-13 14:11:35',
                'updated_at' => '2017-02-13 14:11:35',
                'remarque' => '',
                'versionable_id' => 16,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            65 => 
            array (
                'id' => 586,
                'created_at' => '2017-02-13 14:15:20',
                'updated_at' => '2017-02-13 14:15:20',
                'remarque' => '',
                'versionable_id' => 805,
                'versionable_type' => 'lecon',
                'user_id' => 147,
            ),
            66 => 
            array (
                'id' => 587,
                'created_at' => '2017-02-14 08:34:40',
                'updated_at' => '2017-02-14 08:34:40',
                'remarque' => '',
                'versionable_id' => 182,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            67 => 
            array (
                'id' => 588,
                'created_at' => '2017-02-18 20:19:15',
                'updated_at' => '2017-02-18 20:19:15',
                'remarque' => '',
                'versionable_id' => 91,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            68 => 
            array (
                'id' => 589,
                'created_at' => '2017-02-18 20:32:12',
                'updated_at' => '2017-02-18 20:32:12',
                'remarque' => '',
                'versionable_id' => 226,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            69 => 
            array (
                'id' => 590,
                'created_at' => '2017-02-18 20:45:08',
                'updated_at' => '2017-02-18 20:45:08',
                'remarque' => '',
                'versionable_id' => 227,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            70 => 
            array (
                'id' => 591,
                'created_at' => '2017-02-19 20:03:03',
                'updated_at' => '2017-02-19 20:03:03',
                'remarque' => '',
                'versionable_id' => 228,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            71 => 
            array (
                'id' => 592,
                'created_at' => '2017-02-19 20:09:49',
                'updated_at' => '2017-02-19 20:09:49',
                'remarque' => 'Avec quelques bonus.',
                'versionable_id' => 104,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            72 => 
            array (
                'id' => 593,
                'created_at' => '2017-02-19 20:16:59',
                'updated_at' => '2017-02-19 20:16:59',
                'remarque' => '',
                'versionable_id' => 229,
                'versionable_type' => 'developpement',
                'user_id' => 140,
            ),
            73 => 
            array (
                'id' => 594,
                'created_at' => '2017-02-28 08:09:52',
                'updated_at' => '2017-02-28 08:09:52',
                'remarque' => '',
                'versionable_id' => 806,
                'versionable_type' => 'lecon',
                'user_id' => 156,
            ),
            74 => 
            array (
                'id' => 595,
                'created_at' => '2017-02-28 08:12:47',
                'updated_at' => '2017-02-28 08:12:47',
                'remarque' => '',
                'versionable_id' => 791,
                'versionable_type' => 'lecon',
                'user_id' => 156,
            ),
            75 => 
            array (
                'id' => 596,
                'created_at' => '2017-02-28 08:15:00',
                'updated_at' => '2017-02-28 08:15:00',
                'remarque' => '',
                'versionable_id' => 823,
                'versionable_type' => 'lecon',
                'user_id' => 156,
            ),
            76 => 
            array (
                'id' => 597,
                'created_at' => '2017-02-28 11:11:26',
                'updated_at' => '2017-02-28 11:11:26',
                'remarque' => '',
                'versionable_id' => 67,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            77 => 
            array (
                'id' => 598,
                'created_at' => '2017-02-28 15:11:17',
                'updated_at' => '2017-02-28 15:11:17',
                'remarque' => '',
                'versionable_id' => 231,
                'versionable_type' => 'developpement',
                'user_id' => 156,
            ),
            78 => 
            array (
                'id' => 600,
                'created_at' => '2017-03-05 11:22:14',
                'updated_at' => '2017-03-05 11:22:14',
                'remarque' => 'Référence : Algèbre corporelle, Antoine Chambert-Loir',
                'versionable_id' => 232,
                'versionable_type' => 'developpement',
                'user_id' => 150,
            ),
            79 => 
            array (
                'id' => 601,
                'created_at' => '2017-03-09 10:11:55',
                'updated_at' => '2017-03-09 10:11:55',
                'remarque' => 'Plan présenté à la préparation de Rennes 1.',
                'versionable_id' => 797,
                'versionable_type' => 'lecon',
                'user_id' => 140,
            ),
            80 => 
            array (
                'id' => 602,
                'created_at' => '2017-03-09 14:50:08',
                'updated_at' => '2017-03-09 14:50:08',
            'remarque' => 'Adapté du Chambert-Loir (intérêt porté sur une racine simple, et avec un petit dessin).',
                'versionable_id' => 108,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            81 => 
            array (
                'id' => 603,
                'created_at' => '2017-03-09 15:01:32',
                'updated_at' => '2017-03-09 15:01:32',
                'remarque' => 'Preuve différente, similaire à celle de la cyclicité des inversibles de F_q. Se recase parfaitement dans "Corps finis".
Oubli : Pour mq un poly unitaire de degré d a au plus d racines dans un anneau intègre pas forcément commutatif, il faut utiliser des divisions euclidiennes à gauche.',
                'versionable_id' => 112,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            82 => 
            array (
                'id' => 604,
                'created_at' => '2017-03-09 15:07:49',
                'updated_at' => '2017-03-09 15:07:49',
                'remarque' => '',
                'versionable_id' => 48,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            83 => 
            array (
                'id' => 605,
                'created_at' => '2017-03-09 15:08:08',
                'updated_at' => '2017-03-09 15:08:08',
                'remarque' => '',
                'versionable_id' => 182,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            84 => 
            array (
                'id' => 606,
                'created_at' => '2017-03-09 15:09:22',
                'updated_at' => '2017-03-09 15:09:22',
                'remarque' => '',
                'versionable_id' => 101,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            85 => 
            array (
                'id' => 607,
                'created_at' => '2017-03-09 15:27:14',
                'updated_at' => '2017-03-09 15:27:14',
                'remarque' => 'La partie sur le noyau de reproduction peut être écartée si nécessaire.
Ref : Bayen,Margaria ; Espaces de Hilbert et opérateurs ; p104.',
                'versionable_id' => 233,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            86 => 
            array (
                'id' => 608,
                'created_at' => '2017-03-09 15:44:22',
                'updated_at' => '2017-03-09 15:44:22',
                'remarque' => 'Entre théorie des opérateurs et holomorphie.
Ref : Joel Shapiro, Composition operators and classical function theory',
                'versionable_id' => 234,
                'versionable_type' => 'developpement',
                'user_id' => 174,
            ),
            87 => 
            array (
                'id' => 612,
                'created_at' => '2017-03-14 17:46:28',
                'updated_at' => '2017-03-14 17:46:28',
                'remarque' => 'Vus les recasages proposés c\'est certainement la version Briane-pagès, plutôt accès mesure, on redémontre par exemple Minkowski généralisé. 
Avantage: le cas p égal à l\'infini est correctement traité ici, contrairement à Brézis.',
                'versionable_id' => 237,
                'versionable_type' => 'developpement',
                'user_id' => 181,
            ),
        ));
        
        
    }
}
