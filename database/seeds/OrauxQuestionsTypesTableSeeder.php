<?php

use Illuminate\Database\Seeder;

class OrauxQuestionsTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oraux_questions_types')->delete();
        
        \DB::table('oraux_questions_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nom' => 'Texte',
                'fnom' => 'texte',
            ),
            1 => 
            array (
                'id' => 2,
                'nom' => 'Leçon d\'algèbre',
                'fnom' => 'lecon_algebre',
            ),
            2 => 
            array (
                'id' => 3,
                'nom' => 'Leçon d\'analyse',
                'fnom' => 'lecon_analyse',
            ),
            3 => 
            array (
                'id' => 4,
                'nom' => 'Leçon de maths-info',
                'fnom' => 'lecon_maths_info',
            ),
            4 => 
            array (
                'id' => 5,
                'nom' => 'Leçon d\'informatique',
                'fnom' => 'lecon_info',
            ),
            5 => 
            array (
                'id' => 6,
                'nom' => 'Développement',
                'fnom' => 'developpement',
            ),
            6 => 
            array (
                'id' => 7,
                'nom' => 'Liste de développements',
                'fnom' => 'developpement_liste',
            ),
            7 => 
            array (
                'id' => 8,
                'nom' => 'Liste de référence',
                'fnom' => 'reference_liste',
            ),
            8 => 
            array (
                'id' => 9,
                'nom' => 'Note',
                'fnom' => 'note',
            ),
        ));
        
        
    }
}
