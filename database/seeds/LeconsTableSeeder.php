<?php

use Illuminate\Database\Seeder;

class LeconsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lecons')->delete();
        
        \DB::table('lecons')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                'rapport' => '
Des exemples de nature différente doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel, sur un ensemble de matrices, sur des polynômes. Les exemples issus de la géométrie ne manquent pas. Par ailleurs, il ne faut pas confondre exemples et remarques générales. Les actions naturelles de $PGDL(2,F_q)$ sur les droites du plan donnent des injections intéressantes pour $q = 2,3$.
',
                'options' => 7,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples et applications des notions de sous-groupe distingué et de groupe quotient.',
                'rapport' => '
Les candidats parlent de groupe smple et de sous-groupe dérivé ou de groupe quotient sans savoir utiliser ces notions. La notion de produit semi-direct n\'est plus au programme, mais lorsqu\'elle est utilisée il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral $D_n$ par exemple).
',
                'options' => 7,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => '
Les exemples doivent figurer en bonne place dans cette leçon. On peut par exemple étudier les groupes de symétries $A_4, S_4, A_5$ et relier sur ces exemples géométrie et algèbre. La structure des groupes abéliens finis doit être connue.
',
                'options' => 15,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                'rapport' => '
Le groupe symétrique n\'est pas spécialement plus facile à comprendre que les autres groupes.
Il faut relier rigoureusement les notions d\'orbites et d\'action de groupe et savoir décomposer une permutation en cycles disjoints. Des dessins ou des graphes illustrent de manière commode ce que sont les permutations. Par ailleurs un candidat qui se propose de démontrer que tout groupe simple d\'ordre 60 est isomorphe à $A_5$ devrait aussi montrer que $A_5$ est simple.

L\'existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l\'objet d\'un développement.

Comme pour toute structure algébrique, il est souhaitable de s\'intéresser aux automorphismes du groupe symétrique. Les applications du groupe symétrique ne concernent pas seulement les polyèdres réguliers.
',
                'options' => 15,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
                'rapport' => '
Il faut savoir réaliser $S_n$ dans $GL(n,R)$ et faire le lien entre signature et déterminant.

Cette leçon est souvent présentée comme un catalogue de résultats épars et zoologiques sur $GL(E)$. Il faudrait que les candidats sachent faire correspondre, sous-groupes et noyaux ou stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). À quoi peuvent servir des générateurs du groupe $GL(E)$ ? Qu\'apport la topologie dans cette leçon ? Il est préférable de se poser ces questions avant de les découvrir le jour de l\'oral.
',
                'options' => 15,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => '',
                'options' => 7,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => '
Peu de candidats voient l\'utilité des parties génératrices dans l\'analyse des morphismes de groupes.
',
                'options' => 15,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications',
                'rapport' => '
Cette leçon classique demande toutefois une préparation minutieuse. Tout d\'abord $n$ n\'est pas forcément un nombre premier. Il serait bon de connaître les sous-groupes de $Z/nZ$ et les morphismes de groupes de $Z/nZ$ dans $Z/mZ$.

Bien maîtriser le lemme chinois et sa réciproque. Distinguer clairement propriétés de groupes additifs et d\'anneaux. Connaître les automorphismes, les nilpotents, les idempotents. Enfin, les candidats sont invités à rendre hommage à Gauss en présentant quelques applications arithmétiques des anneaux $Z/nZ$, telles l\'étude de quelques équations diophantiennes bien choisies.
',
                'options' => 15,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 110,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
                'rapport' => '
Il faut savoir si 113 est un nombre premier ! Attention aux choix des développements, ils doivent être pertinents (l\'apparition d\'un nombre premier n\'est pas suffisant!). La réduction modulo $p$ n\'est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important, qu\'il faudrait citer. Sa démonstration n\'est bien-sûr pas exigible au niveau de l\'Agrégation.
',
                'options' => 15,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 111,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Applications.',
                'rapport' => '
On peut aussi donner des exemples d’anneaux non principaux. Les plans sont trop théoriques. Il est possible de présenter des exemples d’anneaux principaux classiques autres que $Z$ et $K[X]$, accompagnés d’une description de leurs irréductibles. Les applications en algèbre linéaire ne manquent pas, il serait bon que les candidats les illustrent.
',
                'options' => 7,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 112,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => '
Un candidat qui étudie les carrés dans un corps fini doit savoir aussi résoudre les équations de degré 2. Les constructions des corps de petit cardinal doivent avoir été pratiquées. Les injections des divers $F_q$ doivent être connues.

Le théorème de Wedderburn ne doit pas constituer le seul développement de cette leçon. En revanche, les applications des corps finis ne doivent pas être négligées. Le théorème de l’élément primitif, s’il est énoncé, doit pouvoir être utilisé.
',
                'options' => 15,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 113,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
                'rapport' => '
Les propriétés des polynômes cyclotomiques doivent être énoncées. Leur irréductibilité sur $Z$ doit être maîtrisée. Il est tout à fait possible de parler d’exponentielle complexe, de théorème du relèvement ou de séries de Fourier tout en veillant à rester dans le contexte de la leçon.
',
                'options' => 7,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 114,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => '
C\'est une leçon qui doit être illustrée par de nombreux exemples et applications; combinatoire, calcul des sommes de Newton, relations de récurrence, etc.
',
                'options' => 7,
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 116,
                'type_id' => 1,
                'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                'rapport' => '
Les applications ne concernent pas que les corps finis. Il existe des corps algébriquement clos de caractéristique nulle autre que $C$. Un polynôme réductible n’admet pas forcément de racines. Il est instructif de chercher des polynômes irréductibles de degré 2, 3, 4 sur $F_2$ .
',
                'options' => 15,
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 117,
                'type_id' => 1,
            'nom' => 'Algèbre des polynômes à $n$ indéterminées ($n \\ge 2$). Polynômes symétriques. Applications.',
                'rapport' => '
La leçon ne doit pas se concentrer exclusivement sur les aspects formels ni sur les les polynômes symétriques. Les aspects arithmétiques ne doivent pas être négligés.

Le théorème fondamental sur la structure de l’algèbre des polynômes symétriques est vrai sur $Z$. L’algorithme peut être présenté sur un exemple.

Les applications aux quadriques, aux relations racines cœfficients ne doivent pas être négligées. On peut faire agir le groupe $GL(n, R)$ sur les polynômes de degré inférieur à 2.
',
                'options' => 7,
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 118,
                'type_id' => 1,
                'nom' => 'Exemples d\'utilisation de la notion de dimension d\'un espace vectoriel.',
                'rapport' => '',
                'options' => 7,
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 119,
                'type_id' => 1,
                'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                'rapport' => '
Cette leçon n\'a pas souvent été prise, elle demande un certain recul.
',
                'options' => 15,
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 120,
                'type_id' => 1,
            'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                'rapport' => '
C’est une leçon qui contrairement aux apparences est devenue difficile pour les candidats. Il faut absolument la préparer avec méthode. Nombre d’entre eux n’ont pas été capable de donner des réponses satisfaisantes à des questions élémentaires comme : un sous-espace vectoriel d’un espace vectoriel de dimension finie, est-il aussi de dimension finie ?
',
                'options' => 15,
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Déterminant. Exemples et applications.',
                'rapport' => '
Il faut que le plan soit cohérent ; si le déterminant n’est défini que sur $R ou $C$ il est délicat de définir $det(A − X I_n )$ avec $A$ une matrice carrée. L’interprétation du déterminant en terme de volume est essentielle. Le jury ne peut se contenter d’un Vandermonde ou d’un déterminant circulant ! Le résultant et les applications simples à l’intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon. D’une manière générale on attend pendant le développement l’illustration d’un calcul ou la manipulation de déterminants non triviaux.
',
                'options' => 15,
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                'rapport' => '
Le titre officiel précise que la dimension est finie. Les polynômes d’un endomorphisme ne sont pas tous nuls ! Il faut consacrer une courte partie de la leçon à l’algèbre $K[u]$, connaître sa dimension sans hésiter. Les propriétés globales pourront être étudiées par les meilleurs. Le jury souhaiterait voir certains liens entre réduction et structure de cette algèbre $K[u]$. Le candidat peut s’interroger sur les idempotents et le lien avec la décomposition en somme de sous-espaces caractéristiques. 

Le jury ne souhaite pas avoir un catalogue de résultats autour de la réduction, mais seulement ce qui a trait aux polynômes d’endomorphismes. Il faut bien préciser que dans la réduction de Dunford, les composantes sont des polynômes en l’endomorphisme.

L’aspect applications est trop souvent négligé.
',
                'options' => 15,
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Sous-espaces stables par un endomorphisme d\'un espace vectoriel de dimension finie. Applications.',
                'rapport' => '
Les candidats doivent s’être interrogés sur les propriétés de l’ensemble des sous-espaces stables par un endomorphisme. Des études de cas détaillées sont les bienvenues.
',
                'options' => 7,
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 126,
                'type_id' => 1,
                'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => '
Il faut pouvoir donner des exemples naturels d’endomorphismes diagonalisables et des critères. Le calcul de l’exponentielle d’un endomorphisme diagonalisable est immédiat une fois que l’on connaît les valeurs propres et ceci sans diagonaliser la matrice.
',
                'options' => 7,
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 127,
                'type_id' => 1,
                'nom' => 'Exponentielle de matrices. Applications.',
                'rapport' => '
C’est une leçon difficile et ce n’est pas une leçon d’analyse. Il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. Les questions de surjectivité ou d’injectivité doivent être abordées. Par exemple la matrice $A = \\begin{vmatrix}
-1 \\esperluette 1 \\\\
0 \\esperluette -1 
\\end{vmatrix}$ est-elle dans l’image $exp(Mat (2, R))$. Qu’en est-il de la matrice blocs $B = \\begin{vmatrix}
A \\esperluette 0 \\\\
0 \\esperluette A
\\end{vmatrix}$ ?

La décomposition de Dunford multiplicative (décomposition de Jordan) de exp(A) doit être connue. Les groupes à un paramètre peuvent trouver leur place dans cette leçon. On peut s’interroger si ces sous-groupes constituent des sous-variétés fermées de GL(n, R). L’étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités.

Les applications aux équations différentielles doivent être évoquées sans constituer l’essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique.

Les notions d’algèbres de Lie ne sont pas au programme de l’Agrégation, on conseille de n’aborder ces sujets qu’à condition d’avoir une certaine solidité.
',
                'options' => 7,
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 128,
                'type_id' => 1,
                'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                'rapport' => '
Il est possible de mener une leçon de bon niveau, même sans la décomposition de Jordan à l’aide des noyaux itérés.
',
                'options' => 15,
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 130,
                'type_id' => 1,
                'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                'rapport' => '
C’est une leçon transversale. La notion de signature doit figurer dans la leçon. On doit faire le lien avec les formes quadratiques et les formes hermitiennes. La partie réelle et la partie imaginaire d’un produit hermitien définissent des structures sur l’espace vectoriel réel sous-jacent.
',
                'options' => 7,
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 131,
                'type_id' => 1,
                'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                'rapport' => '
Le candidat ne doit pas se contenter de travailler sur $R$ et ne doit pas négliger l’interprétation géométrique des notions introduites (lien entre coniques, formes quadratiques, cônes isotropes) ou les aspects élémentaires (par exemple le discriminant de l’équation ax 2 +bx +c = 0 et la signature de la forme quadratique ax 2 +bx y +c y 2 ). On ne peut se limiter à des considérations élémentaires d’algèbre linéaire. Les formes quadratiques ne sont pas toutes non dégénérées (la notion de quotient est utile pour s’y ramener).

L’algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique de $R^3$. Le lien avec la signature doit être clairement énoncé. Malheureusement la notion d’isotropie est mal maîtrisée par les candidats, y compris les meilleurs d’entre eux. Le cône isotrope est un aspect important de cette leçon, qu’il faut rattacher à la géométrie différentielle. Il est important d’illustrer cette leçon d’exemples naturels.
',
                'options' => 15,
            ),
            26 => 
            array (
                'id' => 27,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 132,
                'type_id' => 1,
                'nom' => 'Formes linéaires et hyperplans en dimension finie. Exemples et applications.',
                'rapport' => '
Il est important de replacer la thématique de la dualité dans cette leçon. Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d’une intersection d’hyperplans est au cœur de la leçon. L’utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d’obtenir les équations d’un sous-espace vectoriel ou d’exhiber une base d’une intersection d’hyperplans. Cette leçon peut être traitée sous différents aspects : géométrie, algèbre, topologie, analyse etc. Il faut que les développements proposés soient en lien direct, comme toujours, avec la leçon ; proposer la trigonalisation simultanée est un peu osé ! Enfin rappeler que la différentielle d’une fonction réelle est une forme linéaire semble incontournable.
',
                'options' => 15,
            ),
            27 => 
            array (
                'id' => 28,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 133,
                'type_id' => 1,
            'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                'rapport' => '',
                'options' => 15,
            ),
            28 => 
            array (
                'id' => 29,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:36',
                'annee' => 2010,
                'numero' => 135,
                'type_id' => 1,
                'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Forme réduite. Applications en dimensions $2$ et $3$.',
                'rapport' => '
La classification des isométries en dimension 2 ou 3 est exigible ainsi que le théorème de décomposition commutative. En dimension 3 : déplacements (translation, rotations, vissage) ; antidéplacements (symétries planes, symétries glissées, et isométrie négative à point fixe unique).
',
                'options' => 7,
            ),
            29 => 
            array (
                'id' => 30,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 136,
                'type_id' => 1,
                'nom' => 'Coniques. Applications.',
                'rapport' => '
La définition des coniques affines non dégénérées doit être connue. Les propriétés classiques des coniques doivent être présentées. Bien distinguer les notions affines, métriques ou projectives.
',
                'options' => 7,
            ),
            30 => 
            array (
                'id' => 31,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 137,
                'type_id' => 1,
                'nom' => 'Barycentres dans un espace affine réel de dimension finie; convexité. Applications.',
                'rapport' => '
On attend des candidats qu’ils parlent de coordonnées barycentriques et les utilisent par exemple dans le triangle (coordonnées barycentriques de certains points remarquables).
',
                'options' => 15,
            ),
            31 => 
            array (
                'id' => 32,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 139,
                'type_id' => 1,
                'nom' => 'Applications des nombres complexes à la géométrie.',
                'rapport' => '
Cette leçon ne saurait rester au niveau de la Terminale. Une étude de l’exponentielle complexe et des homographies de la sphère de Riemann est tout à fait appropriée.
',
                'options' => 15,
            ),
            32 => 
            array (
                'id' => 33,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 140,
                'type_id' => 1,
                'nom' => 'Systèmes d\'équations linéaires. Systèmes échelonnés. Résolution. Exemples et applications.',
                'rapport' => '
Le jury n’attend pas une version à l’ancienne articulée autour du théorème de Rouché-Fontené qui n’est pas d’un grand intérêt dans sa version traditionnellement exposée.

La leçon doit impérativement présenter la notion de système échelonné, avec une définition précise et correcte et situer l’ensemble dans le contexte de l’algèbre linéaire (sans oublier la dualité !).

Par exemple les relations de dépendances linéaires sur les colonnes d’une matrice échelonnée sont claires et permettent de décrire simplement les orbites de l’action à gauche de GL(n, K) sur $M_n (K)$ donnée par $(P, A) \\longmapsto P A$. Le candidat doit pourvoir écrire un système d’équations de l’espace vectoriel engendré par les colonnes.

Un point de vue opératoire doit accompagner l’étude théorique et l’intérêt pratique (algorithmique) des méthodes présentées doit être expliqué.
',
                'options' => 15,
            ),
            33 => 
            array (
                'id' => 34,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 141,
                'type_id' => 1,
                'nom' => 'Utilisation des groupes en géométrie.',
                'rapport' => '
C’est une leçon transversale et difficile qui peut aborder des aspects variés selon les structures algébriques présentes. On ne peut prétendre avoir une bonne note si elle n’est pas préparée.
',
                'options' => 15,
            ),
            34 => 
            array (
                'id' => 35,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 144,
                'type_id' => 1,
                'nom' => 'Problèmes d\'angles et de distances en dimension $2$ ou $3$.',
                'rapport' => '',
                'options' => 7,
            ),
            35 => 
            array (
                'id' => 36,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 145,
                'type_id' => 1,
                'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                'rapport' => '
Il faut dans un premier temps dégager clairement les méthodes et les illustrer d’exemples significatifs. L’utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. Le jury s’attend à ce que les candidats sachent calculer des cardinaux classiques et certaines probabilités !
',
                'options' => 15,
            ),
            36 => 
            array (
                'id' => 37,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 146,
                'type_id' => 1,
                'nom' => 'Résultant. Applications.',
                'rapport' => '
Il faut soigner la présentation et ne pas perdre de vue l’application linéaire sous-jacente $(U ,V ) \\longmapsto AU + BV$ qui lie le résultant et le PGCD de A et B.
',
                'options' => 7,
            ),
            37 => 
            array (
                'id' => 38,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 148,
                'type_id' => 1,
                'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                'rapport' => '
La preuve de la loi d’inertie de Silvester doit être connue et le candidat doit avoir compris la signification géométrique de ces deux entiers composant la signature d’une forme quadratique réelle. La différentielle seconde d’une fonction de plusieurs variables est une forme quadratique importante.
',
                'options' => 7,
            ),
            38 => 
            array (
                'id' => 39,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 149,
                'type_id' => 1,
                'nom' => 'Représentations de groupes finis de petit cardinal.',
                'rapport' => '',
                'options' => 7,
            ),
            39 => 
            array (
                'id' => 40,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 201,
                'type_id' => 2,
                'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            40 => 
            array (
                'id' => 41,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 202,
                'type_id' => 2,
                'nom' => 'Exemples de parties denses et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            41 => 
            array (
                'id' => 42,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 203,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de compacité.',
                'rapport' => '
Il est important de ne pas concentrer la leçon sur la compacité générale (confusion générale entre utilisation de la notion compacité et notion de compacité), sans proposer des exemples significatifs d’utilisation (Stone-Weierstrass, point fixe, voire étude qualitative d’équations différentielles, etc.).
',
                'options' => 15,
            ),
            42 => 
            array (
                'id' => 43,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 204,
                'type_id' => 2,
                'nom' => 'Connexité. Exemples et applications.',
                'rapport' => '
Il est important de présenter des résultats naturels dont la démonstration utilise la connexité. Bien distinguer connexité et connexité par arcs.
',
                'options' => 7,
            ),
            43 => 
            array (
                'id' => 44,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 205,
                'type_id' => 2,
                'nom' => 'Espaces complets. Exemples et applications.',
                'rapport' => '
Le théorème de Baire trouvera évidemment sa place, mais il faut l’illustrer par des applications.
',
                'options' => 7,
            ),
            44 => 
            array (
                'id' => 45,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 206,
                'type_id' => 2,
                'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                'rapport' => '
Les applications aux équations différentielles sont importantes. Il faut préparer des contre-exemples pour illustrer la nécessité des hypothèses.
',
                'options' => 15,
            ),
            45 => 
            array (
                'id' => 46,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 207,
                'type_id' => 2,
                'nom' => 'Prolongement de fonctions. Exemples et applications.',
                'rapport' => '
Les questions liées au prolongement analytique font partie de la leçon.
',
                'options' => 7,
            ),
            46 => 
            array (
                'id' => 47,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 208,
                'type_id' => 2,
                'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                'rapport' => '
La justification de la compacité de la boule unité en dimension finie doit être donnée.
',
                'options' => 15,
            ),
            47 => 
            array (
                'id' => 48,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 213,
                'type_id' => 2,
                'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                'rapport' => '
Il est important de faire la différence entre base algébrique et base hilbertienne. Il faut connaître quelques critères simples pour qu’une famille orthogonale forme une base hilbertienne. Le théorème de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d’un espace de Hilbert H est régulièrement mentionné. En revanche, la possibilité de construire de façon élémentaire le dit-projeté dans le cas particulier d’un sous-espace vectoriel de dimension finie semble inconnue de nombreux candidats. Les candidats doivent s’intéresser au sens  $x = \\sum_{ n \\ge 0} ( x | e_n) e_n$ et  $||x||^2 = \\sum_{ n \\ge 0} (x|e_n)^2$ en précisant les hypothèses sur la famille $(e_n)_{n \\in \\mathbb{N}} $ et en justifiant la convergence.
',
                'options' => 7,
            ),
            48 => 
            array (
                'id' => 49,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 214,
                'type_id' => 2,
                'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
                'rapport' => '
On attend des applications en géométrie différentielle (notamment dans la formulation des multiplicateurs de Lagrange). Rappelons que les sous-variétés sont au programme.
',
                'options' => 15,
            ),
            49 => 
            array (
                'id' => 50,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 215,
                'type_id' => 2,
                'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                'rapport' => '
Il faudrait que les candidats à l’Agrégation sachent que les différentielles d’ordre supérieur $d^k f (a)$ définissent des applications $k$-linéaires (sur quel espace ?). Il faut savoir calculer sur des exemples simples, la différentielle d’une fonction ou effectuer un développement limité à l’ordre 1 d’une fonction.
',
                'options' => 15,
            ),
            50 => 
            array (
                'id' => 51,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 216,
                'type_id' => 2,
                'nom' => 'Étude métrique des courbes. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            51 => 
            array (
                'id' => 52,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 217,
                'type_id' => 2,
                'nom' => 'Sous-variétés de $R^n$. Exemples.',
                'rapport' => '
Cette leçon n’a pas eu beaucoup de succès, c’est bien dommage. Elle ne saurait être réduite à un cours de géométrie différentielle abstraite ; ce serait un contresens. Le jury attend une leçon concrète, montrant une compréhension géométrique locale. Aucune notion globale n’est exigible, ni de notion de variété abstraite. Le candidat doit pouvoir être capable de donner plusieurs représentations locales (paramétriques, équations, etc.) et d’illustrer la notion d’espace tangent sur des exemples classiques. Le jury invite les candidats à réfléchir à la pertinence de l’introduction de la notion de sous-variétés. En ce qui concerne les surfaces de $R^3$ , les candidats sont invités à réfléchir aux notions de formes quadratiques fondamentales et à leurs interprétations géométriques. 
Le théorème des extrema liés peut être évoqué dans cette leçon. Les groupes classiques donnent des exemples utiles de sous-variétés.
',
                'options' => 7,
            ),
            52 => 
            array (
                'id' => 53,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 218,
                'type_id' => 2,
                'nom' => 'Applications des formules de Taylor.',
                'rapport' => '
Il faut connaître les formules de Taylor des polynômes et certains développements très classiques. Il y a de très nombreuses applications en géométrie et probabilités (le théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités$ || f^(k) || \\le 2^{k(n−k)/2} || f ||^{ 1−k/n} || f^(n) ||^{ k/n}$ (lorsque f et sa dérivée n-ème sont bornées). On soignera particulièrement le choix des développements.
',
                'options' => 15,
            ),
            53 => 
            array (
                'id' => 54,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 219,
                'type_id' => 2,
                'nom' => 'Problèmes d\'extremums.',
                'rapport' => '
Bien faire la distinction entre propriétés locales (caractérisation d’un extremum) et globales (existence).
',
                'options' => 15,
            ),
            54 => 
            array (
                'id' => 55,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 220,
                'type_id' => 2,
            'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'études qualitatives des solutions.',
                'rapport' => '
Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est rarement énoncé. L’utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en oeuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées.
',
                'options' => 15,
            ),
            55 => 
            array (
                'id' => 56,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 221,
                'type_id' => 2,
                'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                'rapport' => '
Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. L’utilisation des exponentielles de matrices doit pouvoir s’expliquer. Dans le cas général on peut évoquer les généralisations de l’exponentielle ( résolvante) via les intégrales itérées.
',
                'options' => 15,
            ),
            56 => 
            array (
                'id' => 57,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 223,
                'type_id' => 2,
                'nom' => 'Convergence des suites numériques. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            57 => 
            array (
                'id' => 58,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 224,
                'type_id' => 2,
                'nom' => 'Comportement asymptotique de suites numériques. Rapidité de convergence. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            58 => 
            array (
                'id' => 59,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 226,
                'type_id' => 2,
            'nom' => 'Comportement d\'une suite réelle ou vectorielle définie par une itération $u_{n+1} = f(u_n)$. Exemples.',
                'rapport' => '
Le jury attend d’autres exemples que la traditionnelle suite récurrente $u_{n+1} = sin(u _n )$. L’étude des suites homographiques pose des problèmes si on se restreint à $R$ ou $C$. Il ne faut pas négliger la recherche préalable de sous-ensembles (intervalles) stables par $f$ .
',
                'options' => 15,
            ),
            59 => 
            array (
                'id' => 60,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 228,
                'type_id' => 2,
                'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
                'rapport' => '
Un plan découpé en deux parties (I - Continuité, II - Dérivabilité) n’est pas le mieux adapté. Enfin les applications du théorème d’Ascoli (par exemple les opérateurs intégraux à noyau continu), le théorème de Peano, etc. sont les bienvenues.
',
                'options' => 7,
            ),
            60 => 
            array (
                'id' => 61,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 229,
                'type_id' => 2,
                'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                'rapport' => '
Les candidats sont invités à réfléchir à l’incidence de ces notions en théorie des probabilités. La dérivabilité presque partout des fonctions monotones est un résultat important. Le jury souhaiterait que les candidats illustrent leurs propos et raisonnements sur les fonctions convexes par des dessins clairs. Il n’est pas déraisonnable de parler de fonctions à variation bornée.
',
                'options' => 15,
            ),
            61 => 
            array (
                'id' => 62,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 230,
                'type_id' => 2,
                'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                'rapport' => '
L’étude de la convergence d’une série élémentaire par une hiérarchisation des méthodes et par la vérification des hypothèses correspondantes est appréciée du jury. 
Il faut soigner la présentation du plan et ne pas oublier les valeurs absolues lorsqu’on veut énoncer un théorème de convergence absolue (même remarque pour l’intégration).
Le jury demande que les candidats ne confondent pas équivalents et développements asymptotiques.
Les meilleurs pourront invoquer les méthodes classiques de renormalisation des séries divergentes.
',
                'options' => 15,
            ),
            62 => 
            array (
                'id' => 63,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 232,
                'type_id' => 2,
            'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                'rapport' => '
Le jury attire l’attention sur le fait que $X$ peut désigner un vecteur.
',
                'options' => 15,
            ),
            63 => 
            array (
                'id' => 64,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 234,
                'type_id' => 2,
                'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                'rapport' => '
Le jury a apprécié les candidats sachant montrer qu’avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p \\subset L^q $ si $p \\ge q$). Il est important de pouvoir justifier l’existence de produits de convolution (exemple $L^1 \\star L^1$ ).
',
                'options' => 7,
            ),
            64 => 
            array (
                'id' => 65,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 235,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions intégrables. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            65 => 
            array (
                'id' => 66,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 236,
                'type_id' => 2,
                'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                'rapport' => '',
                'options' => 15,
            ),
            66 => 
            array (
                'id' => 67,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 238,
                'type_id' => 2,
                'nom' => 'Méthodes de calcul approché d\'intégrales et de solutions d\'équations différentielles.',
                'rapport' => '',
                'options' => 7,
            ),
            67 => 
            array (
                'id' => 68,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 239,
                'type_id' => 2,
                'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            68 => 
            array (
                'id' => 69,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 240,
                'type_id' => 2,
                'nom' => 'Transformation de Fourier. Applications.',
                'rapport' => '
Cette leçon ne peut se résumer à une collection de relations algébriques (analyse algébrique de la transformée de Fourier). Elle nécessite, pour s’inscrire dans le contexte de l’analyse, une étude minutieuse et une réflexion sur les hypothèses et les définitions des objets manipulés. 
L’extension de la transformée de Fourier aux distributions tempérées trouvera sa place ici.
',
                'options' => 15,
            ),
            69 => 
            array (
                'id' => 70,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 241,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            70 => 
            array (
                'id' => 71,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 242,
                'type_id' => 2,
                'nom' => 'Utilisation en probabilités du produit de convolution et de la transformation de Fourier ou de Laplace.',
                'rapport' => '',
                'options' => 7,
            ),
            71 => 
            array (
                'id' => 72,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 243,
                'type_id' => 2,
                'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                'rapport' => '
Il est dommage de ne parler que de dérivabilité par rapport à une variable réelle quand on énonce (ou utilise) ensuite ces résultats sur les fonctions holomorphes.
',
                'options' => 15,
            ),
            72 => 
            array (
                'id' => 73,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 245,
                'type_id' => 2,
                'nom' => 'Fonctions holomorphes et méromorphes sur un ouvert de $C$. Exemples et applications.',
                'rapport' => '
Les conditions de Cauchy-Riemann doivent être parfaitement connues Z et l’interprétation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z) dz$ a un sens précis, qu’il faut savoir expliquer. Par ailleurs il faut connaître la définition d’une fonction méromorphe (l’ensemble des pôles doit être une partie fermée discrète) !
',
                'options' => 7,
            ),
            73 => 
            array (
                'id' => 74,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:43:37',
                'annee' => 2010,
                'numero' => 246,
                'type_id' => 2,
                'nom' => 'Séries de Fourier. Exemples et applications.',
                'rapport' => '
Les différents modes de convergence ($L^2$ , Fejer, Dirichlet etc...) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). Dans le cas d’une fonction continue et $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet. Cette leçon ne doit pas se réduire à un cours abstrait sur les coefficients de Fourier.
',
                'options' => 15,
            ),
            74 => 
            array (
                'id' => 75,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 247,
                'type_id' => 2,
                'nom' => 'Exemples de problèmes d\'interversion de limites.',
                'rapport' => '',
                'options' => 7,
            ),
            75 => 
            array (
                'id' => 76,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 249,
                'type_id' => 2,
                'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
                'rapport' => '',
                'options' => 7,
            ),
            76 => 
            array (
                'id' => 77,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 250,
                'type_id' => 2,
                'nom' => 'Loi des grands nombres. Théorème central limite. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            77 => 
            array (
                'id' => 78,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 251,
                'type_id' => 2,
                'nom' => 'Indépendance d\'événements et de variables aléatoires. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            78 => 
            array (
                'id' => 79,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 252,
                'type_id' => 2,
                'nom' => 'Loi binomiale. Loi de Poisson. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            79 => 
            array (
                'id' => 80,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 253,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de convexité en analyse.',
                'rapport' => '',
                'options' => 7,
            ),
            80 => 
            array (
                'id' => 81,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 254,
                'type_id' => 2,
            'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées.',
                'rapport' => '',
                'options' => 7,
            ),
            81 => 
            array (
                'id' => 82,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 255,
                'type_id' => 2,
                'nom' => 'Dérivation au sens des distributions. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            82 => 
            array (
                'id' => 83,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 256,
                'type_id' => 2,
            'nom' => 'Transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
                'rapport' => '',
                'options' => 7,
            ),
            83 => 
            array (
                'id' => 84,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 901,
                'type_id' => 3,
                'nom' => 'Structure de données : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            84 => 
            array (
                'id' => 85,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 902,
                'type_id' => 3,
                'nom' => 'Diviser pour régner : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            85 => 
            array (
                'id' => 86,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 903,
                'type_id' => 3,
                'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                'rapport' => '',
                'options' => 8,
            ),
            86 => 
            array (
                'id' => 87,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 904,
                'type_id' => 3,
                'nom' => 'Problèmes NP-complets : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            87 => 
            array (
                'id' => 88,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 906,
                'type_id' => 3,
                'nom' => 'Programmation dynamique : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            88 => 
            array (
                'id' => 89,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 907,
                'type_id' => 3,
                'nom' => 'Algorithmique du texte : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            89 => 
            array (
                'id' => 90,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 908,
                'type_id' => 3,
                'nom' => 'Automates finis. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            90 => 
            array (
                'id' => 91,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 909,
                'type_id' => 3,
                'nom' => 'Langages rationnels. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            91 => 
            array (
                'id' => 92,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 910,
                'type_id' => 3,
                'nom' => 'Langages algébriques. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            92 => 
            array (
                'id' => 93,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 911,
                'type_id' => 3,
                'nom' => 'Automates à pile. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            93 => 
            array (
                'id' => 94,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 912,
                'type_id' => 3,
                'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            94 => 
            array (
                'id' => 95,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 913,
                'type_id' => 3,
                'nom' => 'Machines de Turing. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            95 => 
            array (
                'id' => 96,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 914,
                'type_id' => 3,
                'nom' => 'Décidabilité et indécidabilité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            96 => 
            array (
                'id' => 97,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 915,
                'type_id' => 3,
                'nom' => 'Classes de complexité : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            97 => 
            array (
                'id' => 98,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 916,
                'type_id' => 3,
                'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            98 => 
            array (
                'id' => 99,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 917,
                'type_id' => 3,
                'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                'rapport' => '',
                'options' => 8,
            ),
            99 => 
            array (
                'id' => 100,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 918,
                'type_id' => 3,
                'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            100 => 
            array (
                'id' => 101,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 919,
                'type_id' => 3,
                'nom' => 'Unification : algorithmes et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            101 => 
            array (
                'id' => 102,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 920,
                'type_id' => 3,
                'nom' => 'Réécriture et formes normales. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            102 => 
            array (
                'id' => 103,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 921,
                'type_id' => 3,
                'nom' => 'Algorithmes de recherche et structures de données associées.',
                'rapport' => '',
                'options' => 8,
            ),
            103 => 
            array (
                'id' => 104,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 922,
                'type_id' => 3,
                'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            104 => 
            array (
                'id' => 105,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 923,
                'type_id' => 3,
                'nom' => 'Analyses lexicale et syntaxique : applications.',
                'rapport' => '',
                'options' => 8,
            ),
            105 => 
            array (
                'id' => 106,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 924,
                'type_id' => 3,
                'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            106 => 
            array (
                'id' => 107,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 925,
                'type_id' => 3,
                'nom' => 'Graphes : représentations et algorithmes.',
                'rapport' => '',
                'options' => 8,
            ),
            107 => 
            array (
                'id' => 108,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 926,
                'type_id' => 3,
                'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            108 => 
            array (
                'id' => 109,
                'created_at' => '2016-03-14 12:31:02',
                'updated_at' => '2016-03-14 12:31:02',
                'annee' => 2010,
                'numero' => 927,
                'type_id' => 3,
                'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                'rapport' => '',
                'options' => 8,
            ),
            109 => 
            array (
                'id' => 110,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            110 => 
            array (
                'id' => 111,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples et applications des notions de sous-groupe distingué et de groupe quotient.',
                'rapport' => '',
                'options' => 7,
            ),
            111 => 
            array (
                'id' => 112,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            112 => 
            array (
                'id' => 113,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            113 => 
            array (
                'id' => 114,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            114 => 
            array (
                'id' => 115,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => '',
                'options' => 7,
            ),
            115 => 
            array (
                'id' => 116,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            116 => 
            array (
                'id' => 117,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications',
                'rapport' => '',
                'options' => 15,
            ),
            117 => 
            array (
                'id' => 118,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 110,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            118 => 
            array (
                'id' => 119,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 111,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            119 => 
            array (
                'id' => 120,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 112,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            120 => 
            array (
                'id' => 121,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 113,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            121 => 
            array (
                'id' => 122,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 114,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            122 => 
            array (
                'id' => 123,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 115,
                'type_id' => 1,
                'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            123 => 
            array (
                'id' => 124,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 116,
                'type_id' => 1,
                'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            124 => 
            array (
                'id' => 125,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 117,
                'type_id' => 1,
            'nom' => 'Algèbre des polynômes à $n$ indéterminées ($n \\ge 2$). Polynômes symétriques. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            125 => 
            array (
                'id' => 126,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 118,
                'type_id' => 1,
                'nom' => 'Exemples d\'utilisation de la notion de dimension d\'un espace vectoriel.',
                'rapport' => '',
                'options' => 7,
            ),
            126 => 
            array (
                'id' => 127,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 119,
                'type_id' => 1,
                'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                'rapport' => '',
                'options' => 15,
            ),
            127 => 
            array (
                'id' => 128,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 120,
                'type_id' => 1,
            'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            128 => 
            array (
                'id' => 129,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 121,
                'type_id' => 1,
                'nom' => 'Matrices équivalentes. Matrices semblables. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            129 => 
            array (
                'id' => 130,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Déterminant. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            130 => 
            array (
                'id' => 131,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            131 => 
            array (
                'id' => 132,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Sous-espaces stables par un endomorphisme d\'un espace vectoriel de dimension finie. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            132 => 
            array (
                'id' => 133,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 126,
                'type_id' => 1,
                'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => '',
                'options' => 7,
            ),
            133 => 
            array (
                'id' => 134,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 127,
                'type_id' => 1,
                'nom' => 'Exponentielle de matrices. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            134 => 
            array (
                'id' => 135,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 128,
                'type_id' => 1,
                'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                'rapport' => '',
                'options' => 15,
            ),
            135 => 
            array (
                'id' => 136,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 130,
                'type_id' => 1,
                'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                'rapport' => '',
                'options' => 7,
            ),
            136 => 
            array (
                'id' => 137,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 131,
                'type_id' => 1,
                'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            137 => 
            array (
                'id' => 138,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 132,
                'type_id' => 1,
                'nom' => 'Formes linéaires et hyperplans en dimension finie. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            138 => 
            array (
                'id' => 139,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 133,
                'type_id' => 1,
            'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                'rapport' => '',
                'options' => 15,
            ),
            139 => 
            array (
                'id' => 140,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 135,
                'type_id' => 1,
                'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Forme réduite. Applications en dimensions $2$ et $3$.',
                'rapport' => '',
                'options' => 7,
            ),
            140 => 
            array (
                'id' => 141,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 136,
                'type_id' => 1,
                'nom' => 'Coniques. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            141 => 
            array (
                'id' => 142,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 137,
                'type_id' => 1,
                'nom' => 'Barycentres dans un espace affine réel de dimension finie; convexité. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            142 => 
            array (
                'id' => 143,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 139,
                'type_id' => 1,
                'nom' => 'Applications des nombres complexes à la géométrie.',
                'rapport' => '',
                'options' => 15,
            ),
            143 => 
            array (
                'id' => 144,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 140,
                'type_id' => 1,
                'nom' => 'Systèmes d\'équations linéaires. Systèmes échelonnés. Résolution. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            144 => 
            array (
                'id' => 145,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 141,
                'type_id' => 1,
                'nom' => 'Utilisation des groupes en géométrie.',
                'rapport' => '',
                'options' => 15,
            ),
            145 => 
            array (
                'id' => 146,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 144,
                'type_id' => 1,
                'nom' => 'Problèmes d\'angles et de distances en dimension $2$ ou $3$.',
                'rapport' => '',
                'options' => 7,
            ),
            146 => 
            array (
                'id' => 147,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 145,
                'type_id' => 1,
                'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                'rapport' => '',
                'options' => 15,
            ),
            147 => 
            array (
                'id' => 148,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 146,
                'type_id' => 1,
                'nom' => 'Résultant. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            148 => 
            array (
                'id' => 149,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 148,
                'type_id' => 1,
                'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            149 => 
            array (
                'id' => 150,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 149,
                'type_id' => 1,
                'nom' => 'Représentations de groupes finis de petit cardinal.',
                'rapport' => '',
                'options' => 7,
            ),
            150 => 
            array (
                'id' => 151,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 150,
                'type_id' => 1,
                'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Localisation des racines dans les cas réel et complexe.',
                'rapport' => '',
                'options' => 7,
            ),
            151 => 
            array (
                'id' => 152,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 151,
                'type_id' => 1,
                'nom' => 'Extensions de corps. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            152 => 
            array (
                'id' => 153,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 201,
                'type_id' => 2,
                'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            153 => 
            array (
                'id' => 154,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 202,
                'type_id' => 2,
                'nom' => 'Exemples de parties denses et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            154 => 
            array (
                'id' => 155,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 203,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de compacité.',
                'rapport' => '',
                'options' => 15,
            ),
            155 => 
            array (
                'id' => 156,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 204,
                'type_id' => 2,
                'nom' => 'Connexité. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            156 => 
            array (
                'id' => 157,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 205,
                'type_id' => 2,
                'nom' => 'Espaces complets. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            157 => 
            array (
                'id' => 158,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 206,
                'type_id' => 2,
                'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            158 => 
            array (
                'id' => 159,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 207,
                'type_id' => 2,
                'nom' => 'Prolongement de fonctions. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            159 => 
            array (
                'id' => 160,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 208,
                'type_id' => 2,
                'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            160 => 
            array (
                'id' => 161,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 213,
                'type_id' => 2,
                'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            161 => 
            array (
                'id' => 162,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 214,
                'type_id' => 2,
                'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            162 => 
            array (
                'id' => 163,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 215,
                'type_id' => 2,
                'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            163 => 
            array (
                'id' => 164,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 216,
                'type_id' => 2,
                'nom' => 'Étude métrique des courbes. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            164 => 
            array (
                'id' => 165,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 217,
                'type_id' => 2,
                'nom' => 'Sous-variétés de $R^n$. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            165 => 
            array (
                'id' => 166,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 218,
                'type_id' => 2,
                'nom' => 'Applications des formules de Taylor.',
                'rapport' => '',
                'options' => 15,
            ),
            166 => 
            array (
                'id' => 167,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 219,
                'type_id' => 2,
                'nom' => 'Problèmes d\'extremums.',
                'rapport' => '',
                'options' => 15,
            ),
            167 => 
            array (
                'id' => 168,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 220,
                'type_id' => 2,
            'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'études qualitatives des solutions.',
                'rapport' => '',
                'options' => 15,
            ),
            168 => 
            array (
                'id' => 169,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 221,
                'type_id' => 2,
                'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            169 => 
            array (
                'id' => 170,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 223,
                'type_id' => 2,
                'nom' => 'Convergence des suites numériques. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            170 => 
            array (
                'id' => 171,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 224,
                'type_id' => 2,
                'nom' => 'Comportement asymptotique de suites numériques. Rapidité de convergence. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            171 => 
            array (
                'id' => 172,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 226,
                'type_id' => 2,
            'nom' => 'Comportement d\'une suite réelle ou vectorielle définie par une itération $u_{n+1} = f(u_n)$. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            172 => 
            array (
                'id' => 173,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 228,
                'type_id' => 2,
                'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            173 => 
            array (
                'id' => 174,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 229,
                'type_id' => 2,
                'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            174 => 
            array (
                'id' => 175,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 230,
                'type_id' => 2,
                'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            175 => 
            array (
                'id' => 176,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 232,
                'type_id' => 2,
            'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            176 => 
            array (
                'id' => 177,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 234,
                'type_id' => 2,
                'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                'rapport' => '',
                'options' => 7,
            ),
            177 => 
            array (
                'id' => 178,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 235,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions intégrables. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            178 => 
            array (
                'id' => 179,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 236,
                'type_id' => 2,
                'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                'rapport' => '',
                'options' => 15,
            ),
            179 => 
            array (
                'id' => 180,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 238,
                'type_id' => 2,
                'nom' => 'Méthodes de calcul approché d\'intégrales et de solutions d\'équations différentielles.',
                'rapport' => '',
                'options' => 7,
            ),
            180 => 
            array (
                'id' => 181,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 239,
                'type_id' => 2,
                'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            181 => 
            array (
                'id' => 182,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 240,
                'type_id' => 2,
                'nom' => 'Transformation de Fourier. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            182 => 
            array (
                'id' => 183,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 241,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            183 => 
            array (
                'id' => 184,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 242,
                'type_id' => 2,
                'nom' => 'Utilisation en probabilités du produit de convolution et de la transformation de Fourier ou de Laplace.',
                'rapport' => '',
                'options' => 7,
            ),
            184 => 
            array (
                'id' => 185,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 243,
                'type_id' => 2,
                'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            185 => 
            array (
                'id' => 186,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 245,
                'type_id' => 2,
                'nom' => 'Fonctions holomorphes et méromorphes sur un ouvert de $C$. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            186 => 
            array (
                'id' => 187,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 246,
                'type_id' => 2,
                'nom' => 'Séries de Fourier. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            187 => 
            array (
                'id' => 188,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 247,
                'type_id' => 2,
                'nom' => 'Exemples de problèmes d\'interversion de limites.',
                'rapport' => '',
                'options' => 7,
            ),
            188 => 
            array (
                'id' => 189,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 249,
                'type_id' => 2,
                'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
                'rapport' => '',
                'options' => 7,
            ),
            189 => 
            array (
                'id' => 190,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 250,
                'type_id' => 2,
                'nom' => 'Loi des grands nombres. Théorème central limite. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            190 => 
            array (
                'id' => 191,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 251,
                'type_id' => 2,
                'nom' => 'Indépendance d\'événements et de variables aléatoires. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            191 => 
            array (
                'id' => 192,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 252,
                'type_id' => 2,
                'nom' => 'Loi binomiale. Loi de Poisson. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            192 => 
            array (
                'id' => 193,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 253,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de convexité en analyse.',
                'rapport' => '',
                'options' => 7,
            ),
            193 => 
            array (
                'id' => 194,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 254,
                'type_id' => 2,
            'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées.',
                'rapport' => '',
                'options' => 7,
            ),
            194 => 
            array (
                'id' => 195,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 255,
                'type_id' => 2,
                'nom' => 'Dérivation au sens des distributions. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            195 => 
            array (
                'id' => 196,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 256,
                'type_id' => 2,
            'nom' => 'Transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
                'rapport' => '',
                'options' => 7,
            ),
            196 => 
            array (
                'id' => 197,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 901,
                'type_id' => 3,
                'nom' => 'Structure de données : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            197 => 
            array (
                'id' => 198,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 902,
                'type_id' => 3,
                'nom' => 'Diviser pour régner : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            198 => 
            array (
                'id' => 199,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 903,
                'type_id' => 3,
                'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                'rapport' => '',
                'options' => 8,
            ),
            199 => 
            array (
                'id' => 200,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 904,
                'type_id' => 3,
                'nom' => 'Problèmes NP-complets : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            200 => 
            array (
                'id' => 201,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 906,
                'type_id' => 3,
                'nom' => 'Programmation dynamique : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            201 => 
            array (
                'id' => 202,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 907,
                'type_id' => 3,
                'nom' => 'Algorithmique du texte : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            202 => 
            array (
                'id' => 203,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 908,
                'type_id' => 3,
                'nom' => 'Automates finis. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            203 => 
            array (
                'id' => 204,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 909,
                'type_id' => 3,
                'nom' => 'Langages rationnels. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            204 => 
            array (
                'id' => 205,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 910,
                'type_id' => 3,
                'nom' => 'Langages algébriques. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            205 => 
            array (
                'id' => 206,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 911,
                'type_id' => 3,
                'nom' => 'Automates à pile. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            206 => 
            array (
                'id' => 207,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 912,
                'type_id' => 3,
                'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            207 => 
            array (
                'id' => 208,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 913,
                'type_id' => 3,
                'nom' => 'Machines de Turing. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            208 => 
            array (
                'id' => 209,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 914,
                'type_id' => 3,
                'nom' => 'Décidabilité et indécidabilité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            209 => 
            array (
                'id' => 210,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 915,
                'type_id' => 3,
                'nom' => 'Classes de complexité : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            210 => 
            array (
                'id' => 211,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 916,
                'type_id' => 3,
                'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            211 => 
            array (
                'id' => 212,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 917,
                'type_id' => 3,
                'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                'rapport' => '',
                'options' => 8,
            ),
            212 => 
            array (
                'id' => 213,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 918,
                'type_id' => 3,
                'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            213 => 
            array (
                'id' => 214,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 919,
                'type_id' => 3,
                'nom' => 'Unification : algorithmes et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            214 => 
            array (
                'id' => 215,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 920,
                'type_id' => 3,
                'nom' => 'Réécriture et formes normales. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            215 => 
            array (
                'id' => 216,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 921,
                'type_id' => 3,
                'nom' => 'Algorithmes de recherche et structures de données associées.',
                'rapport' => '',
                'options' => 8,
            ),
            216 => 
            array (
                'id' => 217,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 922,
                'type_id' => 3,
                'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            217 => 
            array (
                'id' => 218,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 923,
                'type_id' => 3,
                'nom' => 'Analyses lexicale et syntaxique : applications.',
                'rapport' => '',
                'options' => 8,
            ),
            218 => 
            array (
                'id' => 219,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 924,
                'type_id' => 3,
                'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            219 => 
            array (
                'id' => 220,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 925,
                'type_id' => 3,
                'nom' => 'Graphes : représentations et algorithmes.',
                'rapport' => '',
                'options' => 8,
            ),
            220 => 
            array (
                'id' => 221,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 926,
                'type_id' => 3,
                'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            221 => 
            array (
                'id' => 222,
                'created_at' => '2016-03-14 12:35:15',
                'updated_at' => '2016-03-14 12:35:15',
                'annee' => 2011,
                'numero' => 927,
                'type_id' => 3,
                'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                'rapport' => '',
                'options' => 8,
            ),
            222 => 
            array (
                'id' => 223,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:45',
                'annee' => 2012,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                'rapport' => '
Il faut bien dominer les deux approches de l\'action de groupe : l\'approche naturelle et l\'approche, plus subtile, via le morphisme qui relie le groupe agissant et le groupe des permutations de l\'ensemble sur lequel il agit. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des polynômes. Les exemples issus de la géométrie ne manquent pas (groupes d\'isométries d\'un solide). Certains candidats décrivent les actions naturelles de $PGL(2, F_q)$ sur la droite projective qui donnent des injections intéressantes pour $q = 2, 3$ et peuvent plus généralement en petit cardinal donner lieu à des isomorphismes de groupes.
',
                'options' => 7,
            ),
            223 => 
            array (
                'id' => 224,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:45',
                'annee' => 2012,
                'numero' => 102,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
                'rapport' => '
Cette leçon est encore abordée de façon élémentaire sans réellement voir où et comment les complexes de modules 1 et les racines de l’unité apparaissent dans divers domaines des mathématiques (polynômes cyclotomiques, théorie des représentations, spectre de certaines matrices remarquables).
',
                'options' => 7,
            ),
            224 => 
            array (
                'id' => 225,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:45',
                'annee' => 2012,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples et applications des notions de sous-groupe distingué et de groupe quotient.',
                'rapport' => '
Les candidats parlent de groupe simple et de sous-groupe dérivé ou de groupe quotient sans savoir utiliser ces notions. Entre autres, il faut savoir pourquoi on s’intéresse particulièrement aux groupes simples. La notion de produit semi-direct n’est plus au programme, mais lorsqu’elle est utilisée il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral D n par exemple).

On pourra noter que les tables de caractères permettent d’illustrer toutes ces notions.
',
                'options' => 7,
            ),
            225 => 
            array (
                'id' => 226,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => '
Les exemples doivent figurer en bonne place dans cette leçon. On peut par exemple étudier les groupes de symétries $A_4$ , $S_4$ , $A_5$ et relier sur ces exemples géométrie et algèbre, les représentations ayant ici toute leur place. Le théorème de structure des groupes abéliens finis doit être connu. 

On attend des candidats de savoir manipuler correctement les éléments de quelques structures usuelles ( $Z /n Z$ , $S_n$ , etc.). Par exemple, proposer un générateur simple de $( Z /n Z , +)$ voire tous les générateurs, calculer aisément un produit de deux permutations, savoir décomposer une permutation en produit de cycles à support disjoint.

Il est important que la notion d’ordre d’un élément soit mentionnée et comprise dans des cas simples.
',
                'options' => 15,
            ),
            226 => 
            array (
                'id' => 227,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                'rapport' => '
Il faut relier rigoureusement les notions d\'orbites et d\'action de groupe et savoir décomposer une permutation en cycles disjoints. Des dessins ou des graphes illustrent de manière commode ce que sont les permutations. Par ailleurs un candidat qui se propose de démontrer que tout groupe simple d\'ordre 60 est isomorphe à $A_5$ devrait aussi savoir montrer que $A_5$ est simple.

L\'existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l\'objet d\'un développement.

Comme pour toute structure algébrique, il est souhaitable de s\'intéresser aux automorphismes du groupe symétrique. Les applications du groupe symétrique ne concernent pas seulement les polyèdres réguliers. Il faut par exemple savoir faire le lien avec les actions de groupe sur un ensemble fini. Il est important de savoir déterminer les classes de conjugaisons du groupe symétrique par la décomposition en cycles.
',
                'options' => 15,
            ),
            227 => 
            array (
                'id' => 228,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
                'rapport' => '
Cette leçon est souvent présentée comme un catalogue de résultats épars et zoologiques sur $GL(E)$. Il faudrait que les candidats sachent faire correspondre sous-groupes et noyaux ou stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). À quoi peuvent servir des générateurs du groupe $GL(E)$ ? Qu\'apporte la topologie dans cette leçon ? Il est préférable de se poser ces questions avant de les découvrir le jour de l\'oral.

Certains candidats affirment que $GL_n(K)$ est dense (respectivement ouvert) dans $M_n(K)$ . Il est judicieux de préciser les hypothèses nécessaires sur le corps K ainsi que la topologie sur $M_n(K)$.

Il faut aussi savoir réaliser $S_n$ dans $GL(n,R)$ et faire le lien entre signature et déterminant.
',
                'options' => 15,
            ),
            228 => 
            array (
                'id' => 229,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => '
Le jury a été généralement bienveillant pour cette leçon, vu que la théorie des représentations était au programme depuis peu. Il s’agit d’une leçon où théorie et exemples doivent apparaître. Le candidat doit d’une part savoir dresser une table de caractères pour des petits groupes. Il doit aussi savoir tirer des informations sur le groupe à partir de sa table de caractères.
',
                'options' => 7,
            ),
            229 => 
            array (
                'id' => 230,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => '
C’est une leçon qui demande un minimum de culture mathématique. Peu de candidats voient l’utilité des parties génératrices dans l’analyse des morphismes de groupes.
',
                'options' => 15,
            ),
            230 => 
            array (
                'id' => 231,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Représentations de groupes finis de petit cardinal.',
                'rapport' => '
Il s’agit d’une leçon où le matériel théorique doit figurer pour ensuite laisser place à des exemples. Les représentations peuvent provenir d’actions de groupes, de groupes d’isométries, d’isomorphismes exceptionnels entre groupes de petit cardinal... Inversement, on peut chercher à interpréter des représentations de façon géométrique, mais il faut avoir conscience qu’une table de caractères provient généralement de représentations complexes et non réelles (a priori). Pour prendre un exemple ambitieux, la construction de l’icosaèdre à partir de la table de caractères de $A_5$ demande des renseignements sur l’indice de Schur (moyenne des caractères sur les carrés des éléments du groupe).
',
                'options' => 7,
            ),
            231 => 
            array (
                'id' => 232,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 120,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications.',
                'rapport' => '
Cette leçon classique demande toutefois une préparation minutieuse. Tout d\'abord $n$ n\'est pas forcément un nombre premier. Il serait bon de connaître les sous-groupes de $Z/nZ$ et les morphismes de groupes de $Z/nZ$ dans $Z/mZ$. 

Bien maîtriser le lemme chinois et sa réciproque. Savoir appliquer le lemme chinois à l\'étude du groupe des inversibles. Distinguer clairement propriétés de groupes additifs et d\'anneaux. Connaître les automorphismes, les nilpotents, les idempotents. Enfin, les candidats sont invités à rendre hommage à Gauss en présentant quelques applications arithmétiques des anneaux $Z/nZ$, telles l\'étude de quelques équations diophantiennes bien choisies.
',
                'options' => 15,
            ),
            232 => 
            array (
                'id' => 233,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 121,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
                'rapport' => '
Il s’agit d’une leçon pouvant être abordée à divers niveaux. Attention au choix des développements, ils doivent être pertinents (l’apparition d’un nombre premier n’est pas suffisant !). La réduction modulo p n’est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important, qu’il faudrait citer. Sa démonstration n’est bien-sûr pas exigible au niveau de l’Agrégation. Quelques résultats sur la géométrie des corps finis sont les bienvenus.
',
                'options' => 15,
            ),
            233 => 
            array (
                'id' => 234,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 122,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Applications.',
                'rapport' => '
Les plans sont trop théoriques. Il est possible de présenter des exemples d’anneaux principaux classiques autres que $Z$ et $K[X ]$ (décimaux, entiers de Gauss ou d’Eisenstein), accompagnés d’une description de leurs irréductibles. Les applications en algèbre linéaire ne manquent pas, il serait bon que les candidats les illustrent. Par exemple, il est étonnant de ne pas voir apparaître la notion de polynôme minimal parmi les applications.

On peut donner des exemples d’anneaux non principaux.
',
                'options' => 7,
            ),
            234 => 
            array (
                'id' => 235,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => '
Un candidat qui étudie les carrés dans un corps fini doit savoir aussi résoudre les équations de degré 2. Les constructions des corps de petit cardinal doivent avoir été pratiquées. Les injections des divers $F_q$ doivent être connues.

Le théorème de Wedderburn ne doit pas constituer le seul développement de cette leçon. En revanche, les applications des corps finis ne doivent pas être négligées. Le théorème de l’élément primitif, s’il est énoncé, doit pouvoir être utilisé.
',
                'options' => 15,
            ),
            235 => 
            array (
                'id' => 236,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => '
C’est une leçon qui doit être illustrée par de nombreux exemples et applications, souvent en lien avec les séries génératrices ; combinatoire, calcul des sommes de Newton, relations de récurrence, nombre de partitions, représentations et séries de Molien, etc.
',
                'options' => 7,
            ),
            236 => 
            array (
                'id' => 237,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Extensions de corps. Exemples et applications.',
                'rapport' => '
Très peu de candidats ont choisi cette leçon d’un niveau difficile. On doit y voir le théorème de la base téléscopique et ses applications à l’irréductibilité de certains polynômes, ainsi que les corps finis. Une version dégradée de la théorie de Galois (qui n’est pas au programme) est très naturelle dans cette leçon.
',
                'options' => 7,
            ),
            237 => 
            array (
                'id' => 238,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 140,
                'type_id' => 1,
                'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
                'rapport' => '
Voici une leçon qui avait disparu et qui revient à l’oral de l’Agrégation. Le bagage théorique est somme toute assez classique, même si parfois le candidat ne voit pas l’unicité de la décomposition en éléments simples en terme d’indépendance en algèbre linéaire. Ce sont surtout les applications qui sont attendues : séries génératrices (avec la question à la clef : à quelle condition une série formelle est-elle le développement d’une fraction rationnelle), automorphismes de $K (X )$, version algébrique du théorème des résidus.

Le théorème de Lüroth n’est pas obligatoire et peut même se révéler un peu dangereux.
',
                'options' => 7,
            ),
            238 => 
            array (
                'id' => 239,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 141,
                'type_id' => 1,
                'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                'rapport' => '
Les applications ne concernent pas que les corps finis. Il existe des corps algébriquement clos de caractéristique nulle autre que C. Un polynôme réductible n’admet pas forcément de racines. Il est instructif de chercher des polynômes irréductibles de degré 2, 3, 4 sur $F_2$ . Il faut connaître le théorème de la base téléscopique ainsi que les utilisations artithmétiques (utilisation de la divisibilité) que l’on peut en faire dans l’étude de l’irréductibilité des polynômes.
',
                'options' => 15,
            ),
            239 => 
            array (
                'id' => 240,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 142,
                'type_id' => 1,
                'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                'rapport' => '
La leçon ne doit pas se concentrer exclusivement sur les aspects formels ni sur les les polynômes symétriques.

Les aspects arithmétiques ne doivent pas être négligés. Il faut savoir montrer l\'irréductibilité d\'un polynôme à plusieurs indéterminées en travaillant sur un anneau de type $A[X]$, où $A$ est factoriel.  

Le théorème fondamental sur la structure de l\'algèbre des polynômes symétriques est vrai sur $Z$. L\'algorithme peut être présenté sur un exemple.

Les applications aux quadriques, aux relations racines coefficients ne doivent pas être négligées. On peut faire agir le groupe $GL(n, R)$ sur les polynômes à $n$ indéterminées de degré inférieur à 2.
',
                'options' => 7,
            ),
            240 => 
            array (
                'id' => 241,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 143,
                'type_id' => 1,
                'nom' => 'Résultant. Applications.',
                'rapport' => '
Le caractère entier du résultant (il se définit sur $Z$) doit être mis en valeur et appliqué.

La partie application doit montrer la diversité du domaine (par exemple en arithmétique, calcul d\'intersection/élimination, calcul différentiel).

Il ne faut pas perdre de vue l\'application linéaire sous-jacente $(U,V) \\rightarrow AU + BV$ qui lie le résultant et le PGCD de $A$ et $B$.
',
                'options' => 7,
            ),
            241 => 
            array (
                'id' => 242,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 144,
                'type_id' => 1,
                'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Localisation des racines dans les cas réel et complexe.',
                'rapport' => '
Il s’agit d’une leçon au spectre assez vaste. On peut y traiter de méthodes de résolutions, de théorie des corps (voire théorie de Galois si affinités), de topologie (continuité des racines) ou même de formes quadratiques.
',
                'options' => 7,
            ),
            242 => 
            array (
                'id' => 243,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:46',
                'annee' => 2012,
                'numero' => 150,
                'type_id' => 1,
                'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                'rapport' => '
Cette leçon n’a pas souvent été prise, elle demande un certain recul. Les actions ne manquent pas et selon l’action on pour dégager d’une part des invariants (rang, matrices échelonnées réduites), d’autre par des algorithmes. On peut aussi, si l’on veut aborder un aspect plus théorique, faire apparaître à travers ces actions quelques décompositions célèbres.
',
                'options' => 15,
            ),
            243 => 
            array (
                'id' => 244,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 151,
                'type_id' => 1,
            'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                'rapport' => '
C’est une leçon qui contrairement aux apparences est devenue difficile pour les candidats. Nombre d’entre eux n’ont pas été capables de donner des réponses satisfaisantes à des questions élémentaires comme : un sous-espace vectoriel d’un espace vectoriel de dimension finie, est-il aussi de dimension finie ? 

Il faut bien connaître les théorèmes fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves.
',
                'options' => 15,
            ),
            244 => 
            array (
                'id' => 245,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 152,
                'type_id' => 1,
                'nom' => 'Déterminant. Exemples et applications.',
                'rapport' => '
Il faut que le plan soit cohérent ; si le déterminant n\'est défini que sur $R$ ou $C$, il est délicat de définir $det(A - XIn)$ avec $A$ une matrice carrée. L\'interprétation du déterminant comme volume est essentielle. Beaucoup de candidats commencent la leçon en disant que le sous-espace des formes $n$-linéaires alternées sur un espace de dimension $n$ est de dimension 1, ce qui est fort à propos. Toutefois, il est essentiel de savoir le montrer.

Le jury ne peut se contenter d\'un Vandermonde ou d\'un déterminant circulant ! Le résultant et les applications simples à l\'intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon. D\'une manière générale on attend pendant le développement l\'illustration d\'un calcul ou la manipulation de déterminants non triviaux.

Il serait bien que la continuité du déterminant trouve une application, ainsi que son caractère polynomial.
',
                'options' => 15,
            ),
            245 => 
            array (
                'id' => 246,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 153,
                'type_id' => 1,
                'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                'rapport' => '
Les polynômes d\'un endomorphisme ne sont pas tous nuls ! Il faut consacrer une courte partie de la leçon à l\'algèbre $K[u]$, connaître sa dimension sans hésiter. Les propriétés globales pourront être étudiées par les meilleurs. Le jury souhaiterait voir certains liens entre réduction et structure de l\'algèbre $K[u]$. Le candidat peut s\'interroger sur les idempotents et le lien avec la décomposition en somme de sous-espaces caractéristiques.

Le jury ne souhaite que le candidat présente un catalogue de résultats autour de la réduction, mais seulement ce qui a trait aux polynômes d\'endomorphismes. Il faut bien préciser que dans la réduction de Dunford, les composantes sont des polynômes en l\'endomorphisme. 

L\'aspect applications est trop souvent négligé. On attend d\'un candidat qu\'il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est souhaitable que les candidats ne fassent pas la confusion entre diverses notions de multiplicité pour une valeur propre $\\lambda$ donnée (algébrique ou géométrique). Enfin rappelons que pour calculer $A^k$ , il n\'est pas nécessaire en général de faire la réduction de $A$ (la donnée d\'un polynôme annulateur de $A$ suffit bien souvent).
',
                'options' => 15,
            ),
            246 => 
            array (
                'id' => 247,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 154,
                'type_id' => 1,
                'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                'rapport' => '
Les candidats doivent s’être interrogés sur les propriétés de l’ensemble des sous-espaces stables par un endomorphisme. Des études détaillées de cas sont les bienvenues. La décomposition de Frobenius trouve tout à fait sa place dans la leçon. Notons qu’il a été ajouté la notion de familles d’endomorphismes. Ceci peut déboucher par exemple sur des endomorphismes commutant entre eux ou sur la théorie des représentations.
',
                'options' => 7,
            ),
            247 => 
            array (
                'id' => 248,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 155,
                'type_id' => 1,
                'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => '
Il faut pouvoir donner des exemples naturels d\'endomorphismes diagonalisables et des critères. Le calcul de l\'exponentielle d\'un endomorphisme diagonalisable est immédiat une fois que l\'on connaît les valeurs propres et ceci sans diagonaliser la matrice, par exemple à l\'aide des projecteurs spectraux. 

On peut sur le corps des réels et des complexes donner des propriétés topologiques, et sur les corps finis, dénombrer les endomorphismes diagonalisables, ou possédant des propriétés données, liées à la diagonalisation.

Mentionnons que l\'affirmation "l\'ensemble des matrices diagonalisables de $M_n(K)$ est dense dans $M_n(K)$" nécessite quelques précisions sur le corps $K$ et la topologie choisie pour $M_n(K)$.
',
                'options' => 7,
            ),
            248 => 
            array (
                'id' => 249,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 156,
                'type_id' => 1,
                'nom' => 'Exponentielle de matrices. Applications.',
                'rapport' => '
C\'est une leçon difficile et ce n\'est pas une leçon d\'analyse. Il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. Les questions de surjectivité ou d\'injectivité doivent être abordées. Par exemple la matrice $A = \\begin{pmatrix} -1 \\esperluette 1 \\\\ 0 \\esperluette -1 \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(2, R))$ ? La matrice blocs $B = \\begin{pmatrix} A \\esperluette 0 \\\\ 0 \\esperluette A \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(4, R))$ ?

La décomposition de Dunford multiplicative (décomposition de Jordan) de $exp(A)$ doit être connue. Les groupes à un paramètre peuvent trouver leur place dans cette leçon. On peut s\'interroger si ces sous-groupes constituent des sous-variétés fermées de $GL(n, R)$. L\'étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités. 

Les applications aux équations différentielles doivent être évoquées sans constituer l\'essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique.

Les notions d\'algèbres de Lie ne sont pas au programme de l\'agrégation, on conseille de n\'aborder ces sujets qu\'à condition d\'avoir une certaine solidité. Sans aller si loin, on pourra donner une application de l\'exponentielle à la décomposition polaire de certains sous-groupes fermés de $GL_n$ (groupes orthogonaux par exemple).
',
                'options' => 7,
            ),
            249 => 
            array (
                'id' => 250,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 157,
                'type_id' => 1,
                'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                'rapport' => '
Il est possible de mener une leçon de bon niveau, même sans la décomposition de Jordan à l’aide des noyaux itérés. On doit savoir déterminer si deux matrices nilpotentes sont semblables grâce aux noyaux itérés (ou grâce à la décomposition de Jordan si celle-ci est maîtrisée). 

Notons que l’étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et l’étude sur un corps fini donne lieu à de jolis problèmes de dénombrement.
',
                'options' => 15,
            ),
            250 => 
            array (
                'id' => 251,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 158,
                'type_id' => 1,
                'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                'rapport' => '
C’est une leçon transversale. La notion de signature doit figurer dans la leçon et on ne doit surtout pas se cantonner au cas des matrices définies positives. Curieusement, il est fréquent que le candidat énonce l’existence de la signature d’une matrice symétrique réelle sans en énoncer l’unicité dans sa classe de congruence. On doit faire le lien avec les formes quadratiques et les formes hermitiennes. La partie réelle et la partie imaginaire d’un produit hermitien définissent des structures sur l’espace vectoriel réel sous-jacent.
',
                'options' => 7,
            ),
            251 => 
            array (
                'id' => 252,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:47',
                'annee' => 2012,
                'numero' => 159,
                'type_id' => 1,
                'nom' => 'Formes linéaires et hyperplans en dimension finie. Exemples et applications.',
                'rapport' => '
Il est important de replacer la thématique de la dualité dans cette leçon. Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d’une intersection d’hyperplans est au cœur de la leçon. L’utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d’obtenir les équations d’un sous-espace vectoriel ou d’exhiber une base d’une intersection d’hyperplans. Cette leçon peut être traitée sous différents aspects : géométrie, algèbre, topologie, analyse etc. Il faut que les développements proposés soient en lien direct, comme toujours, avec la leçon ; proposer la trigonalisation simultanée est un peu osé ! Enfin rappeler que la différentielle d’une fonction réelle est une forme linéaire semble incontournable.
',
                'options' => 15,
            ),
            252 => 
            array (
                'id' => 253,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 160,
                'type_id' => 1,
            'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                'rapport' => '
Les candidats doivent bien prendre conscience que le caractère euclidien de l’espace est essentiel pour que l’endomorphisme soit remarquable. Par exemple, des développements comme le lemme des noyaux ou la décomposition de Dunford n’ont rien à faire ici. En revanche, l’utilisation du fait que l’orthogonal d’un sous-espace stable par un endomorphisme est stable par l’adjoint doit être mis en valeur. La notion de forme normale (ou forme réduite) d’un endomorphisme remarquable est centrale dans la leçon.
',
                'options' => 7,
            ),
            253 => 
            array (
                'id' => 254,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 161,
                'type_id' => 1,
                'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Forme réduite. Applications en dimensions $2$ et $3$.',
                'rapport' => '
La classification des isométries en dimension 2 ou 3 est exigible ainsi que le théorème de décomposition commutative. En dimension 3 : déplacements (translation, rotations, vissage) ; antidéplacements (symétries planes, symétries glissées, et isométrie négative à point fixe unique).
',
                'options' => 7,
            ),
            254 => 
            array (
                'id' => 255,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 162,
                'type_id' => 1,
                'nom' => 'Systèmes d\'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                'rapport' => '
Le jury n\'attend pas une version à l\'ancienne articulée autour du théorème de Rouché-Fontené, qui n\'est pas d\'un grand intérêt dans sa version traditionnellement exposée.

La leçon doit impérativement présenter la notion de système échelonné, avec une définition précise et correcte et situer l\'ensemble dans le contexte de l\'algèbre linéaire (sans oublier la dualité !). Par exemple les relations de dépendances linéaires sur les colonnes d\'une matrice échelonnée sont claires et permettent de décrire simplement les orbites de l\'action à gauche de $GL(n,K)$ sur $M_n(K)$ donnée par $(P,A) \\rightarrow PA$. Le candidat doit pouvoir écrire un système d\'équations de l\'espace vectoriel engendré par les colonnes.

Un point de vue opératoire doit accompagner l\'étude théorique et l\'intérêt pratique (algorithmique) des méthodes présentées doit être expliqué y compris sur des exemples simples où l\'on attend parfois une résolution explicite.
',
                'options' => 15,
            ),
            255 => 
            array (
                'id' => 256,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 170,
                'type_id' => 1,
                'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                'rapport' => '
Le candidat ne doit pas se contenter de travailler sur $R$. Il faut savoir que les formes quadratiques existent sur le corps des complexes et sur les corps finis et savoir les classifier. On ne doit pas négliger l\'interprétation géométrique des notions introduites (lien entre coniques, formes quadratiques, cônes isotropes) ou les aspects élémentaires (par exemple le discriminant de l\'équation $ax^2 + bx + c = 0$ et la signature de la forme quadratique $ax^2 + bxy + cy^2$). On ne peut se limiter à des considérations élémentaires d\'algèbre linéaire. Les formes quadratiques ne sont pas toutes non dégénérées (la notion de quotient est utile pour s\'y ramener).

L\'algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique de $R^3$ . Le lien avec la signature doit être clairement énoncé. Malheureusement la notion d\'isotropie est mal maîtrisée par les candidats, y compris les meilleurs d\'entre eux. Le cône isotrope est un aspect important de cette leçon, qu\'il faut rattacher à la géométrie différentielle. Il est important d\'illustrer cette leçon d\'exemples naturels.
',
                'options' => 15,
            ),
            256 => 
            array (
                'id' => 257,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 171,
                'type_id' => 1,
                'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                'rapport' => '
La preuve de la loi d’inertie de Silvester doit être connue et le candidat doit avoir compris la signification géométrique de ces deux entiers composant la signature d’une forme quadratique réelle ainsi que leur caractère classifiant. La différentielle seconde d’une fonction de plusieurs variables est une forme quadratique
importante.
',
                'options' => 7,
            ),
            257 => 
            array (
                'id' => 258,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 180,
                'type_id' => 1,
                'nom' => 'Coniques. Applications.',
                'rapport' => '
La définition des coniques affines non dégénérées doit être connue. Les propriétés classiques des coniques doivent être présentées. Bien distinguer les notions affines, métriques ou projectives, la classification des coniques étant sensiblement différente selon le cas. 

On peut se situer sur un autre corps que celui des réels. Le lien entre classification des coniques et classifi-
cation des formes quadratiques peut être établi à des fins utiles.
',
                'options' => 7,
            ),
            258 => 
            array (
                'id' => 259,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:48',
                'annee' => 2012,
                'numero' => 181,
                'type_id' => 1,
                'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
                'rapport' => '
On attend des candidats qu’ils parlent de coordonnées barycentriques et les utilisent par exemple dans le
triangle (coordonnées barycentriques de certains points remarquables). Il est judicieux de parler d’enve-
loppe convexe, de points extrémaux, ainsi que des applications qui en résultent.
',
                'options' => 15,
            ),
            259 => 
            array (
                'id' => 260,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 182,
                'type_id' => 1,
                'nom' => 'Applications des nombres complexes à la géométrie.',
                'rapport' => '
Cette leçon ne saurait rester au niveau de la Terminale. Une étude de l’exponentielle complexe et des homographies de la sphère de Riemann est tout à fait appropriée. La réalisation du groupe SU 2 dans le corps des quaternions et ses applications peuvent trouver sa place dans la leçon.
',
                'options' => 15,
            ),
            260 => 
            array (
                'id' => 261,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 183,
                'type_id' => 1,
                'nom' => 'Utilisation des groupes en géométrie.',
                'rapport' => '
C’est une leçon transversale et difficile qui peut aborder des aspects variés selon les structures algébriques présentes. D’une part un groupe de transformations permet de ramener un problème de géométrie à un problème plus simple. D’autre part, les actions de groupes sur la géométrie permettent de dégager des invariants (angle, birapport) essentiels. On retrouvera encore avec bonheur les groupes d’isométries d’un solide.
',
                'options' => 15,
            ),
            261 => 
            array (
                'id' => 262,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 190,
                'type_id' => 1,
                'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                'rapport' => '
Il faut dans un premier temps dégager clairement les méthodes et les illustrer d’exemples significatifs. L’utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. Le jury s’attend à ce que les candidats sachent calculer des cardinaux classiques et certaines probabilités ! L’introduction des corps finis (même en se limitant aux cardinaux premiers) permettent de créer un lien fécond avec l’algèbre linéaire.
',
                'options' => 15,
            ),
            262 => 
            array (
                'id' => 263,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 201,
                'type_id' => 2,
                'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            263 => 
            array (
                'id' => 264,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 202,
                'type_id' => 2,
                'nom' => 'Exemples de parties denses et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            264 => 
            array (
                'id' => 265,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 203,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de compacité.',
                'rapport' => '
Il est important de ne pas concentrer la leçon sur la compacité générale (confusion générale entre utilisation de la notion compacité et notion de compacité), sans proposer des exemples significatifs d’utilisation (Stone-Weierstrass, point fixe, voire étude qualitative d’équations différentielles, etc.). Les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d’utilisation de la compacité. Les applications linéaires compactes sur l’espace de Hilbert ou sur un espace de Banach relèvent également de cette leçon, et on pourra développer par exemple leurs propriétés spectrales. Enfin certains candidats présentent des applications du théorème d’Ascoli (par exemple les opérateurs intégraux à noyau continu, le théorème de Peano, etc).
',
                'options' => 15,
            ),
            265 => 
            array (
                'id' => 266,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 204,
                'type_id' => 2,
                'nom' => 'Connexité. Exemples et applications.',
                'rapport' => '
Il est important de présenter des résultats naturels dont la démonstration utilise la connexité ; par exemple, diverses démonstrations du théorème de d’Alembert-Gauss. On distinguera bien connexité et connexité par arcs, mais il est pertinent de présenter des situations où ces deux notions coïncident.
',
                'options' => 7,
            ),
            266 => 
            array (
                'id' => 267,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 205,
                'type_id' => 2,
                'nom' => 'Espaces complets. Exemples et applications.',
                'rapport' => '
Le théorème de Baire trouve naturellement sa place dans cette leçon, mais il faut l’accompagner d’applications. Rappelons que celles-ci ne se limitent pas aux théorèmes de Banach-Steinhaus et du graphe fermé, mais qu’on peut évoquer au niveau de l’agrégation l’existence de divers objets : fonctions continues nulle part dérivables, points de continuité pour les limites simples de suites de fonctions continues, vecteurs à orbite dense pour certains opérateurs linéaires, etc.
',
                'options' => 7,
            ),
            267 => 
            array (
                'id' => 268,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 206,
                'type_id' => 2,
                'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                'rapport' => '
Les applications aux équations différentielles sont importantes. Il faut préparer des contre-exemples pour illustrer la nécessité des hypothèses. Il est envisageable d’admettre le théorème de point fixe de Brouwer et d’en développer quelques conséquences.
',
                'options' => 15,
            ),
            268 => 
            array (
                'id' => 269,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 207,
                'type_id' => 2,
                'nom' => 'Prolongement de fonctions. Exemples et applications.',
                'rapport' => '
Les candidats exploitent rarement toutes les potentialités de cette leçon très riche. Le prolongement analytique relève bien sûr de cette leçon, ainsi que le théorème de Hahn-Banach, le prolongement de fonctions $C^\\infty$ sur un segment en fonctions de la même classe, le théorème de Tietze sur l’extension des fonctions continues définies sur un sous-ensemble fermé d’un espace métrique, l’intégrale de Riemann, la transformation de Fourier sur $L^2$ et l’extension des fonctions Lipschitziennes définies sur un sous-ensemble (pas nécesairement dense) d’un espace métrique.
',
                'options' => 7,
            ),
            269 => 
            array (
                'id' => 270,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 208,
                'type_id' => 2,
                'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                'rapport' => '
La justification de la compacité de la boule unité en dimension finie doit être donnée. Le théorème d’équivalence des normes en dimension finie, ou le caractère fermé de tout sous-espace de dimension finie d’un espace normé, sont des résultats fondamentaux à propos desquels les candidats doivent se garder des cercles vicieux.
',
                'options' => 15,
            ),
            270 => 
            array (
                'id' => 271,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 213,
                'type_id' => 2,
                'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                'rapport' => '
Il est important de faire la différence entre base algébrique et base hilbertienne. Il faut connaître quelques critères simples pour qu’une famille orthogonale forme une base hilbertienne et illsutrer la leçon par des exemples de bases hilbertiennes (polynômes orthogonaux, séries de Fourier, etc...). Le théorème de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d’un espace de Hilbert H est régulièrement mentionné. En revanche, la possibilité de construire de façon élémentaire le dit-projeté dans le cas particulier d’un sous-espace vectoriel de dimension finie semble Les candidats doivent s’intéresser au sens des formules $x = \\sum_{n \\geq 0} (x|e_n)e_n$ et $||x||^2 = \\sum_{n \\geq 0} (x|e_n)^2$ en précisant les hypothèses sur la famille $(e_n )_{ n \\in N}$ et en justifiant la convergence.
',
                'options' => 7,
            ),
            271 => 
            array (
                'id' => 272,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 214,
                'type_id' => 2,
                'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
                'rapport' => '
On attend des applications en géométrie différentielle (notamment dans la formulation des multiplicateurs de Lagrange). Rappelons que les sous-variétés sont au programme.
',
                'options' => 15,
            ),
            272 => 
            array (
                'id' => 273,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 215,
                'type_id' => 2,
                'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                'rapport' => '
Il faudrait que les candidats à l’agrégation sachent que les différentielles d’ordre supérieur $d^k f (a)$ définissent des applications k-linéaires (sur quel espace ?). Il faut savoir calculer sur des exemples simples, la différentielle d’une fonction, ou effectuer un développement limité à l’ordre 1 d’une fonction.
',
                'options' => 15,
            ),
            273 => 
            array (
                'id' => 274,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:49',
                'annee' => 2012,
                'numero' => 216,
                'type_id' => 2,
                'nom' => 'Étude métrique des courbes. Exemples.',
                'rapport' => '
Cette leçon n’a pas eu beaucoup de succès, c’est bien dommage. Elle ne saurait être réduite à un cours de géométrie différentielle abstraite ; ce serait un contresens. Le jury attend une leçon concrète, montrant une compréhension géométrique locale. Aucune notion globale n’est exigible, ni de notion de variété abstraite. Le candidat doit pouvoir être capable de donner plusieurs représentations locales (paramétriques, équations, etc.) et d’illustrer la notion d’espace tangent sur des exemples classiques. Le jury invite les candidats à réfléchir à la pertinence de l’introduction de la notion de sous-variétés. En ce qui concerne les surfaces de $R^3$ , les candidats sont invités à réfléchir aux notions de formes quadratiques fondamentales et à leurs interprétations géométriques.

Le théorème des extrema liés peut être évoqué dans cette leçon. Les  groupes classiques donnent des exemples utiles de sous-variétés.
',
                'options' => 7,
            ),
            274 => 
            array (
                'id' => 275,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 217,
                'type_id' => 2,
                'nom' => 'Sous-variétés de $R^n$. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            275 => 
            array (
                'id' => 276,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 218,
                'type_id' => 2,
                'nom' => 'Applications des formules de Taylor.',
                'rapport' => '
Il faut connaître les formules de Taylor des polynômes et certains développements très classiques. Il y a de très nombreuses applications en géométrie et probabilités (le théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités $||f^{(k)} || \\leq 2^{\\frac{k(n-k)}{2}} ||f||^{1-k/n} ||f^{(n)}||^{k/n}$ (lorsque f et sa dérivée n-ème sont bornées). On soignera particulièrement le choix des développements. 
',
                'options' => 15,
            ),
            276 => 
            array (
                'id' => 277,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 219,
                'type_id' => 2,
                'nom' => 'Problèmes d\'extremums.',
                'rapport' => '
Il faut bien faire la distinction entre propriétés locales (caractérisation d’un extremum) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local est également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon.
',
                'options' => 15,
            ),
            277 => 
            array (
                'id' => 278,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 220,
                'type_id' => 2,
            'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'études qualitatives des solutions.',
                'rapport' => '
Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est rarement énoncé. L’utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en oeuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées.
',
                'options' => 15,
            ),
            278 => 
            array (
                'id' => 279,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 221,
                'type_id' => 2,
                'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                'rapport' => '
Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. L’utilisation des exponentielles de matrices doit pouvoir s’expliquer. Dans le cas général, certains candidats évoquent les généralisations de l’exponentielle (résolvante) via les intégrales itérées.
',
                'options' => 15,
            ),
            279 => 
            array (
                'id' => 280,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 223,
                'type_id' => 2,
                'nom' => 'Convergence des suites numériques. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            280 => 
            array (
                'id' => 281,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 224,
                'type_id' => 2,
                'nom' => 'Comportement asymptotique de suites numériques. Rapidité de convergence. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            281 => 
            array (
                'id' => 282,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 226,
                'type_id' => 2,
            'nom' => 'Comportement d\'une suite réelle ou vectorielle définie par une itération $u_{n+1} = f(u_n)$. Exemples.',
                'rapport' => '
Le jury attend d’autres exemples que la traditionnelle suite récurrente $u_{n+1} = sin(u_n ). Les suites homographiques réelles ou complexes fournissent des exemples intéressants, rarement évoqués. La méthode du gradient ou les méthodes de Jacobi ou de Gauss-Siedel fournissent des exemples naturels et utiles de suites vectorielles. Le théorème de Sharkovski sur l’itération des fonctions continues sur un intervalle est un résultat récent qui peut se traiter entièrement au niveau de l’agrégation, et il trouve sa place dans cette leçon.
',
                'options' => 15,
            ),
            282 => 
            array (
                'id' => 283,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 228,
                'type_id' => 2,
                'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
                'rapport' => '
Un plan découpé en deux parties (I - Continuité, II - Dérivabilité) n’est pas le mieux adapté. Les candidats doivent disposer d’un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La dérivabilité presque partout des fonctions Lipschitziennes relève de cette leçon.
',
                'options' => 7,
            ),
            283 => 
            array (
                'id' => 284,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 229,
                'type_id' => 2,
                'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                'rapport' => '
Les candidats sont invités à réfléchir à l’incidence de ces notions en théorie des probabilités. La dérivabilité presque partout des fonctions monotones est un résultat important. Il est souhaitable d’illustrer la présentation de la convexité par des dessins clairs, même si ces dessins ne peuvent remplacer un calcul. On notera que la monotonie concerne (à ce niveau) les fonctions réelles d’une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de R , qui fournissent de beaux exemples d’utilisation. L’espace vectoriel engendré par les fonctions monotones (les fonctions à variation bornée) relève de cette leçon. La dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité et les candidats bien préparés peuvent s’aventurer utilement dans cette direction.
',
                'options' => 15,
            ),
            284 => 
            array (
                'id' => 285,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 230,
                'type_id' => 2,
                'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                'rapport' => '
De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être véritablement hors sujet, cette exposition ne doit pas former l’essentiel du plan. Le thème central de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, etc...) et leurs applications diverses, comme par exemple des résultats d’irrationalité, voire de transcendance.
',
                'options' => 15,
            ),
            285 => 
            array (
                'id' => 286,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 232,
                'type_id' => 2,
            'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                'rapport' => '
Le jury attire l’attention sur le fait que X peut désigner un vecteur.
',
                'options' => 15,
            ),
            286 => 
            array (
                'id' => 287,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 234,
                'type_id' => 2,
                'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                'rapport' => '
Le jury a apprécié les candidats sachant montrer qu’avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p \\subset L^q$ si $p \\ge q$. Il est importnat de pouvoir justifier l\'existence de produits de convolution (exemple $L^1 \\star L^1$).
',
                'options' => 7,
            ),
            287 => 
            array (
                'id' => 288,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 235,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions intégrables. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            288 => 
            array (
                'id' => 289,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 236,
                'type_id' => 2,
                'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                'rapport' => '',
                'options' => 15,
            ),
            289 => 
            array (
                'id' => 290,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 239,
                'type_id' => 2,
                'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            290 => 
            array (
                'id' => 291,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 240,
                'type_id' => 2,
                'nom' => 'Produit de convolution, transformation de Fourier. Applications.',
                'rapport' => '
Cette leçon ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C’est bien une leçon d’analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. La transformation de Fourier des distributions tempérées trouve sa place ici. Certains candidats considérent l’extension de la transformée de Fourier à la variable complexe, riche d’applications (dans la direction du théorème de Paley-Wiener, par exemple).
',
                'options' => 15,
            ),
            291 => 
            array (
                'id' => 292,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 241,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            292 => 
            array (
                'id' => 293,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:50',
                'annee' => 2012,
                'numero' => 243,
                'type_id' => 2,
                'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                'rapport' => '
Il faut éviter de ne parler que de dérivabilité par rapport à une variable réelle quand on énonce (ou utilise) ensuite ces résultats sur les fonctions holomorphes. Les séries entières fournissent une méthode puissante d’extension des fonctions au plan complexe, puis au calcul fonctionnel et cela peut être évoqué. Le comportement au bord du disque de convergence (Théorèmes d’Abel, de Tauber et de Hardy-Littlewood) fournit de bons thèmes de développement applicables par exemple à des conditions suffisantes pour que le produit de Cauchy de deux séries semi-convergentes soit convergent.
',
                'options' => 15,
            ),
            293 => 
            array (
                'id' => 294,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:51',
                'annee' => 2012,
                'numero' => 245,
                'type_id' => 2,
                'nom' => 'Fonctions holomorphes et méromorphes sur un ouvert de $C$. Exemples et applications.',
                'rapport' => '
Les conditions de Cauchy-Riemann doivent être parfaitement connues et l’interprétation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z) dz$ a un sens précis, qu’il faut savoir expliquer. Par ailleurs il faut connaître la définition d’une fonction méromorphe (l’ensemble des pôles doit être une partie fermée discrète) !
',
                'options' => 7,
            ),
            294 => 
            array (
                'id' => 295,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:45:51',
                'annee' => 2012,
                'numero' => 246,
                'type_id' => 2,
                'nom' => 'Séries de Fourier. Exemples et applications.',
                'rapport' => '
Les différents modes de convergence ($L^2$ , Fejer, Dirichlet, etc...) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). Dans le cas d’une fonction continue et $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet. Cette leçon ne doit pas se réduire à un cours abstrait sur les coefficients de Fourier.
',
                'options' => 15,
            ),
            295 => 
            array (
                'id' => 296,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 247,
                'type_id' => 2,
                'nom' => 'Exemples de problèmes d\'interversion de limites.',
                'rapport' => '',
                'options' => 7,
            ),
            296 => 
            array (
                'id' => 297,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 249,
                'type_id' => 2,
                'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
                'rapport' => '',
                'options' => 7,
            ),
            297 => 
            array (
                'id' => 298,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 250,
                'type_id' => 2,
                'nom' => 'Loi des grands nombres. Théorème central limite. Applications.',
                'rapport' => '',
                'options' => 7,
            ),
            298 => 
            array (
                'id' => 299,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 251,
                'type_id' => 2,
                'nom' => 'Indépendance d\'événements et de variables aléatoires. Exemples.',
                'rapport' => '',
                'options' => 15,
            ),
            299 => 
            array (
                'id' => 300,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 252,
                'type_id' => 2,
                'nom' => 'Loi binomiale. Loi de Poisson. Applications.',
                'rapport' => '',
                'options' => 15,
            ),
            300 => 
            array (
                'id' => 301,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 253,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de convexité en analyse.',
                'rapport' => '',
                'options' => 7,
            ),
            301 => 
            array (
                'id' => 302,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 254,
                'type_id' => 2,
            'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées. Transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
                'rapport' => '',
                'options' => 7,
            ),
            302 => 
            array (
                'id' => 303,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 255,
                'type_id' => 2,
                'nom' => 'Espaces de Scwartz. Distributions. Dérivation au sens des distributions.',
                'rapport' => '',
                'options' => 7,
            ),
            303 => 
            array (
                'id' => 304,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 901,
                'type_id' => 3,
                'nom' => 'Structure de données : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            304 => 
            array (
                'id' => 305,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 902,
                'type_id' => 3,
                'nom' => 'Diviser pour régner : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            305 => 
            array (
                'id' => 306,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 903,
                'type_id' => 3,
                'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                'rapport' => '',
                'options' => 8,
            ),
            306 => 
            array (
                'id' => 307,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 906,
                'type_id' => 3,
                'nom' => 'Programmation dynamique : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            307 => 
            array (
                'id' => 308,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 907,
                'type_id' => 3,
                'nom' => 'Algorithmique du texte : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            308 => 
            array (
                'id' => 309,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 908,
                'type_id' => 3,
                'nom' => 'Automates finis. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            309 => 
            array (
                'id' => 310,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 909,
                'type_id' => 3,
                'nom' => 'Langages rationnels. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            310 => 
            array (
                'id' => 311,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 910,
                'type_id' => 3,
                'nom' => 'Langages algébriques. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            311 => 
            array (
                'id' => 312,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 911,
                'type_id' => 3,
                'nom' => 'Automates à pile. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            312 => 
            array (
                'id' => 313,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 912,
                'type_id' => 3,
                'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            313 => 
            array (
                'id' => 314,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 913,
                'type_id' => 3,
                'nom' => 'Machines de Turing. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            314 => 
            array (
                'id' => 315,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 914,
                'type_id' => 3,
                'nom' => 'Décidabilité et indécidabilité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            315 => 
            array (
                'id' => 316,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 915,
                'type_id' => 3,
                'nom' => 'Classes de complexité : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            316 => 
            array (
                'id' => 317,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 916,
                'type_id' => 3,
                'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            317 => 
            array (
                'id' => 318,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 917,
                'type_id' => 3,
                'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                'rapport' => '',
                'options' => 8,
            ),
            318 => 
            array (
                'id' => 319,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 918,
                'type_id' => 3,
                'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            319 => 
            array (
                'id' => 320,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 919,
                'type_id' => 3,
                'nom' => 'Unification : algorithmes et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            320 => 
            array (
                'id' => 321,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 920,
                'type_id' => 3,
                'nom' => 'Réécriture et formes normales. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            321 => 
            array (
                'id' => 322,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 921,
                'type_id' => 3,
                'nom' => 'Algorithmes de recherche et structures de données associées.',
                'rapport' => '',
                'options' => 8,
            ),
            322 => 
            array (
                'id' => 323,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 922,
                'type_id' => 3,
                'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            323 => 
            array (
                'id' => 324,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 923,
                'type_id' => 3,
                'nom' => 'Analyses lexicale et syntaxique : applications.',
                'rapport' => '',
                'options' => 8,
            ),
            324 => 
            array (
                'id' => 325,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 924,
                'type_id' => 3,
                'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            325 => 
            array (
                'id' => 326,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 925,
                'type_id' => 3,
                'nom' => 'Graphes : représentations et algorithmes.',
                'rapport' => '',
                'options' => 8,
            ),
            326 => 
            array (
                'id' => 327,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 926,
                'type_id' => 3,
                'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            327 => 
            array (
                'id' => 328,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 927,
                'type_id' => 3,
                'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                'rapport' => '',
                'options' => 8,
            ),
            328 => 
            array (
                'id' => 329,
                'created_at' => '2016-03-14 12:36:00',
                'updated_at' => '2016-03-14 12:36:00',
                'annee' => 2012,
                'numero' => 928,
                'type_id' => 3,
                'nom' => 'Problème NP-complets : exemples de réductions.',
                'rapport' => '',
                'options' => 8,
            ),
            329 => 
            array (
                'id' => 330,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                'rapport' => '
Il faut bien dominer les deux approches de l\'action de groupe : l\'approche naturelle et l\'approche, plus subtile, via le morphisme qui relie le groupe agissant et le groupe des permutations de l\'ensemble sur lequel il agit. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des polynômes. Les exemples issus de la géométrie ne manquent pas (groupes d\'isométries d\'un solide). Certains candidats décrivent les actions naturelles de $PGL(2, F_q)$ sur la droite projective qui donnent des injections intéressantes pour $q = 2, 3$ et peuvent plus généralement en petit cardinal donner lieu à des isomorphismes de groupes.
',
                'options' => 7,
            ),
            330 => 
            array (
                'id' => 331,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 102,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
                'rapport' => '
Cette leçon est encore abordée de façon élémentaire sans réellement expliquer où et comment les nombres complexes de modules 1 et les racines de l\'unité apparaissent dans divers domaines des mathématiques (polynômes cyclotomiques, théorie des représentations, spectre de certaines matrices remarquables).
',
                'options' => 7,
            ),
            331 => 
            array (
                'id' => 332,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples et applications des notions de sous-groupe distingué et de groupe quotient.',
                'rapport' => '
Les candidats parlent de groupe simple et de sous-groupe dérivé ou de groupe quotient sans savoir utiliser ces notions. Entre autres, il faut savoir pourquoi on s\'intéresse particulièrement aux groupes simples. La notion de produit semi-direct n\'est plus au programme, mais lorsqu\'elle est utilisée, il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral $D_n$ par exemple).

On pourra noter que les tables de caractères permettent d\'illustrer toutes ces notions.
',
                'options' => 7,
            ),
            332 => 
            array (
                'id' => 333,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => '
Les exemples doivent figurer en bonne place dans cette leçon. On peut par exemple étudier les groupes de symétries $A_4$ , $S_4$ , $A_5$ et relier sur ces exemples géométrie et algèbre, les représentations ayant ici toute leur place. Le théorème de structure des groupes abéliens finis doit être connu.

On attend des candidats de savoir manipuler correctement les éléments de quelques structures usuelles ($Z/nZ$, $ S_n$, etc.). Par exemple, proposer un générateur simple de $(Z/nZ, +)$ voire tous les générateurs, calculer aisément un produit de deux permutations, savoir décomposer une permutation en produit de cycles à support disjoint.
Il est important que la notion d\'ordre d\'un élément soit mentionnée et comprise dans des cas simples.
',
                'options' => 15,
            ),
            333 => 
            array (
                'id' => 334,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                'rapport' => '
Il faut relier rigoureusement les notions d\'orbites et d\'action de groupe et savoir décomposer une permutation en cycles disjoints. Des dessins ou des graphes illustrent de manière commode ce que sont les permutations. Par ailleurs un candidat qui se propose de démontrer que tout groupe simple d\'ordre 60 est isomorphe à $A_5$ devrait aussi savoir montrer que $A_5$ est simple.

L\'existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l\'objet d\'un développement.

Comme pour toute structure algébrique, il est souhaitable de s\'intéresser aux automorphismes du groupe symétrique. Les applications du groupe symétrique ne concernent pas seulement les polyèdres réguliers. Il faut par exemple savoir faire le lien avec les actions de groupe sur un ensemble fini. Il est important de savoir déterminer les classes de conjugaisons du groupe symétrique par la décomposition en cycles.
',
                'options' => 15,
            ),
            334 => 
            array (
                'id' => 335,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:58',
                'annee' => 2013,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
                'rapport' => '
Cette leçon est souvent présentée comme un catalogue de résultats épars et zoologiques sur $GL(E)$. Il faudrait que les candidats sachent faire correspondre sous-groupes et noyaux ou stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). À quoi peuvent servir des générateurs du groupe $GL(E)$ ? Qu\'apporte la topologie dans cette leçon ? Il est préférable de se poser ces questions avant de les découvrir le jour de l\'oral.

Certains candidats affirment que $GL_n(K)$ est dense (respectivement ouvert) dans $M_n(K)$ . Il est judicieux de préciser les hypothèses nécessaires sur le corps K ainsi que la topologie sur $M_n(K)$.

Il faut aussi savoir réaliser $S_n$ dans $GL(n,R)$ et faire le lien entre signature et déterminant.
',
                'options' => 15,
            ),
            335 => 
            array (
                'id' => 336,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => '
Le jury a été généralement bienveillant pour cette leçon, vu que la théorie des représentations était au programme depuis peu. Il s\'agit d\'une leçon où théorie et exemples doivent apparaître. Le candidat doit d\'une part savoir dresser une table de caractères pour des petits groupes. Il doit aussi savoir tirer des informations sur le groupe à partir de sa table de caractères.
',
                'options' => 7,
            ),
            336 => 
            array (
                'id' => 337,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => '
C\'est une leçon qui demande un minimum de culture mathématique. Peu de candidats voient l\'utilité des parties génératrices dans l\'analyse des morphismes de groupes.
',
                'options' => 15,
            ),
            337 => 
            array (
                'id' => 338,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Représentations de groupes finis de petit cardinal.',
                'rapport' => '
Il s\'agit d\'une leçon où le matériel théorique doit figurer pour ensuite laisser place à des exemples. Les représentations peuvent provenir d\'actions de groupes sur des ensembles finis, de groupes d\'isométries, d\'isomorphismes exceptionnels entre groupes de petit cardinal... Inversement, on peut chercher à interpréter des représentations de façon géométrique, mais il faut avoir conscience qu\'une table de caractères provient généralement de représentations complexes et non réelles (a priori). Pour prendre un exemple ambitieux, la construction de l\'icosaèdre à partir de la table de caractères de $A_5$ demande des renseignements sur l\'indice de Schur (moyenne des caractères sur les carrés des éléments du groupe).
',
                'options' => 7,
            ),
            338 => 
            array (
                'id' => 339,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 120,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications.',
                'rapport' => '
Cette leçon classique demande toutefois une préparation minutieuse. Tout d\'abord $n$ n\'est pas forcément un nombre premier. Il serait bon de connaître les sous-groupes de $Z/nZ$ et les morphismes de groupes de $Z/nZ$ dans $Z/mZ$. 

Bien maîtriser le lemme chinois et sa réciproque. Savoir appliquer le lemme chinois à l\'étude du groupe des inversibles. Distinguer clairement propriétés de groupes additifs et d\'anneaux. Connaître les automorphismes, les nilpotents, les idempotents. Enfin, les candidats sont invités à rendre hommage à Gauss en présentant quelques applications arithmétiques des anneaux $Z/nZ$, telles l\'étude de quelques équations diophantiennes bien choisies.
',
                'options' => 15,
            ),
            339 => 
            array (
                'id' => 340,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 121,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
                'rapport' => '
Il s\'agit d\'une leçon pouvant être abordée à divers niveaux. Attention au choix des développements, ils doivent être pertinents (l\'apparition d\'un nombre premier n\'est pas suffisant !). La réduction modulo $p$ n\'est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important, qu\'il faudrait citer. Sa démonstration n\'est bien-sûr pas exigible au niveau de l\'Agrégation. Quelques résultats sur la géométrie des corps finis sont les bienvenus.
',
                'options' => 15,
            ),
            340 => 
            array (
                'id' => 341,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 122,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Exemples et applications.',
                'rapport' => '
Les plans sont trop théoriques. Il est possible de présenter des exemples d\'anneaux principaux classiques autres que $Z$ et $K[X]$ (décimaux, entiers de Gauss ou d\'Eisenstein), accompagnés d\'une description de leurs irréductibles. Les applications en algèbre linéaire ne manquent pas, il serait bon que les candidats les illustrent. Par exemple, il est étonnant de ne pas voir apparaître la notion de polynôme minimal parmi les applications.

On peut donner des exemples d\'anneaux non principaux.
',
                'options' => 7,
            ),
            341 => 
            array (
                'id' => 342,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => '
Un candidat qui étudie les carrés dans un corps fini doit savoir aussi résoudre les équations de degré 2. Les constructions des corps de petit cardinal doivent avoir été pratiquées. Les injections des divers $F_q$ doivent être connues. 

Le théorème de Wedderburn ne doit pas constituer le seul développement de cette leçon. En revanche, les applications des corps finis (y compris pour $F_q$ avec $q$ non premier !) ne doivent pas être négligées. Le théorème de l\'élément primitif, s\'il est énoncé, doit pouvoir être utilisé.
',
                'options' => 15,
            ),
            342 => 
            array (
                'id' => 343,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => '
C\'est une leçon qui doit être illustrée par de nombreux exemples et applications, souvent en lien avec les séries génératrices ; combinatoire, calcul des sommes de Newton, relations de récurrence, nombre de partitions, représentations et séries de Molien, etc. 
',
                'options' => 7,
            ),
            343 => 
            array (
                'id' => 344,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Extensions de corps. Exemples et applications.',
                'rapport' => '
Très peu de candidats ont choisi cette leçon d\'un niveau difficile. On doit y voir le théorème de la base téléscopique et ses applications à l\'irréductibilité de certains polynômes, ainsi que les corps finis. Une version dégradée de la théorie de Galois (qui n\'est pas au programme) est très naturelle dans cette leçon.
',
                'options' => 7,
            ),
            344 => 
            array (
                'id' => 345,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 126,
                'type_id' => 1,
                'nom' => 'Exemples d\'équations diophantiennes.',
                'rapport' => '',
                'options' => 7,
            ),
            345 => 
            array (
                'id' => 346,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 140,
                'type_id' => 1,
                'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
                'rapport' => '
Voici une leon qui avait disparu et qui revient à l\'oral de l\'Agrégation. Le bagage théorique est somme toute assez classique, même si parfois le candidat ne voit pas l\'unicité de la décomposition en éléments simples en terme d\'indépendance en algèbre linéaire. Ce sont surtout les applications qui sont attendues : séries génératrices (avec la question à la clef : à quelle condition une série formelle est-elle le développement d\'une fraction rationnelle), automorphismes de $K(X)$, version algébrique du théorème des résidus.

Le théorème de Luroth n\'est pas obligatoire et peut même se révéler un peu dangereux.
',
                'options' => 7,
            ),
            346 => 
            array (
                'id' => 347,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 141,
                'type_id' => 1,
                'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                'rapport' => '
Les applications ne concernent pas que les corps finis. Il existe des corps algébriquement clos de caractéristique nulle autre que $C$. Un polynôme réductible n\'admet pas forcément de racines. Il est instructif de chercher des polynômes irréductibles de degré 2, 3, 4 sur $F_2$ . Il faut connaître le théorème de la base téléscopique ainsi que les utilisations artithmétiques (utilisation de la divisibilité) que l\'on peut en faire dans l\'étude de l\'irréductibilité des polynômes.
',
                'options' => 15,
            ),
            347 => 
            array (
                'id' => 348,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 142,
                'type_id' => 1,
                'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                'rapport' => '
La leçon ne doit pas se concentrer exclusivement sur les aspects formels ni sur les les polynômes symétriques.

Les aspects arithmétiques ne doivent pas être négligés. Il faut savoir montrer l\'irréductibilité d\'un polynôme à plusieurs indéterminées en travaillant sur un anneau de type $A[X]$, où $A$ est factoriel.  

Le théorème fondamental sur la structure de l\'algèbre des polynômes symétriques est vrai sur $Z$. L\'algorithme peut être présenté sur un exemple.

Les applications aux quadriques, aux relations racines coefficients ne doivent pas être négligées. On peut faire agir le groupe $GL(n, R)$ sur les polynômes à $n$ indéterminées de degré inférieur à 2.
',
                'options' => 7,
            ),
            348 => 
            array (
                'id' => 349,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 143,
                'type_id' => 1,
                'nom' => 'Résultant. Applications.',
                'rapport' => '
Le caractère entier du résultant (il se définit sur $Z$) doit être mis en valeur et appliqué.

La partie application doit montrer la diversité du domaine (par exemple en arithmétique, calcul d\'intersection/élimination, calcul différentiel).

Il ne faut pas perdre de vue l\'application linéaire sous-jacente $(U,V) \\rightarrow AU + BV$ qui lie le résultant et le PGCD de $A$ et $B$.
',
                'options' => 7,
            ),
            349 => 
            array (
                'id' => 350,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 144,
                'type_id' => 1,
                'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Exemples et applications.',
                'rapport' => '
Il s\'agit d\'une leçon au spectre assez vaste. On peut y traiter de méthodes de résolutions, de théorie des corps (voire théorie de Galois si affinités), de topologie (continuité des racines) ou même de formes quadratiques. 

Il paraît difficile de ne pas introduire la notion de polynôme scindé, de citer le théorème de d\'Alembert-Gauss et des applications des racines (valeurs propres, etc.).
',
                'options' => 7,
            ),
            350 => 
            array (
                'id' => 351,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 150,
                'type_id' => 1,
                'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                'rapport' => '
Cette leçon n\'a pas souvent été prise, elle demande un certain recul. Les actions ne manquent pas et selon l\'action, on pourra dégager d\'une part des invariants (rang, matrices échelonnées réduites), d\'autre part des algorithmes. On peut aussi, si l\'on veut aborder un aspect plus théorique, faire apparaître à travers ces actions quelques décompositions célèbres.
',
                'options' => 15,
            ),
            351 => 
            array (
                'id' => 352,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 151,
                'type_id' => 1,
            'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                'rapport' => '
C\'est une leçon qui, contrairement aux apparences, est devenue difficile pour les candidats. Nombre d\'entre eux n\'ont pas été capables de donner des réponses satisfaisantes à des questions élémentaires comme : un sous-espace vectoriel d\'un espace vectoriel de dimension finie, est-il aussi de dimension finie ?

Il faut bien connaître les théorèmes fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. 
',
                'options' => 15,
            ),
            352 => 
            array (
                'id' => 353,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 152,
                'type_id' => 1,
                'nom' => 'Déterminant. Exemples et applications.',
                'rapport' => '
Il faut que le plan soit cohérent ; si le déterminant n\'est défini que sur $R$ ou $C$, il est délicat de définir $det(A - XIn)$ avec $A$ une matrice carrée. L\'interprétation du déterminant comme volume est essentielle. Beaucoup de candidats commencent la leçon en disant que le sous-espace des formes $n$-linéaires alternées sur un espace de dimension $n$ est de dimension 1, ce qui est fort à propos. Toutefois, il est essentiel de savoir le montrer.

Le jury ne peut se contenter d\'un Vandermonde ou d\'un déterminant circulant ! Le résultant et les applications simples à l\'intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon. D\'une manière générale on attend pendant le développement l\'illustration d\'un calcul ou la manipulation de déterminants non triviaux.

Il serait bien que la continuité du déterminant trouve une application, ainsi que son caractère polynomial.
',
                'options' => 15,
            ),
            353 => 
            array (
                'id' => 354,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 153,
                'type_id' => 1,
                'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                'rapport' => '
Les polynômes d\'un endomorphisme ne sont pas tous nuls ! Il faut consacrer une courte partie de la leçon à l\'algèbre $K[u]$, connaître sa dimension sans hésiter. Les propriétés globales pourront être étudiées par les meilleurs. Le jury souhaiterait voir certains liens entre réduction et structure de l\'algèbre $K[u]$. Le candidat peut s\'interroger sur les idempotents et le lien avec la décomposition en somme de sous-espaces caractéristiques.

Le jury ne souhaite que le candidat présente un catalogue de résultats autour de la réduction, mais seulement ce qui a trait aux polynômes d\'endomorphismes. Il faut bien préciser que dans la réduction de Dunford, les composantes sont des polynômes en l\'endomorphisme. 

L\'aspect applications est trop souvent négligé. On attend d\'un candidat qu\'il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est souhaitable que les candidats ne fassent pas la confusion entre diverses notions de multiplicité pour une valeur propre $\\lambda$ donnée (algébrique ou géométrique). Enfin rappelons que pour calculer $A^k$ , il n\'est pas nécessaire en général de faire la réduction de $A$ (la donnée d\'un polynôme annulateur de $A$ suffit bien souvent).
',
                'options' => 15,
            ),
            354 => 
            array (
                'id' => 355,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 154,
                'type_id' => 1,
                'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                'rapport' => '
Les candidats doivent s\'être interrogés sur les propriétés de l\'ensemble des sous-espaces stables par un endomorphisme. Des études détaillées de cas sont les bienvenues. La décomposition de Frobenius trouve tout à fait sa place dans la leçon. Notons qu\'il a été ajouté la notion de familles d\'endomorphismes. Ceci peut déboucher par exemple sur des endomorphismes commutant entre eux ou sur la théorie des représentations.
',
                'options' => 7,
            ),
            355 => 
            array (
                'id' => 356,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 155,
                'type_id' => 1,
                'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => '
Il faut pouvoir donner des exemples naturels d\'endomorphismes diagonalisables et des critères. Le calcul de l\'exponentielle d\'un endomorphisme diagonalisable est immédiat une fois que l\'on connaît les valeurs propres et ceci sans diagonaliser la matrice, par exemple à l\'aide des projecteurs spectraux. 

On peut sur le corps des réels et des complexes donner des propriétés topologiques, et sur les corps finis, dénombrer les endomorphismes diagonalisables, ou possédant des propriétés données, liées à la diagonalisation.

Mentionnons que l\'affirmation "l\'ensemble des matrices diagonalisables de $M_n(K)$ est dense dans $M_n(K)$" nécessite quelques précisions sur le corps $K$ et la topologie choisie pour $M_n(K)$.
',
                'options' => 7,
            ),
            356 => 
            array (
                'id' => 357,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 156,
                'type_id' => 1,
                'nom' => 'Exponentielle de matrices. Applications.',
                'rapport' => '
C\'est une leçon difficile et ce n\'est pas une leçon d\'analyse. Il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. Les questions de surjectivité ou d\'injectivité doivent être abordées. Par exemple la matrice $A = \\begin{pmatrix} -1 \\esperluette 1 \\\\ 0 \\esperluette -1 \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(2, R))$ ? La matrice blocs $B = \\begin{pmatrix} A \\esperluette 0 \\\\ 0 \\esperluette A \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(4, R))$ ?

La décomposition de Dunford multiplicative (décomposition de Jordan) de $exp(A)$ doit être connue. Les groupes à un paramètre peuvent trouver leur place dans cette leçon. On peut s\'interroger si ces sous-groupes constituent des sous-variétés fermées de $GL(n, R)$. L\'étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités. 

Les applications aux équations différentielles doivent être évoquées sans constituer l\'essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique, mais le jury déconseille aux candidats de proposer ce thème dans un développement. 

Les notions d\'algèbres de Lie ne sont pas au programme de l\'agrégation, on conseille de n\'aborder ces sujets qu\'à condition d\'avoir une certaine solidité. Sans aller si loin, on pourra donner une application de l\'exponentielle à la décomposition polaire de certains sous-groupes fermés de $GL_n$ (groupes orthogonaux par exemple).    ',
                'options' => 7,
            ),
            357 => 
            array (
                'id' => 358,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 157,
                'type_id' => 1,
                'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                'rapport' => '
Il est possible de mener une leçon de bon niveau, même sans la décomposition de Jordan à l\'aide des noyaux itérés. On doit savoir déterminer si deux matrices nilpotentes sont semblables grâce aux noyaux itérés (ou grâce à la décomposition de Jordan si celle-ci est maîtrisée). 

Notons que l\'étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et l\'étude sur un corps fini donne lieu à de jolis problèmes de dénombrement.
',
                'options' => 15,
            ),
            358 => 
            array (
                'id' => 359,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 158,
                'type_id' => 1,
                'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                'rapport' => '
C\'est une leçon transversale. La notion de signature doit figurer dans la leçon et on ne doit surtout pas se cantonner au cas des matrices définies positives. Curieusement, il est fréquent que le candidat énonce l\'existence de la signature d\'une matrice symétrique réelle sans en énoncer l\'unicité dans sa classe de congruence. On doit faire le lien avec les formes quadratiques et les formes hermitiennes. La partie réelle et la partie imaginaire d\'un produit hermitien définissent des structures sur l\'espace vectoriel réel sous-jacent.
',
                'options' => 7,
            ),
            359 => 
            array (
                'id' => 360,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 159,
                'type_id' => 1,
                'nom' => 'Formes linéaires et dualité en dimension finie. Exemples et applications.',
                'rapport' => '
Il est important de replacer la thématique de la dualité dans cette leçon. Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d’une intersection d’hyperplans est au cœur de la leçon. L’utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d’obtenir les équations d’un sous-espace vectoriel ou d’exhiber une base d’une intersection d’hyperplans. Cette leçon peut être traitée sous différents aspects : géométrique, algèbrique, topologique, analytique, etc. Il faut que les développements proposés soient en lien direct, comme toujours, avec la leçon ; proposer la trigonalisation simultanée est un peu osé ! Enfin rappeler que la différentielle d’une fonction réelle est une forme linéaire semble incontournable.
',
                'options' => 15,
            ),
            360 => 
            array (
                'id' => 361,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 160,
                'type_id' => 1,
            'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                'rapport' => '
Les candidats doivent bien prendre conscience que le caractère euclidien de l\'espace est essentiel pour que l\'endomorphisme soit remarquable. Par exemple, des développements comme le lemme des noyaux ou la décomposition de Dunford n\'ont rien à faire ici. En revanche, l\'utilisation du fait que l\'orthogonal d\'un sous-espace stable par un endomorphisme est stable par l\'adjoint doit être mis en valeur.
',
                'options' => 7,
            ),
            361 => 
            array (
                'id' => 362,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 161,
                'type_id' => 1,
                'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Applications en dimensions $2$ et $3$.',
                'rapport' => '
La classification des isométries en dimension 2 est exigible. En dimension 3, les rotations et les liens avec la réduction. On peut penser aux applications aux isométries laissant stables certaines figures en dimension 2 et 3.
',
                'options' => 7,
            ),
            362 => 
            array (
                'id' => 363,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 162,
                'type_id' => 1,
                'nom' => 'Systèmes d\'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                'rapport' => '
Le jury n\'attend pas une version à l\'ancienne articulée autour du théorème de Rouché-Fontené, qui n\'est pas d\'un grand intérêt dans sa version traditionnellement exposée.

La leçon doit impérativement présenter la notion de système échelonné, avec une définition précise et correcte et situer l\'ensemble dans le contexte de l\'algèbre linéaire (sans oublier la dualité !). Par exemple les relations de dépendances linéaires sur les colonnes d\'une matrice échelonnée sont claires et permettent de décrire simplement les orbites de l\'action à gauche de $GL(n,K)$ sur $M_n(K)$ donnée par $(P,A) \\rightarrow PA$. 
Le candidat doit pouvoir écrire un système d\'équations de l\'espace vectoriel engendré par les colonnes.

Un point de vue opératoire doit accompagner l\'étude théorique et l\'intérêt pratique (algorithmique) des méthodes présentées doit être expliqué y compris sur des exemples simples où l\'on attend parfois une résolution explicite.
',
                'options' => 15,
            ),
            363 => 
            array (
                'id' => 364,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 170,
                'type_id' => 1,
                'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                'rapport' => '
Le candidat ne doit pas se contenter de travailler sur $R$. Il faut savoir que les formes quadratiques existent sur le corps des complexes et sur les corps finis et savoir les classifier. On ne doit pas négliger l\'interprétation géométrique des notions introduites (lien entre coniques, formes quadratiques, cônes isotropes) ou les aspects élémentaires (par exemple le discriminant de l\'équation $ax^2 + bx + c = 0$ et la signature de la forme quadratique $ax^2 + bxy + cy^2$). On ne peut se limiter à des considérations élémentaires d\'algèbre linéaire. Les formes quadratiques ne sont pas toutes non dégénérées (la notion de quotient est utile pour s\'y ramener).

L\'algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique de $R^3$ . Le lien avec la signature doit être clairement énoncé. Malheureusement la notion d\'isotropie est mal maîtrisée par les candidats, y compris les meilleurs d\'entre eux. Le cône isotrope est un aspect important de cette leçon, qu\'il faut rattacher à la géométrie différentielle. Il est important d\'illustrer cette leçon d\'exemples naturels.
',
                'options' => 15,
            ),
            364 => 
            array (
                'id' => 365,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 171,
                'type_id' => 1,
                'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                'rapport' => '
La preuve de la loi d\'inertie de Silvester doit être connue ainsi que l\'orthogonalisation simultanée. Le candidat doit avoir compris la signification géométrique de ces deux entiers composant la signature d\'une forme quadratique réelle ainsi que leur caractère classifiant. La différentielle seconde d\'une fonction de plusieurs variables est une forme quadratique importante. 
',
                'options' => 7,
            ),
            365 => 
            array (
                'id' => 366,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:45:59',
                'annee' => 2013,
                'numero' => 180,
                'type_id' => 1,
                'nom' => 'Coniques. Applications.',
                'rapport' => '
La définition des coniques affines non dégénérées doit être connue. Les propriétés classiques des coniques doivent être présentées. Bien distinguer les notions affines, métriques ou projectives, la classification des coniques étant sensiblement différente selon le cas.

On peut se situer sur un autre corps que celui des réels. Le lien entre classification des coniques et classification des formes quadratiques peut être établi à des fins utiles.
',
                'options' => 7,
            ),
            366 => 
            array (
                'id' => 367,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 181,
                'type_id' => 1,
                'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
                'rapport' => '
On attend des candidats qu\'ils parlent de coordonnées barycentriques et les utilisent par exemple dans le triangle (coordonnées barycentriques de certains points remarquables). Il est judicieux de parler d\'enveloppe convexe, de points extrémaux, ainsi que des applications qui en résultent.
',
                'options' => 15,
            ),
            367 => 
            array (
                'id' => 368,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 182,
                'type_id' => 1,
                'nom' => 'Applications des nombres complexes à la géométrie.',
                'rapport' => '
Cette leçon ne saurait rester au niveau de la Terminale. Une étude de l\'exponentielle complexe et des homographies de la sphère de Riemann est tout à fait appropriée. La réalisation du groupe $SU_2$ dans le corps des quaternions et ses applications peuvent trouver sa place dans la leçon.
',
                'options' => 15,
            ),
            368 => 
            array (
                'id' => 369,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 183,
                'type_id' => 1,
                'nom' => 'Utilisation des groupes en géométrie.',
                'rapport' => '
C\'est une leçon transversale et difficile, qui peut aborder des aspects variés selon les structures algébriques présentes. D\'une part un groupe de transformations permet de ramener un problème de géométrie à un problème plus simple. D\'autre part, les actions de groupes sur la géométrie permettent de dégager des invariants (angle, birapport) essentiels. On retrouvera encore avec bonheur les groupes d\'isométries d\'un solide.
',
                'options' => 15,
            ),
            369 => 
            array (
                'id' => 370,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 190,
                'type_id' => 1,
                'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                'rapport' => '
Il faut dans un premier temps dégager clairement les méthodes et les illustrer d\'exemples significatifs. L\'utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. Le jury s\'attend à ce que les candidats sachent calculer des cardinaux classiques et certaines probabilités ! L\'introduction des corps finis (même en se limitant aux cardinaux premiers) permet de créer un lien fécond avec l\'algèbre linéaire.
',
                'options' => 15,
            ),
            370 => 
            array (
                'id' => 371,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 201,
                'type_id' => 2,
                'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => '
C\'est une leçon riche où le candidat devra choisir soigneusement le niveau auquel il souhaite se placer. Les espaces de fonctions continues sur un compact et les espaces de fonctions holomorphes offrent des possibilités de leçons de qualité avec des résultats intéressants. Il est regrettable de voir des candidats, qui auraient eu intérêt à se concentrer sur les bases de la convergence uniforme, proposer en développement le théorème de Riesz-Fisher dont il ne maîtrise visiblement pas la démonstration. Pour les candidats solides, les espaces Lp offrent de belles possibilités. 

Signalons que des candidats proposent assez régulièrement une version incorrecte du théorème de Müntz pour les fonctions continues. La version correcte dans ce cadre est  $$\\overline{Vect\\{1, x^{\\lambda_n} \\}} = \\mathcal{C}([0;1]; R) \\Leftrightarrow \\sum_{n \\geq 1} \\frac{1}{\\lambda_n} = + \\infty$$
Des candidats aguerris peuvent développer la construction et les propriétés de l\'espace de Sobolev $H_0^1(]0,1[)$, ses propriétés d\'injection dans les fonctions continues, et évoquer le rôle de cet espace dans l\'étude de problèmes aux limites elliptiques en une dimension. Ce développement conduit naturellement à une illustration de la théorie spectrale des opérateurs compacts auto-adjoints.  
',
                'options' => 7,
            ),
            371 => 
            array (
                'id' => 372,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 202,
                'type_id' => 2,
                'nom' => 'Exemples de parties denses et applications.',
                'rapport' => '
Cette leçon permet d\'explorer les questions d\'approximations de fonctions par des polynômes et des polynômes trigonométriques. Au delà des exemples classiques, les candidats plus ambitieux peuvent aller jusqu\'à la résolution d\'équations aux dérivées partielles (ondes, chaleur, Schrödinger) par séries de Fourier.  
',
                'options' => 7,
            ),
            372 => 
            array (
                'id' => 373,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 203,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de compacité.',
                'rapport' => '
Il est important de ne pas concentrer la leçon sur la compacité générale (confusion générale entre utilisation de la notion compacité et notion de compacité ), sans proposer des exemples significatifs d\'utilisation (Stone-Weierstrass, point fixe, voire étude qualitative d\'équations différentielles, etc.). Les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d\'utilisation de la compacité. Les applications linéaires compactes sur l\'espace de Hilbert ou sur un espace de Banach relèvent également de cette leçon, et on pourra développer par exemple leurs propriétés spectrales.
',
                'options' => 15,
            ),
            373 => 
            array (
                'id' => 374,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 204,
                'type_id' => 2,
                'nom' => 'Connexité. Exemples et applications.',
                'rapport' => '
Il est important de présenter des résultats naturels dont la démonstration utilise la connexité ; par exemple, diverses démonstrations du théorème de d\'Alembert-Gauss. On distinguera bien connexité et connexité par arcs, mais il est pertinent de présenter des situations où ces deux notions coïncident.  
',
                'options' => 7,
            ),
            374 => 
            array (
                'id' => 375,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 205,
                'type_id' => 2,
                'nom' => 'Espaces complets. Exemples et applications.',
                'rapport' => '
Les candidats devraient faire apparaître que l’un des intérêts essentiel de la complétude est de fournir des théorèmes d’existence en dimension infinie, en particulier dans les espaces de fonctions. Le théorème de Baire trouverait naturellement sa place dans cette leçon, mais il faut l’accompagner d’applications. Rappelons que celles-ci ne se limitent pas aux théorèmes de Banach-Steinhaus et du graphe fermé, mais qu’on peut évoquer au niveau de l’agrégation l’existence de divers objets : fonctions continues nulle part dérivables, points de continuité pour les limites simples de suites de fonctions continues, vecteurs à orbite dense pour certains opérateurs linéaires, etc.   ',
                'options' => 7,
            ),
            375 => 
            array (
                'id' => 376,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 206,
                'type_id' => 2,
                'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                'rapport' => '
Les applications aux équations différentielles sont importantes. Il faut préparer des contre-exemples pour illustrer la nécessité des hypothèses. Il est envisageable d\'admettre le théorème de point fixe de Brouwer et d\'en développer quelques conséquences.
',
                'options' => 15,
            ),
            376 => 
            array (
                'id' => 377,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 207,
                'type_id' => 2,
                'nom' => 'Prolongement de fonctions. Exemples et applications.',
                'rapport' => '
Les candidats exploitent rarement toutes les potentialités de cette leçon très riche. Le prolongement analytique relève bien sûr de cette leçon, ainsi que le théorème de Hahn-Banach, le prolongement de fonctions $C^\\infty$ sur un segment en fonctions de la même classe, le théorème de Tietze sur l’extension des fonctions continues définies sur un sous-ensemble fermé d’un espace métrique, l’intégrale de Riemann, la transformation de Fourier sur $L^2$ et l’extension des fonctions Lipschitziennes définies sur un sous-ensemble (pas nécesairement dense) d’un espace métrique.   ',
                'options' => 7,
            ),
            377 => 
            array (
                'id' => 378,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 208,
                'type_id' => 2,
                'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                'rapport' => '
La justification de la compacité de la boule unité en dimension finie doit être donnée. Le théorème d’équivalence des normes en dimension finie, ou le caractère fermé de tout sous-espace de dimension finie d’un espace normé, sont des résultats fondamentaux à propos desquels les candidats doivent se garder des cercles vicieux.   
',
                'options' => 15,
            ),
            378 => 
            array (
                'id' => 379,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 209,
                'type_id' => 2,
                'nom' => 'Approximation d\'une fonction par des polynômes et des polynômes trigonométriques. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            379 => 
            array (
                'id' => 380,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 213,
                'type_id' => 2,
                'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                'rapport' => '
Il est important de faire la différence entre base algébrique et base hilbertienne. Il faut connaître quelques critères simples pour qu’une famille orthogonale forme une base hilbertienne et illsutrer la leçon par des exemples de bases hilbertiennes (polynômes orthogonaux, séries de Fourier, etc...). Le théorème de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d’un espace de Hilbert H est régulièrement mentionné. En revanche, la possibilité de construire de façon élémentaire le dit-projeté dans le cas particulier d’un sous-espace vectoriel de dimension finie semble Les candidats doivent s’intéresser au sens des formules $x = \\sum_{n \\geq 0} (x|e_n)e_n$ et $||x||^2 = \\sum_{n \\geq 0} (x|e_n)^2$ en précisant les hypothèses sur la famille $(e_n )_{ n \\in N}$ et en justifiant la convergence.
',
                'options' => 7,
            ),
            380 => 
            array (
                'id' => 381,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 214,
                'type_id' => 2,
                'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
                'rapport' => '
On attend des applications en géométrie différentielle (notamment dans la formulation des multiplicateurs de Lagrange). Rappelons que les sous-variétés sont au programme.   
',
                'options' => 15,
            ),
            381 => 
            array (
                'id' => 382,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 215,
                'type_id' => 2,
                'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                'rapport' => '
Il faudrait que les candidats à l’Agrégation sachent que les différentielles d’ordre supérieur $d^kf(a)$ définissent des applications $k$-linéaires (sur quel espace ?). Il faut savoir calculer sur des exemples simples, la différentielle d’une fonction, ou effectuer un développement limité à l’ordre 1 d’une fonction.   
',
                'options' => 15,
            ),
            382 => 
            array (
                'id' => 383,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 216,
                'type_id' => 2,
                'nom' => 'Étude métrique des courbes. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            383 => 
            array (
                'id' => 384,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 217,
                'type_id' => 2,
                'nom' => 'Sous-variétés de $R^n$. Exemples.',
                'rapport' => '
Cette leçon n’a pas eu beaucoup de succès, c’est bien dommage. Elle ne saurait être réduite à un cours de géométrie différentielle abstraite ; ce serait un contresens. Le jury attend une leçon concrète, montrant une compréhension géométrique locale. Aucune notion globale n’est exigible, ni de notion de variété abstraite. Le candidat doit pouvoir être capable de donner plusieurs représentations locales (paramétriques, équations, etc.) et d’illustrer la notion d’espace tangent sur des exemples classiques. Le jury invite les candidats à réfléchir à la pertinence de l’introduction de la notion de sous-variétés. En ce qui concerne les surfaces de $R^3$ , les candidats sont invités à réfléchir aux notions de formes quadratiques fondamentales et à leurs interprétations géométriques. Le théorème des extremas liés devient assez transparent lorsqu’on le traite par les sous-variétés. 

Le théorème des extrema liés peut être évoqué dans cette leçon. Les groupes classiques donnent des exemples utiles de sous-variétés.

',
                'options' => 7,
            ),
            384 => 
            array (
                'id' => 385,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 218,
                'type_id' => 2,
                'nom' => 'Applications des formules de Taylor.',
                'rapport' => '
Il faut connaître les formules de Taylor des polynômes et certains développements très classiques. Il y a de très nombreuses applications en géométrie et probabilités (le théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités $||f^{(k)} || \\leq 2^{\\frac{k(n-k)}{2}} ||f||^{1-k/n} ||f^{(n)}||^{k/n}$ (lorsque f et sa dérivée n-ème sont bornées). On soignera particulièrement le choix des développements.  
',
                'options' => 15,
            ),
            385 => 
            array (
                'id' => 386,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 219,
                'type_id' => 2,
                'nom' => 'Exremums : existence, caractérisation, recherche. Exemples et applications.',
                'rapport' => '
Il faut bien faire la distinction entre propriétés locales (caractérisation d’un extremum) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local st également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon, plus généralement les algorithmes de recherche d’extremas ont toute leur place.
',
                'options' => 15,
            ),
            386 => 
            array (
                'id' => 387,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 220,
                'type_id' => 2,
            'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'étude des solutions en dimension $1$ et $2$.',
                'rapport' => '
Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est rarement énoncé. L’utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en oeuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées.   
',
                'options' => 15,
            ),
            387 => 
            array (
                'id' => 388,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 221,
                'type_id' => 2,
                'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                'rapport' => '
Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est rarement énoncé. L’utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en œuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées.
',
                'options' => 15,
            ),
            388 => 
            array (
                'id' => 389,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 223,
                'type_id' => 2,
                'nom' => 'Suites numériques. Convergence, valeurs d\'adhérence. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            389 => 
            array (
                'id' => 390,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 224,
                'type_id' => 2,
                'nom' => 'Exemples de développements asymptotiques de suites et de fonctions.',
                'rapport' => '',
                'options' => 15,
            ),
            390 => 
            array (
                'id' => 391,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 226,
                'type_id' => 2,
            'nom' => 'Suites vectorielles et réelles définies par une relation de récurrence $u_{n+1} = f(u_n)$. Exemples et applications.',
                'rapport' => '
Le jury attend d’autres exemples que la traditionnelle suite récurrente $u_{n+1} = sin(u_n)$. Les suites homographiques réelles ou complexes fournissent des exemples intéressants, rarement évoqués. La méthode du gradient ou les méthodes de Jacobi ou de Gauss-Siedel fournissent des exemples naturels et utiles de suites vectorielles. Le théorème de Sharkovski sur l’itération des fonctions continues sur un intervalle est un résultat récent qui peut se traiter entièrement au niveau de l’agrégation, et il trouve sa place dans cette leçon. Les notions de point attractif ou répulsif sont essentielles et doivent être connues. Enfin les itérations matricielles ont leur place dans cette leçon.
',
                'options' => 15,
            ),
            391 => 
            array (
                'id' => 392,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 228,
                'type_id' => 2,
                'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
                'rapport' => '
Un plan découpé en deux parties (I - Continuité, II - Dérivabilité) n’est pas le mieux adapté. Les théorèmes de base doivent être maîtrisés et illustrés par des exemples intéressants. Les candidats doivent disposer d’un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La dérivabilité presque partout des fonctions Lipschitziennes relève de cette leçon. Enfin les applications du théorème d’Ascoli (par exemple les opérateurs intégraux à noyau continu, le théorème de Peano, etc), sont les  bienvenues.
',
                'options' => 7,
            ),
            392 => 
            array (
                'id' => 393,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 229,
                'type_id' => 2,
                'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                'rapport' => '
Les candidats sont invités à réfléchir à l\'incidence de ces notions en théorie des probabilités. La dérivabilité presque partout des fonctions monotones est un résultat important. Il est souhaitable d\'illustrer la présentation de la convexité par des dessins clairs, même si ces dessins ne peuvent remplacer un calcul. On notera que la monotonie concerne (à ce niveau) les fonctions réelles d\'une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de $R^n$ , qui fournissent de beaux exemples d\'utilisation. L\'espace vectoriel engendré par les fonctions monotones (les fonctions à variation bornée) relève de cette leçon. La dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité et les candidats bien préparés peuvent s\'aventurer utilement dans cette direction.  
',
                'options' => 15,
            ),
            393 => 
            array (
                'id' => 394,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 230,
                'type_id' => 2,
                'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                'rapport' => '
De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être véritablement hors sujet, cette exposition ne doit pas former l’essentiel du plan. Le thème central de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, etc...) et leurs applications diverses, comme par exemple des résultats d’irrationalité, voire de transcendance. Enfin on rappelle que la transformation d’Abel trouve toute sa place dans cette leçon.
',
                'options' => 15,
            ),
            394 => 
            array (
                'id' => 395,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 232,
                'type_id' => 2,
            'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                'rapport' => '
Le jury attire l\'attention sur le fait que $X$ peut désingner un vecteur.
',
                'options' => 15,
            ),
            395 => 
            array (
                'id' => 396,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 234,
                'type_id' => 2,
                'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                'rapport' => '
Le jury a apprécié les candidats sachant montrer qu\'avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p \\subset L^q$ si $p \\geq q$). Il est important de pouvoir justifier l\'existence de produits de convolution (exemple $L^1 \\star L^1$).
',
                'options' => 7,
            ),
            396 => 
            array (
                'id' => 397,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 235,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions intégrables. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            397 => 
            array (
                'id' => 398,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 236,
                'type_id' => 2,
                'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                'rapport' => '',
                'options' => 15,
            ),
            398 => 
            array (
                'id' => 399,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 239,
                'type_id' => 2,
                'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
                'rapport' => '
Cette leçon doit être enrichie par des études et méthodes asymptotiques et les transformations classiques (Fourier, Laplace, etc.).  
',
                'options' => 15,
            ),
            399 => 
            array (
                'id' => 400,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 240,
                'type_id' => 2,
                'nom' => 'Produit de convolution, transformation de Fourier. Applications.',
                'rapport' => '
Cette leçon ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C’est bien une leçon d’analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. La transformation de Fourier des distributions tempérées trouve sa place ici. Certains candidats considérent l’extension de la transformée de Fourier à la variable complexe, riche d’applications (dans la direction du théorème de Paley-Wiener, par exemple).
',
                'options' => 15,
            ),
            400 => 
            array (
                'id' => 401,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 241,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            401 => 
            array (
                'id' => 402,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 243,
                'type_id' => 2,
                'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                'rapport' => '
Il faut éviter de ne parler que de dérivabilité par rapport à une variable réelle quand on énonce (ou utilise) ensuite ces résultats sur les fonctions holomorphes. Les séries entières fournissent une méthode puissante d’extension des fonctions au plan complexe, puis au calcul fonctionnel et cela peut être évoqué. Le comportement au bord du disque de convergence (Théorèmes d’Abel, de Tauber et de Hardy-Littlewood) fournit de bons thèmes de développement applicables par exemple à des conditions suffisantes pour que le produit de Cauchy de deux séries semi-convergentes soit convergent.
',
                'options' => 15,
            ),
            402 => 
            array (
                'id' => 403,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 244,
                'type_id' => 2,
                'nom' => 'Fonctions développables en série entière, fonctions analytiques. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            403 => 
            array (
                'id' => 404,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 245,
                'type_id' => 2,
                'nom' => 'Fonctions holomorphes sur un ouvert de $C$. Exemples et applications.',
                'rapport' => '
Les conditions de Cauchy-Riemann doivent être parfaitement connues Z et l’interprétation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z) dz$ a un sens précis, qu’il faut savoir expliquer. Par ailleurs il faut connaître la définition d’une fonction méromorphe (l’ensemble des  pôles doit être une partie fermée discrète) !
',
                'options' => 7,
            ),
            404 => 
            array (
                'id' => 405,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 246,
                'type_id' => 2,
                'nom' => 'Séries de Fourier. Exemples et applications.',
                'rapport' => '
Les différents modes de convergence ($L^2$ , Fejer, Dirichlet, etc...) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). Dans le cas d’une fonction continue et $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet. Cette leçon ne doit pas se réduire à un cours abstrait sur les coefficients de Fourier.
',
                'options' => 15,
            ),
            405 => 
            array (
                'id' => 406,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 247,
                'type_id' => 2,
                'nom' => 'Exemples de problèmes d\'interversion de limites.',
                'rapport' => '',
                'options' => 7,
            ),
            406 => 
            array (
                'id' => 407,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 249,
                'type_id' => 2,
                'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
                'rapport' => '',
                'options' => 7,
            ),
            407 => 
            array (
                'id' => 408,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 253,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de convexité en analyse.',
                'rapport' => '',
                'options' => 7,
            ),
            408 => 
            array (
                'id' => 409,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 254,
                'type_id' => 2,
            'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées. Transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
                'rapport' => '
Les leçons 254 et 255 qui portent sur la théorie des distributions et son utilisation ont été choisies par quelques candidats, qui étaient souvent bien préparés et dont la présentation s’est avérée satisfaisante. Rappelons une fois de plus que les attentes du jury sur ces leçons restent modestes, et se placent au niveau de ce qu’un cours de M1 standard sur le sujet peut contenir. Aucune subtilité topologique portant sur l’espace des distributions tempérées n’est attendue. Les candidats sont invités à présenter des situations simples où les distributions sont effectivement utilisées, et à avoir les idées claires sur ce qu’est le support d’une distribution ou son ordre. Les thèmes de développement possibles incluent les formules de saut, les solutions élémentaires (pour le Laplacien par exemple), les conditions sous lesquelles le produit de convolution de deux distributions peut être défini, ou son associativité dans certains cas. La dérivation au sens des distributions des fonctions d’une seule variable réelle fournit déjà une problématique intéressante. Des applications simples aux équations aux dérivées partielles linéaires sont également les bienvenues.
',
                'options' => 7,
            ),
            409 => 
            array (
                'id' => 410,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:46:00',
                'annee' => 2013,
                'numero' => 255,
                'type_id' => 2,
                'nom' => 'Espaces de Scwartz. Distributions. Dérivation au sens des distributions.',
                'rapport' => '
Les leçons 254 et 255 qui portent sur la théorie des distributions et son utilisation ont été choisies par quelques candidats, qui étaient souvent bien préparés et dont la présentation s’est avérée satisfaisante. Rappelons une fois de plus que les attentes du jury sur ces leçons restent modestes, et se placent au niveau de ce qu’un cours de M1 standard sur le sujet peut contenir. Aucune subtilité topologique portant sur l’espace des distributions tempérées n’est attendue. Les candidats sont invités à présenter des situations simples où les distributions sont effectivement utilisées, et à avoir les idées claires sur ce qu’est le support d’une distribution ou son ordre. Les thèmes de développement possibles incluent les formules de saut, les solutions élémentaires (pour le Laplacien par exemple), les conditions sous lesquelles le produit de convolution de deux distributions peut être défini, ou son associativité dans certains cas. La dérivation au sens des distributions des fonctions d’une seule variable réelle fournit déjà une problématique intéressante. Des applications simples aux équations aux dérivées partielles linéaires sont également les bienvenues.
',
                'options' => 7,
            ),
            410 => 
            array (
                'id' => 411,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 260,
                'type_id' => 2,
                'nom' => 'Espérance, variance et moments d\'une variable aléatoire.',
                'rapport' => '',
                'options' => 15,
            ),
            411 => 
            array (
                'id' => 412,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 261,
                'type_id' => 2,
                'nom' => 'Fonction caractéristique et transformée de Laplace d\'une variable aléatoire. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            412 => 
            array (
                'id' => 413,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 262,
                'type_id' => 2,
                'nom' => 'Modes de convergence d\'une suite de variables aléatoires. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            413 => 
            array (
                'id' => 414,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 263,
                'type_id' => 2,
                'nom' => 'Variables aléatoires à densité. Exemples et applications.',
                'rapport' => '',
                'options' => 7,
            ),
            414 => 
            array (
                'id' => 415,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 264,
                'type_id' => 2,
                'nom' => 'Variables aléatoires discrètes. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            415 => 
            array (
                'id' => 416,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 901,
                'type_id' => 3,
                'nom' => 'Structure de données : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            416 => 
            array (
                'id' => 417,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 902,
                'type_id' => 3,
                'nom' => 'Diviser pour régner : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            417 => 
            array (
                'id' => 418,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 903,
                'type_id' => 3,
                'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                'rapport' => '',
                'options' => 8,
            ),
            418 => 
            array (
                'id' => 419,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 906,
                'type_id' => 3,
                'nom' => 'Programmation dynamique : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            419 => 
            array (
                'id' => 420,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 907,
                'type_id' => 3,
                'nom' => 'Algorithmique du texte : exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            420 => 
            array (
                'id' => 421,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 909,
                'type_id' => 3,
                'nom' => 'Langages rationnels. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            421 => 
            array (
                'id' => 422,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 910,
                'type_id' => 3,
                'nom' => 'Langages algébriques. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            422 => 
            array (
                'id' => 423,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 912,
                'type_id' => 3,
                'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            423 => 
            array (
                'id' => 424,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 913,
                'type_id' => 3,
                'nom' => 'Machines de Turing. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            424 => 
            array (
                'id' => 425,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 914,
                'type_id' => 3,
                'nom' => 'Décidabilité et indécidabilité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            425 => 
            array (
                'id' => 426,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 915,
                'type_id' => 3,
                'nom' => 'Classes de complexité : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            426 => 
            array (
                'id' => 427,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 916,
                'type_id' => 3,
                'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            427 => 
            array (
                'id' => 428,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 917,
                'type_id' => 3,
                'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                'rapport' => '',
                'options' => 8,
            ),
            428 => 
            array (
                'id' => 429,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 918,
                'type_id' => 3,
                'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            429 => 
            array (
                'id' => 430,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 919,
                'type_id' => 3,
                'nom' => 'Unification : algorithmes et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            430 => 
            array (
                'id' => 431,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 920,
                'type_id' => 3,
                'nom' => 'Réécriture et formes normales. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            431 => 
            array (
                'id' => 432,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 921,
                'type_id' => 3,
                'nom' => 'Algorithmes de recherche et structures de données associées.',
                'rapport' => '',
                'options' => 8,
            ),
            432 => 
            array (
                'id' => 433,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 922,
                'type_id' => 3,
                'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            433 => 
            array (
                'id' => 434,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 923,
                'type_id' => 3,
                'nom' => 'Analyses lexicale et syntaxique : applications.',
                'rapport' => '',
                'options' => 8,
            ),
            434 => 
            array (
                'id' => 435,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 924,
                'type_id' => 3,
                'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            435 => 
            array (
                'id' => 436,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 925,
                'type_id' => 3,
                'nom' => 'Graphes : représentations et algorithmes.',
                'rapport' => '',
                'options' => 8,
            ),
            436 => 
            array (
                'id' => 437,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 926,
                'type_id' => 3,
                'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            437 => 
            array (
                'id' => 438,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 927,
                'type_id' => 3,
                'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                'rapport' => '',
                'options' => 8,
            ),
            438 => 
            array (
                'id' => 439,
                'created_at' => '2016-03-14 12:36:32',
                'updated_at' => '2016-03-14 12:36:32',
                'annee' => 2013,
                'numero' => 928,
                'type_id' => 3,
                'nom' => 'Problème NP-complets : exemples de réductions.',
                'rapport' => '',
                'options' => 8,
            ),
            439 => 
            array (
                'id' => 440,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-16 08:03:28',
                'annee' => 2014,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
            'rapport' => 'Il faut bien dominer les deux approches de l\'action de groupe : l\'approche naturelle et l\'approche, plus subtile, via le morphisme qui relie le groupe agissant et le groupe des permutations de l\'ensemble sur lequel il agit. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des polynômes. Les exemples issus de la géométrie ne manquent pas (groupes d\'isométries d\'un solide). Certains candidats décrivent les actions naturelles de $PGL(2, F_q)$ sur la droite projective qui donnent des injections intéressantes pour $q = 2, 3$ et peuvent plus généralement en petit cardinal donner lieu à des isomorphismes de groupes. Enfin, on pourra noter que l\'injection du groupe de permutations dans le groupe linéaire par les matrices de permutations donne lieu à des représentations dont il est facile de déterminer le caractère.
',
                'options' => 7,
            ),
            440 => 
            array (
                'id' => 441,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-20 15:31:44',
                'annee' => 2014,
                'numero' => 102,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
            'rapport' => 'Cette leçon est encore abordée de façon élémentaire sans réellement expliquer où et comment les nombres complexes de modules 1 et les racines de l\'unité apparaissent dans divers domaines des mathématiques (polynômes cyclotomiques, théorie des représentations, spectre de certaines matrices remarquables).
',
                'options' => 7,
            ),
            441 => 
            array (
                'id' => 442,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:51:09',
                'annee' => 2014,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples de sous-groupes distingués et de groupes quotients. Applications.',
            'rapport' => 'Les candidats parlent de groupe simple et de sous-groupe dérivé ou de groupe quotient sans savoir utiliser ces notions. Entre autres, il faut savoir pourquoi on s\'intéresse particulièrement aux groupes simples. La notion de produit semi-direct n\'est plus au programme, mais lorsqu\'elle est utilisée, il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral $D_n$ par exemple).

On pourra noter que les tables de caractères permettent d\'illustrer toutes ces notions.
',
                'options' => 7,
            ),
            442 => 
            array (
                'id' => 443,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:51:51',
                'annee' => 2014,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => 'Les exemples doivent figurer en bonne place dans cette leçon. On peut par exemple étudier les groupes de symétries $A_4$ , $S_4$ , $A_5$ et relier sur ces exemples géométrie et algèbre, les représentations ayant ici toute leur place. Le théorème de structure des groupes abéliens finis doit être connu.

On attend des candidats de savoir manipuler correctement les éléments de quelques structures usuelles ($Z/nZ$, $ S_n$, etc.). Par exemple, proposer un générateur simple de $(Z/nZ, +)$ voire tous les générateurs, calculer aisément un produit de deux permutations, savoir décomposer une permutation en produit de cycles à support disjoint.
Il est important que la notion d\'ordre d\'un élément soit mentionnée et comprise dans des cas simples.
',
                'options' => 15,
            ),
            443 => 
            array (
                'id' => 444,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:52:19',
                'annee' => 2014,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
            'rapport' => 'Il faut relier rigoureusement les notions d\'orbites et d\'action de groupe. Il faut aussi savoir décomposer une permutation en cycles disjoints, tant sur le plan théorique (preuve du théorème de décomposition), que pratique (sur un exemple). Des dessins ou des graphes illustrent de manière commode ce que sont les permutations. Par ailleurs un candidat qui se propose de démontrer que tout groupe simple d\'ordre 60 est isomorphe à $A_5$ devrait aussi savoir montrer que $A_5$ est simple.

L\'existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l\'objet d\'un développement.

Comme pour toute structure algébrique, il est souhaitable de s\'intéresser aux automorphismes du groupe symétrique. Les applications du groupe symétrique ne concernent pas seulement les polyèdres réguliers. Il faut par exemple savoir faire le lien avec les actions de groupe sur un ensemble fini. Il est important de savoir déterminer les classes de conjugaisons du groupe symétrique par la décomposition en cycles.
',
                'options' => 15,
            ),
            444 => 
            array (
                'id' => 445,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:52:45',
                'annee' => 2014,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
            'rapport' => 'Cette leçon est souvent présentée comme un catalogue de résultats épars et zoologiques sur $GL(E)$. Il faudrait que les candidats sachent faire correspondre sous-groupes et noyaux ou stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). À quoi peuvent servir des générateurs du groupe $GL(E)$ ? Qu\'apporte la topologie dans cette leçon ? Il est préférable de se poser ces questions avant de les découvrir le jour de l\'oral.

Certains candidats affirment que $GL_n(K)$ est dense (respectivement ouvert) dans $M_n(K)$ . Il est judicieux de préciser les hypothèses nécessaires sur le corps K ainsi que la topologie sur $M_n(K)$.

Il faut aussi savoir réaliser $S_n$ dans $GL(n,R)$ et faire le lien entre signature et déterminant.
',
                'options' => 15,
            ),
            445 => 
            array (
                'id' => 446,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:53:06',
                'annee' => 2014,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => 'Il s\'agit d\'une leçon où théorie et exemples doivent apparaître. Le candidat doit d\'une part savoir dresser une table de caractères pour des petits groupes. Il doit aussi savoir tirer des informations sur le groupe à partir de sa table de caractères, et aussi savoir trouver la table de caractères de certains sous-groupes. 

Les développements prouvent souvent qu\'un candidat qui sait manier les techniques de base sur les caractères ne sait pas forcément relier ceux-ci aux représentations. 

Dans le même ordre d\'idée, le lemme de Schur est symptomatique d\'une confusion : dans le cas où les deux représentations $V$ et $V\'$ sont isomorphes, on voit que les candidats confondent isomorphisme de $V$ dans $V\'$ avec endomorphisme de $V$. Ce qui revient implicitement à identifier $V$ et $V\'$ , ce que le candidat devrait faire de façon consciente et éclairée.
',
                'options' => 7,
            ),
            446 => 
            array (
                'id' => 447,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:53:30',
                'annee' => 2014,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => 'C\'est une leçon qui demande un minimum de culture mathématique. Peu de candidats voient l\'utilité des parties génératrices dans l\'analyse des morphismes de groupes ou pour montrer la connexité de certains groupes.
',
                'options' => 15,
            ),
            447 => 
            array (
                'id' => 448,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:53:57',
                'annee' => 2014,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Exemples et représentations de groupes finis de petit cardinal.',
            'rapport' => 'Il s\'agit d\'une leçon où le matériel théorique doit figurer pour ensuite laisser place à des exemples. Les représentations peuvent provenir d\'actions de groupes sur des ensembles finis, de groupes d\'isométries, d\'isomorphismes exceptionnels entre groupes de petit cardinal... Inversement, on peut chercher à interpréter des représentations de façon géométrique, mais il faut avoir conscience qu\'une table de caractères provient généralement de représentations complexes et non réelles (a priori). Pour prendre un exemple ambitieux, la construction de l\'icosaèdre à partir de la table de caractères de $A_5$ demande des renseignements sur l\'indice de Schur (moyenne des caractères sur les carrés des éléments du groupe).
',
                'options' => 7,
            ),
            448 => 
            array (
                'id' => 449,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:54:18',
                'annee' => 2014,
                'numero' => 110,
                'type_id' => 1,
                'nom' => 'Caractères d\'un groupe abélien fini et transformée de Fourier discrète. Applications.',
                'rapport' => 'Il s\'agit d\'une nouvelle leçon pour laquelle le jury attend une synthèse de résultats théoriques et des applications détaillées. En particulier on pourra y introduire la transformée de Fourier rapide sur un groupe abélien d\'ordre une puissance de 2 ainsi que des applications à la multiplication d\'entiers, de polynômes et éventuellement au décodage de codes via la transformée de Hadamard.
',
                'options' => 7,
            ),
            449 => 
            array (
                'id' => 450,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:54:41',
                'annee' => 2014,
                'numero' => 120,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications.',
                'rapport' => 'Cette leçon, plus élémentaire, demande toutefois une préparation minutieuse. Tout d\'abord $n$ n\'est pas forcément un nombre premier. Il serait bon de connaître les sous-groupes de $Z/nZ$ et les morphismes de groupes de $Z/nZ$ dans $Z/mZ$. 

Bien maîtriser le lemme chinois et sa réciproque. Savoir appliquer le lemme chinois à l\'étude du groupe des inversibles. Distinguer clairement propriétés de groupes additifs et d\'anneaux. Connaître les automorphismes, les nilpotents, les idempotents. Enfin, les candidats sont invités à rendre hommage à Gauss en présentant quelques applications arithmétiques des anneaux $Z/nZ$, telles l\'étude de quelques équations diophantiennes bien choisies.
',
                'options' => 15,
            ),
            450 => 
            array (
                'id' => 451,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:54:55',
                'annee' => 2014,
                'numero' => 121,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
            'rapport' => 'Il s\'agit d\'une leçon pouvant être abordée à divers niveaux. Attention au choix des développements, ils doivent être pertinents (l\'apparition d\'un nombre premier n\'est pas suffisant !). La réduction modulo $p$ n\'est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important, qu\'il faudrait citer. Sa démonstration n\'est bien-sûr pas exigible au niveau de l\'Agrégation. Quelques résultats sur la géométrie des corps finis sont les bienvenus.
',
                'options' => 15,
            ),
            451 => 
            array (
                'id' => 452,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:55:10',
                'annee' => 2014,
                'numero' => 122,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Exemples et applications.',
            'rapport' => 'Les plans sont trop théoriques. Il est possible de présenter des exemples d\'anneaux principaux classiques autres que $Z$ et $K[X]$ (décimaux, entiers de Gauss ou d\'Eisenstein), accompagnés d\'une description de leurs irréductibles. Les applications en algèbre linéaire ne manquent pas, il serait bon que les candidats les illustrent. Par exemple, il est étonnant de ne pas voir apparaître la notion de polynôme minimal parmi les applications.

On peut donner des exemples d\'anneaux non principaux, mais aussi des exemples d\'équations diophantiennes résolues à l\'aide d\'anneaux principaux. A ce sujet, il sera fondamental de savoir déterminer les unités d\'un anneau, et leur rôle au moment de la décomposition en facteurs premiers. 

On a pu noter dans cette leçon l\'erreur répandue que $1+i$ et $1-i$ sont des irréductibles premiers entre eux dans l\'anneau factoriel $Z[i]$.
',
                'options' => 7,
            ),
            452 => 
            array (
                'id' => 453,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:55:25',
                'annee' => 2014,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => 'Un candidat qui étudie les carrés dans un corps fini doit savoir aussi résoudre les équations de degré 2. Les constructions des corps de petit cardinal doivent avoir été pratiquées. Les injections des divers $F_q$ doivent être connues. 

Le théorème de Wedderburn ne doit pas constituer le seul développement de cette leçon. En revanche, les applications des corps finis (y compris pour $F_q$ avec $q$ non premier !) ne doivent pas être négligées. Citons par exemple l\'étude de polynômes à coefficients entiers. 

Le théorème de l\'élément primitif, s\'il est énoncé, doit pouvoir être utilisé.
',
                'options' => 15,
            ),
            453 => 
            array (
                'id' => 454,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:55:35',
                'annee' => 2014,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => 'C\'est une leçon qui doit être illustrée par de nombreux exemples et applications, souvent en lien avec les séries génératrices ; combinatoire, calcul des sommes de Newton, relations de récurrence, nombre de partitions, représentations et séries de Molien, etc. 

A ce propos, on note que les candidats qui choisissent de présenter en développement les séries de Molien ne savent que rarement interpréter ces séries obtenues sur des exemples simples. Ces séries ne font pas que calculer les dimensions de sous-espaces d\'invariants, elles suggèrent aussi des choses plus profondes sur la structure de l\'algèbre d\'invariants.
',
                'options' => 7,
            ),
            454 => 
            array (
                'id' => 455,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:55:45',
                'annee' => 2014,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Extensions de corps. Exemples et applications.',
            'rapport' => 'Très peu de candidats ont choisi cette leçon. On doit y voir le théorème de la base téléscopique et ses applications à l\'irréductibilité de certains polynômes, ainsi que les corps finis. Une version dégradée de la théorie de Galois (qui n\'est pas au programme) est très naturelle dans cette leçon.
',
                'options' => 7,
            ),
            455 => 
            array (
                'id' => 456,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:55:57',
                'annee' => 2014,
                'numero' => 126,
                'type_id' => 1,
                'nom' => 'Exemples d\'équations diophantiennes.',
            'rapport' => 'Il s\'agit d\'une leçon nouvelle ou plus exactement d\'une renaissance. On attend là les notions de bases servant à aborder les équations de type $ax + by = d$ (identité de Bezout, lemme de Gauss), les systèmes de congruences, avec le lemme des noyaux. A ce sujet, il est important que le candidat connaisse l\'image du morphisme du lemme des noyaux lorsque les nombres ne sont pas premiers entre eux. 

On attend bien entendu la méthode de descente et l\'utilisation de la réduction modulo un nombre premier $p$. 

La leçon peut aussi dériver vers la notion de factorialité, illustrée par des équations de type Mordell, Pell-Fermat, et même Fermat (pour $n = 2$, ou pour les nombres premiers de Sophie Germain).
',
                'options' => 7,
            ),
            456 => 
            array (
                'id' => 457,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:56:11',
                'annee' => 2014,
                'numero' => 127,
                'type_id' => 1,
                'nom' => 'Droite projective et birapport.',
            'rapport' => 'Il s\'agit également d\'une leçon nouvelle reprenant un titre ancien. Le birapport peut être vu comme un invariant pour l\'action du groupe linéaire $GL(2, K)$ (ou plus finement son quotient projectif $PGL(2, K)$) sur l\'ensemble $P^1(K)$ des droites du plan vectoriel $K^2$.

Lorsque le corps $K$ est le corps des complexes, il ne faudra pas manquer d\'en voir les applications à la cocyclicité, et à l\'étude des droites et cercles du plan affine euclidien.

On peut s\'aider du birapport, sur des corps finis, afin de construire des isomorphismes classiques entre groupes finis de petit cardinal.

L\'ensemble des droites du plan contenant un point fixe est naturellement une droite projective. Cela permet enfin d\'identifier une conique ayant au moins un point à une droite projective : c\'est l\'idée d\'une preuve classique du théorème de l\'hexagramme de Pascal. Par ailleurs, on pourra remarquer le birapport dans l\'expression de la distance hyperbolique sur le demi-plan de Poincaré.
',
                'options' => 7,
            ),
            457 => 
            array (
                'id' => 458,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:56:22',
                'annee' => 2014,
                'numero' => 140,
                'type_id' => 1,
                'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
            'rapport' => 'Le bagage théorique est somme toute assez classique, même si parfois le candidat ne voit pas l\'unicité de la décomposition en éléments simples en terme d\'indépendance en algèbre linéaire. Ce sont surtout les applications qui sont attendues : séries génératrices (avec la question à la clef : à quelle condition une série formelle est-elle le développement d\'une fraction rationnelle), automorphismes de $K(X)$, version algébrique du théorème des résidus, action par homographies. 

Le théorème de Luroth n\'est pas obligatoire et peut même se révéler un peu dangereux si le candidat n\'est pas suffisamment préparé aux questions classiques qui l\'attendent.
',
                'options' => 7,
            ),
            458 => 
            array (
                'id' => 459,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:56:32',
                'annee' => 2014,
                'numero' => 141,
                'type_id' => 1,
                'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
            'rapport' => 'Les applications ne concernent pas que les corps finis. Il existe des corps algébriquement clos de caractéristique nulle autre que $C$. Un polynôme réductible n\'admet pas forcément de racines. Il est instructif de chercher des polynômes irréductibles de degré 2, 3, 4 sur $F_2$ . Il faut connaître le théorème de la base téléscopique ainsi que les utilisations artithmétiques (utilisation de la divisibilité) que l\'on peut en faire dans l\'étude de l\'irréductibilité des polynômes.
',
                'options' => 15,
            ),
            459 => 
            array (
                'id' => 460,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:56:43',
                'annee' => 2014,
                'numero' => 142,
                'type_id' => 1,
                'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                'rapport' => 'La leçon ne doit pas se concentrer exclusivement sur les aspects formels ni sur les les polynômes symétriques.

Les aspects arithmétiques ne doivent pas être négligés. Il faut savoir montrer l\'irréductibilité d\'un polynôme à plusieurs indéterminées en travaillant sur un anneau de type $A[X]$, où $A$ est factoriel.  Le théorème fondamental sur la structure de l\'algèbre des polynômes symétriques est vrai sur $Z$.

L\'algorithme peut être présenté sur un exemple.

Les applications aux quadriques, aux relations racines coefficients ne doivent pas être négligées. On peut faire agir le groupe $GL(n, R)$ sur les polynômes à $n$ indéterminées de degré inférieur à 2.
',
                'options' => 7,
            ),
            460 => 
            array (
                'id' => 461,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:56:55',
                'annee' => 2014,
                'numero' => 143,
                'type_id' => 1,
                'nom' => 'Résultant. Applications.',
            'rapport' => 'Le caractère entier du résultant (il se définit sur $Z$) doit être mis en valeur et appliqué.

La partie application doit montrer la diversité du domaine (par exemple en arithmétique, calcul d\'intersection/élimination, calcul différentiel, polynômes annulateurs d\'entiers algèbriques).

Il ne faut pas perdre de vue l\'application linéaire sous-jacente $(U,V) \\rightarrow AU + BV$ qui lie le résultant et le PGCD de $A$ et $B$.
',
                'options' => 7,
            ),
            461 => 
            array (
                'id' => 462,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:57:04',
                'annee' => 2014,
                'numero' => 144,
                'type_id' => 1,
                'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Exemples et applications.',
            'rapport' => 'Il s\'agit d\'une leçon au spectre assez vaste. On peut y traiter de méthodes de résolutions, de théorie des corps (voire théorie de Galois si affinités), de topologie (continuité des racines) ou même de formes quadratiques. Il peut être pertinent d\'introduire la notion de polynôme scindé, de citer le théorème de d\'Alembert-Gauss et des applications des racines (valeurs propres, etc.). On pourra parler des applications de la réduction au calcul d\'approximations de racines.
',
                'options' => 7,
            ),
            462 => 
            array (
                'id' => 463,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:57:18',
                'annee' => 2014,
                'numero' => 150,
                'type_id' => 1,
                'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
            'rapport' => 'Cette leçon n\'a pas souvent été prise, elle demande un certain recul. Les actions ne manquent pas et selon l\'action, on pourra dégager d\'une part des invariants (rang, matrices échelonnées réduites), d\'autre part des algorithmes. On peut aussi, si l\'on veut aborder un aspect plus théorique, faire apparaître à travers ces actions quelques décompositions célèbres, ainsi que les adhérences d\'orbites, lorsque la topologie s\'y prête.
',
                'options' => 15,
            ),
            463 => 
            array (
                'id' => 464,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:57:28',
                'annee' => 2014,
                'numero' => 151,
                'type_id' => 1,
            'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                'rapport' => 'C\'est une leçon qui, contrairement aux apparences, est devenue difficile pour les candidats. Nombre d\'entre eux n\'ont pas été capables de donner des réponses satisfaisantes à des questions élémentaires comme : un sous-espace vectoriel d\'un espace vectoriel de dimension finie, est-il aussi de dimension finie ?

Il faut bien connaître les théorèmes fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. 

Les diverses caractérisations du rang doivent être connues.
',
                'options' => 15,
            ),
            464 => 
            array (
                'id' => 465,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:57:41',
                'annee' => 2014,
                'numero' => 152,
                'type_id' => 1,
                'nom' => 'Déterminant. Exemples et applications.',
            'rapport' => 'Il faut que le plan soit cohérent ; si le déterminant n\'est défini que sur $R$ ou $C$, il est délicat de définir $det(A - XIn)$ avec $A$ une matrice carrée. L\'interprétation du déterminant comme volume est essentielle. Beaucoup de candidats commencent la leçon en disant que le sous-espace des formes $n$-linéaires alternées sur un espace de dimension $n$ est de dimension 1, ce qui est fort à propos.
Toutefois, il est essentiel de savoir le montrer.

Le jury ne peut se contenter d\'un Vandermonde ou d\'un déterminant circulant ! Le résultant et les applications simples à l\'intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon. D\'une manière générale on attend pendant le développement l\'illustration d\'un calcul ou la manipulation de déterminants non triviaux.

Il serait bien que la continuité du déterminant trouve une application, ainsi que son caractère polynomial.
',
                'options' => 15,
            ),
            465 => 
            array (
                'id' => 466,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:58:05',
                'annee' => 2014,
                'numero' => 153,
                'type_id' => 1,
                'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie.',
                'rapport' => 'Les polynômes d\'un endomorphisme ne sont pas tous nuls ! Il faut consacrer une courte partie de la leçon à l\'algèbre $K[u]$, connaître sa dimension sans hésiter. Les propriétés globales pourront être étudiées par les meilleurs. Le jury souhaiterait voir certains liens entre réduction et structure de l\'algèbre $K[u]$. Le candidat peut s\'interroger sur les idempotents et le lien avec la décomposition en somme de sous-espaces caractéristiques.

Le jury ne souhaite pas que le candidat présente un catalogue de résultats autour de la réduction, mais seulement ce qui a trait aux polynômes d\'endomorphismes. Il faut bien préciser que dans la réduction de Dunford, les composantes sont des polynômes en l\'endomorphisme. 

L\'aspect applications est trop souvent négligé. On attend d\'un candidat qu\'il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est souhaitable que les candidats ne fassent pas la confusion entre diverses notions de multiplicité pour une valeur propre $\\lambda$ donnée ( algébrique ou géométrique). Enfin rappelons que pour calculer $A_k$ , il n\'est pas nécessaire en général de faire la réduction de $A$ (la donnée d\'un polynôme annulateur de $A$ suffit bien souvent).
',
                'options' => 15,
            ),
            466 => 
            array (
                'id' => 467,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:58:18',
                'annee' => 2014,
                'numero' => 154,
                'type_id' => 1,
                'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                'rapport' => 'Les candidats doivent s\'être interrogés sur les propriétés de l\'ensemble des sous-espaces stables par un endomorphisme. Des études détaillées de cas sont les bienvenues. La décomposition de Frobenius trouve tout à fait sa place dans la leçon. Notons qu\'il a été ajouté la notion de familles d\'endomorphismes. Ceci peut déboucher par exemple sur des endomorphismes commutant entre eux ou sur la théorie des représentations.
',
                'options' => 7,
            ),
            467 => 
            array (
                'id' => 468,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:58:30',
                'annee' => 2014,
                'numero' => 155,
                'type_id' => 1,
                'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => 'Il faut pouvoir donner des exemples naturels d\'endomorphismes diagonalisables et des critères. Le calcul de l\'exponentielle d\'un endomorphisme diagonalisable est immédiat une fois que l\'on connaît les valeurs propres et ceci sans diagonaliser la matrice, par exemple à l\'aide des projecteurs spectraux. 

On peut sur le corps des réels et des complexes donner des propriétés topologiques, et sur les corps finis, dénombrer les endomorphismes diagonalisables, ou possédant des propriétés données, liées à la diagonalisation.

Mentionnons que l\'affirmation "l\'ensemble des matrices diagonalisables de $M_n(K)$ est dense dans $M_n(K)$" nécessite quelques précisions sur le corps $K$ et la topologie choisie pour $M_n(K)$.
',
                'options' => 7,
            ),
            468 => 
            array (
                'id' => 469,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:58:43',
                'annee' => 2014,
                'numero' => 156,
                'type_id' => 1,
                'nom' => 'Exponentielle de matrices. Applications.',
            'rapport' => 'C\'est une leçon difficile et ce n\'est pas une leçon d\'analyse. Il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. Les questions de surjectivité ou d\'injectivité doivent être abordées. Par exemple la matrice $A = \\begin{pmatrix} -1 \\esperluette 1 \\\\ 0 \\esperluette -1 \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(2, R))$ ? La matrice blocs $B = \\begin{pmatrix} A \\esperluette 0 \\\\ 0 \\esperluette A \\end{pmatrix}$ est-elle dans l\'image $exp(Mat(4, R))$ ?

La décomposition de Dunford multiplicative (décomposition de Jordan) de $exp(A)$ doit être connue. Les groupes à un paramètre peuvent trouver leur place dans cette leçon. On peut s\'interroger si ces sous-groupes constituent des sous-variétés fermées de $GL(n, R)$. L\'étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités. 

Les applications aux équations différentielles doivent être évoquées sans constituer l\'essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique, mais le jury déconseille aux candidats de proposer ce thème dans un développement. 

Les notions d\'algèbres de Lie ne sont pas au programme de l\'agrégation, on conseille de n\'aborder ces sujets qu\'à condition d\'avoir une certaine solidité. Sans aller si loin, on pourra donner une application de l\'exponentielle à la décomposition polaire de certains sous-groupes fermés de $GL_n$ (groupes orthogonaux par exemple).    ',
                'options' => 7,
            ),
            469 => 
            array (
                'id' => 470,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:58:55',
                'annee' => 2014,
                'numero' => 157,
                'type_id' => 1,
                'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
            'rapport' => 'Il est possible de mener une leçon de bon niveau, même sans la décomposition de Jordan à l\'aide des noyaux itérés. On doit savoir déterminer si deux matrices nilpotentes sont semblables grâce aux noyaux itérés (ou grâce à la décomposition de Jordan si celle-ci est maîtrisée). 

Notons que l\'étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et l\'étude sur un corps fini donne lieu à de jolis problèmes de dénombrement.
',
                'options' => 15,
            ),
            470 => 
            array (
                'id' => 471,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:59:05',
                'annee' => 2014,
                'numero' => 158,
                'type_id' => 1,
                'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                'rapport' => 'C\'est une leçon transversale. La notion de signature doit figurer dans la leçon et on ne doit surtout pas se cantonner au cas des matrices définies positives. Curieusement, il est fréquent que le candidat énonce l\'existence de la signature d\'une matrice symétrique réelle sans en énoncer l\'unicité dans sa classe de congruence. On doit faire le lien avec les formes quadratiques et les formes hermitiennes. La partie réelle et la partie imaginaire d\'un produit hermitien définissent des structures sur l\'espace vectoriel réel sous-jacent.
',
                'options' => 7,
            ),
            471 => 
            array (
                'id' => 472,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:59:21',
                'annee' => 2014,
                'numero' => 159,
                'type_id' => 1,
                'nom' => 'Formes linéaires et dualité en dimension finie. Exemples et applications.',
            'rapport' => 'Il est important de bien placer la thématique de la dualité dans cette leçon : celle-ci permet de créer une correspondance féconde entre un morphisme et son morphisme transposé, un sous-espace et son orthogonal (canonique), les noyaux et les images, les sommes et les intersections. Bon nombre de résultats d\'algèbre linéaire se voient dédoublés par cette correspondance. 

Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d\'une intersection d\'hyperplans est important dans cette leçon. L\'utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d\'obtenir les équations d\'un sous-espace vectoriel ou d\'exhiber une base d\'une intersection d\'hyperplans. Cette leçon peut être traitée sous différents aspects : géométrique, algèbrique, topologique, analytique, etc. Il faut que les développements proposés soient en lien direct, comme toujours, avec la leçon ; proposer la trigonalisation simultanée est un peu osé ! Enfin rappeler que la différentielle d\'une fonction réelle est une forme linéaire semble incontournable.
',
                'options' => 15,
            ),
            472 => 
            array (
                'id' => 473,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:59:32',
                'annee' => 2014,
                'numero' => 160,
                'type_id' => 1,
            'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                'rapport' => 'Les candidats doivent bien prendre conscience que le caractère euclidien de l\'espace est essentiel pour que l\'endomorphisme soit remarquable. Par exemple, des développements comme le lemme des noyaux ou la décomposition de Dunford n\'ont rien à faire ici. En revanche, l\'utilisation du fait que l\'orthogonal d\'un sous-espace stable par un endomorphisme est stable par l\'adjoint doit être mis en valeur.
',
                'options' => 7,
            ),
            473 => 
            array (
                'id' => 474,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:59:43',
                'annee' => 2014,
                'numero' => 161,
                'type_id' => 1,
                'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Applications en dimensions $2$ et $3$.',
                'rapport' => 'La classification des isométries en dimension 2 est exigible. En dimension 3, les rotations et les liens avec la réduction. On peut penser aux applications aux isométries laissant stables certaines figures en dimension 2 et 3.
',
                'options' => 7,
            ),
            474 => 
            array (
                'id' => 475,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 11:59:54',
                'annee' => 2014,
                'numero' => 162,
                'type_id' => 1,
                'nom' => 'Systèmes d\'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                'rapport' => 'Le jury n\'attend pas une version à l\'ancienne articulée autour du théorème de Rouché-Fontené, qui n\'est pas d\'un grand intérêt dans sa version traditionnellement exposée.

La leçon doit impérativement présenter la notion de système échelonné, avec une définition précise et correcte et situer l\'ensemble dans le contexte de l\'algèbre linéaire (sans oublier la dualité !). Par exemple les relations de dépendances linéaires sur les colonnes d\'une matrice échelonnée sont claires et permettent de décrire simplement les orbites de l\'action à gauche de $GL(n,K)$ sur $M_n(K)$ donnée par $(P,A) \\rightarrow PA$. 
Le candidat doit pouvoir écrire un système d\'équations de l\'espace vectoriel engendré par les colonnes.

Un point de vue opératoire doit accompagner l\'étude théorique et l\'intérêt pratique (algorithmique) des méthodes présentées doit être expliqué y compris sur des exemples simples où l\'on attend parfois une résolution explicite.
',
                'options' => 15,
            ),
            475 => 
            array (
                'id' => 476,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:00:06',
                'annee' => 2014,
                'numero' => 170,
                'type_id' => 1,
                'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
            'rapport' => 'Le candidat ne doit pas se contenter de travailler sur $R$. Il faut savoir que les formes quadratiques existent sur le corps des complexes et sur les corps finis et savoir les classifier. On ne doit pas négliger l\'interprétation géométrique des notions introduites (lien entre coniques, formes quadratiques, cônes isotropes) ou les aspects élémentaires (par exemple le discriminant de l\'équation $ax^2 + bx + c = 0$ et la signature de la forme quadratique $ax^2 + bxy + cy^2$). On ne peut se limiter à des considérations élémentaires d\'algèbre linéaire. Les formes quadratiques ne sont pas toutes non dégénérées (la notion de quotient est utile pour s\'y ramener).

L\'algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique de $R^3$ . Le lien avec la signature doit être clairement énoncé. Malheureusement la notion d\'isotropie est mal maîtrisée par les candidats, y compris les meilleurs d\'entre eux. Le cône isotrope est un aspect important de cette leçon, qu\'il faut rattacher à la géométrie différentielle. Il est important d\'illustrer cette leçon d\'exemples naturels.
',
                'options' => 15,
            ),
            476 => 
            array (
                'id' => 477,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:00:16',
                'annee' => 2014,
                'numero' => 171,
                'type_id' => 1,
                'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                'rapport' => 'La preuve de la loi d\'inertie de Silvester doit être connue ainsi que l\'orthogonalisation simultanée. Le candidat doit avoir compris la signification géométrique de ces deux entiers composant la signature d\'une forme quadratique réelle ainsi que leur caractère classifiant. La différentielle seconde d\'une fonction de plusieurs variables est une forme quadratique importante. 

Pour les candidats de bon niveau, l\'indicatrice de Schur-Frobenius, sur la possibilité de réaliser une représentation donnée sur le corps des réels, permet un belle incursion de la théorie des représentations dans cette leçon.
',
                'options' => 7,
            ),
            477 => 
            array (
                'id' => 478,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:00:30',
                'annee' => 2014,
                'numero' => 180,
                'type_id' => 1,
                'nom' => 'Coniques. Applications.',
                'rapport' => 'La définition des coniques affines non dégénérées doit être connue. Les propriétés classiques des coniques doivent être présentées. Bien distinguer les notions affines, métriques ou projectives, la classification des coniques étant sensiblement différente selon le cas.

On peut se situer sur un autre corps que celui des réels. Le lien entre classification des coniques et classification des formes quadratiques peut être établi à des fins utiles.
',
                'options' => 7,
            ),
            478 => 
            array (
                'id' => 479,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:00:42',
                'annee' => 2014,
                'numero' => 181,
                'type_id' => 1,
                'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
            'rapport' => 'On attend des candidats qu\'ils parlent de coordonnées barycentriques et les utilisent par exemple dans le triangle (coordonnées barycentriques de certains points remarquables). Il est judicieux de parler d\'enveloppe convexe, de points extrémaux, ainsi que des applications qui en résultent.
',
                'options' => 15,
            ),
            479 => 
            array (
                'id' => 480,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:00:54',
                'annee' => 2014,
                'numero' => 182,
                'type_id' => 1,
                'nom' => 'Applications des nombres complexes à la géométrie. Homographies.',
                'rapport' => 'Cette leçon ne saurait rester au niveau de la Terminale. Une étude de l\'exponentielle complexe et des homographies de la sphère de Riemann est tout à fait appropriée. La réalisation du groupe $SU_2$ dans le corps des quaternions et ses applications peuvent trouver sa place dans la leçon.
',
                'options' => 15,
            ),
            480 => 
            array (
                'id' => 481,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:01:07',
                'annee' => 2014,
                'numero' => 183,
                'type_id' => 1,
                'nom' => 'Utilisation des groupes en géométrie.',
            'rapport' => 'C\'est une leçon transversale et difficile, qui peut aborder des aspects variés selon les structures algébriques présentes. D\'une part un groupe de transformations permet de ramener un problème de géométrie à un problème plus simple. D\'autre part, les actions de groupes sur la géométrie permettent de dégager des invariants essentiels (angle, birapport). On retrouvera encore avec bonheur les groupes d\'isométries d\'un solide.
',
                'options' => 15,
            ),
            481 => 
            array (
                'id' => 482,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-21 12:01:18',
                'annee' => 2014,
                'numero' => 190,
                'type_id' => 1,
                'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
            'rapport' => 'Il faut dans un premier temps dégager clairement les méthodes et les illustrer d\'exemples significatifs. L\'utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. Le jury s\'attend à ce que les candidats sachent calculer des cardinaux classiques et certaines probabilités ! L\'introduction des corps finis (même en se limitant aux cardinaux premiers) permet de créer un lien fécond avec l\'algèbre linéaire.
',
                'options' => 15,
            ),
            482 => 
            array (
                'id' => 483,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:16:17',
                'annee' => 2014,
                'numero' => 201,
                'type_id' => 2,
                'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => 'C\'est une leçon riche où le candidat devra choisir soigneusement le niveau auquel il souhaite se placer. Les espaces de fonctions continues sur un compact et les espaces de fonctions holomorphes offrent des possibilités de leçons de qualité avec des résultats intéressants. Il est regrettable de voir des candidats, qui auraient eu intérêt à se concentrer sur les bases de la convergence uniforme, proposer en développement le théorème de Riesz-Fisher dont il ne maîtrise visiblement pas la démonstration. Pour les candidats solides, les espaces Lp offrent de belles possibilités. 

Signalons que des candidats proposent assez régulièrement une version incorrecte du théorème de Müntz pour les fonctions continues. La version correcte dans ce cadre est  $$\\overline{Vect\\{1, x^{\\lambda_n} \\}} = \\mathcal{C}([0;1]; R) \\Leftrightarrow \\sum_{n \\geq 1} \\frac{1}{\\lambda_n} = + \\infty$$
Des candidats aguerris peuvent développer la construction et les propriétés de l\'espace de Sobolev $H_0^1(]0,1[)$, ses propriétés d\'injection dans les fonctions continues, et évoquer le rôle de cet espace dans l\'étude de problèmes aux limites elliptiques en une dimension. Ce développement conduit naturellement à une illustration de la théorie spectrale des opérateurs compacts auto-adjoints.  
',
                'options' => 7,
            ),
            483 => 
            array (
                'id' => 484,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:16:30',
                'annee' => 2014,
                'numero' => 202,
                'type_id' => 2,
                'nom' => 'Exemples de parties denses et applications.',
            'rapport' => 'Cette leçon permet d\'explorer les questions d\'approximations de fonctions par des polynômes et des polynômes trigonométriques. Au delà des exemples classiques, les candidats plus ambitieux peuvent aller jusqu\'à la résolution d\'équations aux dérivées partielles (ondes, chaleur, Schrödinger) par séries de Fourier.  
',
                'options' => 7,
            ),
            484 => 
            array (
                'id' => 485,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:16:47',
                'annee' => 2014,
                'numero' => 203,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de compacité.',
            'rapport' => 'Il est important de ne pas concentrer la leçon sur la compacité générale (confusion générale entre utilisation de la notion compacité et notion de compacité ), sans proposer des exemples significatifs d\'utilisation (Stone-Weierstrass, point fixe, voire étude qualitative d\'équations différentielles, etc.). La leçon peut être aussi avantageusement illustrée par des exemples d\'opérateurs à noyau et l\'analyse de leur compacité par le théorème d\'Ascoli, par exemple. Le rôle de la compacité pour des problèmes d\'existence d\'extrema mériterait d\'être davantage étudié (lien avec la coercivité en dimension finie). 

Pour les candidats solides, les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d\'utilisation de la compacité. Les opérateurs autoadjoint compacts sur l\'espace de Hilbert relèvent également de cette leçon, et on pourra développer par exemple leurs propriétés spectrales.  
',
                'options' => 15,
            ),
            485 => 
            array (
                'id' => 486,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:17:00',
                'annee' => 2014,
                'numero' => 204,
                'type_id' => 2,
                'nom' => 'Connexité. Exemples et applications.',
                'rapport' => 'Il est important de présenter des résultats naturels dont la démonstration utilise la connexité ; par exemple, diverses démonstrations du théorème de d\'Alembert-Gauss. On distinguera bien connexité et connexité par arcs, mais il est pertinent de présenter des situations où ces deux notions coïncident.  
',
                'options' => 7,
            ),
            486 => 
            array (
                'id' => 487,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:17:14',
                'annee' => 2014,
                'numero' => 205,
                'type_id' => 2,
                'nom' => 'Espaces complets. Exemples et applications.',
                'rapport' => 'Les candidats devraient faire apparaître que l\'un des intérêts essentiel de la complétude est de fournir des théorèmes d\'existence en dimension infinie, en particulier dans les espaces de fonctions. Rappelons que l\'on attend des candidats une bonne maîtrise de la convergence uniforme. Le théorème de CauchyLipschitz, mal maîtrisé par beaucoup de candidats, est un point important de cette leçon. 

Les espaces $L_p$ sont des exemples pertinents qui ne sont pas sans danger pour des candidats aux connaissances fragiles. 

Le théorème de Baire trouve naturellement sa place dans cette leçon, mais il faut l\'accompagner d\'applications. Rappelons que celles-ci ne se limitent pas aux théorèmes de Banach-Steinhaus et du graphe fermé, mais qu\'on peut évoquer au niveau de l\'agrégation l\'existence de divers objets : fonctions continues nulle part dérivables, points de continuité pour les limites simples de suites de fonctions continues, vecteurs à orbite dense pour certains opérateurs linéaires, etc. Les candidats prendront toutefois garde à ne pas présenter des applications de ce théorème au dessus de leur force.  
',
                'options' => 7,
            ),
            487 => 
            array (
                'id' => 488,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:17:40',
                'annee' => 2014,
                'numero' => 206,
                'type_id' => 2,
                'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                'rapport' => 'Les applications aux équations différentielles sont importantes. Répétons que la maîtrise du théorème de Cauchy-Lipschitz est attendue. Il faut préparer des contre-exemples pour illustrer la nécessité des hypothèses. 

Pour l\'analyse de convergence des méthodes de point fixe, les candidats ne font pas suffisamment le lien entre le caractère localement contractant de l\'opérateur itéré et la valeur de la différentielle au point fixe. La méthode de Newton, interprétée comme une méthode de point fixe, fournit un exemple où cette différentielle est nulle, la vitesse de convergence étant alors de type quadratique. L\'étude de méthodes itératives de résolution de systèmes linéraires conduit à relier ce caractère contractant à la notion de rayon spectral. 

Il est envisageable d\'admettre le théorème de point fixe de Brouwer et d\'en développer quelques conséquences comme le théorème de Perron-Froebenius.  
',
                'options' => 15,
            ),
            488 => 
            array (
                'id' => 489,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:17:56',
                'annee' => 2014,
                'numero' => 207,
                'type_id' => 2,
                'nom' => 'Prolongement de fonctions. Exemples et applications.',
                'rapport' => 'Les candidats exploitent rarement toutes les potentialités de cette leçon très riche. 

Le jury se réjouirait aussi que les candidats abordent les notions de solution maximale pour les équations différentielles ordinaires et maîtrisent le théorème de sortie des compacts. 

Le prolongement analytique relève bien-sûr de cette leçon ainsi que le prolongement de fonctions $\\mathcal{C}^\\infty$ sur un segment en fonctions de la même classe, le théorème de Tietze sur l\'extension des fonctions continues définies sur un sous-ensemble fermé d\'un espace métrique, la transformation de Fourier sur $L^2$ et l\'extension des fonctions Lipschitziennes définies sur un sous-ensemble (pas nécesairement dense) d\'un espace métrique. 

En ce qui concerne le théorème d\'Hahn-Banach, le candidat n\'en donnera la version en dimension infinie que s\'il peut s\'aventurer sans dommage sur le terrain très souvent mal maîtrisé du lemme de Zorn. Il vaut mieux disposer d\'applications pertinentes autre que des résutats classiques abstraits sur les duaux topologiques.  
',
                'options' => 7,
            ),
            489 => 
            array (
                'id' => 490,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:18:11',
                'annee' => 2014,
                'numero' => 208,
                'type_id' => 2,
                'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                'rapport' => 'La justification de la compacité de la boule unité en dimension finie doit être donnée. Le théorème d\'équivalence des normes en dimension finie, ou le caractère fermé de tout sous-espace de dimension finie d\'un espace normé, sont des résultats fondamentaux à propos desquels les candidats doivent se garder des cercles vicieux. 

Une telle leçon doit bien-sûr contenir beaucoup d\'illustrations et d\'exemples. Lors du choix de ceux-ci (le jury n\'attend pas une liste encyclopédique), le candidat veillera à ne pas mentionner des exemples sans avoir aucune idée de leur étude et à ne pas se lancer dans des développements trop sophistiqués. 

L\'analyse des constantes de stabilité pour l\'interpolation de Lagrange fournit un exemple non trivial peu présenté. 

Pour des candidats aguerris, la formulation variationnelle de problèmes elliptiques mono-dimensionnels peut donner lieu à des approfondissements intéressants.  
',
                'options' => 15,
            ),
            490 => 
            array (
                'id' => 491,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:18:23',
                'annee' => 2014,
                'numero' => 209,
                'type_id' => 2,
                'nom' => 'Approximation d\'une fonction par des polynômes et des polynômes trigonométriques. Exemples et applications.',
            'rapport' => 'Cette leçon comporte un certain nombre de classiques comme le théorème de Stone-Weierstrass. Comme la leçon 202, elle permet d\'explorer aux candidats plus ambitieux d\'aller jusqu\'à la résolution d\'équations aux dérivées partielles (ondes, chaleur, Schrödinger) par séries de Fourier.  
',
                'options' => 7,
            ),
            491 => 
            array (
                'id' => 492,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:18:37',
                'annee' => 2014,
                'numero' => 213,
                'type_id' => 2,
                'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
            'rapport' => 'Il est important de faire la différence entre base algébrique et base hilbertienne. De plus, la formule de la projection orthogonale sur un sous espace de dimension finie d\'un espace de Hilbert doit absolument être connue. Il faut connaître quelques critères simples pour qu\'une famille orthogonale forme une base hilbertienne et illustrer la leçon par des exemples de bases hilbertiennes (polynômes orthogonaux, séries de Fourier, etc.). Le théorème de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d\'un espace de Hilbert H est régulièrement mentionné. Les candidats doivent s\'intéresser au sens des formules $x = \\sum_{n \\geq 0} (x|e_n)e_n$ et $||x||^2 = \\sum_{n \\geq 0} (x|e_n)^2$ en précisant les hypothèses sur la famille $(e_n)$ et en justifiant la convergence.  

La notion d\'adjoint d\'un opérateur continu peut illustrer agréablement cette leçon. 

Pour des candidats solides, le programme permet d\'aborder la résolution, et l\'approximation, de problèmes aux limites en dimension 1 par des arguments exploitant la formulation variationnelle de ces équations. Plus généralement, l\'optimisation de fonctionnelles convexes sur les espaces de Hilbert devrait être plus souvent explorée.

Enfin, pour les plus valeureux, le théorème spectral pour les opérateurs autoadjoints compacts peut être abordé.  
',
                'options' => 7,
            ),
            492 => 
            array (
                'id' => 493,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:18:50',
                'annee' => 2014,
                'numero' => 214,
                'type_id' => 2,
                'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
            'rapport' => 'Il s\'agit d\'une belle leçon qui exige une bonne maîtrise du calcul différentiel. Même si le candidat ne propose pas ces thèmes en développement, on est en droit d\'attendre de lui des idées de démonstration de ces deux théorèmes fondamentaux. On attend des applications en géométrie différentielle (notam- ment dans la formulation des multiplicateurs de Lagrange). Rappelons que les sous-variétés sont au programme.  
',
                'options' => 15,
            ),
            493 => 
            array (
                'id' => 494,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:19:02',
                'annee' => 2014,
                'numero' => 215,
                'type_id' => 2,
                'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                'rapport' => 'Cette leçon requiert une bonne maîtrise de la notion de différentielle première et de son lien avec les dérivés partielles. Le théorème de différentiation composée doit être connu et pouvoir être appliqué dans des cas simples comme le calcul de la différentielle de l\'application $x \\rightarrow ||x||^2$ pour la norme euclidienne sur $R^n$ . 

La notion de différentielle seconde est attendue au moins pour les fonctions de classe $\\mathcal{C}^2$ ainsi que les applications classiques quant à l\'existence d\'extrema locaux.  
',
                'options' => 15,
            ),
            494 => 
            array (
                'id' => 495,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:19:13',
                'annee' => 2014,
                'numero' => 217,
                'type_id' => 2,
                'nom' => 'Sous-variétés de $R^n$. Exemples.',
            'rapport' => 'Cette leçon n\'a pas eu beaucoup de succès, c\'est bien dommage. Elle ne saurait être réduite à un cours de géométrie différentielle abstraite ; ce serait un contresens. Le jury attend une leçon concrète, montrant une compréhension géométrique locale. Aucune notion globale n\'est exigible, ni de notion de variété abstraite. Le candidat doit pouvoir être capable de donner plusieurs représentations locales (paramétriques, équations, etc.) et d\'illustrer la notion d\'espace tangent sur des exemples classiques. Le jury invite les candidats à réfléchir à la pertinence de l\'introduction de la notion de sous-variétés. 

Le théorème des extremas liés devient assez transparent lorsqu\'on le traite par les sous-variétés. Les groupes classiques donnent des exemples utiles de sous-variétés.  
',
                'options' => 7,
            ),
            495 => 
            array (
                'id' => 496,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:19:25',
                'annee' => 2014,
                'numero' => 218,
                'type_id' => 2,
                'nom' => 'Applications des formules de Taylor.',
                'rapport' => 'Il faut connaître les formules de Taylor des polynômes et certains développements très classiques. En général, le développement de Taylor d\'une fonction comprend un terme de reste qu\'il est crucial de savoir analyser. Le jury s\'inquiète des trop nombreux candidats qui ne savent pas expliquer clairement ce que signifient les notations $o$ ou $O$ qu\'ils utilisent. 

Il y a de très nombreuses applications en géométrie et probabilités (le théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités $||f^{(k)} || \\leq 2^{\\frac{k(n-k)}{2}} ||f||^{1-k/n} ||f^{(n)}||^{k/n}$ (lorsque f et sa dérivée n-ème sont bornées). On soignera particulièrement le choix des développements.  
',
                'options' => 15,
            ),
            496 => 
            array (
                'id' => 497,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:19:38',
                'annee' => 2014,
                'numero' => 219,
                'type_id' => 2,
                'nom' => 'Extremums : existence, caractérisation, recherche. Exemples et applications.',
            'rapport' => 'Cette leçon a changé de titre. Il faut bien faire la distinction entre propriétés locales (caractérisation d\'un extremum) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local est également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon. 

L\'étude des algorithmes de recherche d\'extremas y a maintenant toute sa place : méthode de gradient, preuve de la convergence de la méthode de gradient à pas optimal, etc. Le cas particulier des fonctionnelles sur $R^n$ de la forme $\\frac{1}{2} (Ax|x) - (b|x)$, où $A$ est une matrice symétrique définie positive, devrait être totalement maîtrisé. Les candidats devraient aussi être amenés à évoquer les problèmes de type moindres carrés et les équations normales qui y sont attachés. Enfin, les problèmes de minimisation sous contrainte amènent à faire le lien avec les extrema liés, la notion de multiplicateur de Lagrange et, là encore des algorithmes peuvent être présentés et analysés.  
',
                'options' => 15,
            ),
            497 => 
            array (
                'id' => 498,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:19:53',
                'annee' => 2014,
                'numero' => 220,
                'type_id' => 2,
            'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'études des solutions en dimension $1$ et $2$.',
                'rapport' => 'C\'est l\'occasion de rappeler une nouvelle fois que le jury s\'alarme des nombreux défauts de maîtrise du théorème de Cauchy-Lipschitz. La notion même de solution maximale d\'un problème de Cauchy est trop souvent mal comprise. Il est regrettable de voir des candidats ne connaître qu\'un énoncé pour les fonctions globalement lipschitziennes ou plus grave, mélanger les conditions sur la variables de temps et d\'espace. La notion de solution maximale et le théorème de sorties de tout compact sont nécessaires. 

Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est curieusement rarement énoncé. L\'utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en oeuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées. 

Pour les équations autonomes, la notion de point d\'équilibre permet des illustrations de bon goût comme par exemple les petites oscillations du pendule. Trop peu de candidats pensent à tracer et discuter des portraits de phase. 

Enfin, il n\'est pas malvenu d\'évoquer les problèmatiques de l\'approximation numérique dans cette leçon par exemple autour de la notion de problèmes raides et de la conception de schémas implicites pour autant que la candidat ait une matrîse convenable de ces questions.  
',
                'options' => 15,
            ),
            498 => 
            array (
                'id' => 499,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:20:05',
                'annee' => 2014,
                'numero' => 221,
                'type_id' => 2,
                'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
            'rapport' => 'Exemples et applications. On attend d\'un candidat qu\'il sache déterminer rigoureusement la dimension de l\'espace vectoriel des solutions (dans le cas de la dimension finie bien-sûr).

Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. L\'utilisation des exponentielles de matrices doit pouvoir s\'expliquer. Dans le cas général, certains candidats évoquent les généralisations de l\'exponentielle (résolvante) via les intégrales itérées. Les problèmatiques de stabilité des solutions et le lien avec l\'analyse spectrale devrait être exploitées.  
',
                'options' => 15,
            ),
            499 => 
            array (
                'id' => 500,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:20:20',
                'annee' => 2014,
                'numero' => 222,
                'type_id' => 2,
                'nom' => 'Exemples d\'équations aux dérivées partielles linéaires.',
                'rapport' => 'Cette nouvelle leçon peut être abordée en faisant appel à des techniques variées et de nombreux développements pertinents peuvent être construits en exploitant judicieusement les éléments les plus classiques du programme. Les techniques d\'équations différentielles s\'expriment par exemple pour traiter $\\lambda u - u\'\'$ avec des conditions de Dirichlet en $x = 0, x = 1$ ou pour analyser l\'équation de transport par la méthode des caractéristiques. 

Les séries de Fourier trouvent dans cette leçon une mise en pratique toute désignée pour résoudre l\'équation de la chaleur, de Schrödinger ou des ondes dans le contexte des fonctions périodiques. La transformée de Fourier permet ceci dans le cadre des fonctions sur $R^d$. 

Le point de vue de l\'approximation numérique donne lieu à des développements originaux, notamment autour de la matrice du laplacien et de l\'analyse de convergence de la méthode des différences finies. 

Des développements plus sophistiqués se placeront sur le terrain de l\'analyse hilbertienne avec le théorème de Lax-Milgram, l\'espace de Sobolev $H_0^1(]0,1[)$, jusqu\'à la décomposition spectrale des opérateurs compacts, ou encore sur celui des distributions avec l\'étude de solutions élémentaires d\'équations elliptiques.  
',
                'options' => 7,
            ),
        ));
        \DB::table('lecons')->insert(array (
            0 => 
            array (
                'id' => 501,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 223,
                'type_id' => 2,
                'nom' => 'Suites numériques. Convergence, valeurs d\'adhérence. Exemples et applications.',
                'rapport' => '',
                'options' => 15,
            ),
            1 => 
            array (
                'id' => 502,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 224,
                'type_id' => 2,
                'nom' => 'Exemples de développements asymptotiques de suites et de fonctions.',
                'rapport' => '',
                'options' => 15,
            ),
            2 => 
            array (
                'id' => 503,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:24:36',
                'annee' => 2014,
                'numero' => 226,
                'type_id' => 2,
            'nom' => 'Suites vectorielles et réelles définies par une relation de récurrence $u_{n+1} = f(u_n)$. Exemples et applications.',
            'rapport' => 'Exemples et applcations.  un`1 “ f pun q.  Le jury attend d\'autres exemples que la traditionnelle suite récurrente $u_{n+1} = \\sin(u_n)$. Les suites homographiques réelles ou complexes fournissent des exemples intéressants, rarement évoqués. 

Cette leçon doit être l\'occasion d\'évoquer les problématiques de convergence d\'algorithmes d\'approximation de solutions de problèmes linéaires et non linéaires : dichotomie, méthode de Newton, algorithme du gradient, méthode de la puissance, méthodes itératives de résolution de système linéaire, schéma d\'Euler ...
',
                'options' => 15,
            ),
            3 => 
            array (
                'id' => 504,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:24:50',
                'annee' => 2014,
                'numero' => 228,
                'type_id' => 2,
                'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
            'rapport' => 'applications. Un plan découpé en deux parties (I : Continuité, II : Dérivabilité) n\'est pas le mieux adapté. Les théorèmes de base doivent être maîtrisés et illustrés par des exemples intéressants. Les candidats doivent disposer d\'un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La dérivabilité presque partout des fonctions Lipschitziennes relève de cette leçon. Enfin les applications du théorème d\'Ascoli (par exemple les opérateurs intégraux à noyau continu, le théorème de Peano, etc ), sont les bienvenues.  
',
                'options' => 7,
            ),
            4 => 
            array (
                'id' => 505,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:25:03',
                'annee' => 2014,
                'numero' => 229,
                'type_id' => 2,
                'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
            'rapport' => 'Les candidats sont invités à réfléchir à l\'incidence de ces notions en théorie des probabilités. La dérivabilité presque partout des fonctions monotones est un résultat important. Il est souhaitable d\'illustrer la présentation de la convexité par des dessins clairs, même si ces dessins ne peuvent remplacer un calcul. On notera que la monotonie concerne (à ce niveau) les fonctions réelles d\'une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de $R^n$ , qui fournissent de beaux exemples d\'utilisation. L\'espace vectoriel engendré par les fonctions monotones (les fonctions à variation bornée) relève de cette leçon. 

Pour les candidats aguerris, la dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité et les candidats bien préparés peuvent s\'aventurer utilement dans cette direction.  
',
                'options' => 15,
            ),
            5 => 
            array (
                'id' => 506,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:25:17',
                'annee' => 2014,
                'numero' => 230,
                'type_id' => 2,
                'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
            'rapport' => 'De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être véritablement hors sujet, cette exposition ne doit pas former l\'essentiel du plan. Le thème central de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, etc...) et leurs applications diverses, comme par exemple des résultats d\'irrationalité, voire de transcendance. Enfin on rappelle que la transformation d\'Abel trouve toute sa place dans cette leçon.  
',
                'options' => 15,
            ),
            6 => 
            array (
                'id' => 507,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:25:40',
                'annee' => 2014,
                'numero' => 232,
                'type_id' => 2,
            'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                'rapport' => 'Trop de candidats se limitent au simple cas où $X$  est une variable scalaire. Il serait bon d\'envisager les extensions des méthodes classiques dans le cas vectoriel. Au delà de la méthode de Newton, d\'intéressants développements peuvent s\'intéresser à la résolution de systèmes linéaires, notamment par des méthodes itératives.  
',
                'options' => 15,
            ),
            7 => 
            array (
                'id' => 508,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:26:00',
                'annee' => 2014,
                'numero' => 233,
                'type_id' => 2,
                'nom' => 'Analyse numérique matricielle : résolution approchée de systèmes linéaires, recherche de vecteurs propres, exemples.',
            'rapport' => 'Cette leçon puise une bonne part de son contenu dans le programme complémentaire de l\'oral, commun aux différentes options. Les notions de norme matricielle et de rayon spectral sont bien sûr centrales pour ce sujet où le rôle du conditionnement dans l\'étude de sensibilité des solutions de systèmes linéaires doit être bien identifié. L\'analyse de convergence des méthodes itératives de résolution de systèmes linéaires, en identifiant leurs avantages par rapport aux méthodes directes, trouve naturellement sa place dans cette leçon, tout comme l\'étude d\'algorithmes de recherche déléments propres, avec la méthode de la puissance (ou la méthode $QR$) et des applications à des matrices vérifiant les hypothèses des théorèmes de Perron-Frobenius. Le cas particulier des matrices symétriques définies positives doit amener à faire le lien avec les problèmes de minimisation et les méthodes de gradient. On notera d\'ailleurs que de tels développements peuvent aussi être exploités avec bonheur dans la leçon 226. 

Les techniques d\'analyse permettent aussi l\'investigation des propriétés spectrales de matrices et la localisation de valeurs propres de matrices (théorème de Gershgörin, suites de Sturm). Le jury encourage les candidats à illustrer leur propos d\'exemples pertinents issus de la théorie de l\'interpolation ou de la résolution approchée de problèmes aux limites, incluant l\'analyse de stabilité de méthodes numériques.  
',
                'options' => 7,
            ),
            8 => 
            array (
                'id' => 509,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:26:13',
                'annee' => 2014,
                'numero' => 234,
                'type_id' => 2,
                'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
            'rapport' => 'Le jury a apprécié les candidats sachant montrer qu\'avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p \\subset L^q$ si $p \\geq q$). Il est important de pouvoir justifier l\'existence de produits de convolution (exemple $L^1 \\star L^1$). Par ailleurs, les espaces associés à la mesure de décompte sur $N$ ou $Z$ fournissent des exemples pertinents non triviaux.  
',
                'options' => 7,
            ),
            9 => 
            array (
                'id' => 510,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 235,
                'type_id' => 2,
                'nom' => 'Problèmes d\'interversion de limites et d\'intégrales.',
                'rapport' => '',
                'options' => 7,
            ),
            10 => 
            array (
                'id' => 511,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 236,
                'type_id' => 2,
                'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                'rapport' => '',
                'options' => 15,
            ),
            11 => 
            array (
                'id' => 512,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:26:53',
                'annee' => 2014,
                'numero' => 239,
                'type_id' => 2,
                'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
            'rapport' => 'Cette leçon doit être enrichie par des études et méthodes asymptotiques et les transformations classiques (Fourier, Laplace, etc.).  
',
                'options' => 15,
            ),
            12 => 
            array (
                'id' => 513,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:27:14',
                'annee' => 2014,
                'numero' => 240,
                'type_id' => 2,
                'nom' => 'Produit de convolution, transformation de Fourier. Applications.',
                'rapport' => 'Cette leçon ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C\'est bien une leçon d\'analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. Le lien régularité de la fonction et décroissance de sa transformé de Fourier doit être fait même sous des hypothèses qui ne sont pas minimales. 

La formule d\'inversion de Fourier pour une fonction $L^1$ dont la transformée de Fourier est aussi $L^1$ ainsi que les inégalités de Young sont attendues ainsi que l\'extension de la transformée de Fourier à l\'espace $L^2$ par Fourier-Plancherel. Des exemples explicites de calcul de transformations de Fourier paraissent nécessaires. 

Les candidats solides peuvent aborder ici la résolution de l\'équation de la chaleur, de Schrödinger pour des fonctions assez régulières, ou plus délicats la détermination des solutions élémentaires du Laplacien ou de l\'opérateur $k^2 - \\frac{d^2}{dx^2}$ . 

La transformation de Fourier des distributions tempérées ainsi que la convolution dans le cadre des distributions tempérées trouve sa place ici mais est réservé aux candidats aguerris. On peut aussi considérer l\'extension de la transformée de Fourier à la variable complexe, riche d\'applications par exemple dans la direction du théorème de Paley-Wiener.  
',
                'options' => 15,
            ),
            13 => 
            array (
                'id' => 514,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 241,
                'type_id' => 2,
                'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            14 => 
            array (
                'id' => 515,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:27:36',
                'annee' => 2014,
                'numero' => 243,
                'type_id' => 2,
                'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                'rapport' => 'Il est regrettable de voir beaucoup de candidats qui maîtrisent raisonnablement les classiques du comportement au bord du disque de convergence traiter cette leçon en faisant l\'impasse sur la variable complexe. C\'est se priver de beaux exemples d\'applications ainsi que du théorème de composition, pénible à faire dans le cadre purement analytique et d\'ailleurs très peu abordé.  
',
                'options' => 15,
            ),
            15 => 
            array (
                'id' => 516,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 244,
                'type_id' => 2,
                'nom' => 'Fonctions développables en série entière, fonctions analytiques. Exemples.',
                'rapport' => '',
                'options' => 7,
            ),
            16 => 
            array (
                'id' => 517,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:28:00',
                'annee' => 2014,
                'numero' => 245,
                'type_id' => 2,
                'nom' => 'Fonctions holomorphes sur un ouvert de $C$. Exemples et applications.',
            'rapport' => 'Le titre a changé. Les conditions de Cauchy-Riemann doivent être parfaitement connues et l\'interpréş tation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z)dz$ a un sens précis, qu\'il faut savoir expliquer. Par ailleurs, même si cela ne constitue pas le coeur de la leçon, il faut connaître la définition d\'une fonction méromorphe (l\'ensemble des pôles doit être une partie fermée discrète) !  
',
                'options' => 7,
            ),
            17 => 
            array (
                'id' => 518,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:28:15',
                'annee' => 2014,
                'numero' => 246,
                'type_id' => 2,
                'nom' => 'Séries de Fourier. Exemples et applications.',
            'rapport' => 'Les différents modes de convergence ($L^2$ , Fejer, Dirichlet etc...) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). Dans le cas d\'une fonction continue et $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet. 

Il est souhaitable que cette leçon ne se réduise pas à un cours abstrait sur les coefficients de Fourier. La résolution des équations de la chaleur, de Schrödinger et des ondes dans le cadre de fonctions assez régulières peuvent illustrer de manière pertinente cette leçon.  
',
                'options' => 15,
            ),
            18 => 
            array (
                'id' => 519,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:28:26',
                'annee' => 2014,
                'numero' => 249,
                'type_id' => 2,
                'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
            'rapport' => 'La notion d\'indépendance ainsi que les théorèmes de convergence (loi des grands nombres et théorème limite central) doivent être rappelés. 

Il peut être intéressant de donner une construction explicite d\'une suite de variables aléatoires de Bernoulli indépendantes. 

Certains candidats plus aguerris pourront s\'intéresser au comportement asymptotique de marches aléatoires (en utilisant par exemple le lemme de Borel-Cantelli), ou donner des inégalités de grandes déviations (comme l\'inégalité de Hoeffding).  
',
                'options' => 7,
            ),
            19 => 
            array (
                'id' => 520,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 253,
                'type_id' => 2,
                'nom' => 'Utilisation de la notion de convexité en analyse.',
                'rapport' => '',
                'options' => 7,
            ),
            20 => 
            array (
                'id' => 521,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:28:54',
                'annee' => 2014,
                'numero' => 254,
                'type_id' => 2,
            'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées. Transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
            'rapport' => 'Rappelons une fois de plus que les attentes du jury sur ces leçons restent modestes, et se placent au niveau de ce qu\'un cours de M1 standard sur le sujet peut contenir. Aucune subtilité topologique portant sur l\'espace des distributions tempérées n\'est attendue. Par contre, on attend du candidat qu\'il sache faire le lien entre décroissance de la transformée de Fourier et régularité de la fonction. Le fait que la transformée de Fourier envoie $S(R^d)$ dans lui même avec de bonnes estimations des semi normes doit être compris et la formule d\'inversion de Fourier maîtrisée dans ce cadre. 

Le passage à $S\'(R^d)$ repose sur l\'idée dualité qui est le coeur de cette leçon. Des exemples de calcul de transformée de Fourier peuvent être données, classiques comme la gaussienne ou $(1+x^2)^{-1}$ et d\'autres liées à la théorie des distributions comme la détermination de la transformée de Fourier d\'une constante. 

Les candidats ayant une bonne connaissance du sujet peuvent par exemple déterminer la transformée de Fourier de la valeur principale, la solution fondamentale du laplacien, voire résoudre l\'équation de la chaleur ou de Schrödinger.  
',
                'options' => 7,
            ),
            21 => 
            array (
                'id' => 522,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:29:40',
                'annee' => 2014,
                'numero' => 255,
                'type_id' => 2,
                'nom' => 'Espaces de Scwartz. Distributions. Dérivation au sens des distributions.',
                'rapport' => 'Ici aussi, les attentes du jury sur ces leçons restent modestes, et se placent au niveau de ce qu\'un cours de M1 standard sur le sujet peut contenir. Aucune subtilité topologique portant sur l\'espace des distributions tempérées n\'est attendue. 

Le passage à $S\'(R^d)$ repose sur l\'idée dualité qui est le coeur de cette leçon. La détermination de la dérivée de $\\log |x|$, de la dérivée seconde fournit un exemple pertinent tout comme la formule des sauts.  
',
                'options' => 7,
            ),
            22 => 
            array (
                'id' => 523,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:30:01',
                'annee' => 2014,
                'numero' => 260,
                'type_id' => 2,
                'nom' => 'Espérance, variance et moments d\'une variable aléatoire.',
            'rapport' => 'Le jury attend des candidats qu\'ils donnent la définition des moments centrés, qu\'ils rappellent les implications d\'existence de moments. Les inégalités classiques (de Markov, de Bienaymé-Chebichev, de Jensen et de Cauchy-Schwarz) pourront être données, ainsi que les théorèmes de convergence (loi des grands nombres et théorème limite central). 

Le comportement des moyennes de Cesàro pour une suite de variables aléatoires indépendantes et identiquement distribuées n\'admettant pas d\'espérance pourra être étudié. 

La notion de fonction génératrice des moments pourra être présentée.  
',
                'options' => 15,
            ),
            23 => 
            array (
                'id' => 524,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:30:12',
                'annee' => 2014,
                'numero' => 261,
                'type_id' => 2,
                'nom' => 'Fonction caractéristique et transformée de Laplace d\'une variable aléatoire. Exemples et applications.',
                'rapport' => 'Le jury attend l\'énoncé de théorème de Lévy et son utilisation dans la démonstration du théorème limite central. 

Les candidats pourront présenter l\'utilisation de la fonction caractéristique pour le calcul de lois de sommes de variables aléatoires indépendantes et faire le lien entre la régularité de la fonction caractéristique et l\'existence de moments. 

Enfin la transformée de Laplace pourra être utilisée pour établir des inégalités de grandes déviations.  
',
                'options' => 7,
            ),
            24 => 
            array (
                'id' => 525,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:30:22',
                'annee' => 2014,
                'numero' => 262,
                'type_id' => 2,
                'nom' => 'Modes de convergence d\'une suite de variables aléatoires. Exemples et applications.',
                'rapport' => 'Les implications entre les divers modes de convergence, ainsi que les réciproques partielles doivent être connus. Des contre-exemples aux réciproques sont attendus par le jury. 

Les théorèmes de convergence (lois des grands nombres et théorème limite central) doivent être énoncés. 

Les candidats plus aguerris pourront présenter le lemme de Slutsky (et son utilisation pour la construction d\'intervalles de confiance), ou bien certains théorèmes de convergence pour les martingales.  
',
                'options' => 7,
            ),
            25 => 
            array (
                'id' => 526,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:30:34',
                'annee' => 2014,
                'numero' => 263,
                'type_id' => 2,
                'nom' => 'Variables aléatoires à densité. Exemples et applications.',
                'rapport' => 'Le jury attend des candidats qu\'ils rappellent la définition d\'une variable aléatoire à densité et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. 

Le lien entre l\'indépendance et la convolution pourra être étudié. 

Les candidats pourront expliquer comment fabriquer n\'importe quelle variable aléatoire à partir d\'une variable uniforme sur $[0, 1]$ et l\'intérêt de ce résultat pour la simulation informatique. 

Pour aller plus loin, certains candidats pourront aborder la notion de vecteurs gaussiens et son lien avec le théorème limite central.  
',
                'options' => 7,
            ),
            26 => 
            array (
                'id' => 527,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:30:46',
                'annee' => 2014,
                'numero' => 264,
                'type_id' => 2,
                'nom' => 'Variables aléatoires discrètes. Exemples et applications.',
                'rapport' => 'Le jury attend des candidats qu\'ils rappellent la définition d\'une variable aléatoire discrète et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. 

Les techniques spécifiques aux variables discrètes devront être abordées (comme par exemple la caractérisation de la convergence en loi). La notion de fonction génératrice pourra être abordée. 

Pour aller plus loin, certains candidats pourront étudier les chaînes de Markov à espaces d\'états finis ou dénombrables. 
',
                'options' => 15,
            ),
            27 => 
            array (
                'id' => 528,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-04-22 10:31:12',
                'annee' => 2014,
                'numero' => 901,
                'type_id' => 3,
                'nom' => 'Structure de données : exemples et applications.',
                'rapport' => 'Le jury attend du candidat qu\'il traite des exemples d\'algorithmes récursifs et des exemples d\'algorithmes itératifs.

En particulier, le candidat doit présenter des exemples mettant en évidence l\'intérêt de la notion d\'invariant pour la correction partielle et celle de variant pour la terminaison des segments itératifs.

Une formalisation comme la logique de Hoare pourra utilement être introduite dans cette leçon, à condition toutefois que le candidat en maîtrise le langage.
',
                'options' => 8,
            ),
            28 => 
            array (
                'id' => 529,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:46:14',
                'annee' => 2014,
                'numero' => 902,
                'type_id' => 3,
                'nom' => 'Diviser pour régner : exemples et applications.',
                'rapport' => '
Cette leçon permet au candidat de proposer différents algorithmes utilisant le paradigme diviser pour régner . Le jury attend du candidat que ces exemples soient variés et touchent des domaines différents. 

Un calcul de complexité ne peut se limiter au cas où la taille du  problème est une puissance exacte de 2, ni à une application directe d\'un théorème très général recopié approximativement d\'un ouvrage de la bibliothèque de l\'agrégation.
',
                'options' => 8,
            ),
            29 => 
            array (
                'id' => 530,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 903,
                'type_id' => 3,
                'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                'rapport' => '',
                'options' => 8,
            ),
            30 => 
            array (
                'id' => 531,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:46:15',
                'annee' => 2014,
                'numero' => 906,
                'type_id' => 3,
                'nom' => 'Programmation dynamique : exemples et applications.',
                'rapport' => '
Même s\'il s\'agit d\'une leçon d\'exemples et d\'applications, le jury attend des candidats qu\'ils présentent les idées générales de la programmation dynamique et en particulier qu\'ils aient compris le caractère générique de la technique de mémoïsation. Le jury appréciera que les exemples choisis par le candidat couvrent des domaines variés, et ne se limitent pas au calcul de la longueur de la plus grande sous-
séquence commune à deux chaînes de caractères.

Le jury ne manquera pas d\'interroger plus particulièrement le candidat sur la question de la correction des algorithmes proposés et sur la question de leur complexité en espace.
',
                'options' => 8,
            ),
            31 => 
            array (
                'id' => 532,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:46:15',
                'annee' => 2014,
                'numero' => 907,
                'type_id' => 3,
                'nom' => 'Algorithmique du texte : exemples et applications.',
                'rapport' => '
Cette leçon devrait permettre au candidat de présenter une grande variété d\'algorithmes et de paradigmes de programmation, et ne devrait pas se limiter au seul problème de la recherche d\'un motif dans un texte, surtout si le candidat ne sait présenter que la méthode naïve.

De même, des structures de données plus riches que les tableaux de caractères peuvent montrer leur utilité dans certains algorithmes, qu\'il s\'agisse d\'automates ou d\'arbres par exemple.
',
                'options' => 8,
            ),
            32 => 
            array (
                'id' => 533,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:46:15',
                'annee' => 2014,
                'numero' => 909,
                'type_id' => 3,
                'nom' => 'Langages rationnels. Exemples et applications.',
                'rapport' => '
Des applications dans le domaine de la compilation entrent naturellement dans le cadre de ces leçons.
',
                'options' => 8,
            ),
            33 => 
            array (
                'id' => 534,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 910,
                'type_id' => 3,
                'nom' => 'Langages algébriques. Exemples et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            34 => 
            array (
                'id' => 535,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 912,
                'type_id' => 3,
                'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            35 => 
            array (
                'id' => 536,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 913,
                'type_id' => 3,
                'nom' => 'Machines de Turing. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            36 => 
            array (
                'id' => 537,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 914,
                'type_id' => 3,
                'nom' => 'Décidabilité et indécidabilité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            37 => 
            array (
                'id' => 538,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 915,
                'type_id' => 3,
                'nom' => 'Classes de complexité : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            38 => 
            array (
                'id' => 539,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 916,
                'type_id' => 3,
                'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                'rapport' => '',
                'options' => 8,
            ),
            39 => 
            array (
                'id' => 540,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:46:15',
                'annee' => 2014,
                'numero' => 917,
                'type_id' => 3,
                'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                'rapport' => '
La question de la syntaxe dépasse celle de la définition des termes et des formules. Elle comprend aussi celle des règles de la démonstration. Le jury attend donc du candidat qu\'il présente au moins un système de preuve et les liens entre syntaxe et sémantique, en développant en particulier les questions de correction et complétude.
',
                'options' => 8,
            ),
            40 => 
            array (
                'id' => 541,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 918,
                'type_id' => 3,
                'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            41 => 
            array (
                'id' => 542,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 919,
                'type_id' => 3,
                'nom' => 'Unification : algorithmes et applications.',
                'rapport' => '',
                'options' => 8,
            ),
            42 => 
            array (
                'id' => 543,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 920,
                'type_id' => 3,
                'nom' => 'Réécriture et formes normales. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            43 => 
            array (
                'id' => 544,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 921,
                'type_id' => 3,
                'nom' => 'Algorithmes de recherche et structures de données associées.',
                'rapport' => '',
                'options' => 8,
            ),
            44 => 
            array (
                'id' => 545,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 922,
                'type_id' => 3,
                'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            45 => 
            array (
                'id' => 546,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 923,
                'type_id' => 3,
                'nom' => 'Analyses lexicale et syntaxique : applications.',
                'rapport' => '',
                'options' => 8,
            ),
            46 => 
            array (
                'id' => 547,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 924,
                'type_id' => 3,
                'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            47 => 
            array (
                'id' => 548,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 925,
                'type_id' => 3,
                'nom' => 'Graphes : représentations et algorithmes.',
                'rapport' => '',
                'options' => 8,
            ),
            48 => 
            array (
                'id' => 549,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 926,
                'type_id' => 3,
                'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                'rapport' => '',
                'options' => 8,
            ),
            49 => 
            array (
                'id' => 550,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 927,
                'type_id' => 3,
                'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                'rapport' => '',
                'options' => 8,
            ),
            50 => 
            array (
                'id' => 551,
                'created_at' => '2016-03-14 12:36:59',
                'updated_at' => '2016-03-14 12:36:59',
                'annee' => 2014,
                'numero' => 928,
                'type_id' => 3,
                'nom' => 'Problème NP-complets : exemples de réductions.',
                'rapport' => '',
                'options' => 8,
            ),
            51 => 
            array (
                'id' => 552,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-16 08:02:48',
                'annee' => 2015,
                'numero' => 101,
                'type_id' => 1,
                'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
            'rapport' => 'Il faut bien dominer les deux approches de l\'action de groupe : l\'approche naturelle et l\'approche, plus subtile, via le morphisme qui relie le groupe agissant et le groupe des permutations de l\'ensemble sur lequel il agit. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fni, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des fonctions, voire des polynômes. Les exemples issus de la géométrie ne manquent pas (groupes d\'isométries d\'un solide). Certains candidats décrivent les actions naturelles de $PGL(  2, F_q)$ sur la droite projective qui donnent des injections intéressantes pour $q=2,3$ et peuvent plus généralement en petit cardinal donner lieu à des isomorphismes de groupes. Enfin, on pourra noter que l\'injection du groupe de permutations dans le groupe linéaire par les matrices de permutations donne lieu à des représentations dont il est facile de déterminer le caractère.
',
                'options' => 7,
            ),
            52 => 
            array (
                'id' => 553,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:45:15',
                'annee' => 2015,
                'numero' => 102,
                'type_id' => 1,
                'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
            'rapport' => 'Cette leçon est encore abordée de façon élémentaire sans réellement expliquer où et comment les nombres complexes de modules 1 et les racines de l\'unité apparaissent dans divers domaines des mathématiques (polynômes cyclotomiques, spectre de matrices remarquables, théorie des représentations). Il ne faut pas non plus oublier la partie "groupe" de la leçon : on pourra s\'intéresser au relèvement du groupe unité au groupe additif des réels et aux propriétés qui en résultent (par exemple l\'alternative  "sous-groupes denses versus sous-groupes monogènes"). On pourra aussi s\'intéresser aux groupes des nombres complexes de $Q[i]$, et les racines de l\'unité qui y appartiennent.
',
                'options' => 7,
            ),
            53 => 
            array (
                'id' => 554,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:45:39',
                'annee' => 2015,
                'numero' => 103,
                'type_id' => 1,
                'nom' => 'Exemples de sous-groupes distingués et de groupes quotients. Applications.',
            'rapport' => 'Les candidats parlent de groupe simple et de sous-groupe dérivé ou de groupe quotient sans savoir utiliser ces notions. Entre autres, il faut savoir pourquoi on s\'intéresse particulièrement aux groupes simples. La notion de produit semi-direct n\'est plus au programme, mais lorsqu\'elle est utilisée, il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral $D_n$ par exemple). On pourra noter que les tables de caractères permettent d\'illustrer toutes ces notions. Pour les candidats les plus téméraires, on pourra noter que le treillis des sous-groupes distingués d\'un groupe fini se voit dans sa table de caractères, ainsi que l\'indice du sous-groupe dérivé.
',
                'options' => 7,
            ),
            54 => 
            array (
                'id' => 555,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:45:57',
                'annee' => 2015,
                'numero' => 104,
                'type_id' => 1,
                'nom' => 'Groupes finis. Exemples et applications.',
            'rapport' => 'On attend des candidats de savoir manipuler correctement les éléments de quelques structures usuelles ($\\mathbb{Z}/n \\mathbb{Z}$ , $\\mathfrak{S}_n$, etc.). Par exemple, proposer un générateur simple de $\\mathbb{Z}/n\\mathbb{Z}$ voire tous les générateurs, calculer aisément un produit de deux permutations, savoir décomposer une permutation en produit de cycles à supports disjoints.
Il est important que la notion d\'ordre d\'un élément soit mentionnée et comprise dans des cas simples. 

Les exemples doivent figurer en bonne place dans cette leçon. On peut par exemple étudier les groupes de symétries $\\mathfrak{A}_4$ , $\\mathfrak{S}_4$ , $\\mathfrak{A}_5$ et relier sur ces exemples géométrie et algèbre, les représentations ayant ici toute leur place. Il est utile de connaître les groupes diédraux, et pour les candidats aguerris, les spécificités de groupes plus exotiques comme le groupe quaternionique. Le théorème de structure des groupes abéliens finis doit être connu.
',
                'options' => 15,
            ),
            55 => 
            array (
                'id' => 556,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:46:15',
                'annee' => 2015,
                'numero' => 105,
                'type_id' => 1,
                'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
            'rapport' => 'Parmi les attendus, il faut savoir relier la leçon avec les notions d\'orbites et d\'actions de groupes. Il faut aussi savoir décomposer une permutation en cycles à supports disjoints, tant sur le plan théorique (preuve du théorème de décomposition), que pratique (sur un exemple). Il est important de savoir déterminer les classes de conjugaisons du groupe symétrique par la décomposition en cycles, et, pour les candidats confirmés, dominer les problèmes de dénombrement qui en résultent. 

Des dessins ou des graphes illustrent de manière commode ce que sont les permutations. 

Par ailleurs, un candidat qui se propose de démontrer que tout groupe simple d\'ordre 60 est isomorphe à $\\mathfrak{A}_5$  devrait savoir donner des applications à la simplicité d\'un groupe.

L\'existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l\'objet d\'un développement.

Comme pour toute structure algébrique, il est souhaitable de s\'intéresser aux automorphismes d\'un groupe, par exemple, à ceux du groupe symétrique. On note que les candidats connaissent en général les applications du groupe symétrique aux polyèdres réguliers de l\'espace.
',
                'options' => 15,
            ),
            56 => 
            array (
                'id' => 557,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:46:27',
                'annee' => 2015,
                'numero' => 106,
                'type_id' => 1,
            'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications.',
            'rapport' => 'Cette leçon est souvent présentée comme un catalogue de résultats épars et zoologiques sur $GL(E)$. Il serait bien que les candidats unifient la présentation de la leçon en faisant correspondre les sous-groupes  du groupe linéaire avec les stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.).

À quoi peuvent servir des générateurs du groupe $GL(E)$ ? Qu\'apporte la topologie dans cette leçon ? Il est préférable de se poser ces questions avant de les découvrir le jour de l\'oral. Certains candidats affirment que $GL_n(\\mathbb{K})$ est dense (et ouvert) dans $M_n(\\mathbb{K})$. Il est judicieux de préciser les hypothèses nécessaires sur le corps $\\mathbb{K}$ ainsi que la topologie sur $M_n(\\mathbb{K})$. 

La présentation du pivot de Gauss et de ses applications se justifient pleinement.

Il faut aussi savoir réaliser $\\mathfrak{S}_n$ dans $GL(n,\\mathbb{K})$ et faire le lien entre signature et déterminant. Dans le même ordre d\'idée, la théorie des représentations permet d\'illustrer, dans les leçons plus robustes, l\'omniprésence de $GL_n(\\mathbb{C})$ et de son sous-groupe unitaire.
',
                'options' => 15,
            ),
            57 => 
            array (
                'id' => 558,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:46:54',
                'annee' => 2015,
                'numero' => 107,
                'type_id' => 1,
                'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel.',
                'rapport' => 'Il s\'agit d\'une leçon où théorie et exemples doivent apparaître. Le candidat doit, d\'une part, savoir dresser une table de caractères pour des petits groupes. Il doit, d\'autre part, savoir tirer des informations sur le groupe à partir de sa table de caractères, et savoir également trouver la table de caractères de certains sous-groupes.

On voit souvent dans les développements qu\'un candidat qui sait manier les techniques de base sur les caractères ne sait pas forcément relier ceux-ci aux représentations. Le caractère est un outil puissant, mais il reste un outil, ce n\'est pas l\'intérêt ultime de la leçon.

Dans le même ordre d\'idée, le lemme de Schur est symptomatique d\'une confusion : dans le cas où les deux représentations $V$ et $V\'$ sont isomorphes, on voit que les candidats confondent isomorphisme de $V$ dans $V\'$ avec endomorphisme de $V$. Par exemple, diagonaliser une application linéaire de $V$ dans $V\'$ est une faute avérée, il faut pour cela identifier $V$ et $V\'$, ce que le candidat devrait faire de façon consciente et éclairée.
',
                'options' => 7,
            ),
            58 => 
            array (
                'id' => 559,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:47:09',
                'annee' => 2015,
                'numero' => 108,
                'type_id' => 1,
                'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                'rapport' => 'C\'est une leçon qui demande un minimum de culture mathématique. Peu de candidats voient l\'utilité des parties génératrices dans l\'analyse des morphismes de groupes ou pour montrer la connexité de certains groupes.

Tout comme dans la leçon 106, la présentation du pivot de Gauss et de ses applications est envisageable.
',
                'options' => 15,
            ),
            59 => 
            array (
                'id' => 560,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:47:33',
                'annee' => 2015,
                'numero' => 109,
                'type_id' => 1,
                'nom' => 'Représentations de groupes finis de petit cardinal.',
            'rapport' => 'Il s\'agit d\'une leçon où le matériel théorique doit figurer, pour ensuite laisser place à des exemples. Les représentations peuvent provenir d\'actions de groupes sur des ensembles finis, de groupes d\'isométries, d\'isomorphismes exceptionnels entre groupes de petit cardinal... Inversement, on peut chercher à interpréter des représentations de façon géométrique, mais il faut avoir conscience qu\'une table de caractères provient généralement de représentations complexes et non réelles, a priori . Pour prendre un exemple ambitieux, la construction de l\'icosaèdre à partir de la table de caractères de $\\matfrak{A}_5$ demande des renseignements sur l\'indice de Schur (moyenne des caractères sur les carrés des éléments du groupe).
',
                'options' => 7,
            ),
            60 => 
            array (
                'id' => 561,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-04-18 16:48:34',
                'annee' => 2015,
                'numero' => 110,
                'type_id' => 1,
                'nom' => 'Caractères d\'un groupe abélien fini et transformée de Fourier discrète. Applications.',
                'rapport' => 'Il s\'agit d\'une nouvelle leçon qui n\'a pas encore trouvé l\'affection des candidats.

Pourtant, le sujet et abordable, par exemple : le théorème de  structure des groupes abéliens finis, qui a bien entendu une place de choix dans cette leçon. On pourra en profiter pour montrer l\'utilisation de la dualité dans ce contexte. Comme application, la cyclicité du groupe multiplicatif d\'un corps fini est tout à fait adaptée. D\'ailleurs, des exemples de caractères, additifs, ou multiplicatifs dans le cadre des corps finis, sont les bienvenus. Pour les candidats chevronnés, les sommes de Gauss permettent de
constater toute l\'efficacité de ces objets.


L\'algèbre du groupe est un objet intéressant, surtout sur le corps des complexes, où il peut être muni d\'une forme hermitienne. On peut l\'introduire comme une algèbre de fonctions, munie d\'un produit de
convolution, mais il est aussi agréable de la voir comme une algèbre qui "prolonge" la mutiplication du groupe.

La transformée de Fourier discrète pourra être vue comme son analogue analytique, avec ses formules d\'inversion, sa formule de Plancherel, mais dans une version affranchie des problèmes de convergence,
incontournables en analyse de Fourier.

On pourra y introduire la transformée de Fourier rapide sur un groupe abélien d\'ordre une puissance de 2 ainsi que des applications à la multiplication d\'entiers, de polynômes et éventuellement au décodage de codes via la transformée de Hadamard.
',
                'options' => 7,
            ),
            61 => 
            array (
                'id' => 562,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 120,
                'type_id' => 1,
                'nom' => 'Anneaux $Z/nZ$. Applications.',
                'rapport' => '
Cette leçon, souvent choisie par les candidats, demande toutefois une préparation minutieuse.

Tout d\'abord, $n$ n\'est pas forcément un nombre premier. Il serait bon de connaître les sous-groupes de $\\mathbb{Z}/n\\mathbb{Z}$ et, plus généralement, les morphismes de groupes de $\\mathbb{Z}/n\\mathbb{Z}$ dans $\\mathbb{Z}/m\\mathbb{Z}$.

Il est nécessaire de bien maîtriser le lemme chinois et sa réciproque. Et pour les candidats plus étoffés, connaître une généralisation du lemme chinois lorsque deux éléments ne sont pas premiers entre eux, faisant apparaître le pgcd et le ppcm de ces éléments. 

Il faut bien sûr savoir appliquer le lemme chinois à l\'étude du groupe des inversibles, et ainsi, retrouver la multiplicativité de l\'indicatrice d\'Euler. Toujours dans le cadre du lemme chinois, il est bon de distinguer clairement les propriétés de groupes additifs et d\'anneaux, de connaître les automorphismes,
les nilpotents, les idempotents...


Enfin, les candidats sont invités à rendre hommage à Gauss en présentant quelques applications arithmétiques des anneaux $\\mathbb{Z}/n\\mathbb{Z}$ telles que l\'étude de quelques équations diophantiennes bien choisies. De même, les applications cryptographiques telles que l\'algorithme RSA sont naturelles dans cette leçon.
',
                'options' => 15,
            ),
            62 => 
            array (
                'id' => 563,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 121,
                'type_id' => 1,
                'nom' => 'Nombres premiers. Applications.',
                'rapport' => '

Il s\'agit d\'une leçon pouvant être abordée à divers niveaux.

Il y a tant à dire sur la question que le candidat devra fatalement faire des choix. Attention toutefois à celui des développements, ils doivent être pertinents ; l\'apparition d\'un nombre premier n\'est pas
suffisant ! 

La réduction modulo p n\'est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important, qu\'il faudrait citer. Sa démonstration n\'est bien sûr pas exigible au niveau de l\'agrégation.

Quelques résultats sur les corps finis et leur géométrie sont les bienvenus, ainsi que des applications en cryptographie.
',
                'options' => 15,
            ),
            63 => 
            array (
                'id' => 564,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 122,
                'type_id' => 1,
                'nom' => 'Anneaux principaux. Applications.',
                'rapport' => '
C\'est une leçon où les candidats ont tendance à se placer sur un plan trop théorique. Il est possible de présenter des exemples d\'anneaux principaux classiques autres que $\\mathbb{Z}$ et $K[X]$ (décimaux, entiers de Gauss ou d\'Eisenstein), accompagnés d\'une description de leurs irréductibles.

Les applications en algèbre linéaire ne manquent pas, il serait bon que les candidats les illustrent. Par exemple, il est étonnant de ne pas voir apparaître la notion de polynôme minimal parmi les applications. 

Le candidat plus cultivé peut donner des exemples d\'anneaux non principaux, mais aussi des exemples d\'équations diophantiennes résolues à l\'aide d\'anneaux principaux. A ce sujet, il sera fondamental de savoir déterminer les unités d\'un anneau, et leur rôle au moment de la décomposition en facteurs
premiers. On a pu noter dans cette leçon l\'erreur répandue que $1+i$ et $1-i$ sont des irréductibles premiers entre eux dans l\'anneau factoriel $\\mathbb{Z}[i]$.
',
                'options' => 7,
            ),
            64 => 
            array (
                'id' => 565,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 123,
                'type_id' => 1,
                'nom' => 'Corps finis. Applications.',
                'rapport' => '
Il s\'agit d\'une leçon comportant un certain nombre d\'attendus. En premier lieu, une construction des corps finis doit être connue. Ensuite, les constructions des corps de petit cardinal doivent avoir été pratiquées. Les injections des divers $F_q$ doivent être connues. Enfin, les applications des corps finis (y compris pour $F_q$ avec $q$ non premier !) ne doivent pas être oubliées : citons par exemple l\'étude de polynômes à coefficients entiers et de leur irréductibilité.

Il sera bon de comprendre l\'utilisation des degrés des extensions, et leurs petites propriétés arithmétiques amenées par le théorème de la base téléscopique. 

Un candidat qui étudie les carrés dans un corps fini doit savoir aussi résoudre les équations de degré 2.

Le théorème de l\'élément primitif, s\'il est énoncé, doit pouvoir être utilisé.

Les applications sont nombreuses. S\'ils sont bien maîtrisées, alors les codes correcteurs peuvent être mentionés.
',
                'options' => 15,
            ),
            65 => 
            array (
                'id' => 566,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 124,
                'type_id' => 1,
                'nom' => 'Anneau des séries formelles. Applications.',
                'rapport' => '
C\'est une leçon qui doit être illustrée par de nombreux exemples et applications, souvent en lien avec les séries génératrices ; combinatoire, calcul des sommes de Newton, relations de récurrence, nombre de partitions, représentations et séries de Molien, etc.

A ce propos, on note que les candidats qui choisissent de présenter en développement les séries de Molien ne savent que rarement interpréter les séries obtenues sur des exemples simples. Ces séries ne font pas que calculer les dimensions de sous-espaces d\'invariants, elles suggèrent aussi des choses plus profondes sur la structure de l\'algèbre d\'invariants.
',
                'options' => 7,
            ),
            66 => 
            array (
                'id' => 567,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 125,
                'type_id' => 1,
                'nom' => 'Extensions de corps. Exemples et applications.',
                'rapport' => '
Très peu de candidats ont choisi cette leçon. On doit y voir le théorème de la base téléscopique et ses applications à l\'irréductibilité de certains polynômes, ainsi que les corps finis. Une version dégradée de la théorie de Galois (qui n\'est pas au programme) est très naturelle dans cette leçon.
',
                'options' => 7,
            ),
            67 => 
            array (
                'id' => 568,
                'created_at' => '2016-03-14 12:37:28',
                'updated_at' => '2016-03-14 12:46:22',
                'annee' => 2015,
                'numero' => 126,
                'type_id' => 1,
                'nom' => 'Exemples d\'équations diophantiennes.',
                'rapport' => '
Il s\'agit d\'une leçon nouvelle, ou plus exactement d\'une renaissance. On y attend les notions de bases servant à aborder les équations de type $ax+by = d$ (identité de Bezout, lemme de Gauss), les systèmes de congruences, mais aussi bien entendu la méthode de descente et l\'utilisation de la réduction modulo un nombre premier $p$.

La leçon peut aussi dériver la notion de factorialité, illustrée par des équations de type Mordell, Pell-Fermat, et même Fermat (pour $n= 2$, ou pour les nombres premiers de Sophie Germain.
',
                    'options' => 7,
                ),
                68 => 
                array (
                    'id' => 569,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 127,
                    'type_id' => 1,
                    'nom' => 'Droite projective et birapport.',
                    'rapport' => '
Il s\'agit également d\'une leçon récemment introduite et reprenant un titre ancien. Le birraport peut être vu comme un invariant pour l\'action du groupe linéaire $GL_2(\\mathfrak{K})$ (ou plus finement son quotient projectif $PGL_(2 , \\mathfrak{K})$ sur l\'ensemble $P^1(K)$ des droites du plan vectoriel $\\mathfrak{K}^2$.

Lorsque le corps $\\mathfrak{K}$ est le corps des complexes, il ne faudra pas manquer d\'en voir les applications à la cocyclicité, et à l\'étude des droites et cercles du plan affine euclidien.

On peut s\'aider du birapport, sur des corps finis, pour construire des isomorphismes classiques entre groupes finis de petit cardinal. L\'ensemble des droites du plan contenant un point fixe est naturellement une droite projective. Cela permet enfin d\'identifier une conique à une droite projective : c\'est l\'idéal d\'une preuve classique du théorème de l\'hexagramme de Pascal. Par ailleurs, on pourra remarquer le birapport dans l\'expression de la distance hyperbolique sur le demi-plan de Poincaré.
',
                    'options' => 7,
                ),
                69 => 
                array (
                    'id' => 570,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 140,
                    'type_id' => 1,
                    'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
                    'rapport' => '
Le bagage théorique est somme toute assez classique, même si parfois, le candidat ne voit pas l\'unicité de la décomposition en éléments simples en termes d\'indépendance en algèbre linéaire. Ce sont surtout les applications qui sont attendues : séries génératrices (avec la question à la clef : à quelle condition une série formelle est-elle le développement d\'une fraction rationnelle), automorphismes de $\\mathfrak{K}(X)$ , version algébrique du théorème des résidus, action par homographies. 

Le théorème de Lüroth n\'est pas obligatoire et peut même se révéler un peu dangereux si le candidat n\'est pas suffisamment préparé aux questions classiques qui l\'attendent sur ce sujet.
',
                    'options' => 7,
                ),
                70 => 
                array (
                    'id' => 571,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 141,
                    'type_id' => 1,
                    'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                    'rapport' => '
Le jury attend dans cette leçon un bagage théorique permettant de définir corps de rupture, corps de décomposition (la preuve de l\'unicité de ce dernier n\'est pas exigée), ainsi que des illustrations dans différents types de corps (réel, rationnel, corps finis). 

Attention à ne pas croire qu\'un polynôme réductible admet forcément des racines (même en dehors du cadre de cette leçon !). 

sBien entendu, les corps finis ont une place de choix et il sera instructif de chercher des polynômes irréductibles de degré $2,3,4$ sur $\\mathbb{F}_2$, ou $\\mathbb{F}_3$.

Il faut savoir qu\'il existe des corps algébriquement clos de caractéristique nulle autres que $\\mathbb{C}$ se savoir montrer que l\'ensemble des nombres algébriques sur le corps $\\mathbb{Q}$ des rationnels est un corps algébriquement clos.

Il faut connaître le théorème de la base téléscopique ainsi que les utilisations artithmétiques (utilisation de la divisibilité) que l\'on peut en faire dans l\'étude de l\'irréductibilité des polynômes.
',
                    'options' => 15,
                ),
                71 => 
                array (
                    'id' => 572,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 142,
                    'type_id' => 1,
                    'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                    'rapport' => '
La leçon ne doit pas se concentrer exclusivement sur les aspects formels ou uniquement sur les polynômes symétriques.

Les aspects arithmétiques ne doivent pas être négligés. Il faut savoir montrer l\'irréductibilité d\'un polynôme à plusieurs indéterminées en travaillant sur un anneau de type $A[X]$, où $A$ est factoriel.

Le théorème fondamental sur la structure de l\'algèbre des polynômes symétriques est vrai sur $\\mathbb{Z}$. L\'algorithme peut être présenté sur un exemple.

Les applications aux quadriques, aux relations racines/coefficients ne doivent pas être délaissées : on peut faire par exemple agir le groupe $GL(n,\\mathbb{R})$ sur les polynômes à n indéterminées de degré inférieur $2$. 
',
                    'options' => 7,
                ),
                72 => 
                array (
                    'id' => 573,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 143,
                    'type_id' => 1,
                    'nom' => 'Résultant. Applications.',
                    'rapport' => '
Le caractère entier du résultant (il se définit sur Z ) doit être mis en valeur et appliqué. 

La partie application doit montrer la diversité du domaine (par exemple en arithmétique, calcul d\'intersection/élimination, calcul différentiel, polynômes annulateurs d\'entiers algébriques).

Il ne faut pas perdre de vue l\'application linéaire sous-jacente
$(U,V) \\longmapsto AU + BV$ qui lie le résultant et le pgcd de $A$ et $B$.
',
                    'options' => 7,
                ),
                73 => 
                array (
                    'id' => 574,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 144,
                    'type_id' => 1,
                    'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Exemples et applications.',
                    'rapport' => '
Il s\'agit d\'une leçon au spectre assez vaste. On peut y traiter de méthodes de résolutions, de théorie des corps (voire théorie de Galois si affinités), de topologie (continuité des racines) ou même de formes quadratiques. Il peut être pertinent d\'introduire la notion de polynôme scindé, de citer le théorème de d\'Alembert-Gauss et des applications des racines (valeurs propres, etc. ).
On pourra parler des applications de la réduction au calcul d\'approximations de racines.

Notons le lien solide entre la recherche des racines d\'un polynôme et la réduction des matrices. Les valeurs propres de la matrice compagnon d\'un polynôme permet d\'entretenir ce lien. Les problèmes de localisation des valeurs propres, comme les disques de Gershgorin, sont tout à fait appropriés à ce contexte.
',
                    'options' => 7,
                ),
                74 => 
                array (
                    'id' => 575,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 150,
                    'type_id' => 1,
                    'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                    'rapport' => '
Cette leçon demande un certain recul. Les actions ne manquent pas et selon l\'action, on pourra dégager d\'une part des invariants (rang, matrices échelonnées réduites), d\'autre part des algorithmes. On peut aussi, si l\'on veut aborder un aspect plus théorique, faire apparaître à travers ces actions quelques décompositions célèbres, ainsi que les adhérences d\'orbites, lorsque la topologie s\'y prête. 

On pourra aussi travailler sur des corps finis et utiliser le dénombrement dans ce contexte.
',
                    'options' => 15,
                ),
                75 => 
                array (
                    'id' => 576,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 151,
                    'type_id' => 1,
                'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                    'rapport' => '
Dans cette leçon, il est important de bien connaître les théorèmes fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. Ces théorèmes semblent simples car ils ont été très souvent pratiqués, mais leur preuve demande un soin particulier, ce qui rend la leçon plus difficile qu\'on ne le croit.

Des questions élémentaires comme "un sous-espace vectoriel d\'un espace vectoriel de dimension finie, est-il aussi de dimension finie ? " peuvent dérouter un candidat.

Les diverses caractérisations du rang trouvent bien leur place ainsi que, pour les candidats plus chevronnés, l\'utilisation du degré d\'une extension dans la théorie des corps.
',
                    'options' => 15,
                ),
                76 => 
                array (
                    'id' => 577,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 152,
                    'type_id' => 1,
                    'nom' => 'Déterminant. Exemples et applications.',
                    'rapport' => '
Il s\'agit encore d\'une leçon où les résultats abondent et où le candidat devra faire des choix. On doit pouvoir, dans cette leçon, commencer par définir correctement le déterminant. Beaucoup de candidats entament la leçon en disant que le sous-espace des formes dimension n -linéaires alternées sur un espace de n est de dimension 1, ce qui est fort à propos. Toutefois, il est essentiel de savoir le montrer.

Il faut que le plan soit cohérent ; si le déterminant n\'est défini que sur $\\mathbb{R}$ ou $\\mathbb{C}$, il est délicat de définir $\\mathsf{det} (A - X I_n)$ avec $A$ une matrice carrée.

L\'interprétation du déterminant comme volume est essentielle.

Le calcul explicite est important, toutefois, le jury ne peut se contenter que d\'un Vandermonde ou d\'un déterminant circulant ! De même il est envisageable que des candidats s\'intéressent aux calculs de déterminant sur $\\mathbb{Z}$ avec des méthodes multimodulaires. Le résultant et les applications simples à l\'intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon.

Il serait bien que la continuité du déterminant trouve une application, ainsi que son caractère polynomial.
',
                    'options' => 15,
                ),
                77 => 
                array (
                    'id' => 578,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 153,
                    'type_id' => 1,
                    'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                    'rapport' => '
Cette leçon est souvent choisie pour son lien avec la réduction, toutefois, le jury ne souhaite pas que le candidat présente un catalogue de résultats autour de la réduction, mais seulement ce qui a trait aux polynômes d\'endomorphismes.

Il faut consacrer une courte partie de la leçon à l\'algèbre $K[u]$ , connaître sa dimension sans hésiter.
Les propriétés globales pourront être étudiées par les meilleurs. Le jury souhaiterait voir certains liens entre réduction de l\'endomorphisme u et structure de l\'algèbre $K[u]$ . Le candidat peut s\'interroger sur les idempotents et le lien avec la décomposition en somme de sous-espaces caractéristiques.

Il faut bien préciser que, dans la réduction de Dunford, les composantes sont des polynômes en l\'endomorphisme, et en connaître les conséquences théoriques et pratiques. 

L\'aspect applications est trop souvent négligé. On attend d\'un candidat qu\'il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est souhaitable que les candidats ne fassent pas la confusion entre diverses notions de multiplicité pour une valeur propre $\\lambda$ donnée (algébrique ou géométrique). Enfin, rappelons que pour calculer $A^k$ , il n\'est pas nécessaire en général de réduire A (la donnée d\'un polynôme annulateur de A suffit bien souvent).
',
                    'options' => 15,
                ),
                78 => 
                array (
                    'id' => 579,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 154,
                    'type_id' => 1,
                    'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                    'rapport' => '
Les candidats doivent s\'être interrogés sur les propriétés de l\'ensemble des sous-espaces stables par un endomorphisme. Des études détaillées de cas sont les bienvenues, par exemple le cas d\'une matrice diagonalisable, le cas d\'une matrice nilpotente d\'indice maximum. 

La décomposition de Frobenius trouve tout à fait sa place dans la leçon. Notons qu\'il a été ajouté à l\'intitulé la notion de familles d\'endomorphismes. Ceci peut déboucher par exemple sur des endomorphismes commutant entre eux ou sur la théorie des représentations.
',
                    'options' => 7,
                ),
                79 => 
                array (
                    'id' => 580,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 155,
                    'type_id' => 1,
                    'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                    'rapport' => '
Il faut ici pouvoir donner des exemples naturels d\'endomorphismes diagonalisables et des critères de diagonalisabilité. 

On peut coir que le calcul de l\'exponentielle d\'un endomorphisme diagonalisable est immédiat une fois que l\'on connaît les valeurs propres et ceci sans diagonaliser la matrice, par exemple à l\'aide des  projecteurs spectraux.

On peut sur le corps des réels et des complexes donner des propriétés topologiques. Mentionnons que l\'affirmation "l\'ensemble des matrices diagonalisables de $M_n(K)$ est dense dans $M_n(K)$" nécessite quelques précisions sur le corps K et la topologie choisie pour $M_n(K)$.

Sur les corps finis, on a des critères spécifiques de diagonalisabilité. On peut dénombrer les endomorphismes diagonalisables, ou possédant des propriétés données, liées à la diagonalisation.

Le lien peut aussi être fait avec la théorie des représentations et la transformée de Fourier rapide.
',
                    'options' => 7,
                ),
                80 => 
                array (
                    'id' => 581,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 156,
                    'type_id' => 1,
                    'nom' => 'Exponentielle de matrices. Applications.',
                    'rapport' => '
C\'est une leçon difficile et il faut noter que ce n\'est pas une leçon  d\'analyse. Il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle.

Les questions de surjectivité ou d\'injectivité doivent être abordées. Par exemple la matrice $A = \\begin{pmatrix} -1 \\esperluette 1 \\\\ 0 \\esperluette -1 \\end{pmatrix}$ est-elle dans l\'image $\\exp(M_2(\\mathbb{R}))$ ? La matrice définie par blocs $B = \\begin{pmatrix} A \\esperluette 0 \\\\ 0 \\esperluette A \\end{pmatrix}$ est-elle dans l\'image $\\exp(M_4(\\mathbb{R}))$ ?

La décomposition de Dunford multiplicative (décomposition de Jordan) de $\\exp(A)$ trouve toute son utilité dans cette leçon. Pour les candidats plus aguerris, les sous-groupes à un paramètre du groupe linéaire y sont tout à fait à propos. On peut s\'interroger si ces sous-groupes constituent des sous-variétés fermées de $GL(n,\\mathbb{R})$.

Notons que l\'exponentielle fait bon ménage avec la décomposition polaire dans bon nombre de problèmes sur les sous-groupes du groupe linéaire. 

L\'étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si l\'on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités. 

Les applications aux équations différentielles doivent être évoquées sans constituer l\'essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique, mais le jury déconseille aux candidats de proposer ce thème dans un développement.

Les notions d\'algèbres de Lie ne sont pas au programme de l\'agrégation, on conseille de n\'aborder ces sujets qu\'à condition d\'avoir une certaine solidité sur la question.
',
                    'options' => 7,
                ),
                81 => 
                array (
                    'id' => 582,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 157,
                    'type_id' => 1,
                    'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                    'rapport' => '
Il est possible de mener une leçon de bon niveau, même sans la décomposition de Jordan, à l\'aide des noyaux itérés. On doit savoir déterminer si deux matrices nilpotentes sont semblables grâce aux noyaux itérés (ou grâce à la décomposition de Jordan si celle-ci est maîtrisée). 

Deux endomorphismes trigonalisables qui commutent sont simultanément trigonalisables, mais une grande proportion de candidats pensent à tort que la réciproque est vraie. 

Notons que l\'étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et que l\'étude sur un corps fini donne lieu à de jolis problèmes de dénombrement.
',
                    'options' => 15,
                ),
                82 => 
                array (
                    'id' => 583,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 158,
                    'type_id' => 1,
                    'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                    'rapport' => '
C\'est une leçon transversale. La notion de signature doit bien sûr figurer dans la leçon et on ne doit surtout pas se cantonner au cas des matrices définies positives. L\'action du groupe linéaire sur l\'espace des matrices symétriques peut donner un cadre naturel à cette leçon.

Curieusement, il est fréquent que le candidat énonce l\'existence de la signature d\'une matrice symétrique réelle sans en énoncer l\'unicité dans sa classe de congruence.

L\'orthogonalisation simultanée est un résultat important de cette leçon. Il faut en connaître les applications géométriques aux quadriques.

On doit faire le lien avec les formes quadratiques et les formes hermitiennes. La partie réelle et la partie imaginaire d\'un produit hermitien définissent des structures sur l\'espace vectoriel réel sous-jacent.
',
                    'options' => 7,
                ),
                83 => 
                array (
                    'id' => 584,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 159,
                    'type_id' => 1,
                    'nom' => 'Formes linéaires et dualité en dimension finie. Exemples et applications.',
                    'rapport' => '
Il est important de bien placer la thématique de la dualité dans cette leçon : celle-ci permet de créer une correspondance féconde entre un morphisme et son morphisme transposé, un sous-espace et son orthogonal (canonique), les noyaux et les images, les sommes et les intersections. Bon nombre de résultats d\'algèbre linéaire se voient dédoublés par cette correspondance.

Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d\'une intersection d\'hyperplans via la dualité est important dans cette leçon. 

L\'utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d\'obtenir les équations d\'un sous-espace vectoriel ou d\'exhiber une base d\'une intersection d\'hyperplans. Cette leçon peut être traitée sous différents aspects : géométrique, algébrique, topologique, analytique, etc. Il faut que les développements proposés soient en lien direct, comme toujours, avec la leçon ; proposer la trigonalisation simultanée est un peu osé ! Enfin rappeler que la différentielle d\'une fonction réelle est
une forme linéaire semble incontournable.
',
                    'options' => 15,
                ),
                84 => 
                array (
                    'id' => 585,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 160,
                    'type_id' => 1,
                'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                    'rapport' => '
Dans cette leçon, les candidats doivent bien prendre conscience que le caractère euclidien de l\'espace est essentiel pour que l\'endomorphisme soit remarquable. Par exemple, des développements comme le lemme des noyaux ou la décomposition de Dunford n\'ont rien à faire ici. En revanche, l\'utilisation du fait que l\'orthogonal d\'un sous-espace stable par un endomorphisme est stable par l\'adjoint doit être mis en valeur.
',
                    'options' => 7,
                ),
                85 => 
                array (
                    'id' => 586,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 161,
                    'type_id' => 1,
                    'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Applications en dimensions $2$ et $3$.',
                    'rapport' => '
La classification des isométries en dimension 2 est exigible. En dimension 3, il faut savoir classifier les rotations et connaître les liens avec la réduction. On peut penser aux applications aux isométries laissant stables certaines objets en dimension 2 et 3.
',
                    'options' => 7,
                ),
                86 => 
                array (
                    'id' => 587,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:22',
                    'annee' => 2015,
                    'numero' => 162,
                    'type_id' => 1,
                    'nom' => 'Systèmes d\'équations linéaires; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                    'rapport' => '
Il semble que cette leçon soit moins choisie par les candidats depuis l\'ajout de l\'aspect algorithmique dans l\'intitulé. A ce sujet, il faut savoir que les techniques liées au simple pivot de Gauss constituent l\'essentiel des attendus. 

La leçon doit impérativement présenter la notion de système échelonné, avec une définition précise et correcte et situer l\'ensemble dans le contexte de l\'algèbre linéaire (sans oublier la dualité !).

Pour les candidats chevronnés, les relations de dépendances linéaires sur les colonnes d\'une matrice échelonnée sont claires et permettent de décrire simplement les orbites de l\'action à gauche de
$GL(n,K)$ sur $M_n(K)$ donnée par $(P,A) \\longmapsto PA$.

Un point de vue opératoire doit accompagner l\'étude théorique et l\'intérêt pratique (algorithmique) des méthodes présentées doit être expliqué y compris sur des exemples simples où l\'on attend parfois une résolution explicite. 

Des discussions sur la résolution de systèmes sur Z et la forme normale de Hermite peuvent trouver leur place dans cette leçon.
',
                    'options' => 15,
                ),
                87 => 
                array (
                    'id' => 588,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 170,
                    'type_id' => 1,
                    'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                    'rapport' => '
Il faut tout d\'abord noter que l\'intitulé implique implicitement que le candidat ne doit pas se contenter de travailler sur R . Il faut savoir que les formes quadratiques existent sur le corps des complexes et sur les corps finis et il faut savoir les classifier.

On ne doit pas oublier l\'interprétation géométrique des notions introduites (lien entre coniques, formes quadratiques, cônes isotropes) ou les aspects élémentaires (par exemple le discriminant de l\'équation $ax^2 + bx +  cy^2 = 0$ et la signature de la forme quadratique $ax^2 + bxy + cy^2$ ). On ne peut se limiter à des considérations élémentaires d\'algèbre linéaire. Les formes quadratiques ne sont pas toutes non dégénérées (la notion de quotient est utile pour s\'y ramener).

L\'algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique de lien avec la signature doit être clairement énoncé.

Malheureusement la notion d\'isotropie est mal maîtrisée par les candidats, y compris les meilleurs d\'entre eux. Le cône isotrope est un aspect important de cette leçon, qu\'il faut rattacher à la géométrie différentielle. Il est important d\'illustrer cette leçon d\'exemples naturels.
',
                    'options' => 15,
                ),
                88 => 
                array (
                    'id' => 589,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 171,
                    'type_id' => 1,
                    'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                    'rapport' => '
La preuve de la loi d\'inertie de Silvester doit être connue ainsi que l\'orthogonalisation simultanée. Le candidat doit avoir compris la signification géométrique des deux entiers $r$ et $s$ composant la signature
d\'une forme quadratique réelle ainsi que leur caractère classifiant. 

La différentielle seconde d\'une fonction de plusieurs variables est une forme quadratique importante.

Pour les candidats de bon niveau, l\'indicatrice de Schur-Frobenius, sur la possibilité de réaliser une représentation donnée sur le corps des réels,  permet une belle incursion de la théorie des représentations dans cette leçon.
',
                    'options' => 7,
                ),
                89 => 
                array (
                    'id' => 590,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 180,
                    'type_id' => 1,
                    'nom' => 'Coniques. Applications.',
                    'rapport' => '
La définition des coniques affines non dégénérées doit être connue. Les propriétés classiques des coniques doivent être présentées. Bien distinguer les notions affines, métriques ou projectives, la classification des coniques étant sensiblement différente selon le cas. Souvent le candidat annonce qu\'il va "classifier les coniques" mais sans être capable de préciser la nature de cette classification. Plus généralement, il serait bien que les candidats aient réfléchi à ce que l\'on entend par "classification" en mathématiques.

On peut se situer sur un autre corps que celui des réels. Le lien entre classification des coniques et classification des formes quadratiques peut être établi à des fins utiles.
',
                    'options' => 7,
                ),
                90 => 
                array (
                    'id' => 591,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 181,
                    'type_id' => 1,
                    'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
                    'rapport' => '
On attend des candidats qu\'ils parlent de coordonnées barycentriques et les utilisent par exemple dans le triangle (coordonnées barycentriques de certains points remarquables). Il est judicieux de parler d\'enveloppe convexe, de points extrémaux, ainsi que des applications qui en résultent.
',
                    'options' => 15,
                ),
                91 => 
                array (
                    'id' => 592,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 182,
                    'type_id' => 1,
                    'nom' => 'Applications des nombres complexes à la géométrie. Homographies.',
                    'rapport' => '
Cette leçon ne saurait rester au niveau de la Terminale.

L\'étude des inversions est tout à fait appropriée dans cette leçon, en particulier la possibilité de ramener un cercle à une droite et inversement. La formule de Ptolémée, pour donner un exemple, illustre bien l\'utilisation de cet outil.

On peut parler des suites définies par récurrence par une homographie et leur lien avec la réduction dans $SL_2(\\mathbb{C})$.

Une étude de l\'exponentielle complexe et des homographies de la sphère de Riemann est tout à fait appropriée. La réalisation du groupe $SU_2$ dans le corps des quaternions et ses applications peuvent trouver sa place dans la leçon.
',
                    'options' => 15,
                ),
                92 => 
                array (
                    'id' => 593,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 183,
                    'type_id' => 1,
                    'nom' => 'Utilisation des groupes en géométrie.',
                    'rapport' => '
C\'est une leçon transversale et difficile, qui peut aborder des aspects variés selon les structures algébriques présentes. D\'une part un groupe de transformations permet de ramener un problème de géométrie à un problème plus simple.

D\'autre part, les actions de groupes sur la géométrie permettent de dégager des invariants essentiels (angle, birapport). On retrouvera encore avec bonheur les groupes d\'isométries d\'un solide.
',
                    'options' => 15,
                ),
                93 => 
                array (
                    'id' => 594,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 190,
                    'type_id' => 1,
                    'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                    'rapport' => '
Il faut dans un premier temps dégager clairement les méthodes et les illustrer d\'exemples significatifs. L\'utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. Le jury s\'attend à ce que les candidats sachent calculer des cardinaux classiques et certaines probabilités !

L\'introduction des corps finis (même en se limitant aux cardinaux premiers) permet de créer un lien fécond avec l\'algèbre linéaire.
',
                    'options' => 15,
                ),
                94 => 
                array (
                    'id' => 595,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 201,
                    'type_id' => 2,
                    'nom' => 'Espaces de fonctions : exemples et applications.',
                    'rapport' => '
C\'est une leçon riche où le candidat devra choisir soigneusement le niveau auquel il souhaite se placer.

Les espaces de fonctions continues sur un compact (par exemple l\'intervalle
$[0,1]$ ) offrent des exemples élémentaires et pertinents. Dans ce domaine, le jury attend une maîtrise du fait qu\'une limite uniforme de fonctions continues est continue. Il est regrettable de voir des candidats, qui auraient eu intérêt à se concentrer sur les espaces de fonctions continues ou bien de classe $C^1$ et les bases de la convergence uniforme, proposer en développement le théorème de Riesz-Fischer dont il ne maîtrise visiblement pas la démonstration. Toutefois pour des candidats solides, ces espaces offrent de belles possibilités. 

Enfin, les candidats ambitieux pourront aborder les espaces de fonctions holomorphes sur un ouvert de $\\mathbb{C}$.

Signalons que des candidats proposent assez régulièrement une version incorrecte du théorème de Müntz pour les fonctions continues. La version correcte dans ce cadre est 

$$ \\overline{ \\mathsf{Vect}\\{1 , x^{\\lambda_n} \\}} = C( [0,1], \\mathbb{R}) \\iff \\sum_{n \\ge 1} \\frac{1}{\\lambda_n} = + \\infty$$

Des candidats aguerris peuvent développer la construction et les propriétés de l\'espace de Sobolev $H_0^1( ]0,1[)$, ses propriétés d\'injection dans les fonctions continues, et évoquer le rôle de cet espace dans l\'étude de problèmes aux limites elliptiques en une dimension. Ce développement conduit naturellement à une illustration de la théorie spectrale des opérateurs compacts auto-adjoints.
',
                    'options' => 7,
                ),
                95 => 
                array (
                    'id' => 596,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 202,
                    'type_id' => 2,
                    'nom' => 'Exemples de parties denses et applications.',
                    'rapport' => '
Il ne faut pas négliger les exemples élémentaires comme par exemple les sous-groupes de $\\mathbb{R}$ et leurs applications. Cette leçon permet aussi d\'explorer les questions d\'approximations de fonctions par des polynômes et des polynômes trigonométriques. Au delà des exemples classiques, les candidats plus ambitieux peuvent aller jusqu\'à la résolution d\'équations aux dérivées partielles par séries de Fourier.
',
                    'options' => 7,
                ),
                96 => 
                array (
                    'id' => 597,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 203,
                    'type_id' => 2,
                    'nom' => 'Utilisation de la notion de compacité.',
                    'rapport' => '
Il est important de ne pas concentrer la leçon sur la compacité générale (confusion générale entre utilisation de la notion compacité et notion de compacité ). Néanmoins, on attend des candidats une présentation synthétique de la compacité. Des exemples d\'applications comme le théorème de Heine et le théorème de Rolle doivent y figurer et leur démonstration être connue. 

Des exemples significatifs d\'utilisation comme le théorème de Stone-Weierstrass, des théorèmes de point fixe, voire l\'étude qualitative d\'équations différentielles, sont tout-à fait envisageables. Le rôle de la compacité pour des problèmes d\'existence d\'extremums mériterait d\'être davantage étudié (lien avec la coercivité en dimension finie).

Les candidats solides peuvent aussi enrichir leur leçon par des exemples tels que l\'étude des opérateurs à noyau continu.

Pour les candidats ambitieux, les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d\'utilisation de la compacité. Les opérateurs auto-adjoints compacts sur l\'espace de Hilbert relèvent également de cette leçon, et on pourra développer par exemple leurs propriétés spectrales
',
                    'options' => 15,
                ),
                97 => 
                array (
                    'id' => 598,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 204,
                    'type_id' => 2,
                    'nom' => 'Connexité. Exemples et applications.',
                    'rapport' => '
Le rôle clef de la connexité dans le passage du local au global doit être mis en évidence dans cette leçon. Il est important de présenter des résultats naturels dont la démonstration utilise la connexité ; par exemple, diverses démonstrations du théorème de d\'Alembert-Gauss. On distinguera bien connexité et connexité par arcs, mais il est pertinent de présenter des situations où ces deux notions coïncident.
',
                    'options' => 7,
                ),
                98 => 
                array (
                    'id' => 599,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 205,
                    'type_id' => 2,
                    'nom' => 'Espaces complets. Exemples et applications.',
                    'rapport' => '
Les candidats devraient faire apparaître que l\'un des intérêts essentiels de la complétude est de fournir des théorèmes d\'existence en dimension infinie, en particulier dans les espaces de fonctions. Rappelons que l\'on attend des candidats une bonne maîtrise de la convergence uniforme. Le théorème de Cauchy-Lipschitz, mal maîtrisé par beaucoup de candidats, est un point important de cette leçon. Les espaces $L^p$ sont des exemples pertinents qui ne sont pas sans danger pour des candidats aux connaissances fragiles.

On ne s\'aventurera pas à parler du théorème de Baire sans application pertinente et maîtrisée. Rappelons à ce propos que la démonstration détaillée de l\'existence d\'une partie dense de fonctions continues dérivables en aucun point est réservée aux candidats solides.
',
                    'options' => 7,
                ),
                99 => 
                array (
                    'id' => 600,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 206,
                    'type_id' => 2,
                    'nom' => 'Théorèmes de point fixe. Exemples et applications.',
                    'rapport' => '
Il faut préparer des contre-exemples pour illustrer la nécessité des hypothèses des théorèmes énoncés .

Les applications aux équations différentielles sont importantes. Répétons que la maîtrise du théorème de Cauchy-Lipschitz est attendue.

Pour l\'analyse de convergence des méthodes de point fixe, les candidats ne font pas suffisamment le lien entre le caractère localement contractant de l\'opérateur itéré et la valeur de la différentielle au point fixe. La méthode de Newton, interprétée comme une méthode de point fixe, fournit un exemple où cette différentielle est nulle, la vitesse de convergence étant alors de type quadratique. L\'étude de méthodes itératives de résolution de systèmes linéraires conduit à relier ce caractère contractant à la notion de rayon spectral.

Pour les candidats solides, il est envisageable d\'admettre le théorème de point fixe de Brouwer et d\'en développer quelques conséquences comme le théorème de Perron-Frobenius.
',
                    'options' => 15,
                ),
                100 => 
                array (
                    'id' => 601,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 207,
                    'type_id' => 2,
                    'nom' => 'Prolongement de fonctions. Exemples et applications.',
                    'rapport' => '
Il ne faut pas hésiter à commencer par des exemples très simples tel que le prolongement en la fonction $x \\longmapsto \\frac{\\sin(x)}{x}$. Les candidats exploitent rarement toutes les potentialités de cette leçon très riche. Le jury se réjouirait aussi que les candidats abordent les notions de solution maximale pour les équations différentielles ordinaires et maîtrisent le théorème de sortie des compacts.

Le prolongement analytique relève bien sûr de cette leçon ainsi que le  prolongement de fonctions $C^infty$ sur un segment en fonctions de la même classe, le théorème de Tietze sur l\'extension des fonctions continues définies sur un sous-ensemble fermé d\'un espace métrique et la transformation de Fourier sur $L^2$.

En ce qui concerne le théorème d\'Hahn-Banach, le candidat n\'en donnera la version en dimension infinie que s\'il peut s\'aventurer sans dommage sur le terrain délicat et très souvent mal maîtrisé du lemme de Zorn. Il vaut mieux disposer d\'applications pertinentes autres que des résultats classiques abstraits sur les duaux topologiques.
',
                    'options' => 7,
                ),
                101 => 
                array (
                    'id' => 602,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 208,
                    'type_id' => 2,
                    'nom' => 'Espaces vectoriels normés, applications linéaires continues.Exemples.',
                    'rapport' => '
La justification de la compacité de la boule unité en dimension finie doit être donnée. Le théorème d\'équivalence des normes en dimension finie, ou le caractère fermé de tout sous-espace de dimension finie d\'un espace normé, sont des résultats fondamentaux à propos desquels les candidats doivent se
garder des cercles vicieux.

Une telle leçon doit bien sûr contenir beaucoup d\'illustrations et d\'exemples. Lors du choix de ceux-ci (le jury n\'attend pas une liste encyclopédique), le candidat veillera à ne pas mentionner des exemples pour lequel il n\'a aucune idée sur leur pertinence et à ne pas se lancer dans des développements trop sophistiqués.

L\'analyse des constantes de stabilité pour l\'interpolation de Lagrange fournit un exemple non trivial et peu présenté.
',
                    'options' => 15,
                ),
                102 => 
                array (
                    'id' => 603,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 209,
                    'type_id' => 2,
                    'nom' => 'Approximation d\'une fonction par des polynômes et des polynômes trigonométriques. Exemples et applications.',
                    'rapport' => '
Cette leçon comporte un certain nombre de classiques. Les polynômes d\'interpolation de Lagrange, les polynômes de Bernstein sont des classiques tout comme le théorème général de Stone-Weierstrass.

En ce qui concerne le théorème de Weierstrass par les polynômes de Bernstein, un candidat plus ambitieux pourra donner une estimation de la vitesse de convergence (avec le module de continuité), et éventuellement en montrer l\'optimalité. Il n\'est pas absurde de voir la formule de Taylor comme une approximation locale d\'une fonction par des polynômes. Comme la leçon 202, elle permet aux candidats plus ambitieux d\'aller jusqu\'à la résolution d\'équations aux dérivées partielles (ondes, chaleur, Schrödinger) par séries de Fourier.
',
                    'options' => 7,
                ),
                103 => 
                array (
                    'id' => 604,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 213,
                    'type_id' => 2,
                    'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                    'rapport' => '
Il est important de faire la différence entre base algébrique et base hilbertienne. De plus, la formule de la projection orthogonale sur un sous espace de dimension finie d\'un espace de Hilbert doit absolument être connue de même que l\'interprétation géométrique de la méthode de Gramm-Schmidt. Il faut connaître quelques critères simples pour qu\'une famille orthogonale forme une base hilbertienne et illustrer la leçon par des exemples de bases hilbertiennes (polynômes orthogonaux, séries de Fourier, ... ).
Le théorème de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d\'un espace de Hilbert $H$ est régulièrement mentionné. Les candidats doivent s\'intéresser au sens des formules $x  = \\sum_{n\\ge 0} (x |e_n) e_n$ et $||x||^2 = \\sum_{n \\ge 0} (x|e_n)^2$ en précisant les hypothèses sur la famille $(e_n)_{n \\in \\mathbb{N})$ en justifiant la convergence.

La notion d\'adjoint d\'un opérateur continu peut illustrer agréablement cette leçon.

Pour des candidats solides, le programme permet d\'aborder la résolution, et l\'approximation, de problèmes aux limites en dimension 1 par des arguments exploitant la formulation variationnelle de ces équations. Plus généralement, l\'optimisation de fonctionnelles convexes sur les espaces de Hilbert devrait être plus souvent explorée.

Enfin, pour les plus valeureux, le théorème spectral pour les opérateurs auto-adjoints compacts peut être abordé.
',
                    'options' => 7,
                ),
                104 => 
                array (
                    'id' => 605,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 214,
                    'type_id' => 2,
                    'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications.',
                    'rapport' => '
Il s\'agit d\'une belle leçon qui exige une bonne maîtrise du calcul différentiel. Même si le candidat ne propose pas ces thèmes en développement, on est en droit d\'attendre de lui des idées de démonstration de ces deux théorèmes fondamentaux. Il est indispensable de savoir mettre en pratique le théorème des fonctions implicites au moins dans le cas de deux variables réelles. On attend des applications en géométrie différentielle (notamment dans la formulation des multiplicateurs de Lagrange). Rappelons que les sous-variétés sont au programme.
',
                    'options' => 15,
                ),
                105 => 
                array (
                    'id' => 606,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 215,
                    'type_id' => 2,
                    'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                    'rapport' => '
Cette leçon requiert une bonne maîtrise de la notion de différentielle première et de son lien avec les dérivés partielles. Une bonne maîtrise du théorème de différentiation composée est attendue. L\'énoncé doit être connu et compris ; il faut pouvoir l\'appliquer dans des situations simples. Signalons aussi que cette application pose souvent problème lorsque l\'une des fonctions en jeu est une fonction réelle de variable réelle, comme lorsque que l\'on calcule la différentielle de l\'application $x \\longmapsto ||x||$ pour la norme euclidienne sur $\\mathbb{R}^n$.

La notion de différentielle seconde est attendue au moins pour les fonctions de classe applications classiques quant à l\'existence d\'extremums locaux.
',
                    'options' => 15,
                ),
                106 => 
                array (
                    'id' => 607,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 217,
                    'type_id' => 2,
                    'nom' => 'Sous-variétés de $R^n$. Exemples.',
                    'rapport' => '
Cette leçon n\'a pas eu beaucoup de succès, c\'est bien dommage. Elle ne saurait être réduite à un cours de géométrie différentielle abstraite ; ce serait un contresens. Le jury attend une leçon concrète, montrant une compréhension géométrique locale. Aucune notion globale n\'est exigible, ni de notion de variété abstraite. Le candidat doit pouvoir être capable de donner plusieurs représentations locales (paramétriques, équations, ... ) et d\'illustrer la notion d\'espace tangent sur des exemples classiques. Le jury invite les candidats à réfléchir à la pertinence de l\'introduction de la notion de sous-variétés. L\'illustration de la leçon par des dessins est la bienvenue.

Le théorème des extremums liés devient assez transparent lorsqu\'on le traite par les sous-variétés. Les groupes classiques donnent des exemples utiles de sous-variétés.
',
                    'options' => 7,
                ),
                107 => 
                array (
                    'id' => 608,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 218,
                    'type_id' => 2,
                    'nom' => 'Applications des formules de Taylor.',
                    'rapport' => '
Il faut connaître les formules de Taylor des polynômes et certains développements très classiques. En général, le développement de Taylor d\'une fonction comprend un terme de reste qu\'il est crucial de savoir analyser. Le candidat doit pouvoir justifier les différentes formules de Taylor proposées ainsi que leur intérêt. Le jury s\'inquiète des trop nombreux candidats qui ne savent pas expliquer clairement ce que signifient les notations $o$ ou $O$ qu\'ils utilisent. 

De plus la différence entre l\'existence d\'un développement limité à l\'ordre deux et l\'existence de dérivée seconde doit être connue.

Il y a de très nombreuses applications en géométrie et probabilités (par exemple le théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités $||f^{(k)}|| \\le 2^{k(n-k)/2} ||f||^{1 - k/n} ||f^{(n)}||^{k/n}$ (lorsque $f$ et sa dérivée $n$-ième sont bornées). On soignera particulièrement le choix des développements.
',
                    'options' => 15,
                ),
                108 => 
                array (
                    'id' => 609,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 219,
                    'type_id' => 2,
                    'nom' => 'Extremums : existence, caractérisation, recherche. Exemples et applications.',
                    'rapport' => '
Il faut bien faire la distinction entre propriétés locales (caractérisation d\'un extremum) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local est également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon.

L\'étude des algorithmes de recherche d\'extremums y a toute sa place : méthode de gradient, preuve de la convergence de la méthode de gradient à pas optimal, ... 

Le cas particulier des fonctionnelles sur $\\mathbb{R}^n$ de la forme $\\frac{1}{2} (Ax|x) - (b|x)$, où $A$ est une matrice symétrique définie positive, devrait être totalement maîtrisé. Les candidats devraient aussi être amenés à évoquer les problèmes de type moindres carrés et les équations normales qui y sont attachées. Enfin, les problèmes de minimisation sous contrainte amènent à faire le lien avec les extremums liés, la notion de multiplicateur de Lagrange et, là encore, des algorithmes peuvent être présentés et analysés.
',
                    'options' => 15,
                ),
                109 => 
                array (
                    'id' => 610,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 220,
                    'type_id' => 2,
                'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'études des solutions en dimension $1$ et $2$.',
                    'rapport' => '
C\'est l\'occasion de rappeler une nouvelle fois que le jury s\'alarme des nombreux défauts de maîtrise du théorème de Cauchy-Lipschitz. Il est regrettable de voir des candidats ne connaître qu\'un énoncé pour les fonctions globalement lipschitziennes ou plus grave, mélanger les conditions sur la variable de temps et d\'espace. La notion de solution maximale et le théorème de sortie de tout compact sont nécessaires. Bien évidemment, le jury attend des exemples d\'équations différentielles non linéaires.

Le lemme de Gronwall semble trouver toute sa place dans cette leçon mais est curieusement rarement énoncé. L\'utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en oeuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées. 

Pour les équations autonomes, la notion de point d\'équilibre permet des illustrations de bon goût comme par exemple les petites oscillations du pendule. Trop peu de candidats pensent à tracer et discuter des portraits de phase.

Enfin, il n\'est pas malvenu d\'évoquer les problématiques de l\'approximation numérique dans cette leçon par exemple autour de la notion de problèmes raides et de la conception de schémas implicites pour autant que la candidat ait une maîtrise convenable de ces questions.
',
                    'options' => 15,
                ),
                110 => 
                array (
                    'id' => 611,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 221,
                    'type_id' => 2,
                    'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                    'rapport' => '
On attend d\'un candidat qu\'il sache déterminer rigoureusement la dimension de l\'espace vectoriel des solutions (dans le cas de la dimension finie, bien sûr).

Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. L\'utilisation des exponentielles de matrices doit pouvoir s\'expliquer. Dans le cas général, certains candidats évoquent les généralisations de l\'exponentielle (résolvante) via les intégrales itérées. 

Les problématiques de stabilité des solutions et le lien avec l\'analyse spectrale devraient être exploitées.
',
                    'options' => 15,
                ),
                111 => 
                array (
                    'id' => 612,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 222,
                    'type_id' => 2,
                    'nom' => 'Exemples d\'équations aux dérivées partielles linéaires.',
                    'rapport' => '
Cette nouvelle leçon peut être abordée en faisant appel à des techniques variées et de nombreux développements pertinents peuvent être construits en exploitant judicieusement les éléments les plus classiques du programme. Le candidat ne doit pas hésiter à donner des exemples très simples (par exemple les équations de transport).

Les techniques d\'équations différentielles s\'expriment par exemple pour traiter $\\lambda u - u\'\' = f$ avec des conditions de Dirichlet en $x = 0$, $x =1$ ou pour analyser l\'équation de transport par la méthode des caractéristiques.

Les séries de Fourier trouvent dans cette leçon une mise en pratique toute désignée pour résoudre l\'équation de la chaleur, de Schrödinger ou des ondes dans le contexte des fonctions périodiques. La transformée de Fourier permet ceci dans le cadre des fonctions sur $\\mathbb{R}^d$.

Le point de vue de l\'approximation numérique donne lieu à des développements originaux, notamment autour de la matrice du laplacien et de l\'analyse de convergence de la méthode des différences finies. 

Des développements plus sophistiqués se placeront sur le terrain de l\'analyse hilbertienne avec le théorème de Lax-Milgram, l\'espace de Sobolev $H_0^1(]0,1[)$ , jusqu\'à la décomposition spectrale des opérateurs compacts, ou encore sur celui des distributions avec l\'étude de solutions élémentaires d\'équations elliptiques.
',
                    'options' => 7,
                ),
                112 => 
                array (
                    'id' => 613,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 223,
                    'type_id' => 2,
                    'nom' => 'Suites numériques. Convergence, valeurs d\'adhérence. Exemples et applications.',
                    'rapport' => '
Le théorème de Bolzano-Weierstrass doit être cité et le candidat doit être capable d\'en donner une démonstration. On attend des candidats qu\'ils parlent des limites inférieure et supérieure d\'une suite réelle (bornée), et qu\'ils en maîtrisent le concept.
',
                    'options' => 15,
                ),
                113 => 
                array (
                    'id' => 614,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 226,
                    'type_id' => 2,
                'nom' => 'Suites vectorielles et réelles définies par une relation de récurrence $u_{n+1} = f(u_n)$. Exemples et applications.',
                    'rapport' => '
Le jury attend d\'autres exemples que la traditionnelle suite récurrente $u_{n+1} = \\sin(u_n)$. Les suites homographiques réelles ou complexes fournissent des exemples intéressants, rarement évoqués.

Cette leçon doit être l\'occasion d\'évoquer les problématiques de convergence d\'algorithmes, d\'approximation de solutions de problèmes linéaires et non linéaires : dichotomie, méthode de Newton, algorithme du gradient, méthode de la puissance, méthodes itératives de résolution de systèmes linéaires, schéma d\'Euler, ...

L\'aspect vectoriel est souvent négligé. Par exemple, le jury attend des  candidats qu\'ils répondent de façon pertinente à la question de la généralisation de l\'algorithme de Newton dans $\\mathbb{R}^2$.
',
                    'options' => 15,
                ),
                114 => 
                array (
                    'id' => 615,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 228,
                    'type_id' => 2,
                    'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples.',
                    'rapport' => '
Cette leçon permet des exposés de niveaux très variés. Les théorèmes de base doivent être maîtrisés et illustrés par des exemples intéressants, par exemple le théorème des valeurs intermédiaires pour la dérivée. Le jury s\'attend à ce que le candidat connaisse et puisse calculer la dérivée des fonctions usuelles. Les candidats doivent disposer d\'un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La stabilité par passage à la limite des notions de continuité et de dérivabilité par passage à la limite doit être comprise par les candidats.

Pour les candidats aguerris, la dérivabilité presque partout des fonctions lipschitziennes relève de cette leçon. Les applications du théorème d\'Ascoli (par exemple les opérateurs intégraux à noyau continu, le théorème de Peano, ... ), sont les bienvenues.

Pour les candidats qui maîtrisent la notion de dérivée au sens des distributions tempérées, l\'étude de la dérivée au sens des distributions de la primitive d\'une fonction intégrable est un résultat intéressant.
',
                    'options' => 7,
                ),
                115 => 
                array (
                    'id' => 616,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 229,
                    'type_id' => 2,
                    'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                    'rapport' => '
Les propriétés de continuité et de dérivabilité à gauche et à droite des fonctions convexes de la variable réelle sont attendues. Il est souhaitable d\'illustrer la présentation de la convexité par des dessins clairs, même si ces dessins ne peuvent remplacer un calcul. On notera que la monotonie concerne (à ce niveau) les fonctions réelles d\'une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de $\\mathbb{R}^n$, qui fournissent de beaux exemples d\'utilisation. 

Pour les candidats solides, la dérivabilité presque partout des fonctions monotones est un résultat remarquable (dont la preuve peut être éventuellement admise). L\'espace vectoriel engendré par les fonctions monotones (les fonctions à variation bornée) relève de cette leçon. 

Pour les candidats aguerris, la dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité et les candidats bien préparés peuvent s\'aventurer utilement dans cette direction.
',
                    'options' => 15,
                ),
                116 => 
                array (
                    'id' => 617,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 230,
                    'type_id' => 2,
                    'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                    'rapport' => '
De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être véritablement hors sujet, cette exposition ne doit pas former l\'essentiel de la matière de la leçon. Le thème central de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, ... ) et leurs applications diverses, comme par exemple des résultats d\'irrationalité, voire de transcendance. Enfin on rappelle que la transformation d\'Abel trouve toute sa place dans cette leçon.
',
                    'options' => 15,
                ),
                117 => 
                array (
                    'id' => 618,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 232,
                    'type_id' => 2,
                'nom' => 'Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                    'rapport' => '
Trop de candidats se limitent au simple cas où $X$ est une variable scalaire. Il serait bon d\'envisager les extensions des méthodes classiques dans le cas vectoriel. Au delà de la méthode de Newton, d\'intéressants développements peuvent s\'intéresser à la résolution de systèmes linéaires, notamment par des méthodes itératives. À propos de la version bidimensionnelle de la méthode de Newton, il convient de comprendre la généralisation en dimension supérieure de la division par la dérivée.
',
                    'options' => 15,
                ),
                118 => 
                array (
                    'id' => 619,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 233,
                    'type_id' => 2,
                    'nom' => 'Analyse numérique matricielle : résolution approchée de systèmes linéaires, recherche de vecteurs propres, exemples.',
                    'rapport' => '
Cette leçon puise une bonne part de son contenu dans le programme complémentaire de l\'oral, commun aux différentes options. Les notions de norme matricielle et de rayon spectral sont bien sûr centrales pour ce sujet où le rôle du conditionnement dans l\'étude de sensibilité des solutions de systèmes linéaires doit être bien identifié. L\'analyse de convergence des méthodes itératives de résolution de systèmes linéaires, en identifiant leurs avantages par rapport aux méthodes directes, trouve naturellement sa  place dans cette leçon, tout comme l\'étude d\'algorithmes de recherche d\'éléments propres, avec la méthode de la puissance (ou la méthode QR) et des applications à des matrices vérifiant les hypothèses des théorèmes de Perron-Frobenius. Le cas particulier des matrices symétriques définies positives doit amener à faire le lien avec les problèmes de minimisation et les méthodes de gradient. On notera d\'ailleurs que de tels développements peuvent aussi être exploités avec bonheur dans la leçon 226.

Les techniques d\'analyse permettent aussi l\'investigation des propriétés spectrales de matrices et la localisation de valeurs propres de matrices (théorème de Gershgörin, suites de Sturm). Le jury encourage les candidats à illustrer leur propos d\'exemples pertinents issus de la théorie de l\'interpolation ou de la résolution approchée de problèmes aux limites, incluant l\'analyse de stabilité de méthodes numériques.
',
                    'options' => 7,
                ),
                119 => 
                array (
                    'id' => 620,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 234,
                    'type_id' => 2,
                    'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                    'rapport' => '

Le jury a apprécié les candidats sachant montrer qu\'avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p \\subset L^q$ si $p \\ge q$). Il est important de pouvoir justifier l\'existence de produits de convolution (exemple $L^1 \\star L^1$ ). Par ailleurs, les espaces associés à la mesure de comptage sur $\\mathbb{N}$ ou $\\mathbb{Z}$ fournissent des exemples pertinents non triviaux à propos desquels des développements peuvent être proposés comme la complétude ou pour les candidats plus solides la description du dual.
',
                    'options' => 7,
                ),
                120 => 
                array (
                    'id' => 621,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 236,
                    'type_id' => 2,
                    'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                    'rapport' => '
Dans cette leçon, il est souhaitable de présenter des utilisations du théorème  des résidus, ainsi que des exemples faisant intervenir les intégrales multiples. On peut aussi penser à l\'utilisation du théorème d\'inversion de Fourier ou du théorème de Plancherel. Le calcul du volume de la boule unité de $\\mathbb{R}^n$ ne devrait pas poser de problèmes insurmontables.
',
                    'options' => 15,
                ),
                121 => 
                array (
                    'id' => 622,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 239,
                    'type_id' => 2,
                    'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications.',
                    'rapport' => '
Cette leçon peut être enrichie par des études et méthodes de comportements asymptotiques. Les différentes transformations classiques (Fourier, Laplace, ...) relèvent aussi de cette leçon.
',
                    'options' => 15,
                ),
                122 => 
                array (
                    'id' => 623,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 240,
                    'type_id' => 2,
                    'nom' => 'Produit de convolution, transformation de Fourier. Applications.',
                    'rapport' => '
Cette leçon nécessite une bonne maîtrise de questions de base telle que la définition du produit de convolution de deux fonctions de $L^1$. En ce qui concerne la transformation de Fourier, elle ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C\'est bien une leçon d\'analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. Le lien entre la régularité de la fonction et la décroissance de sa transformée de Fourier doit être fait, même sous des hypothèses qui ne sont pas minimales. 

La formule d\'inversion de Fourier pour une fonction $L^1$ dont la  transformée de Fourier est aussi $L^1$ ainsi que les inégalités de Young sont attendues ainsi que l\'extension de la transformée de Fourier à l\'espace $L^2$ par Fourier-Plancherel. Des exemples explicites de calcul de transformations de Fourier paraissent nécessaires.

Les candidats solides peuvent aborder ici la résolution de l\'équation de la chaleur, de Schrödinger pour des fonctions assez régulières, ou la détermination des solutions élémentaires du Laplacien ou de l\'opérateur $k^2 - \\frac{ d^2}{dx^2}$.

La transformation de Fourier des distributions tempérées ainsi que la convolution dans le cadre des distributions tempérées trouvent leur place ici mais sont réservées aux candidats aguerris. On peut aussi considérer l\'extension de la transformée de Fourier à la variable complexe, riche d\'applications par exemple dans la direction du théorème de Paley-Wiener.
',
                    'options' => 15,
                ),
                123 => 
                array (
                    'id' => 624,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 241,
                    'type_id' => 2,
                    'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                    'rapport' => '
Une fois les résultats généraux énoncés, on attend du candidat qu\'il évoque les séries de fonctions particulières classiques : séries entières, série de Fourier. On pourra éventuellement s\'intéresser aussi aux séries de Dirichlet.
',
                    'options' => 7,
                ),
                124 => 
                array (
                    'id' => 625,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:23',
                    'annee' => 2015,
                    'numero' => 243,
                    'type_id' => 2,
                    'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications.',
                    'rapport' => '
Le théorème d\'Abel (radial ou sectoriel) trouve toute sa place mais doit être agrémenté d\'exercices pertinents. Il est regrettable de voir beaucoup de candidats qui maîtrisent raisonnablement les classiques du comportement au bord du disque de convergence traiter cette leçon en faisant l\'impasse sur la variable complexe. C\'est se priver de beaux exemples d\'applications ainsi que du théorème de composition, pénible à faire dans le cadre purement analytique et d\'ailleurs très peu abordé. Le jury attend aussi que le candidat puisse donner des arguments justifiant qu\'une série entière en $0$ dont le rayon de convergence est $R$ est développable en série entière en $0$ en un point $z_0$  intérieur au disque de convergence et de minorer le rayon de convergence de cette série.
',
                    'options' => 15,
                ),
                125 => 
                array (
                    'id' => 626,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 244,
                    'type_id' => 2,
                    'nom' => 'Fonctions développables en série entière, fonctions analytiques. Exemples.',
                    'rapport' => '',
                    'options' => 7,
                ),
                126 => 
                array (
                    'id' => 627,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 245,
                    'type_id' => 2,
                    'nom' => 'Fonctions holomorphes sur un ouvert de $C$. Exemples et applications.',
                    'rapport' => '
Les conditions de Cauchy-Riemann doivent être parfaitement connues et l\'interprétation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z) dz$ a un sens précis, qu\'il faut savoir expliquer. Par ailleurs, même si cela ne constitue pas le coeur de la leçon, il faut connaître la définition d\'une fonction méromorphe (l\'ensemble des pôles doit être une partie fermée discrète). 

Pour les candidats aguerris, cette leçon offre beaucoup de possibilités, notamment en lien avec la topologie du plan.
',
                    'options' => 7,
                ),
                127 => 
                array (
                    'id' => 628,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 246,
                    'type_id' => 2,
                    'nom' => 'Séries de Fourier. Exemples et applications.',
                    'rapport' => '
Les différents modes de convergence ( L , Fejer, Dirichlet, ...) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). 

Dans le cas d\'une fonction $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet.

Il est souhaitable que cette leçon ne se réduise pas à un cours abstrait sur les coefficients de Fourier.

La résolution des équations de la chaleur, de Schrödinger et des ondes dans le cadre de fonctions assez régulières peuvent illustrer de manière pertinente cette leçon.
',
                    'options' => 15,
                ),
                128 => 
                array (
                    'id' => 629,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 247,
                    'type_id' => 2,
                    'nom' => 'Exemples de problèmes d\'interversion de limites.',
                    'rapport' => '',
                    'options' => 7,
                ),
                129 => 
                array (
                    'id' => 630,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 249,
                    'type_id' => 2,
                    'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes.',
                    'rapport' => '
La notion d\'indépendance ainsi que les théorèmes de convergence (loi des grands nombres et théorème central limite) doivent être rappelés. La loi binômiale doit être évoquée et le lien avec la leçon expliqué. 

Il peut être intéressant de donner une construction explicite d\'une suite de variables aléatoires de Bernoulli indépendantes.

Certains candidats plus aguerris pourront s\'intéresser au comportement asymptotique de marches aléatoires (en utilisant par exemple le lemme de Borel-Cantelli), ou donner des inégalités de grandes déviations.
',
                    'options' => 7,
                ),
                130 => 
                array (
                    'id' => 631,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 253,
                    'type_id' => 2,
                    'nom' => 'Utilisation de la notion de convexité en analyse.',
                    'rapport' => '',
                    'options' => 7,
                ),
                131 => 
                array (
                    'id' => 632,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 254,
                    'type_id' => 2,
                'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées. Dérivation et transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$.',
                    'rapport' => '
Rappelons une fois de plus que les attentes du jury sur ces leçons restent modestes. Elles se placent au niveau de ce qu\'un cours de première année de master sur le sujet peut contenir. Aucune subtilité topologique portant sur l\'espace des distributions tempérées n\'est attendue. Par contre, on attend du candidat qu\'il comprenne le rôle fondamental joué par la dualité dans la définition des opérations sur les distributions tempérées. Il faut aussi savoir faire le lien entre décroissance de la transformée de Fourier et régularité de la fonction.

Le fait que la transformée de Fourier envoie $S(\\mathbb{R}^d)$ dans lui même avec de bonnes estimations des semi-normes doit être compris et la formule d\'inversion de Fourier maîtrisée dans ce cadre. 

Le passage à $S(\\mathbb{R}^d)$ repose sur l\'idée de dualité qui est le coeur de cette leçon. Des exemples de calcul de transformée de Fourier peuvent être données, classiques comme la gaussienne ou $(1+x^2)^{-1}$ et d\'autres liées à la théorie des distributions comme la détermination de la transformée de Fourier d\'une constante.

Cette leçon ne doit pas se réduire à une dissertation abstraite sur le dual topologique d\'un espace de Fréchet séparable. Le candidat doit maîtriser des exemples comme la valeur principale, pouvoir calculer leur dérivée et comprendre ce qu\'est la transformée de Fourier d\'une fonction constante.

Les candidats ambitieux peuvent par exemple déterminer la transformée de Fourier de la valeur principale, la solution fondamentale du laplacien, voire résoudre l\'équation de la chaleur ou de Schrödinger.
',
                    'options' => 7,
                ),
                132 => 
                array (
                    'id' => 633,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 260,
                    'type_id' => 2,
                    'nom' => 'Espérance, variance et moments de variables aléatoires.',
                    'rapport' => '
Le jury attend des candidats qu\'ils donnent la définition des moments centrés, qu\'ils rappellent les implications d\'existence de moments. Les inégalités classiques (de Markov, de Bienaymé-Chebychev, de Jensen et de Cauchy-Schwarz) pourront être données, ainsi que les théorèmes de convergence (loi des grands nombres et théorème central limite).

Le comportement des moyennes pour une suite de variables aléatoires indépendantes et identiquement distribuées n\'admettant pas d\'espérance pourra être étudié.

La notion de fonction génératrice des moments pourra être présentée.
',
                    'options' => 15,
                ),
                133 => 
                array (
                    'id' => 634,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 261,
                    'type_id' => 2,
                    'nom' => 'Fonction caractéristique et transformée de Laplace d\'une variable aléatoire. Exemples et applications.',
                    'rapport' => '
Le jury attend l\'énoncé du théorème de Lévy et son utilisation dans la démonstration du théorème central limite.

Les candidats pourront présenter l\'utilisation de la fonction caractéristique pour le calcul de lois de sommes de variables aléatoires indépendantes et faire le lien entre la régularité de la fonction caractéristique et l\'existence de moments.

Enfin la transformée de Laplace pourra être utilisée pour établir des inégalités de grandes déviations.
',
                    'options' => 7,
                ),
                134 => 
                array (
                    'id' => 635,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 262,
                    'type_id' => 2,
                    'nom' => 'Modes de convergence d\'une suite de variables aléatoires. Exemples et applications.',
                    'rapport' => '
Les implications entre les divers modes de convergence, ainsi que les réciproques partielles doivent être connues. Des contre-exemples aux réciproques sont attendus par le jury.

Les théorèmes de convergence (lois des grands nombres et théorème central limite) doivent être énoncés.

Les candidats plus aguerris pourront présenter le lemme de Slutsky (et son utilisation pour la construction d\'intervalles de confiance).
',
                    'options' => 7,
                ),
                135 => 
                array (
                    'id' => 636,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 263,
                    'type_id' => 2,
                    'nom' => 'Variables aléatoires à densité. Exemples et applications.',
                    'rapport' => '
Le jury attend des candidats qu\'ils rappellent la définition d\'une variable aléatoire à densité et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. 

Le lien entre l\'indépendance et la convolution pourra être étudié.

Les candidats pourront expliquer comment fabriquer n\'importe quelle variable aléatoire à partir d\'une variable uniforme sur $[0,1]$ et l\'intérêt de ce résultat pour la simulation informatique.

Pour aller plus loin, certains candidats pourront aborder la notion de vecteurs gaussiens et son lien avec le théorème central limite.
',
                    'options' => 7,
                ),
                136 => 
                array (
                    'id' => 637,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 264,
                    'type_id' => 2,
                    'nom' => 'Variables aléatoires discrètes. Exemples et applications.',
                    'rapport' => '
Le jury attend des candidats qu\'ils rappellent la définition d\'une variable aléatoire discrète et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. Le lien entre variables aléatoires de Bernoulli, binômiale et de Poisson doit être discuté.

Les techniques spécifiques aux variables discrètes devront être abordées (comme par exemple la caractérisation de la convergence en loi). La notion de fonction génératrice pourra être abordée. 

Pour aller plus loin, les candidats ambitieux pourront étudier les chaînes de Markov à espaces d\'états finis ou dénombrables.
',
                    'options' => 15,
                ),
                137 => 
                array (
                    'id' => 638,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 901,
                    'type_id' => 3,
                    'nom' => 'Structure de données : exemples et applications.',
                    'rapport' => '
Le mot algorithme ne figure pas dans l\'intitulé de cette leçon, même si l\'utilisation des structures de données est évidemment fortement liée à des questions algorithmiques. 

La leçon doit donc être orientée plutôt sur la question du choix d\'une structure de données que d\'un algorithme. Le jury attend du candidat qu\'il présente différents types abstraits de structures de données en donnant quelques exemples de leur usage avant de s\'intéresser au choix de la structure concrète. Le candidat ne peut se limiter à des structures linéaires simples comme des tableaux ou des listes, mais doit présenter également quelques structures plus complexes, reposant par exemple sur des implantations à l\'aide d\'arbres.
',
                    'options' => 8,
                ),
                138 => 
                array (
                    'id' => 639,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 902,
                    'type_id' => 3,
                    'nom' => 'Diviser pour régner : exemples et applications.',
                    'rapport' => '
Cette leçon permet au candidat de proposer différents algorithmes utilisant le paradigme diviser pour régner . Le jury attend du candidat que ces exemples soient variés et touchent des domaines différents. 

Un calcul de complexité ne peut se limiter au cas où la taille du problème est une puissance exacte de 2, ni à une application directe d\'un théorème très général recopié approximativement d\'un ouvrage de la bibliothèque de l\'agrégation.
',
                    'options' => 8,
                ),
                139 => 
                array (
                    'id' => 640,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 903,
                    'type_id' => 3,
                    'nom' => 'Exemples d\'algorithmes de tri. Complexité.',
                    'rapport' => '
Sur un thème aussi classique, le jury attend des candidats la plus grande précision et la plus grande rigueur.

Ainsi, sur l\'exemple du tri rapide, il est attendu du candidat qu\'il sache décrire avec soin l\'algorithme de partition et en prouver la correction et que l\'évaluation des complexités dans le cas le pire et en moyenne soit menée avec rigueur.

On attend également du candidat qu\'il évoque la question du tri en place, des tris stables, ainsi que la représentation en machine des collections triées.

Le jury ne manquera pas de demander au candidat des applications non triviales du tri.
',
                    'options' => 8,
                ),
                140 => 
                array (
                    'id' => 641,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 906,
                    'type_id' => 3,
                    'nom' => 'Programmation dynamique : exemples et applications.',
                    'rapport' => '
Même s\'il s\'agit d\'une leçon d\'exemples et d\'applications, le jury attend des candidats qu\'ils présentent les idées générales de la programmation dynamique et en particulier qu\'ils aient compris le caractère générique de la technique de mémoïsation. Le jury appréciera que les exemples choisis par le candidat couvrent des domaines variés, et ne se limitent pas au calcul de la longueur de la plus grande sous-séquence commune à deux chaînes de caractères.

Le jury ne manquera pas d\'interroger plus particulièrement le candidat sur la question de la correction des algorithmes proposés et sur la question de leur complexité en espace.
',
                    'options' => 8,
                ),
                141 => 
                array (
                    'id' => 642,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 907,
                    'type_id' => 3,
                    'nom' => 'Algorithmique du texte : exemples et applications.',
                    'rapport' => '
Cette leçon devrait permettre au candidat de présenter une grande variété d\'algorithmes et de paradigmes de programmation, et ne devrait pas se limiter au seul problème de la recherche d\'un motif dans un texte, surtout si le candidat ne sait présenter que la méthode naïve. De même, des structures de données plus riches que les tableaux de caractères peuvent montrer leur utilité dans certains algorithmes, qu\'il s\'agisse d\'automates ou d\'arbres par exemple. 

Cependant, cette leçon ne doit pas être confondue avec la 909 : Langages rationnels. Exemples et applications ni avec la 910 : Langages algébriques. Exemples et applications .

La compression de texte peut faire partie de cette leçon si les algorithmes présentés contiennent effectivement des opérations comme les comparaisons de chaînes : la compression LZW, par exemple, ressortit davantage à cette leçon que la compression de Huffman.
',
                    'options' => 8,
                ),
                142 => 
                array (
                    'id' => 643,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 909,
                    'type_id' => 3,
                    'nom' => 'Langages rationnels. Exemples et applications.',
                    'rapport' => '
Des applications dans le domaine de la compilation entrent naturellement dans le cadre de ces leçons.
',
                    'options' => 8,
                ),
                143 => 
                array (
                    'id' => 644,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 910,
                    'type_id' => 3,
                    'nom' => 'Langages algébriques. Exemples et applications.',
                    'rapport' => '',
                    'options' => 8,
                ),
                144 => 
                array (
                    'id' => 645,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 912,
                    'type_id' => 3,
                    'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                145 => 
                array (
                    'id' => 646,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 913,
                    'type_id' => 3,
                    'nom' => 'Machines de Turing. Applications.',
                    'rapport' => '',
                    'options' => 8,
                ),
                146 => 
                array (
                    'id' => 647,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 914,
                    'type_id' => 3,
                    'nom' => 'Décidabilité et indécidabilité. Exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                147 => 
                array (
                    'id' => 648,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 915,
                    'type_id' => 3,
                    'nom' => 'Classes de complexité : exemples.',
                    'rapport' => '
Le jury attend que le candidat aborde à la fois la complexité en temps et en espace. Il faut naturellement exhiber des exemples de problèmes appartenant aux classes de complexité introduites, et montrer les relations d\'inclusion existantes entre ces classes.

Le jury s\'attend à ce que le caractère strict ou non de ces inclusions soit abordé, en particulier le candidat doit être capable de montrer la non-appartenance de certains problèmes à certaines classes.

Parler de décidabilité dans cette leçon serait hors sujet.
',
                    'options' => 8,
                ),
                148 => 
                array (
                    'id' => 649,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 916,
                    'type_id' => 3,
                    'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications.',
                    'rapport' => '
Le jury attend des candidats qu\'ils abordent les questions de la complexité de la satisfiabilité. 

Pour autant, les applications ne sauraient se réduire à la réduction de problèmes NP-complets à SAT. 

Une partie significative du plan doit être consacrée à la représentation des formules et à leurs formes
normales.
',
                    'options' => 8,
                ),
                149 => 
                array (
                    'id' => 650,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 917,
                    'type_id' => 3,
                    'nom' => 'Logique du premier ordre : syntaxe et sémantique.',
                    'rapport' => '
La question de la syntaxe dépasse celle de la définition des termes et des formules. Elle comprend aussi celle des règles de la démonstration.

Le jury attend donc du candidat qu\'il présente au moins un système de preuve et les liens entre syntaxe et sémantique, en développant en particulier les questions de correction et complétude.
',
                    'options' => 8,
                ),
                150 => 
                array (
                    'id' => 651,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 918,
                    'type_id' => 3,
                    'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                151 => 
                array (
                    'id' => 652,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 919,
                    'type_id' => 3,
                    'nom' => 'Unification : algorithmes et applications.',
                    'rapport' => '',
                    'options' => 8,
                ),
                152 => 
                array (
                    'id' => 653,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 920,
                    'type_id' => 3,
                    'nom' => 'Réécriture et formes normales. Exemples.',
                    'rapport' => '
Au-delà des propriétés standards (terminaison, confluence) des systèmes de réécriture, le jury attend notamment du candidat qu\'il présente des exemples sur lesquels l\'étude des formes normales est pertinente dans des domaines variés : calcul formel, logique, etc.

Un candidat ne doit pas s\'étonner que le jury lui demande de calculer des paires critiques sur un exemple concret.

Lorsqu\'un résultat classique comme le lemme de Newman est évoqué, le jury attend du candidat qu\'il sache le démontrer.
',
                    'options' => 8,
                ),
                153 => 
                array (
                    'id' => 654,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 921,
                    'type_id' => 3,
                    'nom' => 'Algorithmes de recherche et structures de données associées.',
                    'rapport' => '',
                    'options' => 8,
                ),
                154 => 
                array (
                    'id' => 655,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 922,
                    'type_id' => 3,
                    'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                155 => 
                array (
                    'id' => 656,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 923,
                    'type_id' => 3,
                    'nom' => 'Analyses lexicale et syntaxique : applications.',
                    'rapport' => '',
                    'options' => 8,
                ),
                156 => 
                array (
                    'id' => 657,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 924,
                    'type_id' => 3,
                    'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                157 => 
                array (
                    'id' => 658,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 925,
                    'type_id' => 3,
                    'nom' => 'Graphes : représentations et algorithmes.',
                    'rapport' => '',
                    'options' => 8,
                ),
                158 => 
                array (
                    'id' => 659,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:37:28',
                    'annee' => 2015,
                    'numero' => 926,
                    'type_id' => 3,
                    'nom' => 'Analyse des algorithmes : complexité. Exemples.',
                    'rapport' => '',
                    'options' => 8,
                ),
                159 => 
                array (
                    'id' => 660,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 927,
                    'type_id' => 3,
                    'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                    'rapport' => '
Le jury attend du candidat qu\'il traite des exemples d\'algorithmes récursifs et des exemples d\'algorithmes itératifs.

En particulier, le candidat doit présenter des exemples mettant en évidence l\'intérêt de la notion d\'invariant pour la correction partielle et celle de variant pour la terminaison des segments itératifs. 

Une formalisation comme la logique de Hoare pourra utilement être introduite dans cette leçon, à condition toutefois que le candidat en maîtrise le langage.
',
                    'options' => 8,
                ),
                160 => 
                array (
                    'id' => 661,
                    'created_at' => '2016-03-14 12:37:28',
                    'updated_at' => '2016-03-14 12:46:24',
                    'annee' => 2015,
                    'numero' => 928,
                    'type_id' => 3,
                    'nom' => 'Problème NP-complets : exemples de réductions.',
                    'rapport' => '
L\'objectif ne doit pas être de dresser un catalogue le plus exhaustif possible ; en revanche, pour chaque exemple, il est attendu que le candidat puisse au moins expliquer clairement le problème considéré, et indiquer de quel autre problème une réduction permet de prouver sa NP-complétude.

Les exemples de réduction seront autant que possible choisis dans des domaines variés : graphes, arithmétique, logique, etc. Un exemple de problème NP-complet dans sa généralité qui devient P si on contraint davantage les hypothèses pourra être présenté, ou encore un algorithme P approximant un problème NP-complet.

Si les dessins sont les bienvenus lors du développement, le jury attend une définition claire et concise de la fonction associant, à toute instance du premier problème, une instance du second ainsi que la preuve rigoureuse que cette fonction permet la réduction choisie.
',
                    'options' => 8,
                ),
                161 => 
                array (
                    'id' => 662,
                    'created_at' => '2016-06-17 20:21:34',
                    'updated_at' => '2016-06-17 20:21:34',
                    'annee' => 2015,
                    'numero' => 224,
                    'type_id' => 2,
                    'nom' => ' Exemples de développements asymptotiques de suites et de fonctions. ',
                    'rapport' => '',
                    'options' => 15,
                ),
                162 => 
                array (
                    'id' => 663,
                    'created_at' => '2016-06-17 20:24:26',
                    'updated_at' => '2017-02-07 14:36:24',
                    'annee' => 2016,
                    'numero' => 101,
                    'type_id' => 1,
                    'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                'rapport' => 'Dans cette leçon, il faut bien dominer les deux approches de l\'action de groupe : l\'approche naturelle et l\'approche via le morphisme du groupe agissant vers le groupe des permutations de l\'ensemble sur lequel il agit. La formule des classes et ses applications immédiates sont incontournables. Des exemples de natures différentes doivent être présentés : actions sur un ensemble fini, sur un espace vectoriel (en particulier les représentations), sur un ensemble de matrices, sur des groupes ou des anneaux. Les exemples issus de la géométrie ne manquent pas (groupes d’isométries d’un solide).

S’ils le désirent, les candidats peuvent aller plus loin en décrivant les actions naturelles de $PGL(  2, F_q)$
sur la droite projective qui donnent des injections intéressantes pour $q=2,3$ et peuvent plus généralement en petit cardinal donner lieu à des isomorphismes de groupes. En notant que l’injection du groupe de permutations dans le groupe linéaire par les matrices de permutations donne lieu à des représentations, ils pourront facilement en déterminer le caractère.',
                    'options' => 7,
                ),
                163 => 
                array (
                    'id' => 664,
                    'created_at' => '2016-06-17 20:29:09',
                    'updated_at' => '2017-02-07 14:38:07',
                    'annee' => 2016,
                    'numero' => 102,
                    'type_id' => 1,
                    'nom' => 'Groupe des nombres complexes de module $1$. Sous-groupes des racines de l\'unité. Applications.',
                'rapport' => 'Il ne faut pas uniquement aborder cette leçon de façon élémentaire sans réellement expliquer où et comment les nombres complexes de modules 1 et les racines de l’unité apparaissent dans divers domaines des mathématiques (exponentielle complexe et ses applications, polynômes cyclotomiques, spectre de matrices remarquables, théorie des représentations). Il ne faut pas non plus oublier la partie « groupe » de la leçon : on pourra s’intéresser au relèvement du groupe unité au groupe additif des réels et aux propriétés qui en résultent. De même les sous-groupes finis de $S^1$ sont intéressants à considérer dans cette leçon.

On pourra aussi s’intéresser aux groupes des nombres complexes de $Q[i]$, et les racines de l’unité qui y appartiennent ; tout comme aux sous-groupes compacts de $C^*$ .',
                    'options' => 7,
                ),
                164 => 
                array (
                    'id' => 665,
                    'created_at' => '2016-06-17 20:29:55',
                    'updated_at' => '2017-02-07 14:38:53',
                    'annee' => 2016,
                    'numero' => 103,
                    'type_id' => 1,
                    'nom' => 'Exemples et applications des notions de sous-groupe distingué et de groupe quotient.',
                'rapport' => 'Dans cette leçon, il faut non seulement évoquer les notions de groupe quotient, de sous-groupe dérivé et de groupe simple mais surtout savoir les utiliser et en expliquer l’intérêt. On pourra utiliser des exemples issus de la géométrie, de l’arithmétique, de l’algèbre linéaire (utilisation d’espaces vectoriels quotients par exemple). La notion de produit semi-direct n’est plus au programme ; mais, lorsqu’elle est utilisée, il faut savoir la définir proprement et savoir reconnaître des situations simples où de tels produits apparaissent (le groupe diédral $D_n$ par exemple).

S’ils le désirent, les candidats peuvent poursuivre en illustrant ces notions à l’aide d’une table de caractères et décrire le treillis des sous-groupes distingués, ainsi que l’indice du sous-groupe dérivé, d’un groupe fini à l’aide de cette table.',
                    'options' => 7,
                ),
                165 => 
                array (
                    'id' => 666,
                    'created_at' => '2016-06-17 20:30:48',
                    'updated_at' => '2017-02-07 14:41:18',
                    'annee' => 2016,
                    'numero' => 104,
                    'type_id' => 1,
                    'nom' => 'Groupes finis. Exemples et applications.',
                'rapport' => 'Dans cette leçon il faut savoir manipuler correctement les éléments de différentes structures usuelles ($Z/nZ$, $\\mathfrak{S}_n$, etc.) comme par exemple, en proposer un générateur ou une famille de générateurs, savoir calculer un produit de deux permutations, savoir décomposer une permutation en produit de cycles à supports disjoints. Il est important que la notion d’ordre d’un élément soit mentionnée et comprise dans des cas simples. Le théorème de structure des groupes abéliens finis doit être connu. 

Les exemples doivent figurer en bonne place dans cette leçon. Les groupes d’automorphismes fournissent des exemples très naturels dans cette leçon. On peut par exemple étudier les groupes de symétries $\\mathfrak{A}_4$, $\\mathfrak{S}_4$, $\\mathfrak{A}_5$ et relier sur ces exemples géométrie et algèbre, les représentations ayant ici toute leur place ; il est utile de connaître les groupes diédraux.

S’ils le désirent, les candidats peuvent ensuite mettre en avant les spécificités de groupes comme le groupe quaternionique, les sous-groupes finis de $SU(2)$ou les groupes $GL_n(F_q)$.',
                    'options' => 15,
                ),
                166 => 
                array (
                    'id' => 667,
                    'created_at' => '2016-06-17 20:31:34',
                    'updated_at' => '2017-02-07 14:42:25',
                    'annee' => 2016,
                    'numero' => 105,
                    'type_id' => 1,
                    'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                'rapport' => 'Parmi les attendus, il faut savoir relier la leçon avec les notions d’orbites et d’actions de groupes. Il faut aussi savoir décomposer une permutation en cycles à supports disjoints, tant sur le plan théorique (preuve du théorème de décomposition), que pratique (sur un exemple). Il est important de savoir déterminer les classes de conjugaisons du groupe symétrique par la décomposition en cycles, d’être capable de donner des systèmes de générateurs.

L’existence du morphisme signature est un résultat non trivial mais ne peut pas constituer, à elle seule, l’objet d’un développement.

Les applications sont nombreuses, il est très naturel de parler des déterminants, des polynômes symétriques ou des fonctions symétriques des racines d’un polynôme.

S’ils le désirent, les candidats peuvent aller plus loin en s’intéressant aux automorphismes du groupe symétrique, à des problèmes de dénombrement ou aux représentations des groupes des permutations.',
                    'options' => 15,
                ),
                167 => 
                array (
                    'id' => 668,
                    'created_at' => '2016-06-17 20:32:39',
                    'updated_at' => '2017-02-07 14:43:47',
                    'annee' => 2016,
                    'numero' => 106,
                    'type_id' => 1,
                'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupe de $GL(E)$. Applications.',
                'rapport' => 'Cette leçon ne doit pas se résumer à un catalogue de résultats épars sur $GL(E)$. Il est important de savoir faire correspondre les sous-groupes du groupe linéaire avec les stabilisateurs de certaines actions naturelles (sur des formes quadratiques, symplectiques, sur des drapeaux, sur une décomposition en somme directe, etc.). On doit présenter des systèmes de générateurs, étudier la topologie et préciser pourquoi le choix du corps de base est important. Les liens avec le pivot de Gauss sont à détailler.

Il faut aussi savoir réaliser $\\mathfrak{S}_n$ dans $GL(n,K)$ et faire le lien entre signature et déterminant. S’ils le désirent, les candidats peuvent aller plus loin en remarquant que la théorie des représentations permet d’illustrer l’importance de $GL_n(C)$ et de son sous-groupe unitaire.',
                    'options' => 15,
                ),
                168 => 
                array (
                    'id' => 669,
                    'created_at' => '2016-06-17 20:33:27',
                    'updated_at' => '2017-02-07 14:45:12',
                    'annee' => 2016,
                    'numero' => 107,
                    'type_id' => 1,
                    'nom' => 'Représentations et caractères d\'un groupe fini sur un $C$-espace vectoriel.',
                    'rapport' => 'Il s’agit d’une leçon où théorie et exemples doivent apparaître. D’une part, il est indispensable de savoir dresser une table de caractères pour des petits groupes, et d’autre part, il faut savoir tirer des informations sur le groupe à partir de sa table de caractères, et être capable de trouver la table de caractères de certains sous-groupes. Les représentations peuvent provenir d’actions de groupes sur des ensembles finis, de groupes d’isométries, d’isomorphismes exceptionnels entre groupes de petit cardinal. Inversement, on peut chercher à interpréter des représentations de façon géométrique, mais il faut avoir conscience qu’une table de caractères provient généralement de représentations complexes à priori non réelles. La présentation du lemme de Schur est importante et ses applications doivent être parfaitement maîtrisées.

S’ils le désirent, les candidats peuvent s’aventurer dans la construction de l’icosaèdre à partir de la table de caractères de $\\mathfrak{A}_5$ en utilisant l’indice de Schur (moyenne des caractères sur les carrés des éléments du groupe).',
                    'options' => 7,
                ),
                169 => 
                array (
                    'id' => 670,
                    'created_at' => '2016-06-17 20:34:06',
                    'updated_at' => '2017-02-07 14:45:41',
                    'annee' => 2016,
                    'numero' => 108,
                    'type_id' => 1,
                    'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                    'rapport' => ' C’est une leçon qui doit être illustrée par des exemples très variés en relation avec les groupes de permutations et les groupes linéaires ou de leurs sous-groupes. La connaissance de parties génératrices s’avère très utile dans l’analyse des morphismes de groupes ou pour montrer la connexité de certains groupes.

Tout comme dans la leçon 106, la présentation du pivot de Gauss et de ses applications est envisageable.',
                    'options' => 15,
                ),
                170 => 
                array (
                    'id' => 671,
                    'created_at' => '2016-06-17 20:34:48',
                    'updated_at' => '2016-06-17 20:34:48',
                    'annee' => 2016,
                    'numero' => 109,
                    'type_id' => 1,
                    'nom' => 'Représentations de groupes finis de petit cardinal.',
                    'rapport' => '',
                    'options' => 7,
                ),
                171 => 
                array (
                    'id' => 672,
                    'created_at' => '2016-06-17 20:35:20',
                    'updated_at' => '2017-02-07 14:46:59',
                    'annee' => 2016,
                    'numero' => 110,
                    'type_id' => 1,
                    'nom' => 'Caractères d\'un groupe abélien fini et transformée de Fourier discrète. Applications',
                    'rapport' => 'Le théorème de structure des groupes abéliens finis a une place de choix dans cette leçon. On pourra  en profiter pour montrer l’utilisation de la dualité dans ce contexte. Comme application, la cyclicité du groupe multiplicatif d’un corps fini est tout à fait adaptée. D’ailleurs, des exemples de caractères, additifs, ou multiplicatifs dans le cadre des corps finis, sont les bienvenus. S’ils le désirent, les candidats peuvent s’intéresser aux sommes de Gauss.

L’algèbre du groupe est un objet intéressant, surtout sur le corps des complexes, où elle peut être munie d’une forme hermitienne. On peut l’introduire comme une algèbre de fonctions, munie d’un produit de convolution, mais il est aussi agréable de la voir comme une algèbre qui « prolonge » la multiplication du groupe.

La transformée de Fourier discrète pourra être vue comme son analogue analytique, avec ses formules d’inversion, sa formule de Plancherel, mais dans une version affranchie des problèmes de convergence, incontournables en analyse de Fourier.

On pourra y introduire la transformée de Fourier rapide sur un groupe abélien d’ordre une puissance de 2 ainsi que des applications à la multiplication d’entiers, de polynômes et éventuellement au décodage de codes via la transformée de Hadamard.',
                    'options' => 7,
                ),
                172 => 
                array (
                    'id' => 673,
                    'created_at' => '2016-06-17 20:36:01',
                    'updated_at' => '2017-02-07 14:48:12',
                    'annee' => 2016,
                    'numero' => 120,
                    'type_id' => 1,
                    'nom' => 'Anneaux $Z/nZ$. Applications',
                    'rapport' => 'Dans cette leçon, l’entier n n’est pas forcément un nombre premier. Il serait bon de connaître les idéaux de $Z/nZ$ et, plus généralement, les morphismes de groupes de $Z/nZ$ dans $Z/mZ$. Il est nécessaire de bien maîtriser le lemme chinois et sa réciproque. S’ils le désirent, les candidats peuvent poursuivre en donnant une généralisation du lemme chinois lorsque deux éléments ne sont pas premiers entre eux, ceci en faisant apparaître le pgcd et le ppcm de ces éléments. Il faut bien sûr savoir appliquer le lemme chinois à l’étude du groupe des inversibles, et ainsi, retrouver la multiplicativité de l’indicatrice d’Euler. Toujours dans le cadre du lemme chinois, il est bon de
distinguer clairement les propriétés de groupes additifs et d’anneaux, de connaître les automorphismes, les nilpotents et les idempotents.

Enfin, il est indispensable de présenter quelques applications arithmétiques des propriétés des anneaux $Z/nZ$, telles que l’étude de quelques équations diophantiennes bien choisies. De même, les applications cryptographiques telles que l’algorithme RSA sont naturelles dans cette leçon. 

S’ils le désirent, les candidats peuvent aller plus loin en s’intéressant au calcul effectif des racines carrées dans $Z/nZ$.',
                    'options' => 15,
                ),
                173 => 
                array (
                    'id' => 674,
                    'created_at' => '2016-06-17 20:36:35',
                    'updated_at' => '2017-02-07 14:48:49',
                    'annee' => 2016,
                    'numero' => 121,
                    'type_id' => 1,
                    'nom' => 'Nombres premiers. Applications.',
                    'rapport' => 'Le sujet de cette leçon est très vaste. Aussi les choix devront être clairement motivés. La réduction modulo p n’est pas hors-sujet et constitue un outil puissant pour résoudre des problèmes arithmétiques simples. La répartition des nombres premiers est un résultat historique important qu’il faudrait citer. Sa démonstration n’est bien sûr pas exigible au niveau de l’agrégation. 

Quelques résultats sur les corps finis et leur géométrie sont les bienvenus, ainsi que des applications en
cryptographie.',
                    'options' => 15,
                ),
                174 => 
                array (
                    'id' => 675,
                    'created_at' => '2016-06-17 20:37:09',
                    'updated_at' => '2017-02-07 14:49:41',
                    'annee' => 2016,
                    'numero' => 122,
                    'type_id' => 1,
                    'nom' => 'Anneaux principaux. Exemples et applications.',
                'rapport' => 'Cette leçon n’est pas uniquement théorique, Il est possible de présenter des exemples d’anneaux principaux classiques autres que $Z$ et $K[X]$ (décimaux, entiers de Gauss ou d’Eisenstein), accompagnés d’une description de leurs irréductibles. Les applications en algèbre linéaire ne manquent pas et doivent être mentionnées. Par exemple, les notions de polynôme minimal sont très naturelles parmi les applications. Les anneaux euclidiens représentent une classe d’anneaux principaux importante et l’algorithme d’Euclide a toute sa place dans cette leçon pour effectuer des calculs.

S’ils le désirent, les candidats peuvent aller plus loin en s’intéressant à l’étude des réseaux, à des exemples d’anneaux non principaux, mais aussi à des exemples d’équations diophantiennes résolues à l’aide d’anneaux principaux. À ce sujet, il sera fondamental de savoir déterminer les unités d’un anneau, et leur rôle au moment de la décomposition en facteurs premiers. De même, le calcul effectif des facteurs invariants de matrices à coefficients dans certains anneaux peut être fait.',
                    'options' => 7,
                ),
                175 => 
                array (
                    'id' => 676,
                    'created_at' => '2016-06-17 20:37:42',
                    'updated_at' => '2017-02-07 14:50:42',
                    'annee' => 2016,
                    'numero' => 123,
                    'type_id' => 1,
                    'nom' => 'Corps finis. Applications.',
                'rapport' => 'Une construction des corps finis doit être connue et une bonne maîtrise des calculs dans les corps finis est indispensable. Les injections des divers $F_q$ doivent être connues et les applications des corps finis (y compris pour $F_q$ avec q non premier !) ne doivent pas être oubliées : citons par exemple l’étude de polynômes à coefficients entiers et de leur irréductibilité. Le calcul des degrés des extensions et le théorème de la base télescopique sont incontournables. L’étude des carrés dans un corps fini et la résolution d’équations de degré 2 sont envisageables. 

S’ils le désirent, les candidats peuvent aller plus loin en détaillant des codes correcteurs.',
                    'options' => 15,
                ),
                176 => 
                array (
                    'id' => 677,
                    'created_at' => '2016-06-17 20:38:17',
                    'updated_at' => '2017-02-07 14:53:56',
                    'annee' => 2016,
                    'numero' => 124,
                    'type_id' => 1,
                    'nom' => 'Anneau des séries formelles. Applications.',
                    'rapport' => '',
                    'options' => 7,
                ),
                177 => 
                array (
                    'id' => 678,
                    'created_at' => '2016-06-17 20:38:54',
                    'updated_at' => '2017-02-07 14:54:10',
                    'annee' => 2016,
                    'numero' => 125,
                    'type_id' => 1,
                    'nom' => 'Extensions de corps. Exemples et applications.',
                    'rapport' => 'Le théorème de la base télescopique et ses applications à l’irréductibilité de certains polynômes, ainsi que les corps finis sont incontournables. De même il faut savoir calculer le polynôme minimal d\'un élément algébrique dans des cas simples, notamment pour quelques racines de l’unité. La leçon peut être illustrée par des exemples d’extensions quadratiques et leurs applications en arithmétique, ainsi que par des extensions cyclotomiques.

S’ils le désirent, les candidats peuvent s’aventurer en théorie de Galois.',
                    'options' => 7,
                ),
                178 => 
                array (
                    'id' => 679,
                    'created_at' => '2016-06-17 20:39:25',
                    'updated_at' => '2017-02-07 14:53:39',
                    'annee' => 2016,
                    'numero' => 126,
                    'type_id' => 1,
                    'nom' => 'Exemples d\'équations diophantiennes.',
                'rapport' => 'Dans cette leçon on doit présenter les notions de bases servant à aborder les équations de type $ax + by=d$ (identité de Bezout, lemme de Gauss), les systèmes de congruences, mais aussi bien entendu la méthode de descente de Fermat et l’utilisation de la réduction modulo un nombre premier p. 

La leçon peut aussi dériver vers la notion de factorialité, illustrée par des équations de type Mordell, Pell-Fermat, et même Fermat (pour $n=2$, ou pour les nombres premiers de Sophie Germain).',
                    'options' => 7,
                ),
                179 => 
                array (
                    'id' => 680,
                    'created_at' => '2016-06-17 20:39:56',
                    'updated_at' => '2016-06-17 20:39:56',
                    'annee' => 2016,
                    'numero' => 127,
                    'type_id' => 1,
                    'nom' => 'Droite projective et birapport.',
                    'rapport' => '',
                    'options' => 7,
                ),
                180 => 
                array (
                    'id' => 681,
                    'created_at' => '2016-06-17 20:40:25',
                    'updated_at' => '2016-06-17 20:40:25',
                    'annee' => 2016,
                    'numero' => 140,
                    'type_id' => 1,
                    'nom' => 'Corps des fractions rationnelles à une indéterminée sur un corps commutatif. Applications.',
                    'rapport' => '',
                    'options' => 7,
                ),
                181 => 
                array (
                    'id' => 682,
                    'created_at' => '2016-06-17 20:40:53',
                    'updated_at' => '2017-02-07 14:55:46',
                    'annee' => 2016,
                    'numero' => 141,
                    'type_id' => 1,
                    'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                'rapport' => 'La présentation du bagage théorique permettant de définir corps de rupture, corps de décomposition, ainsi que des illustrations dans différents types de corps (réel, rationnel, corps finis) sont inévitables. Les corps finis peuvent être illustrés par des exemples de polynômes irréductibles de degré 2, 3, 4 sur $F_2$ ou $F_3$ . Il est nécessaire de présenter des critères d’irréductibilité de polynômes et des polynômes minimaux de quelques nombres algébriques.

Il faut savoir qu’il existe des corps algébriquement clos de caractéristique nulle autres que $C$ ; il est bon de savoir montrer que l’ensemble des nombres algébriques sur le corps $Q$ des rationnels est un corps algébriquement clos. Le théorème de la base télescopique, ainsi que les utilisations arithmétiques (utilisation de la divisibilité) que l’on peut en faire dans l’étude de l’irréductibilité des polynômes, est incontournable.',
                    'options' => 15,
                ),
                182 => 
                array (
                    'id' => 683,
                    'created_at' => '2016-06-17 20:41:24',
                    'updated_at' => '2017-02-07 14:58:01',
                    'annee' => 2016,
                    'numero' => 142,
                    'type_id' => 1,
                    'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                'rapport' => 'Cette leçon ne doit pas se concentrer exclusivement sur les aspects formels ou uniquement sur les polynômes symétriques. Les aspects arithmétiques ne doivent pas être négligés. Il faut savoir montrer l’irréductibilité d’un polynôme à plusieurs indéterminées en travaillant sur un anneau de type $A[X]$, où $A$ est factoriel. Le théorème fondamental sur la structure de l’algèbre des polynômes symétriques est vrai sur $Z$. L’algorithme peut être présenté sur un exemple. Les applications aux quadriques, aux relations racines/cœfficients ne doivent pas être délaissées : on peut faire par exemple agir le groupe $GL_n(R)$ sur les polynômes à n indéterminées de degré inférieur à 2. 

S’ils désirent aller plus loin, les candidats peuvent s’aventurer vers la géométrie algébrique et présenter le Nullstellensatz.',
                    'options' => 7,
                ),
                183 => 
                array (
                    'id' => 684,
                    'created_at' => '2016-06-17 20:41:54',
                    'updated_at' => '2016-06-17 20:41:54',
                    'annee' => 2016,
                    'numero' => 143,
                    'type_id' => 1,
                    'nom' => 'Résultant. Applications.',
                    'rapport' => '',
                    'options' => 7,
                ),
                184 => 
                array (
                    'id' => 685,
                    'created_at' => '2016-06-17 20:42:21',
                    'updated_at' => '2017-02-07 14:58:43',
                    'annee' => 2016,
                    'numero' => 144,
                    'type_id' => 1,
                    'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Exemples et applications.',
                'rapport' => 'Dans cette leçon on peut présenter des méthodes de résolutions, de la théorie des corps, des notions de topologie (continuité des racines) ou même des formes quadratiques. Il peut être pertinent d’introduire la notion de polynôme scindé, de citer le théorème de d’Alembert-Gauss et des applications des racines (valeurs propres, etc.). Notons le lien solide entre la recherche des racines d’un polynôme et la réduction des matrices ; les valeurs propres de la matrice compagnon d’un polynôme permet d’entretenir ce lien. 

S’ils le désirent, les candidats peuvent s’aventurer en théorie de Galois ou s’intéresser à des problèmes de localisation des valeurs propres, comme les disques de Gershgorin.',
                    'options' => 7,
                ),
                185 => 
                array (
                    'id' => 686,
                    'created_at' => '2016-06-17 20:42:48',
                    'updated_at' => '2017-02-07 14:59:14',
                    'annee' => 2016,
                    'numero' => 150,
                    'type_id' => 1,
                    'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                'rapport' => 'Dans cette leçon il faut présenter différentes actions (congruence, similitude, équivalence, ...) et dans chaque cas on pourra dégager d’une part des invariants (rang, matrices échelonnées réduites...), d’autre part des algorithmes comme le pivot de Gauss. On peut aussi, si l’on veut aborder un aspect plus théorique, faire apparaître à travers ces actions quelques décompositions célèbres ; on peut décrire les orbites lorsque la topologie s’y prête. 

S’ils le désirent, les candidats peuvent travailler sur des corps finis et utiliser le dénombrement dans ce contexte.',
                    'options' => 15,
                ),
                186 => 
                array (
                    'id' => 687,
                    'created_at' => '2016-06-17 20:43:29',
                    'updated_at' => '2017-02-07 14:59:44',
                    'annee' => 2016,
                    'numero' => 151,
                    'type_id' => 1,
                'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                    'rapport' => 'Dans cette leçon, il est important de présenter les résultats fondateurs de la théorie des espaces vectoriels de dimension finie en ayant une idée de leurs preuves. Ces théorèmes semblent simples car ils ont été très souvent pratiqués, mais leur preuve demande un soin particulier. Il est important de savoir justifier pourquoi un sous-espace vectoriel d’un espace vectoriel de dimension finie est aussi de dimension finie. Les diverses notions et caractérisations du rang trouvent leur place dans cette leçon. Les applications sont nombreuses, on peut par exemple évoquer l’existence de polynômes annulateurs ou alors décomposer les isométries en produits de réflexions. 

S’ils le désirent, les candidats peuvent déterminer des degrés d’extensions dans la théorie des corps ou s’intéresser aux nombres algébriques.',
                    'options' => 15,
                ),
                187 => 
                array (
                    'id' => 688,
                    'created_at' => '2016-06-17 20:44:02',
                    'updated_at' => '2017-02-07 15:01:21',
                    'annee' => 2016,
                    'numero' => 152,
                    'type_id' => 1,
                    'nom' => 'Déterminant. Exemples et applications.',
                'rapport' => 'Dans cette leçon, il faut commencer par définir correctement le déterminant. Il est possible d’entamer la leçon en disant que le sous-espace des formes n-linéaires alternées sur un espace de dimension n est de dimension 1, toutefois, il est essentiel de savoir le montrer. Le plan doit être cohérent ; si le déterminant n’est défini que sur $R$ ou $C$, il est délicat de définir $det(A - XI_n)$ avec $A$ une matrice carrée. L’interprétation du déterminant comme volume est essentielle.

Le calcul explicite est important, mais le jury ne peut se contenter que d’un déterminant de Vandermonde ou d’un déterminant circulant. Les opérations élémentaires permettant de calculer des déterminants, avec des illustrations sur des exemples, doivent être présentées. Il serait bien que la continuité du déterminant trouve une application, ainsi que son caractère polynomial. Pour les utilisations des propriétés topologiques, on n’ommetra  pas de préciser le corps de base sur lequel on se place. 

S’ils le désirent, les candidats peuvent s’intéresser aux calculs de déterminant sur $Z$ avec des méthodes multimodulaires ; de plus, le résultant et les applications simples à l’intersection ensembliste de deux courbes algébriques planes peuvent trouver leur place dans cette leçon.',
                    'options' => 15,
                ),
                188 => 
                array (
                    'id' => 689,
                    'created_at' => '2016-06-17 20:44:35',
                    'updated_at' => '2017-02-08 15:15:46',
                    'annee' => 2016,
                    'numero' => 153,
                    'type_id' => 1,
                    'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                    'rapport' => 'Cette leçon ne doit pas être un catalogue de résultats autour de la réduction qui est ici un moyen pour démontrer des théorèmes ; les polynômes d’endomorphismes doivent y occuper une place importante. Il faut consacrer une courte partie de la leçon à l’algèbre $K[u]$, connaître sa dimension sans hésiter ; s’ils le désirent, les candidats peuvent ensuite s’intéresser à ses propriétés globales.

Les liens entre réduction d’un endomorphisme u et la structure de l’algèbre $K[u]$ sont importants, tout comme ceux entre les idempotents et la décomposition en somme de sous-espaces caractéristiques. Il faut bien préciser que, dans la réduction de Dunford, les composantes sont des polynômes en l’endomorphisme, et en connaître les conséquences théoriques et pratiques.

L’aspect applications est trop souvent négligé. On attend d’un candidat qu’il soit en mesure, pour une matrice simple de justifier la diagonalisabilité et de déterminer un polynôme annulateur (voire minimal). Il est souhaitable que les candidats ne fassent pas la confusion entre diverses notions de multiplicité pour une valeur propre $\\lambda$ donnée (algébrique ou géométrique). Enfin, rappelons que pour calculer $A^k$, il n’est pas nécessaire en général de réduire A (la donnée d’un polynôme annulateur de A suffit souvent).',
                    'options' => 15,
                ),
                189 => 
                array (
                    'id' => 690,
                    'created_at' => '2016-06-17 20:45:14',
                    'updated_at' => '2017-02-08 15:16:23',
                    'annee' => 2016,
                    'numero' => 154,
                    'type_id' => 1,
                    'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                    'rapport' => 'Dans cette leçon, il faut présenter des propriétés de l’ensemble des sous-espaces stables par un endomorphisme. Des études détaillées sont les bienvenues, par exemple le cas d’une matrice diagonalisable ou le cas d’une matrice nilpotente d’indice maximum.

La décomposition de Frobenius trouve tout à fait sa place dans cette leçon. Il ne faut pas oublier d’examiner le cas des sous-espaces stables par des familles d’endomorphismes. Ceci peut déboucher par exemple sur des endomorphismes commutant entre eux ou sur la théorie des représentations.',
                    'options' => 7,
                ),
                190 => 
                array (
                    'id' => 691,
                    'created_at' => '2016-06-17 20:45:47',
                    'updated_at' => '2017-02-08 15:17:12',
                    'annee' => 2016,
                    'numero' => 155,
                    'type_id' => 1,
                    'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                'rapport' => 'Dans cette leçon, on attend des exemples naturels d’endomorphismes diagonalisables et des critères de diagonalisabilité. On peut étudier certaines propriétés topologiques en prenant le soin de donner des précisions sur le corps $K$ et la topologie choisie pour $M_n(K)$. Le calcul de l’exponentielle d’un endomorphisme diagonalisable est immédiat une fois que l’on connaît les valeurs propres et ceci sans diagonaliser la matrice, par exemple à l’aide des projecteurs spectraux. Sur les corps finis, on a des critères spécifiques de diagonalisabilité. On peut dénombrer les endomorphismes diagonalisables, ou possédant des propriétés données, liées à la diagonalisation. 

S’ils le désirent, les candidats peuvent s’intéresser aux liens qui peuvent aussi être fait avec la théorie des représentations et la transformée de Fourier rapide.',
                    'options' => 7,
                ),
                191 => 
                array (
                    'id' => 692,
                    'created_at' => '2016-06-17 20:46:21',
                    'updated_at' => '2017-02-08 15:19:12',
                    'annee' => 2016,
                    'numero' => 156,
                    'type_id' => 1,
                    'nom' => 'Exponentielle de matrices. Applications.',
                    'rapport' => 'Bien que ce ne soit pas une leçon d’analyse, il faut toutefois pouvoir justifier clairement la convergence de la série exponentielle. La distinction entre le cas réel et complexe doit être clairement évoqué. 

Les questions de surjectivité ou d’injectivité doivent être abordées. Par exemple la matrice $A = \\begin{pmatrix} -1 & 1 \\\\ 0 & -1 \\end{pmatrix}$ est-elle l’exponentielle d’une matrice à coefficients réels ? La matrice définie par blocs $B = \\begin{pmatrix} A & 0 \\\\ 0 & A \\end{pmatrix}$ est-elle l’exponentielle d’une matrice à coefficients réels ? La décomposition de Dunford multiplicative (décomposition de Jordan) de exppAq trouve toute son utilité dans cette leçon. Notons que l’exponentielle fait bon ménage avec la décomposition polaire dans bon nombre de problèmes sur les sous-groupes du groupe linéaire. L’étude du logarithme (quand il est défini) trouve toute sa place dans cette leçon. Si l’on traite du cas des matrices nilpotentes, on pourra invoquer le calcul sur les développements limités. Les applications aux équations différentielles doivent être évoquées sans constituer l’essentiel de la leçon. On pourra par exemple faire le lien entre réduction et comportement asymptotique, mais le jury déconseille aux candidats de proposer ce thème dans un développement.

S’ils le désirent, les candidats peuvent s’aventurer vers les sous-groupes à un paramètre du groupe linéaire (on peut alors voir si ces sous-groupes constituent des sous-variétés fermées de $GL(n,R)$) ou vers les algèbres de Lie.',
                    'options' => 7,
                ),
                192 => 
                array (
                    'id' => 693,
                    'created_at' => '2016-06-17 20:46:48',
                    'updated_at' => '2017-02-08 15:19:51',
                    'annee' => 2016,
                    'numero' => 157,
                    'type_id' => 1,
                    'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                    'rapport' => 'L’utilisation des noyaux itérés est fondamentale dans cette leçon, ceci pour déterminer si deux matrices nilpotentes sont semblables par exemple. Il est intéressant de présenter des conditions suffisantes de trigonalisation simultanée ; l’étude des endomorphismes cycliques a toute sa place dans cette leçon. Notons que l’étude des nilpotents en dimension 2 débouche naturellement sur des problèmes de quadriques et que l’étude sur un corps fini donne lieu à de jolis problèmes de dénombrement. 

S’ils le désirent, les candidats peuvent aussi présenter la décomposition de Frobenius.',
                    'options' => 15,
                ),
                193 => 
                array (
                    'id' => 694,
                    'created_at' => '2016-06-17 20:47:20',
                    'updated_at' => '2017-02-08 15:20:25',
                    'annee' => 2016,
                    'numero' => 158,
                    'type_id' => 1,
                    'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                    'rapport' => 'Le théorème spectral est indispensable dans cette leçon sans toutefois être un développement consistant. La notion de signature doit être présentée ainsi que son unicité dans la classe de congruence d’une matrice symétrique réelle. L’action du groupe linéaire et du groupe orthogonal sur l’espace des matrices symétriques peut donner un cadre naturel à cette leçon. Le lien avec les formes quadratiques et les formes hermitiennes est incontournable. La partie réelle et la partie imaginaire d’un produit hermitien définissent des structures sur l’espace vectoriel réel sous-jacent. L’orthogonalisation simultanée est un résultat important de cette leçon. Il faut en connaître les applications géométriques aux quadriques.',
                    'options' => 7,
                ),
                194 => 
                array (
                    'id' => 695,
                    'created_at' => '2016-06-17 20:47:50',
                    'updated_at' => '2017-02-08 15:20:53',
                    'annee' => 2016,
                    'numero' => 159,
                    'type_id' => 1,
                    'nom' => 'Formes linéaires et dualité en dimension nie. Exemples et applications.',
                'rapport' => 'Il est important de bien placer la thématique de la dualité dans cette leçon ; celle-ci permet de mettre en évidence des correspondances entre un morphisme et son morphisme transposé, entre un sous-espace et son orthogonal (canonique), entre les noyaux et les images ou entre les sommes et les intersections. Bon nombre de résultats d’algèbre linéaire se voient dédoublés par cette correspondance. Les liens entre base duale et fonctions de coordonnées doivent être parfaitement connus. Savoir calculer la dimension d’une intersection d’hyperplans via la dualité est important dans cette leçon. 

L’utilisation des opérations élémentaires sur les lignes et les colonnes permet facilement d’obtenir les équations d’un sous-espace vectoriel ou d’exhiber une base d’une intersection d’hyperplans. Cette leçon peut être traitée sous différents aspects : géométrique, algébrique, topologique ou analytique. Il faut que les développements proposés soient en lien direct avec la leçon. Enfin rappeler que la différentielle d’une fonction à valeurs réelles est une forme linéaire semble incontournable.',
                    'options' => 15,
                ),
                195 => 
                array (
                    'id' => 696,
                    'created_at' => '2016-06-17 20:48:20',
                    'updated_at' => '2017-02-08 15:21:15',
                    'annee' => 2016,
                    'numero' => 160,
                    'type_id' => 1,
                'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension nie).',
                    'rapport' => 'Dans cette leçon, le caractère euclidien de l’espace est essentiel pour que l’endomorphisme soit remarquable. Le théorème spectral pour les auto-adjoints et la réduction des endomorphismes orthogonaux sont des résultats incontournables. Le lemme des noyaux ou la décomposition de Dunford ne sont pas des développements adaptés à cette leçon. En revanche, l’utilisation du fait que l’orthogonal d’un sous-espace stable par un endomorphisme est stable par l’adjoint doit être mis en valeur. De même la réduction de endomorphismes normaux peut être évoquée.',
                    'options' => 7,
                ),
                196 => 
                array (
                    'id' => 697,
                    'created_at' => '2016-06-17 20:48:52',
                    'updated_at' => '2017-02-08 15:21:45',
                    'annee' => 2016,
                    'numero' => 161,
                    'type_id' => 1,
                    'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Applications en dimensions 2 et 3.',
                    'rapport' => 'La classification des isométries en dimension 2 et 3 est exigible. Il faut savoir prouver qu’une isométrie est affine, pouvoir donner des générateurs du groupe des isométries affines, et savoir composer des isométries affines. En dimension 3, il faut savoir classifier les rotations et connaître les liens avec la réduction. On peut aussi penser aux applications aux isométries laissant stables certains objets en dimension 2 et 3.',
                    'options' => 7,
                ),
                197 => 
                array (
                    'id' => 698,
                    'created_at' => '2016-06-17 20:49:45',
                    'updated_at' => '2017-02-08 15:22:50',
                    'annee' => 2016,
                    'numero' => 162,
                    'type_id' => 1,
                    'nom' => 'Systèmes d\'équations linéaires ; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                'rapport' => 'Dans cette leçon, les techniques liées au simple pivot de Gauss constituent l’essentiel des attendus. Il est impératif de présenter la notion de système échelonné, avec une définition précise et correcte, et de situer l’ensemble dans le contexte de l’algèbre linéaire (sans oublier la dualité). Un point de vue opératoire doit accompagner l’étude théorique et l’intérêt pratique (algorithmique) des méthodes présentées doit être expliqué y compris sur des exemples simples où l’on attend parfois une résolution explicite.

S’ils le désirent, les candidats peuvent aussi présenter les relations de dépendances linéaires sur les colonnes d’une matrice échelonnée qui permettent de décrire simplement les orbites de l’action à gauche de $GL(n,K)$ sur $M_n(K)$ donnée par $(P,A) \\longmapsto PA$. De même, des discussions sur la résolution de systèmes sur Z et la forme normale de Hermite peuvent trouver leur place dans cette leçon.',
                    'options' => 15,
                ),
                198 => 
                array (
                    'id' => 699,
                    'created_at' => '2016-06-17 20:50:51',
                    'updated_at' => '2017-02-08 15:23:18',
                    'annee' => 2016,
                    'numero' => 170,
                    'type_id' => 1,
                    'nom' => 'Formes quadratiques sur un espace vectoriel de dimension nie. Orthogonalité, isotropie. Applications.',
                    'rapport' => 'Il faut tout d’abord noter que l’intitulé implique implicitement que le candidat ne doit pas se contenter de travailler sur R. Le candidat pourra parler de la classification des formes quadratiques sur le corps des complexes et sur les corps finis. L’algorithme de Gauss doit être énoncé et pouvoir être pratiqué sur une forme quadratique simple.

Les notions d’isotropie et de cône isotrope sont un aspect important de cette leçon. On pourra rattacher cette notion à la géométrie différentielle.',
                    'options' => 15,
                ),
                199 => 
                array (
                    'id' => 700,
                    'created_at' => '2016-06-17 20:51:21',
                    'updated_at' => '2017-02-08 15:24:43',
                    'annee' => 2016,
                    'numero' => 171,
                    'type_id' => 1,
                    'nom' => 'Formes quadratiques réelles. Exemples et applications.',
                    'rapport' => 'Dans cette leçon, la loi d’inertie de Silvester doit être présentée ainsi que l’orthogonalisation simultanée. L’algorithme de Gauss doit être énoncé et pouvoir être expliqué sur une forme quadratique de $R^3$ le lien avec la signature doit être clairement énoncé et la signification géométrique des deux entiers $r$ et $s$ composant la signature d’une forme quadratique réelle doit être expliqué. La différentielle seconde d’une fonction de plusieurs variables est une forme quadratique importante qui mérite d’être présentée dans cette leçon.

La définition des coniques affines non dégénérées doit être connue, et les propriétés classiques des coniques doivent être données. On pourra présenter les liens entre la classification des formes quadratiques et celles des coniques ; de même il est intéressant d’évoquer le lien entre le discriminant de l’équation $ax^2 + bx + c = 0$ la signature de la forme quadratique $ax^2 + bxy + cy^2 $

S’ils le désirent, les candidats peuvent aussi aller vers la théorie des représentation et présenter l’indicatrice de Schur-Frobenius qui permet de réaliser une représentation donnée sur le corps des réels.',
                    'options' => 7,
                ),
                200 => 
                array (
                    'id' => 701,
                    'created_at' => '2016-06-17 20:51:45',
                    'updated_at' => '2016-06-17 20:51:45',
                    'annee' => 2016,
                    'numero' => 180,
                    'type_id' => 1,
                    'nom' => 'Coniques. Applications.',
                    'rapport' => '',
                    'options' => 7,
                ),
                201 => 
                array (
                    'id' => 702,
                    'created_at' => '2016-06-17 20:52:18',
                    'updated_at' => '2017-02-08 15:25:16',
                    'annee' => 2016,
                    'numero' => 181,
                    'type_id' => 1,
                    'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
                'rapport' => 'Dans cette leçon, la notion de coordonnées barycentriques est incontournable ; des illustrations dans le triangle (coordonnées barycentriques de certains points remarquables) sont envisageables. Il est important de parler d’enveloppe convexe, de points extrémaux, ainsi que des applications qui en résultent. 

S’ils le désirent, les candidats peuvent aller plus loin en présentant le lemme de Farkas, le théorème de séparation de Hahn-Banach, ou les théorèmes de Helly et de Caratheodory.',
                    'options' => 15,
                ),
                202 => 
                array (
                    'id' => 703,
                    'created_at' => '2016-06-17 20:52:44',
                    'updated_at' => '2017-02-08 15:25:50',
                    'annee' => 2016,
                    'numero' => 182,
                    'type_id' => 1,
                    'nom' => 'Applications des nombres complexes à la géométrie.',
                'rapport' => 'Cette leçon ne doit pas rester au niveau de la classe terminale. L’étude des inversions est tout à fait appropriée, en particulier la possibilité de ramener un cercle à une droite et inversement ; la formule de Ptolémée illustre bien l’utilisation de cet outil. Il est nécessaire de présenter les similitudes, les homographies et le birapport. On peut parler des suites définies par récurrence par une homographie et leur lien avec la réduction dans $SL_2(C)$.

S’ils le désirent, les candidats peuvent aussi étudier l’exponentielle complexe et les homographies de la sphère de Riemann. La réalisation du groupe SU 2 dans le corps des quaternions et ses applications peuvent trouver leur place dans la leçon.',
                    'options' => 15,
                ),
                203 => 
                array (
                    'id' => 704,
                    'created_at' => '2016-06-17 20:53:27',
                    'updated_at' => '2017-02-08 15:26:17',
                    'annee' => 2016,
                    'numero' => 183,
                    'type_id' => 1,
                    'nom' => 'Utilisation des groupes en géométrie.',
                'rapport' => 'C’est une leçon dans laquelle on s’attend à trouver des utilisations variées. On s’attend à ce que soient définis différents groupes de transformations (isométries, déplacements, similitudes, translations) et à voir résolus des problèmes géométriques par des méthodes consistant à composer des transformations. De plus, les actions de groupes sur la géométrie permettent aussi de dégager des invariants essentiels (angle, birapport, excentricité d’une conique). Les groupes d’isométries d’une figure sont incontournables.',
                    'options' => 15,
                ),
                204 => 
                array (
                    'id' => 705,
                    'created_at' => '2016-06-17 20:54:13',
                    'updated_at' => '2017-02-08 15:26:52',
                    'annee' => 2016,
                    'numero' => 190,
                    'type_id' => 1,
                    'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                'rapport' => 'Il est nécessaire de dégager clairement différentes méthodes de dénombrement et les illustrer d’exemples significatifs. De nombreux domaines de mathématiques sont concernés par des problèmes de dénombrement, cet aspect varié du thème de la leçon doit être mis en avant. L’utilisation de séries génératrices est un outil puissant pour le calcul de certains cardinaux. De plus il est naturel de calculer des cardinaux classiques et certaines probabilités. Il est important de connaître l’interprétation ensembliste de la somme des coefficients binomiaux, et ne pas se contenter d’une justification par le binôme de Newton. L’introduction des corps finis (même en se limitant aux cardinaux premiers) permet de créer un lien avec l’algèbre linéaire. Les actions de groupes peuvent également conduire à des résultats remarquables.

S’ils le désirent, les candidats peuvent aussi présenter des applications de la formule d’inversion de Möebius ou de la formule de Burnside.',
                    'options' => 15,
                ),
                205 => 
                array (
                    'id' => 706,
                    'created_at' => '2016-06-18 08:34:36',
                    'updated_at' => '2017-02-08 15:29:42',
                    'annee' => 2016,
                    'numero' => 201,
                    'type_id' => 2,
                    'nom' => 'Espaces de fonctions : exemples et applications.',
                'rapport' => 'C’est une leçon riche où le candidat devra choisir soigneusement le niveau auquel il souhaite se placer. Les espaces de fonctions continues sur un compact (par exemple l’intervalle $[0,1]$) offrent des exemples élémentaires et pertinents. Dans ce domaine, le jury attend une maîtrise du fait qu’une limite uniforme de fonctions continues est continue. Les candidats peuvent se concentrer dans un premier temps sur les espaces de fonctions continues et les bases de la convergence uniforme. Les espaces de Hilbert de fonctions comme l’espace des fonctions $L^2$ constituent ensuite une ouverture déjà significative. 

Pour aller plus loin, d’autres espaces usuels tels que les espaces $L^p$ ont tout à fait leur place dans cette leçon. Le théorème de Riesz-Fischer est alors un très bon développement pour autant que ses difficultés soient maîtrisées. Les espaces de fonctions holomorphes sur un ouvert de C constituent aussi une ouverture de très bon niveau.',
                    'options' => 7,
                ),
                206 => 
                array (
                    'id' => 707,
                    'created_at' => '2016-06-18 08:35:18',
                    'updated_at' => '2017-02-08 15:30:21',
                    'annee' => 2016,
                    'numero' => 202,
                    'type_id' => 2,
                    'nom' => 'Exemples de parties denses et applications.',
                    'rapport' => 'Il ne faut pas négliger les exemples élémentaires comme les sous-groupes additifs de R et leurs applications, ou encore les critères de densité dans un espace de Hilbert. Le théorème de Weierstrass via les polynômes de Bernstein peut être abordé à des niveaux divers suivant que l’on précise ou pas la vitesse de convergence voire son optimalité.

Pour aller plus loin, la version plus abstraite du théorème de Weierstrass (le théorème de Stone-Weierstrass) est aussi intéressante et a de multiples applications. Cette leçon permet aussi d’explorer les questions d’approximation de fonctions par des polynômes et des polynômes trigonométriques, ou plus généralement la densité de certains espaces remarquables de fonctions dans les espaces de fonctions continues, ou dans les espaces $L^p$ . Il est également possible de parler de l’équirépartition.',
                    'options' => 7,
                ),
                207 => 
                array (
                    'id' => 708,
                    'created_at' => '2016-06-18 08:36:00',
                    'updated_at' => '2017-02-08 15:30:54',
                    'annee' => 2016,
                    'numero' => 203,
                    'type_id' => 2,
                    'nom' => 'Utilisation de la notion de compacité.',
                'rapport' => 'Il est important de ne pas concentrer la leçon sur la compacité générale (confusion entre utilisation de la notion compacité et notion de compacité). Néanmoins, on attend des candidats d’avoir une vision synthétique de la compacité. Des exemples d’applications comme le théorème de Heine et le théorème de Rolle doivent y figurer et leur démonstration être connue. Par ailleurs, le candidat doit savoir quand la boule unité d’un espace vectoriel normé est compacte. Des exemples significatifs d’utilisation comme le théorème de Stone-Weierstrass, des théorèmes de point fixe, voire l’étude qualitative d’équations différentielles, sont tout-à fait envisageables. Le rôle de la compacité pour des problèmes d’existence d’extrema mériterait d’être davantage étudié. On peut penser comme application à la diagonalisation des matrices symétriques à coefficients réels.

Pour aller plus loin, les familles normales de fonctions holomorphes fournissent des exemples fondamentaux d’utilisation de la compacité. Les opérateurs auto-adjoints compacts sur l’espace de Hilbert relèvent également de cette leçon, et on pourra développer l’analyse de leurs propriétés spectrales.',
                    'options' => 15,
                ),
                208 => 
                array (
                    'id' => 709,
                    'created_at' => '2016-06-18 08:36:42',
                    'updated_at' => '2017-02-08 15:31:21',
                    'annee' => 2016,
                    'numero' => 204,
                    'type_id' => 2,
                    'nom' => 'Connexité. Exemples et applications. ',
                'rapport' => 'Le rôle clef de la connexité dans le passage du local au global doit être mis en évidence dans cette leçon : en calcul différentiel, voire pour les fonctions holomorphes. Il est important de présenter des résultats naturels dont la démonstration utilise la connexité. La stabilité par image continue, l’identification des connexes de R sont des résultats incontournables. On distinguera bien connexité et connexité par arcs (avec des exemples compris par le candidat), mais il est pertinent de présenter des situations où ces deux notions coïncident. A contrario, on pourra distinguer leur comportement par passage à l’adhérence.

Des exemples issus d’autres champs (algèbre linéaire notamment) seront appréciés. Le choix des développements doit être pertinent, le préambule en fournit quelques exemples, même s’il fait aussi appel à des thèmes différents ; on peut ainsi suggérer le théorème de Runge.',
                    'options' => 7,
                ),
                209 => 
                array (
                    'id' => 710,
                    'created_at' => '2016-06-18 08:37:37',
                    'updated_at' => '2017-02-08 15:34:42',
                    'annee' => 2016,
                    'numero' => 205,
                    'type_id' => 2,
                    'nom' => 'Espaces complets. Exemples et applications. ',
                'rapport' => 'Les candidats devraient faire apparaître que l’un des intérêts essentiels de la complétude est de fournir des théorèmes d’existence : que ce soit tout simplement dans R ou C mais aussi dans certains espaces de dimension infinie (par exemple dans certains espaces de fonctions). Il est important de présenter des exemples d’espaces usuels, dont on sait justifier la complétude. Rappelons ici que l’on attend des candidats une bonne maîtrise de la convergence uniforme. Les espaces $L^p$ sont des exemples pertinents qui ne sont pas sans danger pour des candidats aux connaissances fragiles. On peut évoquer dans cette leçon des théorèmes classiques tels que le théorème de Cauchy-Lipschitz ou le théorème du point fixe des applications contractantes. 

On ne s’aventurera pas à parler du théorème de Baire sans application pertinente et maîtrisée ; elles sont nombreuses. Rappelons à ce propos que la démonstration détaillée de l’existence d’une partie dense de  fonctions continues dérivables en aucun point est délicate.',
                    'options' => 7,
                ),
                210 => 
                array (
                    'id' => 711,
                    'created_at' => '2016-06-18 08:39:12',
                    'updated_at' => '2017-02-08 15:35:19',
                    'annee' => 2016,
                    'numero' => 206,
                    'type_id' => 2,
                    'nom' => 'Théorèmes de point fixe. Exemples et applications. ',
                    'rapport' => 'Cette leçon n’apparaîtra plus dans la session 2017. Toutefois le théorème du point fixe de Banach trouve naturellement sa place dans les leçons 205, 226, voire 233. Il est également sous-jacent dans d’autres thèmes de leçons : calcul différentiel, équations différentielles.',
                    'options' => 15,
                ),
                211 => 
                array (
                    'id' => 712,
                    'created_at' => '2016-06-18 08:39:50',
                    'updated_at' => '2017-02-08 15:36:02',
                    'annee' => 2016,
                    'numero' => 207,
                    'type_id' => 2,
                    'nom' => ' Prolongement de fonctions. Exemples et applications.',
                'rapport' => 'Il ne faut pas hésiter à commencer par des exemples très simples tels que le prolongement en 0 de la fonction $x \\longmapsto \\sin(x)/x$ , mais il faut aller plus loin que le simple prolongement par continuité. Le prolongement par densité et le prolongement analytique relèvent bien sûr de cette leçon. Pour aller plus loin, on peut par exemple parler de l’extension à $L^2$ de la transformation de Fourier. En ce qui concerne le théorème de Hahn-Banach, le candidat n’en donnera la version la plus générale que s’il peut s’aventurer sur le terrain délicat du lemme de Zorn. Rappelons que l’on peut aussi s’en dispenser pour justifier le théorème de Hahn-Banach de façon plus élémentaire dans le cas séparable.',
                    'options' => 7,
                ),
                212 => 
                array (
                    'id' => 713,
                    'created_at' => '2016-06-18 08:40:37',
                    'updated_at' => '2017-02-08 15:36:40',
                    'annee' => 2016,
                    'numero' => 208,
                    'type_id' => 2,
                    'nom' => 'Espaces vectoriels normés, applications linéaires continues. Exemples. ',
                'rapport' => 'Une telle leçon doit bien sûr contenir beaucoup d’illustrations et d’exemples, notamment avec quelques calculs élémentaires de normes subordonnées. Lors du choix de ceux-ci (le jury n’attend pas une liste encyclopédique), le candidat veillera à ne pas mentionner des exemples pour lesquels il n’a aucune idée de leur pertinence et à ne pas se lancer dans des développements trop sophistiqués. 

La justification de la compacité de la boule unité en dimension finie doit être maîtrisée. Il faut savoir énoncer le théorème de Riesz sur la compacité de la boule unité fermée d’un espace vectoriel normé. Le théorème d’équivalence des normes en dimension finie, ou le caractère fermé de tout sous-espace de dimension finie d’un espace normé, sont des résultats fondamentaux à propos desquels les candidats doivent se garder des cercles vicieux. A contrario, des exemples d’espaces vectoriels normés de dimension infinie ont leur place dans cette leçon et il faut connaître quelques exemples de normes usuelles non équivalentes, notamment sur des espaces de suites ou des espaces de fonctions.

Pour aller plus loin, on peut éventuellement considérer le cas d’espaces métrisables mais dont la métrique n’est pas issue d’une norme, par exemple dans le champ des espaces de fonctions analytiques (topologie de la convergence uniforme sur tout compact par exemple).',
                    'options' => 15,
                ),
                213 => 
                array (
                    'id' => 714,
                    'created_at' => '2016-06-18 08:41:19',
                    'updated_at' => '2017-02-08 15:37:36',
                    'annee' => 2016,
                    'numero' => 209,
                    'type_id' => 2,
                    'nom' => ' Approximation d\'une fonction par des polynômes et des polynômes trigonométriques. Exemples et applications.',
                'rapport' => 'Cette leçon comporte un certain nombre de classiques comme par exemple les polynômes de Bernstein, éventuellement agrémenté d’une estimation de la vitesse de convergence (avec le module de continuité). Il n’est pas absurde de voir la formule de Taylor comme une approximation locale d’une fonction par des polynômes. Les polynômes d’interpolation de Lagrange peuvent être mentionnés en mettant en évidence les problèmes qu’ils engendrent du point de vue de l’approximation. Pour aller plus loin, le théorème de Fejér (versions $L^1$, $L^p$ ou $C(T)$) offre aussi la possibilité d’un joli développement, surtout s’il est agrémenté d’applications (polynômes trigonométriques lacunaires, injectivité de la transformée de Fourier sur $L^1$ , . . .), mais on peut aussi s’intéresser à la convolution avec d’autres noyaux.',
                    'options' => 7,
                ),
                214 => 
                array (
                    'id' => 715,
                    'created_at' => '2016-06-18 08:41:53',
                    'updated_at' => '2017-02-08 15:41:00',
                    'annee' => 2016,
                    'numero' => 213,
                    'type_id' => 2,
                    'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications. ',
                'rapport' => 'Il est bon de connaître et savoir justifier le critère de densité des sous-espaces par passage à l’orthogonal. Il faut aussi illustrer la leçon par des exemples de bases hilbertiennes (polynômes orthogonaux, séries de Fourier, . . .).

Il est important de faire la différence entre base algébrique et base hilbertienne. De plus, la formule de
la projection orthogonale sur un sous espace de dimension finie d’un espace de Hilbert doit absolument
être connue de même que l’interprétation géométrique de la méthode de Gramm-Schmidt. Le théorème
de projection sur les convexes fermés (ou sur un sous-espace vectoriel fermé) d’un espace de Hilbert H
est régulièrement mentionné. Les candidats doivent s’intéresser au sens des formules

$$ x = \\sum_{n \\ge 0}  (x | e_n) e_n \\text{ et } ||x||^2 = \\sum_{n \\ge 0} | (x|e_n)|^2 $$

en précisant les hypothèses sur la famille $(e_n)_{n\\in \\mathbb{N}}$ et en justifiant la convergence. La notion d’adjoint d’un opérateur continu peut illustrer agréablement cette leçon. 

Pour aller plus loin, le programme permet d’aborder la résolution et l’approximation de problèmes aux limites en dimension 1 par des arguments exploitant la formulation variationnelle de ces équations. Plus généralement, l’optimisation de fonctionnelles convexes sur les espaces de Hilbert peut être explorée. Enfin, le difficile théorème spectral pour les opérateurs auto-adjoints compacts peut être abordé.',
                    'options' => 7,
                ),
                215 => 
                array (
                    'id' => 716,
                    'created_at' => '2016-06-18 08:42:29',
                    'updated_at' => '2017-02-08 15:41:45',
                    'annee' => 2016,
                    'numero' => 214,
                    'type_id' => 2,
                    'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications. ',
                    'rapport' => 'Il s’agit d’une belle leçon, formulée ici dans la version qui sera adoptée pour la session 2017, qui exige une bonne maîtrise du calcul différentiel. Même si le candidat ne propose pas ces thèmes en développement, on est en droit d’attendre de lui des idées de démonstration des deux théorèmes fondamentaux qui donnent son intitulé à la leçon. Il est indispensable de savoir mettre en pratique le théorème des fonctions implicites au moins dans le cas de deux variables réelles. On attend des applications en géométrie différentielle notamment dans la formulation des multiplicateurs de Lagrange. Plusieurs inégalités classiques de l’analyse peuvent se démontrer avec ce point de vue : arithmético-géométrique, Hölder, Carleman, Hadamard, . . . En ce qui concerne la preuve du théorème des extrema liés, la présentation de la preuve par raisonnement “sous-matriciel” est souvent obscure ; on priviligiera si possible une présentation géométrique s’appuyant sur l’espace tangent.

Pour aller plus loin, l’introduction des sous-variétés est naturelle dans cette leçon. Il s’agit aussi d’agrémenter cette leçon d’exemples et d’applications en géométrie, sur les courbes et les surfaces.',
                    'options' => 15,
                ),
                216 => 
                array (
                    'id' => 717,
                    'created_at' => '2016-06-18 08:43:01',
                    'updated_at' => '2017-02-08 15:42:26',
                    'annee' => 2016,
                    'numero' => 215,
                    'type_id' => 2,
                    'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications. ',
                'rapport' => 'Cette leçon requiert une bonne maîtrise de la notion de différentielle première et de son lien avec les dérivées partielles. On doit pouvoir mettre en pratique le théorème de différentiation composée pour calculer des dérivées partielles de fonctions composées dans des situations simples (par exemple le laplacien en coordonnées polaires). La différentiation à l’ordre 2 est attendue, notamment pour les applications classiques quant à l’existence d’extrema locaux. On peut aussi faire figurer dans cette leçon la différentielle d’applications issues de l’algèbre linéaire (ou multilinéaire). 

Pour aller plus loin, l’exponentielle matricielle est une ouverture pertinente. D’autres thèmes issus de la leçon 214 trouvent aussi leur place ici.',
                    'options' => 15,
                ),
                217 => 
                array (
                    'id' => 718,
                    'created_at' => '2016-06-18 08:43:27',
                    'updated_at' => '2017-02-08 15:42:36',
                    'annee' => 2016,
                    'numero' => 217,
                    'type_id' => 2,
                    'nom' => 'Sous-variétés de $R^n$. Exemples. ',
                    'rapport' => 'Ce thème retrouve naturellement sa place dans les leçons 214, 215, 219.',
                    'options' => 7,
                ),
                218 => 
                array (
                    'id' => 719,
                    'created_at' => '2016-06-18 08:43:57',
                    'updated_at' => '2017-02-08 15:43:24',
                    'annee' => 2016,
                    'numero' => 218,
                    'type_id' => 2,
                    'nom' => 'Applications des formules de Taylor. ',
                'rapport' => 'Il faut connaître les formules de Taylor et certains développements très classiques. En général, le développement de Taylor d’une fonction comprend un terme de reste qu’il est crucial de savoir analyser. Le candidat doit pouvoir justifier les différentes formules de Taylor proposées ainsi que leur intérêt. Le jury s’inquiète des trop nombreux candidats qui ne savent pas expliquer clairement ce que signifient les notations o ou O qu’ils utilisent. De plus la différence entre l’existence d’un développement limité à l’ordre deux et l’existence de dérivée seconde doit être connue. On peut aussi montrer comment les formules de Taylor permettent d’établir le caractère développable en série entière (ou analytique) d’une fonction dont on contrôle les dérivées successives.

Pour aller plus loin, on peut mentionner des applications en algèbre bilinéaire (lemme de Morse), en géométrie (étude locale au voisinage des points stationnaires pour les courbes et des points critiques pour la recherche d’extrema) et, même si c’est plus anecdotique, en probabilités (Théorème central limite). On peut aussi penser à la méthode de Laplace, du col, de la phase stationnaire ou aux inégalités contrôlant les dérivées intermédiaires lorsque f et sa dérivée n-ième sont bornées. On soignera particulièrement le choix des développements.',
                    'options' => 15,
                ),
                219 => 
                array (
                    'id' => 720,
                    'created_at' => '2016-06-18 08:44:27',
                    'updated_at' => '2017-02-08 15:44:40',
                    'annee' => 2016,
                    'numero' => 219,
                    'type_id' => 2,
                    'nom' => 'Extremums : existence, caractérisation, recherche. Exemples et applications. ',
                'rapport' => 'Comme souvent en analyse, il peut être opportun d’illustrer dans cette leçon un exemple ou un raisonnement à l’aide d’un dessin. Il faut savoir faire la distinction entre propriétés locales (caractérisation d’un extremum) et globales (existence par compacité, par exemple). Dans le cas important des fonctions convexes, un minimum local est également global. Les applications de la minimisation des fonctions convexes sont nombreuses et elles peuvent illustrer cette leçon.

L’étude des algorithmes de recherche d’extremums y a toute sa place : méthode de gradient, preuve de la convergence de la méthode de gradient à pas optimal, . . . Le cas particulier des fonctionnelles sur $R^n$ de la forme $\\frac{1}{2} (Ax|x) - (b|x), où A est une matrice symétrique définie positive, ne devrait pas poser de difficultés. Les problèmes de minimisation sous contrainte amènent à faire le lien avec les extremums liés, la notion de  multiplicateur de Lagrange et, là encore, des algorithmes peuvent être présentés et analysés. À ce sujet, une preuve géométrique des extrema liés sera fortement valorisée par rapport à une preuve algébrique, formelle et souvent mal maîtrisée. 

Les candidats pourraient aussi être amenés à évoquer les problèmes de type moindres carrés, ou, dans un autre registre, le principe du maximum et ses applications.',
                    'options' => 15,
                ),
                220 => 
                array (
                    'id' => 721,
                    'created_at' => '2016-06-18 08:45:07',
                    'updated_at' => '2017-02-08 15:45:46',
                    'annee' => 2016,
                    'numero' => 220,
                    'type_id' => 2,
                'nom' => ' Équations différentielles $X\' = f(t,X)$. Exemples d\'études des solutions en dimension $1$ et $2$.',
                    'rapport' => 'C’est l’occasion de rappeler une nouvelle fois que le jury s’alarme des nombreux défauts de maîtrise du théorème de Cauchy-Lipschitz. Il est regrettable de voir des candidats ne connaître qu’un énoncé pour les fonctions globalement lipschitziennes ou plus grave, mélanger les conditions sur la variable de temps et d’espace. La notion de solution maximale et le théorème de sortie de tout compact sont nécessaires. Bien évidemment, le jury attend des exemples d’équations différentielles non linéaires. Le lemme de Grönwall semble trouver toute sa place dans cette leçon mais est curieusement rarement énoncé. L’utilisation du théorème de Cauchy-Lipschitz doit pouvoir être mise en œuvre sur des exemples concrets. Les études qualitatives doivent être préparées et soignées. 

Pour les équations autonomes, la notion de point d’équilibre permet des illustrations de bon goût comme par exemple les petites oscillations du pendule. Trop peu de candidats pensent à tracer et discuter des portraits de phase alors que le sujet y invite clairement.

Pour aller plus loin, il est possible d’évoquer les problématiques de l’approximation numérique dans cette leçon en présentant le point de vue du schéma d’Euler. On peut aller jusqu’à aborder la notion de problèmes raides et la conception de schémas implicites pour autant que le candidat ait une maîtrise convenable de ces questions.',
                    'options' => 15,
                ),
                221 => 
                array (
                    'id' => 722,
                    'created_at' => '2016-06-18 08:45:36',
                    'updated_at' => '2017-02-08 15:46:26',
                    'annee' => 2016,
                    'numero' => 221,
                    'type_id' => 2,
                    'nom' => ' Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                'rapport' => 'Le jury attend d’un candidat qu’il sache déterminer rigoureusement la dimension de l’espace vectoriel des solutions. Le cas des systèmes à coefficients constants fait appel à la réduction des matrices qui doit être connue et pratiquée. Le jury attend qu’un candidat puisse mettre en œuvre la méthode de variation des constantes pour résoudre une équation différentielle linéaire d’ordre 2 simple (à coefficients constants par exemple) avec second membre.

L’utilisation des exponentielles de matrices a toute sa place ici. Les problématiques de stabilité des solutions et le lien avec l’analyse spectrale devraient être exploitées. 

Le théorème de Cauchy-Lipschitz linéaire constitue un exemple de développement pertinent pour cette leçon. Les résultats autour du comportement des solutions, ou de leurs zéros, de certaines équations linéaires d’ordre 2 (Sturm, Hill-Mathieu, . . .) sont aussi d’autres possibilités.',
                    'options' => 15,
                ),
                222 => 
                array (
                    'id' => 723,
                    'created_at' => '2016-06-18 08:46:03',
                    'updated_at' => '2017-02-08 15:48:19',
                    'annee' => 2016,
                    'numero' => 222,
                    'type_id' => 2,
                    'nom' => 'Exemples d\'équations aux dérivées partielles linéaires. ',
                'rapport' => 'Cette leçon peut être abordée en faisant appel à des techniques variées et de nombreux développements pertinents peuvent être construits en exploitant judicieusement les éléments les plus classiques du programme. Le candidat ne doit pas hésiter à donner des exemples très simples (par exemple les équations de transport).

Les techniques d’équations différentielles s’expriment par exemple pour traiter $\\lambda u - u\'\' = f$ avec des conditions de Dirichlet en $x =0$, $x=1$ ou pour analyser l’équation de transport par la méthode des caractéristiques.

Les séries de Fourier trouvent dans cette leçon une mise en pratique toute désignée pour résoudre l’équation de la chaleur, de Schrödinger ou des ondes dans le contexte des fonctions périodiques. La transformée de Fourier, notamment sur l’espace de Schwartz, peut être considérée. 

Le point de vue de l’approximation numérique donne lieu à des développements originaux, notamment autour de la matrice du laplacien et de l’analyse de convergence de la méthode des différences finies. 

Des développements plus sophistiqués se placeront sur le terrain de l’analyse hilbertienne avec le théorème de Lax-Milgram, l’espace de Sobolev $H_0^1(]0,1[]$, jusqu’à la décomposition spectrale des opérateurs compacts, ou encore sur celui des distributions avec l’étude de solutions élémentaires d’équations elliptiques.',
                        'options' => 7,
                    ),
                    223 => 
                    array (
                        'id' => 724,
                        'created_at' => '2016-06-18 08:46:29',
                        'updated_at' => '2017-02-08 15:48:57',
                        'annee' => 2016,
                        'numero' => 223,
                        'type_id' => 2,
                        'nom' => 'Suites numériques. Convergence, valeurs d\'adhérence. Exemples et applications. ',
                        'rapport' => 'Cette leçon permet souvent aux candidats de s’exprimer. Il ne faut pas négliger les suites de nombres complexes. Le théorème de Bolzano-Weierstrass doit être cité et le candidat doit être capable d’en donner une démonstration. On attend des candidats qu’ils parlent des limites inférieure et supérieure d’une suite réelle bornée, et qu’ils en maîtrisent le concept. Les procédés de sommation peuvent être éventuellement évoqués mais le théorème de Cesàro doit être mentionné et sa preuve maîtrisée par tout candidat à l’agrégation. Les résultats autour des sous-groupes additifs de R permettent d’exhiber des suites denses remarquables et l’ensemble constitue un joli thème.

Pour aller plus loin, un développement autour de l’équirépartition est tout à fait envisageable.',
                        'options' => 15,
                    ),
                    224 => 
                    array (
                        'id' => 725,
                        'created_at' => '2016-06-18 08:47:02',
                        'updated_at' => '2017-02-08 15:51:28',
                        'annee' => 2016,
                        'numero' => 226,
                        'type_id' => 2,
                    'nom' => 'Suites vectorielles et réelles définies par une relation de récurrence $u_{n+1} = f(u_n)$. Exemples et applications. ',
                    'rapport' => 'Citer au moins un théorème de point fixe dans cette leçon est pertinent. Le jury attend d’autres exemples que la traditionnelle suite récurrente $u_{n+1} = \\sin(u_n)$ (dont il est souhaitable de savoir expliquer les techniques sous-jacentes). 

La nouvelle formulation de cette leçon, qui sera en vigueur en 2017, invite à évoquer les problématiques de convergence d’algorithmes (notamment savoir estimer la vitesse), d’approximation de solutions de problèmes linéaires et non linéaires : dichotomie, méthode de Newton, algorithme du gradient, méthode de la puissance, méthodes itératives de résolution de systèmes linéaires, schéma d’Euler, ...

L’aspect vectoriel est souvent négligé. Par exemple, le jury attend des candidats qu’ils répondent de façon pertinente à la question de la généralisation de l’algorithme de Newton au moins dans $R^2$, voire $R^n$.',
                        'options' => 15,
                    ),
                    225 => 
                    array (
                        'id' => 726,
                        'created_at' => '2016-06-18 08:47:30',
                        'updated_at' => '2017-02-08 15:52:56',
                        'annee' => 2016,
                        'numero' => 228,
                        'type_id' => 2,
                        'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et contre-exemples. ',
                        'rapport' => 'Cette leçon permet des exposés de niveaux très variés. Les théorèmes de base doivent être maîtrisés et illustrés par des exemples intéressants, par exemple le théorème des valeurs intermédiaires pour la dérivée. Le jury s’attend à ce que le candidat connaisse et puisse calculer la dérivée des fonctions usuelles. Les candidats doivent disposer d’un exemple de fonction dérivable de la variable réelle qui ne soit pas continûment dérivable. La stabilité par passage à la limite des notions de continuité et de dérivabilité doit être comprise par les candidats. De façon plus fine, on peut s’intéresser aux fonctions continues nulle part dérivables.

Pour aller plus loin, la dérivabilité presque partout des fonctions lipschitziennes ou des fonctions monotones relève de cette leçon. Les applications du théorème d’Ascoli (avec, par exemple, des exemples d’opérateurs à noyaux compacts), sont les bienvenues. L’étude de la dérivée au sens des distributions de 
$x \\in [a,b] \\longmapsto \\int_a^x f(t) dt$ pour une fonction  intégrable $f \\in L^1([a,b])$ est un résultat intéressant.',
                        'options' => 7,
                    ),
                    226 => 
                    array (
                        'id' => 727,
                        'created_at' => '2016-06-18 08:48:03',
                        'updated_at' => '2017-02-08 15:53:32',
                        'annee' => 2016,
                        'numero' => 229,
                        'type_id' => 2,
                        'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications. ',
                        'rapport' => 'L’énoncé et la connaissance de la preuve de l’existence de limites à gauche et à droite pour les fonctions monotones sont attendues. Ainsi on doit parler des propriétés de continuité et de dérivabilité à gauche et à droite des fonctions convexes de la variable réelle. Il est souhaitable d’illustrer la présentation de la convexité par des dessins clairs. On notera que la monotonie concerne les fonctions réelles d’une seule variable réelle, mais que la convexité concerne également les fonctions définies sur une partie convexe de $R^n$ , qui fournissent de beaux exemples d’utilisation.

Pour aller plus loin, la dérivabilité presque partout des fonctions monotones est un résultat remarquable (dont la preuve peut être éventuellement admise). L’espace vectoriel engendré par les fonctions monotones (les fonctions à variation bornée) relève de cette leçon. Enfin, la dérivation au sens des distributions fournit les caractérisations les plus générales de la monotonie et de la convexité ; les candidats maîtrisant ces notions peuvent s’aventurer utilement dans cette direction.',
                        'options' => 15,
                    ),
                    227 => 
                    array (
                        'id' => 728,
                        'created_at' => '2016-06-18 08:48:30',
                        'updated_at' => '2017-02-08 15:54:13',
                        'annee' => 2016,
                        'numero' => 230,
                        'type_id' => 2,
                        'nom' => 'Séries de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples. ',
                    'rapport' => 'De nombreux candidats commencent leur plan par une longue exposition des conditions classiques assurant la convergence ou la divergence des séries numériques. Sans être hors sujet, cette exposition ne doit pas former l’essentiel de la matière de la leçon. Le thème central de la leçon est en effet le comportement asymptotique des restes et sommes partielles (équivalents, développements asymptotiques — par exemple pour certaines suites récurrentes — cas des séries de Riemann, . . .).

On peut aussi s’intéresser à certaines sommes particulières, que ce soit pour exhiber des nombres irrationnels (voire transcendants), ou mettre en valeur des techniques de calculs non triviales (par exemple en faisant appel aux séries de Fourier ou aux séries entières). 

Enfin le jury apprécie que le théorème des séries alternées (avec sa version sur le contrôle du reste) soit maîtrisé, mais on rappelle aussi que la transformation d’Abel trouve toute sa place dans cette leçon.',
                        'options' => 15,
                    ),
                    228 => 
                    array (
                        'id' => 729,
                        'created_at' => '2016-06-18 08:48:57',
                        'updated_at' => '2017-02-08 15:54:28',
                        'annee' => 2016,
                        'numero' => 232,
                        'type_id' => 2,
                    'nom' => ' Méthodes d\'approximation des solutions d\'une équation $F(X) = 0$. Exemples.',
                        'rapport' => 'Ce thème se retrouve naturellement dans la leçon 226.',
                        'options' => 15,
                    ),
                    229 => 
                    array (
                        'id' => 730,
                        'created_at' => '2016-06-18 08:49:30',
                        'updated_at' => '2017-02-08 15:55:51',
                        'annee' => 2016,
                        'numero' => 233,
                        'type_id' => 2,
                        'nom' => ' Analyse numérique matricielle : résolution approchée de systèmes linéaires, recherche de vecteurs propres, exemples.',
                        'rapport' => 'Cette leçon est reformulée pour la session 2017 au profit de la nouvelle leçon 233 suivante.

233 : Méthodes itératives en analyse numérique matricielle.

Dans cette leçon de synthèse, les notions de norme matricielle et de rayon spectral sont centrales, en lien avec le conditionnement et avec la convergence des méthodes itératives ; elles doivent être développées. Le résultat général de convergence, relié au théorème du point fixe de Banach, doit être enrichi de considérations sur la vitesse de convergence. Le jury invite les candidats à étudier diverses méthodes issues de contextes variés : résolution de systèmes linéaires, optimisation de fonctionnelles quadratiques, recherche de valeurs propres, ... Parmi les points intéressants à développer, on peut citer les méthodes de type Jacobi pour la résolution de systèmes linéaires, les méthodes de gradient dans le cadre quadratique, les méthodes de puissance pour la recherche de valeurs propres. Les candidats pourront également envisager les schémas numériques pour les équations différentielles ou aux dérivées partielles linéaires.',
                        'options' => 7,
                    ),
                    230 => 
                    array (
                        'id' => 731,
                        'created_at' => '2016-06-18 08:49:56',
                        'updated_at' => '2017-02-08 15:57:15',
                        'annee' => 2016,
                        'numero' => 234,
                        'type_id' => 2,
                        'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$. ',
                    'rapport' => 'Cette leçon nécessite d’avoir compris les notions de presque partout (comme par exemple les opérations sur les ensembles négligeables) et évidemment la définition des espaces $L^p$. Le jury a apprécié les candidats sachant montrer qu’avec une mesure finie $L^2 \\subset L^1$ (ou même $L^p  \\subset L^q$ si $p > q$). Il est important de pouvoir justifier l’existence de produits de convolution comme par exemple le produit de convolution de deux fonctions de $L^1$ ). Par ailleurs, les espaces associés à la mesure de comptage sur N
ou Z fournissent des exemples pertinents non triviaux à propos desquels des développements peuvent
être proposés comme la description du dual. Par ailleurs, des exemples issus des probabilités peuvent
tout à fait être mentionnés.
Pour aller plus loin, la complétude de $L^p$ (p fini ou infini) offre aussi un bon développement. On peut
aussi penser à certains résultats sur la dimension des sous-espaces fermés de $L^p$ dont les éléments ont
des propriétés particulières de régularité. Enfin, le cas particulier hilbertien $p = 2$ mérite attention
mais il faut se concentrer sur les spécificités d’un espace de fonctions $L^2$ et éviter de faire un catalogue
de propriétés vraies pour n’importe quel espace de Hilbert.',
                        'options' => 7,
                    ),
                    231 => 
                    array (
                        'id' => 732,
                        'created_at' => '2016-06-18 08:50:22',
                        'updated_at' => '2017-02-08 15:58:56',
                        'annee' => 2016,
                        'numero' => 236,
                        'type_id' => 2,
                        'nom' => ' Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonctions d\'une ou plusieurs variables.',
                    'rapport' => 'Cette leçon doit être très riche en exemples simples, comme l’intégrale $\\int_0^{+\\infty} \\frac{\\sin(t)}{t} dt$. Il est souhaitable de présenter des utilisations du théorème des résidus, ainsi que des exemples faisant intervenir les 
intégrales multiples comme le calcul de l’intégrale d’une gaussienne. Le calcul du volume de la boule unité de $R^n$ ne doit pas poser de problèmes insurmontables. Le calcul de la transformation de Fourier d’une gaussienne a sa place dans cette leçon.

On peut aussi penser à l’utilisation du théorème d’inversion de Fourier ou du théorème de Plancherel. Certains éléments de la leçon précédente, comme par exemple l’utilisation des théorèmes de convergence monotone, de convergence dominée et/ou de Fubini, sont aussi des outils permettant le calcul de certaines intégrales. ',
                        'options' => 15,
                    ),
                    232 => 
                    array (
                        'id' => 733,
                        'created_at' => '2016-06-18 08:50:49',
                        'updated_at' => '2017-02-08 15:59:44',
                        'annee' => 2016,
                        'numero' => 239,
                        'type_id' => 2,
                        'nom' => 'Fonctions définies par une inégrales dépendant d\'un paramètre. Exemples et applications. ',
                    'rapport' => 'Souvent les candidats incluent les théorèmes de régularité (version segment — a minima — mais aussi version “convergence dominée”) ce qui est pertinent. Cette leçon peut être enrichie par des études et méthodes de comportements asymptotiques. Les propriétés de la fonction $\\Gamma$ d’Euler fournissent un développement standard (il sera de bon ton d’y inclure le comportement asymptotique). Les différentes transformations classiques (Fourier, Laplace, . . .) relèvent aussi de cette leçon. On peut en donner des applications pour obtenir la valeur d’intégrales classiques (celle de l’intégrale de Dirichlet par exemple). 

Pour aller plus loin, on peut par exemple développer les propriétés des transformations mentionnées (notamment Fourier), ainsi que de la convolution.',
                        'options' => 15,
                    ),
                    233 => 
                    array (
                        'id' => 734,
                        'created_at' => '2016-06-18 08:51:11',
                        'updated_at' => '2017-02-08 16:25:26',
                        'annee' => 2016,
                        'numero' => 240,
                        'type_id' => 2,
                        'nom' => 'Produit de convolution, transformation de Fourier. Applications. ',
                        'rapport' => 'Cette leçon, fusionnée avec la 254, est remplacée par la 250 dont voici le rapport.

Cette leçon, reformulée pour la session 2017, offre de multiples facettes. Les candidats peuvent adopter différents points de vue : $L^1$, $L^2$ et/ou distributions. L’aspect “séries de Fourier” n’est toutefois pas dans l’esprit de cette leçon ; précisons aussi qu’il ne s’agit pas de faire de l’analyse de Fourier sur n’importe quel groupe localement compact mais bien sur $R$ ou $R^d$ . La leçon nécessite une bonne maîtrise de questions de base telle que la définition du produit de convolution de deux fonctions de $L^1$ . En ce qui concerne la transformation de Fourier, elle ne doit pas se limiter à une analyse algébrique de la transformation de Fourier. C’est bien une leçon d’analyse, qui nécessite une étude soigneuse des hypothèses, des définitions et de la nature des objets manipulés. Le lien entre la régularité de la fonction et la décroissance de sa transformée de Fourier doit être fait,
même sous des hypothèses qui ne sont pas minimales. 

La formule d’inversion de Fourier pour une fonction $L^1$ dont la transformée de Fourier est aussi $L^1$ sont attendues ainsi que l’extension de la transformée de Fourier à l’espace $L^2$ par Fourier-Plancherel. Des exemples explicites de calcul de transformations de Fourier, classiques comme la gaussienne ou $(1+x^2)^{-1}$ ,  paraissent nécessaires. 

Pour aller plus loin, la transformation de Fourier des distributions tempérées ainsi que la convolution dans le cadre des distributions tempérées peuvent être abordées. Rappelons une fois de plus que les attentes du jury sur ces questions restent modestes, au niveau de ce qu’un cours de première année de master sur le sujet peut contenir. Le fait que la transformée de Fourier envoie $S(R^d)$ dans lui même avec de bonnes estimations des semi-normes doit alors être compris et la formule d’inversion de Fourier maîtrisée dans ce cadre. Des exemples de calcul de transformée de Fourier peuvent être données dans des contextes liés à la théorie des distributions comme par exemple la transformée de Fourier de la valeur principale.

La résolution de certaines équations aux dérivées partielles telle que, par exemple, l’équation de la chaleur, peut être abordée, avec une discussion sur les propriétés qualitatives des solutions.',
                        'options' => 15,
                    ),
                    234 => 
                    array (
                        'id' => 735,
                        'created_at' => '2016-06-18 08:51:38',
                        'updated_at' => '2017-02-08 16:00:39',
                        'annee' => 2016,
                        'numero' => 241,
                        'type_id' => 2,
                        'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples. ',
                        'rapport' => 'Une fois les résultats généraux énoncés, on attend du candidat qu’il évoque les séries de fonctions particulières classiques : séries entières, séries de Fourier. On pourra éventuellement s’intéresser aussi aux séries de Dirichlet. Il y a beaucoup de développements possibles et les candidats n’ont généralement aucun mal à trouver des idées que ce soit à un niveau élémentaire mais fourni en exemples pertinents ou plus avancé, voire nécessitant une certaine technicité. Par exemple, les théorèmes taubériens offrent une belle palette de développements.

Par ailleurs, la leçon n’exclut pas du tout de s’intéresser au comportement des suites et séries de fonctions dans les espaces de type $L^p$ (notamment pour $p=1$), ou encore aux séries de variables aléatoires indépendantes.',
                        'options' => 7,
                    ),
                    235 => 
                    array (
                        'id' => 736,
                        'created_at' => '2016-06-18 08:52:05',
                        'updated_at' => '2017-02-08 16:02:02',
                        'annee' => 2016,
                        'numero' => 243,
                        'type_id' => 2,
                        'nom' => 'Convergence des séries entières, propriétés de la somme. Exemples et applications. ',
                    'rapport' => 'Les candidats évoquent souvent des critères (Cauchy, D’Alembert) permettant d’estimer le rayon de convergence mais oublient souvent la formule de Cauchy-Hadamard. Le jury attend bien sûr que le candidat puisse donner des arguments justifiant qu’une série entière en 0 dont le rayon de convergence est R est développable en série entière en un point $z_0$ intérieur au disque de convergence et de minorer le rayon de convergence de cette série. Sans tomber dans un catalogue excessif, on peut indiquer les formules de développement de fonctions usuelles importantes ($\\exp$, $\\log$, $1/(1-z)$, $\\sin$, ...). Le jury attend également que le candidat puisse les donner sans consulter ses notes. En ce qui concerne la fonction exponentielle, le candidat doit avoir réfléchi au point de vue adopté sur sa définition et donc sur l’articulation entre l’obtention du développement en série entière et les propriétés de la fonction. À ce propos, les résultats sur l’existence du développement en série entière pour les fonctions dont on contrôle toutes les dérivées successives sur un voisinage de 0 sont souvent méconnus.

Le théorème d’Abel (radial ou sectoriel) trouve toute sa place mais doit être agrémenté d’exercices pertinents. Réciproquement, les théorèmes taubériens offrent aussi de jolis développements. On pourra aller plus loin en abordant quelques propriétés importantes liées à l’analyticité de la somme d’une série entière.',
                        'options' => 15,
                    ),
                    236 => 
                    array (
                        'id' => 737,
                        'created_at' => '2016-06-18 08:52:34',
                        'updated_at' => '2017-02-08 16:02:16',
                        'annee' => 2016,
                        'numero' => 244,
                        'type_id' => 2,
                        'nom' => 'Fonctions développables en série entière, fonctions analytiques. Exemples. ',
                        'rapport' => 'Ce thème se retrouve naturellement dans les leçons 243 et 245.',
                        'options' => 7,
                    ),
                    237 => 
                    array (
                        'id' => 738,
                        'created_at' => '2016-06-18 08:54:32',
                        'updated_at' => '2017-02-08 16:14:06',
                        'annee' => 2016,
                        'numero' => 245,
                        'type_id' => 2,
                        'nom' => 'Fonctions holomorphes sur un ouvert de $C$. Exemples et applications. ',
                    'rapport' => 'Les conditions de Cauchy-Riemann doivent être parfaitement connues ş et l’interprétation de la différentielle en tant que similitude directe doit être comprise. La notation $\\int_\\gamma f(z) dz$ a un sens précis, qu’il faut savoir expliquer. Par ailleurs, même si cela ne constitue pas le cœur de la leçon, il faut connaître la définition d’une fonction méromorphe (l’ensemble des pôles doit être une partie fermée discrète).

Les résultats autour de l’analyticité, ou encore le principe du maximum, le principe des zéros isolés, sont bien sûr cruciaux. Le lemme de Schwarz est un joli résultat permettant de faire un développement élémentaire s’il est agrémenté d’applications pertinentes, comme par exemple déterminer les automorphismes du disque unité.

Pour les candidats qui le souhaitent, cette leçon offre beaucoup de possibilités, notamment en lien avec la topologie du plan. La preuve du théorème de l’application conforme de Riemann est par exemple un  développement de très bon niveau mais qui nécessite une bonne maîtrise.',
                        'options' => 7,
                    ),
                    238 => 
                    array (
                        'id' => 739,
                        'created_at' => '2016-06-18 08:54:57',
                        'updated_at' => '2017-02-08 16:15:06',
                        'annee' => 2016,
                        'numero' => 246,
                        'type_id' => 2,
                        'nom' => 'Séries de Fourier. Exemples et applications. ',
                    'rapport' => 'Les différents résultats autour de la convergence ($L^2$ , Fejér, Dirichlet, . . .) doivent être connus. Il faut avoir les idées claires sur la notion de fonctions de classe $C^1$ par morceaux (elles ne sont pas forcément continues). Dans le cas d’une fonction continue et $C^1$ par morceaux on peut conclure sur la convergence normale de la série Fourier sans utiliser le théorème de Dirichlet. Il est classique d’obtenir des sommes de séries remarquables comme conséquence de ces théorèmes. On peut aussi s’intéresser à la formule de Poisson et à ses conséquences. L’existence d’exemples de séries de Fourier divergentes, associées à des fonctions continues (qu’ils soient explicites ou obtenus par des techniques d’analyse fonctionnelle) peuvent aussi compléter le contenu. 

Mais il est souhaitable que cette leçon ne se réduise pas à un cours abstrait sur les coefficients de Fourier. La résolution d’équations aux dérivées partielles (par exemple l’équation de la chaleur) peuvent illustrer de manière pertinente cette leçon, mais on peut penser à bien d’autres applications (inégalité isopérimétrique, comportements remarquables des fonctions à spectre lacunaire, ...).',
                        'options' => 15,
                    ),
                    239 => 
                    array (
                        'id' => 740,
                        'created_at' => '2016-06-18 08:55:59',
                        'updated_at' => '2016-06-18 08:55:59',
                        'annee' => 2016,
                        'numero' => 247,
                        'type_id' => 2,
                        'nom' => ' Exemples de problèmes d\'interversion de limites.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    240 => 
                    array (
                        'id' => 741,
                        'created_at' => '2016-06-18 08:56:25',
                        'updated_at' => '2017-02-08 16:15:45',
                        'annee' => 2016,
                        'numero' => 249,
                        'type_id' => 2,
                        'nom' => 'Suite de variables aléatoires de Bernoulli indépendantes. ',
                        'rapport' => 'Les éléments de cette leçon trouvent naturellement leur place dans la leçon 264, ainsi que dans les autres leçons de probabilités.',
                        'options' => 7,
                    ),
                    241 => 
                    array (
                        'id' => 742,
                        'created_at' => '2016-06-18 08:56:49',
                        'updated_at' => '2017-02-08 16:16:58',
                        'annee' => 2016,
                        'numero' => 253,
                        'type_id' => 2,
                        'nom' => 'Utilisation de la notion de convexité en analyse. ',
                    'rapport' => 'Il s’agit d’une leçon de synthèse, très riche, qui mérite une préparation soigneuse. Même si localement (notamment lors de la phase de présentation orale) des rappels sur la convexité peuvent être énoncés, ceci n’est pas attendu dans le plan. Il s’agit d’aborder différents champs des mathématiques où la convexité intervient. On pensera bien sûr, sans que ce soit exhaustif, aux problèmes d’optimisation, au théorème de projection sur un convexe fermé, au rôle joué par la convexité dans les espaces vectoriels normés (convexité de la norme, jauge d’un convexe,...). Les fonctions convexes élémentaires permettent aussi d’obtenir des inégalités célèbres. On retrouve aussi ce type d’argument pour justifier des inégalités de type Brunn-Minkowski ou Hadamard. Par ailleurs, l’inégalité de Jensen a aussi des applications en intégration et en probabilités.

Pour aller plus loin, on peut mettre en évidence le rôle joué par la convexité dans le théorème de séparation de Hahn-Banach. On peut aussi parler des propriétés d’uniforme convexité dans certains espaces, les espaces $L^p$ pour $ p > 1$, par exemple, et de leurs conséquences.',
                        'options' => 7,
                    ),
                    242 => 
                    array (
                        'id' => 743,
                        'created_at' => '2016-06-18 08:57:24',
                        'updated_at' => '2017-02-08 16:17:17',
                        'annee' => 2016,
                        'numero' => 254,
                        'type_id' => 2,
                    'nom' => 'Espaces de Schwartz $S(R^d)$ et distributions tempérées. Dérivation et transformation de Fourier dans $S(R^d)$ et $S\'(R^d)$. ',
                        'rapport' => 'Ce thème se retrouve naturellement dans les leçons 222, 236, 239, 250. Cette liste n’étant pas exhaustive.',
                        'options' => 7,
                    ),
                    243 => 
                    array (
                        'id' => 744,
                        'created_at' => '2016-06-18 08:57:48',
                        'updated_at' => '2017-02-08 16:17:53',
                        'annee' => 2016,
                        'numero' => 260,
                        'type_id' => 2,
                        'nom' => 'Espérance, variance et moments de variables aléatoires. ',
                    'rapport' => 'Le jury attend des candidats qu’ils donnent la définition des moments centrés, qu’ils rappellent les implications d’existence de moments (décroissance des L p ). Le candidat peut citer — mais doit surtout savoir retrouver rapidement — les espérances et variances de lois usuelles, notamment Bernoulli, binômiale, géométrique, Poisson, exponentielle, normale. La variance de la somme de variables aléatoires indépendantes suscite souvent des hésitations. Les inégalités classiques (de Markov, de Bienaymé-Chebychev, de Jensen et de Cauchy-Schwarz) pourront être données, ainsi que les théorèmes de convergence (loi des grands nombres et théorème central limite). La notion de fonction génératrice des moments pourra être présentée ainsi que les liens entre moments et fonction caractéristique. 

Pour aller plus loin, le comportement des moyennes pour une suite de variables aléatoires indépendantes et identiquement distribuées n’admettant pas d’espérance pourra être étudié. Pour les candidats suffisamment à l’aise avec ce sujet, l’espérance conditionnelle pourra aussi être abordée.',
                        'options' => 15,
                    ),
                    244 => 
                    array (
                        'id' => 745,
                        'created_at' => '2016-06-18 08:58:17',
                        'updated_at' => '2017-02-08 16:18:17',
                        'annee' => 2016,
                        'numero' => 261,
                        'type_id' => 2,
                        'nom' => 'Fonction caractéristique et transformée de Laplace d\'une variable aléatoire. Exemples et applications. ',
                        'rapport' => 'Les candidats pourront présenter l’utilisation de la fonction caractéristique pour le calcul de lois de sommes de variables aléatoires indépendantes et faire le lien entre la régularité de la fonction caractéristique et l’existence de moments. Le candidat doit être en mesure de calculer la fonction caractéristique des lois usuelles. Le jury attend l’énoncé du théorème de Lévy, que les candidats en comprennent la portée, et son utilisation dans la démonstration du théorème central limite.

Pour aller plus loin, des applications pertinentes de ces résultats seront les bienvenues. Enfin, la transformée de Laplace pourra être utilisée pour établir des inégalités de grandes déviations.',
                        'options' => 7,
                    ),
                    245 => 
                    array (
                        'id' => 746,
                        'created_at' => '2016-06-18 09:01:58',
                        'updated_at' => '2017-02-08 16:18:47',
                        'annee' => 2016,
                        'numero' => 262,
                        'type_id' => 2,
                        'nom' => 'Modes de convergence d\'une suite de variables aléatoires. Exemples et applications. ',
                    'rapport' => 'Les implications entre les divers modes de convergence, ainsi que les réciproques partielles doivent être connues. Des contre-exemples aux réciproques sont attendus par le jury. Les théorèmes de convergence (lois des grands nombres et théorème central limite) doivent être énoncés. On peut par ailleurs exiger de connaître au moins l’architecture des preuves. L’étude de maximum et minimum de n variables aléatoires indépendantes et de même loi peut nourrir de nombreux exemples. 

Pour aller plus loin, les candidats pourront s’intéresser au comportement asymptotique de marches aléatoires (en utilisant par exemple le lemme de Borel-Cantelli, les fonctions génératrices, . . .) ou donner des inégalités de grandes déviations. Enfin, les résultats autour des séries de variables aléatoires indépendantes comme le théorème de Kolmogorov peuvent tout à fait se placer dans cette leçon.',
                        'options' => 7,
                    ),
                    246 => 
                    array (
                        'id' => 747,
                        'created_at' => '2016-06-18 09:02:31',
                        'updated_at' => '2017-02-08 16:19:39',
                        'annee' => 2016,
                        'numero' => 263,
                        'type_id' => 2,
                        'nom' => 'Variables aléatoires à densité. Exemples et applications. ',
                        'rapport' => 'Le jury attend des candidats qu’ils rappellent la définition d’une variable aléatoire à densité et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. Les très bons candidats auront en tête le théorème de Radon-Nikodym, même s’il ne s’agit pas de faire un cours abstrait sur l’absolue continuité. Le lien entre indépendance et produit des densités est un outil important. Le lien entre la somme de variables indépendantes et la convolution de leurs densités est trop souvent oublié. Ce résultat général peut être illustré par des exemples issus des lois usuelles. Les candidats pourront expliquer comment fabriquer n’importe quelle variable aléatoire à partir d’une variable uniforme sur $[0,1]$.

Les candidats proposent parfois en développement la caractérisation de la loi exponentielle comme étant l’unique loi absolument continue sans mémoire : c’est une bonne idée de développement de niveau élémentaire pour autant que les hypothèses soient bien posées et toutes les étapes bien justifiées. On pourra pousser ce développement à un niveau supérieur en s’intéressant au minimum ou aux sommes de telles lois. La preuve du théorème de Scheffé sur la convergence en loi peut aussi faire l’objet d’un développement. La loi de Cauchy offre encore des idées de développements intéressants (par exemple en la reliant au quotient de deux variables  aléatoires indépendantes suivant une loi normale centrée).

Pour aller plus loin, les candidats pourront aborder la notion de vecteurs gaussiens et son lien avec le théorème central limite. On peut aussi proposer en développement le théorème de Cochran.',
                        'options' => 7,
                    ),
                    247 => 
                    array (
                        'id' => 748,
                        'created_at' => '2016-06-18 09:03:01',
                        'updated_at' => '2017-02-08 16:20:18',
                        'annee' => 2016,
                        'numero' => 264,
                        'type_id' => 2,
                        'nom' => 'Variables aléatoires discrètes. Exemples et applications. ',
                        'rapport' => 'Le jury attend des candidats qu’ils rappellent la définition d’une variable aléatoire discrète et que des lois usuelles soient présentées, en lien avec des exemples classiques de modélisation. Le lien entre variables aléatoires de Bernoulli, binômiale et de Poisson doit être discuté. Il peut être d’ailleurs intéressant de mettre en avant le rôle central joué par les variables aléatoires de Bernoulli. 

Les techniques spécifiques aux variables discrètes, notamment à valeurs entières, devront être mises en évidence, comme par exemple la caractérisation de la convergence en loi. 

Pour aller plus loin, la notion de fonction génératrice pourra être abordée. Le processus de Galton-Watson peut se traiter intégralement par fonctions génératrices et cette voie a été choisie par plusieurs
candidats : cela donne un développement de très bon niveau pour ceux qui savent justifier les étapes délicates.

Pour aller beaucoup plus loin, les candidats pourront étudier les marches aléatoires, les chaînes de Markov à espaces d’états finis ou dénombrables, les sommes ou séries de variables aléatoires indépendantes.',
                        'options' => 15,
                    ),
                    248 => 
                    array (
                        'id' => 749,
                        'created_at' => '2016-06-18 09:03:45',
                        'updated_at' => '2016-06-18 09:03:45',
                        'annee' => 2016,
                        'numero' => 901,
                        'type_id' => 3,
                        'nom' => 'Structure de données : exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    249 => 
                    array (
                        'id' => 750,
                        'created_at' => '2016-06-18 09:04:07',
                        'updated_at' => '2016-06-18 09:04:07',
                        'annee' => 2016,
                        'numero' => 902,
                        'type_id' => 3,
                        'nom' => 'Diviser pour régner : exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    250 => 
                    array (
                        'id' => 751,
                        'created_at' => '2016-06-18 09:04:33',
                        'updated_at' => '2016-06-18 09:04:33',
                        'annee' => 2016,
                        'numero' => 903,
                        'type_id' => 3,
                        'nom' => 'Exemples d\'algorithmes de tri. Complexité. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    251 => 
                    array (
                        'id' => 752,
                        'created_at' => '2016-06-18 09:05:12',
                        'updated_at' => '2016-06-18 09:05:12',
                        'annee' => 2016,
                        'numero' => 906,
                        'type_id' => 3,
                        'nom' => 'Programmation dynamique : exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    252 => 
                    array (
                        'id' => 753,
                        'created_at' => '2016-06-18 09:05:35',
                        'updated_at' => '2016-06-18 09:05:35',
                        'annee' => 2016,
                        'numero' => 907,
                        'type_id' => 3,
                        'nom' => 'Algorithmique du texte : exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    253 => 
                    array (
                        'id' => 754,
                        'created_at' => '2016-06-18 09:06:15',
                        'updated_at' => '2016-06-18 09:06:15',
                        'annee' => 2016,
                        'numero' => 909,
                        'type_id' => 3,
                        'nom' => 'Langages rationnels. Exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    254 => 
                    array (
                        'id' => 755,
                        'created_at' => '2016-06-18 09:07:38',
                        'updated_at' => '2016-06-18 09:07:38',
                        'annee' => 2016,
                        'numero' => 910,
                        'type_id' => 3,
                        'nom' => 'Langages algébriques. Exemples et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    255 => 
                    array (
                        'id' => 756,
                        'created_at' => '2016-06-18 09:08:39',
                        'updated_at' => '2016-06-18 09:08:39',
                        'annee' => 2016,
                        'numero' => 912,
                        'type_id' => 3,
                        'nom' => 'Fonctions récursives primitives et non primitives. Exemples. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    256 => 
                    array (
                        'id' => 757,
                        'created_at' => '2016-06-18 09:09:17',
                        'updated_at' => '2016-06-18 09:09:17',
                        'annee' => 2016,
                        'numero' => 913,
                        'type_id' => 3,
                        'nom' => 'Machines de Turing. Applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    257 => 
                    array (
                        'id' => 758,
                        'created_at' => '2016-06-18 09:10:01',
                        'updated_at' => '2016-06-18 09:10:01',
                        'annee' => 2016,
                        'numero' => 915,
                        'type_id' => 3,
                        'nom' => ' Classes de complexité : exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    258 => 
                    array (
                        'id' => 759,
                        'created_at' => '2016-06-18 09:10:31',
                        'updated_at' => '2016-06-18 09:10:31',
                        'annee' => 2016,
                        'numero' => 916,
                        'type_id' => 3,
                        'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfiabilité. Applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    259 => 
                    array (
                        'id' => 760,
                        'created_at' => '2016-06-18 09:11:06',
                        'updated_at' => '2016-06-18 09:11:06',
                        'annee' => 2016,
                        'numero' => 917,
                        'type_id' => 3,
                        'nom' => ' Logique du premier ordre : syntaxe et sémantique.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    260 => 
                    array (
                        'id' => 761,
                        'created_at' => '2016-06-18 09:11:31',
                        'updated_at' => '2016-06-18 09:11:31',
                        'annee' => 2016,
                        'numero' => 918,
                        'type_id' => 3,
                        'nom' => 'Systèmes formels de preuve en logique du premier ordre : exemples. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    261 => 
                    array (
                        'id' => 762,
                        'created_at' => '2016-06-18 09:11:57',
                        'updated_at' => '2016-06-18 09:11:57',
                        'annee' => 2016,
                        'numero' => 919,
                        'type_id' => 3,
                        'nom' => 'Unification : algorithmes et applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    262 => 
                    array (
                        'id' => 763,
                        'created_at' => '2016-06-18 09:12:23',
                        'updated_at' => '2016-06-18 09:12:23',
                        'annee' => 2016,
                        'numero' => 920,
                        'type_id' => 3,
                        'nom' => 'Réécriture et formes normales. Exemples. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    263 => 
                    array (
                        'id' => 764,
                        'created_at' => '2016-06-18 09:12:53',
                        'updated_at' => '2016-06-18 09:12:53',
                        'annee' => 2016,
                        'numero' => 921,
                        'type_id' => 3,
                        'nom' => 'Algorithmes de recherche et structures de données associées. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    264 => 
                    array (
                        'id' => 765,
                        'created_at' => '2016-06-18 09:13:24',
                        'updated_at' => '2016-06-18 09:13:24',
                        'annee' => 2016,
                        'numero' => 923,
                        'type_id' => 3,
                        'nom' => 'Analyses lexicale et syntaxique : applications. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    265 => 
                    array (
                        'id' => 766,
                        'created_at' => '2016-06-18 09:13:51',
                        'updated_at' => '2016-06-18 09:13:51',
                        'annee' => 2016,
                        'numero' => 924,
                        'type_id' => 3,
                        'nom' => 'Théories et modèles en logique du premier ordre. Exemples. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    266 => 
                    array (
                        'id' => 767,
                        'created_at' => '2016-06-18 09:14:14',
                        'updated_at' => '2016-06-18 09:14:14',
                        'annee' => 2016,
                        'numero' => 925,
                        'type_id' => 3,
                        'nom' => 'Graphes : représentations et algorithmes. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    267 => 
                    array (
                        'id' => 768,
                        'created_at' => '2016-06-18 09:14:45',
                        'updated_at' => '2016-06-18 09:14:45',
                        'annee' => 2016,
                        'numero' => 926,
                        'type_id' => 3,
                        'nom' => ' Analyse des algorithmes : complexité. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    268 => 
                    array (
                        'id' => 769,
                        'created_at' => '2016-06-18 09:15:12',
                        'updated_at' => '2016-06-18 09:15:12',
                        'annee' => 2016,
                        'numero' => 927,
                        'type_id' => 3,
                        'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison. ',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    269 => 
                    array (
                        'id' => 770,
                        'created_at' => '2016-06-18 09:15:43',
                        'updated_at' => '2016-06-18 09:15:43',
                        'annee' => 2016,
                        'numero' => 928,
                        'type_id' => 3,
                        'nom' => ' Problème NP-complets : exemples de réductions.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    270 => 
                    array (
                        'id' => 771,
                        'created_at' => '2016-06-18 09:21:22',
                        'updated_at' => '2017-02-08 16:22:41',
                        'annee' => 2016,
                        'numero' => 224,
                        'type_id' => 2,
                        'nom' => 'Exemples de développements asymptotiques de suites et de fonctions.',
                        'rapport' => 'Cette leçon doit permettre aux candidats d’exprimer leur savoir-faire sur les techniques d’analyse élémentaire que ce soit sur les suites, les séries ou les intégrales. On peut par exemple établir un développement asymptotique à quelques termes des sommes partielles de la série harmonique, ou bien la formule de Stirling que ce soit dans sa version factorielle ou pour la fonction Γ. On peut également s’intéresser aux comportements autour des singularités de fonctions spéciales célèbres. Du côté de l’intégration, on peut évaluer la vitesse de divergence de l’intégrale de la valeur absolue du sinus cardinal, avec des applications pour les séries de Fourier ; voire présenter la méthode de Laplace. 

Par ailleurs, le thème de la leçon permet l’étude de suites récurrentes (autres que $u_{n+1} = \\sin(u_n)$), plus généralement de suites ou de fonctions définies implicitement, ou encore des études asymptotiques de solutions d’équations différentielles (sans résolution explicite).',
                        'options' => 15,
                    ),
                    271 => 
                    array (
                        'id' => 772,
                        'created_at' => '2016-06-18 09:22:36',
                        'updated_at' => '2017-02-08 16:22:16',
                        'annee' => 2016,
                        'numero' => 235,
                        'type_id' => 2,
                        'nom' => 'Problèmes d\'interversion de limites et d\'intégrales.',
                    'rapport' => 'Cette leçon s’intéresse aux problèmes d’interversion limite-limite, limite-intégrale et intégrale-intégrale. Il ne s’agit pas de refaire un cours d’intégration. On pourra toutefois mettre en évidence le rôle important joué par des théorèmes cruciaux de ce cours. À un niveau élémentaire, on peut insister sur le rôle de la convergence uniforme, ou de la convergence normale (dans le cas de séries de fonctions).

À un niveau plus avancé, les théorèmes de convergence dominée, de convergence monotone et le théorème de Fubini (et Fubini-Tonelli) ont leur place dans cette leçon. On choisira des exemples pertinents pour illustrer l’intérêt de chacun de ces réultats, mais on pourra aussi exhiber des contre-exemples montrant que des hypothèses trop faibles ne permettent pas en général d’effectuer l’interversion tant désirée. Pour les candidats qui le souhaitent, on pourra parler de la transformée de Fourier et/ou de la transformée de Laplace.',
                        'options' => 7,
                    ),
                    272 => 
                    array (
                        'id' => 773,
                        'created_at' => '2016-06-18 09:26:29',
                        'updated_at' => '2016-06-18 09:26:29',
                        'annee' => 2016,
                        'numero' => 914,
                        'type_id' => 3,
                        'nom' => 'Décidabilité et indécidabilité. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    273 => 
                    array (
                        'id' => 774,
                        'created_at' => '2016-06-18 09:27:38',
                        'updated_at' => '2016-06-18 09:27:38',
                        'annee' => 2016,
                        'numero' => 922,
                        'type_id' => 3,
                        'nom' => 'Ensembles récursifs, récursivement énumérables. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    274 => 
                    array (
                        'id' => 775,
                        'created_at' => '2016-12-19 09:15:25',
                        'updated_at' => '2016-12-19 09:15:25',
                        'annee' => 2017,
                        'numero' => 101,
                        'type_id' => 1,
                        'nom' => 'Groupe opérant sur un ensemble. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    275 => 
                    array (
                        'id' => 776,
                        'created_at' => '2016-12-19 09:16:15',
                        'updated_at' => '2016-12-19 09:16:15',
                        'annee' => 2017,
                        'numero' => 102,
                        'type_id' => 1,
                        'nom' => 'Groupe des nombres complexes de modules $1$. Sous-groupes des racines de l\'unité. Applications',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    276 => 
                    array (
                        'id' => 777,
                        'created_at' => '2016-12-19 09:17:04',
                        'updated_at' => '2016-12-19 09:17:04',
                        'annee' => 2017,
                        'numero' => 103,
                        'type_id' => 1,
                        'nom' => 'Exemples de sous-groupes distingués et de groupes quotients. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    277 => 
                    array (
                        'id' => 778,
                        'created_at' => '2016-12-19 09:17:40',
                        'updated_at' => '2016-12-19 09:17:40',
                        'annee' => 2017,
                        'numero' => 104,
                        'type_id' => 1,
                        'nom' => 'Groupes finis. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    278 => 
                    array (
                        'id' => 779,
                        'created_at' => '2016-12-19 09:18:14',
                        'updated_at' => '2016-12-19 09:18:14',
                        'annee' => 2017,
                        'numero' => 105,
                        'type_id' => 1,
                        'nom' => 'Groupe des permutations d\'un ensemble fini. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    279 => 
                    array (
                        'id' => 780,
                        'created_at' => '2016-12-19 09:18:58',
                        'updated_at' => '2016-12-19 09:18:58',
                        'annee' => 2017,
                        'numero' => 106,
                        'type_id' => 1,
                    'nom' => 'Groupe linéaire d\'un espace vectoriel de dimension finie $E$, sous-groupes de $GL(E)$. Applications',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    280 => 
                    array (
                        'id' => 781,
                        'created_at' => '2016-12-19 09:20:03',
                        'updated_at' => '2016-12-19 09:20:03',
                        'annee' => 2017,
                        'numero' => 107,
                        'type_id' => 1,
                        'nom' => 'Représentations et caractères d\'un groupe fini sur un $\\mathbb{C}$-espace vectoriel. Exemples.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    281 => 
                    array (
                        'id' => 782,
                        'created_at' => '2016-12-19 09:20:44',
                        'updated_at' => '2016-12-19 09:20:44',
                        'annee' => 2017,
                        'numero' => 108,
                        'type_id' => 1,
                        'nom' => 'Exemples de parties génératrices d\'un groupe. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    282 => 
                    array (
                        'id' => 783,
                        'created_at' => '2016-12-19 09:21:29',
                        'updated_at' => '2016-12-19 09:21:29',
                        'annee' => 2017,
                        'numero' => 110,
                        'type_id' => 1,
                        'nom' => 'Caractères d\'un groupe abélien fini et transformée de Fourier discrète. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    283 => 
                    array (
                        'id' => 784,
                        'created_at' => '2016-12-19 09:21:58',
                        'updated_at' => '2016-12-19 09:45:30',
                        'annee' => 2017,
                        'numero' => 120,
                        'type_id' => 1,
                        'nom' => 'Anneaux $\\mathbb{Z}/n\\mathbb{Z}$. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    284 => 
                    array (
                        'id' => 785,
                        'created_at' => '2016-12-19 09:22:20',
                        'updated_at' => '2016-12-19 09:23:20',
                        'annee' => 2017,
                        'numero' => 121,
                        'type_id' => 1,
                        'nom' => 'Nombres premiers. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    285 => 
                    array (
                        'id' => 786,
                        'created_at' => '2016-12-19 09:22:55',
                        'updated_at' => '2016-12-19 09:22:55',
                        'annee' => 2017,
                        'numero' => 122,
                        'type_id' => 1,
                        'nom' => 'Anneaux principaux. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    286 => 
                    array (
                        'id' => 787,
                        'created_at' => '2016-12-19 09:23:59',
                        'updated_at' => '2016-12-19 09:23:59',
                        'annee' => 2017,
                        'numero' => 123,
                        'type_id' => 1,
                        'nom' => 'Corps finis. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    287 => 
                    array (
                        'id' => 788,
                        'created_at' => '2016-12-19 09:24:41',
                        'updated_at' => '2016-12-19 09:24:41',
                        'annee' => 2017,
                        'numero' => 125,
                        'type_id' => 1,
                        'nom' => 'Extension de corps. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    288 => 
                    array (
                        'id' => 789,
                        'created_at' => '2016-12-19 09:25:14',
                        'updated_at' => '2016-12-19 09:25:14',
                        'annee' => 2017,
                        'numero' => 126,
                        'type_id' => 1,
                        'nom' => 'Exemples d\'équations diophantiennes.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    289 => 
                    array (
                        'id' => 790,
                        'created_at' => '2016-12-19 09:25:59',
                        'updated_at' => '2016-12-19 09:26:14',
                        'annee' => 2017,
                        'numero' => 141,
                        'type_id' => 1,
                        'nom' => 'Polynômes irréductibles à une indéterminée. Corps de rupture. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    290 => 
                    array (
                        'id' => 791,
                        'created_at' => '2016-12-19 09:26:49',
                        'updated_at' => '2016-12-19 09:26:49',
                        'annee' => 2017,
                        'numero' => 142,
                        'type_id' => 1,
                        'nom' => 'Algèbre des polynômes à plusieurs indéterminées. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    291 => 
                    array (
                        'id' => 792,
                        'created_at' => '2016-12-19 09:27:38',
                        'updated_at' => '2016-12-19 09:27:38',
                        'annee' => 2017,
                        'numero' => 144,
                        'type_id' => 1,
                        'nom' => 'Racines d\'un polynôme. Fonctions symétriques élémentaires. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    292 => 
                    array (
                        'id' => 793,
                        'created_at' => '2016-12-19 09:28:32',
                        'updated_at' => '2016-12-19 09:28:32',
                        'annee' => 2017,
                        'numero' => 150,
                        'type_id' => 1,
                        'nom' => 'Exemples d\'actions de groupes sur les espaces de matrices.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    293 => 
                    array (
                        'id' => 794,
                        'created_at' => '2016-12-19 09:29:15',
                        'updated_at' => '2016-12-19 09:29:15',
                        'annee' => 2017,
                        'numero' => 151,
                        'type_id' => 1,
                    'nom' => 'Dimension d\'un espace vectoriel (on se limitera au cas de la dimension finie). Rang. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    294 => 
                    array (
                        'id' => 795,
                        'created_at' => '2016-12-19 09:29:41',
                        'updated_at' => '2016-12-19 09:29:41',
                        'annee' => 2017,
                        'numero' => 152,
                        'type_id' => 1,
                        'nom' => 'Déterminant. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    295 => 
                    array (
                        'id' => 796,
                        'created_at' => '2016-12-19 09:30:41',
                        'updated_at' => '2016-12-19 09:30:41',
                        'annee' => 2017,
                        'numero' => 153,
                        'type_id' => 1,
                        'nom' => 'Polynômes d\'endomorphisme en dimension finie. Réduction d\'un endomorphisme en dimension finie. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    296 => 
                    array (
                        'id' => 797,
                        'created_at' => '2016-12-19 09:31:46',
                        'updated_at' => '2016-12-19 09:31:46',
                        'annee' => 2017,
                        'numero' => 154,
                        'type_id' => 1,
                        'nom' => 'Sous-espaces stables par un endomorphisme ou une famille d\'endomorphismes d\'un espace vectoriel de dimension finie. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    297 => 
                    array (
                        'id' => 798,
                        'created_at' => '2016-12-19 09:32:43',
                        'updated_at' => '2016-12-19 09:32:43',
                        'annee' => 2017,
                        'numero' => 155,
                        'type_id' => 1,
                        'nom' => 'Endomorphismes diagonalisables en dimension finie.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    298 => 
                    array (
                        'id' => 799,
                        'created_at' => '2016-12-19 09:33:10',
                        'updated_at' => '2016-12-19 09:33:10',
                        'annee' => 2017,
                        'numero' => 156,
                        'type_id' => 1,
                        'nom' => 'Exponentielle de matrices. Applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    299 => 
                    array (
                        'id' => 800,
                        'created_at' => '2016-12-19 09:33:55',
                        'updated_at' => '2016-12-19 09:46:44',
                        'annee' => 2017,
                        'numero' => 157,
                        'type_id' => 1,
                        'nom' => 'Endomorphismes trigonalisables. Endomorphismes nilpotents.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    300 => 
                    array (
                        'id' => 801,
                        'created_at' => '2016-12-19 09:34:34',
                        'updated_at' => '2016-12-19 09:34:34',
                        'annee' => 2017,
                        'numero' => 158,
                        'type_id' => 1,
                        'nom' => 'Matrices symétriques réelles, matrices hermitiennes.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    301 => 
                    array (
                        'id' => 802,
                        'created_at' => '2016-12-19 09:35:17',
                        'updated_at' => '2016-12-19 09:35:17',
                        'annee' => 2017,
                        'numero' => 159,
                        'type_id' => 1,
                        'nom' => 'Formes linéaires et dualité en dimension finie. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    302 => 
                    array (
                        'id' => 803,
                        'created_at' => '2016-12-19 09:36:01',
                        'updated_at' => '2016-12-19 09:36:01',
                        'annee' => 2017,
                        'numero' => 160,
                        'type_id' => 1,
                    'nom' => 'Endomorphismes remarquables d\'un espace vectoriel euclidien (de dimension finie).',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    303 => 
                    array (
                        'id' => 804,
                        'created_at' => '2016-12-19 09:36:51',
                        'updated_at' => '2016-12-19 09:37:03',
                        'annee' => 2017,
                        'numero' => 161,
                        'type_id' => 1,
                        'nom' => 'Isométries d\'un espace affine euclidien de dimension finie. Applications en dimension $2$ et $3$.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    304 => 
                    array (
                        'id' => 805,
                        'created_at' => '2016-12-19 09:38:02',
                        'updated_at' => '2016-12-19 09:38:02',
                        'annee' => 2017,
                        'numero' => 170,
                        'type_id' => 1,
                        'nom' => 'Formes quadratiques sur un espace vectoriel de dimension finie. Orthogonalité, isotropie. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    305 => 
                    array (
                        'id' => 806,
                        'created_at' => '2016-12-19 09:39:04',
                        'updated_at' => '2016-12-19 09:39:04',
                        'annee' => 2017,
                        'numero' => 162,
                        'type_id' => 1,
                        'nom' => 'Systèmes d\'équations linéaires ; opérations élémentaires, aspects algorithmiques et conséquences théoriques.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    306 => 
                    array (
                        'id' => 807,
                        'created_at' => '2016-12-19 09:39:42',
                        'updated_at' => '2016-12-19 09:40:53',
                        'annee' => 2017,
                        'numero' => 171,
                        'type_id' => 1,
                        'nom' => 'Formes quadratiques réelles. Coniques. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    307 => 
                    array (
                        'id' => 808,
                        'created_at' => '2016-12-19 09:41:39',
                        'updated_at' => '2016-12-19 09:41:39',
                        'annee' => 2017,
                        'numero' => 181,
                        'type_id' => 1,
                        'nom' => 'Barycentres dans un espace affine réel de dimension finie, convexité. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    308 => 
                    array (
                        'id' => 809,
                        'created_at' => '2016-12-19 09:42:10',
                        'updated_at' => '2016-12-19 09:42:24',
                        'annee' => 2017,
                        'numero' => 182,
                        'type_id' => 1,
                        'nom' => 'Applications des nombres complexes à la géométrie.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    309 => 
                    array (
                        'id' => 810,
                        'created_at' => '2016-12-19 09:42:46',
                        'updated_at' => '2016-12-19 09:43:01',
                        'annee' => 2017,
                        'numero' => 183,
                        'type_id' => 1,
                        'nom' => 'Utilisation des groupes en géométrie.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    310 => 
                    array (
                        'id' => 811,
                        'created_at' => '2016-12-19 09:43:27',
                        'updated_at' => '2016-12-19 09:43:27',
                        'annee' => 2017,
                        'numero' => 190,
                        'type_id' => 1,
                        'nom' => 'Méthodes combinatoires, problèmes de dénombrement.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    311 => 
                    array (
                        'id' => 812,
                        'created_at' => '2016-12-19 11:58:00',
                        'updated_at' => '2016-12-19 11:58:00',
                        'annee' => 2017,
                        'numero' => 201,
                        'type_id' => 2,
                        'nom' => 'Espaces de fonctions ; exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    312 => 
                    array (
                        'id' => 813,
                        'created_at' => '2016-12-19 11:59:27',
                        'updated_at' => '2016-12-19 11:59:27',
                        'annee' => 2017,
                        'numero' => 202,
                        'type_id' => 2,
                        'nom' => 'Exemples de parties denses et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    313 => 
                    array (
                        'id' => 814,
                        'created_at' => '2016-12-19 12:00:38',
                        'updated_at' => '2016-12-19 12:00:38',
                        'annee' => 2017,
                        'numero' => 203,
                        'type_id' => 2,
                        'nom' => 'Utilisation de la notion de compacité.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    314 => 
                    array (
                        'id' => 815,
                        'created_at' => '2016-12-19 12:02:03',
                        'updated_at' => '2016-12-19 12:02:17',
                        'annee' => 2017,
                        'numero' => 204,
                        'type_id' => 2,
                        'nom' => 'Connexité. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    315 => 
                    array (
                        'id' => 816,
                        'created_at' => '2016-12-19 12:02:45',
                        'updated_at' => '2016-12-19 12:02:45',
                        'annee' => 2017,
                        'numero' => 205,
                        'type_id' => 2,
                        'nom' => 'Espaces complets. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    316 => 
                    array (
                        'id' => 817,
                        'created_at' => '2016-12-19 12:03:28',
                        'updated_at' => '2016-12-19 12:03:28',
                        'annee' => 2017,
                        'numero' => 207,
                        'type_id' => 2,
                        'nom' => 'Prolongement de fonctions. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    317 => 
                    array (
                        'id' => 818,
                        'created_at' => '2016-12-19 12:04:15',
                        'updated_at' => '2016-12-19 12:04:15',
                        'annee' => 2017,
                        'numero' => 208,
                        'type_id' => 2,
                        'nom' => 'Espaces vectoriels normés, applications linéaires continues. Exemples.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    318 => 
                    array (
                        'id' => 819,
                        'created_at' => '2016-12-19 12:05:10',
                        'updated_at' => '2016-12-19 12:05:35',
                        'annee' => 2017,
                        'numero' => 209,
                        'type_id' => 2,
                        'nom' => 'Approximation d\'une fonction par des polynômes et et des polynômes trigonométriques. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    319 => 
                    array (
                        'id' => 820,
                        'created_at' => '2016-12-19 12:06:18',
                        'updated_at' => '2016-12-19 12:06:18',
                        'annee' => 2017,
                        'numero' => 213,
                        'type_id' => 2,
                        'nom' => 'Espaces de Hilbert. Bases hilbertiennes. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    320 => 
                    array (
                        'id' => 821,
                        'created_at' => '2016-12-19 12:07:09',
                        'updated_at' => '2016-12-19 12:07:46',
                        'annee' => 2017,
                        'numero' => 214,
                        'type_id' => 2,
                        'nom' => 'Théorème d\'inversion locale, théorème des fonctions implicites. Exemples et applications en analyse et en géométrie.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    321 => 
                    array (
                        'id' => 822,
                        'created_at' => '2016-12-19 12:08:31',
                        'updated_at' => '2016-12-19 12:08:31',
                        'annee' => 2017,
                        'numero' => 215,
                        'type_id' => 2,
                        'nom' => 'Applications différentiables définies sur un ouvert de $R^n$. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    322 => 
                    array (
                        'id' => 823,
                        'created_at' => '2016-12-19 12:09:12',
                        'updated_at' => '2016-12-19 12:09:12',
                        'annee' => 2017,
                        'numero' => 218,
                        'type_id' => 2,
                        'nom' => 'Applications des formules de Taylor.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    323 => 
                    array (
                        'id' => 824,
                        'created_at' => '2016-12-19 12:10:03',
                        'updated_at' => '2016-12-19 12:10:03',
                        'annee' => 2017,
                        'numero' => 219,
                        'type_id' => 2,
                        'nom' => 'Extremums : existence, caractérisation, recherche. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    324 => 
                    array (
                        'id' => 825,
                        'created_at' => '2016-12-19 12:11:17',
                        'updated_at' => '2016-12-19 12:11:17',
                        'annee' => 2017,
                        'numero' => 220,
                        'type_id' => 2,
                    'nom' => 'Équations différentielles $X\' = f(t,X)$. Exemples d\'étude des solutions en dimension $1$ et $2$.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    325 => 
                    array (
                        'id' => 826,
                        'created_at' => '2016-12-19 12:12:34',
                        'updated_at' => '2016-12-19 12:12:34',
                        'annee' => 2017,
                        'numero' => 221,
                        'type_id' => 2,
                        'nom' => 'Équations différentielles linéaires. Systèmes d\'équations différentielles linéaires. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    326 => 
                    array (
                        'id' => 827,
                        'created_at' => '2016-12-19 12:13:37',
                        'updated_at' => '2016-12-19 12:13:37',
                        'annee' => 2017,
                        'numero' => 222,
                        'type_id' => 2,
                        'nom' => 'Exemples d\'équations aux dérivées partielles linéaires.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    327 => 
                    array (
                        'id' => 828,
                        'created_at' => '2016-12-19 12:14:31',
                        'updated_at' => '2016-12-19 12:14:31',
                        'annee' => 2017,
                        'numero' => 223,
                        'type_id' => 2,
                        'nom' => 'Suites numériques. Convergence, valeurs d\'adhérence. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    328 => 
                    array (
                        'id' => 829,
                        'created_at' => '2016-12-19 12:15:28',
                        'updated_at' => '2016-12-19 12:15:28',
                        'annee' => 2017,
                        'numero' => 224,
                        'type_id' => 2,
                        'nom' => 'Exemples de développements asymptotiques de suites et de fonctions.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    329 => 
                    array (
                        'id' => 830,
                        'created_at' => '2016-12-19 12:16:55',
                        'updated_at' => '2016-12-19 12:16:55',
                        'annee' => 2017,
                        'numero' => 226,
                        'type_id' => 2,
                    'nom' => 'Suites vectorielles et réelles définies par une relation de récurrence $u_{n+1} = f(u_n)$. Exemples. Applications à la résolution approchée d\'équations.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    330 => 
                    array (
                        'id' => 831,
                        'created_at' => '2016-12-19 12:18:44',
                        'updated_at' => '2016-12-19 12:18:44',
                        'annee' => 2017,
                        'numero' => 228,
                        'type_id' => 2,
                        'nom' => 'Continuité et dérivabilité des fonctions réelles d\'une variable réelle. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    331 => 
                    array (
                        'id' => 832,
                        'created_at' => '2016-12-19 12:19:41',
                        'updated_at' => '2016-12-19 12:19:41',
                        'annee' => 2017,
                        'numero' => 229,
                        'type_id' => 2,
                        'nom' => 'Fonctions monotones. Fonctions convexes. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    332 => 
                    array (
                        'id' => 833,
                        'created_at' => '2016-12-19 12:20:52',
                        'updated_at' => '2016-12-19 12:20:52',
                        'annee' => 2017,
                        'numero' => 230,
                        'type_id' => 2,
                        'nom' => 'Séries et de nombres réels ou complexes. Comportement des restes ou des sommes partielles des séries numériques. Exemples.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    333 => 
                    array (
                        'id' => 834,
                        'created_at' => '2016-12-19 12:21:33',
                        'updated_at' => '2016-12-19 12:21:54',
                        'annee' => 2017,
                        'numero' => 233,
                        'type_id' => 2,
                        'nom' => 'Méthodes itératives en analyse numérique.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    334 => 
                    array (
                        'id' => 835,
                        'created_at' => '2016-12-19 12:22:28',
                        'updated_at' => '2016-12-19 12:22:28',
                        'annee' => 2017,
                        'numero' => 234,
                        'type_id' => 2,
                        'nom' => 'Espaces $L^p$, $1 \\le p \\le + \\infty$.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    335 => 
                    array (
                        'id' => 836,
                        'created_at' => '2016-12-19 12:23:13',
                        'updated_at' => '2016-12-19 12:23:13',
                        'annee' => 2017,
                        'numero' => 235,
                        'type_id' => 2,
                        'nom' => 'Problèmes d\'interversion de limites et d\'intégrales.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    336 => 
                    array (
                        'id' => 837,
                        'created_at' => '2016-12-19 12:24:57',
                        'updated_at' => '2016-12-19 12:24:57',
                        'annee' => 2017,
                        'numero' => 236,
                        'type_id' => 2,
                        'nom' => 'Illustrer par des exemples quelques méthodes de calcul d\'intégrales de fonction d\'une ou plusieurs variables.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    337 => 
                    array (
                        'id' => 838,
                        'created_at' => '2016-12-19 12:25:55',
                        'updated_at' => '2016-12-19 12:25:55',
                        'annee' => 2017,
                        'numero' => 239,
                        'type_id' => 2,
                        'nom' => 'Fonctions définies par une intégrale dépendant d\'un paramètre. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    338 => 
                    array (
                        'id' => 839,
                        'created_at' => '2016-12-19 12:26:45',
                        'updated_at' => '2016-12-19 12:26:45',
                        'annee' => 2017,
                        'numero' => 241,
                        'type_id' => 2,
                        'nom' => 'Suites et séries de fonctions. Exemples et contre-exemples.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    339 => 
                    array (
                        'id' => 840,
                        'created_at' => '2016-12-19 12:27:41',
                        'updated_at' => '2016-12-19 12:27:41',
                        'annee' => 2017,
                        'numero' => 243,
                        'type_id' => 2,
                        'nom' => 'Convergence des séries entière, propriétés de la somme. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    340 => 
                    array (
                        'id' => 841,
                        'created_at' => '2016-12-19 12:28:38',
                        'updated_at' => '2016-12-19 12:28:38',
                        'annee' => 2017,
                        'numero' => 245,
                        'type_id' => 2,
                        'nom' => 'Fonctions holomorphes sur un ouvert de $C$. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    341 => 
                    array (
                        'id' => 842,
                        'created_at' => '2016-12-19 12:29:14',
                        'updated_at' => '2016-12-19 12:29:14',
                        'annee' => 2017,
                        'numero' => 246,
                        'type_id' => 2,
                        'nom' => 'Séries de Fouriers. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    342 => 
                    array (
                        'id' => 843,
                        'created_at' => '2016-12-19 12:30:14',
                        'updated_at' => '2016-12-19 12:30:14',
                        'annee' => 2017,
                        'numero' => 250,
                        'type_id' => 2,
                        'nom' => 'Transformation de Fourier. Applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    343 => 
                    array (
                        'id' => 844,
                        'created_at' => '2016-12-19 12:31:02',
                        'updated_at' => '2016-12-19 12:31:02',
                        'annee' => 2017,
                        'numero' => 253,
                        'type_id' => 2,
                        'nom' => 'Utilisation de la notion de convexité en analyse.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    344 => 
                    array (
                        'id' => 845,
                        'created_at' => '2016-12-19 12:31:49',
                        'updated_at' => '2016-12-19 12:31:49',
                        'annee' => 2017,
                        'numero' => 260,
                        'type_id' => 2,
                        'nom' => 'Espérance, variance et moments d\'une variable aléatoire.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    345 => 
                    array (
                        'id' => 846,
                        'created_at' => '2016-12-19 12:32:40',
                        'updated_at' => '2016-12-19 12:32:40',
                        'annee' => 2017,
                        'numero' => 261,
                        'type_id' => 2,
                        'nom' => 'Fonction caractéristique d\'une variable aléatoire. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    346 => 
                    array (
                        'id' => 847,
                        'created_at' => '2016-12-19 12:33:37',
                        'updated_at' => '2016-12-19 12:33:37',
                        'annee' => 2017,
                        'numero' => 262,
                        'type_id' => 2,
                        'nom' => 'Modes de convergence d\'une suite de variables aléatoires. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    347 => 
                    array (
                        'id' => 848,
                        'created_at' => '2016-12-19 12:34:22',
                        'updated_at' => '2016-12-19 12:34:22',
                        'annee' => 2017,
                        'numero' => 263,
                        'type_id' => 2,
                        'nom' => 'Variables aléatoires à densité. Exemples et applications.',
                        'rapport' => '',
                        'options' => 7,
                    ),
                    348 => 
                    array (
                        'id' => 849,
                        'created_at' => '2016-12-19 12:34:59',
                        'updated_at' => '2016-12-19 12:34:59',
                        'annee' => 2017,
                        'numero' => 264,
                        'type_id' => 2,
                        'nom' => 'Variables aléatoires discrètes. Exemples et applications.',
                        'rapport' => '',
                        'options' => 15,
                    ),
                    349 => 
                    array (
                        'id' => 850,
                        'created_at' => '2016-12-19 22:36:18',
                        'updated_at' => '2016-12-19 22:36:18',
                        'annee' => 2017,
                        'numero' => 901,
                        'type_id' => 3,
                        'nom' => 'Structures de données. Exemples et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    350 => 
                    array (
                        'id' => 851,
                        'created_at' => '2016-12-19 22:36:49',
                        'updated_at' => '2016-12-19 22:36:49',
                        'annee' => 2017,
                        'numero' => 902,
                        'type_id' => 3,
                        'nom' => 'Diviser pour régner. Exemples et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    351 => 
                    array (
                        'id' => 852,
                        'created_at' => '2016-12-19 22:37:23',
                        'updated_at' => '2016-12-19 22:37:44',
                        'annee' => 2017,
                        'numero' => 903,
                        'type_id' => 3,
                        'nom' => 'Exemples d\'algorithmes de tri. Correction et complexité.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    352 => 
                    array (
                        'id' => 853,
                        'created_at' => '2016-12-19 22:38:14',
                        'updated_at' => '2016-12-19 22:38:14',
                        'annee' => 2017,
                        'numero' => 906,
                        'type_id' => 3,
                        'nom' => 'Progammation dynamique. Exemples et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    353 => 
                    array (
                        'id' => 854,
                        'created_at' => '2016-12-19 22:38:56',
                        'updated_at' => '2016-12-19 22:38:56',
                        'annee' => 2017,
                        'numero' => 907,
                        'type_id' => 3,
                        'nom' => 'Algorithme du texte.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    354 => 
                    array (
                        'id' => 855,
                        'created_at' => '2016-12-19 22:42:28',
                        'updated_at' => '2016-12-19 22:42:28',
                        'annee' => 2017,
                        'numero' => 909,
                        'type_id' => 3,
                        'nom' => 'Langages rationnels et automates finis. Exemples et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    355 => 
                    array (
                        'id' => 856,
                        'created_at' => '2016-12-19 22:42:29',
                        'updated_at' => '2016-12-19 22:42:29',
                        'annee' => 2017,
                        'numero' => 909,
                        'type_id' => 3,
                        'nom' => 'Langages rationnels et automates finis. Exemples et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    356 => 
                    array (
                        'id' => 857,
                        'created_at' => '2016-12-19 22:43:15',
                        'updated_at' => '2016-12-19 22:43:15',
                        'annee' => 2017,
                        'numero' => 912,
                        'type_id' => 3,
                        'nom' => 'Fonctions récursives primitives et non primitives. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    357 => 
                    array (
                        'id' => 858,
                        'created_at' => '2016-12-19 22:44:15',
                        'updated_at' => '2016-12-19 22:44:15',
                        'annee' => 2017,
                        'numero' => 913,
                        'type_id' => 3,
                        'nom' => 'Machines de Turing. Applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    358 => 
                    array (
                        'id' => 859,
                        'created_at' => '2016-12-19 22:45:02',
                        'updated_at' => '2016-12-19 22:45:02',
                        'annee' => 2017,
                        'numero' => 914,
                        'type_id' => 3,
                        'nom' => 'Décidabilité et indécidabilité. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    359 => 
                    array (
                        'id' => 860,
                        'created_at' => '2016-12-19 22:45:44',
                        'updated_at' => '2016-12-19 22:45:44',
                        'annee' => 2017,
                        'numero' => 915,
                        'type_id' => 3,
                        'nom' => 'Classes de complexité. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    360 => 
                    array (
                        'id' => 861,
                        'created_at' => '2016-12-19 22:46:35',
                        'updated_at' => '2016-12-19 22:46:35',
                        'annee' => 2017,
                        'numero' => 916,
                        'type_id' => 3,
                        'nom' => 'Formules du calcul propositionnel : représentation, formes normales, satisfabilité. Applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    361 => 
                    array (
                        'id' => 862,
                        'created_at' => '2016-12-19 22:47:47',
                        'updated_at' => '2016-12-19 22:47:47',
                        'annee' => 2017,
                        'numero' => 918,
                        'type_id' => 3,
                        'nom' => 'Systèmes formels de calcul de preuve en logique du premier ordre. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    362 => 
                    array (
                        'id' => 863,
                        'created_at' => '2016-12-19 22:48:27',
                        'updated_at' => '2016-12-19 22:48:27',
                        'annee' => 2017,
                        'numero' => 919,
                        'type_id' => 3,
                        'nom' => 'Unification : algorithmes et applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    363 => 
                    array (
                        'id' => 864,
                        'created_at' => '2016-12-19 22:49:03',
                        'updated_at' => '2016-12-19 22:49:03',
                        'annee' => 2017,
                        'numero' => 920,
                        'type_id' => 3,
                        'nom' => 'Réécriture et formes normales. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    364 => 
                    array (
                        'id' => 865,
                        'created_at' => '2016-12-19 22:50:09',
                        'updated_at' => '2016-12-19 22:50:09',
                        'annee' => 2017,
                        'numero' => 921,
                        'type_id' => 3,
                        'nom' => 'Algorithmes de recherche et structures de données associées.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    365 => 
                    array (
                        'id' => 866,
                        'created_at' => '2016-12-19 22:50:48',
                        'updated_at' => '2016-12-19 22:50:48',
                        'annee' => 2017,
                        'numero' => 923,
                        'type_id' => 3,
                        'nom' => 'Analyses lexicale et syntaxique. Applications.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    366 => 
                    array (
                        'id' => 867,
                        'created_at' => '2016-12-19 22:51:33',
                        'updated_at' => '2016-12-19 22:51:33',
                        'annee' => 2017,
                        'numero' => 924,
                        'type_id' => 3,
                        'nom' => 'Théories et modèles en logique du premier ordre. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    367 => 
                    array (
                        'id' => 868,
                        'created_at' => '2016-12-19 22:52:08',
                        'updated_at' => '2016-12-19 22:52:08',
                        'annee' => 2017,
                        'numero' => 925,
                        'type_id' => 3,
                        'nom' => 'Graphes : représentations et algorithmes.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    368 => 
                    array (
                        'id' => 869,
                        'created_at' => '2016-12-19 22:53:20',
                        'updated_at' => '2016-12-19 22:53:20',
                        'annee' => 2017,
                        'numero' => 926,
                        'type_id' => 3,
                        'nom' => 'Analyse des algorithmes, complexité. Exemples.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    369 => 
                    array (
                        'id' => 870,
                        'created_at' => '2016-12-19 22:54:09',
                        'updated_at' => '2016-12-19 22:54:09',
                        'annee' => 2017,
                        'numero' => 927,
                        'type_id' => 3,
                        'nom' => 'Exemples de preuve d\'algorithme : correction, terminaison.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                    370 => 
                    array (
                        'id' => 871,
                        'created_at' => '2016-12-19 22:54:48',
                        'updated_at' => '2016-12-19 22:54:48',
                        'annee' => 2017,
                        'numero' => 928,
                        'type_id' => 3,
                        'nom' => 'Problèmes NP-complets : exemples et réductions.',
                        'rapport' => '',
                        'options' => 8,
                    ),
                ));
        
        
    }
}
