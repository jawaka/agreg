<?php

use Illuminate\Database\Seeder;

class OrauxTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oraux_types')->delete();
        
        \DB::table('oraux_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nom' => 'Algèbre',
                'fnom' => 'algebre',
            ),
            1 => 
            array (
                'id' => 2,
                'nom' => 'Analyse',
                'fnom' => 'analyse',
            ),
            2 => 
            array (
                'id' => 3,
                'nom' => 'Maths-Info',
                'fnom' => 'mathsinfo',
            ),
            3 => 
            array (
                'id' => 4,
                'nom' => 'Informatique',
                'fnom' => 'informatique',
            ),
            4 => 
            array (
                'id' => 5,
                'nom' => 'Option A',
                'fnom' => 'optionA',
            ),
            5 => 
            array (
                'id' => 6,
                'nom' => 'Option B',
                'fnom' => 'optionB',
            ),
            6 => 
            array (
                'id' => 7,
                'nom' => 'Option C',
                'fnom' => 'optionC',
            ),
            7 => 
            array (
                'id' => 8,
                'nom' => 'Option D',
                'fnom' => 'optionD',
            ),
        ));
        
        
    }
}
