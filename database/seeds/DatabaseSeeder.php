<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder 
{
    public function run()
    {
        Model::unguard();

        $this->call('UsersTableSeeder');
        $this->call('ActivationsTableSeeder');
        $this->call('PasswordResetsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('RoleUsersTableSeeder');

        $this->call('LeconsTypesTableSeeder');
        $this->call('LeconsTableSeeder');

        $this->call('ReferencesTableSeeder');

        $this->call('DeveloppementsTableSeeder');

        $this->call('RecasagesTableSeeder');
        $this->call('RecasagesVotesTableSeeder');

        $this->call('VersionsTableSeeder');
        $this->call('VersionsFichiersTableSeeder');
        $this->call('VersionsReferencesTableSeeder');
        $this->call('LeconsLiensTableSeeder');

        $this->call('OrauxTypesTableSeeder');
        $this->call('OrauxQuestionsTypesTableSeeder');
        $this->call('OrauxQuestionsTableSeeder');
        $this->call('OrauxRetoursTableSeeder');
        $this->call('OrauxRetoursReponsesTableSeeder');

        $this->call('CouplagesTableSeeder');
        $this->call('CouplagesDeveloppementsTableSeeder');
        $this->call('CouplagesCouvertureTableSeeder');
        $this->call('CouplagesLeconsCouvertesTableSeeder');
        $this->call('LeconsCommentairesTableSeeder');
    }
}
