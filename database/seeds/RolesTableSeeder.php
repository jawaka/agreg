<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'admin',
                'name' => 'Administrateur',
                'permissions' => '{"user.create":true,"user.update":true,"user.delete":true,"role.create":true,"role.update":true,"role.delete":true,"reference.create":true,"reference.update":true,"reference.delete":true,"lecon.create":true,"lecon.update":true,"lecon.delete":true,"version.create":true,"version.update":true,"version.delete":true,"version.upload":true,"developpement.create":true,"developpement.update":true,"developpement.delete":true,"section.admin":true,"oral.form.create":true,"oral.form.update":true,"oral.form.delete":true,"oral.retour.create":true,"oral.retour.update":true,"oral.retour.delete":true}',
                'created_at' => '2016-03-17 10:50:36',
                'updated_at' => '2016-06-08 16:25:33',
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'editeur_developpement',
                'name' => 'Éditeur de développements/versions',
                'permissions' => '{"developpement.create":true,"developpement.update":true,"version.create":true,"version.upload":true,"version.update":true,"section.admin":true}',
                'created_at' => '2016-05-15 11:33:24',
                'updated_at' => '2016-05-17 10:52:11',
            ),
        ));
        
        
    }
}
