<?php

use Illuminate\Database\Seeder;

class CouplagesLeconsCouvertesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('couplages_lecons_couvertes')->delete();
        
        \DB::table('couplages_lecons_couvertes')->insert(array (
            0 => 
            array (
                'id' => 23,
                'created_at' => NULL,
                'updated_at' => NULL,
                'couplage_id' => 2,
                'lecon_id' => 683,
            ),
            1 => 
            array (
                'id' => 22,
                'created_at' => NULL,
                'updated_at' => NULL,
                'couplage_id' => 2,
                'lecon_id' => 674,
            ),
            2 => 
            array (
                'id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
                'couplage_id' => 15,
                'lecon_id' => 785,
            ),
            3 => 
            array (
                'id' => 18,
                'created_at' => NULL,
                'updated_at' => NULL,
                'couplage_id' => 2,
                'lecon_id' => 718,
            ),
        ));
        
        
    }
}
