<?php

use Illuminate\Database\Seeder;

class VersionsFichiersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('versions_fichiers')->delete();
        
        \DB::table('versions_fichiers')->insert(array (
            0 => 
            array (
                'id' => 326,
                'created_at' => '2016-04-06 15:54:52',
                'updated_at' => '2016-04-06 15:54:52',
                'version_id' => 275,
                'url' => 'reduc_dunford_tom.pdf',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 1,
                'url' => 'algo_decompo_dunford_scourte.pdf',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 2,
                'url' => 'surjective_exp_matricielle.pdf',
            ),
            3 => 
            array (
                'id' => 327,
                'created_at' => '2016-04-06 15:57:51',
                'updated_at' => '2016-04-06 15:57:51',
                'version_id' => 276,
                'url' => '29 _ Théorème de Von Neumann.pdf',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 3,
                'url' => 'theoreme_von_newmann.pdf',
            ),
            5 => 
            array (
                'id' => 328,
                'created_at' => '2016-04-06 16:00:35',
                'updated_at' => '2016-04-06 16:00:35',
                'version_id' => 277,
                'url' => 'equation_chaleur_cercle.pdf',
            ),
            6 => 
            array (
                'id' => 379,
                'created_at' => '2016-05-07 07:47:11',
                'updated_at' => '2016-05-07 07:47:11',
                'version_id' => 4,
                'url' => 'eq_chaleur_cercle_sylvain.pdf',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 5,
                'url' => 'LRQ.pdf',
            ),
            8 => 
            array (
                'id' => 330,
                'created_at' => '2016-04-06 16:04:04',
                'updated_at' => '2016-04-06 16:04:04',
                'version_id' => 279,
                'url' => 'recipro_quad_form_quad_tom.pdf',
            ),
            9 => 
            array (
                'id' => 329,
                'created_at' => '2016-04-06 16:03:12',
                'updated_at' => '2016-04-06 16:03:12',
                'version_id' => 278,
                'url' => '37 _ Réciprocité quadratique _par les formes quadratiques_.pdf',
            ),
            10 => 
            array (
                'id' => 331,
                'created_at' => '2016-04-06 16:05:17',
                'updated_at' => '2016-04-06 16:05:17',
                'version_id' => 280,
                'url' => 'recipro_quad_resultant_tom.pdf',
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 6,
            'url' => '36 - Réciprocité quadratique (par le résultant).pdf',
            ),
            12 => 
            array (
                'id' => 332,
                'created_at' => '2016-04-06 16:06:51',
                'updated_at' => '2016-04-06 16:06:51',
                'version_id' => 281,
                'url' => 'somme_de_deux_carres_tom.pdf',
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 7,
                'url' => 'entiers_Gauss_scourte.pdf',
            ),
            14 => 
            array (
                'id' => 333,
                'created_at' => '2016-04-06 16:07:53',
                'updated_at' => '2016-04-06 16:07:53',
                'version_id' => 282,
                'url' => 'lemme_morse.pdf',
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 8,
                'url' => '22 - Lemme de Morse.pdf',
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 9,
                'url' => 'ansimple.pdf',
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 10,
                'url' => 'ansimplecomm.pdf',
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 11,
                'url' => 'decompo_polaire.pdf',
            ),
            19 => 
            array (
                'id' => 334,
                'created_at' => '2016-04-06 16:11:08',
                'updated_at' => '2016-04-06 16:11:08',
                'version_id' => 283,
                'url' => '31 _ Un homéomorphisme réalisé par l_exponentielle de matrice.pdf',
            ),
            20 => 
            array (
                'id' => 22,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 12,
                'url' => 'exp_SnR_scourte.pdf',
            ),
            21 => 
            array (
                'id' => 336,
                'created_at' => '2016-04-06 16:13:41',
                'updated_at' => '2016-04-06 16:13:41',
                'version_id' => 285,
                'url' => 'principalite_dun_anneau_tom.pdf',
            ),
            22 => 
            array (
                'id' => 24,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 13,
                'url' => 'princiaplite_anneau_scourte.pdf',
            ),
            23 => 
            array (
                'id' => 25,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 14,
                'url' => 'thm_chevalley_tom.pdf',
            ),
            24 => 
            array (
                'id' => 337,
                'created_at' => '2016-04-06 16:15:40',
                'updated_at' => '2016-04-06 16:15:40',
                'version_id' => 286,
                'url' => 'groupe_modulaire.pdf',
            ),
            25 => 
            array (
                'id' => 27,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 15,
                'url' => 'groupe_modulaire_lefourn.pdf',
            ),
            26 => 
            array (
                'id' => 338,
                'created_at' => '2016-04-06 16:16:47',
                'updated_at' => '2016-04-06 16:16:47',
                'version_id' => 287,
                'url' => '13 _ Endomorphismes semi_simples.pdf',
            ),
            27 => 
            array (
                'id' => 29,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 16,
                'url' => 'endo_semi_simple_tom.pdf',
            ),
            28 => 
            array (
                'id' => 339,
                'created_at' => '2016-04-06 16:18:17',
                'updated_at' => '2016-04-06 16:18:17',
                'version_id' => 288,
                'url' => '21 _ Théorème de Gauss _polygônes constructibles_.pdf',
            ),
            29 => 
            array (
                'id' => 31,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 17,
                'url' => 'thm_gauss_tom.pdf',
            ),
            30 => 
            array (
                'id' => 33,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 18,
                'url' => 'tauberien_fort_tom.pdf',
            ),
            31 => 
            array (
                'id' => 34,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 19,
                'url' => 'thm_relevement.pdf',
            ),
            32 => 
            array (
                'id' => 340,
                'created_at' => '2016-04-06 16:21:08',
                'updated_at' => '2016-04-06 16:21:08',
                'version_id' => 290,
                'url' => '25 _ Processus de Galton_Watson.pdf',
            ),
            33 => 
            array (
                'id' => 341,
                'created_at' => '2016-04-06 16:22:19',
                'updated_at' => '2016-04-06 16:22:19',
                'version_id' => 291,
                'url' => 'theoreme_lacunes_hadamard_tom.pdf',
            ),
            34 => 
            array (
                'id' => 38,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 21,
                'url' => 'thm_lacune_hadamard_scourte.pdf',
            ),
            35 => 
            array (
                'id' => 39,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 22,
                'url' => 'thm-pfister.pdf',
            ),
            36 => 
            array (
                'id' => 40,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 23,
                'url' => 'thm-artin.pdf',
            ),
            37 => 
            array (
                'id' => 343,
                'created_at' => '2016-04-06 16:25:03',
                'updated_at' => '2016-04-06 16:25:03',
                'version_id' => 293,
                'url' => 'table_carcateres.pdf',
            ),
            38 => 
            array (
                'id' => 342,
                'created_at' => '2016-04-06 16:24:44',
                'updated_at' => '2016-04-06 16:24:44',
                'version_id' => 292,
                'url' => 'table_caract_simplicite_tom.pdf',
            ),
            39 => 
            array (
                'id' => 43,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 24,
                'url' => 'table_carac_simplicite_scourte.pdf',
            ),
            40 => 
            array (
                'id' => 345,
                'created_at' => '2016-04-06 16:26:38',
                'updated_at' => '2016-04-06 16:26:38',
                'version_id' => 295,
                'url' => 'table_carac_s4_tom.pdf',
            ),
            41 => 
            array (
                'id' => 45,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 25,
                'url' => 'table_carac_S4_scourte.pdf',
            ),
            42 => 
            array (
                'id' => 344,
                'created_at' => '2016-04-06 16:26:11',
                'updated_at' => '2016-04-06 16:26:11',
                'version_id' => 294,
                'url' => '41 _ Table des caractères de S4 et groupe d_isométries du tétraèdre.pdf',
            ),
            43 => 
            array (
                'id' => 346,
                'created_at' => '2016-04-06 16:27:38',
                'updated_at' => '2016-04-06 16:27:38',
                'version_id' => 296,
                'url' => '05 _ Algorithme de Berlekamp.pdf',
            ),
            44 => 
            array (
                'id' => 48,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 26,
                'url' => 'algo_berlekamp_scourte.pdf',
            ),
            45 => 
            array (
                'id' => 49,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 27,
                'url' => 'nb-racines.pdf',
            ),
            46 => 
            array (
                'id' => 347,
                'created_at' => '2016-04-06 16:29:12',
                'updated_at' => '2016-04-06 16:29:12',
                'version_id' => 297,
                'url' => 'forme_quad_fq.pdf',
            ),
            47 => 
            array (
                'id' => 51,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 28,
                'url' => 'forme_quadratique_fq_scourte.pdf',
            ),
            48 => 
            array (
                'id' => 348,
                'created_at' => '2016-04-06 16:30:30',
                'updated_at' => '2016-04-06 16:30:30',
                'version_id' => 298,
                'url' => 'menelaus_ceva.pdf',
            ),
            49 => 
            array (
                'id' => 53,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 29,
                'url' => 'ceva_menelaus_scourte.pdf',
            ),
            50 => 
            array (
                'id' => 54,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 30,
                'url' => 'conique_5_points_scourte.pdf',
            ),
            51 => 
            array (
                'id' => 55,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 31,
                'url' => '15 - Invariants de similitude.pdf',
            ),
            52 => 
            array (
                'id' => 56,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 32,
                'url' => 'elipse-steiner.pdf',
            ),
            53 => 
            array (
                'id' => 57,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 33,
                'url' => 'so-et-les-quatrnins.pdf',
            ),
            54 => 
            array (
                'id' => 58,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 34,
                'url' => 'kakutani.pdf',
            ),
            55 => 
            array (
                'id' => 349,
                'created_at' => '2016-04-11 20:30:53',
                'updated_at' => '2016-04-11 20:30:53',
                'version_id' => 300,
                'url' => 'methode_gradient.pdf',
            ),
            56 => 
            array (
                'id' => 60,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 35,
                'url' => 'meth_grad_conjugue_scourte.pdf',
            ),
            57 => 
            array (
                'id' => 61,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 36,
                'url' => 'chemin-optique.pdf',
            ),
            58 => 
            array (
                'id' => 62,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 37,
                'url' => 'pts-de-lebegues.pdf',
            ),
            59 => 
            array (
                'id' => 63,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 38,
                'url' => '02 - Théorème de Muntz.pdf',
            ),
            60 => 
            array (
                'id' => 64,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 39,
                'url' => 'simplicite_son_tom.pdf',
            ),
            61 => 
            array (
                'id' => 65,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 40,
                'url' => 'thm-sylow-rec.pdf',
            ),
            62 => 
            array (
                'id' => 66,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 41,
                'url' => 'enveloppe_convexe_groupe_ortho_scourte.pdf',
            ),
            63 => 
            array (
                'id' => 67,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 42,
                'url' => 'optimisation_dans_hilbert.pdf',
            ),
            64 => 
            array (
                'id' => 350,
                'created_at' => '2016-04-11 20:36:20',
                'updated_at' => '2016-04-11 20:36:20',
                'version_id' => 301,
                'url' => '18 _ Théorème de représentation conforme.pdf',
            ),
            65 => 
            array (
                'id' => 69,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 43,
                'url' => 'uniformisation_riemann_tom.pdf',
            ),
            66 => 
            array (
                'id' => 351,
                'created_at' => '2016-04-11 20:37:49',
                'updated_at' => '2016-04-11 20:37:49',
                'version_id' => 302,
                'url' => 'formule_complements.pdf',
            ),
            67 => 
            array (
                'id' => 71,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 44,
                'url' => 'formule_complement_scourte.pdf',
            ),
            68 => 
            array (
                'id' => 72,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 45,
                'url' => 'chemin_au_dessus_courbe_tom.pdf',
            ),
            69 => 
            array (
                'id' => 352,
                'created_at' => '2016-04-11 20:39:30',
                'updated_at' => '2016-04-11 20:39:30',
                'version_id' => 303,
                'url' => 'nulstellnsatz_resltant.pdf',
            ),
            70 => 
            array (
                'id' => 74,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 46,
            'url' => '32 - Nullstellensatz (par le résultant).pdf',
            ),
            71 => 
            array (
                'id' => 353,
                'created_at' => '2016-04-11 20:41:10',
                'updated_at' => '2016-04-11 20:41:10',
                'version_id' => 304,
                'url' => 'thm_luroth.pdf',
            ),
            72 => 
            array (
                'id' => 76,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 47,
                'url' => '42 - Théorème de Lüroth.pdf',
            ),
            73 => 
            array (
                'id' => 77,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 48,
                'url' => 'thm_brauer_permutation_tom.pdf',
            ),
            74 => 
            array (
                'id' => 78,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 49,
                'url' => 'automorphisme_isometrique.pdf',
            ),
            75 => 
            array (
                'id' => 79,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 50,
                'url' => 'etude-ed-non-lin.pdf',
            ),
            76 => 
            array (
                'id' => 80,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 51,
                'url' => 'banach_steinhauss_serie_fourier_tom.pdf',
            ),
            77 => 
            array (
                'id' => 354,
                'created_at' => '2016-04-12 21:02:18',
                'updated_at' => '2016-04-12 21:02:18',
                'version_id' => 305,
                'url' => 'densite_fonctions_test.pdf',
            ),
            78 => 
            array (
                'id' => 82,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 52,
                'url' => 'densite_dans_Lp_scourte.pdf',
            ),
            79 => 
            array (
                'id' => 355,
                'created_at' => '2016-04-12 21:04:03',
                'updated_at' => '2016-04-12 21:04:03',
                'version_id' => 306,
                'url' => 'hadamard_levy_tom.pdf',
            ),
            80 => 
            array (
                'id' => 84,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 53,
                'url' => '11 - Théorème de Hadamard-Lévy.pdf',
            ),
            81 => 
            array (
                'id' => 85,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 54,
                'url' => 'thm_sturm_tom.pdf',
            ),
            82 => 
            array (
                'id' => 86,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 55,
                'url' => 'c-alg-clos.pdf',
            ),
            83 => 
            array (
                'id' => 356,
                'created_at' => '2016-04-12 21:06:37',
                'updated_at' => '2016-04-12 21:06:37',
                'version_id' => 307,
                'url' => 'reduc_jordan_tom.pdf',
            ),
            84 => 
            array (
                'id' => 88,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 56,
                'url' => '38 - Réduction de Jordan.pdf',
            ),
            85 => 
            array (
                'id' => 89,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 57,
                'url' => 'psudeux.pdf',
            ),
            86 => 
            array (
                'id' => 357,
                'created_at' => '2016-04-12 21:08:29',
                'updated_at' => '2016-04-12 21:08:29',
                'version_id' => 308,
                'url' => 'proba_premier.pdf',
            ),
            87 => 
            array (
                'id' => 91,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 58,
                'url' => 'proba_premiers_scourte.pdf',
            ),
            88 => 
            array (
                'id' => 358,
                'created_at' => '2016-04-12 21:09:52',
                'updated_at' => '2016-04-12 21:09:52',
                'version_id' => 309,
                'url' => 'thm_app_ouverte.pdf',
            ),
            89 => 
            array (
                'id' => 93,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 59,
                'url' => 'thm_app_ouverte_scourte.pdf',
            ),
            90 => 
            array (
                'id' => 94,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 60,
                'url' => 'theoreme_cauchy_lipschitz_local.pdf',
            ),
            91 => 
            array (
                'id' => 359,
                'created_at' => '2016-04-12 21:11:51',
                'updated_at' => '2016-04-12 21:11:51',
                'version_id' => 310,
                'url' => 'thm_fond_etude_locale_courbe_tom.pdf',
            ),
            92 => 
            array (
                'id' => 96,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 61,
                'url' => 'thm_fond_courbes_scourte.pdf',
            ),
            93 => 
            array (
                'id' => 97,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 62,
                'url' => 'endo-stabilisant-gl.pdf',
            ),
            94 => 
            array (
                'id' => 98,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 63,
                'url' => 'expmnc.pdf',
            ),
            95 => 
            array (
                'id' => 99,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 64,
                'url' => 'base_hilbertienne_polynome.pdf',
            ),
            96 => 
            array (
                'id' => 360,
                'created_at' => '2016-04-12 21:14:28',
                'updated_at' => '2016-04-12 21:14:28',
                'version_id' => 311,
                'url' => 'inversion_fourier.pdf',
            ),
            97 => 
            array (
                'id' => 101,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 65,
                'url' => 'inversion_fourier_S_scourte.pdf',
            ),
            98 => 
            array (
                'id' => 361,
                'created_at' => '2016-04-12 21:15:44',
                'updated_at' => '2016-04-12 21:15:44',
                'version_id' => 312,
                'url' => 'thm_fourier_plancherl.pdf',
            ),
            99 => 
            array (
                'id' => 103,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 66,
                'url' => 'transformee_Fourier_Plancherel_scourte.pdf',
            ),
            100 => 
            array (
                'id' => 104,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 67,
                'url' => 'dimension-du-commutant.pdf',
            ),
            101 => 
            array (
                'id' => 105,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 68,
                'url' => 'galois_inverse.pdf',
            ),
            102 => 
            array (
                'id' => 106,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 69,
                'url' => 'structure_polynome_symetrique.pdf',
            ),
            103 => 
            array (
                'id' => 107,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 70,
                'url' => 'thm-burnside.pdf',
            ),
            104 => 
            array (
                'id' => 108,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 71,
                'url' => '26 - Test de primalité de Miller-Rabin.pdf',
            ),
            105 => 
            array (
                'id' => 109,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 72,
                'url' => 'caracterisatin-des-puisances-k.pdf',
            ),
            106 => 
            array (
                'id' => 110,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 73,
                'url' => 'thm-frobenius-groupes.pdf',
            ),
            107 => 
            array (
                'id' => 112,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 75,
                'url' => 'thm-paley-wiener.pdf',
            ),
            108 => 
            array (
                'id' => 113,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 76,
                'url' => '44 - Méthode de Newton-Raphson.pdf',
            ),
            109 => 
            array (
                'id' => 114,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 77,
                'url' => 'constuction-exp-et-pi.pdf',
            ),
            110 => 
            array (
                'id' => 115,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 78,
                'url' => 'courbe-a-courbure-croissante.pdf',
            ),
            111 => 
            array (
                'id' => 116,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 79,
                'url' => '39 - Inégalité de Hoeffding.pdf',
            ),
            112 => 
            array (
                'id' => 117,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 80,
                'url' => 'monte-carlo.pdf',
            ),
            113 => 
            array (
                'id' => 118,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 81,
                'url' => 'stat-dordre.pdf',
            ),
            114 => 
            array (
                'id' => 119,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 82,
                'url' => 'temps-darret.pdf',
            ),
            115 => 
            array (
                'id' => 362,
                'created_at' => '2016-04-13 20:09:42',
                'updated_at' => '2016-04-13 20:09:42',
                'version_id' => 313,
                'url' => 'marche_aleatoire_tom.pdf',
            ),
            116 => 
            array (
                'id' => 121,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 83,
                'url' => '43 - Marche aléatoire dans Z^d.pdf',
            ),
            117 => 
            array (
                'id' => 122,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 84,
                'url' => 'weierstrass_approx.pdf',
            ),
            118 => 
            array (
                'id' => 363,
                'created_at' => '2016-04-13 20:10:37',
                'updated_at' => '2016-04-13 20:10:37',
                'version_id' => 314,
                'url' => '24 _ Théorème de Weierstrass.pdf',
            ),
            119 => 
            array (
                'id' => 124,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 85,
                'url' => '40 - Fonction caractéristique et moments.pdf',
            ),
            120 => 
            array (
                'id' => 125,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 86,
                'url' => 'existence-esp-conditionnlle.pdf',
            ),
            121 => 
            array (
                'id' => 364,
                'created_at' => '2016-04-13 20:13:06',
                'updated_at' => '2016-04-13 20:13:06',
                'version_id' => 315,
                'url' => '16 _ Billard convexe.pdf',
            ),
            122 => 
            array (
                'id' => 127,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 87,
                'url' => 'billard_convexe_scourte.pdf',
            ),
            123 => 
            array (
                'id' => 128,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 88,
                'url' => 'carac_frac_ration_tom.pdf',
            ),
            124 => 
            array (
                'id' => 129,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 89,
                'url' => 'thm-abel-angulaire.pdf',
            ),
            125 => 
            array (
                'id' => 365,
                'created_at' => '2016-04-13 20:15:23',
                'updated_at' => '2016-04-13 20:15:23',
                'version_id' => 316,
                'url' => '23 _ Théorème de Riesz_Fischer.pdf',
            ),
            126 => 
            array (
                'id' => 131,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 90,
                'url' => 'lp_complet_scourte.pdf',
            ),
            127 => 
            array (
                'id' => 132,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 91,
                'url' => 'stabilite-syst-autonomne.pdf',
            ),
            128 => 
            array (
                'id' => 133,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 92,
                'url' => 'cercle_euler_tom.pdf',
            ),
            129 => 
            array (
                'id' => 134,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 93,
                'url' => 'thm_kakutani_massera_scourte.pdf',
            ),
            130 => 
            array (
                'id' => 135,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 94,
                'url' => 'gen_OE_tom.pdf',
            ),
            131 => 
            array (
                'id' => 136,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 95,
                'url' => 'eq-fonctinnelle-dzeta.pdf',
            ),
            132 => 
            array (
                'id' => 137,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 96,
                'url' => 'theoreme_inversion_locale.pdf',
            ),
            133 => 
            array (
                'id' => 138,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 97,
                'url' => 'formule_poisson_resultant_tom.pdf',
            ),
            134 => 
            array (
                'id' => 366,
                'created_at' => '2016-04-13 20:20:27',
                'updated_at' => '2016-04-13 20:20:27',
                'version_id' => 317,
                'url' => '06 _ Formule sommatoire de Poisson et application.pdf',
            ),
            135 => 
            array (
                'id' => 140,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 98,
                'url' => 'formule_sommatoire_poisson_tom.pdf',
            ),
            136 => 
            array (
                'id' => 141,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 99,
                'url' => 'thm-realisation-borel.pdf',
            ),
            137 => 
            array (
                'id' => 325,
                'created_at' => '2016-04-06 15:47:21',
                'updated_at' => '2016-04-06 15:47:21',
                'version_id' => 274,
                'url' => 'sophie_germain.pdf',
            ),
            138 => 
            array (
                'id' => 367,
                'created_at' => '2016-04-13 20:22:13',
                'updated_at' => '2016-04-13 20:22:13',
                'version_id' => 318,
                'url' => 'poly_cyclo_irred_tom.pdf',
            ),
            139 => 
            array (
                'id' => 144,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 101,
                'url' => 'irred_poly_cyclo_scourte.pdf',
            ),
            140 => 
            array (
                'id' => 145,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 102,
                'url' => 'loi-recipro-puiss-d.pdf',
            ),
            141 => 
            array (
                'id' => 146,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 103,
                'url' => 'suite-logistique.pdf',
            ),
            142 => 
            array (
                'id' => 368,
                'created_at' => '2016-04-13 20:24:31',
                'updated_at' => '2016-04-13 20:24:31',
                'version_id' => 319,
                'url' => 'methode_iterative_syst_lin_tom.pdf',
            ),
            143 => 
            array (
                'id' => 148,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 104,
                'url' => '33 - Méthode itérative de résolution d\'un système linéaire.pdf',
            ),
            144 => 
            array (
                'id' => 149,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 105,
                'url' => 'thm_barany_tom.pdf',
            ),
            145 => 
            array (
                'id' => 150,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 106,
                'url' => 'thm_sylow_tom.pdf',
            ),
            146 => 
            array (
                'id' => 151,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 107,
                'url' => 'meth_newton_scourte.pdf',
            ),
            147 => 
            array (
                'id' => 152,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 108,
                'url' => 'methode_newton_polynome.pdf',
            ),
            148 => 
            array (
                'id' => 153,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 109,
                'url' => 'inegalite_isoperimetrique_scourte.pdf',
            ),
            149 => 
            array (
                'id' => 154,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 110,
                'url' => 'resolution-equatin-matricielle-ed.pdf',
            ),
            150 => 
            array (
                'id' => 155,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 111,
                'url' => '04 - Théorème de Frobenius-Zolotarev.pdf',
            ),
            151 => 
            array (
                'id' => 156,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 112,
                'url' => 'thm-wederburn.pdf',
            ),
            152 => 
            array (
                'id' => 157,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 113,
                'url' => 'constuction-va-indep.pdf',
            ),
            153 => 
            array (
                'id' => 158,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 114,
                'url' => 'reduction-jordan-dualite.pdf',
            ),
            154 => 
            array (
                'id' => 159,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 115,
                'url' => '01 - Théorème de Brouwer.pdf',
            ),
            155 => 
            array (
                'id' => 160,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 116,
                'url' => '03 - Groupe simple d\'ordre 60.pdf',
            ),
            156 => 
            array (
                'id' => 161,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 117,
                'url' => '07 - Prolongement de la fonction Zeta.pdf',
            ),
            157 => 
            array (
                'id' => 162,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 118,
                'url' => '08 - Théorème de Jordan C1.pdf',
            ),
            158 => 
            array (
                'id' => 163,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 119,
                'url' => '09 - Fonction continue périodique dont la série de Fourier diverge en 0.pdf',
            ),
            159 => 
            array (
                'id' => 369,
                'created_at' => '2016-04-16 08:10:13',
                'updated_at' => '2016-04-16 08:10:13',
                'version_id' => 320,
                'url' => '12 _ Théorème de John.pdf',
            ),
            160 => 
            array (
                'id' => 165,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 120,
                'url' => 'ellipsoide_john_scourte.pdf',
            ),
            161 => 
            array (
                'id' => 166,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 121,
                'url' => '14 - Méthode de Laplace.pdf',
            ),
            162 => 
            array (
                'id' => 167,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 122,
                'url' => '17 - Equation différentielle dans les espaces de Holder.pdf',
            ),
            163 => 
            array (
                'id' => 168,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 123,
                'url' => '19 - Equation de Schrodinger.pdf',
            ),
            164 => 
            array (
                'id' => 169,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 124,
                'url' => '20 - Théorème de Bézout faible.pdf',
            ),
            165 => 
            array (
                'id' => 170,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 125,
                'url' => '27 - Nombre de solutions d\'équations polynomiales dans Fq.pdf',
            ),
            166 => 
            array (
                'id' => 171,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 126,
                'url' => '28 - Théorème de Witt.pdf',
            ),
            167 => 
            array (
                'id' => 172,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 127,
                'url' => 'densite_fonction_nulle_part_derivable.pdf',
            ),
            168 => 
            array (
                'id' => 173,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 128,
                'url' => 'nombre_de_bell.pdf',
            ),
            169 => 
            array (
                'id' => 377,
                'created_at' => '2016-04-21 11:30:04',
                'updated_at' => '2016-04-21 11:30:04',
                'version_id' => 329,
                'url' => 'lemme_morse_sylvain.pdf',
            ),
            170 => 
            array (
                'id' => 376,
                'created_at' => '2016-04-21 11:27:41',
                'updated_at' => '2016-04-21 11:27:41',
                'version_id' => 328,
                'url' => 'thm_sylow_sylvain.pdf',
            ),
            171 => 
            array (
                'id' => 372,
                'created_at' => '2016-04-16 08:16:42',
                'updated_at' => '2016-04-16 08:16:42',
                'version_id' => 323,
                'url' => 'equation_dioph_serie_tom.pdf',
            ),
            172 => 
            array (
                'id' => 177,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 129,
                'url' => 'nb_parts_a_parts_fixes_lefourn.pdf',
            ),
            173 => 
            array (
                'id' => 178,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 130,
                'url' => 'element_primitif_lucas.pdf',
            ),
            174 => 
            array (
                'id' => 179,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 131,
                'url' => 'NombresCarmichael_lucas.pdf',
            ),
            175 => 
            array (
                'id' => 180,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 132,
                'url' => 'calcul_trois_integrales_tom.pdf',
            ),
            176 => 
            array (
                'id' => 181,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 133,
                'url' => 'methode_relaxation_tom.pdf',
            ),
            177 => 
            array (
                'id' => 182,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 134,
                'url' => 'resolution_ed_dans_sprime_tom.pdf',
            ),
            178 => 
            array (
                'id' => 183,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 135,
                'url' => 'thm_sard_tom.pdf',
            ),
            179 => 
            array (
                'id' => 184,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 136,
                'url' => 'reduc_endo_normaux_tom.pdf',
            ),
            180 => 
            array (
                'id' => 185,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 137,
                'url' => 'isom_esp_fonctions_tom.pdf',
            ),
            181 => 
            array (
                'id' => 186,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 138,
                'url' => 'dev base.pdf',
            ),
            182 => 
            array (
                'id' => 187,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 139,
                'url' => 'fermat_corps-fini.pdf',
            ),
            183 => 
            array (
                'id' => 188,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 140,
                'url' => 'kronecker.pdf',
            ),
            184 => 
            array (
                'id' => 189,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 141,
                'url' => 'injection_compacte_sobolev_scourte.pdf',
            ),
            185 => 
            array (
                'id' => 190,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 142,
                'url' => 'isom_grp_topo_scourte.pdf',
            ),
            186 => 
            array (
                'id' => 191,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 143,
                'url' => 'theoreme_morgenstern_scourte.pdf',
            ),
            187 => 
            array (
                'id' => 192,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 144,
                'url' => 'thm_cauchy_peano_scourte.pdf',
            ),
            188 => 
            array (
                'id' => 193,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 145,
                'url' => 'simplicite_SO3_scourte.pdf',
            ),
            189 => 
            array (
                'id' => 194,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 146,
                'url' => 'autom_Sn_scourte.pdf',
            ),
            190 => 
            array (
                'id' => 195,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 147,
                'url' => 'gen_isomE_scourte.pdf',
            ),
            191 => 
            array (
                'id' => 196,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 148,
                'url' => 'thm_ergodique_von_neumann_scourte.pdf',
            ),
            192 => 
            array (
                'id' => 197,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 149,
                'url' => 'suite_rec_cv_lente_scourte.pdf',
            ),
            193 => 
            array (
                'id' => 373,
                'created_at' => '2016-04-16 08:27:55',
                'updated_at' => '2016-04-16 08:27:55',
                'version_id' => 324,
                'url' => 'topo_classe_similitude_scourte.pdf',
            ),
            194 => 
            array (
                'id' => 199,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 150,
                'url' => 'topo-classe-similitude.pdf',
            ),
            195 => 
            array (
                'id' => 200,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 151,
                'url' => 'diag_semi_simple_scourte.pdf',
            ),
            196 => 
            array (
                'id' => 374,
                'created_at' => '2016-04-18 16:18:30',
                'updated_at' => '2016-04-18 16:18:30',
                'version_id' => 325,
                'url' => 'thm_Pascal_scourte.pdf',
            ),
            197 => 
            array (
                'id' => 202,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 152,
                'url' => 'thm_pasacal_lefourn.pdf',
            ),
            198 => 
            array (
                'id' => 203,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 153,
                'url' => 'thm_Dirichlet_faible_scourte.pdf',
            ),
            199 => 
            array (
                'id' => 204,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 154,
                'url' => 'table_carac_D4_H8_scourte.pdf',
            ),
            200 => 
            array (
                'id' => 205,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 155,
                'url' => 'thm_molien_scourte.pdf',
            ),
            201 => 
            array (
                'id' => 206,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 156,
                'url' => 'point_fermat_torricelli_scourte.pdf',
            ),
            202 => 
            array (
                'id' => 207,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 157,
                'url' => 'eq_chaleur_distrib_scourte.pdf',
            ),
            203 => 
            array (
                'id' => 208,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 158,
                'url' => '34 - Théorème de Pascal.pdf',
            ),
            204 => 
            array (
                'id' => 209,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 159,
                'url' => '35 - Théorème de point fixe de Kakutani.pdf',
            ),
            205 => 
            array (
                'id' => 210,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 160,
                'url' => 'thm-permut-riemann.pdf',
            ),
            206 => 
            array (
                'id' => 211,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 161,
                'url' => 'thm-sharkovski.pdf',
            ),
            207 => 
            array (
                'id' => 212,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 162,
                'url' => 'translate-dune-fonction.pdf',
            ),
            208 => 
            array (
                'id' => 213,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 163,
                'url' => 'poly-semi-symetiques.pdf',
            ),
            209 => 
            array (
                'id' => 297,
                'created_at' => '2016-03-29 15:02:07',
                'updated_at' => '2016-03-29 15:02:07',
                'version_id' => 246,
                'url' => 'lax_milgram_thm.pdf',
            ),
            210 => 
            array (
                'id' => 215,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 165,
                'url' => 'espaces_hyperboliques.pdf',
            ),
            211 => 
            array (
                'id' => 216,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 166,
                'url' => 'birapport_homographies.pdf',
            ),
            212 => 
            array (
                'id' => 217,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 167,
                'url' => 'dvpt_bessel.pdf',
            ),
            213 => 
            array (
                'id' => 219,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 169,
                'url' => 'application_du_vandermonde.pdf',
            ),
            214 => 
            array (
                'id' => 296,
                'created_at' => '2016-03-29 15:00:44',
                'updated_at' => '2016-03-29 15:00:44',
                'version_id' => 245,
                'url' => 'laplace_thm.pdf',
            ),
            215 => 
            array (
                'id' => 309,
                'created_at' => '2016-03-30 11:55:58',
                'updated_at' => '2016-03-30 11:55:58',
                'version_id' => 258,
                'url' => 'poincare_geodesiques.pdf',
            ),
            216 => 
            array (
                'id' => 222,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 172,
                'url' => 'biholomorphisme.pdf',
            ),
            217 => 
            array (
                'id' => 274,
                'created_at' => '2016-03-27 19:47:21',
                'updated_at' => '2016-03-27 19:47:21',
                'version_id' => 223,
                'url' => 'courbes_fondamental_thm.pdf',
            ),
            218 => 
            array (
                'id' => 224,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 174,
                'url' => 'chemin_auto_evitant.pdf',
            ),
            219 => 
            array (
                'id' => 375,
                'created_at' => '2016-04-21 11:17:18',
                'updated_at' => '2016-04-21 11:17:18',
                'version_id' => 326,
                'url' => 'surjectivite_expo.pdf',
            ),
            220 => 
            array (
                'id' => 324,
                'created_at' => '2016-04-06 15:46:24',
                'updated_at' => '2016-04-06 15:46:24',
                'version_id' => 273,
                'url' => 'shannon_echantillonage.pdf',
            ),
            221 => 
            array (
                'id' => 298,
                'created_at' => '2016-03-29 15:03:22',
                'updated_at' => '2016-03-29 15:03:22',
                'version_id' => 247,
                'url' => 'matricielle_equation.pdf',
            ),
            222 => 
            array (
                'id' => 286,
                'created_at' => '2016-03-27 20:09:47',
                'updated_at' => '2016-03-27 20:09:47',
                'version_id' => 235,
                'url' => 'hadamard_lacunes.pdf',
            ),
            223 => 
            array (
                'id' => 293,
                'created_at' => '2016-03-27 20:18:30',
                'updated_at' => '2016-03-27 20:18:30',
                'version_id' => 242,
                'url' => 'john_ellipsoide.pdf',
            ),
            224 => 
            array (
                'id' => 323,
                'created_at' => '2016-04-06 15:45:19',
                'updated_at' => '2016-04-06 15:45:19',
                'version_id' => 272,
                'url' => 'morphisme_algebre.pdf',
            ),
            225 => 
            array (
                'id' => 231,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 181,
                'url' => 'groupe_circulaire_lefourn.pdf',
            ),
            226 => 
            array (
                'id' => 232,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 182,
                'url' => 'thm_kronecker_lefourn.pdf',
            ),
            227 => 
            array (
                'id' => 233,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 183,
                'url' => 'thm_dirichlet_lefourn.pdf',
            ),
            228 => 
            array (
                'id' => 234,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 184,
                'url' => 'formule_poisson_discrete.pdf',
            ),
            229 => 
            array (
                'id' => 235,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 185,
                'url' => 'dobble.pdf',
            ),
            230 => 
            array (
                'id' => 236,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 186,
                'url' => 'pavage_S4.pdf',
            ),
            231 => 
            array (
                'id' => 237,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 187,
                'url' => 'FFT.pdf',
            ),
            232 => 
            array (
                'id' => 238,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 188,
                'url' => 'algo_cyk.pdf',
            ),
            233 => 
            array (
                'id' => 239,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 189,
                'url' => 'algo_exp_indep.pdf',
            ),
            234 => 
            array (
                'id' => 240,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 190,
                'url' => 'algo_floyd_warshall.pdf',
            ),
            235 => 
            array (
                'id' => 241,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 191,
                'url' => 'algo_kmp.pdf',
            ),
            236 => 
            array (
                'id' => 242,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 192,
                'url' => 'algo_unification.pdf',
            ),
            237 => 
            array (
                'id' => 243,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 193,
                'url' => 'arbres_splay.pdf',
            ),
            238 => 
            array (
                'id' => 244,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 194,
                'url' => 'calcul_premier_suivant.pdf',
            ),
            239 => 
            array (
                'id' => 245,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 195,
                'url' => 'completude.pdf',
            ),
            240 => 
            array (
                'id' => 246,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 196,
                'url' => 'constructivite_logique_intuitioniste.pdf',
            ),
            241 => 
            array (
                'id' => 247,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 197,
                'url' => 'decidabilite_arithmetique.pdf',
            ),
            242 => 
            array (
                'id' => 248,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 198,
                'url' => 'fonctions_recursives.pdf',
            ),
            243 => 
            array (
                'id' => 249,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 199,
                'url' => 'groupe_totalement_ordonnable.pdf',
            ),
            244 => 
            array (
                'id' => 250,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 200,
                'url' => 'indecidabilite.pdf',
            ),
            245 => 
            array (
                'id' => 251,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 201,
                'url' => 'kleene_efficace.pdf',
            ),
            246 => 
            array (
                'id' => 252,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 202,
                'url' => 'mediane_temps_lineaire.pdf',
            ),
            247 => 
            array (
                'id' => 253,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 203,
                'url' => 'probleme_voyageur_commerce_euclidien.pdf',
            ),
            248 => 
            array (
                'id' => 254,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 204,
                'url' => 'rationnalite_langage_pile.pdf',
            ),
            249 => 
            array (
                'id' => 255,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 205,
                'url' => 'recherche_facteur_distance_edition.pdf',
            ),
            250 => 
            array (
                'id' => 256,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 206,
                'url' => 'theoreme_cook.pdf',
            ),
            251 => 
            array (
                'id' => 257,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 207,
                'url' => 'theorie_ordres_denses.pdf',
            ),
            252 => 
            array (
                'id' => 258,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 208,
                'url' => 'thm_immerman.pdf',
            ),
            253 => 
            array (
                'id' => 259,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 209,
                'url' => 'thm_rice.pdf',
            ),
            254 => 
            array (
                'id' => 260,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 210,
                'url' => 'tri_par_tas.pdf',
            ),
            255 => 
            array (
                'id' => 261,
                'created_at' => '2016-03-17 10:00:02',
                'updated_at' => '2016-03-17 10:00:02',
                'version_id' => 211,
                'url' => 'tri_suffixes.pdf',
            ),
            256 => 
            array (
                'id' => 263,
                'created_at' => '2016-03-24 17:30:28',
                'updated_at' => '2016-03-24 17:30:28',
                'version_id' => 212,
                'url' => 'abel_thm.pdf',
            ),
            257 => 
            array (
                'id' => 264,
                'created_at' => '2016-03-24 17:37:02',
                'updated_at' => '2016-03-24 17:37:02',
                'version_id' => 213,
                'url' => 'aut_znz.pdf',
            ),
            258 => 
            array (
                'id' => 265,
                'created_at' => '2016-03-24 17:39:01',
                'updated_at' => '2016-03-24 17:39:01',
                'version_id' => 214,
                'url' => 'barany_thm.pdf',
            ),
            259 => 
            array (
                'id' => 266,
                'created_at' => '2016-03-24 17:41:24',
                'updated_at' => '2016-03-24 17:41:24',
                'version_id' => 215,
                'url' => 'bezout_faible.pdf',
            ),
            260 => 
            array (
                'id' => 267,
                'created_at' => '2016-03-24 17:44:12',
                'updated_at' => '2016-03-24 17:44:12',
                'version_id' => 216,
                'url' => 'Brauer_thm.pdf',
            ),
            261 => 
            array (
                'id' => 268,
                'created_at' => '2016-03-27 19:29:21',
                'updated_at' => '2016-03-27 19:29:21',
                'version_id' => 217,
                'url' => 'caracteristique_fonction.pdf',
            ),
            262 => 
            array (
                'id' => 269,
                'created_at' => '2016-03-27 19:31:13',
                'updated_at' => '2016-03-27 19:31:13',
                'version_id' => 218,
                'url' => 'cauchy_lipschitz_thm.pdf',
            ),
            263 => 
            array (
                'id' => 270,
                'created_at' => '2016-03-27 19:34:29',
                'updated_at' => '2016-03-27 19:34:29',
                'version_id' => 219,
                'url' => 'chaleur_cercle.pdf',
            ),
            264 => 
            array (
                'id' => 271,
                'created_at' => '2016-03-27 19:41:20',
                'updated_at' => '2016-03-27 19:41:20',
                'version_id' => 220,
                'url' => 'cinq_points_conique.pdf',
            ),
            265 => 
            array (
                'id' => 272,
                'created_at' => '2016-03-27 19:43:43',
                'updated_at' => '2016-03-27 19:43:43',
                'version_id' => 221,
                'url' => 'circulaire_groupe.pdf',
            ),
            266 => 
            array (
                'id' => 273,
                'created_at' => '2016-03-27 19:45:04',
                'updated_at' => '2016-03-27 19:45:04',
                'version_id' => 222,
                'url' => 'corps_fini_existence.pdf',
            ),
            267 => 
            array (
                'id' => 275,
                'created_at' => '2016-03-27 19:48:44',
                'updated_at' => '2016-03-27 19:48:44',
                'version_id' => 224,
                'url' => 'determinant_series_formelles.pdf',
            ),
            268 => 
            array (
                'id' => 276,
                'created_at' => '2016-03-27 19:49:39',
                'updated_at' => '2016-03-27 19:49:39',
                'version_id' => 225,
                'url' => 'deux_carres.pdf',
            ),
            269 => 
            array (
                'id' => 277,
                'created_at' => '2016-03-27 19:50:37',
                'updated_at' => '2016-03-27 19:50:37',
                'version_id' => 226,
                'url' => 'dirichlet_thm.pdf',
            ),
            270 => 
            array (
                'id' => 278,
                'created_at' => '2016-03-27 19:51:52',
                'updated_at' => '2016-03-27 19:51:52',
                'version_id' => 227,
                'url' => 'exp_homeo.pdf',
            ),
            271 => 
            array (
                'id' => 279,
                'created_at' => '2016-03-27 19:53:30',
                'updated_at' => '2016-03-27 19:53:30',
                'version_id' => 228,
                'url' => 'fourier_serie_divergente.pdf',
            ),
            272 => 
            array (
                'id' => 280,
                'created_at' => '2016-03-27 19:58:29',
                'updated_at' => '2016-03-27 19:58:29',
                'version_id' => 229,
                'url' => 'frobenius_thm.pdf',
            ),
            273 => 
            array (
                'id' => 281,
                'created_at' => '2016-03-27 19:59:34',
                'updated_at' => '2016-03-27 19:59:34',
                'version_id' => 230,
                'url' => 'galton_watson.pdf',
            ),
            274 => 
            array (
                'id' => 282,
                'created_at' => '2016-03-27 20:01:22',
                'updated_at' => '2016-03-27 20:01:22',
                'version_id' => 231,
                'url' => 'gauss_thm.pdf',
            ),
            275 => 
            array (
                'id' => 283,
                'created_at' => '2016-03-27 20:05:23',
                'updated_at' => '2016-03-27 20:05:23',
                'version_id' => 232,
                'url' => 'glivenko_cantelli.pdf',
            ),
            276 => 
            array (
                'id' => 284,
                'created_at' => '2016-03-27 20:06:35',
                'updated_at' => '2016-03-27 20:06:35',
                'version_id' => 233,
                'url' => 'groupe_modulaire_vi.pdf',
            ),
            277 => 
            array (
                'id' => 285,
                'created_at' => '2016-03-27 20:07:57',
                'updated_at' => '2016-03-27 20:07:57',
                'version_id' => 234,
                'url' => 'groupe_pq.pdf',
            ),
            278 => 
            array (
                'id' => 287,
                'created_at' => '2016-03-27 20:12:11',
                'updated_at' => '2016-03-27 20:12:11',
                'version_id' => 236,
                'url' => 'hadamard_levy_thm.pdf',
            ),
            279 => 
            array (
                'id' => 288,
                'created_at' => '2016-03-27 20:13:56',
                'updated_at' => '2016-03-27 20:13:56',
                'version_id' => 237,
                'url' => 'harmonique_serie.pdf',
            ),
            280 => 
            array (
                'id' => 289,
                'created_at' => '2016-03-27 20:14:47',
                'updated_at' => '2016-03-27 20:14:47',
                'version_id' => 238,
                'url' => 'hoeffding_inegalite.pdf',
            ),
            281 => 
            array (
                'id' => 290,
                'created_at' => '2016-03-27 20:15:54',
                'updated_at' => '2016-03-27 20:15:54',
                'version_id' => 239,
                'url' => 'isometrie_generateurs.pdf',
            ),
            282 => 
            array (
                'id' => 291,
                'created_at' => '2016-03-27 20:16:36',
                'updated_at' => '2016-03-27 20:16:36',
                'version_id' => 240,
                'url' => 'iterative_methode.pdf',
            ),
            283 => 
            array (
                'id' => 292,
                'created_at' => '2016-03-27 20:17:33',
                'updated_at' => '2016-03-27 20:17:33',
                'version_id' => 241,
                'url' => 'jacobi_methode.pdf',
            ),
            284 => 
            array (
                'id' => 294,
                'created_at' => '2016-03-27 20:20:02',
                'updated_at' => '2016-03-27 20:20:02',
                'version_id' => 243,
                'url' => 'kakutani_thm.pdf',
            ),
            285 => 
            array (
                'id' => 295,
                'created_at' => '2016-03-27 20:21:05',
                'updated_at' => '2016-03-27 20:21:05',
                'version_id' => 244,
                'url' => 'kx_automorphismes.pdf',
            ),
            286 => 
            array (
                'id' => 299,
                'created_at' => '2016-03-29 15:05:35',
                'updated_at' => '2016-03-29 15:05:35',
                'version_id' => 248,
                'url' => 'morgenstern_thm.pdf',
            ),
            287 => 
            array (
                'id' => 300,
                'created_at' => '2016-03-29 15:06:23',
                'updated_at' => '2016-03-29 15:06:23',
                'version_id' => 249,
                'url' => 'morse_lemme.pdf',
            ),
            288 => 
            array (
                'id' => 301,
                'created_at' => '2016-03-29 15:07:03',
                'updated_at' => '2016-03-29 15:07:03',
                'version_id' => 250,
                'url' => 'newton_methode.pdf',
            ),
            289 => 
            array (
                'id' => 302,
                'created_at' => '2016-03-29 15:07:37',
                'updated_at' => '2016-03-29 15:07:37',
                'version_id' => 251,
                'url' => 'nombre_parts.pdf',
            ),
            290 => 
            array (
                'id' => 303,
                'created_at' => '2016-03-29 15:08:14',
                'updated_at' => '2016-03-29 15:08:14',
                'version_id' => 252,
                'url' => 'nulle_part_derivable.pdf',
            ),
            291 => 
            array (
                'id' => 304,
                'created_at' => '2016-03-29 15:10:15',
                'updated_at' => '2016-03-29 15:10:15',
                'version_id' => 253,
                'url' => 'ondes_sol_fond.pdf',
            ),
            292 => 
            array (
                'id' => 305,
                'created_at' => '2016-03-29 17:41:33',
                'updated_at' => '2016-03-29 17:41:33',
                'version_id' => 254,
                'url' => 'orthogonaux_poly.pdf',
            ),
            293 => 
            array (
                'id' => 306,
                'created_at' => '2016-03-29 17:43:20',
                'updated_at' => '2016-03-29 17:43:20',
                'version_id' => 255,
                'url' => 'pascal_det_thm.pdf',
            ),
            294 => 
            array (
                'id' => 307,
                'created_at' => '2016-03-29 17:44:07',
                'updated_at' => '2016-03-29 17:44:07',
                'version_id' => 256,
                'url' => 'pascal_thm.pdf',
            ),
            295 => 
            array (
                'id' => 322,
                'created_at' => '2016-04-06 15:39:12',
                'updated_at' => '2016-04-06 15:39:12',
                'version_id' => 271,
                'url' => 'percolation.pdf',
            ),
            296 => 
            array (
                'id' => 310,
                'created_at' => '2016-03-30 11:57:01',
                'updated_at' => '2016-03-30 11:57:01',
                'version_id' => 259,
                'url' => 'poisson_resultant.pdf',
            ),
            297 => 
            array (
                'id' => 311,
                'created_at' => '2016-03-30 11:58:00',
                'updated_at' => '2016-03-30 11:58:00',
                'version_id' => 260,
                'url' => 'principalite_dun_anneau.pdf',
            ),
            298 => 
            array (
                'id' => 312,
                'created_at' => '2016-03-30 11:59:34',
                'updated_at' => '2016-03-30 11:59:34',
                'version_id' => 261,
                'url' => 'isom_groupes_topo.pdf',
            ),
            299 => 
            array (
                'id' => 313,
                'created_at' => '2016-03-30 12:00:20',
                'updated_at' => '2016-03-30 12:00:20',
                'version_id' => 262,
                'url' => 'recipro_forme_quad.pdf',
            ),
            300 => 
            array (
                'id' => 314,
                'created_at' => '2016-03-30 12:01:44',
                'updated_at' => '2016-03-30 12:01:44',
                'version_id' => 263,
                'url' => 'semi_simple_autom.pdf',
            ),
            301 => 
            array (
                'id' => 315,
                'created_at' => '2016-03-30 12:03:59',
                'updated_at' => '2016-03-30 12:03:59',
                'version_id' => 264,
                'url' => 'similitude_classes.pdf',
            ),
            302 => 
            array (
                'id' => 316,
                'created_at' => '2016-03-30 12:04:53',
                'updated_at' => '2016-03-30 12:04:53',
                'version_id' => 265,
                'url' => 'similitude_classes.pdf',
            ),
            303 => 
            array (
                'id' => 317,
                'created_at' => '2016-03-30 12:05:42',
                'updated_at' => '2016-03-30 12:05:42',
                'version_id' => 266,
                'url' => 'symetriques_poly.pdf',
            ),
            304 => 
            array (
                'id' => 318,
                'created_at' => '2016-03-30 12:06:38',
                'updated_at' => '2016-03-30 12:06:38',
                'version_id' => 267,
                'url' => 'table_distingues.pdf',
            ),
            305 => 
            array (
                'id' => 319,
                'created_at' => '2016-03-30 12:07:23',
                'updated_at' => '2016-03-30 12:07:23',
                'version_id' => 268,
                'url' => 'table_S4.pdf',
            ),
            306 => 
            array (
                'id' => 320,
                'created_at' => '2016-03-30 12:08:26',
                'updated_at' => '2016-03-30 12:08:26',
                'version_id' => 269,
                'url' => 'translates_fonction.pdf',
            ),
            307 => 
            array (
                'id' => 321,
                'created_at' => '2016-03-30 12:09:07',
                'updated_at' => '2016-03-30 12:09:07',
                'version_id' => 270,
                'url' => 'weyl_critere.pdf',
            ),
            308 => 
            array (
                'id' => 335,
                'created_at' => '2016-04-06 16:12:04',
                'updated_at' => '2016-04-06 16:12:04',
                'version_id' => 284,
                'url' => 'exp_sn_tom.pdf',
            ),
            309 => 
            array (
                'id' => 370,
                'created_at' => '2016-04-16 08:14:44',
                'updated_at' => '2016-04-16 08:14:44',
                'version_id' => 321,
                'url' => '30 _ Partitions d_un entier en parts fixées.pdf',
            ),
            310 => 
            array (
                'id' => 371,
                'created_at' => '2016-04-16 08:15:40',
                'updated_at' => '2016-04-16 08:15:40',
                'version_id' => 322,
                'url' => 'denombrement_parts_scourte.pdf',
            ),
            311 => 
            array (
                'id' => 378,
                'created_at' => '2016-04-21 11:42:12',
                'updated_at' => '2016-04-21 11:42:12',
                'version_id' => 331,
                'url' => 'meth_gauss_integration_sylvain.pdf',
            ),
            312 => 
            array (
                'id' => 384,
                'created_at' => '2016-10-04 11:12:05',
                'updated_at' => '2016-10-04 11:12:05',
                'version_id' => 334,
                'url' => 'lecon101_promo_2016.pdf',
            ),
            313 => 
            array (
                'id' => 385,
                'created_at' => '2016-10-04 11:22:02',
                'updated_at' => '2016-10-04 11:22:02',
                'version_id' => 335,
                'url' => 'lecon_102_thomas.pdf',
            ),
            314 => 
            array (
                'id' => 386,
                'created_at' => '2016-10-04 11:22:19',
                'updated_at' => '2016-10-04 11:22:19',
                'version_id' => 336,
                'url' => 'lecon_101_pierre.pdf',
            ),
            315 => 
            array (
                'id' => 387,
                'created_at' => '2016-10-04 11:26:13',
                'updated_at' => '2016-10-04 11:26:13',
                'version_id' => 337,
                'url' => 'lecon_103_ensl_2016.pdf',
            ),
            316 => 
            array (
                'id' => 388,
                'created_at' => '2016-10-04 11:27:43',
                'updated_at' => '2016-10-04 11:27:43',
                'version_id' => 338,
                'url' => 'lecon_104_ensl_2016.pdf',
            ),
            317 => 
            array (
                'id' => 390,
                'created_at' => '2016-10-04 11:34:40',
                'updated_at' => '2016-10-04 11:34:40',
                'version_id' => 340,
                'url' => 'lecon_106_ensl_2016.pdf',
            ),
            318 => 
            array (
                'id' => 391,
                'created_at' => '2016-10-04 11:38:42',
                'updated_at' => '2016-10-04 11:38:42',
                'version_id' => 341,
                'url' => 'lecon_107_ensl_2016.pdf',
            ),
            319 => 
            array (
                'id' => 392,
                'created_at' => '2016-10-04 11:39:34',
                'updated_at' => '2016-10-04 11:39:34',
                'version_id' => 342,
                'url' => 'lecon_108_ensl_2016.pdf',
            ),
            320 => 
            array (
                'id' => 393,
                'created_at' => '2016-10-04 11:42:57',
                'updated_at' => '2016-10-04 11:42:57',
                'version_id' => 343,
            'url' => 'lecon_107_ensl_2016_(2).pdf',
            ),
            321 => 
            array (
                'id' => 394,
                'created_at' => '2016-10-04 11:45:41',
                'updated_at' => '2016-10-04 11:45:41',
                'version_id' => 344,
                'url' => 'lecon_109_ensl_2016.pdf',
            ),
            322 => 
            array (
                'id' => 395,
                'created_at' => '2016-10-04 14:25:09',
                'updated_at' => '2016-10-04 14:25:09',
                'version_id' => 345,
                'url' => 'lecon_110_ensl_2016.pdf',
            ),
            323 => 
            array (
                'id' => 396,
                'created_at' => '2016-10-04 14:25:26',
                'updated_at' => '2016-10-04 14:25:26',
                'version_id' => 346,
                'url' => 'lecon_120_ensl_2016.pdf',
            ),
            324 => 
            array (
                'id' => 397,
                'created_at' => '2016-10-04 14:25:41',
                'updated_at' => '2016-10-04 14:25:41',
                'version_id' => 347,
                'url' => 'lecon_121_ensl_2016.pdf',
            ),
            325 => 
            array (
                'id' => 398,
                'created_at' => '2016-10-04 14:25:56',
                'updated_at' => '2016-10-04 14:25:56',
                'version_id' => 348,
                'url' => 'lecon_122_ensl_2016.pdf',
            ),
            326 => 
            array (
                'id' => 399,
                'created_at' => '2016-10-04 14:26:11',
                'updated_at' => '2016-10-04 14:26:11',
                'version_id' => 349,
                'url' => 'lecon_123_ensl_2016.pdf',
            ),
            327 => 
            array (
                'id' => 400,
                'created_at' => '2016-10-04 14:26:23',
                'updated_at' => '2016-10-04 14:26:23',
                'version_id' => 350,
                'url' => 'lecon_124_ensl_2016.pdf',
            ),
            328 => 
            array (
                'id' => 401,
                'created_at' => '2016-10-04 14:26:36',
                'updated_at' => '2016-10-04 14:26:36',
                'version_id' => 351,
                'url' => 'lecon_125_ensl_2016.pdf',
            ),
            329 => 
            array (
                'id' => 402,
                'created_at' => '2016-10-04 14:26:52',
                'updated_at' => '2016-10-04 14:26:52',
                'version_id' => 352,
                'url' => 'lecon_126_ensl_2016.pdf',
            ),
            330 => 
            array (
                'id' => 403,
                'created_at' => '2016-10-04 14:27:05',
                'updated_at' => '2016-10-04 14:27:05',
                'version_id' => 353,
                'url' => 'lecon_127_ensl_2016.pdf',
            ),
            331 => 
            array (
                'id' => 404,
                'created_at' => '2016-10-04 14:27:18',
                'updated_at' => '2016-10-04 14:27:18',
                'version_id' => 354,
                'url' => 'lecon_140_ensl_2016.pdf',
            ),
            332 => 
            array (
                'id' => 405,
                'created_at' => '2016-10-06 14:52:13',
                'updated_at' => '2016-10-06 14:52:13',
                'version_id' => 355,
                'url' => 'lecon_141_ensl_2016.pdf',
            ),
            333 => 
            array (
                'id' => 406,
                'created_at' => '2016-10-06 14:52:53',
                'updated_at' => '2016-10-06 14:52:53',
                'version_id' => 356,
                'url' => 'lecon_142_ensl_2016.pdf',
            ),
            334 => 
            array (
                'id' => 407,
                'created_at' => '2016-10-06 14:53:20',
                'updated_at' => '2016-10-06 14:53:20',
                'version_id' => 357,
                'url' => 'lecon_143_ensl_2016.pdf',
            ),
            335 => 
            array (
                'id' => 408,
                'created_at' => '2016-10-06 14:53:35',
                'updated_at' => '2016-10-06 14:53:35',
                'version_id' => 358,
                'url' => 'lecon_144_ensl_2016.pdf',
            ),
            336 => 
            array (
                'id' => 409,
                'created_at' => '2016-10-06 14:53:48',
                'updated_at' => '2016-10-06 14:53:48',
                'version_id' => 359,
                'url' => 'lecon_150_ensl_2016.pdf',
            ),
            337 => 
            array (
                'id' => 410,
                'created_at' => '2016-10-06 14:54:01',
                'updated_at' => '2016-10-06 14:54:01',
                'version_id' => 360,
                'url' => 'lecon_151_ensl_2016.pdf',
            ),
            338 => 
            array (
                'id' => 411,
                'created_at' => '2016-10-06 14:54:15',
                'updated_at' => '2016-10-06 14:54:15',
                'version_id' => 361,
                'url' => 'lecon_152_ensl_2016.pdf',
            ),
            339 => 
            array (
                'id' => 412,
                'created_at' => '2016-10-06 14:54:29',
                'updated_at' => '2016-10-06 14:54:29',
                'version_id' => 362,
                'url' => 'lecon_153_ensl_2016.pdf',
            ),
            340 => 
            array (
                'id' => 413,
                'created_at' => '2016-10-06 14:54:41',
                'updated_at' => '2016-10-06 14:54:41',
                'version_id' => 363,
                'url' => 'lecon_154_ensl_2016.pdf',
            ),
            341 => 
            array (
                'id' => 414,
                'created_at' => '2016-10-06 14:54:56',
                'updated_at' => '2016-10-06 14:54:56',
                'version_id' => 364,
                'url' => 'lecon_155_ensl_2016.pdf',
            ),
            342 => 
            array (
                'id' => 415,
                'created_at' => '2016-10-06 15:41:52',
                'updated_at' => '2016-10-06 15:41:52',
                'version_id' => 365,
                'url' => 'lecon_156_ensl_2016.pdf',
            ),
            343 => 
            array (
                'id' => 416,
                'created_at' => '2016-10-06 15:42:06',
                'updated_at' => '2016-10-06 15:42:06',
                'version_id' => 366,
                'url' => 'lecon_157_ensl_2016.pdf',
            ),
            344 => 
            array (
                'id' => 417,
                'created_at' => '2016-10-06 15:42:21',
                'updated_at' => '2016-10-06 15:42:21',
                'version_id' => 367,
                'url' => 'lecon_158_ensl_2016.pdf',
            ),
            345 => 
            array (
                'id' => 418,
                'created_at' => '2016-10-06 15:42:34',
                'updated_at' => '2016-10-06 15:42:34',
                'version_id' => 368,
                'url' => 'lecon_159_ensl_2016.pdf',
            ),
            346 => 
            array (
                'id' => 419,
                'created_at' => '2016-10-06 15:42:50',
                'updated_at' => '2016-10-06 15:42:50',
                'version_id' => 369,
                'url' => 'lecon_160_ensl_2016.pdf',
            ),
            347 => 
            array (
                'id' => 420,
                'created_at' => '2016-10-06 15:43:10',
                'updated_at' => '2016-10-06 15:43:10',
                'version_id' => 370,
                'url' => 'lecon_161_ensl_2016.pdf',
            ),
            348 => 
            array (
                'id' => 421,
                'created_at' => '2016-10-06 15:43:25',
                'updated_at' => '2016-10-06 15:43:25',
                'version_id' => 371,
                'url' => 'lecon_162_ensl_2016.pdf',
            ),
            349 => 
            array (
                'id' => 422,
                'created_at' => '2016-10-06 15:43:41',
                'updated_at' => '2016-10-06 15:43:41',
                'version_id' => 372,
                'url' => 'lecon_170_ensl_2016.pdf',
            ),
            350 => 
            array (
                'id' => 423,
                'created_at' => '2016-10-06 15:43:54',
                'updated_at' => '2016-10-06 15:43:54',
                'version_id' => 373,
                'url' => 'lecon_171_ensl_2016.pdf',
            ),
            351 => 
            array (
                'id' => 424,
                'created_at' => '2016-10-06 15:44:13',
                'updated_at' => '2016-10-06 15:44:13',
                'version_id' => 374,
                'url' => 'lecon_180_ensl_2016.pdf',
            ),
            352 => 
            array (
                'id' => 425,
                'created_at' => '2016-10-06 15:54:48',
                'updated_at' => '2016-10-06 15:54:48',
                'version_id' => 375,
                'url' => 'lecon_181_ensl_2016.pdf',
            ),
            353 => 
            array (
                'id' => 426,
                'created_at' => '2016-10-06 15:55:04',
                'updated_at' => '2016-10-06 15:55:04',
                'version_id' => 376,
                'url' => 'lecon_182_ensl_2016.pdf',
            ),
            354 => 
            array (
                'id' => 427,
                'created_at' => '2016-10-06 15:55:17',
                'updated_at' => '2016-10-06 15:55:17',
                'version_id' => 377,
                'url' => 'lecon_183_ensl_2016.pdf',
            ),
            355 => 
            array (
                'id' => 428,
                'created_at' => '2016-10-06 15:55:35',
                'updated_at' => '2016-10-06 15:55:35',
                'version_id' => 378,
                'url' => 'lecon_190_ensl_2016.pdf',
            ),
            356 => 
            array (
                'id' => 429,
                'created_at' => '2016-10-06 15:55:45',
                'updated_at' => '2016-10-06 15:55:45',
                'version_id' => 379,
                'url' => 'lecon_190_ensl_2016_2.pdf',
            ),
            357 => 
            array (
                'id' => 430,
                'created_at' => '2016-10-06 15:55:59',
                'updated_at' => '2016-10-06 15:55:59',
                'version_id' => 380,
                'url' => 'lecon_201_ensl_2016.pdf',
            ),
            358 => 
            array (
                'id' => 431,
                'created_at' => '2016-10-06 15:56:13',
                'updated_at' => '2016-10-06 15:56:13',
                'version_id' => 381,
                'url' => 'lecon_202_ensl_2016.pdf',
            ),
            359 => 
            array (
                'id' => 432,
                'created_at' => '2016-10-06 15:56:25',
                'updated_at' => '2016-10-06 15:56:25',
                'version_id' => 382,
                'url' => 'lecon_203_ensl_2016.pdf',
            ),
            360 => 
            array (
                'id' => 433,
                'created_at' => '2016-10-06 15:56:38',
                'updated_at' => '2016-10-06 15:56:38',
                'version_id' => 383,
                'url' => 'lecon_204_ensl_2016.pdf',
            ),
            361 => 
            array (
                'id' => 434,
                'created_at' => '2016-10-07 14:37:50',
                'updated_at' => '2016-10-07 14:37:50',
                'version_id' => 384,
                'url' => 'lecon_205_ensl_2016.pdf',
            ),
            362 => 
            array (
                'id' => 435,
                'created_at' => '2016-10-07 14:38:03',
                'updated_at' => '2016-10-07 14:38:03',
                'version_id' => 385,
                'url' => 'lecon_206_ensl_2016.pdf',
            ),
            363 => 
            array (
                'id' => 436,
                'created_at' => '2016-10-07 14:38:15',
                'updated_at' => '2016-10-07 14:38:15',
                'version_id' => 386,
                'url' => 'lecon_207_ensl_2016.pdf',
            ),
            364 => 
            array (
                'id' => 437,
                'created_at' => '2016-10-07 14:38:27',
                'updated_at' => '2016-10-07 14:38:27',
                'version_id' => 387,
                'url' => 'lecon_208_ensl_2016.pdf',
            ),
            365 => 
            array (
                'id' => 438,
                'created_at' => '2016-10-07 14:38:41',
                'updated_at' => '2016-10-07 14:38:41',
                'version_id' => 388,
                'url' => 'lecon_209_ensl_2016.pdf',
            ),
            366 => 
            array (
                'id' => 439,
                'created_at' => '2016-10-07 14:38:53',
                'updated_at' => '2016-10-07 14:38:53',
                'version_id' => 389,
                'url' => 'lecon_213_ensl_2016.pdf',
            ),
            367 => 
            array (
                'id' => 440,
                'created_at' => '2016-10-07 14:39:05',
                'updated_at' => '2016-10-07 14:39:05',
                'version_id' => 390,
                'url' => 'lecon_214_ensl_2016.pdf',
            ),
            368 => 
            array (
                'id' => 441,
                'created_at' => '2016-10-07 14:39:18',
                'updated_at' => '2016-10-07 14:39:18',
                'version_id' => 391,
                'url' => 'lecon_215_ensl_2016.pdf',
            ),
            369 => 
            array (
                'id' => 442,
                'created_at' => '2016-10-07 14:39:31',
                'updated_at' => '2016-10-07 14:39:31',
                'version_id' => 392,
                'url' => 'lecon_217_ensl_2016.pdf',
            ),
            370 => 
            array (
                'id' => 443,
                'created_at' => '2016-10-07 14:39:39',
                'updated_at' => '2016-10-07 14:39:39',
                'version_id' => 393,
                'url' => 'lecon_217_ensl_2016_2.pdf',
            ),
            371 => 
            array (
                'id' => 444,
                'created_at' => '2016-10-07 14:39:53',
                'updated_at' => '2016-10-07 14:39:53',
                'version_id' => 394,
                'url' => 'lecon_218_ensl_2016.pdf',
            ),
            372 => 
            array (
                'id' => 445,
                'created_at' => '2016-10-07 14:40:07',
                'updated_at' => '2016-10-07 14:40:07',
                'version_id' => 395,
                'url' => 'lecon_219_ensl_2016.pdf',
            ),
            373 => 
            array (
                'id' => 446,
                'created_at' => '2016-10-10 12:27:37',
                'updated_at' => '2016-10-10 12:27:37',
                'version_id' => 396,
                'url' => 'lecon_220_ensl_2016.pdf',
            ),
            374 => 
            array (
                'id' => 447,
                'created_at' => '2016-10-10 12:27:48',
                'updated_at' => '2016-10-10 12:27:48',
                'version_id' => 397,
                'url' => 'lecon_220_ensl_2016_2.pdf',
            ),
            375 => 
            array (
                'id' => 448,
                'created_at' => '2016-10-10 12:28:07',
                'updated_at' => '2016-10-10 12:28:07',
                'version_id' => 398,
                'url' => 'lecon_221_ensl_2016.pdf',
            ),
            376 => 
            array (
                'id' => 449,
                'created_at' => '2016-10-10 12:28:28',
                'updated_at' => '2016-10-10 12:28:28',
                'version_id' => 399,
                'url' => 'lecon_222_ensl_2016.pdf',
            ),
            377 => 
            array (
                'id' => 450,
                'created_at' => '2016-10-10 12:28:40',
                'updated_at' => '2016-10-10 12:28:40',
                'version_id' => 400,
                'url' => 'lecon_223_ensl_2016.pdf',
            ),
            378 => 
            array (
                'id' => 451,
                'created_at' => '2016-10-10 12:28:52',
                'updated_at' => '2016-10-10 12:28:52',
                'version_id' => 401,
                'url' => 'lecon_226_ensl_2016.pdf',
            ),
            379 => 
            array (
                'id' => 452,
                'created_at' => '2016-10-10 12:29:42',
                'updated_at' => '2016-10-10 12:29:42',
                'version_id' => 402,
                'url' => 'lecon_228_ensl_2016.pdf',
            ),
            380 => 
            array (
                'id' => 453,
                'created_at' => '2016-10-10 12:29:53',
                'updated_at' => '2016-10-10 12:29:53',
                'version_id' => 403,
                'url' => 'lecon_229_ensl_2016.pdf',
            ),
            381 => 
            array (
                'id' => 454,
                'created_at' => '2016-10-10 12:30:09',
                'updated_at' => '2016-10-10 12:30:09',
                'version_id' => 404,
                'url' => 'lecon_230_ensl_2016.pdf',
            ),
            382 => 
            array (
                'id' => 455,
                'created_at' => '2016-10-10 12:30:23',
                'updated_at' => '2016-10-10 12:30:23',
                'version_id' => 405,
                'url' => 'lecon_232_ensl_2016.pdf',
            ),
            383 => 
            array (
                'id' => 456,
                'created_at' => '2016-10-10 12:30:35',
                'updated_at' => '2016-10-10 12:30:35',
                'version_id' => 406,
                'url' => 'lecon_233_ensl_2016.pdf',
            ),
            384 => 
            array (
                'id' => 457,
                'created_at' => '2016-10-10 12:30:48',
                'updated_at' => '2016-10-10 12:30:48',
                'version_id' => 407,
                'url' => 'lecon_234_ensl_2016.pdf',
            ),
            385 => 
            array (
                'id' => 458,
                'created_at' => '2016-10-10 12:31:01',
                'updated_at' => '2016-10-10 12:31:01',
                'version_id' => 408,
                'url' => 'lecon_236_ensl_2016.pdf',
            ),
            386 => 
            array (
                'id' => 459,
                'created_at' => '2016-10-10 12:31:15',
                'updated_at' => '2016-10-10 12:31:15',
                'version_id' => 409,
                'url' => 'lecon_239_ensl_2016.pdf',
            ),
            387 => 
            array (
                'id' => 460,
                'created_at' => '2016-10-10 12:31:27',
                'updated_at' => '2016-10-10 12:31:27',
                'version_id' => 410,
                'url' => 'lecon_240_ensl_2016.pdf',
            ),
            388 => 
            array (
                'id' => 461,
                'created_at' => '2016-10-10 12:31:46',
                'updated_at' => '2016-10-10 12:31:46',
                'version_id' => 411,
                'url' => 'lecon_224_ensl_2016.pdf',
            ),
            389 => 
            array (
                'id' => 462,
                'created_at' => '2016-10-10 15:48:58',
                'updated_at' => '2016-10-10 15:48:58',
                'version_id' => 412,
                'url' => 'lecon_243_ensl_2016.pdf',
            ),
            390 => 
            array (
                'id' => 463,
                'created_at' => '2016-10-10 15:49:08',
                'updated_at' => '2016-10-10 15:49:08',
                'version_id' => 413,
                'url' => 'lecon_243_ensl_2016_2.pdf',
            ),
            391 => 
            array (
                'id' => 464,
                'created_at' => '2016-10-10 15:49:26',
                'updated_at' => '2016-10-10 15:49:26',
                'version_id' => 414,
                'url' => 'lecon_244_ensl_2016.pdf',
            ),
            392 => 
            array (
                'id' => 465,
                'created_at' => '2016-10-10 15:49:41',
                'updated_at' => '2016-10-10 15:49:41',
                'version_id' => 415,
                'url' => 'lecon_245_ensl_2016.pdf',
            ),
            393 => 
            array (
                'id' => 466,
                'created_at' => '2016-10-10 15:50:11',
                'updated_at' => '2016-10-10 15:50:11',
                'version_id' => 416,
                'url' => 'lecon_246_ensl_2016_2.pdf',
            ),
            394 => 
            array (
                'id' => 467,
                'created_at' => '2016-10-10 15:50:24',
                'updated_at' => '2016-10-10 15:50:24',
                'version_id' => 417,
                'url' => 'lecon_247_ensl_2016.pdf',
            ),
            395 => 
            array (
                'id' => 468,
                'created_at' => '2016-10-10 15:50:44',
                'updated_at' => '2016-10-10 15:50:44',
                'version_id' => 418,
                'url' => 'lecon_249_ensl_2016.pdf',
            ),
            396 => 
            array (
                'id' => 469,
                'created_at' => '2016-10-10 15:51:02',
                'updated_at' => '2016-10-10 15:51:02',
                'version_id' => 419,
                'url' => 'lecon_253_ensl_2016.pdf',
            ),
            397 => 
            array (
                'id' => 470,
                'created_at' => '2016-10-10 15:52:22',
                'updated_at' => '2016-10-10 15:52:22',
                'version_id' => 420,
                'url' => 'lecon_260_ensl_2016.pdf',
            ),
            398 => 
            array (
                'id' => 471,
                'created_at' => '2016-10-10 15:52:34',
                'updated_at' => '2016-10-10 15:52:34',
                'version_id' => 421,
                'url' => 'lecon_261_ensl_2016.pdf',
            ),
            399 => 
            array (
                'id' => 472,
                'created_at' => '2016-10-10 15:52:48',
                'updated_at' => '2016-10-10 15:52:48',
                'version_id' => 422,
                'url' => 'lecon_262_ensl_2016.pdf',
            ),
            400 => 
            array (
                'id' => 473,
                'created_at' => '2016-10-10 15:53:00',
                'updated_at' => '2016-10-10 15:53:00',
                'version_id' => 423,
                'url' => 'lecon_263_ensl_2016.pdf',
            ),
            401 => 
            array (
                'id' => 474,
                'created_at' => '2016-10-10 15:53:10',
                'updated_at' => '2016-10-10 15:53:10',
                'version_id' => 424,
                'url' => 'lecon_264_ensl_2016.pdf',
            ),
            402 => 
            array (
                'id' => 475,
                'created_at' => '2016-10-11 13:20:42',
                'updated_at' => '2016-10-11 13:20:42',
                'version_id' => 425,
                'url' => '101.pdf',
            ),
            403 => 
            array (
                'id' => 476,
                'created_at' => '2016-10-12 08:06:02',
                'updated_at' => '2016-10-12 08:06:02',
                'version_id' => 426,
                'url' => '102.pdf',
            ),
            404 => 
            array (
                'id' => 477,
                'created_at' => '2016-10-12 08:07:27',
                'updated_at' => '2016-10-12 08:07:27',
                'version_id' => 427,
                'url' => '103.pdf',
            ),
            405 => 
            array (
                'id' => 478,
                'created_at' => '2016-10-12 08:09:37',
                'updated_at' => '2016-10-12 08:09:37',
                'version_id' => 428,
                'url' => '104.pdf',
            ),
            406 => 
            array (
                'id' => 479,
                'created_at' => '2016-10-12 08:10:20',
                'updated_at' => '2016-10-12 08:10:20',
                'version_id' => 429,
                'url' => '105.pdf',
            ),
            407 => 
            array (
                'id' => 480,
                'created_at' => '2016-10-12 08:10:45',
                'updated_at' => '2016-10-12 08:10:45',
                'version_id' => 430,
                'url' => '106.pdf',
            ),
            408 => 
            array (
                'id' => 481,
                'created_at' => '2016-10-12 08:11:10',
                'updated_at' => '2016-10-12 08:11:10',
                'version_id' => 431,
                'url' => '107.pdf',
            ),
            409 => 
            array (
                'id' => 482,
                'created_at' => '2016-10-12 08:11:44',
                'updated_at' => '2016-10-12 08:11:44',
                'version_id' => 432,
                'url' => '108.pdf',
            ),
            410 => 
            array (
                'id' => 483,
                'created_at' => '2016-10-12 08:12:05',
                'updated_at' => '2016-10-12 08:12:05',
                'version_id' => 433,
                'url' => '109.pdf',
            ),
            411 => 
            array (
                'id' => 484,
                'created_at' => '2016-10-12 08:12:46',
                'updated_at' => '2016-10-12 08:12:46',
                'version_id' => 435,
                'url' => '120.pdf',
            ),
            412 => 
            array (
                'id' => 485,
                'created_at' => '2016-10-12 08:13:39',
                'updated_at' => '2016-10-12 08:13:39',
                'version_id' => 436,
                'url' => '121.pdf',
            ),
            413 => 
            array (
                'id' => 486,
                'created_at' => '2016-10-12 08:14:01',
                'updated_at' => '2016-10-12 08:14:01',
                'version_id' => 437,
                'url' => '122.pdf',
            ),
            414 => 
            array (
                'id' => 487,
                'created_at' => '2016-10-12 12:54:05',
                'updated_at' => '2016-10-12 12:54:05',
                'version_id' => 438,
                'url' => '123.pdf',
            ),
            415 => 
            array (
                'id' => 488,
                'created_at' => '2016-10-12 12:54:28',
                'updated_at' => '2016-10-12 12:54:28',
                'version_id' => 439,
                'url' => '124.pdf',
            ),
            416 => 
            array (
                'id' => 489,
                'created_at' => '2016-10-12 12:54:49',
                'updated_at' => '2016-10-12 12:54:49',
                'version_id' => 440,
                'url' => '125.pdf',
            ),
            417 => 
            array (
                'id' => 490,
                'created_at' => '2016-10-12 12:55:08',
                'updated_at' => '2016-10-12 12:55:08',
                'version_id' => 441,
                'url' => '126.pdf',
            ),
            418 => 
            array (
                'id' => 491,
                'created_at' => '2016-10-12 12:55:30',
                'updated_at' => '2016-10-12 12:55:30',
                'version_id' => 442,
                'url' => '127.pdf',
            ),
            419 => 
            array (
                'id' => 492,
                'created_at' => '2016-10-12 12:57:59',
                'updated_at' => '2016-10-12 12:57:59',
                'version_id' => 443,
                'url' => '140.pdf',
            ),
            420 => 
            array (
                'id' => 493,
                'created_at' => '2016-10-12 12:58:37',
                'updated_at' => '2016-10-12 12:58:37',
                'version_id' => 444,
                'url' => '141.pdf',
            ),
            421 => 
            array (
                'id' => 494,
                'created_at' => '2016-10-12 12:58:55',
                'updated_at' => '2016-10-12 12:58:55',
                'version_id' => 445,
                'url' => '142.pdf',
            ),
            422 => 
            array (
                'id' => 495,
                'created_at' => '2016-10-12 12:59:17',
                'updated_at' => '2016-10-12 12:59:17',
                'version_id' => 446,
                'url' => '143.pdf',
            ),
            423 => 
            array (
                'id' => 496,
                'created_at' => '2016-10-12 12:59:37',
                'updated_at' => '2016-10-12 12:59:37',
                'version_id' => 447,
                'url' => '144.pdf',
            ),
            424 => 
            array (
                'id' => 497,
                'created_at' => '2016-10-12 12:59:57',
                'updated_at' => '2016-10-12 12:59:57',
                'version_id' => 448,
                'url' => '150.pdf',
            ),
            425 => 
            array (
                'id' => 498,
                'created_at' => '2016-10-12 13:00:18',
                'updated_at' => '2016-10-12 13:00:18',
                'version_id' => 449,
                'url' => '151.pdf',
            ),
            426 => 
            array (
                'id' => 499,
                'created_at' => '2016-10-12 13:00:38',
                'updated_at' => '2016-10-12 13:00:38',
                'version_id' => 450,
                'url' => '152.pdf',
            ),
            427 => 
            array (
                'id' => 500,
                'created_at' => '2016-10-12 13:00:59',
                'updated_at' => '2016-10-12 13:00:59',
                'version_id' => 451,
                'url' => '153.pdf',
            ),
            428 => 
            array (
                'id' => 501,
                'created_at' => '2016-10-12 13:01:20',
                'updated_at' => '2016-10-12 13:01:20',
                'version_id' => 452,
                'url' => '154.pdf',
            ),
            429 => 
            array (
                'id' => 502,
                'created_at' => '2016-10-12 13:01:41',
                'updated_at' => '2016-10-12 13:01:41',
                'version_id' => 453,
                'url' => '155.pdf',
            ),
            430 => 
            array (
                'id' => 503,
                'created_at' => '2016-10-12 13:02:01',
                'updated_at' => '2016-10-12 13:02:01',
                'version_id' => 454,
                'url' => '156.pdf',
            ),
            431 => 
            array (
                'id' => 504,
                'created_at' => '2016-10-12 13:02:20',
                'updated_at' => '2016-10-12 13:02:20',
                'version_id' => 455,
                'url' => '157.pdf',
            ),
            432 => 
            array (
                'id' => 505,
                'created_at' => '2016-10-12 13:02:44',
                'updated_at' => '2016-10-12 13:02:44',
                'version_id' => 456,
                'url' => '158.pdf',
            ),
            433 => 
            array (
                'id' => 506,
                'created_at' => '2016-10-12 13:03:03',
                'updated_at' => '2016-10-12 13:03:03',
                'version_id' => 457,
                'url' => '159.pdf',
            ),
            434 => 
            array (
                'id' => 507,
                'created_at' => '2016-10-12 13:03:31',
                'updated_at' => '2016-10-12 13:03:31',
                'version_id' => 458,
                'url' => '160.pdf',
            ),
            435 => 
            array (
                'id' => 508,
                'created_at' => '2016-10-12 13:03:57',
                'updated_at' => '2016-10-12 13:03:57',
                'version_id' => 459,
                'url' => '161.pdf',
            ),
            436 => 
            array (
                'id' => 509,
                'created_at' => '2016-10-12 13:04:29',
                'updated_at' => '2016-10-12 13:04:29',
                'version_id' => 460,
                'url' => '162.pdf',
            ),
            437 => 
            array (
                'id' => 510,
                'created_at' => '2016-10-12 13:04:51',
                'updated_at' => '2016-10-12 13:04:51',
                'version_id' => 461,
                'url' => '170.pdf',
            ),
            438 => 
            array (
                'id' => 511,
                'created_at' => '2016-10-12 13:05:14',
                'updated_at' => '2016-10-12 13:05:14',
                'version_id' => 462,
                'url' => '171.pdf',
            ),
            439 => 
            array (
                'id' => 512,
                'created_at' => '2016-10-12 13:05:34',
                'updated_at' => '2016-10-12 13:05:34',
                'version_id' => 463,
                'url' => '180.pdf',
            ),
            440 => 
            array (
                'id' => 513,
                'created_at' => '2016-10-12 13:05:51',
                'updated_at' => '2016-10-12 13:05:51',
                'version_id' => 464,
                'url' => '181.pdf',
            ),
            441 => 
            array (
                'id' => 514,
                'created_at' => '2016-10-12 13:06:27',
                'updated_at' => '2016-10-12 13:06:27',
                'version_id' => 465,
                'url' => '182.pdf',
            ),
            442 => 
            array (
                'id' => 515,
                'created_at' => '2016-10-12 13:06:45',
                'updated_at' => '2016-10-12 13:06:45',
                'version_id' => 466,
                'url' => '183.pdf',
            ),
            443 => 
            array (
                'id' => 516,
                'created_at' => '2016-10-12 13:07:09',
                'updated_at' => '2016-10-12 13:07:09',
                'version_id' => 467,
                'url' => '190.pdf',
            ),
            444 => 
            array (
                'id' => 517,
                'created_at' => '2016-10-14 13:17:44',
                'updated_at' => '2016-10-14 13:17:44',
                'version_id' => 468,
                'url' => 'julia_ensemble.pdf',
            ),
            445 => 
            array (
                'id' => 518,
                'created_at' => '2016-10-14 13:36:37',
                'updated_at' => '2016-10-14 13:36:37',
                'version_id' => 469,
                'url' => 'bicommutant.pdf',
            ),
            446 => 
            array (
                'id' => 520,
                'created_at' => '2016-10-28 17:27:59',
                'updated_at' => '2016-10-28 17:27:59',
                'version_id' => 471,
                'url' => 'InvSmith.pdf',
            ),
            447 => 
            array (
                'id' => 521,
                'created_at' => '2016-12-12 18:42:39',
                'updated_at' => '2016-12-12 18:42:39',
                'version_id' => 473,
                'url' => '2016_901.pdf',
            ),
            448 => 
            array (
                'id' => 522,
                'created_at' => '2016-12-12 18:43:32',
                'updated_at' => '2016-12-12 18:43:32',
                'version_id' => 474,
                'url' => '2016_901_2.pdf',
            ),
            449 => 
            array (
                'id' => 523,
                'created_at' => '2016-12-12 18:47:45',
                'updated_at' => '2016-12-12 18:47:45',
                'version_id' => 475,
                'url' => '2016_902_plan.pdf',
            ),
            450 => 
            array (
                'id' => 524,
                'created_at' => '2016-12-12 18:47:49',
                'updated_at' => '2016-12-12 18:47:49',
                'version_id' => 475,
                'url' => '2016_902_remarques.pdf',
            ),
            451 => 
            array (
                'id' => 525,
                'created_at' => '2016-12-12 18:47:54',
                'updated_at' => '2016-12-12 18:47:54',
                'version_id' => 475,
                'url' => '2016_902_devs.pdf',
            ),
            452 => 
            array (
                'id' => 526,
                'created_at' => '2016-12-12 18:52:51',
                'updated_at' => '2016-12-12 18:52:51',
                'version_id' => 476,
                'url' => '2016_903_plan.pdf',
            ),
            453 => 
            array (
                'id' => 527,
                'created_at' => '2016-12-12 18:52:56',
                'updated_at' => '2016-12-12 18:52:56',
                'version_id' => 476,
                'url' => '2016_903_remarque.pdf',
            ),
            454 => 
            array (
                'id' => 528,
                'created_at' => '2016-12-12 18:53:02',
                'updated_at' => '2016-12-12 18:53:02',
                'version_id' => 476,
                'url' => '2016_903_devs.pdf',
            ),
            455 => 
            array (
                'id' => 529,
                'created_at' => '2016-12-12 18:55:06',
                'updated_at' => '2016-12-12 18:55:06',
                'version_id' => 477,
                'url' => '2016_906.pdf',
            ),
            456 => 
            array (
                'id' => 530,
                'created_at' => '2016-12-12 18:56:37',
                'updated_at' => '2016-12-12 18:56:37',
                'version_id' => 478,
                'url' => '2016_907.pdf',
            ),
            457 => 
            array (
                'id' => 531,
                'created_at' => '2016-12-12 18:57:19',
                'updated_at' => '2016-12-12 18:57:19',
                'version_id' => 478,
                'url' => '2016_907_remarque.pdf',
            ),
            458 => 
            array (
                'id' => 532,
                'created_at' => '2016-12-12 18:58:37',
                'updated_at' => '2016-12-12 18:58:37',
                'version_id' => 479,
                'url' => '2016_909.pdf',
            ),
            459 => 
            array (
                'id' => 533,
                'created_at' => '2016-12-12 18:59:53',
                'updated_at' => '2016-12-12 18:59:53',
                'version_id' => 480,
                'url' => '2016_910.pdf',
            ),
            460 => 
            array (
                'id' => 534,
                'created_at' => '2016-12-12 19:01:21',
                'updated_at' => '2016-12-12 19:01:21',
                'version_id' => 481,
                'url' => '2016_912.pdf',
            ),
            461 => 
            array (
                'id' => 535,
                'created_at' => '2016-12-12 19:02:44',
                'updated_at' => '2016-12-12 19:02:44',
                'version_id' => 482,
                'url' => '2016_913.pdf',
            ),
            462 => 
            array (
                'id' => 536,
                'created_at' => '2016-12-12 19:03:13',
                'updated_at' => '2016-12-12 19:03:13',
                'version_id' => 482,
                'url' => '2016_913_remarques.pdf',
            ),
            463 => 
            array (
                'id' => 537,
                'created_at' => '2016-12-12 22:21:22',
                'updated_at' => '2016-12-12 22:21:22',
                'version_id' => 483,
                'url' => '2016_914.pdf',
            ),
            464 => 
            array (
                'id' => 538,
                'created_at' => '2016-12-12 22:23:27',
                'updated_at' => '2016-12-12 22:23:27',
                'version_id' => 484,
                'url' => '2016_915.pdf',
            ),
            465 => 
            array (
                'id' => 539,
                'created_at' => '2016-12-12 22:23:32',
                'updated_at' => '2016-12-12 22:23:32',
                'version_id' => 484,
                'url' => '2016_915_commentaires.pdf',
            ),
            466 => 
            array (
                'id' => 540,
                'created_at' => '2016-12-12 22:23:36',
                'updated_at' => '2016-12-12 22:23:36',
                'version_id' => 484,
                'url' => '2016_915_devs.pdf',
            ),
            467 => 
            array (
                'id' => 541,
                'created_at' => '2016-12-12 22:24:36',
                'updated_at' => '2016-12-12 22:24:36',
                'version_id' => 485,
                'url' => '2016_916.pdf',
            ),
            468 => 
            array (
                'id' => 542,
                'created_at' => '2016-12-12 22:24:40',
                'updated_at' => '2016-12-12 22:24:40',
                'version_id' => 485,
                'url' => '2016_916_devs.pdf',
            ),
            469 => 
            array (
                'id' => 543,
                'created_at' => '2016-12-12 22:24:43',
                'updated_at' => '2016-12-12 22:24:43',
                'version_id' => 485,
                'url' => '2016_916_remarques.pdf',
            ),
            470 => 
            array (
                'id' => 544,
                'created_at' => '2016-12-12 22:26:53',
                'updated_at' => '2016-12-12 22:26:53',
                'version_id' => 486,
                'url' => '2016_917.pdf',
            ),
            471 => 
            array (
                'id' => 545,
                'created_at' => '2016-12-12 22:28:07',
                'updated_at' => '2016-12-12 22:28:07',
                'version_id' => 487,
                'url' => '2016_918.pdf',
            ),
            472 => 
            array (
                'id' => 546,
                'created_at' => '2016-12-12 22:28:15',
                'updated_at' => '2016-12-12 22:28:15',
                'version_id' => 487,
                'url' => '2016_918_commentaires.pdf',
            ),
            473 => 
            array (
                'id' => 547,
                'created_at' => '2016-12-12 22:28:21',
                'updated_at' => '2016-12-12 22:28:21',
                'version_id' => 487,
                'url' => '2016_918_devs.pdf',
            ),
            474 => 
            array (
                'id' => 548,
                'created_at' => '2016-12-12 22:29:10',
                'updated_at' => '2016-12-12 22:29:10',
                'version_id' => 488,
                'url' => '2016_918_2.pdf',
            ),
            475 => 
            array (
                'id' => 549,
                'created_at' => '2016-12-12 22:30:29',
                'updated_at' => '2016-12-12 22:30:29',
                'version_id' => 489,
                'url' => '2016_919.pdf',
            ),
            476 => 
            array (
                'id' => 550,
                'created_at' => '2016-12-12 22:30:35',
                'updated_at' => '2016-12-12 22:30:35',
                'version_id' => 489,
                'url' => '2016_919_devs.pdf',
            ),
            477 => 
            array (
                'id' => 551,
                'created_at' => '2016-12-12 22:32:02',
                'updated_at' => '2016-12-12 22:32:02',
                'version_id' => 490,
                'url' => '2016_920.pdf',
            ),
            478 => 
            array (
                'id' => 552,
                'created_at' => '2016-12-12 22:35:14',
                'updated_at' => '2016-12-12 22:35:14',
                'version_id' => 491,
                'url' => '2016_921.pdf',
            ),
            479 => 
            array (
                'id' => 553,
                'created_at' => '2016-12-12 22:36:39',
                'updated_at' => '2016-12-12 22:36:39',
                'version_id' => 492,
                'url' => '2016_922.pdf',
            ),
            480 => 
            array (
                'id' => 554,
                'created_at' => '2016-12-12 22:36:43',
                'updated_at' => '2016-12-12 22:36:43',
                'version_id' => 492,
                'url' => '2016_922_commentaires.pdf',
            ),
            481 => 
            array (
                'id' => 555,
                'created_at' => '2016-12-12 22:36:53',
                'updated_at' => '2016-12-12 22:36:53',
                'version_id' => 492,
                'url' => '2016_922_devs.pdf',
            ),
            482 => 
            array (
                'id' => 556,
                'created_at' => '2016-12-12 22:38:31',
                'updated_at' => '2016-12-12 22:38:31',
                'version_id' => 493,
                'url' => '2016_923.pdf',
            ),
            483 => 
            array (
                'id' => 557,
                'created_at' => '2016-12-12 22:38:34',
                'updated_at' => '2016-12-12 22:38:34',
                'version_id' => 493,
                'url' => '2016_923_devs.pdf',
            ),
            484 => 
            array (
                'id' => 558,
                'created_at' => '2016-12-12 22:40:43',
                'updated_at' => '2016-12-12 22:40:43',
                'version_id' => 494,
                'url' => '2016_924.pdf',
            ),
            485 => 
            array (
                'id' => 559,
                'created_at' => '2016-12-12 22:40:47',
                'updated_at' => '2016-12-12 22:40:47',
                'version_id' => 494,
                'url' => '2016_924_devs.pdf',
            ),
            486 => 
            array (
                'id' => 560,
                'created_at' => '2016-12-12 22:53:58',
                'updated_at' => '2016-12-12 22:53:58',
                'version_id' => 495,
                'url' => '2016_924_2.pdf',
            ),
            487 => 
            array (
                'id' => 561,
                'created_at' => '2016-12-12 22:55:43',
                'updated_at' => '2016-12-12 22:55:43',
                'version_id' => 496,
                'url' => '2016_925.pdf',
            ),
            488 => 
            array (
                'id' => 562,
                'created_at' => '2016-12-12 22:55:46',
                'updated_at' => '2016-12-12 22:55:46',
                'version_id' => 496,
                'url' => '2016_925_commentaires.pdf',
            ),
            489 => 
            array (
                'id' => 563,
                'created_at' => '2016-12-12 22:55:50',
                'updated_at' => '2016-12-12 22:55:50',
                'version_id' => 496,
                'url' => '2016_925_devs.pdf',
            ),
            490 => 
            array (
                'id' => 564,
                'created_at' => '2016-12-12 22:56:55',
                'updated_at' => '2016-12-12 22:56:55',
                'version_id' => 497,
                'url' => '2016_925_2.pdf',
            ),
            491 => 
            array (
                'id' => 565,
                'created_at' => '2016-12-12 22:58:18',
                'updated_at' => '2016-12-12 22:58:18',
                'version_id' => 498,
                'url' => '2016_926.pdf',
            ),
            492 => 
            array (
                'id' => 566,
                'created_at' => '2016-12-12 22:58:21',
                'updated_at' => '2016-12-12 22:58:21',
                'version_id' => 498,
                'url' => '2016_926_devs.pdf',
            ),
            493 => 
            array (
                'id' => 567,
                'created_at' => '2016-12-12 23:01:53',
                'updated_at' => '2016-12-12 23:01:53',
                'version_id' => 499,
                'url' => '2016_927.pdf',
            ),
            494 => 
            array (
                'id' => 568,
                'created_at' => '2016-12-12 23:02:35',
                'updated_at' => '2016-12-12 23:02:35',
                'version_id' => 500,
                'url' => '2016_928.pdf',
            ),
            495 => 
            array (
                'id' => 569,
                'created_at' => '2017-01-10 10:13:39',
                'updated_at' => '2017-01-10 10:13:39',
                'version_id' => 501,
                'url' => '2015_901.pdf',
            ),
            496 => 
            array (
                'id' => 570,
                'created_at' => '2017-01-10 10:15:56',
                'updated_at' => '2017-01-10 10:15:56',
                'version_id' => 502,
                'url' => '2015_902.pdf',
            ),
            497 => 
            array (
                'id' => 571,
                'created_at' => '2017-01-10 10:17:20',
                'updated_at' => '2017-01-10 10:17:20',
                'version_id' => 503,
                'url' => '2015_903.pdf',
            ),
            498 => 
            array (
                'id' => 572,
                'created_at' => '2017-01-10 10:20:05',
                'updated_at' => '2017-01-10 10:20:05',
                'version_id' => 504,
                'url' => '2015_906.pdf',
            ),
            499 => 
            array (
                'id' => 573,
                'created_at' => '2017-01-10 10:22:19',
                'updated_at' => '2017-01-10 10:22:19',
                'version_id' => 505,
                'url' => '2015_907.pdf',
            ),
        ));
        \DB::table('versions_fichiers')->insert(array (
            0 => 
            array (
                'id' => 574,
                'created_at' => '2017-01-10 11:17:32',
                'updated_at' => '2017-01-10 11:17:32',
                'version_id' => 506,
                'url' => '2015_909.pdf',
            ),
            1 => 
            array (
                'id' => 575,
                'created_at' => '2017-01-10 11:20:01',
                'updated_at' => '2017-01-10 11:20:01',
                'version_id' => 507,
                'url' => '2015_910.pdf',
            ),
            2 => 
            array (
                'id' => 576,
                'created_at' => '2017-01-10 11:25:59',
                'updated_at' => '2017-01-10 11:25:59',
                'version_id' => 509,
                'url' => '2015_913.pdf',
            ),
            3 => 
            array (
                'id' => 577,
                'created_at' => '2017-01-10 11:27:12',
                'updated_at' => '2017-01-10 11:27:12',
                'version_id' => 510,
                'url' => '2015_913.pdf',
            ),
            4 => 
            array (
                'id' => 578,
                'created_at' => '2017-01-10 11:29:46',
                'updated_at' => '2017-01-10 11:29:46',
                'version_id' => 511,
                'url' => '2015_914.pdf',
            ),
            5 => 
            array (
                'id' => 579,
                'created_at' => '2017-01-10 11:31:58',
                'updated_at' => '2017-01-10 11:31:58',
                'version_id' => 512,
                'url' => '2015_914_2.pdf',
            ),
            6 => 
            array (
                'id' => 580,
                'created_at' => '2017-01-10 11:33:16',
                'updated_at' => '2017-01-10 11:33:16',
                'version_id' => 513,
                'url' => '2015_915.pdf',
            ),
            7 => 
            array (
                'id' => 581,
                'created_at' => '2017-01-10 11:34:25',
                'updated_at' => '2017-01-10 11:34:25',
                'version_id' => 514,
                'url' => '2015_916_2.pdf',
            ),
            8 => 
            array (
                'id' => 582,
                'created_at' => '2017-01-10 11:35:07',
                'updated_at' => '2017-01-10 11:35:07',
                'version_id' => 515,
                'url' => '2015_915_3.pdf',
            ),
            9 => 
            array (
                'id' => 583,
                'created_at' => '2017-01-10 11:36:11',
                'updated_at' => '2017-01-10 11:36:11',
                'version_id' => 516,
                'url' => '2015_917.pdf',
            ),
            10 => 
            array (
                'id' => 584,
                'created_at' => '2017-01-10 11:37:04',
                'updated_at' => '2017-01-10 11:37:04',
                'version_id' => 517,
                'url' => '2015_918.pdf',
            ),
            11 => 
            array (
                'id' => 585,
                'created_at' => '2017-01-10 11:37:36',
                'updated_at' => '2017-01-10 11:37:36',
                'version_id' => 518,
                'url' => '2015_918_2.pdf',
            ),
            12 => 
            array (
                'id' => 586,
                'created_at' => '2017-01-10 11:38:36',
                'updated_at' => '2017-01-10 11:38:36',
                'version_id' => 519,
                'url' => '2015_919.pdf',
            ),
            13 => 
            array (
                'id' => 587,
                'created_at' => '2017-01-10 11:39:31',
                'updated_at' => '2017-01-10 11:39:31',
                'version_id' => 520,
                'url' => '2015_920.pdf',
            ),
            14 => 
            array (
                'id' => 588,
                'created_at' => '2017-01-10 11:40:13',
                'updated_at' => '2017-01-10 11:40:13',
                'version_id' => 521,
                'url' => '2015_921.pdf',
            ),
            15 => 
            array (
                'id' => 589,
                'created_at' => '2017-01-10 11:41:34',
                'updated_at' => '2017-01-10 11:41:34',
                'version_id' => 522,
                'url' => '2015_923.pdf',
            ),
            16 => 
            array (
                'id' => 590,
                'created_at' => '2017-01-10 11:42:22',
                'updated_at' => '2017-01-10 11:42:22',
                'version_id' => 523,
                'url' => '2015_924.pdf',
            ),
            17 => 
            array (
                'id' => 591,
                'created_at' => '2017-01-10 11:43:15',
                'updated_at' => '2017-01-10 11:43:15',
                'version_id' => 524,
                'url' => '2015_925.pdf',
            ),
            18 => 
            array (
                'id' => 592,
                'created_at' => '2017-01-10 11:44:14',
                'updated_at' => '2017-01-10 11:44:14',
                'version_id' => 525,
                'url' => '2015_926.pdf',
            ),
            19 => 
            array (
                'id' => 593,
                'created_at' => '2017-01-10 11:46:41',
                'updated_at' => '2017-01-10 11:46:41',
                'version_id' => 526,
                'url' => '2015_927.pdf',
            ),
            20 => 
            array (
                'id' => 594,
                'created_at' => '2017-01-10 11:47:36',
                'updated_at' => '2017-01-10 11:47:36',
                'version_id' => 527,
                'url' => '2015_928.pdf',
            ),
            21 => 
            array (
                'id' => 595,
                'created_at' => '2017-01-15 11:57:03',
                'updated_at' => '2017-01-15 11:57:03',
                'version_id' => 528,
                'url' => 'Méthode de Newton.pdf',
            ),
            22 => 
            array (
                'id' => 596,
                'created_at' => '2017-01-15 11:59:15',
                'updated_at' => '2017-01-15 11:59:15',
                'version_id' => 529,
                'url' => '223.pdf',
            ),
            23 => 
            array (
                'id' => 597,
                'created_at' => '2017-01-18 20:25:31',
                'updated_at' => '2017-01-18 20:25:31',
                'version_id' => 530,
                'url' => 'Critère de Weyl.pdf',
            ),
            24 => 
            array (
                'id' => 598,
                'created_at' => '2017-01-18 20:26:27',
                'updated_at' => '2017-01-18 20:26:27',
                'version_id' => 531,
                'url' => 'Densité dans C° des fonctions continues nulle part dérivables.pdf',
            ),
            25 => 
            array (
                'id' => 599,
                'created_at' => '2017-01-18 20:27:03',
                'updated_at' => '2017-01-18 20:27:03',
                'version_id' => 532,
                'url' => 'Densité des polynômes orthogonaux.pdf',
            ),
            26 => 
            array (
                'id' => 600,
                'created_at' => '2017-01-18 20:28:05',
                'updated_at' => '2017-01-18 20:28:05',
                'version_id' => 533,
                'url' => 'Ellipsoïde de John Loewner.pdf',
            ),
            27 => 
            array (
                'id' => 601,
                'created_at' => '2017-01-18 20:28:44',
                'updated_at' => '2017-01-18 20:28:44',
                'version_id' => 534,
                'url' => 'Formule sommatoire de Poisson.pdf',
            ),
            28 => 
            array (
                'id' => 602,
                'created_at' => '2017-01-18 20:32:28',
                'updated_at' => '2017-01-18 20:32:28',
                'version_id' => 535,
                'url' => 'Théorème central limite.pdf',
            ),
            29 => 
            array (
                'id' => 603,
                'created_at' => '2017-01-18 20:33:23',
                'updated_at' => '2017-01-18 20:33:23',
                'version_id' => 536,
                'url' => 'Theoreme de Chevalley Warning.pdf',
            ),
            30 => 
            array (
                'id' => 604,
                'created_at' => '2017-01-18 20:34:39',
                'updated_at' => '2017-01-18 20:34:39',
                'version_id' => 537,
                'url' => 'Theoreme de structure des polynomes symetriques.pdf',
            ),
            31 => 
            array (
                'id' => 605,
                'created_at' => '2017-01-18 20:35:35',
                'updated_at' => '2017-01-18 20:35:35',
                'version_id' => 538,
                'url' => 'Theoreme de Sylow.pdf',
            ),
            32 => 
            array (
                'id' => 654,
                'created_at' => '2017-02-14 08:34:41',
                'updated_at' => '2017-02-14 08:34:41',
                'version_id' => 587,
                'url' => 'Théorème de Kronecker.pdf',
            ),
            33 => 
            array (
                'id' => 607,
                'created_at' => '2017-01-31 12:22:41',
                'updated_at' => '2017-01-31 12:22:41',
                'version_id' => 540,
                'url' => 'Extremas liés.pdf',
            ),
            34 => 
            array (
                'id' => 608,
                'created_at' => '2017-01-31 12:23:47',
                'updated_at' => '2017-01-31 12:23:47',
                'version_id' => 541,
                'url' => 'Polynomes cyclotomiques.pdf',
            ),
            35 => 
            array (
                'id' => 609,
                'created_at' => '2017-01-31 12:26:40',
                'updated_at' => '2017-01-31 12:26:40',
                'version_id' => 542,
                'url' => 'Polynômes de Bernstein.pdf',
            ),
            36 => 
            array (
                'id' => 611,
                'created_at' => '2017-02-02 15:35:57',
                'updated_at' => '2017-02-02 15:41:39',
                'version_id' => 544,
                'url' => '2017_102.pdf',
            ),
            37 => 
            array (
                'id' => 612,
                'created_at' => '2017-02-02 15:40:46',
                'updated_at' => '2017-02-02 15:41:25',
                'version_id' => 545,
                'url' => '2017_106.pdf',
            ),
            38 => 
            array (
                'id' => 613,
                'created_at' => '2017-02-02 15:42:22',
                'updated_at' => '2017-02-02 15:42:26',
                'version_id' => 546,
                'url' => '2017_107.pdf',
            ),
            39 => 
            array (
                'id' => 614,
                'created_at' => '2017-02-02 15:49:26',
                'updated_at' => '2017-02-02 15:49:32',
                'version_id' => 547,
                'url' => '2017_108.pdf',
            ),
            40 => 
            array (
                'id' => 615,
                'created_at' => '2017-02-02 15:50:28',
                'updated_at' => '2017-02-02 15:50:32',
                'version_id' => 548,
                'url' => '2017_110.pdf',
            ),
            41 => 
            array (
                'id' => 616,
                'created_at' => '2017-02-02 15:53:11',
                'updated_at' => '2017-02-02 15:53:16',
                'version_id' => 549,
                'url' => '2017_120.pdf',
            ),
            42 => 
            array (
                'id' => 617,
                'created_at' => '2017-02-02 15:53:59',
                'updated_at' => '2017-02-02 15:54:03',
                'version_id' => 550,
                'url' => '2017_122.pdf',
            ),
            43 => 
            array (
                'id' => 618,
                'created_at' => '2017-02-02 15:54:41',
                'updated_at' => '2017-02-02 15:54:47',
                'version_id' => 551,
                'url' => '2017_123.pdf',
            ),
            44 => 
            array (
                'id' => 619,
                'created_at' => '2017-02-02 15:55:24',
                'updated_at' => '2017-02-02 15:55:28',
                'version_id' => 552,
                'url' => '2017_125.pdf',
            ),
            45 => 
            array (
                'id' => 620,
                'created_at' => '2017-02-02 15:56:51',
                'updated_at' => '2017-02-02 15:56:55',
                'version_id' => 553,
                'url' => '2017_141.pdf',
            ),
            46 => 
            array (
                'id' => 621,
                'created_at' => '2017-02-02 16:07:33',
                'updated_at' => '2017-02-02 16:07:38',
                'version_id' => 554,
                'url' => '2017_144.pdf',
            ),
            47 => 
            array (
                'id' => 622,
                'created_at' => '2017-02-02 16:08:20',
                'updated_at' => '2017-02-02 16:08:25',
                'version_id' => 555,
                'url' => '2017_151.pdf',
            ),
            48 => 
            array (
                'id' => 623,
                'created_at' => '2017-02-02 16:12:03',
                'updated_at' => '2017-02-02 16:12:06',
                'version_id' => 556,
                'url' => '2017_152.pdf',
            ),
            49 => 
            array (
                'id' => 624,
                'created_at' => '2017-02-02 16:12:41',
                'updated_at' => '2017-02-02 16:12:44',
                'version_id' => 557,
                'url' => '2017_153.pdf',
            ),
            50 => 
            array (
                'id' => 625,
                'created_at' => '2017-02-02 16:13:21',
                'updated_at' => '2017-02-02 16:13:24',
                'version_id' => 558,
                'url' => '2017_157.pdf',
            ),
            51 => 
            array (
                'id' => 626,
                'created_at' => '2017-02-02 16:14:14',
                'updated_at' => '2017-02-02 16:14:17',
                'version_id' => 559,
                'url' => '2017_157.pdf',
            ),
            52 => 
            array (
                'id' => 627,
                'created_at' => '2017-02-02 16:15:27',
                'updated_at' => '2017-02-02 16:15:31',
                'version_id' => 560,
                'url' => '2017_159.pdf',
            ),
            53 => 
            array (
                'id' => 628,
                'created_at' => '2017-02-02 16:16:16',
                'updated_at' => '2017-02-02 16:16:20',
                'version_id' => 561,
                'url' => '2017_160.pdf',
            ),
            54 => 
            array (
                'id' => 629,
                'created_at' => '2017-02-02 16:17:13',
                'updated_at' => '2017-02-02 16:17:16',
                'version_id' => 562,
                'url' => '2017_171.pdf',
            ),
            55 => 
            array (
                'id' => 630,
                'created_at' => '2017-02-02 16:19:20',
                'updated_at' => '2017-02-02 16:19:23',
                'version_id' => 563,
                'url' => '2017_182.pdf',
            ),
            56 => 
            array (
                'id' => 631,
                'created_at' => '2017-02-02 16:19:59',
                'updated_at' => '2017-02-02 16:20:03',
                'version_id' => 564,
                'url' => '2017_190.pdf',
            ),
            57 => 
            array (
                'id' => 632,
                'created_at' => '2017-02-02 16:20:50',
                'updated_at' => '2017-02-02 16:20:54',
                'version_id' => 565,
                'url' => '2017_205.pdf',
            ),
            58 => 
            array (
                'id' => 633,
                'created_at' => '2017-02-02 16:22:38',
                'updated_at' => '2017-02-02 16:22:41',
                'version_id' => 566,
                'url' => '2017_208.pdf',
            ),
            59 => 
            array (
                'id' => 634,
                'created_at' => '2017-02-02 16:23:50',
                'updated_at' => '2017-02-02 16:23:54',
                'version_id' => 567,
                'url' => '2017_215.pdf',
            ),
            60 => 
            array (
                'id' => 635,
                'created_at' => '2017-02-02 16:24:41',
                'updated_at' => '2017-02-02 16:24:45',
                'version_id' => 568,
                'url' => '2017_218.pdf',
            ),
            61 => 
            array (
                'id' => 636,
                'created_at' => '2017-02-02 16:26:24',
                'updated_at' => '2017-02-02 16:26:27',
                'version_id' => 569,
                'url' => '2017_219.pdf',
            ),
            62 => 
            array (
                'id' => 637,
                'created_at' => '2017-02-02 16:28:45',
                'updated_at' => '2017-02-02 16:28:52',
                'version_id' => 570,
                'url' => '2017_221.pdf',
            ),
            63 => 
            array (
                'id' => 638,
                'created_at' => '2017-02-02 16:33:55',
                'updated_at' => '2017-02-02 16:33:58',
                'version_id' => 571,
                'url' => '2017_223.pdf',
            ),
            64 => 
            array (
                'id' => 639,
                'created_at' => '2017-02-02 16:34:28',
                'updated_at' => '2017-02-02 16:34:31',
                'version_id' => 572,
                'url' => '2017_229.pdf',
            ),
            65 => 
            array (
                'id' => 640,
                'created_at' => '2017-02-02 16:35:03',
                'updated_at' => '2017-02-02 16:35:07',
                'version_id' => 573,
                'url' => '2017_230.pdf',
            ),
            66 => 
            array (
                'id' => 641,
                'created_at' => '2017-02-02 16:35:46',
                'updated_at' => '2017-02-02 16:35:50',
                'version_id' => 574,
                'url' => '2017_230_oraux.pdf',
            ),
            67 => 
            array (
                'id' => 642,
                'created_at' => '2017-02-02 16:36:47',
                'updated_at' => '2017-02-02 16:36:50',
                'version_id' => 575,
                'url' => '2017_239.pdf',
            ),
            68 => 
            array (
                'id' => 643,
                'created_at' => '2017-02-02 16:37:53',
                'updated_at' => '2017-02-02 16:37:57',
                'version_id' => 576,
                'url' => '2017_241.pdf',
            ),
            69 => 
            array (
                'id' => 644,
                'created_at' => '2017-02-02 16:42:13',
                'updated_at' => '2017-02-02 16:42:17',
                'version_id' => 577,
                'url' => '2015_245.pdf',
            ),
            70 => 
            array (
                'id' => 645,
                'created_at' => '2017-02-02 16:43:46',
                'updated_at' => '2017-02-02 16:43:50',
                'version_id' => 578,
                'url' => '2017_260.pdf',
            ),
            71 => 
            array (
                'id' => 646,
                'created_at' => '2017-02-02 16:44:21',
                'updated_at' => '2017-02-02 16:44:25',
                'version_id' => 579,
                'url' => '2017_264.pdf',
            ),
            72 => 
            array (
                'id' => 647,
                'created_at' => '2017-02-02 16:44:58',
                'updated_at' => '2017-02-02 16:45:04',
                'version_id' => 580,
                'url' => '2017_918.pdf',
            ),
            73 => 
            array (
                'id' => 648,
                'created_at' => '2017-02-02 16:45:34',
                'updated_at' => '2017-02-02 16:45:38',
                'version_id' => 581,
                'url' => '2017_925.pdf',
            ),
            74 => 
            array (
                'id' => 649,
                'created_at' => '2017-02-03 11:57:23',
                'updated_at' => '2017-02-03 11:57:23',
                'version_id' => 582,
                'url' => 'Théorème de Burnside.pdf',
            ),
            75 => 
            array (
                'id' => 650,
                'created_at' => '2017-02-03 15:14:02',
                'updated_at' => '2017-02-03 15:14:02',
                'version_id' => 583,
                'url' => 'Equation de Hill-Mathieu.pdf',
            ),
            76 => 
            array (
                'id' => 651,
                'created_at' => '2017-02-06 10:18:52',
                'updated_at' => '2017-02-06 10:18:52',
                'version_id' => 584,
                'url' => 'Théorème de Cauchy-Lipschitz.pdf',
            ),
            77 => 
            array (
                'id' => 652,
                'created_at' => '2017-02-13 14:11:35',
                'updated_at' => '2017-02-13 14:11:35',
                'version_id' => 585,
                'url' => 'Endomorphismes semi-simples.pdf',
            ),
            78 => 
            array (
                'id' => 653,
                'created_at' => '2017-02-13 14:15:36',
                'updated_at' => '2017-02-13 14:16:25',
                'version_id' => 586,
                'url' => '2017_170.pdf',
            ),
            79 => 
            array (
                'id' => 655,
                'created_at' => '2017-02-18 20:19:15',
                'updated_at' => '2017-02-18 20:19:15',
                'version_id' => 588,
                'url' => 'Liapounov.pdf',
            ),
            80 => 
            array (
                'id' => 656,
                'created_at' => '2017-02-18 20:32:12',
                'updated_at' => '2017-02-18 20:32:12',
                'version_id' => 589,
                'url' => 'Nombre_zeros_ED.pdf',
            ),
            81 => 
            array (
                'id' => 657,
                'created_at' => '2017-02-18 20:45:08',
                'updated_at' => '2017-02-18 20:45:08',
                'version_id' => 590,
                'url' => 'etude_opq.pdf',
            ),
            82 => 
            array (
                'id' => 658,
                'created_at' => '2017-02-19 20:03:03',
                'updated_at' => '2017-02-19 20:03:03',
                'version_id' => 591,
                'url' => 'diagsymcompacts.pdf',
            ),
            83 => 
            array (
                'id' => 659,
                'created_at' => '2017-02-19 20:09:49',
                'updated_at' => '2017-02-19 20:09:49',
                'version_id' => 592,
                'url' => 'methodesiteratives.pdf',
            ),
            84 => 
            array (
                'id' => 660,
                'created_at' => '2017-02-19 20:16:59',
                'updated_at' => '2017-02-19 20:16:59',
                'version_id' => 593,
                'url' => 'inversion_fonction_caracteristique.pdf',
            ),
            85 => 
            array (
                'id' => 662,
                'created_at' => '2017-02-28 07:53:59',
                'updated_at' => '2017-02-28 07:53:59',
                'version_id' => 539,
                'url' => 'An simple.pdf',
            ),
            86 => 
            array (
                'id' => 663,
                'created_at' => '2017-02-28 08:09:52',
                'updated_at' => '2017-02-28 08:09:52',
                'version_id' => 594,
                'url' => '162.1.pdf',
            ),
            87 => 
            array (
                'id' => 664,
                'created_at' => '2017-02-28 08:10:08',
                'updated_at' => '2017-02-28 08:10:08',
                'version_id' => 594,
                'url' => '162.2.pdf',
            ),
            88 => 
            array (
                'id' => 665,
                'created_at' => '2017-02-28 08:10:22',
                'updated_at' => '2017-02-28 08:10:22',
                'version_id' => 594,
                'url' => '162.3.pdf',
            ),
            89 => 
            array (
                'id' => 666,
                'created_at' => '2017-02-28 08:12:47',
                'updated_at' => '2017-02-28 08:12:47',
                'version_id' => 595,
                'url' => '142.pdf',
            ),
            90 => 
            array (
                'id' => 667,
                'created_at' => '2017-02-28 08:15:00',
                'updated_at' => '2017-02-28 08:15:00',
                'version_id' => 596,
                'url' => '218.pdf',
            ),
            91 => 
            array (
                'id' => 668,
                'created_at' => '2017-02-28 11:11:26',
                'updated_at' => '2017-02-28 11:11:26',
                'version_id' => 597,
                'url' => 'Dimension du commutant.pdf',
            ),
            92 => 
            array (
                'id' => 670,
                'created_at' => '2017-03-01 17:51:44',
                'updated_at' => '2017-03-01 17:51:44',
                'version_id' => 598,
                'url' => 'Factorisation LU et de Cholesky.pdf',
            ),
            93 => 
            array (
                'id' => 672,
                'created_at' => '2017-03-05 11:22:14',
                'updated_at' => '2017-03-05 11:22:14',
                'version_id' => 600,
                'url' => 'Lie-Kolchin.pdf',
            ),
            94 => 
            array (
                'id' => 673,
                'created_at' => '2017-03-09 10:11:55',
                'updated_at' => '2017-03-09 10:11:55',
                'version_id' => 601,
                'url' => 'lecon154.pdf',
            ),
            95 => 
            array (
                'id' => 674,
                'created_at' => '2017-03-09 14:50:08',
                'updated_at' => '2017-03-09 14:50:08',
                'version_id' => 602,
                'url' => 'Newton Polynomial.pdf',
            ),
            96 => 
            array (
                'id' => 675,
                'created_at' => '2017-03-09 15:01:32',
                'updated_at' => '2017-03-09 15:01:32',
                'version_id' => 603,
                'url' => 'Wedderburn.pdf',
            ),
            97 => 
            array (
                'id' => 676,
                'created_at' => '2017-03-09 15:07:49',
                'updated_at' => '2017-03-09 15:07:49',
                'version_id' => 604,
                'url' => 'Théorème de Brauer.pdf',
            ),
            98 => 
            array (
                'id' => 677,
                'created_at' => '2017-03-09 15:08:08',
                'updated_at' => '2017-03-09 15:08:08',
                'version_id' => 605,
                'url' => 'Théorème de Kronecker.pdf',
            ),
            99 => 
            array (
                'id' => 678,
                'created_at' => '2017-03-09 15:09:22',
                'updated_at' => '2017-03-09 15:09:22',
                'version_id' => 606,
                'url' => 'Caractérisation des poly cyclotomiques.pdf',
            ),
            100 => 
            array (
                'id' => 679,
                'created_at' => '2017-03-09 15:27:14',
                'updated_at' => '2017-03-09 15:27:14',
                'version_id' => 607,
                'url' => 'Espace de Bergman.pdf',
            ),
            101 => 
            array (
                'id' => 680,
                'created_at' => '2017-03-09 15:44:22',
                'updated_at' => '2017-03-09 15:44:22',
                'version_id' => 608,
                'url' => 'Théorème de Koenigs.pdf',
            ),
        ));
        
        
    }
}
