<?php

use Illuminate\Database\Seeder;

class ReferencesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('references')->delete();
        
        \DB::table('references')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:44:36',
                'titre' => 'Proofs from the book',
                'auteurs' => 'Aigner, Ziegler',
                'cote' => 'AIGN',
                'ISBN' => '9782287338458',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:45:50',
                'titre' => 'Thèmes de Géométrie',
                'auteurs' => 'Alessandri',
                'cote' => 'ALES',
                'ISBN' => '978-2100045563',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:47:06',
                'titre' => 'Analyse numérique et optimisation: une introduction à la modélisation mathématique et à la simulation numérique',
                'auteurs' => 'Allaire',
                'cote' => 'ALLA',
                'ISBN' => '9782730210072',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:47:40',
                'titre' => 'Algèbre linéaire numérique',
                'auteurs' => 'Allaire',
                'cote' => 'ALLA',
                'ISBN' => '9782729810016',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:48:36',
                'titre' => 'Groups and representations',
                'auteurs' => 'Alperin,Bell',
                'cote' => 'ALPE',
                'ISBN' => '978-0387945262',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:49:20',
                'titre' => 'Analyse Complexe',
                'auteurs' => 'Amar, Mathéron',
                'cote' => 'AMAR',
                'ISBN' => '978-2842250522',
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:50:28',
                'titre' => 'Élimination : le cas d\'une variable',
                'auteurs' => 'Apéry',
                'cote' => 'APER',
                'ISBN' => '9782705666552',
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Polyèdres réguliers',
                'auteurs' => 'Arnaudies',
                'cote' => 'ARNA',
                'ISBN' => '0',
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:52:04',
                'titre' => 'Groupes, algèbres et géométrie, tome 1 ',
                'auteurs' => 'Arnaudiès',
                'cote' => 'ARNA',
                'ISBN' => '978-2729843083',
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 16:11:49',
                'titre' => 'Cours de mathématiques, tome 1 : Algèbre',
                'auteurs' => 'Ramis, Deschamps, Odoux',
                'cote' => 'RAMI',
                'ISBN' => '978-2100056255',
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:53:47',
                'titre' => 'Géométrie',
                'auteurs' => 'Audin',
                'cote' => 'AUDI',
                'ISBN' => '978-2868838834',
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:54:28',
                'titre' => 'Calcul différentiel',
                'auteurs' => 'Avez',
                'cote' => 'AVEZ',
                'ISBN' => '978-2225790799',
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-05 16:55:23',
                'titre' => 'La leçon d\'analyse à l\'oral de l\'agrégation',
                'auteurs' => 'Avez',
                'cote' => 'AVEZ',
                'ISBN' => '978-2225848230',
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:49:07',
                'titre' => 'La leçon de géométrie à l\'oral de l\'agrégation',
                'auteurs' => 'Avez',
                'cote' => 'AVEZ',
                'ISBN' => '978-2225856280',
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:49:36',
                'titre' => 'Theory of numbers',
                'auteurs' => 'Baker',
                'cote' => 'BAKE',
                'ISBN' => '978-0198531715',
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:50:04',
                'titre' => 'Martingales et chaines de Markov',
                'auteurs' => 'Baldi',
                'cote' => 'BALD',
                'ISBN' => '978-2705664251',
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:50:38',
                'titre' => 'Probabilités',
                'auteurs' => 'Barbe-Ledoux',
                'cote' => 'BARB',
                'ISBN' => '978-2868839312',
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:51:23',
                'titre' => 'Analyse complexe et équations différentielles',
                'auteurs' => 'Barreira',
                'cote' => 'BARR',
                'ISBN' => '978-2-7598-0616-4',
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 16:11:21',
                'titre' => 'Objectif Agrégation',
                'auteurs' => 'Beck, Malick, Peyré',
                'cote' => 'BECK',
                'ISBN' => '978-2914010924',
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-04-06 15:52:21',
                'titre' => 'Promenade aléatoire',
                'auteurs' => 'Benaim',
                'cote' => 'BENA',
                'ISBN' => '978-2730211680',
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Modélisation stochastique et simulation',
                'auteurs' => 'Berc',
                'cote' => 'BERC',
                'ISBN' => '0',
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Combinatoire',
                'auteurs' => 'Berge',
                'cote' => 'BERG',
                'ISBN' => '0',
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Geometry I and II',
                'auteurs' => 'Berger',
                'cote' => 'BERG',
                'ISBN' => '0',
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algebraic graph theory',
                'auteurs' => 'Biggs',
                'cote' => 'BIGG',
                'ISBN' => '0',
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Probality and measure',
                'auteurs' => 'Billingsley',
                'cote' => 'BILL',
                'ISBN' => '0',
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Percolation',
                'auteurs' => 'Bollobas',
                'cote' => 'BOLL',
                'ISBN' => '0',
            ),
            26 => 
            array (
                'id' => 27,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Théorie des distributions',
                'auteurs' => 'Bony',
                'cote' => 'BONY',
                'ISBN' => '0',
            ),
            27 => 
            array (
                'id' => 28,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse fonctionelle',
                'auteurs' => 'Brézis',
                'cote' => 'BREZ',
                'ISBN' => '0',
            ),
            28 => 
            array (
                'id' => 29,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse. Théorie de l\'intégration',
                'auteurs' => 'Briane',
                'cote' => 'BRIA',
                'ISBN' => '0',
            ),
            29 => 
            array (
                'id' => 30,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-07-05 09:55:39',
                'titre' => 'Éléments de théorie des groupes',
                'auteurs' => 'Calais',
                'cote' => 'CALA',
                'ISBN' => '0',
            ),
            30 => 
            array (
                'id' => 31,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements de théorie des anneaux',
                'auteurs' => 'Calais',
                'cote' => 'CALA',
                'ISBN' => '0',
            ),
            31 => 
            array (
                'id' => 32,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 15:40:14',
                'titre' => 'Histoires hédonistes de groupes et géométries, Tome 1',
                'auteurs' => 'Caldero, Germoni',
                'cote' => 'CALD',
                'ISBN' => '0',
            ),
            32 => 
            array (
                'id' => 33,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 15:40:25',
                'titre' => 'Histoires hédonistes de groupes et géométries, Tome 2',
                'auteurs' => 'Caldero, Germoni',
                'cote' => 'CALD',
                'ISBN' => '0',
            ),
            33 => 
            array (
                'id' => 34,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-01-10 12:23:06',
                'titre' => 'Calcul intégral',
                'auteurs' => 'Candelpergler',
                'cote' => 'CAND',
                'ISBN' => '0',
            ),
            34 => 
            array (
                'id' => 35,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Differential Geometry of curves and surfaces',
            'auteurs' => 'Carmo (Do)',
                'cote' => 'CARM',
                'ISBN' => '0',
            ),
            35 => 
            array (
                'id' => 36,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Théorie des corps',
                'auteurs' => 'Carréga',
                'cote' => 'CARR',
                'ISBN' => '0',
            ),
            36 => 
            array (
                'id' => 37,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Théorie des fonctions analytiques d\'une ou plusieurs variables complexes',
                'auteurs' => 'Cartan',
                'cote' => 'CART',
                'ISBN' => '0',
            ),
            37 => 
            array (
                'id' => 38,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul différentiel',
                'auteurs' => 'Cartan',
                'cote' => 'CART',
                'ISBN' => '0',
            ),
            38 => 
            array (
                'id' => 39,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul mathématique avec Sage',
                'auteurs' => 'Casamayou',
                'cote' => 'CAS',
                'ISBN' => '0',
            ),
            39 => 
            array (
                'id' => 40,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Exercices pour l\'agrégation - Analyse 1',
                'auteurs' => 'Chambert-Loir',
                'cote' => 'CHAM',
                'ISBN' => '0',
            ),
            40 => 
            array (
                'id' => 41,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Exercices pour l\'agrégation - Analyse 2',
                'auteurs' => 'Chambert-Loir',
                'cote' => 'CHAM',
                'ISBN' => '0',
            ),
            41 => 
            array (
                'id' => 42,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Exercices pour l\'agrégation - Analyse 3',
                'auteurs' => 'Chambert-Loir',
                'cote' => 'CHAM',
                'ISBN' => '0',
            ),
            42 => 
            array (
                'id' => 43,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Introduction à l\'analyse numérique matricielle et à l\'optimisation',
                'auteurs' => 'Ciarlet',
                'cote' => 'CIAR',
                'ISBN' => '0',
            ),
            43 => 
            array (
                'id' => 44,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre linéaire',
                'auteurs' => 'Cognet',
                'cote' => 'COGN',
                'ISBN' => '0',
            ),
            44 => 
            array (
                'id' => 45,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Number Thoery',
                'auteurs' => 'Cohen',
                'cote' => 'COHE',
                'ISBN' => '0',
            ),
            45 => 
            array (
                'id' => 46,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algebraic number theory',
                'auteurs' => 'Cohen',
                'cote' => 'COHE',
                'ISBN' => '0',
            ),
            46 => 
            array (
                'id' => 47,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Measure Theory',
                'auteurs' => 'Cohn',
                'cote' => 'COHN',
                'ISBN' => '0',
            ),
            47 => 
            array (
                'id' => 48,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements d\'analyse et d\'algèbre',
                'auteurs' => 'Colmez',
                'cote' => 'COLM',
                'ISBN' => '0',
            ),
            48 => 
            array (
                'id' => 49,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre et géométrie',
                'auteurs' => 'Combes',
                'cote' => 'COMB',
                'ISBN' => '0',
            ),
            49 => 
            array (
                'id' => 50,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse combinatoire Tome 1',
                'auteurs' => 'Comtet',
                'cote' => 'COMT',
                'ISBN' => '0',
            ),
            50 => 
            array (
                'id' => 51,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse combinatoire Tome 2',
                'auteurs' => 'Comtet',
                'cote' => 'COMT2',
                'ISBN' => '0',
            ),
            51 => 
            array (
                'id' => 52,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Ideals, varietes, and algorithms',
                'auteurs' => 'Cox',
                'cote' => 'COX',
                'ISBN' => '0',
            ),
            52 => 
            array (
                'id' => 53,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Manuel de mathématiques VOL3 Analyse et géométrie différentiel',
                'auteurs' => 'Debeaumarché',
                'cote' => 'DEBE',
                'ISBN' => '0',
            ),
            53 => 
            array (
                'id' => 54,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Introduction à la théorie des nombres',
                'auteurs' => 'De Koninek, Mercier',
                'cote' => 'DEKO',
                'ISBN' => '0',
            ),
            54 => 
            array (
                'id' => 55,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse numérique et équation différentielle',
                'auteurs' => 'Demailly',
                'cote' => 'DEMA',
                'ISBN' => '0',
            ),
            55 => 
            array (
                'id' => 56,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Cours d\'algèbre',
                'auteurs' => 'Demazure',
                'cote' => 'DEMA',
                'ISBN' => '0',
            ),
            56 => 
            array (
                'id' => 57,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Convexité dans les espaces fonctionnels',
                'auteurs' => 'Demerzel',
                'cote' => 'DEME',
                'ISBN' => '0',
            ),
            57 => 
            array (
                'id' => 58,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul infinitésimal',
                'auteurs' => 'Dieudonné',
                'cote' => 'DIEU',
                'ISBN' => '0',
            ),
            58 => 
            array (
                'id' => 59,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse numérique des EDP',
                'auteurs' => 'DiMenza',
                'cote' => 'DIM',
                'ISBN' => '0',
            ),
            59 => 
            array (
                'id' => 60,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Modélisation à l\'oral de l\'agrégation',
                'auteurs' => 'Dumas',
                'cote' => 'DUMA',
                'ISBN' => '0',
            ),
            60 => 
            array (
                'id' => 61,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Probability : Theory and examples',
                'auteurs' => 'Durret',
                'cote' => 'DURR',
                'ISBN' => '0',
            ),
            61 => 
            array (
                'id' => 62,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Fourier Series and integrals',
                'auteurs' => 'Dym Mc Kean',
                'cote' => 'DYM',
                'ISBN' => '0',
            ),
            62 => 
            array (
                'id' => 63,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Géométrie analytique classique',
                'auteurs' => 'Eiden',
                'cote' => 'EIDE',
                'ISBN' => '0',
            ),
            63 => 
            array (
                'id' => 64,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Asymptotic expansions',
                'auteurs' => 'Erdelyi',
                'cote' => 'ERDE',
                'ISBN' => '0',
            ),
            64 => 
            array (
                'id' => 65,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Fractal Geometry',
                'auteurs' => 'Falconer',
                'cote' => 'FALC',
                'ISBN' => '0',
            ),
            65 => 
            array (
                'id' => 66,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse sur les groupes de Lie',
                'auteurs' => 'Faraut',
                'cote' => 'FARA',
                'ISBN' => '0',
            ),
            66 => 
            array (
                'id' => 67,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul Intégral',
                'auteurs' => 'Faraut',
                'cote' => 'FARA',
                'ISBN' => '0',
            ),
            67 => 
            array (
                'id' => 68,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul des probabilités',
                'auteurs' => 'Foata',
                'cote' => 'FOAT',
                'ISBN' => '0',
            ),
            68 => 
            array (
                'id' => 69,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Processus Stochastiques',
                'auteurs' => 'Foata, Fuchs',
                'cote' => 'FOAT',
                'ISBN' => '0',
            ),
            69 => 
            array (
                'id' => 70,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Algèbre 1',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            70 => 
            array (
                'id' => 71,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Algèbre 2',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            71 => 
            array (
                'id' => 72,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Algèbre 3',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            72 => 
            array (
                'id' => 73,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Analyse 1',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            73 => 
            array (
                'id' => 74,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Analyse 2',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            74 => 
            array (
                'id' => 75,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Analyse 3',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            75 => 
            array (
                'id' => 76,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Analyse 4',
                'auteurs' => 'Francinou, Gianella, Nicolas',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            76 => 
            array (
                'id' => 77,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Exercices mathématiques',
                'auteurs' => 'Francinou, Gianella',
                'cote' => 'FRAN',
                'ISBN' => '0',
            ),
            77 => 
            array (
                'id' => 78,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre et géométrie',
                'auteurs' => 'Fresnel',
                'cote' => 'FRES',
                'ISBN' => '0',
            ),
            78 => 
            array (
                'id' => 79,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Representation theory',
                'auteurs' => 'Fulton Harris',
                'cote' => 'FULT',
                'ISBN' => '0',
            ),
            79 => 
            array (
                'id' => 80,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'The theory of matrices',
                'auteurs' => 'Gantmacher',
                'cote' => 'GANT',
                'ISBN' => '0',
            ),
            80 => 
            array (
                'id' => 81,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'De l\'intégration aux probabilités',
                'auteurs' => 'Garet',
                'cote' => 'GARE',
                'ISBN' => '0',
            ),
            81 => 
            array (
                'id' => 82,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre commutative',
                'auteurs' => 'Goblot',
                'cote' => 'GOBL',
                'ISBN' => '0',
            ),
            82 => 
            array (
                'id' => 83,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre linéaire',
                'auteurs' => 'Goblot',
                'cote' => 'GOBL',
                'ISBN' => '0',
            ),
            83 => 
            array (
                'id' => 84,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Thèmes de géométrie',
                'auteurs' => 'Goblot',
                'cote' => 'GOBL',
                'ISBN' => '0',
            ),
            84 => 
            array (
                'id' => 85,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 16:32:53',
                'titre' => 'Calcul  différentiel ',
                'auteurs' => 'Gonnord, Tosel',
                'cote' => 'GONN',
                'ISBN' => '0',
            ),
            85 => 
            array (
                'id' => 86,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Topologie et analyse fonctionnelle',
                'auteurs' => 'Gonnord',
                'cote' => 'GONN',
                'ISBN' => '0',
            ),
            86 => 
            array (
                'id' => 87,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre',
                'auteurs' => 'Gourdon',
                'cote' => 'GOUR',
                'ISBN' => '0',
            ),
            87 => 
            array (
                'id' => 88,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse',
                'auteurs' => 'Gourdon',
                'cote' => 'GOUR',
                'ISBN' => '0',
            ),
            88 => 
            array (
                'id' => 89,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Equation différentielle',
                'auteurs' => 'Gourmelen',
                'cote' => 'GOUR',
                'ISBN' => '0',
            ),
            89 => 
            array (
                'id' => 90,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Théorie de Galois',
                'auteurs' => 'Gozart',
                'cote' => 'GOZA',
                'ISBN' => '0',
            ),
            90 => 
            array (
                'id' => 91,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre linéaire',
                'auteurs' => 'Grifone',
                'cote' => 'GRIF',
                'ISBN' => '0',
            ),
            91 => 
            array (
                'id' => 92,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Percolation',
                'auteurs' => 'Grimmett',
                'cote' => 'GRIMM',
                'ISBN' => '0',
            ),
            92 => 
            array (
                'id' => 93,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Topologie générale et espaces normés',
                'auteurs' => 'Hage Hassan',
                'cote' => 'HAGE',
                'ISBN' => '0',
            ),
            93 => 
            array (
                'id' => 94,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Les contre-exemples en mathématiques',
                'auteurs' => 'Hauchecorne',
                'cote' => 'HAUC',
                'ISBN' => '0',
            ),
            94 => 
            array (
                'id' => 95,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse numérique',
                'auteurs' => 'Heron',
                'cote' => 'HERO',
                'ISBN' => '0',
            ),
            95 => 
            array (
                'id' => 96,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Arithmetics',
                'auteurs' => 'Hindry',
                'cote' => 'HIND',
                'ISBN' => '0',
            ),
            96 => 
            array (
                'id' => 97,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements d\'analyse fonctionnelle',
                'auteurs' => 'Hirsch',
                'cote' => 'HIRS',
                'ISBN' => '0',
            ),
            97 => 
            array (
                'id' => 98,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Coniques projectives, affines et métriques',
                'auteurs' => 'Ingrao',
                'cote' => 'INGR',
                'ISBN' => '0',
            ),
            98 => 
            array (
                'id' => 99,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'A classical introduction to modern number theory',
                'auteurs' => 'Ireland',
                'cote' => 'IRE',
                'ISBN' => '0',
            ),
            99 => 
            array (
                'id' => 100,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Complex Algebraic Curves',
                'auteurs' => 'Kirwan',
                'cote' => 'KIRW',
                'ISBN' => '0',
            ),
            100 => 
            array (
                'id' => 101,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Un bref aperçu de la géométrie différentielle',
                'auteurs' => 'Kloeckner',
                'cote' => 'KLOE',
                'ISBN' => '0',
            ),
            101 => 
            array (
                'id' => 102,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Géométrie',
                'auteurs' => 'Ladegaillerie',
                'cote' => 'LADE',
                'ISBN' => '0',
            ),
            102 => 
            array (
                'id' => 103,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Introduction aux variétés différentielles',
                'auteurs' => 'Lafontaine',
                'cote' => 'LAFO',
                'ISBN' => '0',
            ),
            103 => 
            array (
                'id' => 104,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre',
                'auteurs' => 'Lang',
                'cote' => 'LANG',
                'ISBN' => '0',
            ),
            104 => 
            array (
                'id' => 105,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul différentiel et intégral',
                'auteurs' => 'Laudenbach',
                'cote' => 'LAUD',
                'ISBN' => '0',
            ),
            105 => 
            array (
                'id' => 106,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Géométrie pour le CAPES et l\'Agrégation',
                'auteurs' => 'Laville',
                'cote' => 'LAVI',
                'ISBN' => '0',
            ),
            106 => 
            array (
                'id' => 107,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Analyse',
                'auteurs' => 'Leichtmann',
                'cote' => 'LEIC',
                'ISBN' => '0',
            ),
            107 => 
            array (
                'id' => 108,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Oraux X-ENS Algèbre',
                'auteurs' => 'Leichtmann',
                'cote' => 'LEIC',
                'ISBN' => '0',
            ),
            108 => 
            array (
                'id' => 109,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Equations aux dérivées partielles et leurs approximations',
                'auteurs' => 'Lucquin',
                'cote' => 'LUCQ',
                'ISBN' => '0',
            ),
            109 => 
            array (
                'id' => 110,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Developpements d\'analyse',
                'auteurs' => 'Madere',
                'cote' => 'MADE',
                'ISBN' => '0',
            ),
            110 => 
            array (
                'id' => 111,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Leçons d\'algèbre',
                'auteurs' => 'Madere',
                'cote' => 'MADE',
                'ISBN' => '0',
            ),
            111 => 
            array (
                'id' => 112,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Leçons d\'analyse',
                'auteurs' => 'Madere',
                'cote' => 'MADRE',
                'ISBN' => '0',
            ),
            112 => 
            array (
                'id' => 113,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Mathématiques analyse L3',
                'auteurs' => 'Marco',
                'cote' => 'MARC',
                'ISBN' => '0',
            ),
            113 => 
            array (
                'id' => 114,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul Formel',
                'auteurs' => 'Mignotte',
                'cote' => 'MIGN',
                'ISBN' => '0',
            ),
            114 => 
            array (
                'id' => 115,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 15:38:22',
                'titre' => 'Groupes de Lie classiques',
                'auteurs' => 'Mneimné, Testard',
                'cote' => 'MNEI',
                'ISBN' => '0',
            ),
            115 => 
            array (
                'id' => 116,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Reduction des endomorphismes',
                'auteurs' => 'Mneimné',
                'cote' => 'MNEI',
                'ISBN' => '0',
            ),
            116 => 
            array (
                'id' => 117,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements de géométrie',
                'auteurs' => 'Mneimné',
                'cote' => 'MNEI',
                'ISBN' => '0',
            ),
            117 => 
            array (
                'id' => 118,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elementary methods in number theory',
                'auteurs' => 'Nathanson',
                'cote' => 'NATH',
                'ISBN' => '0',
            ),
            118 => 
            array (
                'id' => 119,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Integral matrices',
                'auteurs' => 'Newmann',
                'cote' => 'NEW',
                'ISBN' => '0',
            ),
            119 => 
            array (
                'id' => 120,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Markov chains',
                'auteurs' => 'Norris',
                'cote' => 'NORRI',
                'ISBN' => '0',
            ),
            120 => 
            array (
                'id' => 121,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Agrégation de mathématiques épreuve orale',
                'auteurs' => 'Nourdin',
                'cote' => 'NOUR',
                'ISBN' => '0',
            ),
            121 => 
            array (
                'id' => 122,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Exercices d\'algèbre',
                'auteurs' => 'Ortiz',
                'cote' => 'ORTI',
                'ISBN' => '0',
            ),
            122 => 
            array (
                'id' => 123,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Probabilités 1',
                'auteurs' => 'Ouvrard',
                'cote' => 'OUVR',
                'ISBN' => '0',
            ),
            123 => 
            array (
                'id' => 124,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Probabilités 2',
                'auteurs' => 'Ouvrard',
                'cote' => 'OUVR',
                'ISBN' => '0',
            ),
            124 => 
            array (
                'id' => 125,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre discrète et codes correcteurs',
                'auteurs' => 'Papini',
                'cote' => 'PAPI',
                'ISBN' => '0',
            ),
            125 => 
            array (
                'id' => 126,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Complexité algorithmique',
                'auteurs' => 'Perifel',
                'cote' => 'PERI',
                'ISBN' => '0',
            ),
            126 => 
            array (
                'id' => 127,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Cours d\'algèbre',
                'auteurs' => 'Perrin',
                'cote' => 'PERR',
                'ISBN' => '0',
            ),
            127 => 
            array (
                'id' => 128,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Géométrie algébrique',
                'auteurs' => 'Perrin',
                'cote' => 'PERR',
                'ISBN' => '0',
            ),
            128 => 
            array (
                'id' => 129,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre discrète de la transformée de Fourier',
                'auteurs' => 'Peyré',
                'cote' => 'PEYR',
                'ISBN' => '0',
            ),
            129 => 
            array (
                'id' => 130,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Polynomials',
                'auteurs' => 'Prasolov',
                'cote' => 'PRAS',
                'ISBN' => '0',
            ),
            130 => 
            array (
                'id' => 131,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Cours d\'analyse',
                'auteurs' => 'Pommelet',
                'cote' => 'POMM',
                'ISBN' => '0',
            ),
            131 => 
            array (
                'id' => 132,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Methodes numériques pour le calcul scientifique',
                'auteurs' => 'Quarteroni',
                'cote' => 'QUART',
                'ISBN' => '0',
            ),
            132 => 
            array (
                'id' => 133,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 16:37:23',
                'titre' => 'Analyse pour l\'agrégation',
                'auteurs' => 'Queffelec, Zuily',
                'cote' => 'QUEF',
                'ISBN' => '0',
            ),
            133 => 
            array (
                'id' => 134,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Topologie',
                'auteurs' => 'Queffelec',
                'cote' => 'QUEF',
                'ISBN' => '0',
            ),
            134 => 
            array (
                'id' => 135,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'L3',
                'auteurs' => 'Ramis',
                'cote' => 'RAMI',
                'ISBN' => '0',
            ),
            135 => 
            array (
                'id' => 136,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre et géométrie',
                'auteurs' => 'Ramis',
                'cote' => 'RAMI',
                'ISBN' => '0',
            ),
            136 => 
            array (
                'id' => 137,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre',
                'auteurs' => 'Ramis',
                'cote' => 'RAMI',
                'ISBN' => '0',
            ),
            137 => 
            array (
                'id' => 138,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre pour la licence 3',
                'auteurs' => 'Risler',
                'cote' => 'RISL',
                'ISBN' => '0',
            ),
            138 => 
            array (
                'id' => 139,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Statistique mathématiques en action',
                'auteurs' => 'Rivoirard',
                'cote' => 'RIVO',
                'ISBN' => '0',
            ),
            139 => 
            array (
                'id' => 140,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Convex Functions',
                'auteurs' => 'Roberts',
                'cote' => 'ROBE',
                'ISBN' => '0',
            ),
            140 => 
            array (
                'id' => 141,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements d\'analyse réelle',
                'auteurs' => 'Rombaldi',
                'cote' => 'ROMB',
                'ISBN' => '0',
            ),
            141 => 
            array (
                'id' => 142,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Interpolation et approximation',
                'auteurs' => 'Rombaldi',
                'cote' => 'ROMB',
                'ISBN' => '0',
            ),
            142 => 
            array (
                'id' => 143,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse matricielle',
                'auteurs' => 'Rombaldi',
                'cote' => 'ROMB',
                'ISBN' => '0',
            ),
            143 => 
            array (
                'id' => 144,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Equations différentielles',
                'auteurs' => 'Roseau',
                'cote' => 'ROSE',
                'ISBN' => '0',
            ),
            144 => 
            array (
                'id' => 145,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Mathématiques et technologies',
                'auteurs' => 'Rousseau',
                'cote' => 'ROUS',
                'ISBN' => '0',
            ),
            145 => 
            array (
                'id' => 146,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Petit guide de calcul différentiel',
                'auteurs' => 'Rouvière',
                'cote' => 'ROUV',
                'ISBN' => '0',
            ),
            146 => 
            array (
                'id' => 147,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Real analysis',
                'auteurs' => 'Royden',
                'cote' => 'ROYD',
                'ISBN' => '0',
            ),
            147 => 
            array (
                'id' => 148,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse réelle et complexe',
                'auteurs' => 'Rudin',
                'cote' => 'RUDI',
                'ISBN' => '0',
            ),
            148 => 
            array (
                'id' => 149,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse fonctionnelle',
                'auteurs' => 'Rduin',
                'cote' => 'RUDI',
                'ISBN' => '0',
            ),
            149 => 
            array (
                'id' => 150,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Principes d\'analyse mathématiques',
                'auteurs' => 'Rudin',
                'cote' => 'RUDI',
                'ISBN' => '0',
            ),
            150 => 
            array (
                'id' => 151,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Topologie, calcul différentiel et variable complexe',
                'auteurs' => 'Saint Raymond',
                'cote' => 'SAIN',
                'ISBN' => '0',
            ),
            151 => 
            array (
                'id' => 152,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Calcul scientifique',
                'auteurs' => 'Sainsaulier',
                'cote' => 'SAIN',
                'ISBN' => '0',
            ),
            152 => 
            array (
                'id' => 153,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algorithmes fondamentaux',
                'auteurs' => 'Saux Picart',
                'cote' => 'SAUX',
                'ISBN' => '0',
            ),
            153 => 
            array (
                'id' => 154,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Invitation aux formes quadratiques',
                'auteurs' => 'Seguin',
                'cote' => 'SEGU',
                'ISBN' => '0',
            ),
            154 => 
            array (
                'id' => 155,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Cours d\'arithmétique',
                'auteurs' => 'Serre',
                'cote' => 'SERR',
                'ISBN' => '0',
            ),
            155 => 
            array (
                'id' => 156,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Représentations linéaires des groupes finis',
                'auteurs' => 'Serre',
                'cote' => 'SERR',
                'ISBN' => '0',
            ),
            156 => 
            array (
                'id' => 157,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Matrices',
                'auteurs' => 'Serre',
                'cote' => 'SERR',
                'ISBN' => '0',
            ),
            157 => 
            array (
                'id' => 158,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
            'titre' => 'Matrices (2ème édition anglaise)',
                'auteurs' => 'Serre',
                'cote' => 'SERR',
                'ISBN' => '0',
            ),
            158 => 
            array (
                'id' => 159,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Probability theory',
                'auteurs' => 'Sissai',
                'cote' => 'SISS',
                'ISBN' => '0',
            ),
            159 => 
            array (
                'id' => 160,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Recursively enumerabl sets and degrees',
                'auteurs' => 'Soar',
                'cote' => 'SOAR',
                'ISBN' => '0',
            ),
            160 => 
            array (
                'id' => 161,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Fixed point theorems',
                'auteurs' => 'Smart',
                'cote' => 'SMAR',
                'ISBN' => '0',
            ),
            161 => 
            array (
                'id' => 162,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2017-02-02 16:42:02',
                'titre' => 'Complex analysis',
                'auteurs' => 'Stein, Shakarchi',
                'cote' => 'STEI',
                'ISBN' => '0',
            ),
            162 => 
            array (
                'id' => 163,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Real',
                'auteurs' => 'Stein',
                'cote' => 'STEI',
                'ISBN' => '0',
            ),
            163 => 
            array (
                'id' => 164,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Functionnal analysis',
                'auteurs' => 'Stein',
                'cote' => 'STEI',
                'ISBN' => '0',
            ),
            164 => 
            array (
                'id' => 165,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre L3',
                'auteurs' => 'Szpirglas',
                'cote' => 'SZPI',
                'ISBN' => '0',
            ),
            165 => 
            array (
                'id' => 166,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Algèbre',
                'auteurs' => 'Tauvel',
                'cote' => 'TAUV',
                'ISBN' => '0',
            ),
            166 => 
            array (
                'id' => 167,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Géométrie',
                'auteurs' => 'Tauvel',
                'cote' => 'TAUV',
                'ISBN' => '0',
            ),
            167 => 
            array (
                'id' => 168,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Corps commutatifs et théorie de Galois',
                'auteurs' => 'Tauvel',
                'cote' => 'TAUV',
                'ISBN' => '0',
            ),
            168 => 
            array (
                'id' => 169,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Introduction à la théorie analytique et probabiliste des nombres',
                'auteurs' => 'Tennenbaum',
                'cote' => 'TENN',
                'ISBN' => '0',
            ),
            169 => 
            array (
                'id' => 170,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse mathématique',
                'auteurs' => 'Testard',
                'cote' => 'TEST',
                'ISBN' => '0',
            ),
            170 => 
            array (
                'id' => 171,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Les maths pour l\'agreg',
                'auteurs' => 'Teytaud',
                'cote' => 'TEYT',
                'ISBN' => '0',
            ),
            171 => 
            array (
                'id' => 172,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Topologie. Espaces fonctionnels',
                'auteurs' => 'Tisseron',
                'cote' => 'TISS',
                'ISBN' => '0',
            ),
            172 => 
            array (
                'id' => 173,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Thèmes de probabilités et statistique',
                'auteurs' => 'Toulouse',
                'cote' => 'TOUL',
                'ISBN' => '0',
            ),
            173 => 
            array (
                'id' => 174,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Analyse harmonique réelle',
                'auteurs' => 'Willem',
                'cote' => 'WILL',
                'ISBN' => '0',
            ),
            174 => 
            array (
                'id' => 175,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Un max de maths',
                'auteurs' => 'Zavidovique',
                'cote' => 'ZAVI',
                'ISBN' => '0',
            ),
            175 => 
            array (
                'id' => 176,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Cours de cryptographie',
                'auteurs' => 'Zennor',
                'cote' => 'ZENN',
                'ISBN' => '0',
            ),
            176 => 
            array (
                'id' => 177,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements d\'analyse pour l\'agrégation',
                'auteurs' => 'Zuily',
                'cote' => 'ZUIL',
                'ISBN' => '0',
            ),
            177 => 
            array (
                'id' => 178,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Elements de distributions et d\'équations aux dérivées partielles',
                'auteurs' => 'Zuily',
                'cote' => 'ZUIL',
                'ISBN' => '0',
            ),
            178 => 
            array (
                'id' => 179,
                'created_at' => '2016-03-14 13:44:19',
                'updated_at' => '2016-03-14 13:44:19',
                'titre' => 'Problèmes de distributions',
                'auteurs' => 'Zuily',
                'cote' => 'ZUIL',
                'ISBN' => '0',
            ),
            179 => 
            array (
                'id' => 180,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Cours et exercices d\'informatique
',
                'auteurs' => 'Albert',
                'cote' => 'ALB',
                'ISBN' => '0',
            ),
            180 => 
            array (
                'id' => 181,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 13:05:02',
                'titre' => 'Compilers',
                'auteurs' => 'Aho, Ullman, Lam, Sethi',
                'cote' => 'D.3.4 AHO',
                'ISBN' => '0',
            ),
            181 => 
            array (
                'id' => 182,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:23:05',
                'titre' => 'Computational Complexity: A Modern Approach',
                'auteurs' => 'Sanjeev Arora, Boaz Barak',
                'cote' => 'ARO',
                'ISBN' => '0',
            ),
            182 => 
            array (
                'id' => 183,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Calculabilité et décidabilité
',
                'auteurs' => 'Autebert',
                'cote' => 'F.1 AUT',
                'ISBN' => '0',
            ),
            183 => 
            array (
                'id' => 184,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Théorie des langages et des automates
',
                'auteurs' => 'Autebert',
                'cote' => 'F.4.3 AUT',
                'ISBN' => '0',
            ),
            184 => 
            array (
                'id' => 185,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:31:04',
                'titre' => 'Term rewriting and All That',
                'auteurs' => 'Franz Baader',
                'cote' => 'F.4.2 BAA',
                'ISBN' => '0',
            ),
            185 => 
            array (
                'id' => 186,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'String rewriting systems
',
                'auteurs' => 'Book',
                'cote' => 'BOOK',
                'ISBN' => '0',
            ),
            186 => 
            array (
                'id' => 187,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 11:28:57',
                'titre' => 'Langages formels, Calculabilité et Complexité',
                'auteurs' => 'Carton',
                'cote' => 'F.4.3 CART',
                'ISBN' => '0',
            ),
            187 => 
            array (
                'id' => 188,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Logique pour l\'informatique
',
                'auteurs' => 'Cernito',
                'cote' => 'CERN',
                'ISBN' => '0',
            ),
            188 => 
            array (
                'id' => 189,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:41:30',
                'titre' => 'Logique mathématique Tome 1',
                'auteurs' => 'René Cori, Daniel Lascar',
                'cote' => 'F.4.1 CORI',
                'ISBN' => '0',
            ),
            189 => 
            array (
                'id' => 190,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:41:46',
                'titre' => 'Logique mathématique Tome 2',
                'auteurs' => 'René Cori, Daniel Lascar',
                'cote' => 'F.4.1 CORI',
                'ISBN' => '0',
            ),
            190 => 
            array (
                'id' => 191,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 23:00:54',
                'titre' => 'Introduction à l\'algorithmique',
                'auteurs' => 'Thomas H. Cormen, Charles E. Leiserson, Clifford Stein, Ronald Rivest',
                'cote' => 'D.1 CORM',
                'ISBN' => '0',
            ),
            191 => 
            array (
                'id' => 192,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Text algorithms
',
                'auteurs' => 'Crochemore',
                'cote' => 'F.2.1 CROC',
                'ISBN' => '0',
            ),
            192 => 
            array (
                'id' => 193,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 11:15:15',
                'titre' => 'Algorithms on string',
                'auteurs' => 'Crochemore, Hancart et Lecroq',
                'cote' => 'CRO',
                'ISBN' => '0',
            ),
            193 => 
            array (
                'id' => 194,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Algorithms
',
                'auteurs' => 'Dasgupta',
                'cote' => 'F.2.0 DASG',
                'ISBN' => '0',
            ),
            194 => 
            array (
                'id' => 195,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:26:39',
                'titre' => 'Introduction à la logique',
            'auteurs' => 'René David, Karim Nour, Christophe Raffalli (DNR)',
                'cote' => 'F.4.1 DAVI',
                'ISBN' => '0',
            ),
            195 => 
            array (
                'id' => 196,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Mathématiques de l\'informatique
',
                'auteurs' => 'Dehornoy',
                'cote' => 'DEHO',
                'ISBN' => '0',
            ),
            196 => 
            array (
                'id' => 197,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:42:11',
                'titre' => 'Les démonstrations et les algorithmes',
                'auteurs' => 'Gilles Dowek',
                'cote' => 'F.4.1 DOWE',
                'ISBN' => '0',
            ),
            197 => 
            array (
                'id' => 198,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Analytic Combinatorics
',
                'auteurs' => 'Flajolet',
                'cote' => 'FLAJ',
                'ISBN' => '0',
            ),
            198 => 
            array (
                'id' => 199,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 11:21:09',
                'titre' => 'Le Langage des machines',
                'auteurs' => 'Floyd, Beigel',
                'cote' => 'F.4.3 FLOY',
                'ISBN' => '0',
            ),
            199 => 
            array (
                'id' => 200,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 23:00:05',
                'titre' => 'Types de données et algorithmes',
                'auteurs' => ' Christine Froidevaux, Marie-Claude Gaudel, Michèle Soria',
                'cote' => 'E.1 FROI',
                'ISBN' => '0',
            ),
            200 => 
            array (
                'id' => 201,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 11:48:13',
                'titre' => 'Computers and intractability',
                'auteurs' => 'Garey, Johnson',
                'cote' => 'F1.3 GARE',
                'ISBN' => '0',
            ),
            201 => 
            array (
                'id' => 202,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Modern Computer algebra
',
                'auteurs' => 'Gathen',
                'cote' => 'GATH',
                'ISBN' => '0',
            ),
            202 => 
            array (
                'id' => 203,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2017-01-10 11:20:40',
                'titre' => 'Introduction to automata theory, languages and computation',
                'auteurs' => 'Hopcroft, Ullman',
                'cote' => 'F.1.1 HOPC',
                'ISBN' => '0',
            ),
            203 => 
            array (
                'id' => 204,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Algorithm Design
',
                'auteurs' => 'Kleinberg',
                'cote' => 'F.2.2 KLEI',
                'ISBN' => '0',
            ),
            204 => 
            array (
                'id' => 205,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'The art of computer programming
',
                'auteurs' => 'Knuth',
                'cote' => 'F.0 KNUT',
                'ISBN' => '0',
            ),
            205 => 
            array (
                'id' => 206,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Algorithmique parallèle
',
                'auteurs' => 'Legrand',
                'cote' => 'LEGR',
                'ISBN' => '0',
            ),
            206 => 
            array (
                'id' => 207,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'The design and analysis of algorithms
',
                'auteurs' => 'Levitin',
                'cote' => 'F.2.0 LEVI',
                'ISBN' => '0',
            ),
            207 => 
            array (
                'id' => 208,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Algorithms from P to NP
',
                'auteurs' => 'Moret',
                'cote' => 'D.1 MORE',
                'ISBN' => '0',
            ),
            208 => 
            array (
                'id' => 209,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Computational complexity
',
                'auteurs' => 'Papadimitriou',
                'cote' => 'F.2.0 PAPA',
                'ISBN' => '0',
            ),
            209 => 
            array (
                'id' => 210,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Optimisation combinatoire
',
                'auteurs' => 'Rasches',
                'cote' => 'RA',
                'ISBN' => '0',
            ),
            210 => 
            array (
                'id' => 211,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Calculabilité complexité et approximation
',
                'auteurs' => 'Rey',
                'cote' => 'F.2.0 REY',
                'ISBN' => '0',
            ),
            211 => 
            array (
                'id' => 212,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Discrete and combinatorial mathmatics
',
                'auteurs' => 'Rosen',
                'cote' => 'ROSE',
                'ISBN' => '0',
            ),
            212 => 
            array (
                'id' => 213,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Introduction to the theory of computation
',
                'auteurs' => 'Sipser',
                'cote' => 'F.4.3 SIPS',
                'ISBN' => '0',
            ),
            213 => 
            array (
                'id' => 214,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Fondements mathématiques de l\'informatique
',
                'auteurs' => 'Stern',
                'cote' => 'F.0 STER',
                'ISBN' => '0',
            ),
            214 => 
            array (
                'id' => 215,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-12-12 22:39:38',
                'titre' => 'Les compilateurs',
                'auteurs' => 'Reinhard Wilhelm, Dieter Maurer',
                'cote' => 'D.3.4 WILH',
                'ISBN' => '0',
            ),
            215 => 
            array (
                'id' => 216,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Compiler design
',
                'auteurs' => 'Wilhelm',
                'cote' => 'D.3.4 WILH',
                'ISBN' => '0',
            ),
            216 => 
            array (
                'id' => 217,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'The formal semantics of programming langages
',
                'auteurs' => 'Winskel',
                'cote' => 'WINS',
                'ISBN' => '0',
            ),
            217 => 
            array (
                'id' => 218,
                'created_at' => '2016-03-14 13:46:23',
                'updated_at' => '2016-03-14 13:46:23',
                'titre' => 'Introduction à la calculabilité',
                'auteurs' => 'Wolper',
                'cote' => 'F.1.3 WOLP',
                'ISBN' => '0',
            ),
            218 => 
            array (
                'id' => 219,
                'created_at' => '2016-03-29 15:05:13',
                'updated_at' => '2016-03-29 15:05:13',
                'titre' => 'Topology',
                'auteurs' => 'Dugundji',
                'cote' => '',
                'ISBN' => '',
            ),
            219 => 
            array (
                'id' => 220,
                'created_at' => '2016-12-12 12:54:12',
                'updated_at' => '2016-12-12 12:54:12',
                'titre' => 'Algorithms and complexity',
                'auteurs' => 'Wilf',
                'cote' => '',
                'ISBN' => '',
            ),
            220 => 
            array (
                'id' => 221,
                'created_at' => '2016-12-12 18:49:00',
                'updated_at' => '2017-01-10 11:14:19',
                'titre' => 'Eléments d\'algorithmique',
                'auteurs' => 'Beauquier, Berstel et Chrétienne',
                'cote' => '',
                'ISBN' => '',
            ),
            221 => 
            array (
                'id' => 222,
                'created_at' => '2016-12-12 22:33:15',
                'updated_at' => '2016-12-12 22:33:15',
                'titre' => 'Handbook of Automated Reasoning',
                'auteurs' => 'Alan Robinson, Andrei Voronkov',
                'cote' => '',
                'ISBN' => '',
            ),
            222 => 
            array (
                'id' => 223,
                'created_at' => '2016-12-12 22:34:00',
                'updated_at' => '2016-12-12 22:34:00',
                'titre' => 'Logique, réduction, résolution',
                'auteurs' => 'Michel Demazure, René Lalement',
                'cote' => '',
                'ISBN' => '',
            ),
            223 => 
            array (
                'id' => 224,
                'created_at' => '2016-12-12 22:59:13',
                'updated_at' => '2016-12-12 22:59:13',
                'titre' => 'An Introduction to the Analysis of Algorithms',
                'auteurs' => 'Robert Sedgewick, Phillipe Flajolet',
                'cote' => '',
                'ISBN' => '',
            ),
            224 => 
            array (
                'id' => 225,
                'created_at' => '2017-01-10 11:12:34',
                'updated_at' => '2017-01-10 11:12:34',
                'titre' => 'Flexible Pattern Matching in Strings',
                'auteurs' => 'Gonzalo Navarro',
                'cote' => '',
                'ISBN' => '',
            ),
            225 => 
            array (
                'id' => 226,
                'created_at' => '2017-01-10 11:18:15',
                'updated_at' => '2017-01-10 11:18:15',
                'titre' => 'Elements de théorie des automates',
                'auteurs' => 'Sakarovitch',
                'cote' => '',
                'ISBN' => '',
            ),
            226 => 
            array (
                'id' => 227,
                'created_at' => '2017-01-10 11:30:11',
                'updated_at' => '2017-01-10 11:30:11',
                'titre' => 'Introduction à l\'informatique théorique: calculabilité & complexité',
                'auteurs' => 'Arto Salomaa',
                'cote' => '',
                'ISBN' => '',
            ),
            227 => 
            array (
                'id' => 228,
                'created_at' => '2017-01-10 11:32:20',
                'updated_at' => '2017-01-10 11:32:20',
                'titre' => 'Logique et fondements de l\'informatique',
                'auteurs' => 'Rougemont, Lassaigne',
                'cote' => '',
                'ISBN' => '',
            ),
            228 => 
            array (
                'id' => 229,
                'created_at' => '2017-01-10 11:44:33',
                'updated_at' => '2017-01-10 11:44:33',
                'titre' => 'The Design and Analysis of Algorithms',
                'auteurs' => 'Kozen',
                'cote' => '',
                'ISBN' => '',
            ),
            229 => 
            array (
                'id' => 230,
                'created_at' => '2017-01-10 11:45:32',
                'updated_at' => '2017-01-10 11:45:32',
                'titre' => 'Invitation to Fixed Parameter Algorithms',
                'auteurs' => 'Niedermeier',
                'cote' => '',
                'ISBN' => '',
            ),
            230 => 
            array (
                'id' => 231,
                'created_at' => '2017-01-10 11:48:52',
                'updated_at' => '2017-01-10 11:48:52',
                'titre' => 'New NP-hard and NP-complete polynomial and integer divisibility',
                'auteurs' => 'Plaisted',
                'cote' => '',
                'ISBN' => '',
            ),
            231 => 
            array (
                'id' => 232,
                'created_at' => '2017-01-10 11:49:37',
                'updated_at' => '2017-01-10 11:49:37',
            'titre' => 'Backtrack: An O(1) expected time algorithm for the graph coloring problem',
                'auteurs' => 'Wilf',
                'cote' => '',
                'ISBN' => '',
            ),
            232 => 
            array (
                'id' => 233,
                'created_at' => '2017-02-02 15:38:53',
                'updated_at' => '2017-02-02 15:38:53',
                'titre' => 'Algèbre Géométrique',
                'auteurs' => 'Artin',
                'cote' => '',
                'ISBN' => '',
            ),
            233 => 
            array (
                'id' => 234,
                'created_at' => '2017-02-02 15:52:52',
                'updated_at' => '2017-02-02 15:52:52',
                'titre' => 'Algèbre MPSI: cours et 700 exercices corrigés',
                'auteurs' => 'Monier',
                'cote' => '',
                'ISBN' => '',
            ),
            234 => 
            array (
                'id' => 235,
                'created_at' => '2017-02-02 16:10:54',
                'updated_at' => '2017-02-02 16:10:54',
                'titre' => 'Elimination. Le cas d\'une variable.',
                'auteurs' => 'Apery, Jouanolou',
                'cote' => '',
                'ISBN' => '',
            ),
            235 => 
            array (
                'id' => 236,
                'created_at' => '2017-02-02 16:18:39',
                'updated_at' => '2017-02-02 16:18:39',
                'titre' => 'Algèbre et géométries',
                'auteurs' => 'Boyer',
                'cote' => '',
                'ISBN' => '',
            ),
            236 => 
            array (
                'id' => 237,
                'created_at' => '2017-02-02 16:19:01',
                'updated_at' => '2017-02-02 16:19:01',
                'titre' => 'Visual Complex Analysis',
                'auteurs' => 'Needham',
                'cote' => '',
                'ISBN' => '',
            ),
            237 => 
            array (
                'id' => 238,
                'created_at' => '2017-02-02 16:22:23',
                'updated_at' => '2017-02-02 16:22:23',
                'titre' => 'Topologie et analyse, 3ème année',
                'auteurs' => 'Skandalis',
                'cote' => '',
                'ISBN' => '',
            ),
            238 => 
            array (
                'id' => 239,
                'created_at' => '2017-02-02 16:39:55',
                'updated_at' => '2017-02-02 16:39:55',
                'titre' => 'Complex Analysis: an introduction to the theory of analytic functions',
                'auteurs' => 'Ahlfors',
                'cote' => '',
                'ISBN' => '',
            ),
            239 => 
            array (
                'id' => 240,
                'created_at' => '2017-02-02 16:40:43',
                'updated_at' => '2017-02-02 16:40:43',
                'titre' => 'Analyse  complexe',
                'auteurs' => 'Dolbeault',
                'cote' => '',
                'ISBN' => '',
            ),
            240 => 
            array (
                'id' => 241,
                'created_at' => '2017-02-02 16:41:26',
                'updated_at' => '2017-02-02 16:41:26',
                'titre' => 'Complex Proofs of Real Theorems',
                'auteurs' => 'Lax, Zalcman',
                'cote' => '',
                'ISBN' => '',
            ),
            241 => 
            array (
                'id' => 242,
                'created_at' => '2017-02-13 14:16:57',
                'updated_at' => '2017-02-13 14:16:57',
                'titre' => 'Quadratic and Hermitian Forms',
                'auteurs' => 'Scharlau',
                'cote' => '',
                'ISBN' => '',
            ),
            242 => 
            array (
                'id' => 243,
                'created_at' => '2017-02-28 08:16:49',
                'updated_at' => '2017-02-28 08:16:49',
                'titre' => 'Cours d\'analyse',
                'auteurs' => 'Jean-Michel Bony',
                'cote' => '',
                'ISBN' => '',
            ),
        ));
        
        
    }
}
