<?php

use Illuminate\Database\Seeder;

class CouplagesCouvertureTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('couplages_couverture')->delete();
        
        \DB::table('couplages_couverture')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2017-03-25 20:56:53',
                'updated_at' => '2017-03-25 20:56:53',
                'couplage_id' => 15,
                'developpement_id' => 5,
                'lecon_id' => 785,
            ),
            1 => 
            array (
                'id' => 3,
                'created_at' => '2017-03-25 20:57:00',
                'updated_at' => '2017-03-25 20:57:00',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 663,
            ),
            2 => 
            array (
                'id' => 4,
                'created_at' => '2017-03-25 20:59:35',
                'updated_at' => '2017-03-25 20:59:35',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 663,
            ),
            3 => 
            array (
                'id' => 5,
                'created_at' => '2017-03-25 21:15:32',
                'updated_at' => '2017-03-25 21:15:32',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 663,
            ),
            4 => 
            array (
                'id' => 6,
                'created_at' => '2017-03-25 21:20:39',
                'updated_at' => '2017-03-25 21:20:39',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 716,
            ),
            5 => 
            array (
                'id' => 7,
                'created_at' => '2017-03-25 21:20:40',
                'updated_at' => '2017-03-25 21:20:40',
                'couplage_id' => 2,
                'developpement_id' => 8,
                'lecon_id' => 716,
            ),
            6 => 
            array (
                'id' => 8,
                'created_at' => '2017-03-25 21:23:20',
                'updated_at' => '2017-03-25 21:23:20',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 718,
            ),
            7 => 
            array (
                'id' => 9,
                'created_at' => '2017-03-25 21:23:21',
                'updated_at' => '2017-03-25 21:23:21',
                'couplage_id' => 2,
                'developpement_id' => 8,
                'lecon_id' => 718,
            ),
            8 => 
            array (
                'id' => 10,
                'created_at' => '2017-03-25 21:24:09',
                'updated_at' => '2017-03-25 21:24:09',
                'couplage_id' => 2,
                'developpement_id' => 5,
                'lecon_id' => 663,
            ),
            9 => 
            array (
                'id' => 11,
                'created_at' => '2017-03-25 21:24:24',
                'updated_at' => '2017-03-25 21:24:24',
                'couplage_id' => 2,
                'developpement_id' => 5,
                'lecon_id' => 674,
            ),
            10 => 
            array (
                'id' => 13,
                'created_at' => '2017-03-25 21:24:37',
                'updated_at' => '2017-03-25 21:24:37',
                'couplage_id' => 2,
                'developpement_id' => 7,
                'lecon_id' => 674,
            ),
            11 => 
            array (
                'id' => 14,
                'created_at' => '2017-03-25 21:25:00',
                'updated_at' => '2017-03-25 21:25:00',
                'couplage_id' => 2,
                'developpement_id' => 69,
                'lecon_id' => 683,
            ),
            12 => 
            array (
                'id' => 15,
                'created_at' => '2017-03-25 21:25:00',
                'updated_at' => '2017-03-25 21:25:00',
                'couplage_id' => 2,
                'developpement_id' => 13,
                'lecon_id' => 683,
            ),
            13 => 
            array (
                'id' => 18,
                'created_at' => '2017-03-27 14:23:19',
                'updated_at' => '2017-03-27 14:23:19',
                'couplage_id' => 14,
                'developpement_id' => 106,
                'lecon_id' => 778,
            ),
            14 => 
            array (
                'id' => 19,
                'created_at' => '2017-03-27 14:23:19',
                'updated_at' => '2017-03-27 14:23:19',
                'couplage_id' => 14,
                'developpement_id' => 9,
                'lecon_id' => 778,
            ),
            15 => 
            array (
                'id' => 20,
                'created_at' => '2017-03-27 14:23:20',
                'updated_at' => '2017-03-27 14:23:20',
                'couplage_id' => 14,
                'developpement_id' => 146,
                'lecon_id' => 778,
            ),
            16 => 
            array (
                'id' => 21,
                'created_at' => '2017-03-27 14:23:21',
                'updated_at' => '2017-03-27 14:23:21',
                'couplage_id' => 14,
                'developpement_id' => 9,
                'lecon_id' => 779,
            ),
            17 => 
            array (
                'id' => 22,
                'created_at' => '2017-03-27 14:23:22',
                'updated_at' => '2017-03-27 14:23:22',
                'couplage_id' => 14,
                'developpement_id' => 146,
                'lecon_id' => 779,
            ),
            18 => 
            array (
                'id' => 23,
                'created_at' => '2017-03-27 14:23:26',
                'updated_at' => '2017-03-27 14:23:26',
                'couplage_id' => 14,
                'developpement_id' => 11,
                'lecon_id' => 780,
            ),
            19 => 
            array (
                'id' => 24,
                'created_at' => '2017-03-27 14:23:26',
                'updated_at' => '2017-03-27 14:23:26',
                'couplage_id' => 14,
                'developpement_id' => 106,
                'lecon_id' => 780,
            ),
            20 => 
            array (
                'id' => 25,
                'created_at' => '2017-03-27 14:23:31',
                'updated_at' => '2017-03-27 14:23:31',
                'couplage_id' => 14,
                'developpement_id' => 9,
                'lecon_id' => 782,
            ),
            21 => 
            array (
                'id' => 26,
                'created_at' => '2017-03-27 14:23:32',
                'updated_at' => '2017-03-27 14:23:32',
                'couplage_id' => 14,
                'developpement_id' => 146,
                'lecon_id' => 782,
            ),
            22 => 
            array (
                'id' => 27,
                'created_at' => '2017-03-27 14:24:33',
                'updated_at' => '2017-03-27 14:24:33',
                'couplage_id' => 14,
                'developpement_id' => 131,
                'lecon_id' => 784,
            ),
            23 => 
            array (
                'id' => 28,
                'created_at' => '2017-03-27 14:24:37',
                'updated_at' => '2017-03-27 14:24:37',
                'couplage_id' => 14,
                'developpement_id' => 153,
                'lecon_id' => 784,
            ),
            24 => 
            array (
                'id' => 29,
                'created_at' => '2017-03-27 14:24:38',
                'updated_at' => '2017-03-27 14:24:38',
                'couplage_id' => 14,
                'developpement_id' => 153,
                'lecon_id' => 785,
            ),
            25 => 
            array (
                'id' => 30,
                'created_at' => '2017-03-27 14:24:38',
                'updated_at' => '2017-03-27 14:24:38',
                'couplage_id' => 14,
                'developpement_id' => 131,
                'lecon_id' => 785,
            ),
            26 => 
            array (
                'id' => 53,
                'created_at' => '2017-03-27 14:31:55',
                'updated_at' => '2017-03-27 14:31:55',
                'couplage_id' => 14,
                'developpement_id' => 127,
                'lecon_id' => 814,
            ),
            27 => 
            array (
                'id' => 32,
                'created_at' => '2017-03-27 14:24:46',
                'updated_at' => '2017-03-27 14:24:46',
                'couplage_id' => 14,
                'developpement_id' => 173,
                'lecon_id' => 787,
            ),
            28 => 
            array (
                'id' => 33,
                'created_at' => '2017-03-27 14:24:48',
                'updated_at' => '2017-03-27 14:24:48',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 787,
            ),
            29 => 
            array (
                'id' => 34,
                'created_at' => '2017-03-27 14:25:16',
                'updated_at' => '2017-03-27 14:25:16',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 790,
            ),
            30 => 
            array (
                'id' => 35,
                'created_at' => '2017-03-27 14:25:17',
                'updated_at' => '2017-03-27 14:25:17',
                'couplage_id' => 14,
                'developpement_id' => 173,
                'lecon_id' => 790,
            ),
            31 => 
            array (
                'id' => 103,
                'created_at' => '2017-03-28 14:32:06',
                'updated_at' => '2017-03-28 14:32:06',
                'couplage_id' => 14,
                'developpement_id' => 127,
                'lecon_id' => 814,
            ),
            32 => 
            array (
                'id' => 37,
                'created_at' => '2017-03-27 14:25:21',
                'updated_at' => '2017-03-27 14:25:21',
                'couplage_id' => 14,
                'developpement_id' => 11,
                'lecon_id' => 793,
            ),
            33 => 
            array (
                'id' => 38,
                'created_at' => '2017-03-27 14:25:26',
                'updated_at' => '2017-03-27 14:25:26',
                'couplage_id' => 14,
                'developpement_id' => 136,
                'lecon_id' => 793,
            ),
            34 => 
            array (
                'id' => 39,
                'created_at' => '2017-03-27 14:25:36',
                'updated_at' => '2017-03-27 14:25:36',
                'couplage_id' => 14,
                'developpement_id' => 56,
                'lecon_id' => 794,
            ),
            35 => 
            array (
                'id' => 40,
                'created_at' => '2017-03-27 14:25:39',
                'updated_at' => '2017-03-27 14:25:39',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 794,
            ),
            36 => 
            array (
                'id' => 41,
                'created_at' => '2017-03-27 14:25:43',
                'updated_at' => '2017-03-27 14:25:43',
                'couplage_id' => 14,
                'developpement_id' => 38,
                'lecon_id' => 795,
            ),
            37 => 
            array (
                'id' => 42,
                'created_at' => '2017-03-27 14:25:44',
                'updated_at' => '2017-03-27 14:25:44',
                'couplage_id' => 14,
                'developpement_id' => 120,
                'lecon_id' => 795,
            ),
            38 => 
            array (
                'id' => 43,
                'created_at' => '2017-03-27 14:25:53',
                'updated_at' => '2017-03-27 14:25:53',
                'couplage_id' => 14,
                'developpement_id' => 1,
                'lecon_id' => 796,
            ),
            39 => 
            array (
                'id' => 44,
                'created_at' => '2017-03-27 14:25:56',
                'updated_at' => '2017-03-27 14:25:56',
                'couplage_id' => 14,
                'developpement_id' => 136,
                'lecon_id' => 796,
            ),
            40 => 
            array (
                'id' => 45,
                'created_at' => '2017-03-27 14:26:21',
                'updated_at' => '2017-03-27 14:26:21',
                'couplage_id' => 14,
                'developpement_id' => 56,
                'lecon_id' => 800,
            ),
            41 => 
            array (
                'id' => 46,
                'created_at' => '2017-03-27 14:26:22',
                'updated_at' => '2017-03-27 14:26:22',
                'couplage_id' => 14,
                'developpement_id' => 1,
                'lecon_id' => 800,
            ),
            42 => 
            array (
                'id' => 47,
                'created_at' => '2017-03-27 14:26:32',
                'updated_at' => '2017-03-27 14:26:32',
                'couplage_id' => 14,
                'developpement_id' => 162,
                'lecon_id' => 802,
            ),
            43 => 
            array (
                'id' => 48,
                'created_at' => '2017-03-27 14:28:21',
                'updated_at' => '2017-03-27 14:28:21',
                'couplage_id' => 14,
                'developpement_id' => 136,
                'lecon_id' => 805,
            ),
            44 => 
            array (
                'id' => 49,
                'created_at' => '2017-03-27 14:28:52',
                'updated_at' => '2017-03-27 14:28:52',
                'couplage_id' => 14,
                'developpement_id' => 120,
                'lecon_id' => 805,
            ),
            45 => 
            array (
                'id' => 50,
                'created_at' => '2017-03-27 14:28:58',
                'updated_at' => '2017-03-27 14:28:58',
                'couplage_id' => 14,
                'developpement_id' => 104,
                'lecon_id' => 806,
            ),
            46 => 
            array (
                'id' => 51,
                'created_at' => '2017-03-27 14:28:59',
                'updated_at' => '2017-03-27 14:28:59',
                'couplage_id' => 14,
                'developpement_id' => 35,
                'lecon_id' => 806,
            ),
            47 => 
            array (
                'id' => 54,
                'created_at' => '2017-03-27 14:32:11',
                'updated_at' => '2017-03-27 14:32:11',
                'couplage_id' => 14,
                'developpement_id' => 51,
                'lecon_id' => 818,
            ),
            48 => 
            array (
                'id' => 55,
                'created_at' => '2017-03-27 14:32:22',
                'updated_at' => '2017-03-27 14:32:22',
                'couplage_id' => 14,
                'developpement_id' => 96,
                'lecon_id' => 821,
            ),
            49 => 
            array (
                'id' => 56,
                'created_at' => '2017-03-27 14:32:23',
                'updated_at' => '2017-03-27 14:32:23',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 821,
            ),
            50 => 
            array (
                'id' => 57,
                'created_at' => '2017-03-27 14:32:29',
                'updated_at' => '2017-03-27 14:32:29',
                'couplage_id' => 14,
                'developpement_id' => 96,
                'lecon_id' => 822,
            ),
            51 => 
            array (
                'id' => 58,
                'created_at' => '2017-03-27 14:32:29',
                'updated_at' => '2017-03-27 14:32:29',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 822,
            ),
            52 => 
            array (
                'id' => 59,
                'created_at' => '2017-03-27 14:33:14',
                'updated_at' => '2017-03-27 14:33:14',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 823,
            ),
            53 => 
            array (
                'id' => 60,
                'created_at' => '2017-03-27 14:34:16',
                'updated_at' => '2017-03-27 14:34:16',
                'couplage_id' => 14,
                'developpement_id' => 162,
                'lecon_id' => 826,
            ),
            54 => 
            array (
                'id' => 61,
                'created_at' => '2017-03-27 14:34:19',
                'updated_at' => '2017-03-27 14:34:19',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 826,
            ),
            55 => 
            array (
                'id' => 62,
                'created_at' => '2017-03-27 14:39:22',
                'updated_at' => '2017-03-27 14:39:22',
                'couplage_id' => 14,
                'developpement_id' => 107,
                'lecon_id' => 823,
            ),
            56 => 
            array (
                'id' => 63,
                'created_at' => '2017-03-27 14:39:52',
                'updated_at' => '2017-03-27 14:39:52',
                'couplage_id' => 14,
                'developpement_id' => 120,
                'lecon_id' => 824,
            ),
            57 => 
            array (
                'id' => 64,
                'created_at' => '2017-03-27 14:39:53',
                'updated_at' => '2017-03-27 14:39:53',
                'couplage_id' => 14,
                'developpement_id' => 35,
                'lecon_id' => 824,
            ),
            58 => 
            array (
                'id' => 65,
                'created_at' => '2017-03-27 14:40:00',
                'updated_at' => '2017-03-27 14:40:00',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 825,
            ),
            59 => 
            array (
                'id' => 66,
                'created_at' => '2017-03-27 14:40:17',
                'updated_at' => '2017-03-27 14:40:17',
                'couplage_id' => 14,
                'developpement_id' => 107,
                'lecon_id' => 828,
            ),
            60 => 
            array (
                'id' => 67,
                'created_at' => '2017-03-27 14:40:52',
                'updated_at' => '2017-03-27 14:40:52',
                'couplage_id' => 14,
                'developpement_id' => 168,
                'lecon_id' => 829,
            ),
            61 => 
            array (
                'id' => 68,
                'created_at' => '2017-03-27 14:41:26',
                'updated_at' => '2017-03-27 14:41:26',
                'couplage_id' => 14,
                'developpement_id' => 168,
                'lecon_id' => 828,
            ),
            62 => 
            array (
                'id' => 69,
                'created_at' => '2017-03-27 14:41:51',
                'updated_at' => '2017-03-27 14:41:51',
                'couplage_id' => 14,
                'developpement_id' => 107,
                'lecon_id' => 829,
            ),
            63 => 
            array (
                'id' => 70,
                'created_at' => '2017-03-27 14:41:59',
                'updated_at' => '2017-03-27 14:41:59',
                'couplage_id' => 14,
                'developpement_id' => 104,
                'lecon_id' => 830,
            ),
            64 => 
            array (
                'id' => 71,
                'created_at' => '2017-03-27 14:42:05',
                'updated_at' => '2017-03-27 14:42:05',
                'couplage_id' => 14,
                'developpement_id' => 107,
                'lecon_id' => 830,
            ),
            65 => 
            array (
                'id' => 72,
                'created_at' => '2017-03-27 14:42:13',
                'updated_at' => '2017-03-27 14:42:13',
                'couplage_id' => 14,
                'developpement_id' => 127,
                'lecon_id' => 831,
            ),
            66 => 
            array (
                'id' => 73,
                'created_at' => '2017-03-27 14:42:21',
                'updated_at' => '2017-03-27 14:42:21',
                'couplage_id' => 14,
                'developpement_id' => 107,
                'lecon_id' => 831,
            ),
            67 => 
            array (
                'id' => 74,
                'created_at' => '2017-03-27 14:42:39',
                'updated_at' => '2017-03-27 14:42:39',
                'couplage_id' => 14,
                'developpement_id' => 120,
                'lecon_id' => 832,
            ),
            68 => 
            array (
                'id' => 75,
                'created_at' => '2017-03-27 14:43:06',
                'updated_at' => '2017-03-27 14:43:06',
                'couplage_id' => 14,
                'developpement_id' => 35,
                'lecon_id' => 832,
            ),
            69 => 
            array (
                'id' => 76,
                'created_at' => '2017-03-27 14:43:10',
                'updated_at' => '2017-03-27 14:43:10',
                'couplage_id' => 14,
                'developpement_id' => 38,
                'lecon_id' => 833,
            ),
            70 => 
            array (
                'id' => 77,
                'created_at' => '2017-03-27 14:43:14',
                'updated_at' => '2017-03-27 14:43:14',
                'couplage_id' => 14,
                'developpement_id' => 168,
                'lecon_id' => 833,
            ),
            71 => 
            array (
                'id' => 78,
                'created_at' => '2017-03-27 14:43:49',
                'updated_at' => '2017-03-27 14:43:49',
                'couplage_id' => 14,
                'developpement_id' => 104,
                'lecon_id' => 834,
            ),
            72 => 
            array (
                'id' => 79,
                'created_at' => '2017-03-27 14:44:13',
                'updated_at' => '2017-03-27 14:44:13',
                'couplage_id' => 14,
                'developpement_id' => 35,
                'lecon_id' => 834,
            ),
            73 => 
            array (
                'id' => 80,
                'created_at' => '2017-03-27 14:45:05',
                'updated_at' => '2017-03-27 14:45:05',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 837,
            ),
            74 => 
            array (
                'id' => 81,
                'created_at' => '2017-03-27 14:45:15',
                'updated_at' => '2017-03-27 14:45:15',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 838,
            ),
            75 => 
            array (
                'id' => 82,
                'created_at' => '2017-03-27 14:46:01',
                'updated_at' => '2017-03-27 14:46:01',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 840,
            ),
            76 => 
            array (
                'id' => 83,
                'created_at' => '2017-03-27 14:46:18',
                'updated_at' => '2017-03-27 14:46:18',
                'couplage_id' => 14,
                'developpement_id' => 51,
                'lecon_id' => 842,
            ),
            77 => 
            array (
                'id' => 84,
                'created_at' => '2017-03-27 14:46:38',
                'updated_at' => '2017-03-27 14:46:38',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 843,
            ),
            78 => 
            array (
                'id' => 85,
                'created_at' => '2017-03-28 07:42:42',
                'updated_at' => '2017-03-28 07:42:42',
                'couplage_id' => 2,
                'developpement_id' => 3,
                'lecon_id' => 666,
            ),
            79 => 
            array (
                'id' => 86,
                'created_at' => '2017-03-28 09:02:10',
                'updated_at' => '2017-03-28 09:02:10',
                'couplage_id' => 14,
                'developpement_id' => 153,
                'lecon_id' => 784,
            ),
            80 => 
            array (
                'id' => 87,
                'created_at' => '2017-03-28 09:02:13',
                'updated_at' => '2017-03-28 09:02:13',
                'couplage_id' => 14,
                'developpement_id' => 153,
                'lecon_id' => 785,
            ),
            81 => 
            array (
                'id' => 88,
                'created_at' => '2017-03-28 14:25:03',
                'updated_at' => '2017-03-28 14:25:03',
                'couplage_id' => 14,
                'developpement_id' => 173,
                'lecon_id' => 787,
            ),
            82 => 
            array (
                'id' => 89,
                'created_at' => '2017-03-28 14:25:03',
                'updated_at' => '2017-03-28 14:25:03',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 787,
            ),
            83 => 
            array (
                'id' => 92,
                'created_at' => '2017-03-28 14:25:20',
                'updated_at' => '2017-03-28 14:25:20',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 790,
            ),
            84 => 
            array (
                'id' => 91,
                'created_at' => '2017-03-28 14:25:13',
                'updated_at' => '2017-03-28 14:25:13',
                'couplage_id' => 14,
                'developpement_id' => 173,
                'lecon_id' => 790,
            ),
            85 => 
            array (
                'id' => 93,
                'created_at' => '2017-03-28 14:25:35',
                'updated_at' => '2017-03-28 14:25:35',
                'couplage_id' => 14,
                'developpement_id' => 26,
                'lecon_id' => 794,
            ),
            86 => 
            array (
                'id' => 94,
                'created_at' => '2017-03-28 14:25:52',
                'updated_at' => '2017-03-28 14:25:52',
                'couplage_id' => 14,
                'developpement_id' => 56,
                'lecon_id' => 794,
            ),
            87 => 
            array (
                'id' => 95,
                'created_at' => '2017-03-28 14:26:10',
                'updated_at' => '2017-03-28 14:26:10',
                'couplage_id' => 14,
                'developpement_id' => 38,
                'lecon_id' => 795,
            ),
            88 => 
            array (
                'id' => 96,
                'created_at' => '2017-03-28 14:26:34',
                'updated_at' => '2017-03-28 14:26:34',
                'couplage_id' => 14,
                'developpement_id' => 1,
                'lecon_id' => 796,
            ),
            89 => 
            array (
                'id' => 97,
                'created_at' => '2017-03-28 14:26:45',
                'updated_at' => '2017-03-28 14:26:45',
                'couplage_id' => 14,
                'developpement_id' => 56,
                'lecon_id' => 800,
            ),
            90 => 
            array (
                'id' => 98,
                'created_at' => '2017-03-28 14:26:45',
                'updated_at' => '2017-03-28 14:26:45',
                'couplage_id' => 14,
                'developpement_id' => 1,
                'lecon_id' => 800,
            ),
            91 => 
            array (
                'id' => 99,
                'created_at' => '2017-03-28 14:27:10',
                'updated_at' => '2017-03-28 14:27:10',
                'couplage_id' => 14,
                'developpement_id' => 162,
                'lecon_id' => 802,
            ),
            92 => 
            array (
                'id' => 100,
                'created_at' => '2017-03-28 14:27:35',
                'updated_at' => '2017-03-28 14:27:35',
                'couplage_id' => 14,
                'developpement_id' => 104,
                'lecon_id' => 806,
            ),
            93 => 
            array (
                'id' => 101,
                'created_at' => '2017-03-28 14:28:40',
                'updated_at' => '2017-03-28 14:28:40',
                'couplage_id' => 14,
                'developpement_id' => 128,
                'lecon_id' => 811,
            ),
            94 => 
            array (
                'id' => 102,
                'created_at' => '2017-03-28 14:31:01',
                'updated_at' => '2017-03-28 14:31:01',
                'couplage_id' => 14,
                'developpement_id' => 106,
                'lecon_id' => 811,
            ),
            95 => 
            array (
                'id' => 104,
                'created_at' => '2017-03-28 14:32:06',
                'updated_at' => '2017-03-28 14:32:06',
                'couplage_id' => 14,
                'developpement_id' => 144,
                'lecon_id' => 814,
            ),
            96 => 
            array (
                'id' => 105,
                'created_at' => '2017-03-28 14:32:43',
                'updated_at' => '2017-03-28 14:32:43',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 821,
            ),
            97 => 
            array (
                'id' => 106,
                'created_at' => '2017-03-28 14:32:58',
                'updated_at' => '2017-03-28 14:32:58',
                'couplage_id' => 14,
                'developpement_id' => 96,
                'lecon_id' => 821,
            ),
            98 => 
            array (
                'id' => 107,
                'created_at' => '2017-03-28 14:33:05',
                'updated_at' => '2017-03-28 14:33:05',
                'couplage_id' => 14,
                'developpement_id' => 96,
                'lecon_id' => 822,
            ),
            99 => 
            array (
                'id' => 108,
                'created_at' => '2017-03-28 14:33:05',
                'updated_at' => '2017-03-28 14:33:05',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 822,
            ),
            100 => 
            array (
                'id' => 109,
                'created_at' => '2017-03-28 14:33:10',
                'updated_at' => '2017-03-28 14:33:10',
                'couplage_id' => 14,
                'developpement_id' => 8,
                'lecon_id' => 823,
            ),
            101 => 
            array (
                'id' => 110,
                'created_at' => '2017-03-28 14:33:33',
                'updated_at' => '2017-03-28 14:33:33',
                'couplage_id' => 14,
                'developpement_id' => 144,
                'lecon_id' => 825,
            ),
            102 => 
            array (
                'id' => 111,
                'created_at' => '2017-03-28 14:33:47',
                'updated_at' => '2017-03-28 14:33:47',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 825,
            ),
            103 => 
            array (
                'id' => 112,
                'created_at' => '2017-03-28 14:33:50',
                'updated_at' => '2017-03-28 14:33:50',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 826,
            ),
            104 => 
            array (
                'id' => 113,
                'created_at' => '2017-03-28 14:34:49',
                'updated_at' => '2017-03-28 14:34:49',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 837,
            ),
            105 => 
            array (
                'id' => 114,
                'created_at' => '2017-03-28 14:34:53',
                'updated_at' => '2017-03-28 14:34:53',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 838,
            ),
            106 => 
            array (
                'id' => 115,
                'created_at' => '2017-03-28 14:35:20',
                'updated_at' => '2017-03-28 14:35:20',
                'couplage_id' => 14,
                'developpement_id' => 128,
                'lecon_id' => 840,
            ),
            107 => 
            array (
                'id' => 116,
                'created_at' => '2017-03-28 14:35:21',
                'updated_at' => '2017-03-28 14:35:21',
                'couplage_id' => 14,
                'developpement_id' => 167,
                'lecon_id' => 840,
            ),
            108 => 
            array (
                'id' => 117,
                'created_at' => '2017-03-28 14:35:43',
                'updated_at' => '2017-03-28 14:35:43',
                'couplage_id' => 14,
                'developpement_id' => 65,
                'lecon_id' => 843,
            ),
            109 => 
            array (
                'id' => 119,
                'created_at' => '2017-03-28 14:36:19',
                'updated_at' => '2017-03-28 14:36:19',
                'couplage_id' => 14,
                'developpement_id' => 223,
                'lecon_id' => 845,
            ),
            110 => 
            array (
                'id' => 120,
                'created_at' => '2017-03-28 14:36:40',
                'updated_at' => '2017-03-28 14:36:40',
                'couplage_id' => 14,
                'developpement_id' => 83,
                'lecon_id' => 849,
            ),
            111 => 
            array (
                'id' => 121,
                'created_at' => '2017-03-28 14:36:48',
                'updated_at' => '2017-03-28 14:36:48',
                'couplage_id' => 14,
                'developpement_id' => 83,
                'lecon_id' => 845,
            ),
        ));
        
        
    }
}
