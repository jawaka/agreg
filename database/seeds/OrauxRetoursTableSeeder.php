<?php

use Illuminate\Database\Seeder;

class OrauxRetoursTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('oraux_retours')->delete();
        
        \DB::table('oraux_retours')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-06-08 16:25:43',
                'updated_at' => '2016-06-17 08:55:35',
                'oraux_type_id' => 3,
                'user_id' => 111,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-06-12 10:03:23',
                'updated_at' => '2016-06-12 10:09:16',
                'oraux_type_id' => 5,
                'user_id' => 105,
                'annee' => 2015,
                'anonyme' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-06-12 10:10:25',
                'updated_at' => '2016-06-12 10:57:39',
                'oraux_type_id' => 5,
                'user_id' => 105,
                'annee' => 2015,
                'anonyme' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-06-12 10:58:25',
                'updated_at' => '2016-06-14 19:49:32',
                'oraux_type_id' => 5,
                'user_id' => 105,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-06-12 11:08:10',
                'updated_at' => '2016-06-17 08:27:20',
                'oraux_type_id' => 5,
                'user_id' => 105,
                'annee' => 2015,
                'anonyme' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2016-06-17 08:30:46',
                'updated_at' => '2016-06-17 14:34:35',
                'oraux_type_id' => 6,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2016-06-17 08:33:15',
                'updated_at' => '2016-06-17 08:33:15',
                'oraux_type_id' => 6,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-06-17 08:34:51',
                'updated_at' => '2016-06-17 08:34:51',
                'oraux_type_id' => 6,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2016-06-17 08:36:29',
                'updated_at' => '2016-06-17 08:36:29',
                'oraux_type_id' => 7,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2016-06-17 08:37:41',
                'updated_at' => '2016-06-17 08:37:41',
                'oraux_type_id' => 7,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2016-06-17 08:38:27',
                'updated_at' => '2016-06-17 08:38:27',
                'oraux_type_id' => 7,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-06-17 08:40:00',
                'updated_at' => '2016-06-17 08:40:00',
                'oraux_type_id' => 7,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2016-06-17 08:40:53',
                'updated_at' => '2016-06-17 08:40:53',
                'oraux_type_id' => 8,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-06-17 08:50:23',
                'updated_at' => '2016-06-17 08:50:23',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2016-06-17 08:51:29',
                'updated_at' => '2016-06-17 08:51:29',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-06-17 08:56:09',
                'updated_at' => '2016-06-17 08:56:46',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-06-17 08:57:50',
                'updated_at' => '2016-06-17 08:57:50',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-06-17 08:58:57',
                'updated_at' => '2016-06-17 08:58:57',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-06-17 09:00:03',
                'updated_at' => '2016-06-17 09:00:03',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2016-06-17 09:01:07',
                'updated_at' => '2016-06-17 09:01:07',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2016-06-17 09:02:20',
                'updated_at' => '2016-06-17 09:02:20',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2016-06-17 09:03:12',
                'updated_at' => '2016-06-17 09:03:12',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2016-06-17 09:04:27',
                'updated_at' => '2016-06-17 09:04:27',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2016-06-17 09:05:46',
                'updated_at' => '2016-06-17 09:05:46',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2016-06-17 09:06:53',
                'updated_at' => '2016-06-17 09:06:53',
                'oraux_type_id' => 3,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2016-06-17 09:07:48',
                'updated_at' => '2016-06-17 09:07:48',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            26 => 
            array (
                'id' => 27,
                'created_at' => '2016-06-17 09:08:41',
                'updated_at' => '2016-06-17 09:08:41',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            27 => 
            array (
                'id' => 28,
                'created_at' => '2016-06-17 09:10:00',
                'updated_at' => '2016-06-17 09:10:00',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            28 => 
            array (
                'id' => 29,
                'created_at' => '2016-06-17 09:11:04',
                'updated_at' => '2016-06-17 09:11:04',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            29 => 
            array (
                'id' => 30,
                'created_at' => '2016-06-17 10:07:54',
                'updated_at' => '2016-06-17 10:07:54',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            30 => 
            array (
                'id' => 31,
                'created_at' => '2016-06-17 10:34:41',
                'updated_at' => '2016-06-17 10:34:41',
                'oraux_type_id' => 3,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            31 => 
            array (
                'id' => 32,
                'created_at' => '2016-06-17 10:35:53',
                'updated_at' => '2016-06-17 10:35:53',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            32 => 
            array (
                'id' => 33,
                'created_at' => '2016-06-17 10:36:54',
                'updated_at' => '2016-06-17 10:36:54',
                'oraux_type_id' => 4,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            33 => 
            array (
                'id' => 34,
                'created_at' => '2016-06-17 10:37:54',
                'updated_at' => '2016-06-17 10:37:54',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            34 => 
            array (
                'id' => 35,
                'created_at' => '2016-06-17 10:39:03',
                'updated_at' => '2016-06-17 10:39:03',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            35 => 
            array (
                'id' => 36,
                'created_at' => '2016-06-17 10:40:15',
                'updated_at' => '2016-06-17 10:40:27',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            36 => 
            array (
                'id' => 37,
                'created_at' => '2016-06-17 10:41:39',
                'updated_at' => '2016-06-17 10:41:39',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            37 => 
            array (
                'id' => 38,
                'created_at' => '2016-06-17 10:42:54',
                'updated_at' => '2016-06-17 10:42:54',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            38 => 
            array (
                'id' => 39,
                'created_at' => '2016-06-17 10:44:18',
                'updated_at' => '2016-06-17 10:44:18',
                'oraux_type_id' => 4,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            39 => 
            array (
                'id' => 40,
                'created_at' => '2016-06-17 10:45:48',
                'updated_at' => '2016-06-17 10:45:48',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            40 => 
            array (
                'id' => 41,
                'created_at' => '2016-06-17 10:46:45',
                'updated_at' => '2016-06-17 10:46:45',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            41 => 
            array (
                'id' => 42,
                'created_at' => '2016-06-17 10:47:51',
                'updated_at' => '2016-06-17 10:47:51',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            42 => 
            array (
                'id' => 43,
                'created_at' => '2016-06-17 10:48:18',
                'updated_at' => '2016-06-17 10:48:18',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            43 => 
            array (
                'id' => 44,
                'created_at' => '2016-06-17 10:49:28',
                'updated_at' => '2016-06-17 10:49:28',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            44 => 
            array (
                'id' => 45,
                'created_at' => '2016-06-17 10:50:25',
                'updated_at' => '2016-06-17 10:50:25',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            45 => 
            array (
                'id' => 46,
                'created_at' => '2016-06-17 10:50:59',
                'updated_at' => '2016-06-17 10:51:40',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            46 => 
            array (
                'id' => 47,
                'created_at' => '2016-06-17 10:52:47',
                'updated_at' => '2016-06-17 10:52:47',
                'oraux_type_id' => 1,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            47 => 
            array (
                'id' => 48,
                'created_at' => '2016-06-17 10:53:43',
                'updated_at' => '2016-06-17 10:53:43',
                'oraux_type_id' => 2,
                'user_id' => 1,
                'annee' => 2015,
                'anonyme' => 0,
            ),
            48 => 
            array (
                'id' => 51,
                'created_at' => '2016-07-01 11:58:10',
                'updated_at' => '2016-07-05 19:06:36',
                'oraux_type_id' => 5,
                'user_id' => 117,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            49 => 
            array (
                'id' => 52,
                'created_at' => '2016-07-01 12:19:53',
                'updated_at' => '2016-07-05 19:07:20',
                'oraux_type_id' => 2,
                'user_id' => 117,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            50 => 
            array (
                'id' => 53,
                'created_at' => '2016-07-01 12:40:43',
                'updated_at' => '2016-07-05 19:07:41',
                'oraux_type_id' => 1,
                'user_id' => 117,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            51 => 
            array (
                'id' => 54,
                'created_at' => '2016-07-01 22:58:04',
                'updated_at' => '2016-07-01 22:58:04',
                'oraux_type_id' => 2,
                'user_id' => 123,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            52 => 
            array (
                'id' => 55,
                'created_at' => '2016-07-01 23:13:40',
                'updated_at' => '2016-07-01 23:13:40',
                'oraux_type_id' => 7,
                'user_id' => 123,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            53 => 
            array (
                'id' => 56,
                'created_at' => '2016-07-02 10:01:17',
                'updated_at' => '2016-07-05 22:48:18',
                'oraux_type_id' => 5,
                'user_id' => 124,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            54 => 
            array (
                'id' => 57,
                'created_at' => '2016-07-02 10:20:18',
                'updated_at' => '2016-07-05 22:48:51',
                'oraux_type_id' => 1,
                'user_id' => 124,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            55 => 
            array (
                'id' => 58,
                'created_at' => '2016-07-02 10:38:13',
                'updated_at' => '2016-07-05 22:49:51',
                'oraux_type_id' => 2,
                'user_id' => 124,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            56 => 
            array (
                'id' => 59,
                'created_at' => '2016-07-02 15:41:19',
                'updated_at' => '2016-07-02 15:41:19',
                'oraux_type_id' => 7,
                'user_id' => 125,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            57 => 
            array (
                'id' => 60,
                'created_at' => '2016-07-02 15:51:58',
                'updated_at' => '2016-07-02 15:57:00',
                'oraux_type_id' => 1,
                'user_id' => 125,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            58 => 
            array (
                'id' => 61,
                'created_at' => '2016-07-03 17:02:10',
                'updated_at' => '2016-07-03 17:08:17',
                'oraux_type_id' => 2,
                'user_id' => 125,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            59 => 
            array (
                'id' => 62,
                'created_at' => '2016-07-04 13:01:06',
                'updated_at' => '2016-07-05 09:28:58',
                'oraux_type_id' => 6,
                'user_id' => 126,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            60 => 
            array (
                'id' => 63,
                'created_at' => '2016-07-04 13:04:50',
                'updated_at' => '2016-07-05 09:30:31',
                'oraux_type_id' => 2,
                'user_id' => 126,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            61 => 
            array (
                'id' => 64,
                'created_at' => '2016-07-04 13:11:49',
                'updated_at' => '2016-07-05 09:29:39',
                'oraux_type_id' => 1,
                'user_id' => 126,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            62 => 
            array (
                'id' => 65,
                'created_at' => '2016-07-04 18:14:52',
                'updated_at' => '2016-07-05 12:23:28',
                'oraux_type_id' => 3,
                'user_id' => 120,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            63 => 
            array (
                'id' => 66,
                'created_at' => '2016-07-04 18:23:11',
                'updated_at' => '2016-07-05 12:24:45',
                'oraux_type_id' => 4,
                'user_id' => 120,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            64 => 
            array (
                'id' => 67,
                'created_at' => '2016-07-04 18:35:22',
                'updated_at' => '2016-07-05 12:34:04',
                'oraux_type_id' => 8,
                'user_id' => 120,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            65 => 
            array (
                'id' => 68,
                'created_at' => '2016-07-05 12:08:49',
                'updated_at' => '2016-07-05 12:09:54',
                'oraux_type_id' => 1,
                'user_id' => 128,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            66 => 
            array (
                'id' => 69,
                'created_at' => '2016-07-05 12:19:26',
                'updated_at' => '2016-07-05 12:22:42',
                'oraux_type_id' => 2,
                'user_id' => 128,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            67 => 
            array (
                'id' => 70,
                'created_at' => '2016-07-05 12:35:49',
                'updated_at' => '2016-07-05 12:38:51',
                'oraux_type_id' => 5,
                'user_id' => 128,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            68 => 
            array (
                'id' => 71,
                'created_at' => '2016-07-13 09:11:42',
                'updated_at' => '2016-07-13 09:11:42',
                'oraux_type_id' => 1,
                'user_id' => 130,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            69 => 
            array (
                'id' => 72,
                'created_at' => '2016-07-13 09:12:56',
                'updated_at' => '2016-07-13 09:12:56',
                'oraux_type_id' => 1,
                'user_id' => 130,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            70 => 
            array (
                'id' => 73,
                'created_at' => '2016-07-13 09:21:26',
                'updated_at' => '2016-07-13 09:21:26',
                'oraux_type_id' => 2,
                'user_id' => 130,
                'annee' => 2016,
                'anonyme' => 0,
            ),
            71 => 
            array (
                'id' => 74,
                'created_at' => '2016-07-13 09:32:46',
                'updated_at' => '2016-07-13 09:32:46',
                'oraux_type_id' => 5,
                'user_id' => 130,
                'annee' => 2016,
                'anonyme' => 0,
            ),
        ));
        
        
    }
}
