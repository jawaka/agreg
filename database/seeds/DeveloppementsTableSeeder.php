<?php

use Illuminate\Database\Seeder;

class DeveloppementsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('developpements')->delete();
        
        \DB::table('developpements')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 11:37:11',
                'nom' => 'Décomposition de Dunford',
                'details' => 'Tout endomorphisme $f$ s\'écrit, sous réserve que $\\chi_f$ soit scindé dans $K$, sous la forme $f = d + n$ où $d$ est un endomorphisme diagonalisable et $n$ un endomorphisme nilpotent.',
                'user_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-07 07:31:21',
                'nom' => 'Surjectivité de l\'exponentielle matricielle',
            'details' => 'L\'application $\\exp : M_n(\\mathbb{C}) \\to GL_n(\\mathbb{C})$ est surjective.',
                'user_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 15:23:52',
                'nom' => 'Théorème de Von Neumann des sous-variétés',
            'details' => 'Soit $K = \\mathbb{R}$ ou $\\mathbb{C}$ et $G$ un sous-groupe fermé de $GL_n(K)$. Alors $G$ est une sous-variété de $M_n(K)$.',
                'user_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-07 07:39:01',
                'nom' => 'Équation de la chaleur sur le cercle',
                'details' => 'On résoud l\'EDP suivante :

$$ \\partial_t u - \\Delta_x u = 0 $$

sur le cercle $\\mathbb{U} = \\mathbb{R}/2\\pi\\mathbb{Z}$.',
                'user_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-07 07:56:18',
            'nom' => 'Loi de réciprocité quadratique (via les formes quadratiques)',
                'details' => 'La loi de réciprocité quadratique stipule que 

$$ \\left( \\frac{p}{q} \\right) \\left( \\frac{q}{p} \\right) = (-1)^{ \\frac{p-1}{2} \\frac{q-1}{2} } $$

où $p$ et $q$ sont deux nombres premiers impairs.

Ce développement propose une démonstration utilisant classification des formes quadratiques sur un corps finis.',
                'user_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 15:21:19',
            'nom' => 'Loi de réciprocité quadratique (par le résultant)',
                'details' => 'La loi de réciprocité quadratique stipule que 

$$ \\left( \\frac{p}{q} \\right) \\left( \\frac{q}{p} \\right) = (-1)^{ \\frac{p-1}{2} \\frac{q-1}{2} } $$

où $p$ et $q$ sont deux nombres premiers impairs.

Ce développement propose une démonstration utilisant les propriétés du résultant.',
                'user_id' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 15:33:50',
            'nom' => 'Théorème des deux carrés de Fermat (par les entiers de Gauss)',
                'details' => 'Un entier est somme de deux carrés si et seulement si la $p$-valuation de chacun des facteurs premiers $p$ congru à $3$ modulo $4$ est paire.


Cela revient à donner une condition nécessaire sur l\'existence d\'une solution à l\'équation diophantienne 

$$ n = x^2+ y^2$$',
                'user_id' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 21:03:18',
                'nom' => 'Lemme de Morse',
            'details' => 'Soit $f : U \\to \\mathbb{R}$ où $U$ est un ouvert de $\\mathbb{R}^n$ de classe $C^3$ telle que $0 \\in U$, $df_0 = 0$, $d^2f_0$ soit non-dégénérée. On note $(p,n-p)$ la signature de $d^2f_0$. Alors à un $C^1$-difféomorphisme près on a

$$ f(x) = f(0)+ x_1^2 + \\cdots + x_p^2 - x_{p+1}^2 - \\cdots - x_{n}^2 $$',
                'user_id' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 21:07:41',
                'nom' => 'Simplicité du groupe alterné',
                'details' => 'Le groupe alterné $\\mathfrak{A}_n$ est simple si $n \\ge 5$.',
                'user_id' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 21:11:58',
            'nom' => 'Simplicité du groupe alterné (par les commutateurs)',
                'details' => 'Le groupe alterné $\\mathfrak{A}_n$ est simple si $n\\ge 5$.',
                'user_id' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 21:15:08',
                'nom' => 'Décomposition polaire ',
                'details' => 'L\'application suivante est un homéomorphisme 

$$ \\begin{array}{ccc}
O_n(\\mathbb{R}) \\times S_n^{++}(\\mathbb{R}) & \\to & GL_n(\\mathbb{R}) \\\\
(O,S) & \\longmapsto & OS 
\\end{array}$$',
                'user_id' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:05:54',
            'nom' => 'L\'exponentielle induit un homéomorphisme entre Sn(R) et Sn++(R)',
                'details' => 'L\'application suivante est un homéomorphisme :

$$\\begin{array}{ccc}
S_n(\\mathbb{R}) & \\to & S_n^{++}(\\mathbb{R}) \\\\
S & \\longmapsto & \\exp(S) 
\\end{array}$$',
                'user_id' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:06:40',
            'nom' => 'C[X,Y]/(X^2+Y^2-1) est principal',
            'details' => 'L\'anneau $C[X,Y]/(X^2+Y^2-1)$ est principal.',
                'user_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-08 21:47:36',
                'nom' => 'Théorème de Chevalley-Warning',
                'details' => 'Ce théorème porte sur le nombre de racines communes d\'un nombre fini de polynômes sur $\\mathbb{F}_q^n$.

À la limite on peut voir ce problème comme une équation diophantienne si on se restreint au cas de $\\mathbb{Z}/p\\mathbb{Z}$. En effet résoudre $P(x) = 0 [p]$ revient à résoudre l\'équation diophantienne $ P(x) = pk$ avec les variables $x$ et $k$.',
                'user_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-12 15:24:39',
                'nom' => 'Le groupe modulaire',
            'details' => 'Le groupe modulaire est le groupe $PSL_2(\\mathbb{Z})$. On montre que ce groupe est engendré par 

$$ PSL_2(\\mathbb{Z}) = \\left \\langle \\overline{ \\begin{pmatrix} 0 & -1 \\\\ 1 & 0 \\end{pmatrix}} , \\overline{\\begin{pmatrix} 1 & 1 \\\\ 0 & 1 \\end{pmatrix}} \\right\\rangle $$

où $\\overline{M}$ est la projection canonique d\'une matrice $M \\in SL_2(\\mathbb{Z})$ dans $PSL_2(\\mathbb{Z})$.',
                'user_id' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-12 15:33:10',
                'nom' => 'Endomorphismes semi-simples',
            'details' => 'On dit que $u \\in L(E)$ est semi-simple si tout sous-espace $F$ stable par $u$ admet un supplémentaire stable par $u$.

On montre que $u$ est semi-simple si et seulement le polynôme minimal de $u$ est sans facteurs carrés.

Ce développement se recase dans les leçons sur les extensions pour la version de Tom.',
                'user_id' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-13 15:57:42',
            'nom' => 'Théorème de Gauss (polygones constructibles)',
                'details' => 'Un polygône régulier à $n$ côtés est constructible à la règle et au compas si et seulement si $$ n = 2^a p_1 \\cdots p_r $$
où $a \\ge 1$ et les $p_i$ sont des nombres premiers de Fermat ($= 2^n +1$) distincts.

Pour le recasage dans la leçon sur les anneaux anneaux principaux c\'est parce qu\'il y a des histoires de polynômes minimaux ...',
                'user_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-13 16:11:17',
                'nom' => 'Théorème taubérien fort',
            'details' => 'Soit $(a_n)$ une suite réelle telle que $a_n = o(1/n)$. On suppose que la série entière $\\sum_{n \\ge 0} a_n x^n$ soit de rayon de convergence $1$ et que sa somme $F$ vérifie $F(x) \\to 0$ lorsque $x \\to 1^-$. Alors la série $\\sum_{n \\ge 0} a_n$ est convergente. et vaut $0$.

Il s\'agit d\'une sorte de réciproque au théorème d\'Abel angulaire.',
                'user_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-13 16:48:45',
                'nom' => 'Théorème du relèvement',
                'details' => 'Soient $\\mathbb{U} = \\{ z \\in \\mathbb{C} : |z| = 1 \\}$, $n \\ge 1$ et $u : \\mathbb{R}^n \\to \\mathbb{U}$ de classe $C^k$ avec $k \\ge 2$. Alors il existe une fonction $t : \\mathbb{R}^n \\to \\mathbb{R}$ de classe $C^k$ telle que pour tout $x \\in \\mathbb{R}$ on ait 

$$ u(x) = e^{it(x)} $$',
                'user_id' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-13 16:55:53',
            'nom' => 'Processus de Galton-Watson (ou processus de branchement)',
            'details' => 'Soit $(X^n_i)_{ i \\ge 1, n \\ge 1}$ une famille de variables aléatoires iid à valeurs dans $\\mathbb{N}$. On définit $Z_0=1$ et $Z_{n+1} = \\sum_{i=1}^{Z_n} X^{n+1}_i$ pour tout $n \\ge 0$. On pose $m = E[X^1_1]$.

On suppose que $P(Z_1 = 1) \\not=1$, alors

$$ \\begin{cases}
P(Z_n =0) \\to 1 \\text{ lorsque } n \\to +\\infty \\text{ si } m \\le 1 \\\\
\\exists c > 0 : P(Z_n > 0) \\ge c, \\forall  n \\ge 0 \\text{ sinon }
\\end{cases}$$

',
                'user_id' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-15 10:35:14',
                'nom' => 'Théorème des lacunes d\'Hadamard',
            'details' => 'Soit $(\\lambda_n)_{ n \\ge 0}$ une suite croissante d\'entiers telle que pour tout $n \\ge 0$ on ait $\\lambda_{n+1}/\\lambda_n \\ge \\alpha > 1$. 

Soit $f(z) = \\sum_{n \\ge 0} a_n z^{\\lambda_n}$ une série entière de rayon de convergence égal à $1$ qu\'on dit lacunaire.

Alors tout point du cercle de module $1$ est singulier pour $f$. En d\'autres termes pour tout $z$ de module $1$, pour tout ouvert contenant $\\{ z \\in \\mathbb{C} : |z| < 1\\}$ et $z$, il n\'existe pas de prolongement analytique de $f$ à cet ouvert.',
                'user_id' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 13:49:43',
                'nom' => 'Théorème de Pfister',
            'details' => 'Soient $K$ un corps de caractéristique $\\not=2$ et $n \\ge 1$. Soient $\\alpha_1 , \\ldots , \\alpha_n \\in K^*$, $P \\in K[X]$ et  $F_1, \\ldots, F_n \\in K(X)$ telles que $\\sum_{i=1}^n \\alpha_i F_i^2 = P $. Alors il existe $P_1 , \\ldots , P_n \\in K[X]$ telles que 

$$ \\sum_{i=1}^n \\alpha_i P_i^2 = P $$

Pour le recasage ça passe dans les anneaux principaux parce qu\'on utilise le théorème de Bézout.',
                'user_id' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 13:52:11',
                'nom' => 'Théorème d\'Artin',
            'details' => 'Soient $K$ un corps et $G$ un sous-groupe fini des automorphismes de $K$. On note $n$ le cardinal de $G$ et on pose $K^G = \\{ x \\in K : \\forall g \\in G, g(x) = x \\}$. Alors $[K:K^G] = n$ et $\\mathsf{Aut}_{K^G}(K) = G$.',
                'user_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:02:31',
                'nom' => 'Sous-groupes distingués et tables de caractères',
            'details' => 'Soit $G$ un groupe d\'ordre $n$. Soient $\\chi_1, \\ldots , \\chi_r$ les caractères irréductibles de $G$. On note $K_\\chi = \\{ g \\in G : \\chi(g) = \\chi(e)$ où $\\chi$ est un caractère irréductible de $G$ et $e$ l\'élément neutre de $G$.

Soit $H$ un sous-groupe distingué de $G$. Alors il existe $I \\subseteq \\{ 1 , \\ldots , r \\}$ telle que

$$ H =  \\bigcap_{i \\in I} K_{\\chi_i} $$',
                'user_id' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:07:06',
                'nom' => 'Table de caractères de S4 et les isométries du tétraèdre',
                'details' => 'On calcule la table de $\\mathfrak{S}_4$ en trouvant une représentation de dimension $3$ qu\'on interprète comme l\'action de $\\mathfrak{S}_4$ sur les sommets d\'un tétraèdre.',
                'user_id' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:14:40',
                'nom' => 'Algorithme de Berlekamp',
            'details' => 'Le but de cet algorithme est de trouver un facteur non trivial d\'un polynôme $P \\in \\mathbb{F}_q[X]$ où $q=p^n$ est une puissance d\'un nombre premier. Cet algorithme utilise l\'algèbre linéaire pour trouver un polynôme $V \\in \\mathbb{F}_q[X]$ et $a \\in \\mathbb{F}_q$ tels que $\\mathsf{pgcd}( P, V- a) $ soit un facteur trivial de $P$.

Ce développement se recase dans la leçon sur les anneaux principaux car on utilise la principalité de K[X] et ses propriétés arithmétiques.',
                'user_id' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:20:08',
                'nom' => 'Détermination du nombre de racines distinctes d\'un polynôme',
                'details' => 'Soit $P \\in \\mathbb{R}[X]$. On montre dans ce développement que l\'on peut calculer le nombre de racines réelles en fonction de la signature d\'une forme quadratique bien définie.',
                'user_id' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:08:10',
                'nom' => 'Classification des formes quadratiques sur Fq',
            'details' => 'Soit $\\mathbb{F}_q$ un corps fini de caractéristique différente de $2$. Soit $E$ un $\\mathbb{F}_q$-espace vectoriel de dimension $n$. Soit $a \\in \\mathbb{F}_q^*$ qui ne soit pas un carré. Il y a alors deux classes d\'équivalence de formes quadratiques non dégénérées sur $E$. Leur matrice dans une base adaptée est $I_n$ ou $\\mathsf{diag} ( 1 ,\\ldots , 1 , a)$. ',
                'user_id' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:42:40',
                'nom' => 'Menelaüs et Ceva',
            'details' => 'Soit $ABC$ un triangle non applati du plan affine $\\mathcal{E}$. Soient trois points $A\', B\'$ et $C\'$ respectivement sur les droites $(BC), (CA)$ et $(AB)$. On pose $\\mathcal{D}_A = (AA\')$, $\\mathcal{D}_B = (BB\')$ et $\\mathcal{D}_C = (CC\')$. 


$\\bullet$ (Menelaüs) Les points $A\', B\'$ et $C\'$ sont alignés si et seulement si

$$ \\frac{\\overline{A\'B}}{\\overline{A\'C}} \\frac{ \\overline{B\'C} }{\\overline{B\'A}} \\frac{ \\overline{C\'A}}{\\overline{C\'B}} = 1 $$

$\\bullet$ (Ceva) Les droites $\\mathcal{D}_A$, $\\mathcal{D}_B$ et $\\mathcal{D}_C$ sont alignées si et seulement si

$$ \\frac{\\overline{A\'B}}{\\overline{A\'C}} \\frac{ \\overline{B\'C} }{\\overline{B\'A}} \\frac{ \\overline{C\'A}}{\\overline{C\'B}} = -1 $$',
                'user_id' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:44:31',
                'nom' => 'Par cinq points passe une conique',
                'details' => 'Par cinq points distincts d\'un plan affine passe une conique. On précise l\'unicité et la dégénérescence de cette conique en fonction des positions de ces points.',
                'user_id' => 1,
            ),
            30 => 
            array (
                'id' => 31,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:53:40',
            'nom' => 'Invariants de similitude (réduction de Frobenius)',
            'details' => 'Soit $u \\in L(E)$ et $E$ un espace vectoriel de dimension finie. La réduction de Frobenius consiste à montrer qu\'il existe des sous-espaces $F_i$ tels que $E = \\bigoplus_{i=1}^r F_i$, les $F_i$ soient stables par $u$, $u_{F_i}$ soit cyclique et $P_i$ soit le polynôme minimal de $u_{F_i}$ de sorte que $P_r | \\cdots | P_1 = \\mu_u$.',
                'user_id' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 14:57:43',
                'nom' => 'Ellipse de Steiner',
            'details' => 'On identifie le plan affine euclidien à $\\mathbb{C}$. Soient $a,b,c \\in \\mathbb{C}$ trois points non alignés. Alors il existe une unique ellipse tangente aux côtés du triangle $abc$ en leurs milieux. De plus les foyers de cette ellipse sont les racines du polynôme dérivée de $(X-a)(X-b)(X-c)$.',
                'user_id' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 17:14:35',
            'nom' => 'SO₃(R) et les quaternions',
                'details' => 'Soit $H$ le corps non commutatif des quaternions. Soit $G$ le groupe des quaternions de module $1$ : $G = \\{ a + bi +cj + dk  \\in H : a^2 + b^2 + c^2 + d^2 \\}$. 

On montre qu\'il existe un isomorphisme entre $G / \\{ 1 , -1 \\}$ et $SO_3(\\mathbb{R})$.',
                'user_id' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:11:04',
            'nom' => 'Kakutani et sous-groupes compacts de GLn(R)',
            'details' => 'Soit $E$ un $\\mathbb{R}$-espace vectoriel normé. Soit $G$ un sous-groupe compact de $GL(E)$. Soit $K$ un compact convexe non vide tel que $\\forall u \\in G, u(K) \\subseteq K$. 

Alors il existe $x \\in K $ tel que $u(x) = x$ pour tout $u \\in G$.',
                'user_id' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 15:14:23',
                'nom' => 'Méthode du gradient conjugué',
                'details' => 'Le but de cette méthode est de trouver une solution à l\'équation $Ax = b$.

On se place dans le cas où $A \\in S_n^{++}(\\mathbb{R})$, $b \\in \\mathbb{R}^n$. Soit $x_0 \\in \\mathbb{R}^n$. On définit $r_0 = b- Ax_0$ et les sous-espaces vectoriels $K_m = \\mathsf{Vect} ( r_0 , \\ldots , A^m r_0 )$. Il existe une unique suite $(x_m)$ définie par récurrence de sorte que $x_m \\in x_0 + K_{m-1}$, $r_m = b - A x_m\\bot K_{m-1}$ et $r_m \\in K_m$ et cette suite stationne vers l\'unique solution de $Ax = b$ après au plus $n$ itérations.',
                'user_id' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 15:20:40',
                'nom' => 'Chemin optique et calcul des variations',
            'details' => 'Soient $A,B \\in \\mathbb{R}^2$ et $E = \\{ u \\in C^1 ( [0,1], \\mathbb{R}^2 ) , u(0) = A, u(1) = B \\}$. Soit $n : \\mathbb{R}^2 \\to \\mathbb{R}_+^*$ de classe $C^2$. On munit $\\mathbb{R}^2$ du produit scalaire usuel.

Si $u \\in E$, on pose $F(u) = \\int_{[0,1]} n(u) ||u\'\'||^2 $ et on suppose qu\'il existe $u_0 \\in E$ tel que $F(x_0) = \\min_E F$. Alors $u_0$ est $C^2$ et vérifie $2 n(u_0) u_0\'\' - ||u_0\'||^2 \\nabla n(y) + 2 \\langle \\nabla n(u_0) , u_0\' \\rangle u_0\' = 0$.',
                'user_id' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:11:47',
                'nom' => 'Points de Lebesgue d\'une fonction L1',
            'details' => 'Soit $f \\in L^1(\\mathbb{R}^k)$. Alors, pour presque tout $x \\in \\mathbb{R}^k$, 

$$ \\lim_{r \\to 0} \\frac{1}{\\lambda( B(x,r))} \\int_{B(x,r)} |f(y) - f(x)| dy = 0 $$',
                'user_id' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 15:28:05',
                'nom' => 'Théorème de Müntz',
            'details' => 'On munite $C( [0,1], \\mathbb{R})$ du produit scalaire usuel. Soit $(a_n)$ une suite stictement croissante de $\\mathbb{R}_+^*$. 

Alors la famille $(x^{a_k})_{k \\ge 0}$ est une base hilbertienne de $C( [0,1], \\mathbb{R})$ si et seulement si la série $\\sum_{k \\ge 0} \\frac{1}{a_k}$ est divergente.',
                'user_id' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:12:31',
            'nom' => 'Simplicité de SOn(R)',
            'details' => 'Le groupe $SO_n(\\mathbb{R})$ est simple pour $n \\ge 3$ impair.',
                'user_id' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:31:16',
            'nom' => 'Théorème de Sylow (par récurrence sur le cardinal)',
                'details' => 'Soit $G$ un groupe fini et $p$ premier tel que $|G| = p^a m$ avec $a$ un entier non nul et $m$ premier à $p$. On note $n_p$ le nombre de $p$-Sylow de $G$. Alors $n_p$ est non nul et divise $m$. De plus $n_p = 1 [p]$ et tous les $p$-Sylow sont conjugés.

Ce développement présente une démonstration de ce théorème à l\'aide du principe de récurrence. Il existe une autre démonstration utilisant des actions de groupes.',
                'user_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:15:27',
            'nom' => 'Enveloppe convexe de On(R)',
            'details' => 'L\'enveloppe convexe de $O_n(\\mathbb{R})$ est $M_n(\\mathbb{R})$ pour la norme $||.||_2$.',
                'user_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:36:07',
                'nom' => 'Optimisation dans un Hilbert',
            'details' => 'Soient $H$ un espace de Hilbert et $J : H \\to \\mathbb{R}$ convexe, continue et coercive (c\'est-à-dire $J(x) \\to +\\infty$ lorsque $||x|| \\to +\\infty$). Alors il existe $a \\in H$ tel que $J(a) = \\inf_H J$.',
                'user_id' => 1,
            ),
            42 => 
            array (
                'id' => 43,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:38:24',
                'nom' => 'Théorème d\'uniformisation de Riemann',
                'details' => 'Tout ouvert simplement connexe de $\\mathbb{C}$ et distinct de $\\mathbb{C}$ est biholomorphe au disque unité ouvert. ',
                'user_id' => 1,
            ),
            43 => 
            array (
                'id' => 44,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:42:49',
                'nom' => 'Formule des compléments',
            'details' => 'Pour tout $z \\in \\mathbb{C}$ tel que $0 < \\mathsf{Re}(z) < 1$, alors 

$$ \\Gamma(z) \\Gamma(1-z) =\\frac{\\pi}{\\sin(\\pi z)} $$

où $\\Gamma(z) = \\int_0^{+\\infty} t^{z-1} e^{-t} dt$ est la fonction $\\Gamma$ d\'Euler définie pour $\\mathsf{Re}(z) > 0$.',
                'user_id' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:46:50',
                'nom' => 'Chemin au dessus d\'une courbe concave',
                'details' => 'Soit $f : [0,1] \\to \\mathbb{R}$ de classe $C^1$ et concave. Soit $\\gamma$ un chemin dans $\\mathbb{R}^2$ de classe $C^1$ situé au dessus du graphe de $f$ et joignant ses extrémités. Alors 

$$ l(\\gamma) \\ge l(\\Gamma_f)$$

où $\\Gamma_f : x \\to (x,f(x))$ pour $x \\in [0,1]$ et $l(\\Gamma)$ est la longueur d\'un arc $\\Gamma$.',
                'user_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-15 10:41:56',
            'nom' => 'Nullstellensatz via le résultant (théorème des zéros de Hilbert)',
            'details' => 'Soient $K$ un corps algébriquement clos et $I$ un idéal de $K[X_1 , \\ldots , X_n]$ vérifiant $V(I) = \\{ x \\in K : P(x) = 0 \\forall P \\in P \\} = \\emptyset$. Alors $I = K[X_1, \\ldots , X_n]$.

Ce développement se recase dans la leçon sur les anneaux principaux car on utilise à plusieurs reprises la principalité de $K[X]$ (pas uniquement dans l\'initialisation de la récurrence).',
                'user_id' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-15 10:45:39',
                'nom' => 'Théorème de Lüroth',
            'details' => 'Soit $K/k$ une extension transcendante pure telle que $\\mathsf{deg tr}_k( K) = 1$. Soit $L$ un sous-corps de $K$ contenant strictement $k$. Alors $L$ est une extension transcendante pure telle que $\\mathsf{degtr}_k (L) = 1$.

Ce théorème a comme conséquence une CNS pour une courbe algébrique plane (irréductible) soit paramétrable par des fractions rationnelles (cf pp 8-9 du tome 1 de Shafarevich).',
                'user_id' => 1,
            ),
            47 => 
            array (
                'id' => 48,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-15 18:49:36',
                'nom' => 'Théorème de Brauer',
            'details' => 'Soient $K$ un corps et $n$ un entier. On note $P_\\sigma$ la matrice associée à la permutation $\\sigma$ de $\\mathfrak{S}_n$. Soient $\\sigma$ et $\\sigma\' \\in \\mathfrak{S}_n$. Alors $\\sigma $ et $\\sigma\'$ sont conjugués si et seulement si $P_\\sigma$ et $P_{\\sigma\'}$ sont semblables dans $M_n(K)$.',
                'user_id' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:16:04',
            'nom' => 'Les automorphismes isométriques des l(p)',
                'details' => 'Soit $p \\in [1 , +\\infty [$ avec $p \\not=2$. Pour toute bijection $\\varphi : \\mathbb{N} \\to \\mathbb{N}$ et pour toute suite $\\epsilon : \\mathbb{N} \\to \\{ 1 , - 1 \\}$ on définit la fonction $T_{\\varphi, \\epsilon } : l^p \\to l^p$ définie par

$$ T_{\\varphi , \\epsilon } (u_n)  = ( \\epsilon_n u_{\\varphi(n)} ) $$

Alors les automorphismes isométriques de $l^p$ sont exactement les $T_{\\varphi, \\epsilon}$ pour les bijections $\\varphi : \\mathbb{N} \\to \\mathbb{N}$ et les suites $\\epsilon : \\mathbb \\to \\{ 1 , -1 \\}$.',
                'user_id' => 1,
            ),
            49 => 
            array (
                'id' => 50,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:16:24',
                'nom' => 'Étude qualitative de x\' = x^2 - t',
            'details' => 'On considère l\'équation différentielle $x\' = x^2 - t$. Soit $t_0 , x_0 \\in \\mathbb{R}$. On note $\\varphi_{(t_0, x_0)}$ l\'unique solution maximale de $(E)$ vérifiant $\\varphi_{(t_0,x_0)}(t_0) = x_0$. Alors il existe un unique $\\gamma \\in [0,1]$ tel que 

$$ \\varphi_{(0,\\gamma)}(t) = \\sqrt{t} + O(1/\\sqrt{t}) $$',
                'user_id' => 1,
            ),
            50 => 
            array (
                'id' => 51,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-15 19:03:17',
                'nom' => 'Théorème de Banach-Steinhaus et série de Fourier divergente',
            'details' => 'Soient $E$ un espace de Banach et $F$ un espace vectoriel. On munit $L_C(E,F)$ de la norme subordonnée $|||f||| = \\sup_{ ||x||_E = 1} ||f(x)||_F$. Soit $H \\subseteq L_C(E,F)$. Alors ou bien $H$ est bornée (c\'est-à-dire qu\'il existe $M >0$ telle que $\\forall f \\in H, |||f||| < M$ ) ou bien il existe $\\Omega$ un $G_\\delta$ dense de $E$ tel que $\\forall x \\in \\Omega$, $\\sup_{ f \\in H} || f(x)||_F = +\\infty$.

On utilise ce résultat pour obtenir un exemple de série de Fourier divergente.',
                'user_id' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:15:06',
                'nom' => 'Densité des fonctions tests dans Lp',
            'details' => 'Soit $p \\in [1, +\\infty[$, alors l\'espace $D( \\mathbb{R}^n)$ des fonctions $C^\\infty$ sur $\\mathbb{R}^n$ à support compact  est dense dans $L^p(\\mathbb{R}^n)$.',
                'user_id' => 1,
            ),
            52 => 
            array (
                'id' => 53,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 17:57:26',
                'nom' => 'Théorème d\'Hadamard Levy',
                'details' => 'Soit $f : \\mathbb{R}^n \\to \\mathbb{R}^n$ de classe $C^2$. La fonction $f$ est un $C^1$ difféomorphisme de $\\mathbb{R}^n \\to \\mathbb{R}^n$ si et seulement si $f$ est propre et pour tout $x \\in \\mathbb{R}^n$, $Df_x$ est inversible.',
                'user_id' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 18:00:24',
                'nom' => 'Théorème de Sturm',
                'details' => 'Soit $I$ un intervalle de $\\mathbb{R}$ et $q_1, q_2 : I \\to \\mathbb{R}$ deux fonctions continues telles que $q_1 \\ge q_2$. Soient $\\alpha < \\beta$ deux zéros d\'une solution non nulle de $y\'\' + q_2 y = 0$. Alors toute solution de $y\'\' + q_1 y = 0$ s\'annule sur $[\\alpha, \\beta ]$. ',
                'user_id' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 18:05:37',
                'nom' => 'Théorème de d\'Alembert-Gauss',
                'details' => 'Le corps $\\mathbb{C}$ est algébriquement clos.

Le développement propose ici une démonstration par récurrence.',
                'user_id' => 1,
            ),
            55 => 
            array (
                'id' => 56,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 18:09:57',
                'nom' => 'Réduction de Jordan d\'un endomorphisme nilpotent',
            'details' => 'Soit $u \\in L(E)$ un endomorphisme nilpotent. Il existe une base dans laquelle $u$ s\'écrit

$$ \\begin{pmatrix}
0 & v_1 & 0 &  & & \\\\
& \\ddots & \\ddots & \\ddots & \\\\
& & \\ddots  & \\ddots & 0 \\\\
& & & \\ddots & v_1\\\\
& & & & 0 
\\end{pmatrix}$$',
                'user_id' => 1,
            ),
            56 => 
            array (
                'id' => 57,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 17:14:55',
            'nom' => 'Isomorphisme entre PSU2(C) et SO₃(R)',
            'details' => 'Le groupe $PSU_2(\\mathbb{C})$ est isomorphe au groupe $SO_3(\\mathbb{R})$.',
                'user_id' => 1,
            ),
            57 => 
            array (
                'id' => 58,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-16 18:15:55',
                'nom' => 'Probabilité que deux nombres soient premiers entre eux',
                'details' => 'Soit $n \\in \\mathbb{N}^*$ un entier. On note $r_n$ la probabilité que deux entiers pris au hasard dans $\\{ 1 ,\\ldots , n \\}$ soient premiers entre eux. Alors

$$ r_n = \\frac{1}{n^2} \\sum_{d=1}^n \\mu(d) E( n/d)^2 \\to 6/\\pi^2$$

où $\\mu$ est la fonction de Möbius.',
                'user_id' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-17 09:50:00',
                'nom' => 'Théorème de l\'application ouverte',
                'details' => 'Soient $E$ et $F$ deux espaces vectoriels normés et $T : E \\to F$ une application linéaire. Elle est ouverte si et seulement si elle est ouverte en $0$. Dans ce cas, elle est surjective. 

Réciproquement si elle est surjective, alors elle est ouverte si on suppose de plus que $E$ et $F$ sont complets.',
                'user_id' => 1,
            ),
            59 => 
            array (
                'id' => 60,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-17 09:53:30',
                'nom' => 'Théorème de Cauchy-Lipschitz local',
            'details' => 'Soit $U$ un ouvet de $\\mathbb{R} \\times \\mathbb{R}^n$ et $f : U \\to \\mathbb{R}^n$ continue et localement lipschitzienne par rapport à la seconde variable. Soit $(t_0, x_0) \\in U$, alors il existe une solution locale au problème de Cauchy $x\' = f(t,x)$ avec $x(t_0) = x_0$.',
                'user_id' => 1,
            ),
            60 => 
            array (
                'id' => 61,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-17 14:46:28',
                'nom' => 'Théorème fondamentale de l\'étude locale des courbes',
                'details' => 'Soit $I$ un intervalle de $\\mathbb{R}$ contenant $0$. Soit $c : I \\to \\mathbb{R}_+^*$ de classe $C^1$. Soit $d : I \\to \\mathbb{R}$ de classe $C^0$. Soient $a \\in \\mathbb{R}^3$, ||v|| = 1, ||w||= 1, v orthogonal à $w$. 

Alors il existe une unique courbe $\\gamma$ de classe $C^3$ telle que $\\gamma : I \\to \\mathbb{R}^3$ soit birégulière paramétrée par longueur d\'arc et que sa courbure soit $c$, sa torsion $d$, $\\gamma(0) = a$, $\\gamma\'(0) = v$ et $\\gamma\'\'(0) / ||\\gamma\'\'(0)|| = w$.',
                'user_id' => 1,
            ),
            61 => 
            array (
                'id' => 62,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:17:11',
            'nom' => 'Endomorphismes de Mn(C) stabilisant le groupe linéaire',
            'details' => 'Soit $\\varphi$ un endomorphisme de $M_n(\\mathbb{C})$. Alors $\\varphi$ conserve le rang si et seulement si 

$$ M \\in GL_n(\\mathbb{C}) \\Rightarrow \\varphi(M) \\in GL_n(\\mathbb{C}) $$',
                'user_id' => 1,
            ),
            62 => 
            array (
                'id' => 63,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:17:32',
            'nom' => 'Calcul de exp(Mn(C)) et exp(Mn(R))',
            'details' => '$$\\exp( M_n(\\mathbb{C}) = GL_n(\\mathbb{C}) $$

$$ \\exp( M_n(\\mathbb{R}) = \\{ M \\in GL_n(\\mathbb{R}) : \\exists A \\in GL_n(\\mathbb{R}) , M = A^2 \\}$$',
                'user_id' => 1,
            ),
            63 => 
            array (
                'id' => 64,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-02-28 07:45:23',
                'nom' => 'Densité des polynômes orthogonaux',
            'details' => 'Soit $I$ un intervalle  de $R$ et $\\rho:I \\to ]0, \\infty[$ une fonction mesurable. S\'il existe  $a$ tel que $$ \\int_I e^{a|x|} \\rho(x) dx < \\infty$$, alors la famille de polynômes orthogonaux associée à $\\rho$ forme une base hilbertienne de $L^2(I, \\rho)$.',
                'user_id' => 1,
            ),
            64 => 
            array (
                'id' => 65,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:17:55',
            'nom' => 'Formule d\'inversion de Fourier dans S(Rd) ou L(Rd)',
            'details' => 'Soit $u \\in \\mathcal{S}(\\mathbb{R}^d)$, on définit $\\widehat{u} (y) = \\mathcal{F}(u)(y) = \\int_{\\mathbb{R}^d} u(x) e^{-ixy} dx$ et $\\overline{\\mathcal{F}}(u)(x) =(2\\pi)^{-d} \\int_{\\mathbb{R}^d} u(y) e^{ixy}dy $.
Alors 

$$ \\mathcal{F} \\circ \\overline{\\mathcal{F}} = \\overline{\\mathcal{F}} \\circ \\mathcal{F} = \\mathsf{id}_{\\mathcal{S}(\\mathbb{R}^d)} $$',
                'user_id' => 1,
            ),
            65 => 
            array (
                'id' => 66,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:24:34',
                'nom' => 'Théorème de Fourier-Plancherel',
            'details' => 'Soit $f \\in L^1 \\bigcap L^2$. Alors $|| \\widehat{f} ||_2 = ||f||_2$ et $\\mathcal{F} (L^1 \\bigcap L^2) \\subseteq L^2$ et de plus cette partie est dense.',
                'user_id' => 1,
            ),
            66 => 
            array (
                'id' => 67,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:26:16',
                'nom' => 'Dimension du commutant',
            'details' => 'Soit $A \\in M_n(K)$. On note $C(A) = \\{ M \\in M_n(K) : AM = MA \\}$. Alors $\\mu_A = \\chi_A$ si et seulement si $K[A] = C(A)$.',
                'user_id' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:28:14',
                'nom' => 'Galois inverse',
            'details' => 'Soit $p$ un nombre premier. Il existe un polynôme $P \\in \\mathbb{Q}[X]$, irréductible sur $\\mathbb{Q}$, dont on note $K$ le corps de décomposition, tel que $\\mathsf{Aut}(K) \\simeq \\mathfrak{S}_p$.',
                'user_id' => 1,
            ),
            68 => 
            array (
                'id' => 69,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:30:52',
                'nom' => 'Structure des polynômes symétriques',
                'details' => 'L\'algèbre $A[X_1 , \\ldots , X_n]_{\\text{sym}}$ des polynômes symétriques est engendrée par les polynômes symétriques élémentaires en $X_1 , \\ldots , X_n$.',
                'user_id' => 1,
            ),
            69 => 
            array (
                'id' => 70,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:33:26',
                'nom' => 'Théorème de Burnside',
            'details' => 'Soit $G$ un sous-groupe de $GL_n(\\mathbb{C})$ tel qu\'il existe $N \\in \\mathbb{N}^*$ tel que $\\forall A \\in G$, $A^N = I_n$. Alors $G$ est fini.',
                'user_id' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:35:56',
                'nom' => 'Test de Miller-Rabin',
                'details' => 'La probabilité d\'échec du test de primalité de Miller-Rabin est inférieure à $1/4$.',
                'user_id' => 1,
            ),
            71 => 
            array (
                'id' => 72,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:18:15',
                'nom' => 'Caractères linéaires continus de U',
                'details' => 'Soit $G$ un sous-groupe infini de $\\mathbb{U}$. Alors les seuls morphismes continus de $G$ dans $\\mathbb{C}^*$ sont les $z \\longmapsto z^k$ pour $k \\in \\mathbb{Z}$.',
                'user_id' => 1,
            ),
            72 => 
            array (
                'id' => 73,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:46:53',
                'nom' => 'Prolongement des caractères',
                'details' => 'Soit $G$ un groupe fini et $H$ un sous-groupe de $G$ tel que 

$$ \\forall g \\in G \\setminus H , gHg^{-1} \\bigcap H= \\{1 \\} $$

On définit $N = (G \\setminus \\bigcup_{g \\in G} gHg^{-1} ) \\bigcup \\{1 \\}$. Alors tout caractère de $H$ se prolonge en un caractère de $G$. S\'il est irréductible sur $H$, son prolongement peut être choisi irréductible.',
                'user_id' => 1,
            ),
            73 => 
            array (
                'id' => 215,
                'created_at' => '2016-03-29 17:45:14',
                'updated_at' => '2016-03-29 17:45:34',
                'nom' => 'Percolation',
                'details' => 'des proba
reca j\'en sais rien',
                'user_id' => 1,
            ),
            74 => 
            array (
                'id' => 75,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:50:58',
                'nom' => 'Théorème de Paley-Wiener',
            'details' => 'Soit $T \\in \\mathcal{E}\'(\\mathbb{R})$ d\'ordre $N_0$ telle que $\\mathsf{supp}(T) \\subseteq [-r,r]$. Il existe alors $F : \\mathbb{C} \\to \\mathbb{C}$ holomorphe telle que $F_{|\\mathbb{R}} = \\widehat{T}$ et pour tout $z \\in \\mathbb{C}$ on a

$$ | F(z)| \\le C ( 1 + |z|)^{N_0} e^{r |Im(z|} $$
',
                'user_id' => 1,
            ),
            75 => 
            array (
                'id' => 76,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-22 10:54:17',
                'nom' => 'Méthode de Newton-Raphson',
            'details' => 'Soient $\\Omega$ un ouvert de $\\mathbb{R}^m$ et $ f : \\Omega \\to \\mathbb{R}^m$ de classe $C^1$ telle qu\'il existe $a \\in \\Omega$ vérifiant $f(a) = 0$. On suppose $df_a$ inversible. Il existe un voisinage $U$ de $a$ dans $\\Omega$ tel que l\'application $\\varphi : x \\longmapsto x - df_x^{-1}(f(x))$ de $U$ dans $\\mathbb{R}^m$  soit bien défini et possède $a$ comme point fixe superattractif.',
                'user_id' => 1,
            ),
            76 => 
            array (
                'id' => 77,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:18:31',
                'nom' => 'Construction de l\'exponentielle et de Pi',
                'details' => 'Dans ce développement on défini l\'exponentielle et $\\pi$ à partir de la série entière $\\sum_{n \\ge 0} \\frac{z^n}{n!}$.',
                'user_id' => 1,
            ),
            77 => 
            array (
                'id' => 78,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:25:51',
                'nom' => 'Courbe à courbure croissante et applications',
            'details' => 'Soit $\\gamma : [a,b] \\to \\mathbb{R}^2$ une courbe $C^3$ paramétrée par longueur d\'arc. On suppose que $\\forall s \\in [a,b]$ , $k(s) > 0$ et $k\'(s) > 0$.

Si $s_1 < s_2$ alors $D_{s_2} \\subsetneq D_{s_1}$.

Si $A$ est la couronne ouverte comprise entre $D_b$ et $D_a$, si $x \\in A$, alors il existe un unique $C_s$ cercle osculateur à $\\gamma$ en $\\gamma(s)$ tel que $x \\in C_s$.',
                'user_id' => 1,
            ),
            78 => 
            array (
                'id' => 79,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:29:54',
                'nom' => 'Inégalité de Hoeffding',
            'details' => 'Soit $(X_n)$ une suite de variables aléatoires indépendantes centrées telles que $|X_n| \\le c_n$ presque surement. Soit $S_n = \\sum_{j=1}^n X_j$. Alors pour tout $\\epsilon > 0$, 

$$ P( |S_n| > \\epsilon ) \\le 2 \\exp \\left( - \\frac{\\epsilon^2}{2\\sum_{j=1}^n c_j^2 } \\right)$$',
                'user_id' => 1,
            ),
            79 => 
            array (
                'id' => 80,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:34:07',
                'nom' => 'Méthode de Monte-Carlo',
            'details' => 'Soit $P = [0,1]^d$ muni de la mesure de Lebesgue. Soit $f \\in L^1(P)$ et $X_1 , \\ldots , X_n$ des variables aléatoires iid de loi uniforme sur $P$. Soit $e_N = \\frac{1}{N} \\sum_{j=1}^N f(X_j) - \\int_P f$.

Alors $e_N $ converge vers $0$ presque sûrement.

Si de plus il existe $A$ et $B$ tels que $|f| \\le A$ presque partout et $| \\int f^2| \\le B$ alors $\\forall \\beta \\in [0,B/A^2]$, 

$$ P( |e_N| \\ge \\beta A ) \\le 2 \\exp \\left( \\frac{ -N \\beta^2 A^2}{4B} \\right) $$',
                'user_id' => 1,
            ),
            80 => 
            array (
                'id' => 81,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:37:33',
                'nom' => 'Statistiques d\'ordre',
            'details' => 'Soient $(X_i)$ une suite de variables aléatoires iid de loi $\\mu$ et admettant une densité $f$ $C_n^0$, une fonction de répartition $F$. On note $r(x_1 , \\ldots , x_n) = (x_{(1)} , \\ldots , x_{(n)} )$ où $x_{(1)} \\le \\cdots x_{(n)}$ et $\\{x_1 , \\ldots , x_n \\} = \\{ x_{(1)} , \\ldots , x_{(n)} \\}$ et $(X_1 , \\ldots , X_n) = r (X_1, \\ldots , X_n)$ où $X_{(k)}$ est la $k$-ième  statistique d\'ordre. 

Alors $X_{(k)}$ admet une densité $f_k$.',
                'user_id' => 1,
            ),
            81 => 
            array (
                'id' => 82,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:40:49',
                'nom' => 'Temps d\'arrêt',
            'details' => 'Soient $(X_n)_{n \\ge 0}$ une suite de variables aléatoires iid de loi de Bernouli de paramètre $p$. Notons $A_n$ l\'événement $X_{n-1} \\not= X_n$ pour $n\\not=2$ et appelons $T$ la variable aléatoire $T = \\int \\{ n \\in \\mathbb{N} : A_n \\text{ soit vérifiée} \\} $. Alors

Les $A_n$ sont indépendantes ssi $p = 1/2$.

$P ( T < +\\infty) = 1$

$ P(  (X_T , X_{T+1}) = (0,1) ) = p^2$',
                'user_id' => 1,
            ),
            82 => 
            array (
                'id' => 83,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:18:50',
                'nom' => 'Marche aléatoire dans Z^d',
            'details' => 'On considère le modèle suivant. Soit $d $ un entier $\\ge 1$ et $(u_i)$ la base canonique de $\\mathbb{R}^d$, on pose $S_0 = 0 \\in \\mathbb{Z}^d$, le $k$-ième pas est défini par $e_k \\in  \\{ \\pm u_i : 1 \\le i \\le d \\} = A$ et $P(e_k = u_i ) = P(e_k = -u_i)  = 1/2d$ pour tout $i$.
Les $e_k$ sont des variables aléatoires iid de sorte que $P(e_1 = \\epsilon_1, \\ldots , e_k = \\epsilon_k) = 1/(2d)^k$.

On pose $S_n  = \\sum_{k=1}^n e_k$ pour $n \\ge 1$.

Alors si $d \\le 2$, $P( S_n = 0 \\text{ infiniment souvent} ) = 1$
Si $ d \\ge 3$, alors $P( \\lim |S_n| = +\\infty ) = 1 $',
                'user_id' => 1,
            ),
            83 => 
            array (
                'id' => 84,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:47:58',
            'nom' => 'Théorème de Weierstrass (par les probabilités)',
            'details' => 'Soit $f : [0,1] \\to \\mathbb{R}$ une fonction continue. Pour tout $n$ on définit $B_n(x) = \\sum_{k=0}^n f(k/n) \\binom{n}{k} x^k (1-x)^{n-k}$ avec $x \\in [0,1]$. La suite de polynômes $(B_n)$ converge uniformément sur $[0,1]$ vers $f$.',
                'user_id' => 1,
            ),
            84 => 
            array (
                'id' => 85,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:52:22',
                'nom' => 'Fonctions caractéristiques et moments',
                'details' => 'Soit $X$ une variable aléatoire réelle. On note $\\varphi_X$ sa fonction caractéristique. 

Si $X$ admet un moment d\'ordre $n$, $\\varphi_X$ est $C^n$ et pour tout $k \\in [1, n]$ , on a pour tout $t  \\in \\mathbb{R}$, $\\varphi_X^{(k)}(t) = i^k \\int_{\\Omega} X^k \\exp(itX) dP$. En particulier $\\varphi_X^{(k)}(0) = i^k E[X^k]$. 

Si $\\varphi_X$ est $k$ fois dérivable en $0$, avec $k \\ge 2$, alors $X$ admet des moments jusqu\'à l\'ordre $2 E(k/2)$ ,où $E(k/2)$ est la partie entière, données par $\\mathbb{E} [X^j] = (-1)^j \\varphi_X^{(j)} (0)$.',
                'user_id' => 1,
            ),
            85 => 
            array (
                'id' => 86,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:56:00',
                'nom' => 'Existence de l\'espérance conditionelle',
            'details' => 'Soient $(\\Omega, A , P)$ un espace probabilisé, $B$ une sous-tribu de $A$ et $X$ une variable aléatoire réelle $L^1$. Alors il existe une unique (au sens presque sûrement) variable aléatoire $Y$ notée $E(X|B)$, $B$ mesurable vérifiant $\\forall B \\in \\mathcal{B}$, $\\int_B E( X|\\mathcal{B})(w) dP(w) = \\int_B X dP$ et $E( E(X | \\mathcal{B} ) 1_B) = E( X 1_B)$.

',
                'user_id' => 1,
            ),
            86 => 
            array (
                'id' => 87,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:57:57',
                'nom' => 'Billard convexe',
                'details' => 'Soit $K$ un compact de $\\mathbb{R}^2$ dont le bord $\\delta K$ est une sous-variété $C^1$ de dimension $1$ de $\\mathbb{R}^2$. Alors il existe une trajectoire fermée à trois rebonds vérifiant les lois de l\'optique.',
                'user_id' => 1,
            ),
            87 => 
            array (
                'id' => 88,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 09:59:44',
                'nom' => 'Fractions rationnelles et séries formelles',
                'details' => ' Soit $S \\in K[[X]] \\setminus K[X]$ notée $\\sum_{n \\ge 0} a_n X^n$ avec $a_n \\in K$ pour tout $n \\ge 0$. Sont équivalents :

$\\bullet$ La fraction rationnelle $S \\in K(X)_0$.
$\\bullet$ Il existe $ N \\ge 1, m \\ge N, \\lambda_1 , \\ldots , \\lambda_N \\in K$ telles que $\\lambda_N \\not= 0$ et $\\forall n \\ge m$ on a la relation $a_n - \\lambda_1 a_{n-1} - \\cdots - \\lambda_N a_{n-N} = 0$.
$\\bullet$ Il existe $N \\ge 1$ telle que $\\forall n \\ge N$, $\\det(A_n) = 0$ où $A_n = ( a_{i+j})_{ 0 \\le i , j \\le n } \\in M_{n+1}(K)$.


',
                'user_id' => 1,
            ),
            88 => 
            array (
                'id' => 89,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 10:03:40',
                'nom' => 'Théorème d\'Abel angulaire',
            'details' => 'Soit $\\sum a_n z^n$ une série entière, de rayon de convergence $1$. On note $f(z)$ la somme de cette série entière sur $D = D(0,1)$. On suppose que $\\sum a_n$ converge. On note $S = \\sum a_n$. Soit $\\theta_0 \\in [0, \\pi/2[$. 
On note 

$$ \\Delta_{\\theta_0} = \\{ z \\in \\mathbb{C} : |z| < 1 \\text{ et } 1-z = \\rho e^{i \\theta} \\text{ où } \\rho > 0 \\text{ et } \\theta \\in [-\\theta_0 , \\theta_0] \\} $$

Sous ces hypothèses, $f(z) \\to S$ lorsque $z $ tend vers $1$ en restant dans $\\Delta_{\\theta_0}$.
',
                'user_id' => 1,
            ),
            89 => 
            array (
                'id' => 90,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-25 10:04:24',
                'nom' => 'Théorème de Riesz-Fischer',
                'details' => 'Pour tout $p \\in [1 , +\\infty]$, $L^p$ est complet.',
                'user_id' => 1,
            ),
            90 => 
            array (
                'id' => 91,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:12:59',
                'nom' => 'Stabilité d\'un système autonome',
                'details' => 'L\'équilibre $O$ de $x\' = Ax$ est stable si et seulement si les valeurs propres de $A$ sont de partie réelle $< 0$ ou nulle et de multiplicité $1$. ',
                'user_id' => 1,
            ),
            91 => 
            array (
                'id' => 92,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:16:46',
                'nom' => 'Cercle d\'Euler',
            'details' => 'Soient $A,B,C$ trois points non alignés du plan affine euclidien. Alors il existe un unique cercle qui passe par $9$ points particuliers du triangle $ABC$. De plus le centre de ce cercle est l\'isobarycentre de $(H,A,B,C)$ où $H$ est l\'orthocentre de $A,B,C$.',
                'user_id' => 1,
            ),
            92 => 
            array (
                'id' => 93,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:18:58',
                'nom' => 'Théorèmes de Kakutani et Massera',
                'details' => 'Soit $E$ un espace vectoriel normé et $K$ un compact convexe non vide de $E$. Toute application affine continue $T : E \\to E$ admet un unique point fixe dans $K$.',
                'user_id' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:19:07',
            'nom' => 'Générateurs de O(E)',
            'details' => 'Soit $E$ un espace euclidien et $u \\in O(E)$. On note $r = \\mathsf{rg}(u- \\mathsf{id}_E)$. Alors $u$ est produit de $r$ réflexions mais pas moins. Si $u \\in SO(E)$, $u$ est produit d\'au plus $r$ retournements (avec $\\mathsf{dim}(E) \\ge 3$).',
                'user_id' => 1,
            ),
            94 => 
            array (
                'id' => 95,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:28:33',
                'nom' => 'Équation fonctionnelle de la fonction zêta de Riemann',
            'details' => 'On pose $\\zeta (s) = \\sum_{n \\ge1} \\frac{1}{n^s}$ pour $Re(s) > 1$. Alors la fonction $\\zeta$ admet un prolongement méromorphe à $\\mathbb{C}$ et vérifie

$$ \\pi^{-s/2} \\Gamma(s/2) \\zeta(s) = \\pi^{ -(1-s)/2} \\Gamma( (1-s)/2) \\zeta(1-s) $$',
                'user_id' => 1,
            ),
            95 => 
            array (
                'id' => 96,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:30:32',
                'nom' => 'Théorème d\'inversion locale',
            'details' => 'Soit $f : \\mathbb{R}^n \\to \\mathbb{R}^n$ de classe $C^1$ et $a \\in \\mathbb{R}^n$. Si $Df_a$ est inversible, alors il existe un voisinage $V$ de $a$ tel que $ W = f(V)$ est ouvert  et $f_{|V} : V \\to W$ est un $C^1$-difféomorphisme.
',
                'user_id' => 1,
            ),
            96 => 
            array (
                'id' => 97,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:32:54',
                'nom' => 'Formule de Poisson',
            'details' => 'Soit $A$ un anneau commutatif intègre. Soit $f = a_m X^m + \\cdots + a_0$ avec $a_m \\in A^*$. Soit $g \\in A[X]$. On note $S = A[X]/(f)$ et $g_S : S \\to S$ le passage au quotient de la multiplication par $g$. Alors 
$$ Res_{m,n}(f,g) = a_m^n \\det(g_S)$$',
                'user_id' => 1,
            ),
            97 => 
            array (
                'id' => 98,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:35:24',
                'nom' => 'Formule sommatoire de Poisson',
            'details' => 'Soit $f : \\mathbb{R} \\to \\mathbb{C}$ telle qu\'il existe $M > 0$ et $\\alpha > 1$ vérifiant $|f(x)| \\le \\frac{M}{ (1+|x|)^\\alpha}$ pour tout $x$. Si $\\sum_{\\mathbb{Z}} | \\widehat{f}(m)| < +\\infty$ alors 
$$ \\sum_{m \\in \\mathbb{Z}} \\widehat{f}(m) = 2 \\pi \\sum_{m \\in \\mathbb{Z}} f(2\\pi m )$$',
                'user_id' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:37:47',
                'nom' => 'Théorème de Borel',
            'details' => 'Soit $(a_n)$ une suite de nombres complexes. Alors il existe $ f : \\mathbb{R} \\to \\mathbb{C}$ une application $C^\\infty$ telle que pour tout $n$,
$$ \\frac{f^{(n)}}{n!} = a_n $$',
                'user_id' => 1,
            ),
            99 => 
            array (
                'id' => 100,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 07:39:21',
                'nom' => 'Théorème de Sophie-Germain',
            'details' => 'Soit $p$ un nombre premier impair tel que $q = 2p+1$ soit premier. Alors il n\'existe pas de triplet $(x,y,z) \\in \\mathbb{Z}^3$ tel que $x^p + y^p + z^p = 0$ et $xyz \\not=0 [p]$.',
                'user_id' => 1,
            ),
            100 => 
            array (
                'id' => 101,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:19:27',
                'nom' => 'Irréductibilité des polyômes cyclotomiques sur Q',
            'details' => 'Pout tout $n \\in \\mathbb{N}^*$, $\\Phi(n)$ est irréductible sur $\\mathbb{Q}$.',
                'user_id' => 1,
            ),
            101 => 
            array (
                'id' => 102,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:19:50',
                'nom' => 'Loi de réciprocité de la puissance d-ième',
                'details' => 'Soient $a,P,Q \\in A$ avec $P$ et $Q$ irréductibles unitaires de degré $\\delta$ et $\\gamma$. Alors
$(\\frac{a}{P})_d = 1 \\iff x^d = a [P]$ admet une solution

et  $(\\frac{Q}{P})_d = (-1)^{ \\frac{q-1}{d} \\delta \\gamma} (\\frac{P}{Q})_d$',
                'user_id' => 1,
            ),
            102 => 
            array (
                'id' => 103,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:20:38',
            'nom' => 'Étude de la suite logistique : x(n+1) = 1 - a x(n)^2',
            'details' => 'Soit $\\lambda \\in ]0,1]$ et $x_0 \\in ]0,1[$. On définit la suite $(x_n)$ par récurrence : $x_{n+1} = 1 - \\lambda x_n^2$.
On pose $f(x) = 1 -\\lambda x^2$. On étudie $f\\circ f $ et $f\\circ f - x$ pour étudier la limite éventuelle de la suite.',
                'user_id' => 1,
            ),
            103 => 
            array (
                'id' => 104,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:17:12',
                'nom' => 'Méthodes itératives de résolution d\'un système linéaire',
            'details' => 'Soient $A \\in GL_n(\\mathbb{R})$, $b \\in \\mathbb{R}^n$ et $u$ l\'unique solution de$Au = b$. On pose $u_0 \\in \\mathbb{R}^n$ et $u_{k+1} = M^{-1} ( N u_k +b)$ où $A = M-N$ avec $M \\in GL_n(\\mathbb{R})$. Alors la suite $(u_k)$ converge vers $u$ (quelque soit $u_0$) si et seulement si $\\rho( M^{-1}N) < 1$.',
                'user_id' => 1,
            ),
            104 => 
            array (
                'id' => 105,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:20:05',
                'nom' => 'Théorème de Barany',
            'details' => 'Soit $\\mathcal{E}$ un espace affine de dimension $n$. Soit $m \\ge n+1$. Soient $A_1 , \\ldots , A_m$ des parties de $\\mathcal{E}$. On appelle convexe multicolore tout convexe de la forme $\\mathsf{conv}(a_1 , \\ldots , a_m)$ où $(a_1 , \\ldots , a_m) \\in A_1 \\times \\cdots \\times A_m$.

Soit $w\\in \\mathcal{E}$. Si $w \\in \\bigcap_{i=1}^m \\mathsf{conv}(A_i)$ alors il existe un convexe multicolore $S$ tel que $w \\in S$.',
                'user_id' => 1,
            ),
            105 => 
            array (
                'id' => 106,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-14 16:30:43',
            'nom' => 'Théorème de Sylow (version opération de groupes)',
                'details' => 'Soit $G$ un groupe fini et $p$ premier tel que $|G| = p^a m$ avec $a$ un entier non nul et $m$ premier à $p$. On note $n_p$ le nombre de $p$-Sylow de $G$. Alors $n_p$ est non nul et divise $m$. De plus $n_p = 1 [p]$ et tous les $p$-Sylow sont conjugés.

Ce développement présente une démonstration de ce théorème à l\'aide d\'opérations de groupes. Il existe une autre démonstration utilisant le principe de récurrence.',
                'user_id' => 1,
            ),
            106 => 
            array (
                'id' => 107,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:22:55',
                'nom' => 'Méthode de Newton',
            'details' => 'Soit $f : [c,d] \\to \\mathbb{R}$ de classe $C^2$ tel que $f(a) = 0$ où $a \\in ]c,d[$. On définit la suite $(x_n)$ par $x_0 \\in [c,d]$ et $x_{n+1} = x_n - \\frac{ f(x_n) }{ f\'(x_n)}$ si $f\'(x_n) \\not=0$ et $x_{n+1} = x_n$ sinon. Alors sous certaines conditions on montre que $(x_n)$ converge vers $a$.',
                'user_id' => 1,
            ),
            107 => 
            array (
                'id' => 108,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:24:35',
                'nom' => 'Méthode de Newton pour les polyômes',
            'details' => 'La méthode de Newton générale est la suivante. Soit $f : [c,d] \\to \\mathbb{R}$ de classe $C^2$ tel que $f(a) = 0$ où $a \\in ]c,d[$. On définit la suite $(x_n)$ par $x_0 \\in [c,d]$ et $x_{n+1} = x_n - \\frac{ f(x_n) }{ f\'(x_n)}$ si $f\'(x_n) \\not=0$ et $x_{n+1} = x_n$ sinon. Alors sous certaines conditions on montre que $(x_n)$ converge vers $a$.

On précise ici la convergence dans le cas où $f$ est un polynôme.',
                'user_id' => 1,
            ),
            108 => 
            array (
                'id' => 109,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:26:39',
                'nom' => 'Inégalité isopérimétrique',
                'details' => 'Soit $\\gamma$ une courbe fermée simple de classe $C^1$ du plan euclidien $\\mathbb{R}^2$. On pose $A = \\frac{1}{2} | \\int xdy - y dx | $ qui correspond à l\'aire enfermée par $\\gamma$. Alors 
$$ A \\le \\frac{l^2}{4 \\pi} $$',
                'user_id' => 1,
            ),
            109 => 
            array (
                'id' => 110,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-05-27 11:28:22',
                'nom' => 'Résolution d\'une équation matricielle grâce aux équations différentielles',
            'details' => 'Soient $A$ et $B$ deux matrices réelles telles que $\\forall \\lambda \\in Sp(A) \\bigcap Sp(B)$, $Re(\\lambda)< 0$. Alors pour toute matrice réelle $C$, il existe une unique matrice $X \\in M_n(\\mathbb{R})$ telle que $AX + XB = C$.',
                'user_id' => 1,
            ),
            110 => 
            array (
                'id' => 111,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-06-02 12:09:32',
                'nom' => 'Théorème de Frobenius-Zolotarev',
            'details' => 'Soient $p$ un nombre premier au moins égal à $3$, $m \\in \\mathbb{N}^*$ et $u \\in GL_n(\\mathbb{F}_p)$. Alors $u$ est une permutation de $\\mathbb{F}_p^n$, de signature $(\\frac{ \\det(u)}{p} )$.',
                'user_id' => 1,
            ),
            111 => 
            array (
                'id' => 112,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-06-02 12:10:11',
                'nom' => 'Théorème de Wedderburn',
                'details' => 'Tout corps fini est commutatif.',
                'user_id' => 1,
            ),
            112 => 
            array (
                'id' => 113,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-06-02 12:12:35',
                'nom' => 'Construction de variables aléatoires indépendantes de loi donnée',
            'details' => 'Soit $(\\mu_j)_{j \\in \\mathbb{N}}$ une suite de probabilités sur $(\\mathbb{R} , \\mathcal{B}(\\mathbb{R})$. Alors il existe une suite de variables aléatoires $(X_j)_{j \\in \\mathbb{N}^*}$ définie sur $([0,1[, \\mathcal{B}( [0,1[) , P)$ où $P$ est la mesure de Lebesgue telle que $X_j$ soit de loi $\\mu_j$ pour tout $j$.',
                'user_id' => 1,
            ),
            113 => 
            array (
                'id' => 114,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-06-02 12:23:02',
            'nom' => 'Réduction de Jordan (par la dualité)',
            'details' => 'Soit $u \\in L(E)$ un endomorphisme nilpotent. Il existe une base dans laquelle $u$ s\'écrit

$$ \\begin{pmatrix}
0 & v_1 & 0 &  & & \\\\
& \\ddots & \\ddots & \\ddots & \\\\
& & \\ddots  & \\ddots & 0 \\\\
& & & \\ddots & v_1\\\\
& & & & 0 
\\end{pmatrix}$$',
                'user_id' => 1,
            ),
            114 => 
            array (
                'id' => 115,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorème du point fixe de Brouwer',
                'details' => 'On note $B^{n}$ la boule unité fermée de $\\mathbb{R}^{n}$. Toute application continue $B^{n} \\longrightarrow B^{n}$ admet un point fixe.',
                'user_id' => 1,
            ),
            115 => 
            array (
                'id' => 116,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:21:23',
                'nom' => 'A5 est l\'unique groupe simple d\'ordre 60',
                'details' => '$\\mathfrak{A}_{5}$ est l\'unique groupe simple d\'ordre 60.',
                'user_id' => 1,
            ),
            116 => 
            array (
                'id' => 117,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-06-02 12:25:57',
                'nom' => 'Prolongement de la fonction Zeta de Riemann',
                'details' => 'La fonction $\\zeta$ se prolonge en une fonction méromorphe sur $\\mathbb{C}$ holomorphe sur $\\mathbb{C} \\setminus \\{1\\}$ et admettant un pôle simple en $1$.',
                'user_id' => 1,
            ),
            117 => 
            array (
                'id' => 118,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:21:03',
                'nom' => 'Théorème de Jordan C1',
            'details' => 'Soit $g : \\mathbb{R} \\to \\mathbb{C}$ une application $L$-périodique $C^1$, injective sur $[0,L[$, telle que $g(0) = 0$, $g\'(0) = 1$, et $|g\'(t)| = 1$ pour tout $t \\in \\mathbb{R}$. On pose $\\Gamma = g(\\mathbb{R})$. 

Alors $\\mathbb{C} \\setminus \\Gamma$ a deux composantes connexes.',
                'user_id' => 1,
            ),
            118 => 
            array (
                'id' => 119,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:21:43',
                'nom' => 'Fonction continue 2Pi-périodique dont la série de Fourier diverge en 0',
                'details' => 'Soit $f : \\mathbb{R} \\to \\mathbb{R}$ la fonction paire, $2\\pi$-périodique telle que pour tout $x \\in [0, \\pi]$ on ait 

$$ f(x) = \\sum_{p \\ge 1} \\frac{1}{p^2} \\sin(  (2^{p^3} + 1) x/2 ) $$

On montre que série de de Fourier de $f$ diverge en $0$.',
                'user_id' => 1,
            ),
            119 => 
            array (
                'id' => 120,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-02-28 07:47:02',
                'nom' => 'Ellipsoïde de John Loewner',
            'details' => 'Pour tout $S \\in S_n^{++}(\\mathbb{R})$, on définit l\'ellipsoïde 

$$ \\Sigma_S = \\{ x \\in \\mathbb{R}^n : {}^t x S x \\le 1 \\} $$

Soit $K$ un compact d\'intérieur contenant $0 \\in \\mathbb{R}^n$. Alors l\'ensemble des ellipsoïdes contenant $K$ admet un élément de volume minimal.',
                'user_id' => 1,
            ),
            120 => 
            array (
                'id' => 121,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:20:52',
                'nom' => 'Méthode de Laplace',
            'details' => 'Soit $I = ]a,b[$ un intervalle de $\\mathbb{R}$ borné ou non. Soit $\\varphi \\in C^2(I, \\mathbb{R}) , f \\in C( I, \\mathbb{C})$. On suppose que 

$$ \\forall t > 0 , \\int_a^b e^{t \\varphi(x)} | f(x)|dx < +\\infty $$

que $\\varphi\'$ ne s\'annule qu\'en un point $x_0$ et que $\\varphi\'\'(x_0) < 0$.

que $f(x_0) \\not=0$.

Alors on a l\'équivalent trop hype :

$$ F(t) = \\int_a^b e^{t \\varphi(x)} f(x) dx \\sim_{t \\to +\\infty}  \\sqrt{ \\frac{ 2\\pi}{t |\\varphi\'\'(x_0)|} } e^{t \\varphi(x_0)} f(x_0) $$',
                'user_id' => 1,
            ),
            121 => 
            array (
                'id' => 122,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:25:29',
                'nom' => 'Équation différentielle dans les espaces de Hölder',
            'details' => 'Soient $\\alpha \\in ]0,1[$, $a,b \\in \\mathbb{R}$, $q \\in C^{0, \\alpha}( ]a,b[)$ positive. Pour tout $f \\in C^{0,\\alpha}( ]a,b[)$ et tous $u_0, u_1 \\in \\mathbb{R}$, le problème 

$$\\begin{cases}
-u\'\'(x) + q(x) u(x) = f(x) , x \\in ]a,b[ \\\\
u(a) = u_0 , u(b) = u_1
\\end{cases}$$

admet une unique solution $u \\in C^{2, \\alpha}( ]a,b[)$.',
                'user_id' => 1,
            ),
            122 => 
            array (
                'id' => 123,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:35:48',
                'nom' => 'Équation de Schrödinger',
            'details' => 'Soit $g \\in S\'(\\mathbb{R}^n)$. Il existe une unique solution $u = (u_t)_{t \\in \\mathbb{R}} \\in C^{\\infty}(\\mathbb{R}, S\'(\\mathbb{R}^n))$ au problème

$$\\begin{cases}
$\\partial_t u - i \\Delta u = 0 \\text{ dans } \\mathcal{D}\'( \\mathbb{R} \\times \\mathbb{R}^n)\\\\
u_0 = g
\\end{cases}$$',
                'user_id' => 1,
            ),
            123 => 
            array (
                'id' => 124,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:39:08',
            'nom' => 'Théorème de Bézout faible (par le résultant)',
            'details' => 'Soient $P,Q \\in \\mathbb{C}[X,Y]$ sans facteur commun (i.e. $\\mathsf{pgcd}_{\\mathbb{C}[X][Y]} (P,Q) = 1$). Soit $d$ le degré total de $P$ et $d\'$ celui de $Q$. On note $V(P) = \\{ (x,y) \\in \\mathbb{C}^2 : P(x,y) =0 \\}$. Alors

$$ | V(P) \\bigcap V(Q) | \\le dd\' $$',
                'user_id' => 1,
            ),
            124 => 
            array (
                'id' => 125,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:22:02',
                'nom' => 'Nombre de solutions d\'équations polynomiales sur Fq',
            'details' => 'Soient $d$ divisant $q-1$ et $S_d$ l\'ensemble des $n$-uplets de caractères $(\\chi_1, \\ldots , \\chi_n)$ tels que $\\chi_j \\not= \\chi_0$, $\\chi_j^d = \\chi_0$ et $\\chi_1 \\cdots \\chi_n = \\chi_0$ où $\\chi_0$ est le caractère trivial. Le nombre $N$ de solutions de l\'équation $a_1 x_1^d + \\cdots + a_n x_n^d = 0$ dans $\\mathbb{F}_q^n$ est égal à 

$$ N = q^{n-1} + \\frac{q-1}{q} \\sum_{ (\\chi_1 , \\ldots , chi_n) \\in S_d } \\overline{\\chi_1}(a_1) \\cdots \\overline{\\chi_n}(a_n) G( \\chi_1, \\psi) \\cdots G(\\chi_n, \\psi) $$',
                'user_id' => 1,
            ),
            125 => 
            array (
                'id' => 126,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:48:23',
                'nom' => 'Théorème de Witt',
            'details' => 'Soient $(E, q)$ , $(F,\\varphi)$ , $(G, \\psi)$ trois espaces quadratiques réguliers. Alors $(E, q) \\bot (F,\\varphi)$ est isomorphe à $(E,q) \\bot (G,\\psi)$ si et seulement si $(F, \\varphi)$ est isomorphe à $(G, \\psi)$.',
                'user_id' => 1,
            ),
            126 => 
            array (
                'id' => 127,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Densité des fonctions continues nulles part dérivables',
            'details' => 'L\'ensemble des fonctions continues nulles part dérivables est dense dans $C([0,1], R)$ munie de la norme infinie.',
                'user_id' => 1,
            ),
            127 => 
            array (
                'id' => 128,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:51:16',
                'nom' => 'Nombres de Bell',
                'details' => 'Pour tout $n \\in \\mathbb{N}_{> 0}$, on définit $B_n$ comme étant le nombre de partition de $\\{1 , \\ldots , n \\}$. Alors ces nombres vérifient

$$ B_n =\\frac{1}{e} \\sum_{k \\ge 0} \\frac{k^n}{n!} $$',
                'user_id' => 1,
            ),
            128 => 
            array (
                'id' => 129,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:53:49',
                'nom' => 'Partition d\'un entier en parts fixées',
            'details' => 'Soient $a_1 , \\ldots , a_k$ des entiers naturels non nuls premiers entre eux dans leur ensemble. Pour tout $n \\ge 1$, on note $u_n$ le nombre de $k$-uplets $(x_1 , \\ldots , x_k) \\in \\mathbb{N}^k$ tels que $a_1 x_1 + \\cdots + a_k x_k = n $. Alors 

$$ u_n \\sim_{n \\to +\\infty }  \\frac{1}{a_1 \\cdots a_k} \\frac{n^{k-1}}{(k-1)!} $$',
                'user_id' => 1,
            ),
            129 => 
            array (
                'id' => 130,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorème de l\'élement primitif',
                'details' => 'Toute extension séparable de type finie est monogène.',
                'user_id' => 1,
            ),
            130 => 
            array (
                'id' => 131,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:58:17',
                'nom' => 'Nombres de Carmichael et théorème de Korselt',
                'details' => 'Soit $n$ un entier naturel. On dit que $n$ est un nombre de Carmichael si $a^n = a [n]$ pour tout entier $a$.

Un entier naturel $n$ est de Carmichael ssi $n$ est sans facteur carré et pour tout nombre premier $p$ divisant $n$,

$$ p-1 | n-1 $$',
                'user_id' => 1,
            ),
            131 => 
            array (
                'id' => 132,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 15:59:55',
                'nom' => 'Calcul de trois intégrales',
                'details' => 'À l\'aide de la convergence monotone, de la convergence dominée et du théorème des résidus, on calule

$$ \\int_0^\\pi \\frac{x}{ 1 - e^{ix}} dx$$',
                'user_id' => 1,
            ),
            132 => 
            array (
                'id' => 133,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:04:57',
                'nom' => 'Méthode de relaxation',
                'details' => 'Méthode numérique pour résoudre $$A u = b$$

où $A \\in S_n^{++}(\\mathbb{R})$ et on décompose $A$ sous la forme de $D + T + {}^t T$ où $D$ est la diagonale de $A$ et $T$ est la partie triangulaire supérieure stricte. On introduit alors $w >0$, $M = D/w + {}^t T$ et $N = (1-w)/wD - T$ de sorte que $A = M-N$.',
                'user_id' => 1,
            ),
            133 => 
            array (
                'id' => 134,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:23:29',
            'nom' => 'Résolution de y\'\' - y = H dans S\'(R)',
            'details' => 'L\'équation différentielle $(E)$: $y\'\' - y = H$ admet une unique solution tempérée, qui est une fonction continue sur $\\mathbb{R}$.',
                'user_id' => 1,
            ),
            134 => 
            array (
                'id' => 135,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:07:53',
            'nom' => 'Théorème de Sard (version faible)',
            'details' => 'Soit $f : \\mathbb{R} \\to \\mathbb{R}^n$ de classe $C^1$, on pose $C = \\{ x \\in \\mathbb{R} : f\'(x) = 0\\}$. Alors $f(C)$ est de mesure de Lebesgue nulle.',
                'user_id' => 1,
            ),
            135 => 
            array (
                'id' => 136,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Réduction des endomorphismes normaux',
            'details' => 'Soit $E$ un espace euclidien et $f$ un endomorphisme normal (qui commute à son adjoint). Alors il existe une bonne base telle que la matrice de $f$ dans cette base soit presque diagonale par blocs avec des blocs de dimension $1$ et des blocs de dimensions $2$ spéciaux.',
                'user_id' => 1,
            ),
            136 => 
            array (
                'id' => 137,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:23:53',
            'nom' => 'Isomorphismes entre espaces de fonctions C(K)',
            'details' => 'Soient $K_1$ et $K_2$ deux espaces topologiques métriques compacts. Alors $K_1$ et $K_2$ sont homéomorphes ssi $C(K_1)$ sont $C(K_2)$ sont homéomorphes en tant qu\'algèbres.',
                'user_id' => 1,
            ),
            137 => 
            array (
                'id' => 138,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Un développement limité',
                'details' => '',
                'user_id' => 1,
            ),
            138 => 
            array (
                'id' => 139,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:11:21',
                'nom' => 'Équation de Fermat sur un corps fini',
            'details' => 'Soient $k \\ge 1$ un entier et $d = \\mathsf{pgcd}(d,q-1)$. Si $q \\ge d^4 +4$, l\'équation $x^k + y^k = z^k$ possède des solutions dans $(\\mathbb{F}_q^*)^3$.',
                'user_id' => 1,
            ),
            139 => 
            array (
                'id' => 140,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:13:53',
                'nom' => 'Structure des groupes abéliens finis',
                'details' => 'Soit $G$ un groupe abélien fini non trivial. Il existe un entier $r \\ge 1$ et des entiers $n_1 , \\ldots , n_r \\ge 2$ tels que  $n_r | n_{r-1} |  \\cdots | n_1 $ et 
$$G \\simeq \\mathbb{Z}/n_1 \\mathbb{Z} \\times \\cdots \\times \\mathbb{Z}/n_r \\mathbb{Z}$$',
                'user_id' => 1,
            ),
            140 => 
            array (
                'id' => 141,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:26:34',
                'nom' => 'Injection compacte dans les espaces de Sobolev',
            'details' => 'On définit $H^1([0,1]) = \\{ u \\in L^2( [0,1]) : u\' \\in L^2([0,1]) \\}$. Alors $H^1$ est un espace de Hilbert, ses éléments sont des fonctions continues sur $[0,1]$ et l\'injection $H^1 \\to C^0$ est compacte.',
                'user_id' => 1,
            ),
            141 => 
            array (
                'id' => 142,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:26:21',
                'nom' => 'Un isomorphisme entre groupes topologiques',
            'details' => 'Les espaces $PSL(2, \\mathbb{R})$ et $SO_0(1,2)$ (composante connexe de $I_3$ dans $SO(1,2)$) sont des groupes isomorphes.',
                'user_id' => 1,
            ),
            142 => 
            array (
                'id' => 143,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:29:37',
                'nom' => 'Théorème de Morgenstern',
                'details' => 'L\'espace $C^{\\infty}$ muni de la topologie de la convergence uniforme de toutes les dérivées, contient un ensemble dense de fonctions nulle part développables en série entière.',
                'user_id' => 1,
            ),
            143 => 
            array (
                'id' => 144,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:31:21',
                'nom' => 'Théorème de Cauchy-Peano',
                'details' => 'Soit $U$ un ouvert de $\\mathbb{R}^n$ et $X$ un champ de vecteurs continu sur $U$. Alors en tout point $y_0$, il existe $T > 0$ et une solution définie sur $[-T,T]$ qui passe par $y_0$ en $t = 0$.',
                'user_id' => 1,
            ),
            144 => 
            array (
                'id' => 145,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:26:52',
            'nom' => 'Le groupe SO3(R) est simple',
                'details' => 'C\'est peut être simple à énoncer  mais c\'est pas simple.',
                'user_id' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:27:25',
                'nom' => 'Automorphismes de Sn',
                'details' => 'Pour tout $n \\not=6$, les automorphismes de $\\mathfrak{S}_n$ sont intérieurs.

C\'est-à-dire que pour tout automorphisme $f$ de $\\mathfrak{S}_n$, il existe $\\sigma_0 \\in \\mathfrak{S}_n$ tel que pour toute permutation $\\sigma$,

$$ f(\\sigma) = \\sigma_0 \\sigma \\sigma_0^{-1} $$',
                'user_id' => 1,
            ),
            146 => 
            array (
                'id' => 147,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:27:41',
            'nom' => 'Générateurs du groupe Isom(E)',
            'details' => 'Soit $E$ un espace affine euclidien et $f \\in Isom(E)$. On note $r = rg( \\overrightarrow{f} - id)$. 

Si $f$ admet un point fixe alors $f$ est la composée de $r$ réflexions et pas moins.

Sinon, $f$ est la composée de $r+2$ réflexions et pas moins.',
                'user_id' => 1,
            ),
            147 => 
            array (
                'id' => 148,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:39:00',
                'nom' => 'Théorème ergodique de Von Neumann',
            'details' => 'Soit $H$ un espace de Hilbert, $T$ un endomorphisme continue de norme $\\le 1$ et $p$ la projection orthogonale sur $ker(T - id)$. On pose $T_n = \\frac{1}{n+1} \\sum_{k=1}^n T^k$. Alors pour tout $x\\in H$, 

$$ T_n(x) \\to p(x) $$

lorsque $n \\to +\\infty$.',
                'user_id' => 1,
            ),
            148 => 
            array (
                'id' => 149,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-08 16:41:26',
                'nom' => 'Suite récurrente : convergence lente',
                'details' => 'Soit $f : I \\to \\mathbb{R}$ définie au voisinage de $0$ qui a développement limité de la forme 

$$ f(x) = x - ax^\\alpha + o(x^\\alpha) $$

avec $a > 0$ et $\\alpha > 1$. Alors la suite récurrence $u_{n+1} = f(u_n)$ admet l\'équivalent 

$$ u_n \\sim \\frac{1}{ (na(\\alpha-1)^{\\frac{1}{1-\\alpha} } } $$',
                'user_id' => 1,
            ),
            149 => 
            array (
                'id' => 150,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 07:51:57',
                'nom' => 'Topologie des classes de similitude',
            'details' => 'Soit $A \\in M_n(\\mathbb{C})$.

La matrice $A$ est nilpotente ssi $0$ appartient à la classe de similitude de $A$.

La matrice $A$ est diagonalisable ssi la classe de similitude de $A$ est fermée.',
                'user_id' => 1,
            ),
            150 => 
            array (
                'id' => 151,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 07:59:40',
                'nom' => 'Diagonalisibilité et semi-simplicité',
                'details' => 'Soit $E$ un $K$-espace vectoriel de dimension finie et $u$ un endomorphisme de $E$.

L\'endomorphisme $u$ est diagonalisable ssi tout sous-espace admet un supplémentaire stable.

L\'endomorphisme $u$ est diagonalisable ssi $\\chi_u$ est scindé et tout sous-espace stable admet un supplémentaire stable.

[oui le théorème est un peu chelou ... ???]',
                'user_id' => 1,
            ),
            151 => 
            array (
                'id' => 152,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:02:13',
                'nom' => 'Théorème de Pascal',
                'details' => 'Un hexagone est une famille de six points distincts dont trois ne sont jamais alignés.

Un hexagone $(a,b,c,d,e,f)$ est inscrit dans une conique $C$ de $P_2(K)$ ssi les points $(ab) \\bigcap (de)$, $(bc) \\bigcap (ef)$ et $(cd)\\bigcap (fa)$ sont alignés. Dans ce cas, la conique est propre.',
                'user_id' => 1,
            ),
            152 => 
            array (
                'id' => 153,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:03:05',
                'nom' => 'Théorème de Dirichlet faible',
                'details' => 'Soit $n$ un entier. Alors il existe une infinité de nombre premiers congru à $1$ modulo $n$.',
                'user_id' => 1,
            ),
            153 => 
            array (
                'id' => 154,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:28:00',
                'nom' => 'Table de caractères de D4 et H8',
            'details' => 'On dresse la table de caractères de $D_4$ et de $H_8$ (qui est la même).',
                'user_id' => 1,
            ),
            154 => 
            array (
                'id' => 155,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:09:04',
                'nom' => 'Théorème de Molien',
            'details' => 'Soit $G$ un sous-groupe fini de $GL_n(\\mathbb{C})$. On pose $A= \\mathbb{C} [X_1 , \\ldots , X_n]$. On définit la représentation $\\rho$ suivante de $G$ :

$$ \\rho(g)(P) = P \\circ g^{-1} $$

où $P \\in A$. Cette représentation conserve $A_k$ : les polynômes $k$ homogènes de $A$ pour tout $k$. On note $A_k^G$ l\'ensemble des vecteurs invariants de $A_k$ sous l\'action de $G$. On note $a_k(G)$ la dimension de cet espace. Alors

$$ \\sum_{k\\ge 0} a_k(G) X^k = \\frac{1}{|G|} \\sum_{g \\in G} \\frac{1}{\\det( id - Xg)} $$',
                'user_id' => 1,
            ),
            155 => 
            array (
                'id' => 156,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:12:58',
                'nom' => 'Point de Fermat-Torricelli',
                'details' => 'Soit $ABC$ un triangle non aplati du plan affine euclidien identifié à $\\mathbb{C}$ via un repère orthonormé. On note $a,b,c$ les fixes des points $A,B,C$. On construit les points $A\',B\',C\'$ tels que $ACB\'$, $CB\'A$ et $A\'CB$ soient les triangles équilatéraux directs. Alors :

Les droites $(AA\')$, $(BB\')$ et $(CC\')$ concourent en un point $M$.

Si les angles du triangle $ABC$ sont tous $\\le 2\\pi/3$ alors $M$ est un minimum de la fonction Fermat $f(M)= MA + MB + MC$.',
                'user_id' => 1,
            ),
            156 => 
            array (
                'id' => 157,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:17:55',
                'nom' => 'Équation de la chaleur et distributions',
                'details' => 'L\'équation de la chaleur : $\\partial_t u - \\Delta u = 0$.

Si $u_0 \\in \\mathcal{E}\'(\\mathbb{R})$, il existe une solution $u$ à ce problème. De plus, si $u_0$ est dans $C_c^0(\\mathbb{R})$ alors la solution obtenue est $C^0$ sur $[0, +\\infty[ \\times \\mathbb{R}$ et $C^\\infty$ sur $]0, +\\infty[ \\times \\mathbb{R}$ et vérifie la condition initiale $u(0,.) = u_0$',
                'user_id' => 1,
            ),
            157 => 
            array (
                'id' => 158,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:20:08',
            'nom' => 'Théorème de Pascal (par le birapport)',
            'details' => 'Soit $\\varphi$ une homographie de $P^1(K)$ qui n\'est pas l\'identité. Il y a équivalence entre

$\\varphi$ est une involution

$\\varphi$ est de trace nulle

Il existe $p \\in P^1(K)$ tel que $\\varphi(p) \\not= p$ et $\\varphi^2(p) = p$.',
                'user_id' => 1,
            ),
            158 => 
            array (
                'id' => 159,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:22:00',
            'nom' => 'Théorème de point fixe de Kakutani (par Hahn-Banach)',
            'details' => 'Soient $G$ un groupe compact, $V$ un $\\mathbb{R}$-espace vectoriel de dimension finie, $\\rho : G \\to GL(V)$ un morphisme de groupes continu et $\\Omega \\subseteq V$ un convexe $G$-stable non vide. Alors 

$$ \\exists x \\in \\Omega : g(x) = x, \\forall g \\in G $$',
                'user_id' => 1,
            ),
            159 => 
            array (
                'id' => 160,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:26:33',
                'nom' => 'Théorème de réarrangment de Riemann',
            'details' => 'Soit $(u_n)$ une série réelle semi-convergente et soit $\\alpha \\in [-\\infty, +\\infty]$. Alors il existe une permutation $\\sigma$ de $\\mathbb{N}$ telle que 

$$ \\sum_{k = 0}^n u_{\\sigma(k)} \\to \\alpha $$',
                'user_id' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:29:41',
                'nom' => 'Théorème de Sharkovski',
                'details' => 'Soit $f : I \\to I$ continue où $I$ est un intervalle. Si $f$ admet un point périodique de période $n$, alors pour tout $m$ succédant à $n$ dans l\'ordre de Sharkovski, $f$ admet un point périodique de période $m$.',
                'user_id' => 1,
            ),
            161 => 
            array (
                'id' => 162,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Translatés d\'une fonction',
            'details' => 'Pour $f : R \\to R$ et $a \\in R$, on note $f_a(x) = f(a +x)$. Soit $f : R \\to R$ dérivable, alors : $Vect( f_a)_{a \\in R}$ est de dimension finie ssi $f$ est solution homogène d\'une équation différentielle linéaire à coefficients constants.',
                'user_id' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:33:37',
                'nom' => 'Polynômes semi-symétriques',
                'details' => 'Soit $K$ un corps de caractéristique $\\not=2$. 
Un polynôme $P \\in K[T_1,\\ldots , T_n]$ est dit semi-symétrique si pour tout $\\sigma \\in \\mathfrak{A}_n$, $\\sigma \\dot{} P = P$.

Soit $F \\in K[T_1 , \\ldots , T_n]$ un polynôme semi-symétrique. Alors il existe un unique couple $(P,Q)$ de polynômes symétriques tels que $F = P + QV$ (où $V$ est le Vandermonde de $T_1, \\ldots , T_n$).',
                'user_id' => 1,
            ),
            163 => 
            array (
                'id' => 164,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:36:32',
                'nom' => 'Théorème de Lax-Milgram et une application',
                'details' => 'Soit $H$ un espace de Hilbert. Soit $a$ une forme bilinéaire telle que 

$ \\exists M > 0, \\forall u,v, |a(u,v)| \\le M ||u|| \\dot{} ||v|| $ (a est continue)

$\\exists \\nu >0, \\forall u , a(u,u) \\ge \\nu ||u||^2  $ ($a$ est coercive)

Soit $l$ une forme linéaire continue.
Alors il existe un unique $u \\in H$ tel que $\\forall v \\in H$, $a(u,v) = l(v)$.',
                'user_id' => 1,
            ),
            164 => 
            array (
                'id' => 165,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:41:10',
                'nom' => 'Espaces hyperboliques',
                'details' => 'Soit $E$ un espace vectoriel et $q$ une forme quadratique. On note $f$ la forme bilinéaire symétrique associée.

Un plan hyperbolique pour $q$ c\'est un sous-espace vectoriel de dimension $2$ tel qu\'il existe $x,y \\in P$ tels que $q(x) = q(y) = 0$ et $f(x,y) = 1$.

Soit $F$ un sous-espace vectoriel de $E$. On note $F_0 = Ker(q_{|F})$ et $U$ un supplémentaire de $F_0$ dans $F$. Soit $(u_1, \\ldots , u_r)$ une base de $F_0$. Il existe alors $v_1 , \\ldots , v_r \\in E$ tels que 

$P_i = Vect(u_i, v_i)$ est un plan hyperbolique pour $q_{|P_i}$ et $(u_i,v_i)$ est une base hyperbolique de $P_i$.

$U,P_1 , \\ldots , P_r$ sont en somme directe dans $E$ deux à deux orthogonaux.',
                'user_id' => 1,
            ),
            165 => 
            array (
                'id' => 166,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Birapport et homographies',
                'details' => 'On montre que les homographies de la droite projective sont caractérisées par l\'invariance du birapport.',
                'user_id' => 1,
            ),
            166 => 
            array (
                'id' => 167,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:44:22',
                'nom' => 'Solutions développables en série entière de l\'équation de Bessel',
                'details' => 'On considère l\'équation différentielle $xy\'\' + y\' + xy  = 0$.
Il existe une unique solution développable en série entière en $0$ et valant $1$ en $0$. C\'est 

$$ f(x) = \\sum_{n \\ge 0} \\frac{(-1)^n}{4^n (n!)^2} x^n $$

',
                'user_id' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:47:05',
                'nom' => 'Développement asymptotique de la série harmonique',
                'details' => 'Soit $H_n = \\sum_{k=1}^n \\frac{1}{k}$.

Il existe $\\gamma > 0$ tel que $H_n -\\ln(n) \\to \\gamma$.

$H_n = \\ln(n) + \\gamma + \\frac{1}{2n} + o(1/n)$

On pose $h_n = \\min\\{ k \\in \\mathbb{N} : H_k \\ge n\\}$. Alors $h_{k+1}/h_k \\to e$.',
                'user_id' => 1,
            ),
            168 => 
            array (
                'id' => 169,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 08:51:37',
                'nom' => 'Application du Vandermonde',
            'details' => 'On note $V(a_1 , \\ldots , a_n) = \\det( a_i^{j-1} )$ le déterminant de Vandermonde.
Soit $P_1 , \\ldots , P_n \\in \\mathbb{R}_m[X]$, $a_1 , \\ldots , a_n \\in \\mathbb{R}$.
On suppose que $n > m$.

Alors 

$$
\\begin{vmatrix}
P_1(a_1) & \\cdots & P_1(a_n) \\\\
\\vdots & & \\vdots \\\\
P_n(a_1) & \\cdots & P_n(a_n)
\\end{vmatrix} = V(a_1, \\ldots , a_n) \\det(P_1, \\ldots , P_n) $$

où le dernier déterminant est le déterminant dans la base canonique de $\\mathbb{R}_{n-1}[X]$.
',
                'user_id' => 1,
            ),
            169 => 
            array (
                'id' => 170,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:28:20',
            'nom' => 'Automorphismes de K(X)',
            'details' => 'Les automorphismes de $K$-algèbre de $K(X)$ sont les applications de $K(X)$ vers $K(X)$ définies par 

$$ F \\longmapsto F ( \\frac{aX +b}{cX + d} )$$

avec $ad-bd \\not=0$',
                'user_id' => 1,
            ),
            170 => 
            array (
                'id' => 171,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:31:50',
                'nom' => 'Géodésiques du demi plan de Poincarré',
            'details' => 'Soit $H = \\{ z \\in \\mathbb{C} : Im(z) > 0 \\}$. Soient $A,B \\in H$ on pose  $V = \\{ v \\in C^1 ( [0,1], H)  , v(0) = A, v(1) = B \\}$.  Pour tout $v \\in V$, on pose 

$$ E(v) = \\frac{1}{2} \\int_0^1  \\frac{v_1\'(s)^2 + v_2\'(s)^2}{ v_2(s)^2} ds $$

Soit $v$ minimisant $E$. Alors $Im(v)$ est incluse soit dans une demi-droite  verticale soit dans un demi-cercle centré sur l\'axe des abscisses.
',
                'user_id' => 1,
            ),
            171 => 
            array (
                'id' => 172,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:34:14',
                'nom' => 'Lemme de Schwarz et biholomorphismes du disque',
                'details' => 'Pour $a \\in \\mathbb{D}$ et $\\lambda \\in \\mathbb{U}$, on pose 

$$ \\varphi_{a,u}(z) = \\lambda \\frac{ a-z}{1 - \\overline{a} z }$$

définie sur $\\mathbb{D}$.

L\'ensemble des biholomorphismes du disque est exactement l\'ensemble des applications $\\varphi_{a,u}$.',
                'user_id' => 1,
            ),
            172 => 
            array (
                'id' => 173,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:35:45',
                'nom' => 'Construction des corps finis',
                'details' => 'On montre dans ce développement l\'existence d\'un polynôme irréductible de degré $n$ sur $\\mathbb{F}_p[X]$. On crée alors un corps fini à $p^n$ éléments en prenant le corps de rupture de ce polynôme.',
                'user_id' => 1,
            ),
            173 => 
            array (
                'id' => 174,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:40:18',
                'nom' => 'Chemin auto-évitants',
                'details' => 'Un chemin auto-évitant est, informellement, un chemin sur $\\mathbb{Z}^d$ qui ne passe pas deux fois par le même point et qui se déplace d\'un point à un autre point à distance $1$.

En générant un chemin avec une loi de Bernoulli, on montre qu\'il existe presque sûrement un chemin auto-évitant de longueur fini. On gère de plus le cas d\'un chemin auto-évitant infini.',
                'user_id' => 1,
            ),
            174 => 
            array (
                'id' => 175,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:43:47',
                'nom' => 'Critère de Weyl',
            'details' => 'Soit une suite $(u_n)_{n\\ge 1} \\in [0,1]$. Pour tout $0 \\le a \\le b \\le 1$, on note 

$$ X_n[a,b] = |\\{ k \\in \\mathbb{N}, 1 \\le k \\le n : u_k \\in [a,b] \\} | $$

Les assertions suivantes sont équivalentes :

- $\\forall 0 \\le a \\le b \\le 1,  \\frac{ X_n[a,b]}{n} \\to b-a$ 
- $\\forall f \\in C^0[0,1] , \\frac{1}{n} \\sum_{k=1}^n f(u_k) \\to \\int_0^1 f$
- $ \\forall p \\in \\mathbb{N}^*, \\frac{1}{n} \\sum_{k=1}^n e^{i 2\\pi p u_k} \\to 0$
',
                'user_id' => 1,
            ),
            175 => 
            array (
                'id' => 176,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:46:36',
                'nom' => 'Échantillonage de Shannon',
            'details' => 'On pose $BL^2 = \\{ u \\in L^2(\\mathbb{R}) : \\widehat{u}_{| \\mathbb{R}\\setminus I} = 0 \\}$ où $I = [ -1/2 , 1/2]$.

Alors 
- $BL^2$ est un espace de Hilbert
- L\'application $BL^2 \\to l^2(\\mathbb{Z})$ définie par $u \\longmapsto (u(n))_{n \\in \\mathbb{Z}}$ est une isométrie.',
                'user_id' => 1,
            ),
            176 => 
            array (
                'id' => 177,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 09:48:37',
                'nom' => 'Équation matricielle',
            'details' => 'Soient $A, B \\in M_n(\\mathbb{R})$ telles que $Sp_\\mathbb{C}(A) \\bigcup Sp_\\mathbb{C}(B)= \\{ \\lambda : Re(\\lambda) < 0 \\}$. Alors pour tout $C \\in M_n(\\mathbb{R})$ l\'équation $AX + BX = C$ admet une unique solution dans $M_n(\\mathbb{R})$.',
                'user_id' => 1,
            ),
            177 => 
            array (
                'id' => 178,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:28:36',
                'nom' => 'Groupes d\'ordre pq',
                'details' => 'Soit $G$ un groupe d\'ordre $pq$ où $p$ et $q$ sont des nombres premiers tels que $p < q$.

Si $q \\not=1 [p]$ alors $G \\simeq \\mathbb{Z}/pq\\mathbb{Z}$.

Si $q = 1[p]$ alors $G \\simeq \\mathbb{Z}/pq \\mathbb{Z}$ ou $\\mathbb{Z}/q\\mathbb{Z} \\rtimes_\\alpha \\mathbb{Z}/p\\mathbb{Z}$ où $\\alpha$ est un morphisme de groupe non trivial de $\\mathbb{Z}/p\\mathbb{Z} \\to Aut( \\mathbb{Z}/q\\mathbb{Z}) $.',
                'user_id' => 1,
            ),
            178 => 
            array (
                'id' => 179,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-10 10:00:01',
                'nom' => 'Méthode de Jacobi',
            'details' => 'Soit $A \\in S_n(\\mathbb{R})$. La méthode de Jacobi consiste à définir la suite $(A_k)$ par $A_0 = A$, $A_{k+1} = A_k$ si $A_k$ est diagonale et
$$ A_{k+1} = {}^t \\Omega_{k+1} A_k \\Omega_{k+1} $$

où on définira la matrice $\\Omega_k$.

On note $\\lambda_i$ les valeurs propres de $A$. Alors il existe $\\sigma \\in \\mathfrak{S}_n$ telle que 

$$ A_k \\to_{k \\to +\\infty} \\mathsf{diag}( \\lambda_{\\sigma(i)} )$$',
                'user_id' => 1,
            ),
            179 => 
            array (
                'id' => 180,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:28:57',
            'nom' => 'Morphisme d\'algèbre sur C(K,R)',
                'details' => 'Soit $K$ un espace métrique compact non vide.
$C(K,R)$ est l\'ensemble des applications continues de $K$ dans $R$.
L\'ensemble des morphismes d\'algèbre de $C(K,R)$ dans $R$ sont les $  f \\longmapsto f(s)$ pour un $s \\in K$, ainsi que l\'application nulle.',
                'user_id' => 1,
            ),
            180 => 
            array (
                'id' => 181,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:03:44',
                'nom' => 'Groupe Circulaire',
            'details' => 'Toute bijection de $P^1(\\mathbb{C})$ préservant les cercles-droites appartient au groupe circulaire.',
                'user_id' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:04:51',
                'nom' => 'Théorème de Kronecker',
            'details' => 'Soit $P \\in \\mathbb{Z}[X]$ unitaire tel que $P(0) \\not=0$. Si toutes les racines de $P$ sont de module inférieur à $1$, alors ce sont des racines de l\'unité.',
                'user_id' => 1,
            ),
            182 => 
            array (
                'id' => 183,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:06:29',
            'nom' => 'Théorème de Dirichlet (fort)',
                'details' => 'Soient $a$ et $b$ deux nombres premiers entre eux. Alors il existe une infinité de nombres premiers congrus à $a$ modulo $b$.

Dans ce développement qu\'une partie de ce théorème.',
                'user_id' => 1,
            ),
            183 => 
            array (
                'id' => 184,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:08:59',
                'nom' => 'Formule de Poisson discrète',
                'details' => 'Soit $G$ un groupe abélien fini, $H$ un sous-groupe de $G$ et $f : G \\to \\mathbb{C}$. Alors pour tout $g \\in G$, 

$$ \\sum_{h \\in H} f(gh) = \\frac{ |H|}{|G|} \\sum_{ \\chi \\in H^\\#} \\widehat{f} ( \\overline{\\chi} ) \\chi(g) $$',
                'user_id' => 1,
            ),
            184 => 
            array (
                'id' => 185,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:14:51',
                'nom' => 'Dobble et géométrie projective',
                'details' => 'On étudie la combinatoire du jeu de Dobble. Il s\'agit d\'un jeu de cartes construit ainsi. Sur chaque carte sont dessinés des symboles avec les propriétés suivantes :

entre deux cartes il y a exactement un symbole en commun,

pour toute paire de symbole il y a exactement une carte qui contient cette paire de symboles,

toute carte possède au moins trois symboles.

Un tel jeu de cartes peut être réalisé en prenant les droites dans $P^2(K)$ où $K$ est un corps fini.
',
                'user_id' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2017-03-01 13:29:14',
                'nom' => 'S4 est un groupe de pavage',
                'details' => 'On donne une interprétation de la représentation irréductible de degré $2$ de $\\mathfrak{S}_4$.
En particulier on montre pour cela l\'isomorphisme

$$ \\mathfrak{S}_4 \\simeq \\langle t_1, t_{j^2}, r_{O, 2\\pi/3} , s_{Ox} \\rangle / \\langle t_2 , t_{2j^2} \\rangle $$',
                'user_id' => 1,
            ),
            186 => 
            array (
                'id' => 187,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:19:40',
                'nom' => 'Transformée de Fourier rapide',
            'details' => 'Soient $P,Q \\in \\mathbb{R}_n[X]$, on peut calculer les coefficients de $P \\dot{} Q$ en temps $O(n\\ln(n))$.',
                'user_id' => 1,
            ),
            187 => 
            array (
                'id' => 188,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:20:58',
                'nom' => 'Algorithme CYK',
            'details' => 'Soit $G$ une grammaire donnée sous forme normale de Chomsky. Alors, étant donné un mot $w$, on peut tester si $w \\in L(G)$ en temps $O(|w|^3)$.',
                'user_id' => 1,
            ),
            188 => 
            array (
                'id' => 189,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:23:11',
                'nom' => 'Algorithmes exponentiels pour INDEP',
                'details' => 'On y étudie plusieurs algorithmes exponentiels pour trouver l\'ensemble indépendant de taille maximale d\'un graphe.',
                'user_id' => 1,
            ),
            189 => 
            array (
                'id' => 190,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:25:03',
                'nom' => 'Algorithme de Floyd-Warshall',
            'details' => 'Soit $G = (S,A)$ et $w : A \\to \\mathbb{R}$ une fonction de poids. On peut trouver toutes les plus courtes distances entre les paires de sommet si $G$ n\'a pas de cycle de poids négatif ou trouver un cycle de poids négatif en $O( |S|^3)$.',
                'user_id' => 1,
            ),
            190 => 
            array (
                'id' => 191,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-12 15:26:29',
                'nom' => 'Algorithme KMP',
                'details' => 'On étudie dans ce développement l\'algorithme KMP permettant de trouver une occurence d\'un sous-mot dans un mot.',
                'user_id' => 1,
            ),
            191 => 
            array (
                'id' => 192,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:32:30',
                'nom' => 'Algorithme d\'unification',
                'details' => 'On montre que si l\'algorithme d\'unification termine avec $E_n = \\emptyset$, alors la substitution $\\sigma$ est l\'unification la plus générale du système d\'équations donné. S\'il échoue alors le système d\'équations n\'a pas d\'unification.',
                'user_id' => 1,
            ),
            192 => 
            array (
                'id' => 193,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:35:02',
                'nom' => 'Arbres Splay',
            'details' => 'Les arbres splay sont une structure de données permettant de réaliser en complexité amortie $O( \\ln(n))$ les opérations suivantes : mem(i,s), ajout(i,s), supp(i,s), union(s,s\'), pivote(i,s) qui se font à l\'aide d\'une opération élémentaire : l\'opération splay.',
                'user_id' => 1,
            ),
            193 => 
            array (
                'id' => 194,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:37:34',
                'nom' => 'Calcul de Premier et Suivant',
                'details' => 'Étude de l\'opération de calcul de "premier" et "suivant" dans un compilateur.',
                'user_id' => 1,
            ),
            194 => 
            array (
                'id' => 195,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:40:20',
                'nom' => 'Complétude de la méthode de résolution',
                'details' => 'Soit $\\Sigma$ un ensemble de clauses contradictoires. Alors il existe un arbre de réfutation de $\\Sigma$.',
                'user_id' => 1,
            ),
            195 => 
            array (
                'id' => 196,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:42:46',
                'nom' => 'Constructivité de la logique intuitionniste',
                'details' => 'On étudie la logique intuitionniste.',
                'user_id' => 1,
            ),
            196 => 
            array (
                'id' => 197,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:43:37',
                'nom' => 'Décidabilité de l\'arithmétique de Presburger',
                'details' => 'La théorie du premier ordre des entiers munis de l\'addition est décidable.',
                'user_id' => 1,
            ),
            197 => 
            array (
                'id' => 198,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:44:18',
                'nom' => 'Fonctions récursives et Turing calculabilité',
                'details' => 'Une fonction est Turing calculable si et seulement si elle est récursive.',
                'user_id' => 1,
            ),
            198 => 
            array (
                'id' => 199,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:46:17',
                'nom' => 'Groupes totalement ordonnable',
                'details' => 'Un groupe est dit totalement ordonnable s\'il peut être muni d\'un ordre total compatible avec l\'opération de groupe.

Tout groupe abélien est totalement ordonnable ssi il est sans torsion.',
                'user_id' => 1,
            ),
            199 => 
            array (
                'id' => 200,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-18 18:47:35',
                'nom' => 'Indécidabilité de la confluence et de la terminaison',
                'details' => 'On montre que le Problème de Correspondance de Post est indécidable.',
                'user_id' => 1,
            ),
            200 => 
            array (
                'id' => 201,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Algorithme de Kleene efficace',
                'details' => '',
                'user_id' => 1,
            ),
            201 => 
            array (
                'id' => 202,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-19 14:06:06',
                'nom' => 'Médiane en temps linéaire',
                'details' => 'On peut trouver la médiane d\'une liste d\'éléments non triée en temps linéaire.',
                'user_id' => 1,
            ),
            202 => 
            array (
                'id' => 203,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-19 14:09:01',
                'nom' => 'Problème du voyageur de commerce euclidien',
                'details' => 'Le problème du voyageur de commerce est NP-complet.',
                'user_id' => 1,
            ),
            203 => 
            array (
                'id' => 204,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-09-19 14:09:57',
                'nom' => 'Rationnalité du langage de pile',
                'details' => 'Pour tout automate à pile $A$, le langage $L_A$ est rationnel.',
                'user_id' => 1,
            ),
            204 => 
            array (
                'id' => 205,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Recherche des facteurs à distance d\'édition au plus k',
                'details' => '',
                'user_id' => 1,
            ),
            205 => 
            array (
                'id' => 206,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorème de Cook',
                'details' => '',
                'user_id' => 1,
            ),
            206 => 
            array (
                'id' => 207,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorie des ordres denses',
                'details' => '',
                'user_id' => 1,
            ),
            207 => 
            array (
                'id' => 208,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorème d\'Immerman-Szelepcsergi',
                'details' => '',
                'user_id' => 1,
            ),
            208 => 
            array (
                'id' => 209,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Théorème de Rice',
                'details' => '',
                'user_id' => 1,
            ),
            209 => 
            array (
                'id' => 210,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Tri par tas',
                'details' => '',
                'user_id' => 1,
            ),
            210 => 
            array (
                'id' => 211,
                'created_at' => '2016-03-17 09:33:26',
                'updated_at' => '2016-03-17 09:33:26',
                'nom' => 'Tri des suffixes',
                'details' => '',
                'user_id' => 1,
            ),
            211 => 
            array (
                'id' => 212,
                'created_at' => '2016-03-24 17:36:02',
                'updated_at' => '2017-03-01 13:29:37',
                'nom' => 'Automorphismes de Z/nZ',
                'details' => 'Si $p \\ge 3$, ...',
                'user_id' => 1,
            ),
            212 => 
            array (
                'id' => 213,
                'created_at' => '2016-03-27 20:04:43',
                'updated_at' => '2016-03-27 20:04:43',
                'nom' => 'Théorème de Glivenko-Cantelli',
                'details' => 'stats',
                'user_id' => 1,
            ),
            213 => 
            array (
                'id' => 214,
                'created_at' => '2016-03-29 15:09:46',
                'updated_at' => '2016-03-29 15:09:46',
                'nom' => 'Solution fondamentale de l\'équation des ondes',
                'details' => 'des distributions

recasages à revoir',
                'user_id' => 1,
            ),
            214 => 
            array (
                'id' => 216,
                'created_at' => '2016-04-21 11:41:29',
                'updated_at' => '2016-04-21 11:41:29',
                'nom' => 'Méthode de Gauss et polynômes orthogonaux',
                'details' => 'Sur l\'approximation de l\'intégration à poids.',
                'user_id' => 1,
            ),
            215 => 
            array (
                'id' => 217,
                'created_at' => '2016-07-04 11:07:37',
                'updated_at' => '2016-07-04 11:07:37',
                'nom' => 'Preuve de la factorielle en logique de Hoare',
                'details' => 'On illustre l\'utilisation des règles de Hoare en prouvant l\'algorithme calculant la factorielle. Ce développement peut être aussi l\'occasion également de parler des difficultés principales pour l\'automatisation des preuves utilisant ce système.',
                'user_id' => 1,
            ),
            216 => 
            array (
                'id' => 218,
                'created_at' => '2016-10-14 13:17:04',
                'updated_at' => '2016-10-14 13:19:25',
                'nom' => 'Connexité de l\'ensemble de Julia',
                'details' => 'Soit $P \\in \\mathbb{C}[X]$ de degré $\\ge 2$.
On note $P_n = P \\circ P \\circ \\cdots \\circ P$ ($n$ facteurs).
Soit $K_P = \\{ z \\in \\mathbb{C} : (P_n(z))_{n \\in \\mathbb{N}} \\text{ est bornée } \\}$.
Alors $K_P$ est non vide, compact et $\\mathbb{C} \\setminus K_P$ est connexe par arcs.',
                'user_id' => 1,
            ),
            217 => 
            array (
                'id' => 219,
                'created_at' => '2016-10-14 13:34:46',
                'updated_at' => '2016-10-14 13:34:46',
                'nom' => 'Bicommutant',
                'details' => 'Soit $E$ un $K$-espace vectoriel de dimension finie.
Soit $u \\in L(E)$. 
On note $C(u) = \\{ v \\in L(E) : u \\circ v = v \\circ u \\}$.

Alors $C(C(u)) = K[u]$. De plus $C(u) = K[u]$ ssi $u$ est cyclique.',
                'user_id' => 1,
            ),
            218 => 
            array (
                'id' => 220,
                'created_at' => '2016-10-28 17:20:29',
                'updated_at' => '2017-02-19 22:41:32',
                'nom' => 'invariants de Smith',
                'details' => '',
                'user_id' => 137,
            ),
            219 => 
            array (
                'id' => 223,
                'created_at' => '2017-01-18 20:30:44',
                'updated_at' => '2017-02-28 07:50:30',
                'nom' => 'Théorème central limite',
                'details' => 'Soit $X_n$ une suite de variables iid à valeurs dans $\\mathbb{R}$ admettant un moment d\'ordre 2. Alors
\\[Y_n=\\frac{1}{\\sqrt{n}}\\left[\\sum_{i=1}^n(X_i-\\mathbb{E}[X_i])\\right]\\overset{\\mathcal{L}}{\\longrightarrow}\\mathcal{N}(0,Var(X_1)).\\]',
                'user_id' => 156,
            ),
            220 => 
            array (
                'id' => 222,
                'created_at' => '2016-12-11 08:31:25',
                'updated_at' => '2017-02-19 22:41:07',
                'nom' => 'Polynômes irréductibles sur corps fini',
                'details' => '',
                'user_id' => 146,
            ),
            221 => 
            array (
                'id' => 224,
                'created_at' => '2017-01-31 12:22:09',
                'updated_at' => '2017-02-28 07:58:05',
                'nom' => 'Extrema liés',
            'details' => 'Soit $f,g_1,...,g_p:U\\rightarrow\\mathbb{R}$ de classe $\\mathcal{C}^1$ ($U\\subset\\mathbb{R}^n$ ouvert). Posons
\\[X=\\{\\omega\\in U : g_1(\\omega)=...=g_p(\\omega)=0\\}.\\]
Si la restriction de $f$ à $X$ admet un extremum local en $a\\in X$ et si la famille $(D_ag_i)_{1\\leq i\\leq p}$ est linéairement indépendante, alors il existe $(\\lambda_1,...,\\lambda_p)\\in\\mathbb{R}^p$ tels que 
\\[D_af=\\sum_{i=1}^p\\lambda_iD_ag_i\\]
Les $(\\lambda_i)_{1\\leq i\\leq p}$ sont appelés les multiplicateurs de Lagrange.',
                'user_id' => 1,
            ),
            222 => 
            array (
                'id' => 225,
                'created_at' => '2017-02-03 15:13:37',
                'updated_at' => '2017-02-28 08:03:06',
                'nom' => 'Equation de Hill-Mathieu',
                'details' => 'On va s\'intéresser à l\'équation différentielle suivante
\\[(E) : y\'\'+qy=0 \\textrm{ où } q:\\mathbb{R}\\rightarrow\\mathbb{R} \\textrm{ est continue, $\\pi$-périodique et paire}\\]',
                'user_id' => 156,
            ),
            223 => 
            array (
                'id' => 226,
                'created_at' => '2017-02-18 20:31:30',
                'updated_at' => '2017-02-19 22:40:01',
                'nom' => 'Nombre de zéros d\'une équation différentielle',
            'details' => 'On considère l\'équation différentielle linéaire du second ordre $(E) : y\'\'+qy=0$. On suppose que $q \\in \\mathcal{C}^1([a,+\\infty[,\\mathbb{R}^*_+)$, que $\\displaystyle\\int_{a}^{+\\infty} \\sqrt{q(u)}\\mathrm{d}u=+\\infty$ et que $q\'(x)=o_{+\\infty}(q^{3/2}(x))$. Alors pour toute solution $y$ non nulle de $(E)$, on a un équivalent à l\'infini de $N : x \\mapsto \\mathrm{Card} \\left\\lbrace u \\in [a,x]: y(u)=0 \\right\\rbrace$ donné par $N(x) \\sim \\frac{1}{\\pi} \\displaystyle\\int_{a}^x \\sqrt{q(u)} \\mathrm{d}u$.

',
                'user_id' => 140,
            ),
            224 => 
            array (
                'id' => 227,
                'created_at' => '2017-02-18 20:44:52',
                'updated_at' => '2017-03-01 13:30:52',
            'nom' => 'Etude de O(p,q)',
                'details' => 'Le groupe orthogonal de la forme quadratique sur $\\mathbb{R}^n$ représentée dans la base canonique par la matrice $I_{p,q}=\\begin{pmatrix}
I_p & 0 \\\\ 
0 & -I_q
\\end{pmatrix}$, où $p+q=n$ est noté $O(p,q)$. On a un homéomorphisme $O(p,q) \\simeq O_p(\\mathbb{R}) \\times O_q(\\mathbb{R}) \\times \\mathbb{R}^{pq}$.',
                'user_id' => 140,
            ),
            225 => 
            array (
                'id' => 228,
                'created_at' => '2017-02-19 20:01:44',
                'updated_at' => '2017-02-19 22:37:11',
                'nom' => 'Diagonalisation des opérateurs symétriques compacts',
            'details' => 'Soit $H$ un Hilbert et $T \\in \\mathcal{L}(H)$ un opérateur symétrique compact non nul. Il existe $(e_n)_{n \\in \\mathbb{N}}$ base hilbertienne de $H$ constituée de vecteurs propres de $T$. La suite des valeurs propres de $T$, notée $(\\lambda_n)_{n \\in \\mathbb{N}}$ tend vers 0 et pour tout $x \\in H$, $T(x)=\\sum\\limits_{n=0}^{+\\infty} \\lambda_n \\langle x, e_n \\rangle e_n$.',
                'user_id' => 140,
            ),
            226 => 
            array (
                'id' => 229,
                'created_at' => '2017-02-19 20:16:25',
                'updated_at' => '2017-02-19 22:36:49',
                'nom' => 'Inversion de la fonction caractéristique',
            'details' => 'Soit $\\mu$ mesure de probabilité sur $(\\mathbb{R}, \\mathcal{B}(\\mathbb{R}))$ et $\\varphi : t \\mapsto \\displaystyle\\int_{\\mathbb{R}} e^{itx} \\mathrm{d}\\mu(x)$ sa fonction caractéristique. Alors si $a < b$,

$$ \\mu(]a,b[)+\\frac{1}{2} \\mu(\\left\\{ a,b \\right\\})=\\lim\\limits_{T \\rightarrow + \\infty} \\int_{-T}^{T} \\frac{e^{-ita}-e^{-itb}}{it} \\varphi(t) \\mathrm{d}t$$',
                'user_id' => 140,
            ),
            227 => 
            array (
                'id' => 231,
                'created_at' => '2017-02-28 15:09:37',
                'updated_at' => '2017-02-28 15:10:06',
                'nom' => 'Décomposition LU et décomposition de Cholesky',
                'details' => 'On montrera dans ce développement la factorisation LU : 
Soit une matrice $A=(a_{ij})_{1\\leq i,j\\leq n}$ d\'ordre $n$ dont toutes les sous-matrices diagonales 
\\[\\Delta_k=
\\begin{pmatrix}
a_{11} & . & . & a_{1k} \\\\
. & . & . & . \\\\
. & . & . & . \\\\
a_{k1} & . & . & a_{kk} 
\\end{pmatrix}
\\]
sont inversibles. Il existe un unique couple $(L,U)$ avec $U$ triangulaire supérieure et $L$ triangulaire inférieure à diagonale unité tel que 
\\[A=LU.\\]

mais aussi la décomposition de Cholesky : 

Soit $A$ une matrice symétrique réelle définie positive. Il existe une unique matrice réelle $B$ triangulaire inférieure, telle que tous ses éléments diagonaux sont strictement positifs et qui vérifie : 
\\[A=B^tB.\\]',
                'user_id' => 156,
            ),
            228 => 
            array (
                'id' => 232,
                'created_at' => '2017-03-04 21:13:56',
                'updated_at' => '2017-03-04 21:13:56',
                'nom' => 'Théorème de Lie-Kolchin',
            'details' => 'Tout sous-groupe connexe et résoluble de $GL(n,\\mathbb{C})$ est simultanément trigonalisable.',
                'user_id' => 150,
            ),
            229 => 
            array (
                'id' => 233,
                'created_at' => '2017-03-09 15:23:31',
                'updated_at' => '2017-03-09 15:34:21',
                'nom' => 'Espace de Bergman du disque unité',
            'details' => 'L\'espace de Bergman du disque unité est $B^2( \\mathbb{D} ) := Hol(\\mathbb{D}) \\cap L^2(\\mathbb{D} )$.
$(B^2(\\mathbb{D} ) , \\|.\\|_{L^2} )$ est un espace de Hilbert.
Une base hilbertienne de cet espace est $(z \\mapsto z^n.\\sqrt{\\frac{n+1}{\\pi}})_n$.
De plus, $B^2(\\mathbb{D} )$ possède un noyau de reproduction, $K_{B^2(\\mathbb{D})}(z;w) := \\frac{1}{\\pi}.\\frac{1}{(1-\\overline{w}z)^2}$, qui vérifie :
- $\\overline{K_{B^2(\\mathbb{D})}(z;.)} \\in B^2( \\mathbb{D} )$ $\\forall$ z $\\in \\mathbb{D}$
- $f(z) = \\iint_{\\mathbb{D}} K(z;w)f(w) dxdy$, $\\forall $f$ \\in B^2(\\mathbb{D})$ $\\forall$ z $\\in \\mathbb{D}$.',
                'user_id' => 174,
            ),
            230 => 
            array (
                'id' => 234,
                'created_at' => '2017-03-09 15:41:59',
                'updated_at' => '2017-03-09 15:45:15',
                'nom' => 'Théorème de Koenigs',
            'details' => 'Soit $\\varphi : \\mathbb{D} \\to \\mathbb{D}$ holomorphe ayant un point fixe $\\alpha \\in \\mathbb{D}$, avec $\\varphi\'(\\alpha) \\neq 0$.
Alors les valeurs propres de l\'opérateur $C_{\\varphi} : \\begin{array}{ccc} & Hol(\\mathbb{D}) & \\to & Hol(\\mathbb{D}) \\\\
& f & \\mapsto & f \\circ \\varphi \\end{array} $ sont exactement les $\\varphi\'(\\alpha)^n$, $\\forall n \\geq 0$.
',
                'user_id' => 174,
            ),
            231 => 
            array (
                'id' => 235,
                'created_at' => '2017-03-14 17:27:22',
                'updated_at' => '2017-03-14 17:27:22',
                'nom' => 'Théorème de Riesz-Fischer 2',
                'details' => '',
                'user_id' => 181,
            ),
            232 => 
            array (
                'id' => 236,
                'created_at' => '2017-03-14 17:39:36',
                'updated_at' => '2017-03-14 17:39:36',
            'nom' => 'Théorème de Riesz-Fischer (bis)',
                'details' => '',
                'user_id' => 181,
            ),
            233 => 
            array (
                'id' => 237,
                'created_at' => '2017-03-14 17:43:59',
                'updated_at' => '2017-03-14 17:43:59',
                'nom' => 'L_p complet',
                'details' => '',
                'user_id' => 181,
            ),
            234 => 
            array (
                'id' => 238,
                'created_at' => '2017-03-14 17:50:02',
                'updated_at' => '2017-03-14 17:50:02',
                'nom' => 'Intégrale de Fresnel',
                'details' => '',
                'user_id' => 181,
            ),
            235 => 
            array (
                'id' => 239,
                'created_at' => '2017-03-14 18:06:07',
                'updated_at' => '2017-03-14 18:06:07',
                'nom' => 'Théorème Fourier-Plancherel',
                'details' => '',
                'user_id' => 181,
            ),
            236 => 
            array (
                'id' => 240,
                'created_at' => '2017-03-14 18:15:17',
                'updated_at' => '2017-03-14 18:15:17',
                'nom' => 'Base hilbertienne des polynômes orthogonaux',
                'details' => '',
                'user_id' => 181,
            ),
        ));
        
        
    }
}
