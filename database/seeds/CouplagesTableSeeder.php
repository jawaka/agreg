<?php

use Illuminate\Database\Seeder;

class CouplagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('couplages')->delete();
        
        \DB::table('couplages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2016-10-12 14:47:07',
                'updated_at' => '2016-10-12 14:47:07',
                'user_id' => 111,
                'public' => 0,
                'annee' => 2015,
                'option' => 4,
                'commentaires' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2016-10-12 15:34:48',
                'updated_at' => '2016-10-12 15:34:48',
                'user_id' => 108,
                'public' => 0,
                'annee' => 2016,
                'option' => 3,
                'commentaires' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2016-10-12 15:53:58',
                'updated_at' => '2016-10-12 15:53:58',
                'user_id' => 108,
                'public' => 1,
                'annee' => 2016,
                'option' => 1,
                'commentaires' => '',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2016-10-14 10:01:07',
                'updated_at' => '2016-10-14 10:01:07',
                'user_id' => 102,
                'public' => 1,
                'annee' => 2015,
                'option' => 1,
                'commentaires' => '',
            ),
            4 => 
            array (
                'id' => 6,
                'created_at' => '2016-11-05 11:59:16',
                'updated_at' => '2016-11-05 12:03:57',
                'user_id' => 138,
                'public' => 0,
                'annee' => 2016,
                'option' => 2,
                'commentaires' => '',
            ),
            5 => 
            array (
                'id' => 7,
                'created_at' => '2016-11-05 12:32:24',
                'updated_at' => '2016-11-05 12:32:24',
                'user_id' => 138,
                'public' => 0,
                'annee' => 2017,
                'option' => 2,
                'commentaires' => '',
            ),
            6 => 
            array (
                'id' => 9,
                'created_at' => '2016-11-19 10:52:52',
                'updated_at' => '2016-11-19 10:52:52',
                'user_id' => 143,
                'public' => 0,
                'annee' => 2017,
                'option' => 3,
                'commentaires' => '',
            ),
            7 => 
            array (
                'id' => 10,
                'created_at' => '2016-11-19 19:31:52',
                'updated_at' => '2016-11-19 19:31:52',
                'user_id' => 144,
                'public' => 0,
                'annee' => 2016,
                'option' => 3,
                'commentaires' => '',
            ),
            8 => 
            array (
                'id' => 12,
                'created_at' => '2016-12-11 08:27:07',
                'updated_at' => '2016-12-11 08:27:07',
                'user_id' => 146,
                'public' => 0,
                'annee' => 2016,
                'option' => 1,
                'commentaires' => '',
            ),
            9 => 
            array (
                'id' => 13,
                'created_at' => '2016-12-13 18:07:59',
                'updated_at' => '2016-12-13 18:07:59',
                'user_id' => 148,
                'public' => 0,
                'annee' => 2017,
                'option' => 1,
                'commentaires' => '',
            ),
            10 => 
            array (
                'id' => 14,
                'created_at' => '2016-12-13 19:45:44',
                'updated_at' => '2017-01-14 16:32:28',
                'user_id' => 134,
                'public' => 1,
                'annee' => 2017,
                'option' => 4,
                'commentaires' => '',
            ),
            11 => 
            array (
                'id' => 15,
                'created_at' => '2017-01-14 14:47:06',
                'updated_at' => '2017-01-14 14:47:06',
                'user_id' => 1,
                'public' => 0,
                'annee' => 2017,
                'option' => 4,
                'commentaires' => '',
            ),
            12 => 
            array (
                'id' => 16,
                'created_at' => '2017-01-21 16:47:35',
                'updated_at' => '2017-01-21 16:54:52',
                'user_id' => 157,
                'public' => 0,
                'annee' => 2017,
                'option' => 3,
                'commentaires' => '',
            ),
            13 => 
            array (
                'id' => 17,
                'created_at' => '2017-02-07 23:42:01',
                'updated_at' => '2017-02-07 23:42:01',
                'user_id' => 163,
                'public' => 0,
                'annee' => 2016,
                'option' => 1,
                'commentaires' => '',
            ),
            14 => 
            array (
                'id' => 21,
                'created_at' => '2017-03-02 13:56:24',
                'updated_at' => '2017-03-02 13:56:24',
                'user_id' => 150,
                'public' => 0,
                'annee' => 2017,
                'option' => 3,
                'commentaires' => '',
            ),
            15 => 
            array (
                'id' => 19,
                'created_at' => '2017-02-18 19:35:58',
                'updated_at' => '2017-02-18 19:35:58',
                'user_id' => 140,
                'public' => 0,
                'annee' => 2017,
                'option' => 3,
                'commentaires' => '',
            ),
            16 => 
            array (
                'id' => 20,
                'created_at' => '2017-02-27 15:57:32',
                'updated_at' => '2017-02-27 18:34:36',
                'user_id' => 158,
                'public' => 0,
                'annee' => 2017,
                'option' => 2,
                'commentaires' => '',
            ),
            17 => 
            array (
                'id' => 22,
                'created_at' => '2017-03-07 14:14:09',
                'updated_at' => '2017-03-09 18:16:12',
                'user_id' => 174,
                'public' => 1,
                'annee' => 2017,
                'option' => 3,
                'commentaires' => 'La liste de mes recasages.',
            ),
            18 => 
            array (
                'id' => 23,
                'created_at' => '2017-03-09 17:16:15',
                'updated_at' => '2017-03-09 20:58:43',
                'user_id' => 167,
                'public' => 0,
                'annee' => 2018,
                'option' => 4,
                'commentaires' => 'Les développements à trouver dans "petit guide de calcul différentiel" de François Rouvière.',
            ),
            19 => 
            array (
                'id' => 24,
                'created_at' => '2017-03-09 17:22:00',
                'updated_at' => '2017-03-09 17:27:21',
                'user_id' => 167,
                'public' => 0,
                'annee' => 2017,
                'option' => 4,
                'commentaires' => 'Développements Probas',
            ),
            20 => 
            array (
                'id' => 25,
                'created_at' => '2017-03-14 16:41:08',
                'updated_at' => '2017-03-14 17:52:13',
                'user_id' => 181,
                'public' => 1,
                'annee' => 2017,
                'option' => 1,
                'commentaires' => '',
            ),
        ));
        
        
    }
}
