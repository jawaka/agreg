<?php

use Illuminate\Database\Seeder;

class LeconsTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lecons_types')->delete();
        
        \DB::table('lecons_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nom' => 'Algèbre',
                'fnom' => 'algebre',
            ),
            1 => 
            array (
                'id' => 2,
                'nom' => 'Analyse',
                'fnom' => 'analyse',
            ),
            2 => 
            array (
                'id' => 3,
                'nom' => 'Informatique',
                'fnom' => 'informatique',
            ),
        ));
        
        
    }
}
