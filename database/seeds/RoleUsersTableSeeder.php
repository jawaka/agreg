<?php

use Illuminate\Database\Seeder;

class RoleUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_users')->delete();
        
        \DB::table('role_users')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 1,
                'created_at' => '2016-03-17 10:50:36',
                'updated_at' => '2016-03-17 10:50:36',
            ),
            1 => 
            array (
                'user_id' => 112,
                'role_id' => 2,
                'created_at' => '2016-05-16 17:54:16',
                'updated_at' => '2016-05-16 17:54:16',
            ),
            2 => 
            array (
                'user_id' => 113,
                'role_id' => 2,
                'created_at' => '2016-05-15 21:04:49',
                'updated_at' => '2016-05-15 21:04:49',
            ),
            3 => 
            array (
                'user_id' => 140,
                'role_id' => 2,
                'created_at' => '2017-02-19 18:41:36',
                'updated_at' => '2017-02-19 18:41:36',
            ),
            4 => 
            array (
                'user_id' => 156,
                'role_id' => 2,
                'created_at' => '2017-02-27 22:22:17',
                'updated_at' => '2017-02-27 22:22:17',
            ),
        ));
        
        
    }
}
