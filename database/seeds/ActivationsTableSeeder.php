<?php

use Illuminate\Database\Seeder;

class ActivationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('activations')->delete();
        
        \DB::table('activations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'code' => '5nbE8jOmjpqK2XgUWRXOU7bUD8xVxM13',
                'completed' => 1,
                'completed_at' => '2016-03-17 10:50:36',
                'created_at' => '2016-03-17 10:50:36',
                'updated_at' => '2016-03-17 10:50:36',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 112,
                'code' => 'OZGkedMxObwiazhz82bxq9VrXke5QgcB',
                'completed' => 1,
                'completed_at' => '2016-04-24 10:23:48',
                'created_at' => '2016-04-24 10:23:48',
                'updated_at' => '2016-04-24 10:23:48',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 113,
                'code' => 'Z6Tb9islKHrZ3UjcslsdeIrCXLyksVnv',
                'completed' => 1,
                'completed_at' => '2016-05-15 11:38:13',
                'created_at' => '2016-05-15 11:38:13',
                'updated_at' => '2016-05-15 11:38:13',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 102,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 103,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 104,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 105,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 106,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 107,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 108,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 109,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            11 => 
            array (
                'id' => 12,
                'user_id' => 110,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            12 => 
            array (
                'id' => 13,
                'user_id' => 111,
                'code' => '',
                'completed' => 1,
                'completed_at' => '2016-05-16 00:00:00',
                'created_at' => '2016-05-16 00:00:00',
                'updated_at' => '2016-05-16 00:00:00',
            ),
            13 => 
            array (
                'id' => 14,
                'user_id' => 114,
                'code' => 'XZi1BfW1lI6C5BfqA8hbIR0A8lgMM9LS',
                'completed' => 1,
                'completed_at' => '2016-05-19 09:29:36',
                'created_at' => '2016-05-19 09:29:36',
                'updated_at' => '2016-05-19 09:29:36',
            ),
            14 => 
            array (
                'id' => 15,
                'user_id' => 115,
                'code' => 'B9d7wzr1F46XEVZIHO9T7qaJLQYrFa0k',
                'completed' => 1,
                'completed_at' => '2016-05-23 09:00:47',
                'created_at' => '2016-05-23 09:00:47',
                'updated_at' => '2016-05-23 09:00:47',
            ),
            15 => 
            array (
                'id' => 16,
                'user_id' => 116,
                'code' => 'HMeO5Fyec3fzQshJu1b1hZ5sWja5pkqI',
                'completed' => 1,
                'completed_at' => '2016-05-23 20:48:19',
                'created_at' => '2016-05-23 20:48:19',
                'updated_at' => '2016-05-23 20:48:19',
            ),
            16 => 
            array (
                'id' => 17,
                'user_id' => 117,
                'code' => 'OftKPWAUVcNoFY0Vo7dfTLs5L3jcKrHj',
                'completed' => 1,
                'completed_at' => '2016-06-07 06:49:40',
                'created_at' => '2016-06-07 06:49:40',
                'updated_at' => '2016-06-07 06:49:40',
            ),
            17 => 
            array (
                'id' => 18,
                'user_id' => 118,
                'code' => 'wKfZH5UWKsIm6dnrg4k1qPcqvNZ8IDqn',
                'completed' => 1,
                'completed_at' => '2016-06-07 09:07:03',
                'created_at' => '2016-06-07 09:07:03',
                'updated_at' => '2016-06-07 09:07:03',
            ),
            18 => 
            array (
                'id' => 19,
                'user_id' => 119,
                'code' => 'kHaCt9MLufhf6lf9QINoHAG35feBm0ZH',
                'completed' => 1,
                'completed_at' => '2016-06-13 15:08:35',
                'created_at' => '2016-06-13 15:08:35',
                'updated_at' => '2016-06-13 15:08:35',
            ),
            19 => 
            array (
                'id' => 20,
                'user_id' => 120,
                'code' => 'mVEPqsUYRd2AKk8UJUjLQ9DbNBoQhaer',
                'completed' => 1,
                'completed_at' => '2016-06-17 15:41:37',
                'created_at' => '2016-06-17 15:41:37',
                'updated_at' => '2016-06-17 15:41:37',
            ),
            20 => 
            array (
                'id' => 21,
                'user_id' => 121,
                'code' => '5sVtbL8cptcuotXxWu0JADgjVEiZunad',
                'completed' => 1,
                'completed_at' => '2016-06-17 15:42:21',
                'created_at' => '2016-06-17 15:42:21',
                'updated_at' => '2016-06-17 15:42:21',
            ),
            21 => 
            array (
                'id' => 22,
                'user_id' => 122,
                'code' => 'ZZkvQB4QRbwPK5Gx4XGsqyqlfPppkQP9',
                'completed' => 1,
                'completed_at' => '2016-06-17 16:00:34',
                'created_at' => '2016-06-17 16:00:34',
                'updated_at' => '2016-06-17 16:00:34',
            ),
            22 => 
            array (
                'id' => 23,
                'user_id' => 123,
                'code' => 'XOs13VaWriuEPcoxc7wBe6r15lTCSl7N',
                'completed' => 1,
                'completed_at' => '2016-07-01 22:45:41',
                'created_at' => '2016-07-01 22:45:41',
                'updated_at' => '2016-07-01 22:45:41',
            ),
            23 => 
            array (
                'id' => 24,
                'user_id' => 124,
                'code' => 'SeBMNY2eUZdCGMWFCYjIHcxhzFE4Emqn',
                'completed' => 1,
                'completed_at' => '2016-07-02 09:33:13',
                'created_at' => '2016-07-02 09:33:13',
                'updated_at' => '2016-07-02 09:33:13',
            ),
            24 => 
            array (
                'id' => 25,
                'user_id' => 125,
                'code' => 'xRybJVEEriZr5Ic0EzXHrFVBEccvJZwz',
                'completed' => 1,
                'completed_at' => '2016-07-02 15:30:17',
                'created_at' => '2016-07-02 15:30:17',
                'updated_at' => '2016-07-02 15:30:17',
            ),
            25 => 
            array (
                'id' => 26,
                'user_id' => 126,
                'code' => 'NE0ZTtiF4sNIMwNCJY68vm54CqxzZloq',
                'completed' => 1,
                'completed_at' => '2016-07-04 06:52:45',
                'created_at' => '2016-07-04 06:52:45',
                'updated_at' => '2016-07-04 06:52:45',
            ),
            26 => 
            array (
                'id' => 27,
                'user_id' => 127,
                'code' => 'NVHNuNa2BIz051Puf0Np14yg2OMWMBmf',
                'completed' => 1,
                'completed_at' => '2016-07-05 09:54:16',
                'created_at' => '2016-07-05 09:54:16',
                'updated_at' => '2016-07-05 09:54:16',
            ),
            27 => 
            array (
                'id' => 28,
                'user_id' => 128,
                'code' => '78JosdwrC1LcyD2IO2jQzQszq9yhIkwN',
                'completed' => 1,
                'completed_at' => '2016-07-05 11:54:20',
                'created_at' => '2016-07-05 11:54:20',
                'updated_at' => '2016-07-05 11:54:20',
            ),
            28 => 
            array (
                'id' => 29,
                'user_id' => 129,
                'code' => '0YqAavhpQ8o5B6oh3QmLN0J90Ma98t9P',
                'completed' => 1,
                'completed_at' => '2016-07-05 22:36:07',
                'created_at' => '2016-07-05 22:36:07',
                'updated_at' => '2016-07-05 22:36:07',
            ),
            29 => 
            array (
                'id' => 30,
                'user_id' => 130,
                'code' => 'sgNQfvJJfW6EIuuNf34FKApoFsBQ2r9b',
                'completed' => 1,
                'completed_at' => '2016-07-13 09:04:47',
                'created_at' => '2016-07-13 09:04:47',
                'updated_at' => '2016-07-13 09:04:47',
            ),
            30 => 
            array (
                'id' => 31,
                'user_id' => 131,
                'code' => '3HyFgiYOT1HRuzAG8iXSAVirfHJ5DqRJ',
                'completed' => 1,
                'completed_at' => '2016-07-30 16:08:25',
                'created_at' => '2016-07-30 16:08:25',
                'updated_at' => '2016-07-30 16:08:25',
            ),
            31 => 
            array (
                'id' => 32,
                'user_id' => 132,
                'code' => 'lvsu0czh7nHQIyy6jInJs9P876ZGaXXz',
                'completed' => 1,
                'completed_at' => '2016-09-10 15:01:43',
                'created_at' => '2016-09-10 15:01:43',
                'updated_at' => '2016-09-10 15:01:43',
            ),
            32 => 
            array (
                'id' => 33,
                'user_id' => 133,
                'code' => '7HiU0oR4BWDgVT9tpl0O5DeAtWWfHZiH',
                'completed' => 1,
                'completed_at' => '2016-09-12 16:12:15',
                'created_at' => '2016-09-12 16:12:15',
                'updated_at' => '2016-09-12 16:12:15',
            ),
            33 => 
            array (
                'id' => 34,
                'user_id' => 134,
                'code' => 'LzGe73p8HreYHWplBu4fKM3L3ihgJ5b0',
                'completed' => 1,
                'completed_at' => '2016-10-08 09:26:45',
                'created_at' => '2016-10-08 09:26:45',
                'updated_at' => '2016-10-08 09:26:45',
            ),
            34 => 
            array (
                'id' => 35,
                'user_id' => 135,
                'code' => 'EGtXJL2jCQnDdeS8OyLf9h1FiSPMt8h8',
                'completed' => 1,
                'completed_at' => '2016-10-11 09:34:10',
                'created_at' => '2016-10-11 09:34:10',
                'updated_at' => '2016-10-11 09:34:10',
            ),
            35 => 
            array (
                'id' => 36,
                'user_id' => 136,
                'code' => 'dFljh6q3KipJNtfBHMRM6tkgQWFqklLN',
                'completed' => 1,
                'completed_at' => '2016-10-16 13:33:39',
                'created_at' => '2016-10-16 13:33:39',
                'updated_at' => '2016-10-16 13:33:39',
            ),
            36 => 
            array (
                'id' => 37,
                'user_id' => 137,
                'code' => 'eQDP6qEnJm6LxMcJByolGymzMjDYcCrf',
                'completed' => 1,
                'completed_at' => '2016-10-24 15:53:32',
                'created_at' => '2016-10-24 15:53:32',
                'updated_at' => '2016-10-24 15:53:32',
            ),
            37 => 
            array (
                'id' => 38,
                'user_id' => 138,
                'code' => 'ZEJZqALCmR3UXq7YGrcOBL8JAzCkd5uW',
                'completed' => 1,
                'completed_at' => '2016-10-25 11:26:31',
                'created_at' => '2016-10-25 11:26:31',
                'updated_at' => '2016-10-25 11:26:31',
            ),
            38 => 
            array (
                'id' => 39,
                'user_id' => 139,
                'code' => 'Rz3kRawSgZ8IYqO8nOrEvrKuhnjluDWZ',
                'completed' => 1,
                'completed_at' => '2016-10-29 17:41:44',
                'created_at' => '2016-10-29 17:41:44',
                'updated_at' => '2016-10-29 17:41:44',
            ),
            39 => 
            array (
                'id' => 40,
                'user_id' => 140,
                'code' => 'THqScjbfCdV5Nh9g5C1VznAn2WmthpEV',
                'completed' => 1,
                'completed_at' => '2016-11-08 13:04:21',
                'created_at' => '2016-11-08 13:04:21',
                'updated_at' => '2016-11-08 13:04:21',
            ),
            40 => 
            array (
                'id' => 41,
                'user_id' => 141,
                'code' => 'SDPSaf2hS8gVSjkCu1DfBoUVZqvTP8Dk',
                'completed' => 1,
                'completed_at' => '2016-11-09 11:31:30',
                'created_at' => '2016-11-09 11:31:30',
                'updated_at' => '2016-11-09 11:31:30',
            ),
            41 => 
            array (
                'id' => 42,
                'user_id' => 142,
                'code' => '8sFIZs1m50fMdxlMG632DxbP6rVF02CO',
                'completed' => 1,
                'completed_at' => '2016-11-09 20:30:13',
                'created_at' => '2016-11-09 20:30:13',
                'updated_at' => '2016-11-09 20:30:13',
            ),
            42 => 
            array (
                'id' => 43,
                'user_id' => 143,
                'code' => 'pzpsBwN4srVb24kybNHKjO8tqxfALbif',
                'completed' => 1,
                'completed_at' => '2016-11-19 10:51:36',
                'created_at' => '2016-11-19 10:51:36',
                'updated_at' => '2016-11-19 10:51:36',
            ),
            43 => 
            array (
                'id' => 44,
                'user_id' => 144,
                'code' => 'zmsvIXt7wkHQjuf5psGGguhxRKwXMyOB',
                'completed' => 1,
                'completed_at' => '2016-11-19 19:27:04',
                'created_at' => '2016-11-19 19:27:04',
                'updated_at' => '2016-11-19 19:27:04',
            ),
            44 => 
            array (
                'id' => 45,
                'user_id' => 145,
                'code' => '4mvXhqSEGNk4fF88nwIowCfWa0WKpVBZ',
                'completed' => 1,
                'completed_at' => '2016-12-09 21:16:12',
                'created_at' => '2016-12-09 21:16:12',
                'updated_at' => '2016-12-09 21:16:12',
            ),
            45 => 
            array (
                'id' => 46,
                'user_id' => 146,
                'code' => 'ui6AYlzOYNuAhvVnLWsYjbYAWHgfLoJE',
                'completed' => 1,
                'completed_at' => '2016-12-11 08:24:20',
                'created_at' => '2016-12-11 08:24:20',
                'updated_at' => '2016-12-11 08:24:20',
            ),
            46 => 
            array (
                'id' => 47,
                'user_id' => 147,
                'code' => 'eefqMl0B1vDJ31HDnsRinWcBsxUAiz1V',
                'completed' => 1,
                'completed_at' => '2016-12-12 23:04:32',
                'created_at' => '2016-12-12 23:04:32',
                'updated_at' => '2016-12-12 23:04:32',
            ),
            47 => 
            array (
                'id' => 48,
                'user_id' => 148,
                'code' => 'qsYpwkKsOHhIfyyQkq54caijvEtdKErD',
                'completed' => 1,
                'completed_at' => '2016-12-13 18:05:20',
                'created_at' => '2016-12-13 18:05:20',
                'updated_at' => '2016-12-13 18:05:20',
            ),
            48 => 
            array (
                'id' => 49,
                'user_id' => 149,
                'code' => 'Wxe7864LVZ2TkLadE9VYzoBBEw9Hjgzs',
                'completed' => 1,
                'completed_at' => '2016-12-14 21:49:29',
                'created_at' => '2016-12-14 21:49:29',
                'updated_at' => '2016-12-14 21:49:29',
            ),
            49 => 
            array (
                'id' => 50,
                'user_id' => 150,
                'code' => 'boIGknAmkRwSjaDhMXWkbhjX33SFe9g5',
                'completed' => 1,
                'completed_at' => '2016-12-14 21:53:14',
                'created_at' => '2016-12-14 21:53:14',
                'updated_at' => '2016-12-14 21:53:14',
            ),
            50 => 
            array (
                'id' => 51,
                'user_id' => 151,
                'code' => 'A3yEnDZ7wCcQ93usALx94WLXC0yx6cEY',
                'completed' => 1,
                'completed_at' => '2016-12-24 17:28:54',
                'created_at' => '2016-12-24 17:28:54',
                'updated_at' => '2016-12-24 17:28:54',
            ),
            51 => 
            array (
                'id' => 52,
                'user_id' => 152,
                'code' => 'bevagf2sI2UkVUms5pBpg8E7SYipnVbL',
                'completed' => 1,
                'completed_at' => '2016-12-29 13:47:49',
                'created_at' => '2016-12-29 13:47:49',
                'updated_at' => '2016-12-29 13:47:49',
            ),
            52 => 
            array (
                'id' => 53,
                'user_id' => 153,
                'code' => '9TvdzhKduYIwey7G7eJ4fjJBZxpQNh7g',
                'completed' => 1,
                'completed_at' => '2017-01-08 22:07:44',
                'created_at' => '2017-01-08 22:07:44',
                'updated_at' => '2017-01-08 22:07:44',
            ),
            53 => 
            array (
                'id' => 54,
                'user_id' => 154,
                'code' => '83B7kZgZk9o3aZYaVW2fgsALjbfKOT4A',
                'completed' => 1,
                'completed_at' => '2017-01-10 10:12:44',
                'created_at' => '2017-01-10 10:12:44',
                'updated_at' => '2017-01-10 10:12:44',
            ),
            54 => 
            array (
                'id' => 55,
                'user_id' => 155,
                'code' => '9aei7eHw8i70rdJ03fbWOCXq8dtUFJv5',
                'completed' => 1,
                'completed_at' => '2017-01-13 09:17:39',
                'created_at' => '2017-01-13 09:17:39',
                'updated_at' => '2017-01-13 09:17:39',
            ),
            55 => 
            array (
                'id' => 56,
                'user_id' => 156,
                'code' => 'fzUP2gzyFY1ls9pYRlf9wMhJyVUI0O8k',
                'completed' => 1,
                'completed_at' => '2017-01-15 11:55:21',
                'created_at' => '2017-01-15 11:55:20',
                'updated_at' => '2017-01-15 11:55:21',
            ),
            56 => 
            array (
                'id' => 57,
                'user_id' => 157,
                'code' => 'IjGKkp9ecd1NB2v2tjWDeI0gEdbAOzcy',
                'completed' => 1,
                'completed_at' => '2017-01-21 16:44:46',
                'created_at' => '2017-01-21 16:44:46',
                'updated_at' => '2017-01-21 16:44:46',
            ),
            57 => 
            array (
                'id' => 58,
                'user_id' => 158,
                'code' => 'WxYFSxw0gBZNufzvAkQyXzyAiw5QVVpN',
                'completed' => 1,
                'completed_at' => '2017-01-31 08:54:22',
                'created_at' => '2017-01-31 08:54:22',
                'updated_at' => '2017-01-31 08:54:22',
            ),
            58 => 
            array (
                'id' => 59,
                'user_id' => 159,
                'code' => 'hGbtIjkGIAS3cbRCtB782E2teHMxv2Wr',
                'completed' => 1,
                'completed_at' => '2017-02-02 15:28:21',
                'created_at' => '2017-02-02 15:28:21',
                'updated_at' => '2017-02-02 15:28:21',
            ),
            59 => 
            array (
                'id' => 60,
                'user_id' => 160,
                'code' => 'ujuzZFDo22nbVcawiOlUPV1XafJVrQZM',
                'completed' => 1,
                'completed_at' => '2017-02-04 19:07:52',
                'created_at' => '2017-02-04 19:07:52',
                'updated_at' => '2017-02-04 19:07:52',
            ),
            60 => 
            array (
                'id' => 61,
                'user_id' => 161,
                'code' => 'RfGJoOLaQGMLqaSkNkclwbAhewWiiaWu',
                'completed' => 1,
                'completed_at' => '2017-02-06 15:50:56',
                'created_at' => '2017-02-06 15:50:56',
                'updated_at' => '2017-02-06 15:50:56',
            ),
            61 => 
            array (
                'id' => 62,
                'user_id' => 162,
                'code' => 'DxXxaOJjJBhLR0yIx2jSP3P1sZH7v1gR',
                'completed' => 1,
                'completed_at' => '2017-02-07 07:03:40',
                'created_at' => '2017-02-07 07:03:40',
                'updated_at' => '2017-02-07 07:03:40',
            ),
            62 => 
            array (
                'id' => 63,
                'user_id' => 163,
                'code' => 'HfSjVnLZ5PzRAINJGY1noyPxaFalT12f',
                'completed' => 1,
                'completed_at' => '2017-02-07 23:37:12',
                'created_at' => '2017-02-07 23:37:12',
                'updated_at' => '2017-02-07 23:37:12',
            ),
            63 => 
            array (
                'id' => 64,
                'user_id' => 164,
                'code' => 'UEwYdcSf33woQkcfc0yfcBWTHUH58QFQ',
                'completed' => 1,
                'completed_at' => '2017-02-09 14:16:37',
                'created_at' => '2017-02-09 14:16:37',
                'updated_at' => '2017-02-09 14:16:37',
            ),
            64 => 
            array (
                'id' => 65,
                'user_id' => 165,
                'code' => 'ffryAVkpVxSdDvzKYu0IKUZwv8uA0eNP',
                'completed' => 1,
                'completed_at' => '2017-02-20 09:52:58',
                'created_at' => '2017-02-20 09:52:58',
                'updated_at' => '2017-02-20 09:52:58',
            ),
            65 => 
            array (
                'id' => 66,
                'user_id' => 166,
                'code' => 'ncow6hQuEWOwUOXflKQiBMhHGMDHn6rW',
                'completed' => 1,
                'completed_at' => '2017-02-23 23:24:22',
                'created_at' => '2017-02-23 23:24:22',
                'updated_at' => '2017-02-23 23:24:22',
            ),
            66 => 
            array (
                'id' => 67,
                'user_id' => 167,
                'code' => '6TpDUino9XXAuso4vuc2KfHZkQV4KZYV',
                'completed' => 1,
                'completed_at' => '2017-02-28 21:02:17',
                'created_at' => '2017-02-28 21:02:17',
                'updated_at' => '2017-02-28 21:02:17',
            ),
            67 => 
            array (
                'id' => 68,
                'user_id' => 168,
                'code' => 'trVjLJCC3DfkOXg9CMcFRKK3hjjiZ4Zf',
                'completed' => 1,
                'completed_at' => '2017-03-02 10:02:04',
                'created_at' => '2017-03-02 10:02:04',
                'updated_at' => '2017-03-02 10:02:04',
            ),
            68 => 
            array (
                'id' => 69,
                'user_id' => 169,
                'code' => 'u5NAkUQB7kZT8qIkFJCLqYHP1gqx427T',
                'completed' => 1,
                'completed_at' => '2017-03-04 09:05:40',
                'created_at' => '2017-03-04 09:05:40',
                'updated_at' => '2017-03-04 09:05:40',
            ),
            69 => 
            array (
                'id' => 70,
                'user_id' => 170,
                'code' => 'vDymSBN2ndXCzeFcoamoh5l2YAiWCos3',
                'completed' => 1,
                'completed_at' => '2017-03-05 15:17:13',
                'created_at' => '2017-03-05 15:17:13',
                'updated_at' => '2017-03-05 15:17:13',
            ),
            70 => 
            array (
                'id' => 71,
                'user_id' => 171,
                'code' => 'ZntZx9VHL7kPbuZsLIiP6cvj8QLd0nbd',
                'completed' => 1,
                'completed_at' => '2017-03-06 01:39:32',
                'created_at' => '2017-03-06 01:39:32',
                'updated_at' => '2017-03-06 01:39:32',
            ),
            71 => 
            array (
                'id' => 72,
                'user_id' => 172,
                'code' => 'TZCnJGb8L3muqTiKkSHNt9mjYS9fYZOR',
                'completed' => 1,
                'completed_at' => '2017-03-06 12:03:35',
                'created_at' => '2017-03-06 12:03:35',
                'updated_at' => '2017-03-06 12:03:35',
            ),
            72 => 
            array (
                'id' => 73,
                'user_id' => 173,
                'code' => '6ve5zWox9NTjVY30mismmpA0hNbZWErm',
                'completed' => 1,
                'completed_at' => '2017-03-06 14:18:11',
                'created_at' => '2017-03-06 14:18:11',
                'updated_at' => '2017-03-06 14:18:11',
            ),
            73 => 
            array (
                'id' => 74,
                'user_id' => 174,
                'code' => 'JSKzj0jtN6jFX0qmQNfEaoxnqAywN6PB',
                'completed' => 1,
                'completed_at' => '2017-03-07 14:11:45',
                'created_at' => '2017-03-07 14:11:45',
                'updated_at' => '2017-03-07 14:11:45',
            ),
            74 => 
            array (
                'id' => 75,
                'user_id' => 175,
                'code' => 'zekNLvmdtiQzVboRMFGk920k8vUqiUkC',
                'completed' => 1,
                'completed_at' => '2017-03-07 15:35:22',
                'created_at' => '2017-03-07 15:35:22',
                'updated_at' => '2017-03-07 15:35:22',
            ),
            75 => 
            array (
                'id' => 76,
                'user_id' => 176,
                'code' => '67O0lMk6CYjN3QVN7O5F7cQqhEXn1Wb1',
                'completed' => 1,
                'completed_at' => '2017-03-08 04:59:51',
                'created_at' => '2017-03-08 04:59:51',
                'updated_at' => '2017-03-08 04:59:51',
            ),
            76 => 
            array (
                'id' => 77,
                'user_id' => 177,
                'code' => 'c4dJVGeoahAnVOZ4YN4G2kCnF8dWuFju',
                'completed' => 1,
                'completed_at' => '2017-03-08 07:03:39',
                'created_at' => '2017-03-08 07:03:39',
                'updated_at' => '2017-03-08 07:03:39',
            ),
            77 => 
            array (
                'id' => 78,
                'user_id' => 178,
                'code' => '3F0svrf9Y9nVemVcMmJeBhAhdiByGzvC',
                'completed' => 1,
                'completed_at' => '2017-03-08 08:56:58',
                'created_at' => '2017-03-08 08:56:58',
                'updated_at' => '2017-03-08 08:56:58',
            ),
            78 => 
            array (
                'id' => 79,
                'user_id' => 179,
                'code' => '8BCw5spkMipiyp1wnLjoKCAikwI0gjmX',
                'completed' => 1,
                'completed_at' => '2017-03-08 18:41:38',
                'created_at' => '2017-03-08 18:41:38',
                'updated_at' => '2017-03-08 18:41:38',
            ),
            79 => 
            array (
                'id' => 80,
                'user_id' => 180,
                'code' => 'WSYenW5g8Xp8GX852mSYNN8lhU7dAwtv',
                'completed' => 1,
                'completed_at' => '2017-03-12 14:12:28',
                'created_at' => '2017-03-12 14:12:28',
                'updated_at' => '2017-03-12 14:12:28',
            ),
            80 => 
            array (
                'id' => 81,
                'user_id' => 181,
                'code' => 'T9Ve5353QhA81G0xIQexzXeqZz4cVXlN',
                'completed' => 1,
                'completed_at' => '2017-03-14 16:02:35',
                'created_at' => '2017-03-14 16:02:35',
                'updated_at' => '2017-03-14 16:02:35',
            ),
            81 => 
            array (
                'id' => 82,
                'user_id' => 182,
                'code' => 'Hwe75ZGNdOjYjaRon5rIlkSVxePorWKC',
                'completed' => 1,
                'completed_at' => '2017-03-15 13:54:44',
                'created_at' => '2017-03-15 13:54:44',
                'updated_at' => '2017-03-15 13:54:44',
            ),
            82 => 
            array (
                'id' => 83,
                'user_id' => 183,
                'code' => 'mpMAAyWjuuvh6u53ui1XyJRTEQ7C49cn',
                'completed' => 1,
                'completed_at' => '2017-03-15 15:31:32',
                'created_at' => '2017-03-15 15:31:32',
                'updated_at' => '2017-03-15 15:31:32',
            ),
            83 => 
            array (
                'id' => 84,
                'user_id' => 184,
                'code' => 'zniSdEQLBm6WpshDV2thcWdz6fcxL2AH',
                'completed' => 1,
                'completed_at' => '2017-03-17 10:15:34',
                'created_at' => '2017-03-17 10:15:34',
                'updated_at' => '2017-03-17 10:15:34',
            ),
            84 => 
            array (
                'id' => 85,
                'user_id' => 185,
                'code' => 'iDPXxdk5didaw7jdlXg0wygYXjVcSUyJ',
                'completed' => 1,
                'completed_at' => '2017-03-26 14:14:35',
                'created_at' => '2017-03-26 14:14:35',
                'updated_at' => '2017-03-26 14:14:35',
            ),
            85 => 
            array (
                'id' => 86,
                'user_id' => 186,
                'code' => 'JSLruvAwZa8Mdhbg912Mi5QT7xHygMcd',
                'completed' => 1,
                'completed_at' => '2017-03-28 15:22:59',
                'created_at' => '2017-03-28 15:22:59',
                'updated_at' => '2017-03-28 15:22:59',
            ),
        ));
        
        
    }
}
