<?php
namespace Agreg\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use View;
use Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'developpement' => \Agreg\Models\Developpement\Developpement::class,
            'lecon'         => \Agreg\Models\Lecon\Lecon::class,
            'reference'     => \Agreg\Models\Reference\Reference::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require base_path() . '/app/Services/UploadsManager.php';

    }
}
