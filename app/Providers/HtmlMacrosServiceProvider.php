<?php
namespace Agreg\Providers;

use Illuminate\Support\ServiceProvider;


class HtmlMacrosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        require base_path() . '/resources/macros/form.php';
        require base_path() . '/resources/macros/html.php';
    }
}
