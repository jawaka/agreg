<?php 

namespace Agreg\Extensions;

class CustomCollection extends \Illuminate\Database\Eloquent\Collection
{
    public function toRatingArray($name = "rating", $callback = false)
    {
        $result = [];
        foreach($this as $item)
        {
            if($callback === false)
            {
                if($name == "qualite")
                    $value = $item->pivot->qualite;
                else
                    $value = 1;
            }
            else
                $value = call_user_func($callback, $item);

            $result[$item->id][$name] = $value;
        }

        return $result;
    }


    public function toList()
    {
        return $this->keyBy('id')->keys();
    }


    public function toSelect($callback)
    {
        return $this->keyBy('id')->map($callback);
    }
}