<?php
namespace Agreg\Repositories\Lecon;
use       Agreg\Repositories\ResourceRepository;
use       Agreg\Repositories\Version\VersionRepository;

use Agreg\Models\Lecon\Lecon;
use Agreg\Models\Lecon\LeconCommentaire;

class LeconRepository extends ResourceRepository
{
    public function __construct(Lecon $lecon, VersionRepository $versionRepository)
    {
        $this->model = $lecon;
        $this->versionRepository = $versionRepository;
        $this->searchable = ['nom', 'rapport'];
    }

    /**
     * Get the lecons of given type.
     *
     * @return mixed
     */
    public function algebre($annee)
    {
        return $this->toArray($this->model->where('annee','=',$annee)->where('type_id','=','1')->get());
    }
    public function analyse($annee)
    {
        return $this->toArray($this->model->where('annee','=',$annee)->where('type_id','=','2')->get());
    }
    public function informatique($annee)
    {
        return $this->toArray($this->model->where('annee','=',$annee)->where('type_id','=','3')->get());
    }
    public function mathsinfo($annee)
    {
        return $this->toArray($this->model->where('annee','=',$annee)->where('type_id','!=','3')->where('options','>','8')->get());
    }


    /**
     * Get the lecons of given option and year.
     *
     * @return mixed
     */
    public function getAnneeOption($annee, $option)
    {
        return $this->model->where('annee', '=', $annee)->whereRaw('(options & 1<<'.($option - 1).') != 0')->get();
    }


    /**
     * Get the lecons in an ordered way.
     *
     * @return mixed
     */
    public function allOrdered()
    {
        return $this->model->ordered()->get();
    }



    /**
     * Get the lecons of a year grouped by type
     *
     * @return mixed
     */
    public function getAnnee($year)
    {
        return $this->model->where('annee', '=', $year)->with('type')->ordered()->get();
    }


    /**
     * Get the lecons years
     *
     * @return mixed
     */
    public function getAllAnnees()
    {
        $result = [];
        $t = array_keys($this->model->select('annee')->orderBy('annee', 'desc')->groupBy('annee')->get()->keyBy('annee')->toArray());
        foreach($t as $a)
            $result[$a] = $a;
        return $result;
    }


    /**
     * Create an array of all lecons
     *
     * @return array
     */
    public function toArray($collection = null, $callback = null)
    {
        $tmp = ($collection == null)? $this->allOrdered() : $collection;
        $b = function($l){
            return sprintf('
                    (%s) %s : %s
                ',
                $l->annee,
                $l->numero,
                $l->nom
            );
        };

        return $tmp->keyBy('id')->map( ($callback == null)? $b : $callback);
    }


    /**
     * Create a lecon.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $lecon = $this->model->create($data);
        if(isset($data['recasages']))
            $lecon->recasages()->attach($data['recasages']);

        return $lecon;
    }


    /**
     * Updates a lecon.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Lecon
     */
    public function update(array $data, $id)
    {
        $lecon = $this->find($id)->load(['recasages', 'parents', 'enfants']);

        // Recasages
        $lecon->recasages()->sync(array_key_exists('recasages', $data)? $data['recasages'] : []);

        // Links
        if(!array_key_exists('parents', $data)) $data['parents'] = [];
        if(!array_key_exists('enfants', $data)) $data['enfants'] = [];

        $linksToAdd  = $data['parents'] + $data['enfants'];
        $fRemoveLinks = function($l) use (&$linksToAdd) { unset($linksToAdd[$l->id]); };
        $lecon->parents->map($fRemoveLinks);
        $lecon->enfants->map($fRemoveLinks);


        $recasagesToAdd = [];
        $fAddRecasage = function($d) use (&$recasagesToAdd) { $recasagesToAdd[$d->id] = ['qualite' => $d->pivot->qualite]; };
        foreach($linksToAdd as $lid => $tmp)
        {
            $l = $this->find($lid)->load('recasages');
            $l->recasages->map($fAddRecasage);
        }
        $lecon->recasages()->attach($recasagesToAdd);

        $lecon->parents()->sync(array_keys($data['parents']));
        $lecon->enfants()->sync(array_keys($data['enfants']));

        // Updates
        $lecon->type()->associate($data['type_id']);
        return $lecon->update($data);
    }


    /**
     * Updates the lecon votes for a user
     *
     * @param  array  $data
     * @param  int $id
     * @param  int $user_id
     * @return Agreg\Models\Lecon
     */
    public function updateVotes(array $data, $id, $user_id)
    {
        $lecon = $this->find($id)->load('votes');

        // Detach votes of the user on this lecon
        $lecon->votesOfUser($user_id)->detach();

        // Votes
        $votes = array_key_exists('votes', $data)? $data['votes'] : [];
        if(count($votes) > 0)
        foreach($votes as &$v)
            $v['user_id'] = $user_id;

        $lecon->votes()->attach($votes);

        return $lecon;
    }


    /**
     * Delete a lecon.
     *
     * @param  int|Lecon $param
     * @return void
     */
    public function delete($param)
    {
        $lecon = ($param instanceof Lecon)? $param : $this->find($param);

        foreach ($lecon->versions as $version)
            $this->versionRepository->delete($version);
        $lecon->recasages()->detach();
        $lecon->parents()->detach();
        $lecon->enfants()->detach();
        $lecon->delete();
    }


    /**
     * Add a commentaire to a lecon.
     *
     * @param  int|Lecon $param
     * @return void
     */
    public function addCommentaire($data)
    {
        return LeconCommentaire::create($data);
    }
}
