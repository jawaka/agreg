<?php
namespace Agreg\Repositories\Lecon;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Lecon\LeconType;

class LeconTypeRepository extends ResourceRepository
{
    public function __construct(LeconType $leconType)
    {
        $this->model = $leconType;
    }

    /**
     * Create an array of all lecons types
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($l){
            return $l->nom;
        });
    }
}