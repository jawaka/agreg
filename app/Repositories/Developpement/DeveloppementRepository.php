<?php
namespace Agreg\Repositories\Developpement;
use       Agreg\Repositories\ResourceRepository;
use       Agreg\Repositories\Version\VersionRepository;

use Agreg\Models\Developpement\Developpement;
use Agreg\Models\Developpement\DeveloppementCommentaire;

class DeveloppementRepository extends ResourceRepository
{
    protected $versionRepository;

    public function __construct(Developpement $developpement, VersionRepository $versionRepository)
    {
        $this->model = $developpement;
        $this->versionRepository = $versionRepository;
    }

    /**
     * Create an array of all developpements
     *
     * @return array
     */
    public function toArray($callback = false)
    {
        if($callback === false)
            return $this->all()->keyBy('id')->map(function($d){
                    return $d->nom;
            });
        else
            return $this->all()->keyBy('id')->map($callback);
    }


    /**
     * Create a developpement.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $developpement = $this->model->create($data);
        $developpement->recasages()->attach($data['recasages']);

        return $developpement;
    }


    /**
     * Updates a developpement.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Developpement
     */
    public function update(array $data, $id)
    {
        $developpement = $this->find($id)->load('recasages');

        // Recasages
        $rec = array_key_exists('recasages', $data)? $data['recasages'] : [];
        $rec = is_array($rec)? $rec : [];
        $developpement->recasages()->sync($rec);

        // Update
        return $developpement->update($data);
    }


    /**
     * Updates the developpement votes for a user
     *
     * @param  array  $data
     * @param  int $id
     * @param  int $user_id
     * @return Agreg\Models\Developpement
     */
    public function updateVotes(array $data, $id, $user_id)
    {
        $developpement = $this->find($id)->load('votes');

        // Detach votes of the user on this developpement
        $developpement->votesOfUser($user_id)->detach();

        // Votes
        $votes = array_key_exists('votes', $data)? $data['votes'] : [];
        if(count($votes) > 0)
        foreach($votes as &$v)
            $v['user_id'] = $user_id;

        $developpement->votes()->attach($votes);

        return $developpement;
    }

    public function updateSingleVote($id, $user_id, $lecon_id, $value)
    {
        $developpement = $this->find($id);
        if($value == 0)
            return $developpement->votesOfUser($user_id)->detach($lecon_id);
        else {
            if($developpement->votesOfUser($user_id)->where('lecon_id', $lecon_id)->count())
                return $developpement->votesOfUser($user_id)->updateExistingPivot($lecon_id, ['qualite' => $value]);
            else
                return $developpement->votes()->attach([$lecon_id => ['user_id' => $user_id, 'qualite' => $value]]);
        }
    }


    /**
     * Delete a developpement.
     *
     * @param  int|Developpement $param
     * @return void
     */
    public function delete($param)
    {
        $developpement = ($param instanceof Developpement)? $param : $this->find($param);

        foreach ($developpement->versions as $version)
            $this->versionRepository->delete($version);
        $developpement->recasages()->detach();
        $developpement->delete();
    }


    /**
     * Add a commentaire to a developpement.
     *
     * @param  int|Lecon $param
     * @return void
     */
    public function addCommentaire($data)
    {
        return DeveloppementCommentaire::create($data);
    }
}
