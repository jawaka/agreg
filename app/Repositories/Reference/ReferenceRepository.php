<?php
namespace Agreg\Repositories\Reference;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Reference\Reference;

class ReferenceRepository extends ResourceRepository
{
    public function __construct(Reference $ref)
    {
        $this->model = $ref;
    }

    /**
     * Create an array of all references
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($r){
            return sprintf('
                    %s : %s
                ',
                $r->titre,
                $r->auteurs
            );
        });
    }

}