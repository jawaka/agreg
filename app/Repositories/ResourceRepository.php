<?php
namespace Agreg\Repositories;

use Agreg\Repositories\ResourceRepositoryInterface;

/**
 * Class Repository
 */
abstract class ResourceRepository implements ResourceRepositoryInterface
{
    /**
     * @var
     */
    protected $model;
    protected $searchable;

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function last($d = 1)
    {
        return $this->model->orderBy('updated_at', 'desc')->take($d)->get();
    }


    /**
     * @param string $attribute
     * @param string $operator
     * @param string $value
     * @return mixed
     */
    public function allWhere($attribute, $eq, $value)
    {
        return $this->model->where($attribute, $eq, $value)->get();
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return mixed
     */
    public function allWhereIn($attribute, $values)
    {
        return $this->model->whereIn($attribute, $values)->get();
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return mixed
     */
    public function search($value)
    {
        $query = $this->model;
        foreach($this->searchable as $col)
            $query = $query->orWhere($col, 'like', '%'.$value.'%');
        return $query->get();
    }



    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id)
    {
        return $this->model->where('id', '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }
}
?>
