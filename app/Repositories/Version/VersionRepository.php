<?php
namespace Agreg\Repositories\Version;
use       Agreg\Repositories\ResourceRepository;
use       Agreg\Repositories\Version\VersionFichierRepository;

use Agreg\Models\Version\Version;

use Agreg\Services\UploadsManager;
use Illuminate\Support\Facades\File;
use Request;
class VersionRepository extends ResourceRepository
{
    protected $manager;
    protected $versionFichierRepository;

    public function __construct(Version $version, VersionFichierRepository $versionFichierRepository)
    {
        $this->model = $version;
        $this->versionFichierRepository = $versionFichierRepository;
    }

    /**
     * Get the versions with corresponding id and clean it
     *
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        $this->check($id);
        return $this->model->find($id, $columns);
    }



    /**
     * Get the versions with associated versionable objects.
     *
     * @return mixed
     */
    public function allWith()
    {
        return $this->model->with('versionable')->with('user', 'user.roles')->get();
    }

    /**
     * Create a version.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $version = $this->model->create($data);
        $id = $version->id;

        // References
        if(!array_key_exists('references', $data))
            $data['references'] = [];
        $version->references()->sync(array_keys($data['references']));

        // Fichiers
        if(array_key_exists('version_fichiers', $data))
        {
            foreach($data['version_fichiers'] as $file)
            {
                if($file == null)
                    continue;

                // Try to upload it
                if($versionFichier = $this->versionFichierRepository->moveCreate($id, $file))
                {
                    $version->fichiers()->save($versionFichier);
                }
                else
                    return false;
            }
        }

        return $version;
    }


    /**
     * Updates a version.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Version
     */
    public function update(array $data, $id)
    {
        $version = $this->find($id)->load('references')->load('fichiers');

        // References
        if(!array_key_exists('references', $data))
            $data['references'] = [];
        $version->references()->sync(array_keys($data['references']));

        // Fichiers
        if(array_key_exists('fichiers', $data))
        foreach ($data['fichiers'] as $id => $dataFichier)
            $this->versionFichierRepository->update($dataFichier, $id);

        // Update
        return $version->update($data);
    }


    /**
     * Delete a version.
     *
     * @param  int|Version $param
     * @return void
     */
    public function delete($param)
    {
        $version = ($param instanceof Version)? $param : $this->find($param);

        foreach($version->fichiers as $fichier)
            $this->versionFichierRepository->delete($fichier);

        $version->references()->detach();
        $version->delete();
    }




    /**
     * Upload and add a fichier to a version
     *
     * @param  int $id
     * @param  file $file
     * @return void
     */
    public function uploadFile($id, $file)
    {
        $version = $this->find($id);

        // Try to upload it
        if($versionFichier = $this->versionFichierRepository->uploadCreate($id, $file))
        {
            $version->fichiers()->save($versionFichier);
            return $versionFichier;
        }
        else
            return false;
    }


    /**
     *  Detach a fichier of a version
     *
     * @param  int $id
     * @param  int $file_id
     * @return void
     */
    public function deleteFile($id, $file_id)
    {
        return $this->versionFichierRepository->delete($file_id);
    }


    /**
     *  Check if associated files really exists
     *
     * @param  int $id
     * @param  int $file_id
     * @return void
     */
    public function check($id)
    {
        $version = $this->model->find($id, ['*']);
        foreach($version->fichiers as $f)
        if(!$this->versionFichierRepository->exists($f->id))
            $this->deleteFile($id, $f->id);
    }

}
