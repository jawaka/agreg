<?php
namespace Agreg\Repositories\Version;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Version\VersionFichier;

use Agreg\Services\UploadsManager;
use Illuminate\Support\Facades\File;

class VersionFichierRepository extends ResourceRepository
{
    protected $manager;

    public function __construct(VersionFichier $versionFichier, UploadsManager $manager)
    {
        $this->model = $versionFichier;
        $this->manager = $manager;
    }


    /**
     * Upload and create a fichier
     *
     * @param  int $version_id
     * @param  file $file
     * @return void
     */
    public function uploadCreate($version_id, $file)
    {
        // Getting the file info
        $name = $file['name'];
        $path = "/versions/". $version_id ."/". $name;
        $content = File::get($file['tmp_name']);

        // Try to upload it
        if($this->manager->saveFile($path, $content, true) === true)
        {
            $data = [
                'version_id' => $version_id,
                'url' => $name
            ];
            return $this->create($data);
        }
        else
            return false;
    }


    /**
     * Move and create a fichier
     *
     * @param  int $version_id
     * @param  file $file
     * @return void
     */
    public function moveCreate($version_id, $file)
    {
        // Getting the file info
        $name = $file->getClientOriginalName();
        $dir  = "/versions/". $version_id ."/";

        $this->manager->createDirectory($dir);

        // Try to upload it
        if($file->move(public_path('uploads').$dir, $name))
        {
            $data = [
                'version_id' => $version_id,
                'url' => $name
            ];
            return $this->create($data);
        }
        else
            return false;
    }



    /**
     * Updates a version.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Version
     */
    public function update(array $data, $id)
    {
        $f = $this->find($id);

        $basepath = "/versions/". $f->version_id ."/";
        $newname = $data['url'];

        if($this->manager->moveFile($basepath.$f->url, $basepath.$newname) === true)
            return $f->update($data);
        else
            return false;

        $version = $this->find($id)->load('references')->load('fichiers');
    }


    /**
     * Delete a version file
     *
     * @param  int|fichier $param
     * @return void
     */
    public function delete($param)
    {
        $f = ($param instanceof VersionFichier)? $param : $this->find($param);

        $path = "/versions/". $f->version_id ."/". $f->url;

        if($this->manager->deleteFile($path))
        {
            $f->delete();
            return true;
        }
        else
            return false;
    }


    /**
     * Check if a file exists on the disk
     *
     * @param  int|fichier $param
     * @return void
     */
    public function exists($param)
    {
        $f = ($param instanceof VersionFichier)? $param : $this->find($param);

        $path = "/versions/". $f->version_id ."/". $f->url;

        return $this->manager->exists($path);
    }

}
