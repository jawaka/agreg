<?php
namespace Agreg\Repositories\Role;
use       Agreg\Repositories\ResourceRepository;

use Sentinel;

class RoleRepository extends ResourceRepository
{
    public function __construct()
    {
        $this->model = Sentinel::getRoleRepository()->createModel();
    }


    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id")
    {
        // Permissions
        if(!is_array($data['permissions']))
            $data['permissions'] = [];

        foreach ($data['permissions'] as $pnom => $pvalue)
            $data['permissions'][$pnom] = ($pvalue == 1);

        $data['permissions'] = json_encode($data['permissions']);


        // Update
        return $this->model->where($attribute, '=', $id)->update($data);
    }


    /**
     * Create an array of all rôles
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($t){
            return $t->name;
        });
    }
}