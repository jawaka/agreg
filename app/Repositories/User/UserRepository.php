<?php
namespace Agreg\Repositories\User;

use Sentinel;
use Agreg\Models\User\User;
use Agreg\Models\Lecon\LeconCommentaire;
use Cartalyst\Sentinel\Users\IlluminateUserRepository;
use Cartalyst\Sentinel\Hashing\BcryptHasher;

class UserRepository extends IlluminateUserRepository
{
    public function __construct()
    {
        $this->setHasher(new BcryptHasher);
        $this->setModel('Agreg\Models\User\User');
    }

    public function last($d = 1)
    {
        return $this->orderBy('created_at', 'desc')->take($d)->get();
    }

    /**
     * Updates a user.
     *
     * @param  \Cartalyst\Sentinel\Users\UserInterface|int  $user
     * @param  array  $data
     * @return \Cartalyst\Sentinel\Users\UserInterface
     */
    public function update($user, array $data)
    {
        $user = ($user instanceOf User)? $user : $this->findById($user)->load('roles');

        // Permissions
        if(!isset($data['permissions']))
            $data['permissions'] = [];

        foreach ($data['permissions'] as $pnom => $pvalue)
            $data['permissions'][$pnom] = ($pvalue == 1);

        $user->permissions = $data['permissions'];


        // Rôles
        if(!isset($data['roles']))
            $data['roles'] = [];

        $user->roles()->sync(array_keys($data['roles']));

        // Montrer email
        $data['montrer_email'] = (isset($data['montrer_email']))? true : false;

        // Update
        return $user->update($data);
    }


    /**
     * Creates a user.
     *
     * @param  array  $data
     * @param  \Closure  $callback
     * @return \Cartalyst\Sentinel\Users\UserInterface
     */
    public function createUser(array $data, Closure $callback = null)
    {
        // Creation of the user
        $credentials = [
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => $data['password'],
        ];
        $user = Sentinel::registerAndActivate($credentials);
//        $user = parent::create($data, $callback);

        // Permissions
        if(!isset($data['permissions']))
            $data['permissions'] = [];

        foreach ($data['permissions'] as $pnom => $pvalue)
            $data['permissions'][$pnom] = ($pvalue == 1);

        $user->permissions = $data['permissions'];

        // Rôles
        if(!isset($data['roles']))
            $data['roles'] = [];

        $user->roles()->sync(array_keys($data['roles']));

        return $user;
    }


    /**
     * Delete a user
     *
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        $this->findById($id)->delete();
    }


    /**
     * Create an array of all users
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($t){
            return $t->name;
        });
    }
}
