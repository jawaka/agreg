<?php
namespace Agreg\Repositories\Couplage;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Couplage\Couplage;
use Agreg\Models\Couplage\CouplageCouverture;

class CouplageRepository extends ResourceRepository
{
    public function __construct(Couplage $couplage)
    {
        $this->model = $couplage;
    }

    /**
     * Create an array of all developpements
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($d){
            return $d->id;
        });
    }


    /**
     * Create a couplage.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $couplage = $this->model->create($data);

        return $couplage;
    }


    /**
     * Updates a couplage.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Couplage
     */
    public function update(array $data, $id)
    {
        $data['public'] = array_key_exists('public', $data)? $data['public'] : 0;
        $couplage = $this->find($id)->load('developpements');

        return $couplage->update($data);
    }


    /**
     * Updates the developpements of a couplage.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Couplage
     */
    public function updateDeveloppements(array $data, $id)
    {
        $couplage = $this->find($id)->load('developpements');
        $developpements = array_key_exists('developpements', $data)? $data['developpements'] : [];
        $couplage->developpements()->sync(array_keys($developpements));
        return $couplage;
    }

    public function addDeveloppement($did, $cid)
    {
        $couplage = $this->find($cid)->load('developpements');
        if(!$couplage->developpements->contains($did))
            $couplage->developpements()->attach($did);
        return $couplage;
    }



    /**
     * (Test-)Delete the developpements of a couplage.
     *
     * @param  int $did
     * @param  int $cid
     * @return Agreg\Models\Couplage
     */
    public function testDeleteDeveloppement($did, $cid)
    {
        return CouplageCouverture::where('couplage_id', $cid)->where('developpement_id', $did)->get()->count() == 0;
    }


    public function deleteDeveloppement($did, $cid)
    {
        CouplageCouverture::where('couplage_id', $cid)->where('developpement_id', $did)->delete();
        $couplage = $this->find($cid)->load('developpements');
        if($couplage->developpements->contains($did))
            $couplage->developpements()->detach($did);
        return $couplage;
    }


    /**
     * Change the status of a lecon in a couplage.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Couplage
     */
    public function updateStatus($lid, $status, $cid)
    {
        $couplage = $this->find($cid)->load('leconsStatus');
        if($couplage->leconsStatus->keyBy('id')->has($lid))
            return $couplage->leconsStatus()->updateExistingPivot($lid, ['status' => $status]);
        else
            return $couplage->leconsStatus()->attach($lid, ['status' => $status]);
    }


    /**
     * (Un)lock the developpements in a couplage.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Couplage
     */
    public function lockDeveloppement($did, $cid)
    {
        return $this->find($cid)->developpements()->updateExistingPivot($did, ['locked' => '1']);
    }

    public function unlockDeveloppement($did, $cid)
    {
        return $this->find($cid)->developpements()->updateExistingPivot($did, ['locked' => '0']);
    }


    /**
     * (Un)cover the developpements in a lecon in a couplage.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Couplage
     */
    public function addCouverture($lid, $did, $cid)
    {
        return CouplageCouverture::create([
            'lecon_id' => $lid,
            'developpement_id' => $did,
            'couplage_id' => $cid
        ]);
    }

    public function deleteCouverture($lid, $did, $cid)
    {
        return CouplageCouverture::where('couplage_id', $cid)->where('lecon_id', $lid)->where('developpement_id', $did)->delete();
    }

    /**
     * Delete a couplage.
     *
     * @param  int|Couplage $param
     * @return void
     */
    public function delete($param)
    {
        $couplage = ($param instanceof Couplage)? $param : $this->find($param);

        $couplage->developpements()->detach();
        $couplage->delete();
    }
}
