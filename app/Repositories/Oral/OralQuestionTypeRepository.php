<?php
namespace Agreg\Repositories\Oral;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Oral\OralQuestionType;

class OralQuestionTypeRepository extends ResourceRepository
{
    public function __construct(OralQuestionType $questionType)
    {
        $this->model = $questionType;
    }

    /**
     * Create an array of all developpements
     *
     * @return array
     */
    public function toArray()
    {
        return $this->all()->keyBy('id')->map(function($d){
            return $d->nom;
        });
    }
}