<?php
namespace Agreg\Repositories\Oral;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Oral\OralRetour;
use Agreg\Models\Oral\OralRetourReponse;

use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Reference\ReferenceRepository;

class OralRetourRepository extends ResourceRepository
{
    protected $leconRepository;
    protected $developpementRepository;
    protected $referenceRepository;

    public function __construct(OralRetour $retour,
        LeconRepository $leconRepository, 
        DeveloppementRepository $developpementRepository,
        ReferenceRepository $referenceRepository)
    {
        $this->model = $retour;
        $this->leconRepository         = $leconRepository;
        $this->developpementRepository = $developpementRepository;
        $this->referenceRepository     = $referenceRepository;
    }

    public function ressources($annee)
    {
        return [
            'lecons_algebre'    => $this->leconRepository->algebre($annee),
            'lecons_analyse'    => $this->leconRepository->analyse($annee),
            'lecons_info'       => $this->leconRepository->informatique($annee),
            'lecons_maths_info' => $this->leconRepository->mathsinfo($annee),
            'developpements'    => $this->developpementRepository->toArray(),
            'references'        => $this->referenceRepository->toArray()
        ];
    }


    /**
     * Create a retour.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $data['anonyme'] = array_key_exists('anonyme', $data) && $data['anonyme'];
        return $this->model->create($data);
    }


    /**
     * Updates a retour.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Oral\OralRetour
     */
    public function update(array $data, $id)
    {
        $retour = $this->find($id);
        $data['anonyme'] = array_key_exists('anonyme', $data) && $data['anonyme'];

        if($data['questions'] != null)
        foreach($retour->oral->questions as $question)
        {
            $rdata = [
                'retour_id'   => $id,
                'question_id' => $question->id
            ];

            switch($question->type->fnom)
            {
                case 'texte':
                case 'note':
                    if(!array_key_exists($question->id, $data['questions']))
                        break;

                    OralRetourReponse::create($rdata + [
                        'reponse_texte' => $data['questions'][$question->id]
                    ]);
                    break;

                case 'lecon_analyse':
                case 'lecon_algebre':
                case 'lecon_info':
                case 'lecon_maths_info':
                case 'developpement':
                    if(!array_key_exists($question->id, $data['questions']) || empty($data['questions'][$question->id]))
                        break;

                    OralRetourReponse::create($rdata + [
                        'reponsable_id'   => $data['questions'][$question->id],
                        'reponsable_type' => ($question->type->fnom == 'developpement')? 'developpement' : 'lecon'
                    ]);
                    break;

                case 'developpement_liste':
                case 'reference_liste':
                    OralRetourReponse::where('question_id', '=', $question->id)->delete();

                    if(!array_key_exists($question->id, $data['questions']))
                        break;

                    foreach($data['questions'][$question->id] as $rid => $v)
                        OralRetourReponse::create($rdata + [
                            'reponsable_id'   => $rid,
                            'reponsable_type' => ($question->type->fnom == 'developpement_liste')? 'developpement' : 'reference'
                        ]);
                    break;
                default:
            }
        }

        if(is_array($data['reponses']))
        foreach($data['reponses'] as $rid => $v)
        {
            $reponse = OralRetourReponse::find($rid);

            if(empty($reponse->reponsable_type))
                $reponse->reponse_texte = $v;
            else
                $reponse->reponsable_id = $v;

            $reponse->save();
        }

        return $retour->update($data);
    }


    /**
     * Delete a retour.
     *
     * @param  int|OralRetour $param
     * @return void
     */
    public function delete($param)
    {
        $retour = ($param instanceof OralRetour)? $param : $this->find($param);
        OralRetourReponse::where('retour_id', '=', $retour->id)->delete();
        $retour->delete();
    }
}
