<?php
namespace Agreg\Repositories\Oral;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Oral\OralForm;
//use Agreg\Repositories\Eloquent\OralQuestionRepository;

class OralFormRepository extends ResourceRepository
{
    protected $oralQuestionRepository;
    public function __construct(OralForm $oral, OralQuestionRepository $oralQuestionRepository)
    {
        $this->model = $oral;
        $this->oralQuestionRepository = $oralQuestionRepository;
    }

    /**
     * Create an oral.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }


    /**
     * Updates a oral and its list of questions.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Developpement
     */
    public function update(array $data, $id)
    {
        $oral = $this->find($id)->load('questions');

        // Delete unwanted questions
        foreach($oral->questions as $question)
        {
            if(!in_array($question->id, $data['questionsIds']))
                $this->oralQuestionRepository->delete($question);
        }

        // Create/update remaining questions
        for($i = 0; $i < count($data['questions']); $i++)
        {
            $questionData = [
                'oraux_type_id' => $id,
                'type_id'       => $data['questionsTypes'][$i],
                'label'         => $data['questions'][$i],
                'ordre'         => $i+1
            ];

            // New question
            if($data['questionsIds'][$i] == 0)
                $question = $this->oralQuestionRepository->create($questionData);
            else
                $question = $this->oralQuestionRepository->update($questionData, $data['questionsIds'][$i]);

        }

        // Update
        return $oral->update($data);
    }


    /**
     * Delete an oral.
     *
     * @param  int|OralForm $param
     * @return void
     */
    public function delete($param)
    {
        $oral = ($param instanceof OralForm)? $param : $this->find($param);

        foreach ($oral->questions as $question)
            $this->oralQuestionRepository->delete($question);

        $oral->delete();
    }
}