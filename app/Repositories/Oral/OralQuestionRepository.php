<?php
namespace Agreg\Repositories\Oral;
use       Agreg\Repositories\ResourceRepository;

use Agreg\Models\Oral\OralQuestion;
use Agreg\Models\Oral\OralRetourReponse;

class OralQuestionRepository extends ResourceRepository
{

    public function __construct(OralQuestion $question)
    {
        $this->model = $question;
    }

    /**
     * Create a question.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }


    /**
     * Updates a question.
     *
     * @param  array  $data
     * @param  int $id
     * @return Agreg\Models\Oral\OralQuestion
     */
    public function update(array $data, $id)
    {
        $question = $this->find($id);
        if(isset($data['type_id']) && $data['type_id'] != $question->type_id)
            OralRetourReponse::where('question_id', '=', $id)->delete();
        return $question->update($data);
    }


    /**
     * Delete a question.
     *
     * @param  int|OralQuestion $param
     * @return void
     */
    public function delete($param)
    {
        $question = ($param instanceof OralQuestion)? $param : $this->find($param);

/*
        foreach ($developpement->versions as $version)
            $this->versionRepository->delete($version);
        $developpement->recasages()->detach();
*/
        $question->delete();
    }
}