<?php
namespace Agreg\Models\Reference;
use       Agreg\Models\BaseModel;


class Reference extends BaseModel {

	protected $table = 'references';
	public $timestamps = true;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre', 'auteurs', 'cote', 'ISBN'
    ];


    /**
     * Versions containing this reference.
     *
     * @var array
     */
	public function versions()
	{
		return $this->belongsToMany('Agreg\Models\Version\Version', 'versions_references');
	}

    public function versionsDeveloppements()
    {
        return $this->versions()->where('versionable_type', '=', 'developpement');
    }


    public function versionsLecons()
    {
        return $this->versions()->where('versionable_type', '=', 'lecon');
    }

    public function getDeveloppements()
    {
        $devs = collect();
        foreach ($this->versionsDeveloppements as $version) {
            $devs = $devs->merge($version->versionable_id);
        }
        return $devs->unique();
    }

    public function getLecons()
    {
        $lecons = collect();
        foreach ($this->versionsLecons as $version) {
            $lecons = $lecons->merge($version->versionable_id);
        }
        return $lecons->unique();
    }


}