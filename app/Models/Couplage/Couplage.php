<?php
namespace Agreg\Models\Couplage;
use       Agreg\Models\BaseModel;
use       Agreg\Models\Lecon\Lecon;
class Couplage extends BaseModel
{
	protected $table = 'couplages';
	public $timestamps = true;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'user_id', 'public', 'commentaires', 'annee', 'option'
    ];


    protected $with = [
        'user', 'developpements'
    ];

    public function getNomAttribute($value)
    {
        return (empty($value))? ('#'.$this->id) : $value;
    }


    /**
     * The developpements associated to the couplage
     *
     * @var array
     */
	public function developpements()
	{
		return $this->belongsToMany('Agreg\Models\Developpement\Developpement', 'couplages_developpements')->orderBy('nom')->withTimestamps()->withPivot('locked');
	}

    /**
     * On rajoute aux développements un booléen 'superflu' qui dit s'ils sont superflus et on compte le nombre de leçons dans lesquelles ils interviennent 'recasagesCount'.
     * 
     * Un dév est superflu quand il n'est recasé que dans des lecons couvertes par au moins 3 dévs.
     * 
     * Retourne un array.
     */
    public function developpementsMoreData()
	{
        // Les recasages persos
        $recasages = $this->recasages();

        // Les développements utilisés dans le couplage
        $devs = $this->belongsToMany('Agreg\Models\Developpement\Developpement', 'couplages_developpements')->orderBy('nom')->withTimestamps()->withPivot('locked')->get();

        foreach ($devs as $devId => $dev){

            // Calcule un booléen $superflu pour savoir si un dév est superflu
            $superflu = true;
            foreach($recasages as $leconId => $reca){
                if (count($reca) <= 2){
                    if (array_key_exists($dev->id, $reca)){
                        $superflu = false;
                        break;
                    }
                }
            }
            $dev->superflu = $superflu;

            // Calcule le nombre de leçons dans lesquelles intervient $dev
            // Ajoute un champ recasagesCount à $dev
            $count = 0;
            foreach( $recasages as $leconId => $reca){
                if (array_key_exists($dev->id, $reca)){
                    $count ++;
                }
            }
            $dev->recasagesCount = $count;

            // Calcule le recasage effectif
            // C'est le nombre de leçons dans lesquelles il intervient qui ont <= 2 dév
            $recaEffectif = 0;
            foreach( $recasages as $leconId => $reca){
                if (count($reca) <= 2 && array_key_exists($dev->id, $reca)){
                    $recaEffectif ++;
                }
            }
            $dev->recaEffectif = $recaEffectif;
        }

        return $devs;
    }

    



	public function hasDeveloppement($did)
	{
		return $this->developpements->keyBy('id')->has($did);
	}


	// Check if a developpement is locked in a couplage
	public function isLocked($did)
	{
		return $this->developpements->keyBy('id')->get($did)->pivot->locked;
	}

    /**
     * The user associated to the couplage
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('Agreg\Models\User\User');
    }



	/**
     * The lecons status in the couplage
     *
     * @var array
     */
	public function leconsStatus()
    {
        return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'couplages_lecons')->withPivot('status')->withTimestamps();
    }

	public function getLeconStatus($lid)
    {
        $l = $this->leconsStatus->keyBy('id');
		if($l->has($lid))
			return $l->get($lid)->pivot->status;
		else
			return 1;
    }


    /**
     * Get formatted option
     *
     * @var array
     */

    public function getFoptionAttribute()
    {
        $value = $this->option;
        $t = ['', 'A', 'B', 'C', 'D', 'Docteur'];
        return $t[$value];
    }



    /**
     * The lecons associated to the couplage
     *
     * @var array
     */
    public function lecons()
    {
        static $lecons = null;
        if($lecons == null)
            $lecons =  Lecon::where('annee', '=', $this->annee)->whereRaw('(options & 1<<'.($this->option - 1).') != 0')->orderBy('numero')->get()->keyBy('id');
        return $lecons;
    }

		public function hasLecon($lid)
		{
			return $this->lecons()->has($lid);
		}



    /**
     * The lecons associated to the couplage
     *
     * @var array
     */
    public function recasages()
    {
        static $leconsDevs = [];
        if($leconsDevs != [])
            return $leconsDevs;

        $lecons = $this->lecons();
        foreach($lecons as $l)
            $leconsDevs[$l->id] = [];

        $rec = $this->developpements()->with('recasages', 'votes')->get();
        foreach($rec as $developpement)
        {
            foreach($developpement->votesOfUser($this->user->id)->get() as $l)
            if($l->pivot->qualite > 0 && $this->hasLecon($l->id))
                $leconsDevs[$l->id][$developpement->id]['qualite'] = $l->pivot->qualite;
        }

        foreach($lecons as $l)
            uasort($leconsDevs[$l->id], function($a,$b){ return $b['qualite'] - $a['qualite']; });

        return $leconsDevs;
    }


    public function leconRecasages($lid)
    {
        $result = [];

        $rec = $this->developpements()->with('votes')->get();
        $couverture = $this->couverture->keyBy(function($item){ return $item->lecon_id."_".$item->developpement_id; });

        foreach($rec as $developpement)
        {
            foreach($developpement->votesOfUser($this->user->id)->get() as $l)
            if($l->id == $lid && $l->pivot->qualite > 0)
            {
                $result[$developpement->id]['qualite'] = $l->pivot->qualite;
                $result[$developpement->id]['checked'] = $couverture->has($lid.'_'.$developpement->id);
            }
        }

        uasort($result, function($a,$b){ return $b['qualite'] - $a['qualite']; });

        return $result;
    }


		/**
     * The inverted recasages : devs -> lecons
     *
     * @var array
     */
		public function invRecasages()
    {
				static $devsLecons = [];
				if($devsLecons != [])
					return $devsLecons;

				$leconsDevs = $this->recasages();
				foreach($leconsDevs as $lid => $rec)
				foreach($rec as $did => $info)
				{
						if(!array_key_exists($did, $devsLecons))
								$devsLecons[$did] = [];

						$devsLecons[$did][$lid]['qualite'] = $info['qualite'];
				}

				foreach($devsLecons as $did => $recasages)
            uasort($devsLecons[$did], function($a,$b){ return $b['qualite'] - $a['qualite']; });

				return $devsLecons;
    }



    /**
     * The couverture associated to the couplage
     *
     * @var array
     */
    public function couverture()
    {
        return $this->hasMany('Agreg\Models\Couplage\CouplageCouverture');
    }

		public function couvertureHas($lid, $did)
		{
				return $this->couverture->keyBy(function($item){ return $item->lecon_id."_".$item->developpement_id; })->has($lid."_".$did);
		}
}
