<?php
namespace Agreg\Models\Couplage;
use       Agreg\Models\BaseModel;

class CouplageCouverture extends BaseModel
{
    protected $table = 'couplages_couverture';
    public $timestamps = true;
    protected $fillable = ['lecon_id', 'developpement_id', 'couplage_id'];

    public function couplage()
    {
        return $this->belongsTo('Agreg\Models\Couplage\Couplage');
    }

    public function lecon()
    {
        return $this->belongsTo('Agreg\Models\Lecon\Lecon');
    }

    public function developpement()
    {
        return $this->belongsTo('Agreg\Models\Developpement\Developpement');
    }
}