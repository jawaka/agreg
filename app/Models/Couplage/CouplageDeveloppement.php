<?php
namespace Agreg\Models\Couplage;
use       Agreg\Models\BaseModel;

class CouplageDeveloppement extends BaseModel
{
    protected $table = 'couplages_developpements';
    public $timestamps = true;
    protected $fillable = ['couplage_id', 'developpement_id', 'locked'];

    public function couplage()
    {
        return $this->belongsTo('Agreg\Models\Couplage\Couplage');
    }

    public function developpement()
    {
        return $this->belongsTo('Agreg\Models\Developpement\Developpement');
    }
}