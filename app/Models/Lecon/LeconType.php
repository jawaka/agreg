<?php
namespace Agreg\Models\Lecon;
use       Agreg\Models\BaseModel;

class LeconType extends BaseModel {

	protected $table = 'lecons_types';
	public $timestamps = false;

	public function lecons()
	{
		return $this->hasMany('Agreg\Models\Lecon\Lecon');
	}

}