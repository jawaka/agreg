<?php
namespace Agreg\Models\Lecon;
use       Agreg\Models\BaseModel;

class LeconCommentaire extends BaseModel
{
    protected $table = 'lecons_commentaires';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'lecon_id', 'commentaire'
    ];


    /**
     * The user associated to the couplage
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('Agreg\Models\User\User');
    }


    /**
     * The user associated to the couplage
     *
     * @var array
     */
    public function lecon()
    {
        return $this->belongsTo('Agreg\Models\Lecon\Lecon');
    }
}