<?php
namespace Agreg\Models\Lecon;
use       Agreg\Models\BaseModel;

use Agreg\Models\Couplage\CouplageCouverture;
use Agreg\Models\Oral\OralRetour;
use Sentinel;
use DB;




class Lecon extends BaseModel
{
	protected $table = 'lecons';
	public $timestamps = true;
    protected $with = ['type'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'annee', 'numero', 'nom', 'type_id', 'rapport', 'options'
    ];


    /**
     * Mutator for options
     *
     * @var array
     */
    public function getOptionsAttribute($v)
    {
        $options = [];
        if($v & 1) $options['A'] = 1;
        if($v & 2) $options['B'] = 1;
        if($v & 4) $options['C'] = 1;
        if($v & 8) $options['D'] = 1;
        if($v & 16) $options['Docteur'] = 1;

        return $options;
    }

    public function setOptionsAttribute($v)
    {
        $this->attributes['options'] =
            1 * isset($v['A'])
          + 2 * isset($v['B'])
          + 4 * isset($v['C'])
          + 8 * isset($v['D'])
          + 16 * isset($v['Docteur']);
    }

    public function getIsDAttribute()
    {
        return isset($this->options['D']);
    }

    public function getIsDocteurAttribute()
    {
        return isset($this->options['Docteur']);
    }


    /**
     * The type of the leçon
     *
     * @var array
     */
	public function type()
	{
		return $this->belongsTo('Agreg\Models\Lecon\LeconType');
	}


    /**
     * The versions associated to this lecon
     *
     * @var array
     */
    public function versions()
    {
        return $this->morphMany('Agreg\Models\Version\Version', 'versionable');
    }



    /**
     * Get all references from versions of this Lecon and from the versions of its parents.
     * No duplicates.
     */
    public function getReferences(){
        $references = collect();

        foreach( $this->allParents() as $parentLecon){
            foreach ($parentLecon->versions as $version) {
                $references = $references->merge($version->references);
            }
        }
        
        return $references->unique('id');
    }


    /**
     * The recasages associated to this lecon with pivot column 'qualite'
     *
     * @var array
     */
	public function recasages()
	{
		return $this->belongsToMany('Agreg\Models\Developpement\Developpement', 'recasages')->withTimestamps()->withPivot('qualite');
	}

	public function recasagesOrdered()
	{
		return $this->recasages()->orderBy('pivot_qualite', 'desc');
	}

    /**
     * Return the lecon and all its ancestors in an array
     */
    public function ancetres(){
        $r = array();
        $test = array();
        array_push($test, $this);
        while ($test){
            $lecon = array_pop($test);
            array_push($r,$lecon);
            foreach($lecon->parents as $parent){
                array_push($test, $parent);
            }
        }
        return $r;
    }

    

    public function stats(){
        $ancetres = $this->ancetres();
        $ancetres_ids = array();
        foreach ($ancetres as $ancetre){
            array_push($ancetres_ids, $ancetre->id);
        }
        $votes = DB::table('recasages_votes')->get();
        $counter = [];
        foreach( $votes as $vote){
            if (in_array($vote->lecon_id, $ancetres_ids) ){
                if (array_key_exists($vote->developpement_id, $counter)){
                    $counter[$vote->developpement_id]["count"] ++ ;
                    $counter[$vote->developpement_id]["quality_sum"] += $vote->qualite;
                    $counter[$vote->developpement_id]["quality_avg"] =  number_format($counter[$vote->developpement_id]["quality_sum"] /  $counter[$vote->developpement_id]["count"], 2, ',', '');
                }else {
                    $row = DB::table('developpements')->where('id', $vote->developpement_id)->first();
                    $nom = $row->nom;
                    
                    $counter[$vote->developpement_id] =[ "id" => $vote->developpement_id, "nom" => $nom, "count" => 1, "quality_sum" => $vote->qualite, "quality_avg" => $vote->qualite, "quality_reca" => "/"];
                    $reca = DB::table('recasages')->where('developpement_id', $vote->developpement_id)->where('lecon_id', $this->id)->first();
                    if ($reca){
                        $counter[$vote->developpement_id]["quality_reca"] = $reca->qualite;
                    }
                }
            }
        }

 
        usort($counter, function($a, $b) {
            return $b["count"] -$a["count"];
        });

        return $counter;
    }

    public function couvertures()
    {

        return $this->belongsToMany('Agreg\Models\Developpement\Developpement', 'recasages_votes')->selectRaw('developpement_id,COUNT(*) AS count, AVG(qualite) AS avg')->groupBy('developpement_id')->orderBy('count','desc');
    }

    /**
     * The votes associated to this lecon with pivot column 'qualite' and 'user'
     *
     * @var array
     */
    public function votes()
    {
        return $this->belongsToMany('Agreg\Models\Developpement\Developpement', 'recasages_votes')->withTimestamps()->withPivot('qualite', 'user_id');
    }


    public function votesOfUser($user_id)
    {
        return $this->votes()->where('recasages_votes.user_id', '=', $user_id)
            ->orderBy('pivot_qualite', 'desc');
    }



    /**
     * Basic order on leçons
     *
     * @var array
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('annee', 'desc')->orderBy('numero', 'asc');
    }


    /**
     * Links between different years
     *
     * @var array
     */
    public function parents()
    {
        return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'lecons_liens', 'enfant_id', 'parent_id');
    }
    public function getRparentsAttribute($v)
    {
        $res = [];
        foreach($v as $l)
            $res[$l->id]["rating"] = 1;
        return $res;
    }


    public function enfants()
    {
        return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'lecons_liens', 'parent_id', 'enfant_id');
    }






    public function allParents()
    {
        $parents = $this->parents;
        $result = [$this];
        foreach($parents as $p)
            $result = array_merge($result, $p->allParents());

        return $result;
    }


    public function allRapports()
    {
        $parents = $this->allParents();
        $result =  array_filter(
            $parents,
            function($l){ return (!empty($l->rapport)); }
        );

        usort(
            $result,
            function($a, $b){ return $b->annee - $a->annee; }
        );

        return $result;
    }

    public function lastRapports()
    {
        $rapports = $this->allRapports();
        if(count($rapports) == 0)
            return [];

        $annee = $rapports[0]->annee;
        $result = [];
        $i = 0;
        while($i < count($rapports) && $annee == $rapports[$i]->annee)
            array_push($result, $rapports[$i++]);

        return $result;
    }

    public function otherRapports()
    {
        $rapports = $this->allRapports();
        if(count($rapports) == 0)
            return [];

        $annee = $rapports[0]->annee;
        $result = [];
        $i = 0;
        while($i < count($rapports) && $annee == $rapports[$i]->annee)
            $i++;
        while($i < count($rapports))
            array_push($result, $rapports[$i++]);

        return $result;
    }


    /**
     * The comments associated with this lecon
     *
     * @var array
     */
    public function commentaires()
    {
        return $this->hasMany('Agreg\Models\Lecon\LeconCommentaire');
    }


	/**
     * The retours associated with this lecon
     *
     * @var array
     */
    public function retours()
    {
        return $this->belongsToMany('Agreg\Models\Oral\OralRetour', 'oraux_retours_reponses', 'reponsable_id', 'retour_id')->withPivot('reponsable_type', 'question_id')->where('oraux_retours_reponses.reponsable_type', 'lecon')->whereIn('oraux_retours_reponses.question_id', [1,10,19,28]);
    }

}
