<?php
namespace Agreg\Models\User;

use Cartalyst\Sentinel\Users\EloquentUser;
use Carbon\Carbon;

class User extends EloquentUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'permissions', 'montrer_email', 'annee', 'option', 'resultat', 'classement', 'biographie'
    ];

    protected $loginNames = ['name', 'email'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getLastLoginAttribute($value)
    {
        $v = new Carbon($value);
        return $v;
    }

    public function getFoptionAttribute()
    {
        $value = $this->option;

        $t = ['Non renseigné', 'A', 'B', 'C', 'D', 'Docteur'];
        $value = ($value >= 0 && $value < 5)? $value : 0;
        return $t[$value];
    }

    public function getFresultatAttribute()
    {
        $value = $this->resultat;

        $t = ['Non renseigné', 'Non-admissible', 'Admissible', 'Admis'];
        $value = ($value >= 0 && $value < 4)? $value : 0;
        $value = $t[$value];

        if($this->resultat == 3 && !empty($this->classement))
            $value .= ", classé(e) ".$this->classement."ème";

        return $value;
    }

    /**
     * Versions of developpements and lecons of user
     *
     * @var array
     */
    public function versions()
    {
        return $this->hasMany('Agreg\Models\Version\Version', 'user_id');
    }

    public function versionsDeveloppements()
    {
        return $this->versions()->where('versionable_type', '=', 'developpement');
    }


    public function versionsLecons()
    {
        return $this->versions()->where('versionable_type', '=', 'lecon');
    }


    /**
     * Retours of user
     *
     * @var array
     */
    public function retours()
    {
        return $this->hasMany('Agreg\Models\Oral\OralRetour', 'user_id');
    }


    /**
     * Couplages of user
     *
     * @var array
     */
        public function couplage()
    {
        return $this->hasOne('Agreg\Models\Couplage\Couplage');
    }


    /**
     * Commentaires lecons of user
     *
     * @var array
     */
    public function leconsCommentaires()
    {
        return $this->hasMany('Agreg\Models\Lecon\LeconCommentaire', 'user_id');
    }

    public function hasLCommentaire($lid)
    {
        return $this->leconsCommentaires->keyBy('lecon_id')->has($lid);
    }

    public function getLCommentaire($lid)
    {
        return $this->leconsCommentaires->keyBy('lecon_id')->get($lid);
    }


    /**
     * Commentaires developpements of user
     *
     * @var array
     */
    public function developpementsCommentaires()
    {
        return $this->hasMany('Agreg\Models\Developpement\DeveloppementCommentaire', 'user_id');
    }

    public function hasDCommentaire($did)
    {
        return $this->developpementsCommentaires->keyBy('developpement_id')->has($did);
    }

    public function getDCommentaire($did)
    {
        return $this->developpementsCommentaires->keyBy('developpement_id')->get($did);
    }


    /**
     * References lecons of user
     *
     * @var array
     */
    public function leconsReferences()
    {
        return $this->belongsToMany('Agreg\Models\Reference\Reference', 'lecons_references')->withTimestamps()->withPivot('lecon_id');
    }

    public function hasLReference($lid, $rid)
    {
        $references = $this->leconsReferences->groupBy(function($i,$k){ return $i->pivot->lecon_id; });
        if($references->has($lid))
            return $references->get($lid)->keyBy('id')->has($rid);
        else
            return false;
    }


    public function syncLReferences($lid, $references)
    {
        $data = [];
        foreach($references as $r)
            $data[$r] = [
                'user_id'  => $this->id,
                'lecon_id' => $lid
            ];

        $this->leconsReferences()->wherePivot('lecon_id', $lid)->detach();
        $this->leconsReferences()->attach($data);
    }


    /**
     * References developpements of user
     *
     * @var array
     */
    public function developpementsReferences()
    {
        return $this->belongsToMany('Agreg\Models\Reference\Reference', 'developpements_references')->withTimestamps()->withPivot('developpement_id');
    }

    public function hasDReference($did, $rid)
    {
        $references = $this->developpementsReferences->groupBy(function($i,$k){ return $i->pivot->developpement_id; });
        if($references->has($did))
            return $references->get($did)->keyBy('id')->has($rid);
        else
            return false;
    }


    public function syncDReferences($did, $references)
    {
        $data = [];
        foreach($references as $r)
            $data[$r] = [
                'user_id'  => $this->id,
                'developpement_id' => $did
            ];

        $this->developpementsReferences()->wherePivot('developpement_id', $did)->detach();
        $this->developpementsReferences()->attach($data);
    }


}
