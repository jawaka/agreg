<?php
namespace Agreg\Models\Version;
use       Agreg\Models\BaseModel;

class Version extends BaseModel
{
	protected $table = 'versions';
	public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'remarque', 'versionable_id', 'versionable_type'
    ];

    protected $with = ['versionable', 'references', 'fichiers'];


	public function versionable()
	{
		return $this->morphTo();
	}

	public function fichiers()
	{
		return $this->hasMany('Agreg\Models\Version\VersionFichier');
	}

	public function references()
	{
		return $this->belongsToMany('Agreg\Models\Reference\Reference', 'versions_references');
	}

	public function user()
	{
		return $this->belongsTo('Agreg\Models\User\User', 'user_id');
	}


	public function getUrlAttribute()
	{
		return '/uploads/versions/'. $this->id .'/';
	}
}