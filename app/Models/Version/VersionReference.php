<?php
namespace Agreg\Models\Version;
use       Agreg\Models\BaseModel;

class VersionReference extends BaseModel 
{
	protected $table = 'versions_references';
	public $timestamps = true;
}