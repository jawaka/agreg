<?php
namespace Agreg\Models\Version;
use       Agreg\Models\BaseModel;

class VersionFichier extends BaseModel {

	protected $table = 'versions_fichiers';
	public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'version_id', 'url'
    ];


    /**
     * 1 : n relations
     *
     * @var array
     */
	public function version()
	{
		return $this->belongsTo('Agreg\Models\Version\Version');
	}
}