<?php
namespace Agreg\Models\Developpement;
use       Agreg\Models\BaseModel;
use       Agreg\Models\Couplage\CouplageCouverture;
use       Agreg\Models\Lecon;
use DB;
use Debugbar;

use Sentinel;

class Developpement extends BaseModel
{
	protected $table = 'developpements';
	public $timestamps = true;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'nom', 'details', 'user_id'
    ];
 

    public function user()
    {
        return $this->belongsTo('Agreg\Models\User\User', 'user_id');
    }


    /**
     * The versions associated to this developpement
     *
     * @var array
     */
	public function versions()
	{
		return $this->morphMany('Agreg\Models\Version\Version', 'versionable');
	}


     /**
     * Get all references from versions of this Lecon and from the versions of its parents.
     * No duplicates.
     */
    public function getReferences(){
        $references = collect();

        foreach ($this->versions as $version) {
            $references = $references->merge($version->references);
        }
        
        return $references->unique('id');
    }


    /**
     * The recasages associated to this developpement with pivot column 'qualite'
     *
     * @var array
     */
	public function recasages()
	{
		return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'recasages')->withTimestamps()->withPivot('qualite');
	}

	public function recasagesOrdered()
	{
		return $this->recasages()
			->orderBy('annee', 'desc')
            //->orderBy('numero', 'asc');
            ->orderBy('qualite', 'desc');
	}


    // /*
    // * Return the number of times that the developpement is chosen in a couplage
    // * Note: I think it is bad to use DB::table, and it should use $this.belongsToMany ...
    // */
    // public function utilisation(){
    //     $couplages_developpements = DB::table('couplages_developpements')->get();
    //     $count = 0;
    //     foreach( $couplages_developpements as $use){
    //         if ($use->developpement_id == $this->id){
    //             $count ++;
    //         }
    //     }
    //     return $count;
    // }


    public function diffAutresDevs(){
        $annee = config('app.annee');
        $devs = DB::table('developpements')
            ->join('recasages', 'developpements.id', '=', 'recasages.developpement_id')
            ->join('lecons', 'recasages.lecon_id', '=', 'lecons.id')
            ->where('lecons.annee', '=', 2017)
            ->select('*')
            ->get();
        return $devs;
    } 

    /*
Table couplages_couverture; dit juste si un développement est coché ou pas dans une lecon
(Pas de rapport avec locké qui est géré dans couplages_dev et qui dépend pas de la lecon)


mysql> DESCRIBE couplages_developpements;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int unsigned | NO   | PRI | NULL    | auto_increment |
| couplage_id      | int unsigned | NO   | MUL | NULL    |                |
| developpement_id | int unsigned | NO   | MUL | NULL    |                |
| locked           | tinyint(1)   | NO   |     | 0       |                |
+------------------+--------------+------+-----+---------+----------------+

mysql> DESCRIBE recasages_votes;
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| id               | int unsigned | NO   | PRI | NULL    | auto_increment |
| lecon_id         | int unsigned | NO   | MUL | NULL    |                |
| developpement_id | int unsigned | NO   | MUL | NULL    |                |
| qualite          | int          | NO   |     | NULL    |                |
| user_id          | int unsigned | NO   | MUL | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+

    */

    public function recasagesUtilisateurs(){
        // \Debugbar::debug($this->id);


        // On ne garde que les lecons qui sont dans les recasages de ce dev
        $lecons = DB::table('lecons')
            ->select('lecons.id', 'lecons.numero', 'lecons.nom', 'lecons.annee')
            ->pluck(null, 'id');


        // Pour chaque leçon on associe le descendant le plus jeune (le leçon héritière la plus actuelle)
        $derniereLecon = array();
        $leconsLiens = DB::table('lecons_liens')->orderBy('enfant_id', 'desc')->get();

        // normalement toute lecon n'a qu'un enfant au plus et cet enfant devrait être d'id supérieur
        // enfin au moins d'année supérieure stricte
        foreach ($leconsLiens as $leconsLien) {
            $parent_id = $leconsLien->parent_id;
            $enfant_id = $leconsLien->enfant_id;

            if (isset($derniereLecon[$enfant_id])){
                $derniereLecon[$parent_id] = $derniereLecon[$enfant_id];
            } else {
                $derniereLecon[$parent_id] = $enfant_id;
            }
        }

        // Debugbar::debug($derniereLecon);



        $recasagesVotes = DB::select("
            SELECT recasages_votes.*
            FROM recasages_votes
            JOIN couplages_developpements ON recasages_votes.developpement_id = couplages_developpements.developpement_id
                AND recasages_votes.user_id = (
                    SELECT user_id
                    FROM couplages
                    WHERE couplages.id = couplages_developpements.couplage_id
                )
            WHERE couplages_developpements.developpement_id = :developpementId
        ", ['developpementId' => $this->id]);

        // Calcule des pré résultats
        // C'est compliqué car un utilisateur peut avoir plusieurs qualité pour une même graine de leçon (genre la 633 est parent de 775 mais c'est la même)
        // Donc on fait la moyenne pour chaque utilisateur pour chaque graine
        $results = array();
        foreach ($recasagesVotes as $reca){
            // \Debugbar::debug($reca->id);
            $derId = $reca->lecon_id;
            if (isset($derniereLecon[$reca->lecon_id])){
                $derId = $derniereLecon[$reca->lecon_id];
            }
            if (isset($results[$derId])){
                if (isset($results[$derId][$reca->user_id])){
                    $results[$derId][$reca->user_id][0] += $reca->qualite;
                    $results[$derId][$reca->user_id][1] += 1;
                } else {
                    $results[$derId][$reca->user_id] = [$reca->qualite, 1];
                }
                
            } else {
                $results[$derId] = array();
                $results[$derId][$reca->user_id] = [$reca->qualite, 1];
            }
        }

        // \Debugbar::debug($results);

        // Les recasages officiels
        $recasagesOffRaw = DB::table('recasages')
            ->where('recasages.developpement_id', $this->id)
            ->get();
        $recasagesOff = array();
        foreach($recasagesOffRaw as $reca){
            $recasagesOff[$reca->lecon_id] = $reca->qualite;
        }


        $results2 = array();
        foreach($results as $leconId => $result){

            $results2[$leconId] = [0,0, $leconId, $lecons[$leconId]->numero, $lecons[$leconId]->nom, $lecons[$leconId]->annee, "/"];
            if (isset($recasagesOff[$leconId])){
                $results2[$leconId][6] = $recasagesOff[$leconId];
            }
            
            foreach($result as $userId => $sumNnumber){
                $results2[$leconId][0] += $sumNnumber[0]/$sumNnumber[1];
            }
            $results2[$leconId][0] /= count($result);
            $results2[$leconId][1] = count($result);
        }

        // \Debugbar::debug($results2);

        // Tri décroissant selon le nombre d'utilisations de chaque graine de leçon
        usort($results2, function($a, $b) {
            return $b[1] - $a[1];
        });

        return $results2;
    }



    /**
     * OBSOLETE : a été remplacé par recasagesUtilisateurs()
     * Ya un problème parce que des leçons peuvent avoir le même nom
     */
    public function couvertures()
    {
        return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'recasages_votes')->selectRaw('numero,COUNT(*) AS count, AVG(qualite) AS avg')->groupBy('numero')->orderBy('count','desc');
    }



    /**
     * The votes associated to this developpement with pivot column 'qualite' and 'user'
     *
     * @var array
     */
    public function votes()
    {
        return $this->belongsToMany('Agreg\Models\Lecon\Lecon', 'recasages_votes')->withTimestamps()->withPivot('qualite', 'user_id');
    }


    public function votesOfUser($user_id)
    {
        return $this->votes()->wherePivot('user_id', $user_id)
            ->orderBy('annee', 'desc')
//            ->orderBy('numero', 'asc');
            ->orderBy('qualite', 'desc');
    }


    public function votesOfCUser()
    {
        $user = Sentinel::check();
        return $this->votesOfUser($user->id);
    }

}