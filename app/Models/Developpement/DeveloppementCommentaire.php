<?php
namespace Agreg\Models\Developpement;
use       Agreg\Models\BaseModel;

class DeveloppementCommentaire extends BaseModel
{
    protected $table = 'developpements_commentaires';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'developpement_id', 'commentaire'
    ];


    /**
     * The user associated to the commentaire
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('Agreg\Models\User\User');
    }


    /**
     * The developpement associated to the commentaire
     *
     * @var array
     */
    public function developpement()
    {
        return $this->belongsTo('Agreg\Models\Developpement\Developpement');
    }
}
