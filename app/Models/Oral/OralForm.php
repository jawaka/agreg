<?php
namespace Agreg\Models\Oral;
use       Agreg\Models\BaseModel;

class OralForm extends BaseModel
{
	protected $table = 'oraux_types';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'nom', 'fnom'
    ];
 

    /**
     * The questions associated to this form
     *
     * @var array
     */
	public function questions()
	{
		return $this->hasMany('Agreg\Models\Oral\OralQuestion', 'oraux_type_id')->orderBy('ordre', 'asc');
	}
}