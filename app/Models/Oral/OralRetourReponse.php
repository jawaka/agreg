<?php
namespace Agreg\Models\Oral;
use       Agreg\Models\BaseModel;

class OralRetourReponse extends BaseModel
{
	protected $table = 'oraux_retours_reponses';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'retour_id', 'question_id', 'reponse_texte', 'reponsable_id', 'reponsable_type'
    ];
 

    /**
     * Question associated with this answer
     *
     * @var OralQuestion
     */
    public function question()
    {
        return $this->belongsTo('Agreg\Models\Oral\OralQuestion', 'question_id');
    }


    /**
     * Reference to reponse
     *
     * @var mixed
     */
    public function reponsable()
    {
        return $this->morphTo();
    }
}