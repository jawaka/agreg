<?php
namespace Agreg\Models\Oral;
use       Agreg\Models\BaseModel;

class OralQuestion extends BaseModel
{
	protected $table = 'oraux_questions';
	public $timestamps = false;
    protected $with = ['type'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'oraux_type_id', 'type_id', 'label', 'ordre'
    ];
 

    /**
     * The oral form associated to this question
     *
     * @var array
     */
	public function oral()
	{
		return $this->belongsTo('Agreg\Models\Oral\OralForm', 'oraux_type_id');
	}


    /**
     * The type of question
     *
     * @var array
     */
    public function type()
    {
        return $this->belongsTo('Agreg\Models\Oral\OralQuestionType', 'type_id');
    }
}