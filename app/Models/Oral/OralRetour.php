<?php
namespace Agreg\Models\Oral;
use       Agreg\Models\BaseModel;

class OralRetour extends BaseModel
{
	protected $table = 'oraux_retours';
	public $timestamps = true;
    protected $with = ['oral'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'oraux_type_id', 'user_id', 'annee', 'anonyme'
    ];
 

    /**
     * Type of form of this retour
     *
     * @var OralForm
     */
    public function oral()
    {
        return $this->belongsTo('Agreg\Models\Oral\OralForm', 'oraux_type_id');
    }


    /**
     * Owner of the retour
     *
     * @var User
     */
    public function user()
    {
        return $this->belongsTo('Agreg\Models\User\User', 'user_id');
    }


    /**
     * Associated answer
     *
     * @array OralRetourReponse
     */
    public function reponses()
    {
        return $this->hasMany('Agreg\Models\Oral\OralRetourReponse', 'retour_id');
    }
}