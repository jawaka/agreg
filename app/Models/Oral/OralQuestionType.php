<?php
namespace Agreg\Models\Oral;
use       Agreg\Models\BaseModel;

class OralQuestionType extends BaseModel
{
	protected $table = 'oraux_questions_types';
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'nom', 'fnom'
    ];
 

    /**
     * Questions of this type
     *
     * @var array
     */
    public function questions()
    {
        return $this->hasMany('Agreg\Models\Oral\OralQuestion', 'type_id');
    }
}