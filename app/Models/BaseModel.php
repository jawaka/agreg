<?php
namespace Agreg\Models;

use Illuminate\Database\Eloquent\Model;
use Agreg\Extensions\CustomCollection;
use Sentinel;


class BaseModel extends Model 
{
    public function newCollection(array $models = array())
    {
        return new CustomCollection($models);
    }

    public function getIsOwnerAttribute()
    {
        if($user = Sentinel::check())
            return ($this->user_id == $user->id);
        else
            return false;
    }
}