<?php
namespace Agreg\Models\Recasage;
use       Agreg\Models\BaseModel;

class Recasage extends BaseModel
{
	protected $table = 'recasages';
	public $timestamps = true;

	public function lecon()
	{
		return $this->belongsTo('Agreg\Models\Lecon\Lecon');
	}

	public function developpement()
	{
		return $this->belongsTo('Agreg\Models\Developpement\Developpement');
	}
}