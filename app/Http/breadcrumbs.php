<?php

// Home
BC::register('home', function($bc) {
    $bc->push('Accueil', route('home'));
});



/*############################
######### Admin panel ########
############################*/
// Index
BC::register('admin.index', function($bc) {
    $bc->parent('home');
    $bc->push('Admin', route('admin.index'));
});


/*
 * Developpement breadcrumbs
 */
BC::register('admin.developpements.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Developpements', route('admin.developpements.index'));
});

BC::register('admin.developpements.show', function($bc, $developpement)
{
    $bc->parent('admin.developpements.index');
    $bc->push($developpement->nom, route('admin.developpements.show', $developpement->id));
});

BC::register('admin.developpements.edit', function($bc, $developpement)
{
    $bc->parent('admin.developpements.show', $developpement);
    $bc->push('Modification', route('admin.developpements.edit', $developpement->id));
});

BC::register('admin.developpements.create', function($bc)
{
    $bc->parent('admin.developpements.index');
    $bc->push('Création', route('admin.developpements.create'));
});

BC::register('admin.developpements.versions.create', function($bc, $versionable)
{
    $bc->parent('admin.developpements.show', $versionable);
    $bc->push("Création d'une version", route('admin.developpements.versions.create', $versionable->id));
});



/*
 * Lecon breadcrumbs
 */
BC::register('admin.lecons.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Lecons', route('admin.lecons.index'));
});

BC::register('admin.lecons.index.annee', function($bc, $annee) {
    $bc->parent('admin.lecons.index');
    $bc->push($annee, route('admin.lecons.index.annee', $annee));
});


BC::register('admin.lecons.show', function($bc, $lecon)
{
    $text = $lecon->numero ." : ". $lecon->nom;
    $bc->parent('admin.lecons.index.annee', $lecon->annee);
    $bc->push($text, route('admin.lecons.show', $lecon->id));
});

BC::register('admin.lecons.edit', function($bc, $lecon)
{
    $bc->parent('admin.lecons.show', $lecon);
    $bc->push('Modification', route('admin.lecons.edit', $lecon->id));
});

BC::register('admin.lecons.create', function($bc)
{
    $bc->parent('admin.lecons.index');
    $bc->push('Création', route('admin.lecons.create'));
});

BC::register('admin.lecons.versions.create', function($bc, $versionable)
{
    $bc->parent('admin.lecons.show', $versionable);
    $bc->push("Création d'une version", route('admin.lecons.versions.create', $versionable->id));
});



/*
 * Version breadcrumbs
 */
BC::register('admin.versions.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Versions', route('admin.versions.index'));
});

BC::register('admin.versions.show', function($bc, $version)
{
    if($version->versionable instanceof Agreg\Models\Lecon\Lecon)
        $bc->parent('admin.lecons.show', $version->versionable);
    else
        $bc->parent('admin.developpements.show', $version->versionable);

    $bc->push('Version de '.$version->user->name, route('admin.versions.show', $version->id));
});

BC::register('admin.versions.edit', function($bc, $version)
{
    $bc->parent('admin.versions.show', $version);
    $bc->push('Modification', route('admin.versions.edit', $version->id));
});



/*
 * Reference breadcrumbs
 */
BC::register('admin.references.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('References', route('admin.references.index'));
});

BC::register('admin.references.show', function($bc, $reference)
{
    $bc->parent('admin.references.index');
    $bc->push($reference->titre, route('admin.references.show', $reference->id));
});

BC::register('admin.references.edit', function($bc, $reference)
{
    $bc->parent('admin.references.show', $reference);
    $bc->push('Modification', route('admin.references.edit', $reference->id));
});

BC::register('admin.references.create', function($bc)
{
    $bc->parent('admin.references.index');
    $bc->push('Création', route('admin.references.create'));
});


/*
 * User breadcrumbs
 */
BC::register('admin.users.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Membres', route('admin.users.index'));
});

BC::register('admin.users.show', function($bc, $user)
{
    $bc->parent('admin.users.index');
    $bc->push($user->name, route('admin.users.show', $user->id));
});

BC::register('admin.users.edit', function($bc, $user)
{
    $bc->parent('admin.users.show', $user);
    $bc->push('Modification', route('admin.users.edit', $user->id));
});

BC::register('admin.users.create', function($bc)
{
    $bc->parent('admin.users.index');
    $bc->push('Création', route('admin.users.create'));
});


/*
 * Rôle breadcrumbs
 */
BC::register('admin.roles.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Rôles', route('admin.roles.index'));
});

BC::register('admin.roles.show', function($bc, $role)
{
    $bc->parent('admin.roles.index');
    $bc->push($role->name, route('admin.roles.show', $role->id));
});

BC::register('admin.roles.edit', function($bc, $role)
{
    $bc->parent('admin.roles.show', $role);
    $bc->push('Modification', route('admin.roles.edit', $role->id));
});

BC::register('admin.roles.create', function($bc)
{
    $bc->parent('admin.roles.index');
    $bc->push('Création', route('admin.roles.create'));
});



/*
 * Oral breadcrumbs
 */
BC::register('admin.oraux.index', function($bc) {
    $bc->parent('admin.index');
    $bc->push('Oraux', route('admin.oraux.index'));
});

BC::register('admin.oraux.show', function($bc, $oral)
{
    $bc->parent('admin.oraux.index');
    $bc->push($oral->nom, route('admin.oraux.show', $oral->id));
});

BC::register('admin.oraux.edit', function($bc, $oral)
{
    $bc->parent('admin.oraux.show', $oral);
    $bc->push('Modification', route('admin.oraux.edit', $oral->id));
});

BC::register('admin.oraux.create', function($bc)
{
    $bc->parent('admin.oraux.index');
    $bc->push('Création', route('admin.oraux.create'));
});



/*
 * Retour breadcrumbs
 */
BC::register('admin.retours.index', function($bc) {
    $bc->parent('admin.oraux.index');
    $bc->push('Retours', route('admin.retours.index'));
});

BC::register('admin.retours.show', function($bc, $retour)
{
    $bc->parent('admin.retours.index');
    $bc->push('Retour de '.$retour->user->name, route('admin.retours.show', $retour->id));
});

BC::register('admin.retours.edit', function($bc, $retour)
{
    $bc->parent('admin.retours.show', $retour);
    $bc->push('Modification', route('admin.retours.edit', $retour->id));
});

BC::register('admin.retours.create', function($bc)
{
    $bc->parent('admin.retours.index');
    $bc->push('Création', route('admin.retours.create'));
});


// Outils
BC::register('admin.outils.doublon.index', function($bc)
{
    $bc->parent('admin.index');
    $bc->push('Gérer doublon', route('admin.outils.doublon.index'));
});
