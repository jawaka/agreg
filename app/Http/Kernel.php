<?php

namespace Agreg\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
//            \Agreg\Http\Middleware\EncryptCookies::class,
            \Agreg\Http\Middleware\VerifyCsrfToken::class,
            \Agreg\Http\Middleware\HttpsProtocol::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'throttle'   => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'auth'       => \Agreg\Http\Middleware\SentinelAuth::class,
        'guest'      => \Agreg\Http\Middleware\SentinelGuest::class,

        'setTheme'   => \igaster\laravelTheme\Middleware\setTheme::class,
        'loadTheme'  => \Agreg\Http\Middleware\LoadTheme::class,
        'admin'  => \Agreg\Http\Middleware\Admin::class,
    ];
}
