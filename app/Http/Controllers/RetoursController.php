<?php 
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Oral\OralRetourCreateRequest;
use Agreg\Http\Requests\Oral\OralRetourUpdateRequest;
use Agreg\Http\Requests\Oral\OralRetourDeleteRequest;
use Agreg\Repositories\Oral\OralFormRepository;
use Agreg\Repositories\Oral\OralRetourRepository;
use Agreg\Repositories\Oral\OralQuestionTypeRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Request;
use Sentinel;

class RetoursController extends Controller
{
    protected $oralFormRepository;
    protected $oralRetourRepository;

    public function __construct(OralFormRepository $oralFormRepository, OralRetourRepository $oralRetourRepository)
    {
        $this->oralFormRepository   = $oralFormRepository;
        $this->oralRetourRepository = $oralRetourRepository;
    }

   /**
    * Display a listing of the resources
    *
    * @return Response
    */
    public function index()
    {
        $oraux = $this->oralFormRepository->all();
        $retours = $this->oralRetourRepository->allWhere('annee', '=', config('app.annee'));
        return view('retours.index', compact('oraux', 'retours'));
    }


   /**
    * Display a listing of the resources of a given type
    *
    * @return Response
    */
    public function indexOral($oral)
    {
        $oral    = $this->oralFormRepository->allWhere('fnom','=',$oral)->first();
        $oraux   = $this->oralFormRepository->all();
        $retours = $this->oralRetourRepository->allWhere('oraux_type_id', '=', $oral->id);

        return view('retours.index-oral', compact('oral', 'oraux', 'retours'));
    }

   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        $oraux = $this->oralFormRepository->all()->keyBy('id')->map(function($t){
            return $t->nom;
        });

        return view('retours.create', compact('oraux'));
    }
    

   /**
    * Show the remaining form for creating a new resource.
    *
    * @return Response
    */
    public function create2()
    {
        $retourAnnee = Request::input('annee');
        $retourIsAnonyme = Request::input('anonyme');
        $oral        = $this->oralFormRepository->find(Request::input('oraux_type_id'));
        $ressources  = $this->oralRetourRepository->ressources($retourAnnee);

        return view('retours.create2', compact('oral', 'ressources', 'retourAnnee', 'retourIsAnonyme'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(OralRetourCreateRequest $request)
    {
        // Create basis
        $user = Sentinel::check();
        $data = $request->only('oraux_type_id', 'annee', 'anonyme');
        $data['user_id'] = $user->id;
        $retour = $this->oralRetourRepository->create($data);

        // Add reponses
        $data2 = $request->only('anonyme','questions', 'reponses');
        $retour2 = $this->oralRetourRepository->update($data2, $retour->id);

        return redirect(route('retours.index-oral', $retour->oral->fnom))
            ->withOk("Le retour a été créé avec succès.");
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $user   = Sentinel::check();
        $retour = $this->oralRetourRepository->find($id)->load('reponses');
        if(!$user->hasAccess('oral.retour.update') && $user->id != $retour->user_id)
            return redirect(route('retours.index'))
                ->withError("Vous n'avez pas les permissions pour modifier ce retour");

        $reponses   = $retour->reponses->groupBy('question_id');
        $ressources = $this->oralRetourRepository->ressources($retour->annee);

        return view('retours.edit',  compact('retour', 'reponses', 'ressources'));
    }

   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(OralRetourUpdateRequest $request, $id)
    {
        $data = $request->only('anonyme', 'questions', 'reponses');
        $data['anonyme'] = $request->has('anonyme');

        // comportement très étrange du champ anonyme de la modification
        // si décoché, alors request[anonyme] existe pas
        // si coché, alors request[anonyme] vaut 0
        // il faut donc le transformer en booléen
        
        $this->oralRetourRepository->update($data, $id);
        $retour = $this->oralRetourRepository->find($id)->load('reponses');

        return redirect(route('retours.index-oral', $retour->oral->fnom))
            ->withOk("Le retour a été modifié.");
    }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    *
    public function destroy(OralRetourDeleteRequest $request, $id)
    {
        $this->oralRetourRepository->delete($id);

        return redirect(route('admin.oral.index'))->withOk("Le retour a été supprimé.");;
    }
    */
}