<?php
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Developpement\DeveloppementCreateRequest;
use Agreg\Http\Requests\Developpement\DeveloppementUpdateRequest;
use Agreg\Http\Requests\Developpement\DeveloppementDeleteRequest;
use Agreg\Http\Requests\Developpement\DeveloppementUpdateVotesRequest;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Sentinel;

class DeveloppementsController extends Controller
{
    protected $developpementRepository;

    public function __construct(DeveloppementRepository $developpementRepository)
    {
        $this->developpementRepository = $developpementRepository;
        $this->middleware('auth', ['except' => ['index', 'show', 'kiviat', 'kiviatData']]);
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $developpements = $this->developpementRepository->all();
        return view('developpements.index', compact('developpements'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create(LeconRepository $leconRepository, UserRepository $userRepository)
    {
        $lecons = $leconRepository->toArray();
        $users = $userRepository->toArray();

        return view('developpements.create', compact('lecons', 'users'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(DeveloppementCreateRequest $request)
    {
        $data = $request->only('nom', 'details', 'recasages');
        $user = Sentinel::check();
        $data['user_id'] = $user->id;

        $developpement = $this->developpementRepository->create($data);

//        return redirect(route('developpements.show', $developpement->id))
//            ->withOk("Le développement " . $developpement->nom . " a été créée.");
        return redirect(route('versions.create.developpement', $developpement->id))
            ->withOk("Le développement " . $developpement->nom . " a été créée. Vous pouvez maintenant ajouter votre version de ce développement (facultatif).");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id, LeconRepository $leconRepository, ReferenceRepository $referenceRepository)
    {
        $developpement   = $this->developpementRepository->find($id);
        $recasagesByYear = $developpement->recasagesOrdered->groupBy('annee');
        $lecons          = $leconRepository->toArray();
        $references      = $referenceRepository->toArray();

        $couplage = getCouplage();
        if($couplage && $couplage->hasDeveloppement($id)){
            $recasages = $developpement->votesOfCUser()->where('annee', $couplage->annee)->get()->keyBy('id');
            return view('developpements.show',  compact('developpement', 'recasagesByYear', 'lecons', 'couplage', 'recasages', 'references'));
        }
        else
            return view('developpements.show',  compact('developpement', 'recasagesByYear', 'lecons', 'references'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, LeconRepository $leconRepository, UserRepository $userRepository)
    {
        $developpement = $this->developpementRepository->find($id);
        $lecons = $leconRepository->toArray();
        $users = $userRepository->toArray();

        $user = Sentinel::check();
        if(!$user->hasAccess('developpement.update') && $user->id != $developpement->user_id)
            return redirect(route('developpements.show', $developpement->id))
                ->withError("Vous n'avez pas les droits nécessaires pour modifier ce développement.");

        return view('developpements.edit',  compact('developpement', 'lecons', 'users'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(DeveloppementUpdateRequest $request, $id)
    {
        if($id != $request->input('id'))
            return redirect(route('developpements.index'));

        $this->developpementRepository->update($request->only('nom', 'details', 'recasages'), $id);

        return redirect(route('developpements.show', $id))
            ->withOk("Le développement " . $request->input('nom') . " a été modifié avec succès.");
    }


   /**
    * Update the votes of a user
    *
    * @param  int  $id
    * @return Response
    */
    public function updateVotes(DeveloppementUpdateVotesRequest $request, $id)
    {
        if(!($id == $request->input('id') && $user = Sentinel::check()))
            return redirect(route('developpements.index'));

        $this->developpementRepository->updateVotes($request->only('votes'), $id, $user->id);

        return redirect(route('developpements.show', $id))
            ->withOk("Vos recasages pour ce développement ont été mis à jour.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(DeveloppementDeleteRequest $request, $id)
    {
        // Check existence of version
        $developpement = $this->developpementRepository->find($id);
        if($developpement == null)
            return redirect(route('developpements.index'))
                ->withError("Ce développement n'existe pas.");

        // Check permissions to delete
        $user = Sentinel::check();
        if(!$user->hasAccess('developpements.delete') && $user->id != $developpement->user_id)
            return redirect(route('developpements.show', $developpement->id))
                ->withError("Vous n'avez pas les droits nécessaires pour supprimer ce développement.");

        $this->developpementRepository->delete($id);

        return redirect(route('developpements.index'))
            ->withOk("Le développement a été supprimé.");;
    }


   /**
    * Display a kiviat diagram.
    *
    * @return Response
    */
    public function kiviat($annee, LeconRepository $leconRepository)
    {
        $developpements = $this->developpementRepository->all();
        $lecons = $leconRepository->allWhere('annee', '=', $annee);

        return view('developpements.kiviat', compact('developpements', 'lecons', 'annee'));
    }

   /**
    * Return json object of data for the kiviat diagram.
    *
    * @return Response
    */
    public function kiviatData($annee, LeconRepository $leconRepository)
    {
        $lecons = $leconRepository->allWhere('annee', '=', $annee)->load('recasages');
        $obj = [];
        foreach ($lecons as $lecon)
        {
            $obj[$lecon->numero] = [
                "nom" => $lecon->nom,
                "recasages" => []
            ];
            foreach ($lecon->recasages as $dev)
                array_push($obj[$lecon->numero]["recasages"], [
                    "developpement" => $dev->id,
                    "qualite" => $dev->pivot->qualite
                ]);
        }

        return response()->json($obj);
    }
}
