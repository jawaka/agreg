<?php 
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Version\VersionCreateRequest;
use Agreg\Http\Requests\Version\VersionUpdateRequest;
use Agreg\Http\Requests\Version\VersionDeleteRequest;
use Agreg\Repositories\Version\VersionRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Sentinel;

class VersionsController extends Controller
{
    protected $versionRepository;
    protected $referenceRepository;

    public function __construct(VersionRepository $versionRepository, ReferenceRepository $referenceRepository)
    {
        $this->versionRepository   = $versionRepository;
        $this->referenceRepository = $referenceRepository;
    }


   /**
    * Show the form for creating a new version for a developpement
    *
    * @return Response
    */
    public function createForDeveloppement($id, DeveloppementRepository $developpementRepository)
    {
        $versionable = $developpementRepository->find($id);

        return $this->create($versionable);
    }

   /**
    * Show the form for creating a new version for a lecon
    *
    * @return Response
    */
    public function createForLecon($id, LeconRepository $leconRepository)
    {
        $versionable = $leconRepository->find($id);

        return $this->create($versionable);
    }

   /**
    * Show the form for creating a new version
    *
    * @return Response
    */
    public function create($versionable)
    {
        $references = $this->referenceRepository->toArray();
        $type = (get_class($versionable) == "Agreg\Models\Developpement\Developpement")? "developpement" : "lecon";

        return view('versions.create', compact('versionable', 'references', 'type'));
    }



   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(VersionCreateRequest $request)
    {
        $data = $request->except(['_method', '_token']);
        $user = Sentinel::check();
        $data['user_id'] = $user->id;

        $version = $this->versionRepository->create($data);
        return redirect(route($request->input('versionable_type') . 's.show', $request->input('versionable_id')))
            ->withOk('Votre version a été créée avec succès.');
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        // Check existence of version
        $version = $this->versionRepository->find($id);
        if($version == null)
            return redirect('/')
                ->withError("Cette version n'existe pas.");

        // Check permissions to edit
        $user = Sentinel::check();
        if(!$user->hasAccess('version.update') && $user->id != $version->user_id)
            return redirect(route($version->versionable_type . 's.show', $version->versionable_id))
                ->withError("Vous n'avez pas les droits nécessaires pour modifier cette version.");


        // Deal with form
        $references = $this->referenceRepository->toArray();
        $type       = (get_class($version->versionable) == "Agreg\Models\Developpement\Developpement")? "developpement" : "lecon";

        return view('versions.edit',  compact('version', 'references', 'type'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(VersionUpdateRequest $request, $id)
    {
        // Check existence of version
        $version = $this->versionRepository->find($id);
        if($version == null)
            return redirect('/')
                ->withError("Cette version n'existe pas.");

        // Check permissions to edit
        $user = Sentinel::check();
        if(!$user->hasAccess('version.update') && $user->id != $version->user_id)
            return redirect(route($version->versionable_type . 's.show', $version->versionable_id))
                ->withError("Vous n'avez pas les droits nécessaires pour modifier cette version.");


        // Deal with update
        $this->versionRepository->update($request->except(['_method', '_token']), $id);

        $version = $this->versionRepository->find($id);
        return redirect(route($version->versionable_type . 's.show', $version->versionable_id))
            ->withOk("La version a été modifiée avec succès.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(VersionDeleteRequest $request, $id)
    {
        // Check existence of version
        $version = $this->versionRepository->find($id);
        if($version == null)
            return redirect('/')
                ->withError("Cette version n'existe pas.");

        // Check permissions to delete
        $user = Sentinel::check();
        if(!$user->hasAccess('version.delete') && $user->id != $version->user_id)
            return redirect(route($version->versionable_type . 's.show', $version->versionable_id))
                ->withError("Vous n'avez pas les droits nécessaires pour supprimer cette version.");


        // Deal with deletion
        $url = route($version->versionable_type.'s.show', $version->versionable->id);
        $this->versionRepository->delete($id);

        return redirect($url)->withOk("La version a été supprimée avec succès.");;
    }




   /**
    * Upload a file and attach it to the version
    *
    * @param  int  $id
    * @return Response
    */
    public function uploadFile($id)
    {
        if(!Sentinel::check())
            return response()->json(['error' => "Non autorisé."]);

        // Check existence of version
        $version = $this->versionRepository->find($id);
        if($version == null)
            return response()->json(['error' => "Cette version n'existe pas."]);

        // Check permissions to upload
        $user = Sentinel::check();
        if(!$user->hasAccess('version.update') && $user->id != $version->user_id)
            return response()->json(['error' => "Vous n'avez pas les droits nécessaires pour uploader ce fichier."]);


        // Deal with upload
        if($f = $this->versionRepository->uploadFile($id, $_FILES['version_fichier']))
        {
            return response()->json([
                'id'  => $f->id,
                'url' => $f->url
            ]);
        }
        else
            return response()->json(['error' => "Erreur lors de l'envoi du fichier."]);
    }

    /**
    * Delete a file and detach it from the version
    *
    * @param  int  $id
    * @return Response
    */
    public function deleteFile($id)
    {
        if(!Sentinel::check())
            return response()->json(['error' => "Non autorisé."]);

        // Check existence of version
        $version = $this->versionRepository->find($id);
        if($version == null)
            return response()->json(['error' => "Cette version n'existe pas."]);

        // Check permissions to upload
        $user = Sentinel::check();
        if(!$user->hasAccess('version.update') && $user->id != $version->user_id)
            return response()->json(['error' => "Vous n'avez pas les droits nécessaires pour supprimer ce fichier."]);


        // Deal with deletion
        if($this->versionRepository->deleteFile($id, $_POST["fichier_id"]))
            return response()->json([]);
        else
            return response()->json(['error' => "Erreur lors de la suppression du fichier."]);
    }

}