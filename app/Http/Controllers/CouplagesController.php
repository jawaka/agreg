<?php
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Couplage\CouplageCreateRequest;
use Agreg\Http\Requests\Couplage\CouplageUpdateRequest;
use Agreg\Http\Requests\Couplage\CouplageDeleteRequest;
use Agreg\Http\Requests\Couplage\CouplageUpdateDeveloppementsRequest;
use Agreg\Http\Requests\Couplage\CouplageAddDeveloppementRequest;
use Agreg\Http\Requests\Couplage\CouplageAjaxRequest;
use Agreg\Repositories\Couplage\CouplageRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Sentinel;
use Debugbar;
use Request;
use PDF;
use DB;

class CouplagesController extends Controller
{
    protected $couplageRepository;

    public function __construct(CouplageRepository $couplageRepository,
                                DeveloppementRepository $developpementRepository,
                                LeconRepository $leconRepository,
                                ReferenceRepository $referenceRepository)
    {
        $this->couplageRepository      = $couplageRepository;
        $this->developpementRepository = $developpementRepository;
        $this->leconRepository         = $leconRepository;
        $this->referenceRepository     = $referenceRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    *
    public function index()
    {
        $developpements = $this->couplageRepository->all();
        return view('developpements.index', compact('developpements'));
    }
    */


   /**
    * Display the couplage of user
    *
    * @return Response
    */
    public function indexUser()
    {
        $user = Sentinel::check();

        if(is_null($user->couplage)){
            $annees   = $this->leconRepository->getAllAnnees();
            return view('couplages.create', compact('user', 'annees'));
        }

        $couplage       = $user->couplage;
        $lecons         = $couplage->lecons()->keyBy('id');
        $couverture     = $couplage->couverture->keyBy(function($item){ return $item->lecon_id."_".$item->developpement_id; });
        $recasages      = $couplage->recasages();
        $developpements = $this->developpementRepository->toArray();
        $references     = $this->referenceRepository->toArray();
        $dcommentaires  = $user->developpementsCommentaires()->with('developpement')->get()
                            ->reject(function($d, $k) use ($couplage) {
                                return $couplage->developpements->keyBy('id')->has($d->developpement->id);
                            })->reject(function($d, $k){ return empty($d->commentaire); });
        return view('couplages.index-user', compact('user', 'couplage', 'lecons', 'developpements', 'recasages', 'couverture', 'dcommentaires', 'references'));
    }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(CouplageCreateRequest $request)
    {
        $user = Sentinel::check();
        $data = $request->all();
        $data['user_id'] = $user->id;

        $couplage = $this->couplageRepository->create($data);

        return redirect(route('couplages.index.user'))
            ->withOk("Votre couplage a été activé. Vous pouvez maintenant ajouter des développements.");
    }


   /**
    * Ajax queries.
    *
    * @return Response
    */
    public function ajaxGetRecasages(CouplageAjaxRequest $request)
    {
        $user   = Sentinel::check();
        $annee  = $user->couplage->annee;
        $option = $user->couplage->option;
        $type   = $request->input('type');
        $id     = $request->input('id');

        $lecons        = $this->leconRepository->toArray($this->leconRepository->getAnneeOption($annee, $option), function($l){ return $l->numero." : ".$l->nom; });
        $developpement = $this->developpementRepository->find($id);
        $recasages     = $developpement->votesOfUser($user->id)->where('annee', $annee)->get();
        if($recasages->isEmpty())
            $recasages = $developpement->recasagesOrdered()->where('annee', $annee)->get();


        if($type == "add" || $type == "addFromDev" || $type == "couverture")
        {
            if($user->couplage->developpements->keyBy('id')->has($developpement->id))
                return view('couplages.recasages-new-edit', compact('lecons', 'recasages', 'developpement'));
            else
                return view('couplages.recasages-new', compact('lecons', 'recasages', 'developpement'));
        }
        else
            return view('couplages.recasages-edit', compact('lecons', 'recasages', 'developpement'));
    }


    public function ajaxDeveloppement(CouplageAjaxRequest $request)
    {
        $user = Sentinel::check();
        $action = $request->action;

        if($action == "add")
            $this->couplageRepository->addDeveloppement($request->input('id'), $user->couplage->id);

        if($action == "add" || $action == "edit")
        {
            $this->developpementRepository->updateVotes($request->only('votes'), $request->input('id'), $user->id);
            return $request->only('votes');
        }


        if($action == "testDelete")
            return response()->json(['safe' => $this->couplageRepository->testDeleteDeveloppement($request->input('id'), $user->couplage->id)]);
        if($action == "delete")
            $this->couplageRepository->deleteDeveloppement($request->input('id'), $user->couplage->id);


        if($action == "lock")
            $this->couplageRepository->lockDeveloppement($request->input('id'), $user->couplage->id);
        if($action == "unlock")
            $this->couplageRepository->unlockDeveloppement($request->input('id'), $user->couplage->id);


        if($action == "rating")
            return $this->developpementRepository->updateSingleVote($request->input('id'), $user->id, $request->input('lecon-id'), $request->input('value'));
    }

 
    /**
     * Retourne les developpements du couplage avec les données additionnelles (superflu, recasagesCount)
     */
    public function ajaxGetDevsMoreData(CouplageAjaxRequest $request)
    {

        $user = Sentinel::check();
        return $user->couplage->developpementsMoreData();
    }

    public function ajaxCouverture(CouplageAjaxRequest $request)
    {
        $user = Sentinel::check();
        if($request->action == 'add')
            return $this->couplageRepository->addCouverture($request->lecon_id, $request->developpement_id, $user->couplage->id);
        if($request->action == 'delete')
            return $this->couplageRepository->deleteCouverture($request->lecon_id, $request->developpement_id, $user->couplage->id);

        if($request->action == 'status')
            $this->couplageRepository->updateStatus($request->lecon_id, $request->status, $user->couplage->id);
    }




    public function ajaxCommentaires(CouplageAjaxRequest $request)
    {
        $user = Sentinel::check();

        if($request->input('type') == 'generaux')
        {
            $user->couplage->update(['commentaires' => $request->input('value')]);
            return $request->input('value');
        }

        if($request->input('type') == 'lecon')
        {
            $id = $request->input('id');
            $data = [
                'lecon_id' => $id,
                'user_id'  => $user->id,
                'commentaire' => $request->input('value')
            ];

            if($lc = $user->getLCommentaire($id))
                $lc->update($data);
            else
                $this->leconRepository->addCommentaire($data);
        }

        if($request->input('type') == 'developpement')
        {
            $id = $request->input('id');
            $data = [
                'developpement_id' => $id,
                'user_id'  => $user->id,
                'commentaire' => $request->input('value')
            ];

            if($lc = $user->getDCommentaire($id))
                $lc->update($data);
            else
                $this->developpementRepository->addCommentaire($data);
        }
    }


    /**
     * Calcul 3 développements qui pourraient remplacer le développement demandé.
     * Sachant qu'il faut couvrir au moins les leçons critiques du dév demandé.
     * C'est-à-dire les leçons dans lesquelles il intervient où le nombre de dévs dedans est <= 2.
     */
    public function ajaxGetPropoRemplaUn(CouplageAjaxRequest $request){
        $start = microtime(true);
        \Debugbar::debug("propo rempla simple");

        $user = Sentinel::check();
        $lecons = $user->couplage->lecons();
        $recasages = $user->couplage->recasages();
        $couplageDevs = $user->couplage->developpementsMoreData(); // Liste
        $allRawDevs = $this->developpementRepository->all()->load('recasages');

        $devId = $request->input('devId');

        // calculer les leçons couv(lecon) <= 2 où il intervient
        $leconsAcouvrir = array();
        foreach($recasages as $leconId => $reca){
            if (count($reca) <= 2 && array_key_exists($devId, $reca)){
                array_push($leconsAcouvrir, $leconId);
            }
        }
        
        \Debugbar::debug($leconsAcouvrir);

        $coDevs = array(); // liste de tous les dévs qui couvrent les $leconsAcouvrir selon recasages officiels
        foreach( $allRawDevs as $rawDev){
            // S'il est déjà dedans on passe
            $dansCouplage = false;
            foreach( $couplageDevs as $dev2){
                if ($dev2->id == $rawDev->id){
                    $dansCouplage = true;
                    break;
                }
            }
            if ($dansCouplage){
                continue;
            }

            // S'il ne couvre pas les leçons qu'il faut couvrir, on passe
            $isCovering = true;
            foreach ( $leconsAcouvrir as $leconId){
                $found = false;
                foreach( $rawDev->recasages as $reca){
                    if ($leconId == $reca->id){
                        $found = true;
                        break;
                    }
                }
                if ($found == false){
                    $isCovering = false;
                }
            }
            if ($isCovering){
                $newDev = $rawDev;
                // calcul du recasage effectif de $newDev en supposant $dev enlevé du couplage
                $recaEffectif = 0;
                foreach($recasages as $leconId => $reca){
                    $found = false;
                    foreach( $rawDev->recasages as $recaOff){
                        if ($leconId == $recaOff->id){
                            $found = true;
                            break;
                        }
                    }
                    if ($found){
                        $offset = array_key_exists($devId, $reca) ? 1 : 0;
                        if (count($reca) - $offset <= 1 ){
                            $recaEffectif ++;
                        }
                    }
                }
                $newDev->recaEffectif = $recaEffectif;
                array_push($coDevs, $newDev);
            }
        }
        usort($coDevs, function($a, $b) {
            return $b->recaEffectif - $a->recaEffectif;
        });
        $coDevs = array_map(function($item) {
            return [
                'id' => $item['id'],
                'nom' => $item['nom'],
                'recaEffectif' => $item['recaEffectif'],
            ];
        }, $coDevs);
        $coDevs = array_slice($coDevs, 0,3);
        \Debugbar::debug($coDevs);

        $leconsCritiques = array();
        foreach($leconsAcouvrir as $leconId){
            array_push($leconsCritiques, $lecons[$leconId]->numero);
        }

        $retour = (object)[];
        $retour->propositionDevs = $coDevs;
        $retour->leconsCritiques = $leconsCritiques;


        \Debugbar::debug(microtime(true) - $start);
        \Debugbar::debug("fin");
        return json_encode($retour);
    }





    public function ajaxGetPropoRempla(CouplageAjaxRequest $request){

        $start = microtime(true);
        \Debugbar::debug("propo rempla");

        $user = Sentinel::check();
        $lecons = $user->couplage->lecons();
        $recasages = $user->couplage->recasages();
        $couplageDevs = $user->couplage->developpementsMoreData(); // Liste
        $allRawDevs = $this->developpementRepository->all()->load('recasages');

        $retour = array();

        // pour chacun des dévs du couplage
        foreach( $couplageDevs as $dev){
            // calculer les leçons couv(lecon) <= 2 où il intervient
            $leconsAcouvrir = array();
            foreach($recasages as $leconId => $reca){
                if (count($reca) <= 2 && array_key_exists($dev->id, $reca)){
                    array_push($leconsAcouvrir, $leconId);
                }
            }
            
            if (count($leconsAcouvrir) == 0){
                continue;
            }
            \Debugbar::debug(sprintf("-------\ndev %d %s", $dev->id, $dev->nom));
            \Debugbar::debug($leconsAcouvrir);

            $coDevs = array(); // liste de tous les dévs qui couvrent les $leconsAcouvrir selon recasages officiels
            foreach( $allRawDevs as $rawDev){
                // S'il est déjà dedans on passe
                $dansCouplage = false;
                foreach( $couplageDevs as $dev2){
                    if ($dev2->id == $rawDev->id){
                        $dansCouplage = true;
                        break;
                    }
                }
                if ($dansCouplage){
                    continue;
                }

                // S'il ne couvre pas les leçons qu'il faut couvrir, on passe
                $isCovering = true;
                foreach ( $leconsAcouvrir as $leconId){
                    $found = false;
                    foreach( $rawDev->recasages as $reca){
                        if ($leconId == $reca->id){
                            $found = true;
                            break;
                        }
                    }
                    if ($found == false){
                        $isCovering = false;
                    }
                }
                if ($isCovering){
                    $newDev = $rawDev;
                    // calcul du recasage effectif de $newDev en supposant $dev enlevé du couplage
                    $recaEffectif = 0;
                    foreach($recasages as $leconId => $reca){
                        $found = false;
                        foreach( $rawDev->recasages as $recaOff){
                            if ($leconId == $recaOff->id){
                                $found = true;
                                break;
                            }
                        }
                        if ($found){
                            $offset = array_key_exists($dev->id, $reca) ? 1 : 0;
                            if (count($reca) - $offset <= 1 ){
                                $recaEffectif ++;
                            }
                        }
                    }
                    $newDev->recaEffectif = $recaEffectif;
                    array_push($coDevs, $newDev);
                }
            }
            usort($coDevs, function($a, $b) {
                return $b->recaEffectif - $a->recaEffectif;
            });
            $coDevs = array_map(function($item) {
                return [
                    'id' => $item['id'],
                    'nom' => $item['nom'],
                    'recaEffectif' => $item['recaEffectif'],
                ];
            }, $coDevs);
            $coDevs = array_slice($coDevs, 0,3);
            \Debugbar::debug($coDevs);

            $leconsCritiques = array();
            foreach($leconsAcouvrir as $leconId){
                // \Debugbar::debug($lecons[$leconId]);
                array_push($leconsCritiques, $lecons[$leconId]->numero);
            }

            $retour[$dev->id] = (object)[];
            $retour[$dev->id]->propositionDevs = $coDevs;
            $retour[$dev->id]->leconsCritiques = $leconsCritiques;
        }


        \Debugbar::debug(microtime(true) - $start);
        \Debugbar::debug("fin");
        return $retour;
    }



    public function ajaxReferences(CouplageAjaxRequest $request)
    {
        $user = Sentinel::check();

        $id = $request->input('id');
        $references = $request->input('value');

        if($request->input('type') == 'lecon')
            $user->syncLReferences($id, $references);

        if($request->input('type') == 'developpement')
            $user->syncDReferences($id, $references);
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    *
    */
    public function show($id, LeconRepository $leconRepository, DeveloppementRepository $developpementRepository)
    {
        $couplage       = $this->couplageRepository->find($id);
        $lecons         = $leconRepository->getAnneeOption($couplage->annee, $couplage->option);
        $developpements = $developpementRepository->toArray(function($d){
            return '<a href="'.route('developpements.show', $d->id). '">'. $d->nom .'</a>';
        });

        // Fetch the developpements
        $leconsDevs = [];
        foreach($lecons as $l)
            $leconsDevs[$l->id] = [];

        foreach($couplage->developpements as $developpement)
        {
            // User recasages or standard recasages ?
            if($couplage->votes)
                $r = $developpement->votesOfUser($couplage->user_id)->get();
            else
                $r = $developpement->recasages;

            // Fetch them
            foreach($developpement->recasages as $l)
            {
                if($l->pivot->qualite > 0)
                if(array_key_exists($l->id, $leconsDevs))
                    array_push($leconsDevs[$l->id], [$l->pivot->qualite, $developpement]);
            }
        }

        foreach($lecons as $l)
            usort($leconsDevs[$l->id], function($a,$b){ return $b[0] - $a[0]; });


        return view('couplages.show',  compact('couplage', 'lecons', 'leconsDevs', 'developpements'));
    }
    

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $couplage = $this->couplageRepository->find($id);
        $annees   = $this->leconRepository->getAllAnnees();

        return view('couplages.edit',  compact('couplage', 'annees'));
    }


   /**
    * Update the specified resource in storage.
    * TODO : utiliser DB c'est mal, il faudrait utiliser la structure de laravel
    * Il y a quasi 3x le même code, c'est mal.
    * Ca double les données dans la BD. Il faudrait peut être supprimer les anciennces données.
    * Problème : des données sur des leçons perdues vont etre perdues.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(CouplageUpdateRequest $request, $id)
    {
        if($id != $request->input('id'))
            return redirect(route('couplages.index.user'));

        $couplage = DB::table('couplages')->where('id',$id)->first();
        $user_id = $couplage->user_id;
        echo "user_id : " . $user_id . "<br>"; // echos are for debugging
        echo "annee : " . $couplage->annee . "<br>";


        $lecons_liens = DB::table('lecons_liens')->get(); 
        // -------------
        // Transfer couplages_lecons
        $couplages_lecons = DB::table('couplages_lecons')->where('couplage_id', $id)->get();
        $nouveautes = [];
        foreach ($couplages_lecons as $status){
            $lecon = DB::table('lecons')->where('id', $status->lecon_id)->first();
            if ($request->annee == $lecon->annee){
                continue; // ne pas déplacer ce statut à une autre année vu que c'est déjà la bonne
            }

            // get the affiliated lesson with year = request->year
            $old_lecons = [$lecon->id];
            $current_year = $lecon->annee;
            while(true){
                if ($request->annee == $current_year){
                    break;
                }

                $old_next_lecons = [];
                foreach ($lecons_liens as $lien){
                    if ( $request->annee < $current_year){
                        if (in_array($lien->enfant_id, $old_lecons)){
                            $old_next_lecons[] = $lien->parent_id;
                        }
                    } else {
                        if (in_array($lien->parent_id, $old_lecons)){
                            $old_next_lecons[] = $lien->enfant_id;
                        }
                    }
                }

                $old_lecons = $old_next_lecons;

                if ( $request->annee < $current_year){
                    $current_year -= 1;
                } else {
                    $current_year += 1;
                }
            }

            foreach ($old_lecons as $old_lecon){
                $status_already = false;
                foreach ($couplages_lecons as $status2){
                    if ($status2->lecon_id == $old_lecon){
                        $status_already = true;
                        break;
                    }
                }
                foreach ($nouveautes as $nouveaute){
                    if ($old_lecon == $nouveaute[0]){
                        $status_already = true;
                        break;
                    }
                }
                if ($status_already){
                    continue;
                }

                $nouveautes[] = [$old_lecon];
                DB::table('couplages_lecons')->insert([
                    'lecon_id' => $old_lecon,
                    'couplage_id' => $id,
                    'status' => $status->status,
                ]);
            }
        }


        // -------------
        // Transfer couplages_couverture
        $couplages_couverture = DB::table('couplages_couverture')->where('couplage_id', $id)->get();
        $nouveautes = [];
        foreach ($couplages_couverture as $couverture){
            $lecon = DB::table('lecons')->where('id', $couverture->lecon_id)->first();
            if ($request->annee == $lecon->annee){
                continue; // ne pas déplacer cette couverture à une autre année vu que c'est déjà la bonne
            }

            // get the affiliated lesson with year = request->year
            $old_lecons = [$lecon->id];
            $current_year = $lecon->annee;
            while(true){
                if ($request->annee == $current_year){
                    break;
                }

                $old_next_lecons = [];
                foreach ($lecons_liens as $lien){
                    if ( $request->annee < $current_year){
                        if (in_array($lien->enfant_id, $old_lecons)){
                            $old_next_lecons[] = $lien->parent_id;
                        }
                    } else {
                        if (in_array($lien->parent_id, $old_lecons)){
                            $old_next_lecons[] = $lien->enfant_id;
                        }
                    }
                }

                $old_lecons = $old_next_lecons;

                if ( $request->annee < $current_year){
                    $current_year -= 1;
                } else {
                    $current_year += 1;
                }
            }

            foreach ($old_lecons as $old_lecon){
                $already = false;
                foreach ($couplages_couverture as $couverture2){
                    if ($couverture2->lecon_id == $old_lecon && $couverture2->developpement_id == $couverture->developpement_id){
                        $already = true;
                        break;
                    }
                }
                foreach ($nouveautes as $nouveaute){
                    if ($old_lecon == $nouveaute[0] && $couverture->developpement_id == $nouveaute[1]){
                        $already = true;
                        break;
                    }
                }
                if ($already){
                    continue;
                }
                
                $nouveautes[] = [$old_lecon, $couverture->developpement_id];
                DB::table('couplages_couverture')->insert([
                    'lecon_id' => $old_lecon,
                    'couplage_id' => $id,
                    'developpement_id' => $couverture->developpement_id,
                ]);
            }
        }

        // --------------
        // Transfer votes
        // How does it works ?
        // For every vote in the DB of the user, get the affiliated lessons of the request year
        // Then insert a new vote for each of these lessons.
        // Do not insert if a vote for the triplet (user, dev, lesson) already exists.
        $records = DB::table('recasages_votes')->where('user_id', $user_id)->get();
        $nouveautes = [];
        foreach ( $records  as $vote ){
            $lecon = DB::table('lecons')->where('id', $vote->lecon_id)->first();
            echo $vote->lecon_id . " - " . $lecon->annee . " - " .  $vote->qualite . "<br>";
            if ($request->annee == $lecon->annee){
                continue; // ne pas déplacer ce vote à une autre année vu que c'est déjà la bonne
            }


            $old_lecons = [$lecon->id];
            $current_year = $lecon->annee;
            while(true){
                if ($request->annee == $current_year){
                    break;
                }

                $old_next_lecons = [];
                foreach ($lecons_liens as $lien){
                    if ( $request->annee < $current_year){
                        if (in_array($lien->enfant_id, $old_lecons)){
                            $old_next_lecons[] = $lien->parent_id;
                        }
                    } else {
                        if (in_array($lien->parent_id, $old_lecons)){
                            $old_next_lecons[] = $lien->enfant_id;
                        }
                    }
                }

                $old_lecons = $old_next_lecons;

                if ( $request->annee < $current_year){
                    $current_year -= 1;
                } else {
                    $current_year += 1;
                }
            }

            // $old_lecons est le tableau des IDs des lecons affiliées à la leçon initiale mais à l'année de la request
            // print_r($old_lecons);
            // echo "<br>";

            foreach ($old_lecons as $old_lecon){
                $vote_already = false;
                foreach ($records as $vote2){
                    if ($vote2->lecon_id == $old_lecon && $vote2->developpement_id == $vote->developpement_id){
                        $vote_already = true;
                        break;
                    }
                }
                foreach ($nouveautes as $nouveaute){
                    if ($old_lecon == $nouveaute[0] && $vote->developpement_id == $nouveaute[1]){
                        $vote_already = true;
                        break;
                    }
                }

                if ($vote_already){
                    continue;
                }
                
                $nouveautes[] = [$old_lecon, $vote->developpement_id];
                DB::table('recasages_votes')->insert([
                    'lecon_id' => $old_lecon,
                    'developpement_id' => $vote->developpement_id,
                    'qualite' => $vote->qualite,
                    'user_id' => $vote->user_id, 
                ]);
            }
        }




        $this->couplageRepository->update($request->all(), $id);

        return redirect(route('couplages.index.user'))
            ->withOk("Les paramètres ont été modifiés avec succès.");
    }




    /**
     * Set the settings of the export of a couplage
     *
     * @param  int  $id
     * @return Response
     */
     public function ajaxExportSettings(CouplageAjaxRequest $request)
     {
        $request->session()->put('couplageExportOptions', [
             'developpements'             => $request->has('developpements'),
             'developpementsCommentaires' => $request->has('developpementsCommentaires'),
             'developpementsReferences'   => $request->has('developpementsReferences'),
             'lecons'                     => $request->has('lecons'),
             'leconsCommentaires'         => $request->has('leconsCommentaires'),
             'leconsReferences'           => $request->has('leconsReferences'),
             'impasse'                    => $request->has('impasse'),
             'checkedOnly'                => $request->has('checkedOnly')
        ]);

        return "";
     }

     /**
      * Export a couplage
      *
      * @param  int  $id
      * @return Response
      */
    public function export($id, $a=true,$b=false,$c=false,$d=true,$e=true,$f=true,$g=false,$h=false,$i=false)
    {
        $opts = is_array(Request::session()->get('couplageExportOptions'))?
            Request::session()->get('couplageExportOptions') :
            [
            'developpements' => $a,
            'developpementsCommentaires' => $b,
            'developpementsReferences' => $c,
            'lecons' => $d,
            'leconsCommentaires' => $e,
            'leconsReferences' => $f,
            'impasse' => $g,
            'checkedOnly' => $h,
            'developpementsRecasages' => $i
        ];

        $couplage       = $this->couplageRepository->find($id);
        $lecons         = $couplage->lecons();
        $couverture     = $couplage->couverture->keyBy(function($item){ return $item->lecon_id."_".$item->developpement_id; });
        $recasages      = $couplage->recasages();
        $invRecasages   = $couplage->invRecasages();
        $references     = $this->referenceRepository->toArray();
        $developpements = $this->developpementRepository->toArray();

        Debugbar::disable();
        return view('couplages.pdf', compact('couplage', 'lecons', 'couverture', 'recasages', 'invRecasages', 'developpements', 'references', 'opts'));
    }

    // public function export_html($id, $a=true,$b=false,$c=false,$d=true,$e=true,$f=true,$g=false,$h=false,$i=false)
    // {
        

    //     $opts = is_array(Request::session()->get('couplageExportOptions'))?
    //         Request::session()->get('couplageExportOptions') :
    //         [
    //         'developpements' => $a,
    //         'developpementsCommentaires' => $b,
    //         'developpementsReferences' => $c,
    //         'lecons' => $d,
    //         'leconsCommentaires' => $e,
    //         'leconsReferences' => $f,
    //         'impasse' => $g,
    //         'checkedOnly' => $h,
    //         'developpementsRecasages' => $i
    //     ];

    //     $couplage       = $this->couplageRepository->find($id);
    //     $lecons         = $couplage->lecons();
    //     $couverture     = $couplage->couverture->keyBy(function($item){ return $item->lecon_id."_".$item->developpement_id; });
    //     $recasages      = $couplage->recasages();
    //     $invRecasages   = $couplage->invRecasages();
    //     $references     = $this->referenceRepository->toArray();
    //     $developpements = $this->developpementRepository->toArray();

    //     echo $couplage;

    //     // return view('couplages.html', compact('couplage', 'lecons', 'couverture', 'recasages', 'invRecasages', 'developpements', 'references', 'opts'));
    // }

}
