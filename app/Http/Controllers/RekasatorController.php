<?php 
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Outil\OutilRekasatorRequest;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Sentinel;

class RekasatorController extends Controller
{
    protected $developpementRepository;
    protected $leconRepository;

    public function __construct(DeveloppementRepository $developpementRepository, LeconRepository $leconRepository)
    {
        $this->developpementRepository = $developpementRepository;
        $this->leconRepository = $leconRepository;
    }


   /**
    * Display form for the rekasator
    *
    * @return Response
    */
    public function index($annee)
    {
        $lecons = $this->leconRepository->allWhere('annee', '=', $annee)->load('type');

        return view('outils.rekasator', compact('lecons', 'annee'));
    }



    public function result($annee, OutilRekasatorRequest $request)
    {
        $start = microtime(true);

        $extendUserCouplage = ($request->input('extendUserCouplage') == 'true')? true : false;
        \Debugbar::info($extendUserCouplage);


        $user = Sentinel::check();

        if ($extendUserCouplage && $user == null){
            return response()->json(["erreur" => "noAuth" ]);
        }

        if ($extendUserCouplage && $user) {
            // User is authenticated
            \Debugbar::info("Authenticated user: ");

            if(isset($user->couplage) ){
                $userCouplage       = $user->couplage;
                
                // On crée $userLecons qui ne contient que les lecons non impassés (status à 0)
                $userLeconsRaw      = $userCouplage->lecons()->pluck('id')->toArray();
                $userLecons = array();
                foreach($userLeconsRaw as $i => $leconId  ){ // toutes les lecons de l'année
                    if ($userCouplage->getLeconStatus($leconId) != 0){
                        array_push($userLecons, $leconId); 
                    }
                }

                // On crée $userDevs qui contient les dévs avec les qualités perso
                $userRecasages      = $userCouplage->recasages();
                // $userDevs         = $userCouplage->developpements->keyBy('id');
                $userDevsRaw         = $userCouplage->developpements->pluck('id')->toArray(); // Les valeurs contiennent les ids des devs utilisés

                $userDevs = array();
                foreach($userDevsRaw as $i => $devId) {
                    $userDevs[$devId] = array();
                }
                foreach ($userRecasages as $leconId => $recasages){
                    foreach ($recasages as $devId => $reca){
                        if (in_array($leconId, $userLecons)){
                            array_push($userDevs[$devId], array($leconId, $reca["qualite"]));
                        }
                        
                    }
                }

            } else {
                \Debugbar::info("No user Couplage: ");
                return response()->json(["erreur" => "noUserCouplage" ]);
            }
        } else {
            \Debugbar::info("No auth user: ");
        }


        
        $_qualite   = $request->input('qualite');
        $_useUnreferenced = $request->input('useUnreferenced'); // ! 'true' or 'false' not boolean
        // $_maxessais = $request->input('nbr_essais');
        $_lecons    = $request->input('lecons');

        if (isset($userLecons)){
            $_lecons = $userLecons;
        }
        \Debugbar::info($_lecons);

        $_useUnreferenced = ($_useUnreferenced == 'true')? true : false;
        \Debugbar::info($_useUnreferenced);

        \Debugbar::info(sprintf("qualite minimal: %d", $_qualite));
        if ($_qualite > 5){
            \Debugbar::debug("qualite doit pas dépasser 5"); 
        }
        \Debugbar::info(sprintf("nb lecons: %d", count($_lecons)));


        // Recuperer les données des développements sous la forme d'un Map<DevId, Array<(LeconId,int)>>
        // le deuxième la qualité
        $devs = array();
        $devsRecaCount = array();
        $rawDevs = $this->developpementRepository->all()->load('recasages');
        foreach($rawDevs as $developpement) {
            $devId = $developpement->id;

            // Si le dév est dans le userCouplage, on rajoute les recasages perso et on passe
            if (isset($userDevs) && array_key_exists($devId, $userDevs) ){
                $devs[$devId] = $userDevs[$devId];
                array_push($devsRecaCount, count($devs[$devId]));
                continue;
            }

            // Ne pas utiliser les dévs Doublons
            if (stripos($developpement->nom, 'doublon') !== false || stripos($developpement->nom, 'Doublon') !== false ) {
                continue;
            }


            $nbRefs = 0;
            foreach($developpement->versions as $version){
                $nbRefs += count($version->references);
            }
            if ($nbRefs == 0 && $_useUnreferenced == false){
                continue; // si on exclut les dévs sans réf on le passe, il ne sera pas dans la BD locale
            }
            
            
            $devs[$devId] = array();

            $recaCount = 0;
            foreach($developpement->recasages as $rlecon){
                if(in_array($rlecon->id, $_lecons) && $rlecon->pivot->qualite >= $_qualite) {
                    // \Debugbar::debug(sprintf('DevId: %d, leconId: %d, q: %d', $devId, $rlecon->id, $rlecon->pivot->qualite));
                    array_push($devs[$devId], array($rlecon->id, $rlecon->pivot->qualite));
                    $recaCount ++;
                }
            }
            array_push($devsRecaCount, $recaCount);
        }
        $rawDevs = $rawDevs->keyBy('id');
        $nDevs = count($devs);
        \Debugbar::info(sprintf("nb devs: %d", $nDevs));
        
        
        
        // Calcul borne inf de la taille d'un couplage minimale : 
        //      Les développements triés par ordre décroissant de nombre de recasages
        //      On les prend 1 par 1 jusqu'à que ça dépasse 2 fois le nombre de lecons.
        rsort($devsRecaCount);
        $minCouplageSize = 0;
        $minCouplageRecaCount = 0;
        while ($minCouplageRecaCount < count($_lecons)*2 ){
            $minCouplageRecaCount += $devsRecaCount[$minCouplageSize];
            $minCouplageSize ++;
        }
        \Debugbar::info(sprintf("taille minimale d'un couplage : %d %d", $minCouplageSize, $minCouplageRecaCount));



        // Verifier la couvrabilité des leçons
        if (checkCouvrabilite($devs, $_lecons) == false){
            \Debugbar::debug("stop");
            $li = leconsIncouvrable($devs, $_lecons);
            $leconsIncou2 = array();
            $lecons = $this->leconRepository->allWhereIn('id', $_lecons)->keyBy('id');
            foreach ($li as $id => $leconId){
                \Debugbar::debug($leconId); 
                \Debugbar::debug($lecons[$leconId]->numero); 
                array_push($leconsIncou2, $lecons[$leconId]->numero);
            }
            return response()->json(["erreur" => "incouvrable", "lecons" => $leconsIncou2 ]);
        }


        // Paramtères algorithme génétique
        $s = 20; // Population size
        $nbEvolutions = 100;
        $mutationPower = 2;


        


        // Initialisation de la population.
        // Un individu est un couplage qui est un Array<DevId> (donc des entiers)
        // Les qualités sont dans $devs
        $couplages = array();
        for ($i = 0; $i < $s; $i++){
            $couplages[$i] = array();
            if (isset($userDevs)){
                foreach($userDevs as $devId => $dev){
                    array_push($couplages[$i], $devId);
                }
            }
            fillCouplage($couplages[$i], $devs, $_lecons);
            // \Debugbar::debug(nbDevsInutiles($couplages[$i], $devs, $_lecons));
            enleverDevsInutiles($couplages[$i], $devs, $_lecons, isset($userDevs) ? $userDevs : null);
            // \Debugbar::debug(nbDevsInutiles($couplages[$i], $devs, $_lecons));

        }
        printCouplagesCount($couplages);

        for ($i = 0 ; $i < $nbEvolutions ; $i++){
            // printCouplagesCount($couplages, true);
            selectCouplages($couplages);
            // printCouplagesCount($couplages, true);
            mutateCouplages($couplages, $devs, $_lecons, $mutationPower, isset($userDevs) ? $userDevs : null);

            // for ($i = 0; $i < $s; $i++){
            //     enleverDevsInutiles($couplages[$i], $devs, $_lecons, isset($userDevs) ? $userDevs : null);
            // }
        }
        printCouplagesCount($couplages);

        
        for ($i = 0; $i < $s; $i++){
            enleverDevsInutiles($couplages[$i], $devs, $_lecons, isset($userDevs) ? $userDevs : null);
        }



        // Choisir le couplage avec le moins de dév
        $chosenPopId = 0;
        for ($i = 0 ; $i < $s ; $i++){
            if( count($couplages[$i]) < count($couplages[$chosenPopId])){
                $chosenPopId = $i;
            }
        }
        \Debugbar::debug(sprintf("best couplage %d, nb devs: %d ", $chosenPopId, count($couplages[$chosenPopId])));
        


        // Initialisation du recasage final
        $recasages = [];
        foreach($_lecons as $lid)
            $recasages[$lid] = [];
        
        foreach ($couplages[$chosenPopId] as $devId){
            // \Debugbar::debug(sprintf("devId %d", $devId));
            foreach($devs[$devId] as $reca){
                // \Debugbar::debug(sprintf("reca %d %d", $reca[0], $reca[1]));
                array_push($recasages[$reca[0]], [
                    'id' => $devId,
                    'developpement' => $rawDevs[$devId],
                    'qualite' => $reca[1]
                ]);
            }
        }
        $end = microtime(true);
        $executionTime = $end - $start;
        \Debugbar::debug($executionTime);

        \Debugbar::debug("fin");

        $lecons = $this->leconRepository->allWhereIn('id', $_lecons)->keyBy('id');
        if (isset($userDevs)){

            $usedDevNames = array();
            foreach( $couplages[$chosenPopId] as $i => $devId){
                $usedDevNames[$devId] = $rawDevs[$devId]->nom;
            }
            
            return response()->json([
                'recasages' => $recasages,
                'lecons'    => $lecons,
                'userDevs'  => $userDevs,
                'resultCouplage' => $couplages[$chosenPopId],
                'usedDevNames' => $usedDevNames
            ]);
        }
        return response()->json([
            'recasages' => $recasages,
            'lecons'    => $lecons
        ]);
    }





   /**
    * Return json object of data for the rekasator.
    *
    * @return Response
    */
    public function result2($annee, OutilRekasatorRequest $request)
    {
        $_qualite   = $request->input('qualite');
        $_recasages = $request->input('recasages');
        $_maxessais = $request->input('nbr_essais');
        $_lecons = $request->input('lecons');

        // Calcul des scores
        $totalScore = 0;
        $aDevScore  = [];
        $aDevScores = [];

        $developpements = $this->developpementRepository->all()->load('recasages');
        foreach($developpements as $developpement)
        {
            // Init
            $id = $developpement->id;
            $aDevScore[$id]  = 0;
            $aDevScores[$id] = [];
            foreach($_lecons as $lid)
                $aDevScores[$id][$lid] = 0;

            // Calcul
            foreach($developpement->recasages as $rlecon)
            if(in_array($rlecon->id, $_lecons))
            {
                $aDevScore[$id]++;
                $aDevScores[$id][$rlecon->id] = $rlecon->pivot->qualite;
            }

            $aDevScore[$id] = pow($aDevScore[$id], $_recasages);
            $totalScore += $aDevScore[$id];
        }
        $developpements = $developpements->keyBy('id');
        arsort($aDevScore);


        // Initialisation du recasages
        $recasages = [];
        foreach($_lecons as $lid)
            $recasages[$lid] = [];


        // Tire un développement selon une loi géométrique
        $tirerDeveloppement = function() use ($totalScore, $aDevScore)
        {
            $k = rand(1, $totalScore);
            $i = 0;
            foreach($aDevScore as $id => $score) 
            {
                $i += $score;
                if($i >= $k)
                    return $id;
            }
        };

        // Itère le tirage
        for($i = 0; $i < $_maxessais; $i++)
        {
            $id = $tirerDeveloppement();
            foreach($_lecons as $lid)
            {
                if(count($recasages[$lid]) == 2 || in_array($id, array_column($recasages[$lid], 'id')) || $aDevScores[$id][$lid] == 0)
                    continue;

                if($aDevScores[$id][$lid] < 5 && rand(0,100) + $_qualite >= 100)
                    continue;

                // On ajoute le développement à la leçon
                array_push($recasages[$lid], [
                    'id' => $id,
                    'developpement' => $developpements[$id],
                    'qualite' => $aDevScores[$id][$lid]
                ]);
            }
        }


        $lecons = $this->leconRepository->allWhereIn('id', $_lecons)->keyBy('id');
        return response()->json([
            'recasages' => $recasages,
            'lecons'    => $lecons
        ]);
    }
}


/**
 * Un dév est inutile dans un couplage quand il n'aide pas à couvrir des lecons.
 * Il est inutile si counter[leconId] >= 3 pour tout leconId où il intervient
 * On ne retire pas des dévs du UserCouplage (s'il existe) même s'ils sont inutiles (la suppression d'autres dévs le rendra utile)
 * @param $userDevs est null ou un Map<DevId, Recasages>
 */
function enleverDevsInutiles(&$couplage, $devs, $lecons, $userDevs){
    $counter = array();

    foreach($lecons as $leconId){
        $counter[$leconId] = 0;
    }

    foreach($couplage as $devId){
        foreach($devs[$devId] as $reca){
            $leconId = $reca[0];
            $counter[$leconId] += 1;
        }
    }

    foreach($couplage as $i => $devId){
        // Si $userCouplage existe et que le dév en fait parti, alors on passe (on le supprime pas)
        if (isset($userDevs) && array_key_exists($devId, $userDevs)){
            continue;
        }

        $utile = false;
        foreach($devs[$devId] as $reca){
            if ($counter[$reca[0]] == 2){
                $utile = true;
                break;
            }
        }
        if ($utile == false){
            // \Debugbar::debug($devId);
            unset($couplage[$i]);
            enleverDevsInutiles($couplage, $devs, $lecons, $userDevs);
            break;
        }
    }
}

function nbDevsInutiles($couplage, $devs, $lecons){
    $counter = array();

    foreach($lecons as $leconId){
        $counter[$leconId] = 0;
    }

    foreach($couplage as $devId){
        foreach($devs[$devId] as $reca){
            $leconId = $reca[0];
            $counter[$leconId] += 1;
        }
    }

    $count = 0;
    foreach($couplage as $i => $devId){
        $utile = false;
        foreach($devs[$devId] as $reca){
            if ($counter[$reca[0]] == 2){
                $utile = true;
                break;
            }
        }
        if ($utile == false){
            $count ++;
        }
    }
    return $count;
}



function leconsIncouvrable($devs, $lecons){
    $coverCounter = array();
    foreach($lecons as $leconId){
        $coverCounter[$leconId] = 0;
    }

    foreach($devs as $devId => $devRecasages){
        foreach($devRecasages as $reca){
            $leconId = $reca[0];
            $coverCounter[$leconId] += 1;
        }
    }

    $incouvrables = array();
    foreach($coverCounter as $leconId => $count){
        if ($count < 2){
            array_push($incouvrables, $leconId);
        }
    }
    return $incouvrables;
}

function checkCouvrabilite($devs, $lecons){
    $coverCounter = array();
    foreach($lecons as $leconId){
        $coverCounter[$leconId] = 0;
    }
    // \Debugbar::debug(count($devs));


    foreach($devs as $devId => $devRecasages){
        foreach($devRecasages as $reca){
            $leconId = $reca[0];
            $coverCounter[$leconId] += 1;
        }
    }

    // \Debugbar::debug(min($coverCounter));
    if (min($coverCounter) < 2){
        \Debugbar::debug("impossible de couvrir toutes les leçons à causes des leçons suivantes :");
        foreach($coverCounter as $leconId => $count){
            if ($count < 2){
                \Debugbar::debug(sprintf("%d %d", $leconId, $count));
            }
        }
        return false;
    }
    return true;
}



function printCouplagesCount($couplages, $printValues = false){
    $s = "";
    $min = count($couplages[0]);
    $max = count($couplages[0]);
    $sum = 0;
    foreach($couplages as $couplage){
        $s .= " _ " . strval(count($couplage));
        $min = min($min, count($couplage));
        $max = max($max, count($couplage));
        $sum += count($couplage);
    }
    $mean = $sum/count($couplages);
    if ($printValues){
        \Debugbar::debug(sprintf("[%d, %.1f, %d] values %s", $min, $mean, $max, $s ));
    } else {
        \Debugbar::debug(sprintf("[%d, %.1f, %d]", $min, $mean, $max, $s ));
    }
    
}




function selectCouplages(&$couplages){
    $fitness = array();
    $totalFitness = 0;
    for ($i=0 ; $i < count($couplages); $i++){
        $f = pow(count($couplages[$i]), -10) ;
        array_push($fitness, $f );
        $totalFitness += $f;
    }
    for ($i=0 ; $i < count($couplages); $i++){
        $fitness[$i] /= $totalFitness;
    }

    // Debug: print the fitness (total sum = 1)
    // $str = "_____________________";
    // for ($i=0 ; $i < count($couplages); $i++){
    //     $str .= " " . sprintf("%.2f", $fitness[$i]);
    // }
    // \Debugbar::debug($str);


    $newCouplages = array();
    for ($i=0 ; $i < count($couplages); $i++){
        $r = mt_rand() / mt_getrandmax();
        $s = 0.;
        for  ($j=0 ; $j < count($couplages); $j++){
            if ($r <= $s + $fitness[$j] ) {
                array_push($newCouplages, $couplages[$j]);
                break;
            } else {
                $s += $fitness[$j];
            }
        }
    }
    // couplages.clear();
    $couplages = $newCouplages;
}



function mutateCouplages(&$couplages, $devs, $_lecons, $mutationPower, $userDevs){
    foreach($couplages as &$couplage){
        removeCouplage($couplage, $mutationPower, $userDevs);
        fillCouplage($couplage, $devs, $_lecons);
    }
}





/**
 * Remove n devs from couplage avoiding removing dev from $userDevs if isset
 */
function removeCouplage(&$couplage, $n, $userDevs ){
    for($i= 0; $i < $n ; $i++){
        if (count($couplage) == 0){
            return;
        }
        while(true){
            $id = array_rand($couplage);
            if (isset($userDevs) && array_key_exists($couplage[$id], $userDevs)){
                continue;
            }
            unset($couplage[$id] );
            break;
        }
        
    }
}


function fillCouplage(&$rec, $devs, $lecons ){
    // Pour chaque lecon on compte le nombre de dév que le couplage a choisi dedans
    // \Debugbar::debug("fill -------- ");
    $counter = array();
    foreach($lecons as $leconId){
        $counter[$leconId] = 0;
    }

    foreach($rec as $devId){
        foreach($devs[$devId] as $reca){
            $leconId = $reca[0];
            $counter[$leconId] += 1;
        }
    }
    // \Debugbar::debug(sprintf('salut %d', min($counter)));
    // foreach($lecons as $leconId){
        // \Debugbar::debug(sprintf('%d %d', $leconId, $counter[$leconId]));
    // }


    $devsUtiles = array();
    foreach($devs as $devId => $recasages){
        if (in_array($devId, $rec) == false ){
            foreach($recasages as $reca){
                $leconId = $reca[0];
                if ($counter[$leconId] < 2){
                    $devsUtiles[$devId] = $recasages;
                }
            }
        }
    }


    // Tant qu'il y a une lecon non couverte, on rajoute un dev au hasard dans le couplage
    // Il ne faut pas qu'il soit déjà pris et qu'il ne serve à rien en terme de couverture :
    // Il doit passer dans une lecon ayant counter < 2
    while (min($counter) < 2){

        $devId =  array_rand($devsUtiles);
        
        if (in_array($devId, $rec) == false ){

            $utile = false;
            foreach($devsUtiles[$devId] as $reca){
                $leconId = $reca[0];
                if ($counter[$leconId] < 2){
                    $utile = true;
                }
            }
            if ($utile){
                // \Debugbar::debug(sprintf('%d %d', $devId, count($devs[$devId])));
                foreach($devsUtiles[$devId] as $reca){
                    $leconId = $reca[0];
                    $counter[$leconId] += 1;
                }
                array_push($rec,$devId);
            }

            
        }
        
    }

   return true;
}