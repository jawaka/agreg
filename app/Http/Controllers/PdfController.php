<?php
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Repositories\Couplage\CouplageRepository;
use Sentinel;
use Request;
use PDF;

class PdfController extends Controller
{
    public function __construct()
    {
    }

    public function couplage($couplage_id, CouplageRepository $couplageRepository)
    {
        $couplage = $couplageRepository->find($couplage_id);
        if(!$couplage->public){
            $user = Sentinel::check();
            if(!$user || $user->id != $couplage->user->id)
                return "Vous n'avez pas les autorisations pour voir ce couplage.";
        }


        $opts = Request::session()->get('couplageExportOptions', function(){
          return [
            'developpements' => true,
            'developpementsCommentaires' => true,
            'developpementsReferences' => true,
            'lecons' => true,
            'leconsCommentaires' => true,
            'leconsReferences' => true,
            'impasse' => false,
            'checkedOnly' => true
          ];
        });
        $fopts = [];
        foreach($opts as $k => $val)
            $fopts[] = $val? 1 : 0;

        return PDF::loadFile(env('BASE_URI').'/couplages/pdf/'.$couplage_id.'/'.implode($fopts, '/'))->download('couplage.pdf');
    }
}
