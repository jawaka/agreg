<?php 
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Lecon\LeconCreateRequest;
use Agreg\Http\Requests\Lecon\LeconUpdateRequest;
use Agreg\Http\Requests\Lecon\LeconDeleteRequest;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Lecon\LeconTypeRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;

class LeconsController extends Controller
{
    protected $leconRepository;

    public function __construct(LeconRepository $leconRepository)
    {
        $this->leconRepository = $leconRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $lecons = $this->leconRepository->allOrdered();
        $leconsAnnees = $this->leconRepository->getAllAnnees();
        $tableorder = '"order": [[1, "desc"], [2, "asc"]]';

        return view('admin.lecons.index', compact('lecons', 'tableorder', 'leconsAnnees'));
    }


   /**
    * Display a listing of the resource where annee = $annee
    *
    * @return Response
    */
    public function indexAnnee($anneeLecon)
    {
        $lecons = $this->leconRepository->allWhere('annee', '=', $anneeLecon);
        $tableorder   = '"order": [[1, "asc"]],';

        return view('admin.lecons.index-annee', compact('lecons', 'tableorder', 'anneeLecon'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create(DeveloppementRepository $developpementRepository, LeconTypeRepository $leconTypeRepository)
    {
        $leconsTypes    = $leconTypeRepository->toArray();
        $leconsOptions  = ['A'=>'A', 'B'=>'B', 'C'=>'C', 'D'=>'D', 'Docteur'=>'Docteur'];
        $developpements = $developpementRepository->toArray();

        return view('admin.lecons.create',  compact('developpements', 'leconsTypes', 'leconsOptions'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(LeconCreateRequest $request)
    {
        $lecon = $this->leconRepository->create($request->all());

        return redirect(route('admin.lecons.show', $lecon->id))
            ->withOk("La leçon " . $lecon->titre . " a été créée.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $lecon = $this->leconRepository->find($id);
        $recasages = $lecon->recasagesOrdered;

        return view('admin.lecons.show',  compact('lecon', 'recasages'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, DeveloppementRepository $developpementRepository, LeconTypeRepository $leconTypeRepository)
    {
        $lecon = $this->leconRepository->find($id);
        $leconsTypes    = $leconTypeRepository->toArray();
        $leconsOptions  = ['A'=>'A', 'B'=>'B', 'C'=>'C', 'D'=>'D', 'Docteur'=>'Docteur'];
        $developpements = $developpementRepository->toArray();

        // Prepare the lecons lists for the rating inputs
        $leconsParents = $this->leconRepository->toArray(); // do not filter for annee=$lecon.annee -1 so that we can have resurgence
        $leconsEnfants = $this->leconRepository->toArray($this->leconRepository->allWhere('annee', '=', $lecon->annee + 1));

        return view('admin.lecons.edit',  compact('lecon', 'developpements', 'leconsTypes', 'leconsOptions', 'leconsParents', 'leconsEnfants'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(LeconUpdateRequest $request, $id)
    {
        $this->leconRepository->update($request->all(), $id);

        return redirect(route('admin.lecons.show', $id))
            ->withOk("La leçon " . $request->input('nom') . " a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(LeconDeleteRequest $request, $id)
    {
        $this->leconRepository->delete($id);

        return redirect(route('admin.lecons.index'))
            ->withOk("La leçon a été supprimée.");;
    }
}