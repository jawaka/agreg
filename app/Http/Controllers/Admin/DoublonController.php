<?php

namespace Agreg\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Agreg\Http\Requests;
use Agreg\Http\Controllers\Controller;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use DB;


class DoublonController extends Controller
{
    protected $developpementRepository;

    public function __construct(DeveloppementRepository $developpementRepository)
    {
        $this->developpementRepository = $developpementRepository;
    }
    
    public function index()
    {
        $developpements = $this->developpementRepository->toArray();

        return view('admin.outils.doublon', compact('developpements'));
    }

    public function operate(Request $request){
        $input = $request->all();

        if ($input['to_keep'] == '' || $input['to_delete'] == ''){
            return "ID vide";
        }
        
        if ($input['to_keep'] == $input['to_delete']){
            return "même ID";
        }

        
        // Rediriger le dév dans les couplages

        // les votes
        DB::table('recasages_votes')->where('developpement_id', $input['to_delete'])->update(array(
            'developpement_id' => $input['to_keep'],
        ));

        // je crois : si le dév est locké
        DB::table('couplages_developpements')->where('developpement_id', $input['to_delete'])->update(array(
            'developpement_id' => $input['to_keep'],
        ));

        // je crois : les recasages effectifs
        DB::table('couplages_couverture')->where('developpement_id', $input['to_delete'])->update(array(
            'developpement_id' => $input['to_keep'],
        ));

        // changer dans les retours des oraux
        DB::table('oraux_retours_reponses')
        ->where('reponsable_type', 'developpement')
        ->where('reponsable_id', $input['to_delete'])
        ->update(['reponsable_id' => $input['to_keep']]);

        // Rediriger les devId des versions
        DB::table('versions')
        ->where('versionable_type', 'developpement')
        ->where('versionable_id', $input['to_delete'])
        ->update(['versionable_id' => $input['to_keep']]);


        return "ok";
    }
}
