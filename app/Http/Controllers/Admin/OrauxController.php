<?php 
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Oral\OralFormCreateRequest;
use Agreg\Http\Requests\Oral\OralFormDeleteRequest;
use Agreg\Http\Requests\Oral\OralFormUpdateRequest;
use Agreg\Repositories\Oral\OralFormRepository;
use Agreg\Repositories\Oral\OralRetourRepository;
use Agreg\Repositories\Oral\OralQuestionTypeRepository;

class OrauxController extends Controller
{
    protected $oralFormRepository;
    protected $oralRetourRepository;

    public function __construct(OralFormRepository $oralFormRepository, OralRetourRepository $oralRetourRepository)
    {
        $this->oralFormRepository   = $oralFormRepository;
        $this->oralRetourRepository = $oralRetourRepository;
    }


   /**
    * Display a listing of the oral forms
    *
    * @return Response
    */
    public function index()
    {
        $oraux   = $this->oralFormRepository->all();
        $retours = $this->oralRetourRepository->all();

        return view('admin.oraux.index', compact('oraux', 'retours'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('admin.oraux.create');
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(OralFormCreateRequest $request)
    {
        $oral = $this->oralFormRepository->create($request->only('nom', 'fnom'));

        return redirect(route('admin.oraux.edit', $oral->id))
            ->withOk("L'oral " . $oral->nom . " a été créée.");
    }


   /**
    * Display the specified form.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $oral = $this->oralFormRepository->find($id)->load('questions');

        return view('admin.oraux.show',  compact('oral'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, OralQuestionTypeRepository $questionTypeRepository)
    {
        $oral = $this->oralFormRepository->find($id);
        $types = $questionTypeRepository->toArray();

        return view('admin.oraux.edit',  compact('oral', 'types'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(OralFormUpdateRequest $request, $id)
    {
        $this->oralFormRepository->update($request->only('nom', 'fnom', 'questions', 'questionsIds', 'questionsTypes'), $id);

        return redirect(route('admin.oraux.show', $id))
            ->withOk("L'oral " . $request->input('nom') . " a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(OralFormDeleteRequest $request, $id)
    {
        $this->oralFormRepository->delete($id);

        return redirect(route('admin.oraux.index'))
            ->withOk("L'oral a été supprimé.");;
    }
}