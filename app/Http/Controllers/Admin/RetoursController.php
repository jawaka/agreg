<?php 
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Oral\OralRetourCreateRequest;
use Agreg\Http\Requests\Oral\OralRetourUpdateRequest;
use Agreg\Http\Requests\Oral\OralRetourDeleteRequest;
use Agreg\Repositories\Oral\OralFormRepository;
use Agreg\Repositories\Oral\OralRetourRepository;
use Agreg\Repositories\Oral\OralQuestionTypeRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Reference\ReferenceRepository;


class RetoursController extends Controller
{
    protected $oralFormRepository;
    protected $oralRetourRepository;

    public function __construct(OralFormRepository $oralFormRepository, OralRetourRepository $oralRetourRepository)
    {
        $this->oralFormRepository   = $oralFormRepository;
        $this->oralRetourRepository = $oralRetourRepository;
    }


   /**
    * Redirect to oral index
    *
    * @return Response
    */
    public function index()
    {
        return redirect(route('admin.oraux.index'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create(UserRepository $userRepository)
    {
        $oraux = $this->oralFormRepository->all()->keyBy('id')->map(function($t){
            return $t->nom;
        });
        $users = $userRepository->all()->keyBy('id')->map(function($t){
            return $t->name;
        });

        return view('admin.retours.create', compact('oraux', 'users'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(OralRetourCreateRequest $request)
    {
        $data = $request->only('user_id', 'oraux_type_id', 'annee', 'anonyme');
        $retour = $this->oralRetourRepository->create($data);
        return redirect(route('admin.retours.edit', $retour->id))
            ->withOk("Le retour de " . $retour->user->name . " a été créé.");
    }


   /**
    * Display the specified form.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $retour = $this->oralRetourRepository->find($id);
        if (empty($retour)) {
            echo("Erreur : L'id du retour est inexistant dans la base de données.");
        }
        else {
            $reponses = $retour->load('reponses')->reponses->groupBy('question_id');
            return view('admin.retours.show',  compact('retour', 'reponses'));
        }
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id,   UserRepository $userRepository)
    {
        $retour   = $this->oralRetourRepository->find($id)->load('reponses');
        $reponses = $retour->reponses->groupBy('question_id');
        $users    = $userRepository->toArray();
        $ressources = $this->oralRetourRepository->ressources($retour->annee);

        return view('admin.retours.edit',  compact('retour', 'reponses', 'users', 'ressources'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(OralRetourUpdateRequest $request, $id)
    {
        $data = $request->only('user_id', 'anonyme', 'questions', 'reponses');
        $data['anonyme'] = $request->has('anonyme');
        
        // comportement très étrange du champ anonyme de la modification
        // si décoché, alors request[anonyme] existe pas
        // si coché, alors request[anonyme] vaut 0
        // il faut donc le transformer en booléen

        $this->oralRetourRepository->update($data, $id);

        return redirect(route('admin.retours.show', $id))
            ->withOk("Le retour a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(OralRetourDeleteRequest $request, $id)
    {
        $this->oralRetourRepository->delete($id);

        return redirect(route('admin.retours.index'))
            ->withOk("Le retour a été supprimé.");;
    }
}