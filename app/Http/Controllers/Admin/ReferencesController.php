<?php 
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Reference\ReferenceCreateRequest;
use Agreg\Http\Requests\Reference\ReferenceUpdateRequest;
use Agreg\Http\Requests\Reference\ReferenceDeleteRequest;
use Agreg\Repositories\Reference\ReferenceRepository;

class ReferencesController extends Controller
{
    protected $referenceRepository;

    public function __construct(ReferenceRepository $referenceRepository)
    {
        $this->referenceRepository = $referenceRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $references = $this->referenceRepository->all();

        return view('admin.references.index', compact('references'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('admin.references.create');
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(ReferenceCreateRequest $request)
    {
        $reference = $this->referenceRepository->create($request->except('_method', '_token'));

        return redirect(route('admin.references.show', $reference->id))
            ->withOk("La référence " . $reference->titre . " a été créée.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $reference = $this->referenceRepository->find($id);

        return view('admin.references.show',  compact('reference'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $reference = $this->referenceRepository->find($id);

        return view('admin.references.edit',  compact('reference'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(ReferenceUpdateRequest $request, $id)
    {
        $this->referenceRepository->update($request->except('_method', '_token'), $id);

        return redirect(route('admin.references.show', $id))
            ->withOk("La référence " . $request->input('titre') . " a été modifiée.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(ReferenceDeleteRequest $request, $id)
    {
        $this->referenceRepository->delete($id);

        return redirect(route('admin.references.index'))
            ->withOk("La référence a été supprimée.");;
    }
}