<?php
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Role\RoleCreateRequest;
use Agreg\Http\Requests\Role\RoleUpdateRequest;
use Agreg\Http\Requests\Role\RoleDeleteRequest;
use Agreg\Repositories\Role\RoleRepository;

class RolesController extends Controller
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $roles = $this->roleRepository->all();

        return view('admin.roles.index', compact('roles'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('admin.roles.create');
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(RoleCreateRequest $request)
    {
        $role = $this->roleRepository->create($request->only('slug', 'name', 'permissions'));

        return redirect(route('admin.role.show', $role->id))
            ->withOk("Le rôle " . $role->name . " a été créé.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $role = $this->roleRepository->find($id);

        return view('admin.roles.show',  compact('role'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $role = $this->roleRepository->find($id);

        return view('admin.roles.edit',  compact('role'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(RoleUpdateRequest $request, $id)
    {
        $this->roleRepository->update($request->only('slug', 'name', 'permissions'), $id);

        return redirect(route('admin.roles.show', $id))
            ->withOk("Le rôle " . $request->input('name') . " a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(RoleDeleteRequest $request, $id)
    {
        $this->roleRepository->delete($id);

        return redirect(route('admin.roles.index'))
            ->withOk("Le rôle a été supprimé.");;
    }
}
