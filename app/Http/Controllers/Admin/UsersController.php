<?php
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\User\UserCreateRequest;
use Agreg\Http\Requests\User\UserUpdateRequest;
use Agreg\Http\Requests\User\UserDeleteRequest;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Role\RoleRepository;
use Sentinel;

class UsersController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $users = $this->userRepository->all();

        return view('admin.users.index', compact('users'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create(RoleRepository $roleRepository)
    {
        $roles = $roleRepository->toArray();

        return view('admin.users.create', compact('roles'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(UserCreateRequest $request)
    {
        $user = $this->userRepository->createUser($request->all());

        return redirect(route('admin.users.show', $user->id))
            ->withOk("L'utilisateur " . $user->name . " a été créé.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $User = $this->userRepository->find($id)->load('versions');

        return view('admin.users.show',  compact('User'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, RoleRepository $roleRepository)
    {
        $User = $this->userRepository->find($id)->load('roles');
        $roles = $roleRepository->all();

        return view('admin.users.edit',  compact('User', 'roles'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(UserUpdateRequest $request, $id)
    {
        $this->userRepository->update($id, $request->all());

        return redirect(route('admin.users.show', $id))
            ->withOk("L'utilisateur " . $request->input('name') . " a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {
        $this->userRepository->delete($id);

        return redirect(route('admin.users.index'))
            ->withOk("L'utilisateur a été supprimé.");;
    }
}
