<?php
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Version\VersionRepository;
use Agreg\Repositories\Couplage\CouplageRepository;
use Agreg\Repositories\Oral\OralRetourRepository;


class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index(DeveloppementRepository $dr, VersionRepository $vr, UserRepository $ur, CouplageRepository $cr, OralRetourRepository $or)
    {
        $ressources = [];
        $n = 10;

        foreach($dr->last($n) as $d)
            $ressources[] = [
                'cdate'=> $d->created_at,
                'date' => $d->updated_at,
                'type' => 'Développement',
                'href' => route('admin.developpements.show', $d->id),
                'nom'  => $d->nom,
                'user' => isset($d->user)? $d->user->name : "Anonyme"
            ];

        foreach($vr->last($n) as $d)
            $ressources[] = [
                'cdate'=> $d->created_at,
                'date' => $d->updated_at,
                'type' => 'Version',
                'href' => route('admin.versions.show', $d->id),
                'nom'  => $d->versionable->nom,
                'user' => isset($d->user)? $d->user->name : "Anonyme"
            ];

        foreach($ur->last($n) as $d)
            $ressources[] = [
                'cdate'=> $d->created_at,
                'date' => $d->created_at,
                'type' => 'Utilisateur',
                'href' => route('admin.users.show', $d->id),
                'nom'  => $d->name,
                'user' => $d->name
            ];

        foreach($cr->last($n) as $d)
            $ressources[] = [
                'cdate'=> $d->created_at,
                'date' => $d->updated_at,
                'type' => 'Couplage',
                'href' => '#',
                'nom'  => 'Couplage de '.(isset($d->user)? $d->user->name : $d->user_id),
                'user' => isset($d->user)? $d->user->name : "Anonyme"
            ];

        foreach($or->last($n) as $d)
            $ressources[] = [
                'cdate'=> $d->created_at,
                'date' => $d->updated_at,
                'type' => 'Retour',
                'href' => route('admin.retours.show', $d->id),
                'nom'  => "Retour d'".$d->oral->nom,
                'user' => isset($d->user)? $d->user->name : "Anonyme"
            ];

        return view('admin.index', compact('ressources'));
    }
}
