<?php
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Version\VersionCreateRequest;
use Agreg\Http\Requests\Version\VersionUpdateRequest;
use Agreg\Http\Requests\Version\VersionDeleteRequest;
use Agreg\Repositories\Version\VersionRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Agreg\Repositories\User\UserRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Sentinel;

class VersionsController extends Controller
{
    protected $versionRepository;

    public function __construct(VersionRepository $versionRepository)
    {
        $this->versionRepository = $versionRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $versions = $this->versionRepository->allWith();
        $tablenoadd = true;

        return view('admin.versions.index', compact('versions', 'tablenoadd'));
    }



   /**
    * Show the form for creating a new version for a developpement
    *
    * @return Response
    */
    public function createForDeveloppement($id, DeveloppementRepository $developpementRepository, UserRepository $userRepository)
    {
        $versionable = $developpementRepository->find($id);

        return $this->create($versionable, $userRepository);
    }

   /**
    * Show the form for creating a new version for a lecon
    *
    * @return Response
    */
    public function createForLecon($id, LeconRepository $leconRepository, UserRepository $userRepository)
    {
        $versionable = $leconRepository->find($id);

        return $this->create($versionable, $userRepository);
    }

   /**
    * Show the form for creating a new version
    *
    * @return Response
    */
    public function create($versionable, UserRepository $userRepository)
    {
        $users = $userRepository->toArray();

        return view('admin.versions.create', compact('versionable', 'users'));
    }



   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(VersionCreateRequest $request)
    {
        $version = $this->versionRepository->create($request->except(['_method', '_token']));

        return redirect(route('admin.versions.edit', $version->id));
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $version = $this->versionRepository->find($id);

        return view('admin.versions.show',  compact('version'));
    }



   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, ReferenceRepository $referenceRepository, UserRepository $userRepository)
    {
        $version    = $this->versionRepository->find($id);
        $references = $referenceRepository->toArray();
        $users      = $userRepository->toArray();

        return view('admin.versions.edit',  compact('version', 'references', 'users'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(VersionUpdateRequest $request, $id)
    {
        $this->versionRepository->update($request->except(['_method', '_token']), $id);

        return redirect(route('admin.versions.show', $id))
            ->withOk("La version a été modifiée.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(VersionDeleteRequest $request, $id)
    {
        $version = $this->versionRepository->find($id);
        $url = route('admin.'. $version->versionable_type.'s.show', $version->versionable->id);

        $this->versionRepository->delete($id);

        return redirect($url)->withOk("La version a été supprimée.");;
    }




   /**
    * Upload a file and attach it to the version
    *
    * @param  int  $id
    * @return Response
    */
    public function uploadFile($id)
    {
        if(!Sentinel::check())
            return response()->json(['error' => "Non autorisé."]);

        if($f = $this->versionRepository->uploadFile($id, $_FILES['version_fichier']))
        {
            return response()->json([
                'id'  => $f->id,
                'url' => $f->url
            ]);
        }
        else
            return response()->json(['error' => "Erreur lors de l'envoi du fichier."]);
    }

    /**
    * Delete a file and detach it from the version
    *
    * @param  int  $id
    * @return Response
    */
    public function deleteFile($id)
    {
        if(!Sentinel::check())
            return response()->json(['error' => "Non autorisé."]);

        if($this->versionRepository->deleteFile($id, $_POST["fichier_id"]))
            return response()->json([]);
        else
            return response()->json(['error' => "Erreur lors de la suppression du fichier."]);
    }

}
