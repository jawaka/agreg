<?php 
namespace Agreg\Http\Controllers\Admin;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Developpement\DeveloppementCreateRequest;
use Agreg\Http\Requests\Developpement\DeveloppementUpdateRequest;
use Agreg\Http\Requests\Developpement\DeveloppementDeleteRequest;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\User\UserRepository;

class DeveloppementsController extends Controller
{
    protected $developpementRepository;

    public function __construct(DeveloppementRepository $developpementRepository)
    {
        $this->developpementRepository = $developpementRepository;
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $developpements = $this->developpementRepository->all();

        return view('admin.developpements.index', compact('developpements'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create(LeconRepository $leconRepository, UserRepository $userRepository)
    {
        $lecons = $leconRepository->toArray();
        $users = $userRepository->toArray();

        return view('admin.developpements.create', compact('lecons', 'users'));
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(DeveloppementCreateRequest $request)
    {
        $developpement = $this->developpementRepository->create($request->only('nom', 'details', 'recasages'));

        return redirect(route('admin.developpements.show', $developpement->id))
            ->withOk("Le développement " . $developpement->nom . " a été créée.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $developpement = $this->developpementRepository->find($id);
        $recasagesByYear = $developpement->recasagesOrdered->groupBy('annee');

        return view('admin.developpements.show',  compact('developpement', 'recasagesByYear'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id, LeconRepository $leconRepository, UserRepository $userRepository)
    {
        $developpement = $this->developpementRepository->find($id);
        $lecons = $leconRepository->toArray();
        $users = $userRepository->toArray();

        return view('admin.developpements.edit',  compact('developpement', 'lecons', 'users'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(DeveloppementUpdateRequest $request, $id)
    {
        $this->developpementRepository->update($request->only('nom', 'details', 'recasages', 'user_id'), $id);

        return redirect(route('admin.developpements.show', $id))
            ->withOk("Le développement " . $request->input('nom') . " a été modifié.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(DeveloppementDeleteRequest $request, $id)
    {
        $this->developpementRepository->delete($id);

        return redirect(route('admin.developpements.index'))
            ->withOk("Le développement a été supprimé.");;
    }
}