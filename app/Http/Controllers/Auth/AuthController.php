<?php
namespace Agreg\Http\Controllers\Auth;

use Sentinel;
use Agreg\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Agreg\Repositories\Role\RoleRepository;

use Agreg\Http\Requests\Auth\LoginRequest;
use Agreg\Http\Requests\Auth\RegisterRequest;
use Debugbar;
use ReCaptcha\ReCaptcha;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }



    /**
     * Show the login form
     *
     * @return view
    */
    protected function showLoginForm()
    {
        return view('auth.login');
    }


    /**
     * Attempt to login
     *
     * @return void
    */
    protected function login(LoginRequest $request)
    {
        $credentials = [
            'login'    => $request->input('login'),
            'password' => $request->input('password'),
            'remember' => $request->has('remember')
        ];

        if($user = Sentinel::stateless($credentials))
        {
            Sentinel::login($user, $request->has('remember'));
            return redirect()->back()->withOk('Vous avez été connecté avec succès.');
        }
        else
        {
            return redirect('login')->withError('Les informations saisies ne correspondent à aucun membre.');
        }
    }


    /**
     * Logout
     *
     * @return void
    */
    protected function logout()
    {
        Sentinel::logout();
        return redirect('/')->withOk('Vous avez été déconnecté avec succès.');
    }



    /**
     * Show the registration form
     *
     * @return view
    */
    protected function showRegistrationForm()
    {
        return view('auth.register');
    }


    /**
     * Attempt to register.
     *
     * @param  RegisterRequest $request
     * @return bool
     */
    protected function register(RegisterRequest $request)
    {

        // Recaptcha v2 Vérification avec la clef privée
        $recaptchaResponse = $request->input('recaptcha_response');
        \Debugbar::debug($recaptchaResponse);

        $recaptcha = new ReCaptcha(env('RECAPTCHA_SECRET_KEY'));
        $result = $recaptcha->verify($recaptchaResponse, $_SERVER['REMOTE_ADDR']);

        if ($result->isSuccess()) {
            // reCAPTCHA verification successful
            \Debugbar::debug("ok");
        } else {
            // reCAPTCHA verification failed
            $errors = $result->getErrorCodes();
            \Debugbar::debug($errors);
            return "Erreur : " . implode(", ", $errors);
        }

        


        $credentials = [
            'name'     => $request->input('name'),
            'email'    => $request->input('email'),
            'password' => $request->input('password'),
        ];
        if($user = Sentinel::registerAndActivate($credentials))
        {
            $user = Sentinel::update($user, ['annee' => config('app.annee')]);
            Sentinel::login($user);
            return redirect('/')->withOk("L'inscription s'est finalisée avec succès. Vous êtes maintenant connecté.");
        }
        else
            return view('auth.register')->withError("Erreur lors de la finalisation de l'inscription");
    }
}
