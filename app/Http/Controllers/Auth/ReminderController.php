<?php
namespace Agreg\Http\Controllers\Auth;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Auth\ResetRequest;
use Illuminate\Http\Request;
use Sentinel;
use Reminder;
use Mail;

class ReminderController extends Controller
{
    protected $userRepository;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Show the form for the reminder
     *
     * @return view
    */
    protected function create()
    {
        return view('auth.passwords.email');
    }


    /**
     * Store the reminder
     *
     * @return view
    */
    public function store(Request $request)
    {
        $credentials = [
            'login' => $request->input('login'),
        ];

        if ($user = Sentinel::findByCredentials($credentials))
        {
            // Get an existing $reminding or create a new one
            ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));

            // Send the mail
            $data = [
                'email' => $user->email,
                'name'  => $user->name,
                'subject' => 'Réinitialisation du mot de passe',
                'code' => $reminder->code,
                'id' => $user->id
            ];

            Mail::queue('auth.emails.password', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject($data['subject']);
            });

            return view('auth.passwords.email-sent');
        }
        else
        {
            return redirect('password/reset')->withError('Aucun utilisateur trouvé avec cet identifiant.');
        }

    }




    /**
     * Edit the password
     *
     * @return view
    */
    public function edit($id, $code)
    {
        $user = Sentinel::findById($id);

        if (Reminder::exists($user, $code))
        {
            return view('auth.passwords.reset', compact('id','code'));
        }
        else
        {
            return redirect('password/reset')->withError("Ce code de réinitialisation n'est pas/plus valable. Merci de recommencer la procédure");
        }
    }


    /**
     * Store the new password
     *
     * @return view
    */
    public function update($id, $code, ResetRequest $request)
    {
        $user = Sentinel::findById($id);
        $reminder = Reminder::exists($user, $code);

        // Incorrect info
        if ($reminder == false)
        {
            return redirect('password/reset')->withError('Code de réinitialisation invalide.');
        }

        // Complete reminder
        $password = $request->input('password');
        Reminder::complete($user, $code, $password);

        return redirect('/')->withOk('Le mot de passe a été modifié avec succès.');
    }

}
