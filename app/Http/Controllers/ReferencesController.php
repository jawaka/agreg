<?php 
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Reference\ReferenceCreateRequest;
use Agreg\Http\Requests\Reference\ReferenceUpdateRequest;
use Agreg\Http\Requests\Reference\ReferenceDeleteRequest;
use Agreg\Repositories\Reference\ReferenceRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Lecon\LeconRepository;
use Debugbar;

class ReferencesController extends Controller
{
    protected $referenceRepository;

    public function __construct(ReferenceRepository $referenceRepository, LeconRepository $leconRepository, DeveloppementRepository $developpementRepository)
    {
        $this->referenceRepository = $referenceRepository;
        $this->developpementRepository = $developpementRepository;
        $this->leconRepository         = $leconRepository;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }


   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $references = $this->referenceRepository->all();

        return view('references.index', compact('references'));
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {
        return view('references.create');
    }


   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store(ReferenceCreateRequest $request)
    {
        $reference = $this->referenceRepository->create($request->only('titre', 'auteurs'));

        return redirect(route('references.show', $reference->id))
            ->withOk("La référence " . $reference->titre . " a été créée avec succès.");
    }


   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $reference = $this->referenceRepository->find($id);
        $developpements = $this->developpementRepository->all();

        $refDevIds = $reference->getDeveloppements()->values()->toArray();
        $refLeconsIds = $reference->getLecons()->values()->toArray();

        $refDevs = $developpements->filter(function ($developpement) use ($refDevIds) {
            return in_array($developpement->id, $refDevIds);
        });

        $mostRecentLecons = [];
        foreach( $refLeconsIds as $leconId){
            $lecon = $this->leconRepository->find($leconId);
            $toTreat = [$lecon];
            while (!empty( $toTreat) ){
                $leconEnfant = array_pop($toTreat);
                if ( count($leconEnfant->enfants) == 0 ){
                    array_push($mostRecentLecons, $leconEnfant);
                } else {
                    $childrenLecons = $leconEnfant->enfants;
                    foreach ($childrenLecons as $childLecon) {
                        array_push($toTreat, $childLecon);
                    }
                }
            }
        }

        // Convert to associative array
        $mostRecentLecons = array_reduce($mostRecentLecons, function ($carry, $item) {
            if (!isset($carry[$item->id])) {
                $carry[$item->id] = $item;
            }
            return $carry;
        }, []);
        
        // Convert the associative array back to a simple indexed array
        $mostRecentLecons = array_values($mostRecentLecons);
        $refLecons = $mostRecentLecons;
        $refLecons = collect($refLecons);

        // First version but does not work because
        // si version dans leçon 101 de 2023 et dans leçon 101 de 2024 ça mettra deux leçons au lieu d'une
        // $refLecons = $lecons->filter(function ($lecon) use ($refLeconsIds) {
        //     return in_array($lecon->id, $refLeconsIds);
        // });


        return view('references.show',  compact('reference', 'refDevs', 'refLecons'));
    }


   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {
        $reference = $this->referenceRepository->find($id);

        return view('references.edit',  compact('reference'));
    }


   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(ReferenceUpdateRequest $request, $id)
    {
        if($id != $request->input('id'))
            return redirect(route('references.index'));
        
        $this->referenceRepository->update($request->only('titre', 'auteurs'), $id);

        return redirect(route('references.show', $id))
            ->withOk("La référence " . $request->input('titre') . " a été modifiée avec succès.");
    }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy(ReferenceDeleteRequest $request, $id)
    {
        $this->referenceRepository->delete($id);

        return redirect(route('references.index'))
            ->withOk("La référence a été supprimée.");;
    }
}