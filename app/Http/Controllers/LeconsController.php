<?php
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\Lecon\LeconCreateRequest;
use Agreg\Http\Requests\Lecon\LeconUpdateRequest;
use Agreg\Http\Requests\Lecon\LeconDeleteRequest;
use Agreg\Http\Requests\Lecon\LeconSearchRequest;
use Agreg\Http\Requests\Lecon\LeconUpdateVotesRequest;
use Agreg\Repositories\Lecon\LeconRepository;
use Agreg\Repositories\Lecon\LeconTypeRepository;
use Agreg\Repositories\Developpement\DeveloppementRepository;
use Agreg\Repositories\Reference\ReferenceRepository;
use Sentinel;

class LeconsController extends Controller
{
    protected $leconRepository;

    public function __construct(LeconRepository $leconRepository)
    {
        $this->leconRepository = $leconRepository;
    }


   /**
    * Display a listing of the resource where annee = $annee
    *
    * @return Response
    */
    public function indexAnnee($anneeLecon, LeconTypeRepository $leconTypeRepository)
    {
        $lecons = $this->leconRepository->getAnnee($anneeLecon);
        $leconsAnnees = $this->leconRepository->getAllAnnees();

        return view('lecons.index-annee', compact('lecons', 'leconsAnnees', 'anneeLecon'));
    }


   /**
    * Display a listing of the resource matching some string
    *
    * @return Response
    */
    public function search($anneeLecon, LeconSearchRequest $request)
    {
        $search = $request->input('search');
        $lecons = $this->leconRepository->search($search)->where('annee', $anneeLecon)->load('recasagesOrdered');

        // Possible years
        $leconsAnnees = $this->leconRepository->getAllAnnees();

        return view('lecons.search', compact('lecons', 'search', 'leconsAnnees', 'anneeLecon'));
    }



   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id, DeveloppementRepository $developpementRepository, ReferenceRepository $referenceRepository)
    {
        $lecon          = $this->leconRepository->find($id);
        $recasages      = $lecon->recasagesOrdered;
        $anneeLecon     = $lecon->annee;
        $developpements = $developpementRepository->toArray();

        $couplage = getCouplage();
        if($couplage && $couplage->hasLecon($id)){
            $references = $referenceRepository->toArray();
            $usedDevs = [];
            foreach($couplage->leconRecasages($lecon->id) as $id => $info)
                array_push($usedDevs, $id);

            return view('lecons.show',  compact('lecon', 'recasages', 'anneeLecon', 'developpements', 'couplage', 'usedDevs', 'references'));
        }
        else
            return view('lecons.show',  compact('lecon', 'recasages', 'anneeLecon', 'developpements'));
    }



   /**
    * Update the votes of a user
    *
    * @param  int  $id
    * @return Response
    */
    public function updateVotes(LeconUpdateVotesRequest $request, $id)
    {
        if(!($id == $request->input('id') && $user = Sentinel::check()))
            return redirect(route('lecons.index.annee', [config('app.annee')]));

        $this->leconRepository->updateVotes($request->only('votes'), $id, $user->id);

        return redirect(route('lecons.show', $id))
            ->withOk("Vos recasages pour cette leçon ont été mis à jour.");
    }
}
