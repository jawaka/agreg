<?php
namespace Agreg\Http\Controllers;

use Agreg\Http\Controllers\Controller;
use Agreg\Http\Requests\User\UserUpdateRequest;
use Agreg\Repositories\User\UserRepository;
use Sentinel;

class UsersController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }



   /**
    * Display the specified user.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $ruser  = $this->userRepository->find($id)->load('versions');
        $cuser = Sentinel::check();

        if($cuser && $cuser->id == $ruser->id)
            return view('users.edit',  compact('ruser'));
        else
            return view('users.show',  compact('ruser'));
    }


   /**
    * Update the user.
    *
    * @param  int  $id
    * @return Response
    */
    public function update(UserUpdateRequest $request, $id)
    {
        if($id != $request->input('id'))
            return redirect(route('index'));

        $this->userRepository->update($id, $request->all());

        return redirect(route('users.show', $id))
            ->withOk("Vos informations ont été mises à jour.");
    }
}
