<?php
namespace Agreg\Http\Middleware;

use Closure;
use Sentinel;
use Illuminate\Support\Facades\Auth;

class SentinelGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Sentinel::check()) {
            return redirect('/');
        }

        return $next($request);
    }
}
