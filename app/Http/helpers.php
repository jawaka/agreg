<?php

//use Sentinel;

function getCouplage(){
    $user = Sentinel::check();
    if(!$user)
        return false;

    $couplage = $user->couplage;
    if(is_null($couplage))
        return false;

    return $couplage;
}
