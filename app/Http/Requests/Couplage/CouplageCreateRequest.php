<?php
namespace Agreg\Http\Requests\Couplage;

use Agreg\Http\Requests\Request;
use Sentinel;

class CouplageCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();// && Sentinel::hasAccess('couplage.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'public'        => '',
            'annee'         => 'required|digits:4',
            'option'        => 'required|in:1,2,3,4,5',
            'votes'         => '',
        ];
    }
}
