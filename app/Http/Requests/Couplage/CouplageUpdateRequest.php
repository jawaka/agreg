<?php
namespace Agreg\Http\Requests\Couplage;

use Agreg\Http\Requests\Request;
use Agreg\Repositories\Couplage\CouplageRepository;
use Sentinel;

class CouplageUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(CouplageRepository $couplageRepository)
    {
        $id       = $this->input('id');
        $couplage = $couplageRepository->find($id);

        return $couplage->isOwner || Sentinel::hasAccess('couplage.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'public'        => '',
            'annee'         => 'required|digits:4',
            'option'        => 'required|in:1,2,3,4,5',
            'votes'         => '',
        ];
    }
}
