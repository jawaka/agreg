<?php
namespace Agreg\Http\Requests\Couplage;

use Agreg\Http\Requests\Request;
use Agreg\Repositories\Couplage\CouplageRepository;
use Sentinel;

class CouplageAddDeveloppementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(CouplageRepository $couplageRepository)
    {
        $couplage = $couplageRepository->find($this->input('couplage_id'));

        return $couplage->isOwner || Sentinel::hasAccess('couplage.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'          => 'required|integer',
            'couplage_id' => 'required|integer',
        ];
    }
}
