<?php
namespace Agreg\Http\Requests\Couplage;

use Agreg\Http\Requests\Request;
use Agreg\Repositories\Couplage\CouplageRepository;
use Sentinel;

class CouplageAjaxRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(CouplageRepository $couplageRepository)
    {
        return $user = Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commentaires' => 'string'
        ];
    }
}
