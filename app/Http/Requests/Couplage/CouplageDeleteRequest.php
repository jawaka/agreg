<?php
namespace Agreg\Http\Requests\Couplage;

use Agreg\Http\Requests\Request;
use Agreg\Repositories\Couplage\CouplageRepository;
use Sentinel;

class CouplageDeleteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(CouplageRepository $couplageRepository)
    {
        $s        = $this->segments();
        $couplage = $couplageRepository->find(end($s));

        return $couplage->isOwner || Sentinel::hasAccess('couplage.delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [];
    }
}
