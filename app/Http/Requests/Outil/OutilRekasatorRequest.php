<?php
namespace Agreg\Http\Requests\Outil;

use Agreg\Http\Requests\Request;
use Sentinel;

class OutilRekasatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qualite'   => 'required|integer|between:0,5',
            // 'recasages' => 'required|integer|between:0,5', // TODO to change
            // 'recasages' => 'required|boolean', // Marche pas boolean parce que c'est un string 'true' ou 'false' ...
            // 'nbr_essais'=> 'required|integer|between:500,2000',
            // 'lecons'    => 'required|array'
        ];
    }
}
