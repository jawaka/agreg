<?php
namespace Agreg\Http\Requests\Role;

use Agreg\Http\Requests\Request;
use Sentinel;

class RoleUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check() && Sentinel::hasAccess('role.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'slug' => 'required|max:255|unique:roles,slug,' . $id,
            'name' => 'required|max:255|unique:roles,name,' . $id,
        ];
    }
}
