<?php
namespace Agreg\Http\Requests\Reference;

use Agreg\Http\Requests\Request;
use Sentinel;

class ReferenceCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();// && Sentinel::hasAccess('reference.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titre'   => 'required|max:255|unique:references,titre',
            'auteurs' => 'required|',
        ];
    }
}
