<?php
namespace Agreg\Http\Requests\Reference;

use Agreg\Http\Requests\Request;
use Sentinel;

class ReferenceDeleteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check() && Sentinel::hasAccess('reference.delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [];
    }
}
