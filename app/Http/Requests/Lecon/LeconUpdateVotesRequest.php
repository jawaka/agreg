<?php
namespace Agreg\Http\Requests\Lecon;

use Agreg\Http\Requests\Request;
use Sentinel;

class LeconUpdateVotesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check(); // && Sentinel::hasAccess('developpement.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'votes' => 'array',
        ];
    }
}
