<?php
namespace Agreg\Http\Requests\Oral;

use Agreg\Http\Requests\Request;
use Sentinel;

class OralFormCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check() && Sentinel::hasAccess('oral.form.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom'     => 'required|max:300|unique:oraux_types,nom',
            'fnom'    => 'required|unique:oraux_types,fnom',
        ];
    }
}
