<?php
namespace Agreg\Http\Requests\Oral;

use Agreg\Http\Requests\Request;
use Sentinel;

class OralRetourUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();// && Sentinel::hasAccess('oral.retour.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       => '',
        ];
    }
}
