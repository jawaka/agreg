<?php
namespace Agreg\Http\Requests\Oral;

use Agreg\Http\Requests\Request;
use Sentinel;

class OralFormUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check() && Sentinel::hasAccess('oral.form.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'nom'     => 'required|max:300|unique:oraux_types,nom,' . $id,
            'fnom'    => 'required|max:300|unique:oraux_types,fnom,'. $id,
            'questions'      => 'array',
            'questionsIds'   => 'array',
            'questionsTypes' => 'array',
        ];
    }
}
