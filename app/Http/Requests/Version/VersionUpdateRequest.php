<?php
namespace Agreg\Http\Requests\Version;

use Agreg\Http\Requests\Request;
use Sentinel;

class VersionUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'user_id'  => 'integer',
            'remarque' => 'string'
        ];
    }
}
