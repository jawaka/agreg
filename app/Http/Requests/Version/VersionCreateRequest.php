<?php
namespace Agreg\Http\Requests\Version;

use Agreg\Http\Requests\Request;
use Sentinel;

class VersionCreateRequest extends Request
{
    /**
     * Determine if the version is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'versionable_id'   => 'required|integer',
            'versionable_type' => 'required',
            'user_id'          => 'integer',
            'remarque'         => 'string'
        ];
    }
}
