<?php
namespace Agreg\Http\Requests\Version;

use Agreg\Http\Requests\Request;
use Sentinel;

class VersionDeleteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check();// && Sentinel::hasAccess('version.delete');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
