<?php
namespace Agreg\Http\Requests\User;

use Agreg\Http\Requests\Request;
use Sentinel;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Sentinel::check(); // && Sentinel::hasAccess('user.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->input('id');
        return [
            'name'          => 'max:255|unique:users,name,' . $id,
            'email'         => 'required|email|max:255|unique:users,email,' . $id,
            'montrer_email' => 'boolean',
            'classement'    => 'integer'
        ];
    }
}
