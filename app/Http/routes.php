<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Defining theme
Route::get('setTheme/{themeName}', ['middleware' => 'web', function($themeName)
{
    Session::put('themeName', $themeName);
    return Redirect::to('/');
}]);



// Administration
Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => ['web', 'loadTheme', 'auth', 'admin']
], function()
{
    Route::get('/',  ['as' => 'admin.index', 'uses' => 'HomeController@index']);

    Route::resource('lecons',            'LeconsController');
    Route::get('lecons/annee/{annee}',  ['as' => 'admin.lecons.index.annee', 'uses' => 'LeconsController@indexAnnee']);

    Route::resource('versions',                  'VersionsController', ['except' => ['create']]);
    Route::post(  'versions/{id}/uploadFile',   ['as' => 'admin.versions.uploadFile', 'uses' => 'VersionsController@uploadFile']);
    Route::delete('versions/{id}/deleteFile',   ['as' => 'admin.versions.deleteFile', 'uses' => 'VersionsController@deleteFile']);
    Route::get('developpements/{id}/versions/create', ['as' => 'admin.developpements.versions.create', 'uses' => 'VersionsController@createForDeveloppement']);
    Route::get('lecons/{id}/versions/create',         ['as' => 'admin.lecons.versions.create',         'uses' => 'VersionsController@createForLecon']);

    Route::resource('developpements',    'DeveloppementsController');
    Route::resource('references',        'ReferencesController');
    Route::resource('roles',             'RolesController');
    Route::resource('users',             'UsersController');

    Route::resource('oraux',             'OrauxController');
    Route::resource('retours',           'RetoursController');

    // Controller gérant un doublon
    Route::resource('outils/doublon',             'DoublonController');
    // Route::get('outils/doublon',  ['as' => 'doublon.index',  'uses' => 'DoublonController@index']);
    // Route::post('outils/doublon/operate',  ['as' => 'doublon.operate', 'uses' => 'DoublonController@operate']);
    Route::post('admin/outils/doublon/operate', 'DoublonController@operate')->name('admin.outils.doublon.operate');

});



// Main pages
Route::group(['middleware' => ['web', 'loadTheme']], function()
{
    Route::get('/', ['as' => 'home', function()
    {
        return view('index');
    }]);

    Route::get('/soutien', ['as' => 'soutien', function()
    {
        return view('soutien');
    }]);

    Route::get('/livre', ['as' => 'livre', function()
    {
        return view('livre');
    }]);


    Route::get('/tirages', ['as' => 'tirages', 'uses' => 'TiragesController@index']);


    Route::get('/test', ['as' => 'test', function(){
        return view('test');
    }]);

    // Leçons
    Route::resource('lecons', 'LeconsController', ['except' => ['index']]);
    Route::get( 'lecons/annee/{annee}',           ['as' => 'lecons.index.annee', 'uses' => 'LeconsController@indexAnnee']);
    Route::post('lecons/annee/{annee}/search',    ['as' => 'lecons.search',      'uses' => 'LeconsController@search']);
    Route::put( 'lecons/{id}/votes',              ['as' => 'lecons.votes.update','uses' => 'LeconsController@updateVotes']);

    // Développements
    Route::resource('developpements', 'DeveloppementsController');
    Route::get('developpements/kiviat/{annee}',      ['as' => 'developpements.kiviat',           'uses' => 'DeveloppementsController@kiviat']);
    Route::get('developpements/kiviat/{annee}/data', ['as' => 'developpements.kiviat.data',      'uses' => 'DeveloppementsController@kiviatData']);
    Route::put('developpements/{id}/votes',          ['as' => 'developpements.votes.update',     'uses' => 'DeveloppementsController@updateVotes']);

    // Versions
    Route::get('developpements/{id}/versions/create',  ['as' => 'versions.create.developpement', 'uses' => 'VersionsController@createForDeveloppement',   'middleware' => 'auth']);
    Route::get('lecons/{id}/versions/create',          ['as' => 'versions.create.lecon',         'uses' => 'VersionsController@createForLecon',           'middleware' => 'auth']);
    Route::post(  'versions',                          ['as' => 'versions.store',                'uses' => 'VersionsController@store',                    'middleware' => 'auth']);
    Route::get(   'versions/{id}/edit',                ['as' => 'versions.edit',                 'uses' => 'VersionsController@edit',                     'middleware' => 'auth']);
    Route::put(   'versions/{id}',                     ['as' => 'versions.update',               'uses' => 'VersionsController@update',                   'middleware' => 'auth']);
    Route::delete('versions/{id}',                     ['as' => 'versions.delete',               'uses' => 'VersionsController@destroy',                  'middleware' => 'auth']);
    Route::post(  'versions/{id}/uploadFile',          ['as' => 'versions.uploadFile',           'uses' => 'VersionsController@uploadFile']);
    Route::delete('versions/{id}/deleteFile',          ['as' => 'versions.deleteFile',           'uses' => 'VersionsController@deleteFile']);


    // Rekasator
    $rekasatorSection = "outils/rekasator";
    Route::get( $rekasatorSection.'/{annee}',  ['as' => 'rekasator.index',  'uses' => 'RekasatorController@index']);
    Route::post($rekasatorSection.'/{annee}',  ['as' => 'rekasator.result', 'uses' => 'RekasatorController@result']);

    // Retours
    $retourSection = "ressources/retours";
    Route::get( $retourSection,                 ['as' => 'retours.index',        'uses' => 'RetoursController@index']);
    Route::post($retourSection,                 ['as' => 'retours.store',        'uses' => 'RetoursController@store',   'middleware' => 'auth']);
    Route::get( $retourSection.'create',        ['as' => 'retours.create',       'uses' => 'RetoursController@create',  'middleware' => 'auth']);
    Route::post($retourSection.'create',        ['as' => 'retours.create2',      'uses' => 'RetoursController@create2', 'middleware' => 'auth']);
    Route::get( $retourSection.'edit/{retour}', ['as' => 'retours.edit',         'uses' => 'RetoursController@edit',    'middleware' => 'auth']);
    Route::put( $retourSection.'edit/{retour}', ['as' => 'retours.update',       'uses' => 'RetoursController@update',  'middleware' => 'auth']);
    Route::get( $retourSection.'{oral}',        ['as' => 'retours.index-oral',   'uses' => 'RetoursController@indexOral']);

    // Références
    Route::resource('ressources/references', 'ReferencesController', ['names' =>
        ['index' => 'references.index', 'store' => 'references.store', 'create' => 'references.create', 'update' => 'references.update', 'destroy' => 'references.destroy', 'show' => 'references.show', 'edit' => 'references.edit']]);

    // Utilisateurs
    Route::resource('users', 'UsersController', ['except' => ['index', 'create', 'delete']]);

    // Couplages
    $couplageSection = "couplages";
    Route::put(   $couplageSection.'/add',              ['as' => 'couplages.developpements.add',    'uses' => 'CouplagesController@addDeveloppement',     'middleware' => 'auth']);
    Route::put(   $couplageSection.'/update/{couplage}',['as' => 'couplages.developpements.update', 'uses' => 'CouplagesController@updateDeveloppements', 'middleware' => 'auth']);
    Route::get(   $couplageSection.'/user',             ['as' => 'couplages.index.user',   'uses' => 'CouplagesController@indexUser', 'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user',             ['as' => 'couplages.store',        'uses' => 'CouplagesController@store',     'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/getDevsMoreData',   ['as' => 'couplages.ajax.getDevsMoreData',      'uses' => 'CouplagesController@ajaxGetDevsMoreData',   'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/getPropoRempla',    ['as' => 'couplages.ajax.getPropoRempla',       'uses' => 'CouplagesController@ajaxGetPropoRempla',    'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/getPropoRemplaUn',  ['as' => 'couplages.ajax.getPropoRemplaUn',     'uses' => 'CouplagesController@ajaxGetPropoRemplaUn',  'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/getRecasages',      ['as' => 'couplages.ajax.getRecasages',         'uses' => 'CouplagesController@ajaxGetRecasages',      'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/developpement',     ['as' => 'couplages.ajax.developpement',        'uses' => 'CouplagesController@ajaxDeveloppement',     'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/couverture',        ['as' => 'couplages.ajax.couverture',           'uses' => 'CouplagesController@ajaxCouverture',        'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/commentaires',      ['as' => 'couplages.ajax.commentaires',         'uses' => 'CouplagesController@ajaxCommentaires',      'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/references',        ['as' => 'couplages.ajax.references',           'uses' => 'CouplagesController@ajaxReferences',        'middleware' => 'auth']);
    Route::post(  $couplageSection.'/user/ajax/exportSettings',    ['as' => 'couplages.ajax.exportSettings',       'uses' => 'CouplagesController@ajaxExportSettings']);
    Route::get(   $couplageSection.'/pdf/{couplage}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}/{g?}/{h?}/{i?}',    ['as' => 'couplages.export',   'uses' => 'CouplagesController@export']);
    Route::get($couplageSection.'/user/ajax', ['as' => 'couplages.ajax']);


    Route::put(   $couplageSection.'/{couplage}',       ['as' => 'couplages.update',       'uses' => 'CouplagesController@update',    'middleware' => 'auth']);
    Route::get(   $couplageSection.'/{couplage}',       ['as' => 'couplages.show',         'uses' => 'CouplagesController@show']);
    Route::get(   $couplageSection.'/{couplage}/edit',  ['as' => 'couplages.edit',         'uses' => 'CouplagesController@edit',      'middleware' => 'auth']);
    Route::delete($couplageSection.'/{id}',             ['as' => 'couplages.delete',       'uses' => 'CouplagesController@destroy',   'middleware' => 'auth']);
    Route::get( $couplageSection.'/export/html/{couplage}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}/{g?}/{h?}/{i?}', ['as' => 'couplages.export.html',         'uses' => 'CouplagesController@export_html']); 

    // PDF
    Route::get('/pdf/couplage/{couplage}',    ['as' => 'pdf.couplage',   'uses' => 'PdfController@couplage']);
    Route::get('/pdf/couplage/html/{couplage}', ['as' => 'pdf.couplage_html',   'uses' => 'PdfController@couplage_html'] );


    // Auth
    Route::get('login',  ['as' => 'auth.login.form', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::post('login', ['as' => 'auth.login',      'uses' => 'Auth\AuthController@login']);
    Route::get('logout', ['as' => 'auth.logout',     'uses' => 'Auth\AuthController@logout']);
    // Register
    Route::get( 'register', ['as' => 'auth.register.form', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    Route::post('register', ['as' => 'auth.register',      'uses' => 'Auth\AuthController@register']);
    // Password forgotten
    Route::get( 'password/reset', ['as' => 'auth.password.form', 'uses' => 'Auth\ReminderController@create']);
    Route::post('password/reset', ['as' => 'auth.password',      'uses' => 'Auth\ReminderController@store']);
    Route::get( 'password/reset/{id}/{token}', ['as' => 'auth.password.reset.form', 'uses' => 'Auth\ReminderController@edit']);
    Route::post('password/reset/{id}/{token}', ['as' => 'auth.password.reset',      'uses' => 'Auth\ReminderController@update']);
});
