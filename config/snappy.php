<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => env('wkhtmltopdf'),
        'timeout' => false,
        'options' => array(
            'javascript-delay' => '1000',
            'margin-top'    => 10,
            'margin-right'  => 0,
            'margin-bottom' => 10,
            'margin-left'   => 0,
        ),
        'env'     => array(),
        'tmp'   => '/tmp'
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => env('wkhtmltoimage'),
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
