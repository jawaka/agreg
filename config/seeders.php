<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Seeders Ranges
    |--------------------------------------------------------------------------
    */
    'developpements'      => 100,
    'lecons'              => 100,
    'references'          => 100,
    'users'               => 100,
    'versions'            => 400,
    'versions_references' => 700,
    'versions_fichiers'   => 700,

    'insert_limit'        => 900
];

?>