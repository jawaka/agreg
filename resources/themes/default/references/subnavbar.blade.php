@if($subpage == 'index')
            <ul>
                <li class="clickable">
                    {{ Html::link(route('retours.index'), 'Retours') }}
                </li>
                <li class="clickable active">
                    {{ Html::link(route('references.index'), 'Références') }}
                </li>
            </ul>
@else


            <ul>
                <li class="active clickable">
                    {{ Html::link(route('references.index'), 'Références') }}
                </li>

@if($subpage == 'show')
                <li class="separator"></li>
                <li>{{ $reference->titre }}</li>
@elseif($subpage == 'create')
                <li class="separator"></li>
                <li>Nouvelle référence</li>
            </ul>
@elseif($subpage == 'edit')
                <li class="separator"></li>
                <li class="active clickable">
                    {{ Html::link(route('references.show', $reference->id), $reference->titre) }}
                </li>
                <li class="separator"></li>
                <li>Modification</li>
            </ul>
@endif
            </ul>
@endif