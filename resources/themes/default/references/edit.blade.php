@extends('templates.full-page')

@section('title', "Modification d'une référence")
@section('subnavbar')
    @include('references.subnavbar', ['subpage' => 'edit'])
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>Modification d'une référence</h1>
    </div>
    </div>
</div>
<div class="container">
{!! Form::model($reference, ['route' => ['references.update', $reference->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
    {!! Form::hidden('id', $reference->id) !!}
    {!! Form::control('text', 'titre',   'Titre',   ['size' => 'lg']) !!}
    {!! Form::control('text', 'auteurs', 'Auteurs', ['size' => 'lg']) !!}

    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
@stop
