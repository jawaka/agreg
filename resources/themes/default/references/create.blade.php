@extends('templates.full-page')

@section('title', "Ajout d'une référence")
@section('subnavbar')
    @include('references.subnavbar', ['subpage' => 'create'])
@stop

@section('content')
<div class="jumbotron">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>Ajout d'une référence</h1>
    </div>
    </div>
</div>
<div class="strip">
<div class="container">
{!! Form::open(['route' => ['references.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
    {!! Form::control('text', 'titre',   'Titre',   ['size' => 'lg']) !!}
    {!! Form::control('text', 'auteurs', 'Auteurs', ['size' => 'lg']) !!}

    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
</div>
@stop