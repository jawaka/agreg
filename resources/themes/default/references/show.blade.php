@extends('templates.full-page')

@section('title', 'Référence : '.$reference->titre)
@section('subnavbar')
    @include('references.subnavbar', ['subpage' => 'show'])
@stop

@section('content')
    <div class="jumbotron">
    <div class="container">
        <h2>
            {{ $reference->titre }}
            @if(Sentinel::check())
            @if(Sentinel::hasAccess('reference.delete'))
            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#modal-delete">
                <i class="fa fa-times"></i>
                &nbsp; Supprimer
            </button>
            @endif
            <a href="{{ route('references.edit', $reference->id) }}" class="btn btn-primary pull-right">
                <i class="fa fa-pencil"></i>
                Modifier
            </a>
            @endif
        </h2>
        <p>
            {{ $reference->auteurs }}
        </p>
    </div>
    </div>

    <div class="strip" id="utilisation">
        <div class="container">

            

            <h3>
                Utilisée dans les {{ $refDevs->count() }} développements suivants :
             </h3>

            @foreach ($refDevs as $dev) 
                <a href="{{ route('developpements.show', [$dev->id]) }}">{{ $dev->nom }}</a> <br>
            @endforeach

            <br>

            <h3>
                Utilisée dans les {{ $refLecons->count() }} leçons suivantes :
             </h3>

            @foreach ($refLecons as $lecon) 
                {{ $lecon->numero }} ({{$lecon->annee}}) <a href="{{ route('lecons.show', [$lecon->id]) }}">{{ $lecon->nom }}</a> <br>
            @endforeach

            <br>
        </div>
    </div>

    <div class="strip" id="developpements">
    <div class="container">

       

        <h3>
           Utilisée dans les {{ $reference->versionsDeveloppements()->count() }} versions de développements suivants :
        </h3>
        @foreach ($reference->versionsDeveloppements as $version) 
            @include('partials.version_ext')
        @endforeach

        <br>
        
        <h3>
           Utilisée dans les {{ $reference->versionsLecons()->count() }} versions de leçons suivantes :
        </h3>
        @foreach ($reference->versionsLecons as $version) 
            @include('partials.version_ext')
        @endforeach
    </div>
    </div>
@stop

@if(Sentinel::check() && Sentinel::hasAccess('reference.delete'))
@section('modal')
    @include('partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cette référence ?', 'deleteRoute' => ['references.destroy', $reference->id]])
@stop
@endif