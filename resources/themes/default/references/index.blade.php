@extends('templates.full-page')

@section('title', 'Liste des références')
@section('subnavbar')
    @include('references.subnavbar', ['subpage' => 'index'])
@stop

@section('content')
<div id="references-container">
    <div class="jumbotron">
    <div class="container">
        <h1>Liste des références</h1>
        <p>
            Utilisées pour des plans de leçons, des développements, ...
            La colonne <i>Nb de versions</i> indique le nombre de versions de développements et de versions de leçons dans lesquelles la référence est utilisée.
            @if(Sentinel::check())
            <a href="{{ route("references.create") }}" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-plus"></span>
                Ajouter la votre
            </a>
            @endif
        </p>
    </div>
    </div>
    <table class="table table-striped table-bordered" id="references-table">
        <thead>
            <tr>
                <th>
                    Auteurs
                </th>
                <th>
                    Titre
                </th>
                <th>
                    Nb de versions
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($references as $reference)
        <tr class="clickable" data-href="{{ route('references.show', [$reference->id]) }}">
            <td>
                {{ $reference->auteurs }}
            </td>
            <td class="primary">
                <a href="{{ route('references.show', [$reference->id]) }}">
                    {{ $reference->titre  }}
                </a>
            </td>
            <td >
                {{ $reference->versionsDeveloppements()->count() + $reference->versionsLecons()->count() }}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop



@section('scripts')
<script type="text/javascript">

var dataRefs = [
    @foreach($references as $id => $elem)
    { id:{{$id}}, text:`{!! $elem !!}` },
    @endforeach
];



</script>
@stop

{{-- @section('scripts')
<script type="text/javascript">
// References datatable
$(document).ready(function() {
    function setSizes(){
        var ratio = 3;
        const r2 = 6;
        const r3 = 3;
        const sr = ratio + r2 + r3;

        var total = $('#references-table').width();
        var container = $('.jumbotron .container').innerWidth();
        var margin = 0;

        var s1 =  margin + (container) * ratio / sr;
        var s2 =  margin + (container) * r2 / sr;
        var s3 =  margin + (container) * r3 / sr;
        console.log(s1, s2, s3);
        console.log(total)
        console.log(container);

        // var th = $('#references-table th');
        // th.first().width(s1);
        // th.eq(2).width(s2)
        // th.last().width(s3);

        // var td = $('#references-table td');
        // td.filter(':nth-child(1)').width(s1); // Set the width of the first column
        // td.filter(':nth-child(2)').width(s2); // Set the width of the second column
        // td.filter(':nth-child(3)').width(s3); // Set the width of the third column

    }
    setSizes();
    $(window).resize(setSizes);
});
</script>
@stop --}}