@extends('templates.full-page')
@css('assets/css/bootstrap-tour.min.css')

@section('title', "Diagramme de Kiviat des développements")
@section('meta', "Cet outil permet de visualiser et comparer les développements en fonction de leurs recasages dans les leçons demandées")
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'kiviat'])
@stop

@section('content')
<div id="kiviat-wrapper">
    <div id="kiviat-lecons">
        <h3>Leçons</h3>
        <div>
            <ul>
            @foreach($lecons as $lecon)
                <li>
                    <div class="kiviat-lecon">
                        <a href='#' id="kiviat-liens-{{ $lecon->numero }}" title="{{ $lecon->nom }}">
                            {{ $lecon->numero }}
                            <i class="fa fa-plus-circle add"></i>
                            <i class="fa fa-minus-circle remove"></i>
                        </a>
                    </div>
                </li>
            @endforeach
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>

    <div id="kiviat-canvas">
        <div id="kiviat-buttons">
            <button type="button" id="kiviat-mode" class="btn btn-default pull-right unite"></button>
            <button type="button" id="kiviat-help" class="btn btn-default"><span class="glyphicon glyphicon-question-sign"></span></button>
        </div>
        <div id="kiviat-chart-container">
            <canvas id="kiviat-chart"></canvas>
        </div>
    </div>

    <div id="kiviat-developpements">
        <h3>Développements</h3>
        <ul>
        @foreach ($developpements as $developpement)
            <li id="developpement_{{ $developpement->id }}">
                <div><div></div></div>
                <a href="{{ route('developpements.show', [$developpement->id]) }}">
                    <i class="fa fa-arrow-circle-o-right"></i>
                </a>
               {{ $developpement->nom  }}
            </li>
        @endforeach
        </ul>
        <ul></ul>
    </div>
</div>
@stop
