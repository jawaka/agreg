@extends('templates.full-page')

@section('title', $developpement->nom)
@section('meta', $developpement->details)
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'show'])
@stop

@section('content')
<section id="developpement" class="developpement">
    <div class="jumbotron">
    <div class="container">
        <h1>Développement :
            {{ $developpement->nom }}
            @if($user = Sentinel::check())
            @if((Sentinel::hasAccess('developpement.delete')))
            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#modal-delete">
                <i class="fa fa-times"></i>
                &nbsp; Supprimer
            </button>
            @endif
            @if(($user->id == $developpement->user_id) || (Sentinel::hasAccess('developpement.update')))
            <a href="{{ route('developpements.edit', $developpement->id) }}" class="btn btn-primary pull-right">
                <i class="fa fa-pencil"></i>
                Modifier
            </a>
            @endif
            @endif
        </h1>

        @if(!isset($couplage) && getCouplage())
            <button type="button" class="btn btn-sm btn-success developpement-add-btn" data-id="{{ $developpement->id }}">
                <i class="fa fa-plus"></i>
                &nbsp;
                Ajouter à votre couplage
            </button>
        @endif
    </div>
    </div>

@if(isset($couplage))
    <div class="strip {!! $couplage->isLocked($developpement->id)? 'locked' : '' !!}" id="couplage" data-id="{{ $developpement->id }}">
    <div class="container">
        <div id="commentaire">
            <button type="button" class="btn btn-success">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
            <textarea rows="1" data-id="{{ $developpement->id }}" placeholder="Pas de commentaires personnels pour ce développement">{!!
                ($user->hasDCommentaire($developpement->id))?
                    $user->getDCommentaire($developpement->id)->commentaire :
                    ''
                !!}</textarea>
        </div>

        <div id="references">
            <select data-placeholder="Rajouter une référence personnelle pour ce développement" class="chosen-select developpements-references" multiple data-id="{{ $developpement->id }}">
                @foreach($references as $id => $elem)
                    <option value="{{$id}}"{!! $user->hasDReference($developpement->id, $id)? ' selected="selected"' : ''!!}>{{ $elem }}</option>
                @endforeach
            </select>
        </div>


        <h2>
            Vos recasages dans votre couplage :
            <button type="button" class="developpement-lock">
                <i class="fa"></i>
            </button>
            <button type="button" class="developpement-delete" data-id="{{ $developpement->id }}">
                <span class="glyphicon glyphicon-trash"></span>
            </button>
        </h2>
        <form autocomplete="off">
        <ul class="couplage-lecon">
            @foreach($recasages as $id => $l)
            <li class="list-group-item couverture-rating couverture-rating-{{ $l->pivot->qualite }} {{ $couplage->couvertureHas($id, $developpement->id)? 'checked' : ''}}"
                data-id="{{ $developpement->id }}" data-lecon-id="{{ $id }}">
                <div class="couverture-checkbox {{ $couplage->couvertureHas($id, $developpement->id)? 'checked' : ''}}" id="couverture_{{ $id }}_{{ $developpement->id }}"></div>
                {!! Form::rating('couverture_'.$developpement->id.'['.$id.'][qualite]', $l->pivot->qualite, 5, false, $couplage->isLocked($developpement->id)) !!}
                <button type="button" class="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
                <div>
                    <a href="{{ route('lecons.show', $id) }}">
                        {{ $l->numero.' : ' .$l->nom }}
                    </a>
                </div>
            </li>
            @endforeach

            <li class="list-group-item clearfix rating-add-container">
                <div class="input-group">
                    <div class="btn input-group-addon rating-add couverture-add-btn" data-id="{{ $developpement->id }}">
                        <span class="glyphicon glyphicon-plus"></span>
                    </div>
                    <select data-placeholder="Choisir une leçon..." class="form-control chosen-select">
                        <option value=""></option>
                        @foreach($couplage->lecons() as $l)
                            <option value="{{ $l->id }}"{!! $recasages->has($l->id)? ' disabled' : ''!!}>{{ $l->numero.' : '.$l->nom }}</option>
                        @endforeach
                  </select>
                </div>
            </li>
        </ul>
        </form>
    </div>
    </div>
@endif



@if(!isset($couplage) && $user)
    <div class="strip" id="commentaire">
    <div class="container">
        <button type="button" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span>
        </button>
        <textarea rows="1" data-id="{{ $developpement->id }}" placeholder="Pas de notes personnelles pour ce développement">{!!
            ($user->hasDCommentaire($developpement->id))?
                $user->getDCommentaire($developpement->id)->commentaire :
                ''
            !!}</textarea>

        <div id="references">
            <select data-placeholder="Rajouter une référence personnelle pour ce développement" class="chosen-select developpements-references" multiple data-id="{{ $developpement->id }}">
                @foreach($references as $id => $elem)
                    <option value="{{$id}}"{!! $user->hasDReference($developpement->id, $id)? ' selected="selected"' : ''!!}>{{ $elem }}</option>
                @endforeach
            </select>
        </div>
    </div>
    </div>
@endif

    <div class="strip" id="details">
    <div class="container">
        <h2>Détails/Enoncé : </h2>
        <p>
            {!! nl2br($developpement->details) !!}
        </p>
    </div>
    </div>

@if(!isset($couplage))
    <div class="strip" id="votes">
@if($user)
    @if($developpement->votesOfUser($user->id)->get()->isEmpty())
    <div class="container collapse in" id="show-votes">
        <p>
            <a href="#edit-votes" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="edit-votes">
                Vous n'êtes pas d'accord avec les recasages ci-dessous ? Proposez les vôtres ici !
            </a>
        </p>
    </div>
    <div class="container collapse" id="edit-votes">
    @else
    <div class="container" id="edit-votes">
    @endif

        <h2>Vos recasages : </h2>
        {!! Form::model($developpement, ['route' => ['developpements.votes.update', $developpement->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('id', $developpement->id) !!}

            <div class="form-group form-group-lg">
                <ul id="list-votes">
                {!! Form::lRating('votes',
                    ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...',
                     'val' => $developpement->votesOfUser($user->id)->get()->toRatingArray('qualite')]) !!}
                </ul>
            </div>

            {!! Form::submitbtn('Mettre à jour', ['col' => 0]) !!}
        {!! Form::close() !!}
    </div>
@else
    <div class="container" id="show-votes">
        <p>
            <a href="http://www.agreg.fr/login" class="login-modal-toggle">
                Vous n'êtes pas d'accord avec les recasages ci-dessous ? Connectez-vous pour proposer les vôtres !
            </a>
        </p>
    </div>
@endif
    </div>
@endif


    <div class="strip" id="lecons">
    <div class="container">
        <h2>
            <button class="collapsed" data-target="#old-lecons" data-toggle="collapse" aria-expanded="false" aria-controls="old-lecons" role="button">
                Afficher les autres années &nbsp;<span></span>
            </button>
            Recasages pour l'année {{ config('app.annee') }} :
        </h2>
        <ul>
        @if(isset($recasagesByYear[config('app.annee')]))
        @foreach($recasagesByYear[config('app.annee')]  as $rlecon)
            <li>
                <a href="{{ route('lecons.show', $rlecon->id) }}">
                    {!! Html::rating($rlecon->pivot->qualite) !!}
                    {{ $rlecon->numero. ($rlecon->isD? '' : '').' : ' .$rlecon->nom }}
                </a>
            </li>
        @endforeach
        @else
            <li>Pas de recasages pour cette année.</li>
        @endif
        </ul>
    </div>
    <div class="container collapse" id="old-lecons">
        <h2>Autres années : </h2>
        <ul>
        @foreach($recasagesByYear as $year => $recasages)
        @if($year != config('app.annee'))
        @foreach($recasages as $rlecon)
            <li>
                <a href="{{ route('lecons.show', $rlecon->id) }}">
                    {!! Html::rating($rlecon->pivot->qualite) !!}
                    ({{ $rlecon->annee }})
                    {{ $rlecon->numero. ' : ' .$rlecon->nom }}
                </a>
            </li>
        @endforeach
        @endif
        @endforeach
        </ul>
    </div>
    </div>


    <div class="strip" id="versions">
    <div class="container">
        <h2>
            Versions :
            @if(Sentinel::check())
            <a href="{{ route('versions.create.developpement', $developpement->id) }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i>
                Ajouter votre version
            </a>
            @endif
        </h2>
            @forelse($developpement->versions as $version)
                @include('partials.version')
            @empty
                Pas de version pour ce développement.
            @endforelse
    </div>
    </div>

    {{-- Réferences --}}
    <div class="strip" id="references">
        <div class="container">
            <h2>
            Références utilisées dans les versions de ce développement :
            </h2>
            @foreach ($developpement->getReferences() as $ref)
                <a href="{{ route('references.show', [$ref->id]) }}">{{ $ref->titre }}</a>, {{$ref->auteurs}} (utilisée dans {{$ref->versionsDeveloppements()->count() + $ref->versionsLecons()->count()}} versions au total) <br>
            @endforeach

        </div>
    </div>

</section>
@stop


@section('scripts')
<script type="text/javascript">
var couplageAjaxUrl = "{{ route('couplages.ajax') }}";
</script>
@stop


@section('modal')
<div class="modal fade" id="modal-developpement"></div>
@include('partials.confirm-delete', ['modalId' => 'modal-developpement-delete', 'formDelete' => false, 'msgconfirm' => 'Attention, ce développement est utilisé dans des leçons de votre couplage. Voulez-vous quand même le supprimer de votre couplage ?'])

@if(Sentinel::check() && Sentinel::hasAccess('developpement.delete'))
    @include('partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer ce développement ?'])
@endif
@stop
