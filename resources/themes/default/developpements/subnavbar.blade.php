@if($subpage == 'show')
            <ul>
                <li class="active clickable">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="separator"></li>
                <li>{{ $developpement->nom }}</li>
            </ul>
@elseif($subpage == 'create')
            <ul>
                <li class="active clickable">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="separator"></li>
                <li>Nouveau développement</li>
            </ul>
@elseif($subpage == 'edit')
            <ul>
                <li class="active clickable">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="separator"></li>
                <li class="active clickable">
                    {{ Html::link(route('developpements.show', $developpement->id), $developpement->nom) }}
                </li>
                <li class="separator"></li>
                <li>Modification</li>
            </ul>
@elseif($subpage == 'version.create')
            <ul>
                <li class="active clickable">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="separator"></li>
                <li class="active clickable">
                    {{ Html::link(route('developpements.show', $versionable->id), $versionable->nom) }}
                </li>
                <li class="separator"></li>
                <li>Nouvelle version</li>
            </ul>
@elseif($subpage == 'version.edit')
            <ul>
                <li class="active clickable">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="separator"></li>
                <li class="active clickable">
                    {{ Html::link(route('developpements.show', $version->versionable->id), $version->versionable->nom) }}
                </li>
                <li class="separator"></li>
                <li>
                    Version de {{ $version->user->name }}
                </li>
                <li class="separator"></li>
                <li>Modification</li>
            </ul>
@else
            <ul>
                <li class="clickable {{ $subpage === 'list' ? 'active' : null }}">
                    {{ Html::link(route('developpements.index'), 'Lister') }}
                </li>
                <li class="clickable {{ $subpage === 'kiviat' ? 'active' : null }}">
                    {{ Html::link(route('developpements.kiviat', config('app.annee')), 'Kiviat') }}
                </li>
            </ul>
@endif
{{--
            {{ Form::open([
                'url' => '#', 
                'method' => 'post', 
                'class' => 'subnavbar-right', 
                'id' => 'search_form', 
                'role' => 'search'])
            }}
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Rechercher par mot-clef" name="search" id="search_field" />
                        <div class="input-group-btn">
                            <button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
--}}