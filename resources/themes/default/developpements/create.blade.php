@extends('templates.full-page')

@section('title', "Création d'un développement")
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'create'])
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>Création d'un développement</h1>
    </div>
    </div>
</div>
<div class="container">
    Merci de vérifier au préalable que le développement que vous voulez ajouter n'existe pas déjà sur le site pour éviter les doublons : vous pouvez utiliser la barre de recherche de <a href=".">cette page</a>.

    De plus, merci de rajouter une description, même courte, du résultat démontré ainsi que quelques recasages même s'ils ne sont pas précis.
    <br>
    <br>

{!! Form::open(['route' => ['developpements.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}

    {!! Form::control('text',     'nom',       'Nom',     ['size' => 'lg']) !!}
    {!! Form::control('textarea', 'details',   'Details', ['size' => 'lg']) !!}
    {!! Form::control('lRating',  'recasages', 'Recasages', 
        ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...', 'size' => 'lg']) !!}

    {!! Form::submitbtn('Envoyer', ['class' => 'btn btn-primary btn-lg']) !!}
{!! Form::close() !!}
</div>
@stop