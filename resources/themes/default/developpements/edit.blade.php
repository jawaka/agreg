@extends('templates.full-page')

@section('title', "Modification d'un développement")
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'edit'])
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>Modification d'un développement</h1>
    </div>
    </div>
</div>
<div class="container">
{!! Form::model($developpement, ['route' => ['developpements.update', $developpement->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
    {!! Form::hidden('id', $developpement->id) !!}
    {!! Form::control('text',     'nom',       'Nom',     ['size' => 'lg']) !!}
    {!! Form::control('textarea', 'details',   'Details', ['size' => 'lg']) !!}
    {!! Form::control('lRating',  'recasages', 'Recasages', 
        ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...', 'size' => 'lg',
         'val' => $developpement->recasages->toRatingArray('qualite')]) !!}

    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
@stop
