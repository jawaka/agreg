@extends('templates.full-page')

@section('title', "Développements de mathématiques pour l'agrégation")
@section('meta', "Une liste de développements de mathématiques avec des propositions de recasages, des références, et des scans pdf.")
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'list'])
@stop

@section('content')
<div id="developpements-container">
    <div class="jumbotron">
    <div class="container">
        <h1>Liste des développements</h1>
        <p>
            Avec leurs recasages, leurs différentes versions, leurs références, ...
            @if(Sentinel::check())
            <a href="{{ route("developpements.create") }}" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-plus"></span>
                Ajouter le votre
            </a>
            @endif
        </p>
    </div>
    </div>

    <table class="table table-striped table-bordered" id="developpements-table">
        <thead>
            <tr>
                <th>
                    <div>
                    Nom
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($developpements as $developpement)
        <tr>
            <td>
                <div>
                <a href="{{ route('developpements.show', [$developpement->id]) }}">
                    {{ $developpement->nom  }}
                </a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop