@extends('templates.full-page')
@section('title', "Agreg-maths.fr : le couteau suisse de l'agrégatif de mathématiques")
@section('meta', "Ce site regroupe des plans de leçons, des développements, des rapports de jury, un éditeur de couplage et encore d'autres outils facilitant la préparation à l'agregation de mathématiques.")

@section('subnavbar')

@stop

@section('content')
<div id="index-page">

<div class="jumbotron">
  <div class="container">
    <h1>Nous soutenir</h1>
  </div>
</div>
<div class="strip">
<div class="container">
<h2>Historique</h2>
<p style="font-size:16px; line-height:24px">
Durant notre année de préparation à l'agrégation en 2014-2015, nous avons pu constater qu'il existait assez peu de ressources dédiées à la préparation de ce concours exigeant.
À l'ENS de Lyon, nous avions accès à des classeurs des promotions précédentes, véritables mines d'or de développements et de plans de leçon, et nous avons souhaité que le plus grand nombre puisse en profiter.
C'est ainsi qu'après une première version du site en interne en 2015 (les vieux se souviendront du fameux "site du canard"), nous avons commencé à travailler sur une version ouverte à tous début 2016.
Après plusieurs mois de travail, le site agreg-maths était fin prêt pour accueillir les agrégatifs de 2017 !
Le site se fit connaître en cours d'année, et enregistra rapidement des milliers de visiteurs par mois.

<br />
<br />
<img src="assets/images/stats_2024_mars.png" class="img-responsive center-block" alt="Stats" title="Il manque des données entre 2020 et 2021 à cause d'un bug." />
<br />

Le site ne nous demande plus beaucoup de temps de gestion car c'est vous, les utilisateurs, qui échangez et partagez vos ressources.
Cependant, le site nécessite un hébergement internet pour fonctionner, hébergement qui nous coûte environ 100€ par an que nous payons de notre poche car nous ne souhaitons pas mettre en place de publicité.
Ainsi, si le site a pu vous être utile et que vous souhaitez l'aider à vivre et à se développer (à quand la v2 ?), vous avez la possibilité d'effectuer un don par virement bancaire ou par Paypal via le bouton ci-dessous.
<br />
Merci d'avance pour votre générosité !
</p>

<br />
<div class="text-center">
	<form action="https://www.paypal.com/donate" method="post" target="_top">
	<input type="hidden" name="business" value="P65Z3GQYZVC54" />
	<input type="hidden" name="item_name" value="Soutien au site agreg-maths.fr" />
	<input type="hidden" name="currency_code" value="EUR" />
	<input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donate_LG.gif" border="0" name="submit" title="Don par virement bancaire ou par Paypal" alt="Bouton Faites un don" />
	<img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
	</form>
</div>

</div>
</div>
<div class="strip" style="background-color:#e1ecf2">
<div class="container">
<h2>Qui sommes-nous ?</h2>
<p>
	<a href="https://www.lirmm.fr/~isenmann/">Lucas Isenmann</a> : après avoir obtenu l'agrégation en 2015, il effectue une thèse en informatique à Montpellier dans l'équipe ALGCO (LIRMM). 
</p>
<p>
<a href="http://perso.ens-lyon.fr/timothee.pecatte/">Timothée Pecatte</a> : après avoir obtenu l'agrégation (option D) en 2015, il effectue une thèse en informatique à Lyon dans l'équipe MC2 (LIP) avant d'obtenir un poste de PRAG à l'INSA Lyon en septembre 2018.
</p>
</div>
</div>

</div>
@stop
