@extends('templates.full-page')
@section('title', "Agreg-maths.fr : le couteau suisse de l'agrégatif de mathématiques")

@section('subnavbar')

@stop

@section('content')
<div id="tirages">
<div class="jumbotron">
    <div class="container">
        <h1>Quelques tirages de l'année 2015</h1>
    </div>
</div>
<div class="strip">
<div class="container">
    <p>
    Voici une liste de tirages qui sont tombés lors de l'année 2015.
    Les numérotations des leçons ont été rapportées à celles de l'année 2017 lorsque c'était possible.
    Ce sont des agrégatifs, très certainement tout comme vous, qui les ont recueillis lorsqu'ils assistaient à des oraux avant d'aller passer les leurs.
    <br />
    Cette liste peut vous donner une idée pour savoir à peu près quels tirages tombent et surtout ceux qui ne tombent pas. Un générateur aléatoire de tirage va être développé très prochainement et utilisera ces échantillons comme base de l'algorithme.

    <br />
    <br />
    Si vous trouvez ça utile vous pouvez nous <b>aider à récupérer ceux de cette année</b>.
    <br />
    Comment faire ? Rien de plus simple : allez sur <a href="https://framacalc.org/z4nUoEMTUH">https://framacalc.org/z4nUoEMTUH</a> et rajouter les tirages que vous voyez pour une session à telle heure et tel jour.
    Si la ligne est déjà remplie : tant mieux quelqu'un d'autre a déjà saisi ces tirages !
    Le but de cette feuille de calcul coopératif, en plus de se vouloir pratique, est aussi d'éviter les doublons sur les tirages recueillis.
    Notez qu'en plus des tirages que vous avez vu aux oraux, vous pouvez aussi rajouter les tirages que vous avez vous même piochés.
    </p>

    <h2>Liste de tirages d'algèbre tombés en 2015 :</h2>

    <ul class="list-group">
    @foreach ($algebre as $tirage)
        <li class="list-group-item row">
            <a href="{{ route('lecons.show', $lecons[$tirage[0]]->id) }}" class="col-sm-5">{{ $tirage[0] }} : {{ $lecons[$tirage[0]]->nom }}</a>
            <div class="col-sm-1">-</div>
            <a href="{{ route('lecons.show', $lecons[$tirage[1]]->id) }}" class="col-sm-6">{{ $tirage[1] }} : {{ $lecons[$tirage[1]]->nom }}</a>
        </li>
    @endforeach
    </ul>
    <br />

    <h2>Liste de tirages d'analyse tombés en 2015 :</h2>
    <ul class="list-group">
    @foreach ($analyse as $tirage)
        <li class="list-group-item row">
            <a href="{{ route('lecons.show', $lecons[$tirage[0]]->id) }}" class="col-sm-5">{{ $tirage[0] }} : {{ $lecons[$tirage[0]]->nom }}</a>
            <div class="col-sm-1">-</div>
            <a href="{{ route('lecons.show', $lecons[$tirage[1]]->id) }}" class="col-sm-6">{{ $tirage[1] }} : {{ $lecons[$tirage[1]]->nom }}</a>
        </li>
    @endforeach
    </ul>


</div>
</div>
</div>
@stop
