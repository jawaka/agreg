<?php $section = Request::segment(1); ?>
<nav id="navbar">
    <div class="container-fluid">
        <div id="header">
            <button type="button" class="collapsed" data-toggle="collapse" data-target="#navbar-collapsable" aria-expanded="false">
                <span class="sr-only">Activer la navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div id="couteau-suisse">
              <div id="outil1"></div>
              <div id="outil2"></div>
              <div id="outil3"></div>
            </div>
            <a href="/">
                <span>Agreg</span>-<span>Maths</span>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapsable">
            <ul>
                <li {!! $section === 'lecons' ? 'class="active"' : null !!}>
                    {{ Html::link(route('lecons.index.annee', [config('app.annee')]), 'Leçons') }}
                </li>
                <li {!! $section === 'developpements' ? 'class="active"' : null !!}>
                    {{ Html::link(route('developpements.index'), 'Développements') }}
                </li>
                <li {!! $section === 'tirages' ? 'class="active"' : null !!}>
                    <a href="{{ route('tirages')}}">
                        Tirages
                    </a>
                </li>
                <li {!! $section === 'outils' ? 'class="active"' : null !!}>
                    {{ Html::link(route('rekasator.index', config('app.annee')), 'Outils') }}
                </li>
                @if($user = Sentinel::check())
                <li {!! $section === 'couplages' ? 'class="active"' : null !!}>
                    {{ Html::link(route('couplages.index.user'), 'Couplage') }}
                </li>
                @endif
                <li {!! $section === 'ressources' ? 'class="active"' : null !!}>
                    {{ Html::link(route('retours.index'), 'Ressources') }}
                </li>
                <li>
                    {{ Html::link(route('soutien'), 'Nous soutenir') }}
                </li>
                <li>
                    {{ Html::link(route('livre'), 'Notre livre') }}
                </li>
            </ul>

            <ul>
            @if($user = Sentinel::check())
                @if(Sentinel::hasAccess('section.admin'))
                <li>{{ Html::link('admin', 'Admin', ['style' => 'color:red']) }}</li>
                @endif

                <li {!! $section === 'users' ? 'class="active"' : null !!}>
                    <a href="{{ route('users.show', $user->id) }}">{{ $user->name }}</a>
                </li>
                <li><a href="{{ route('auth.logout') }}"><span class="glyphicon glyphicon-log-out"></span></a></li>
            @else
                <li {!! $section === 'register' ? 'class="active"' : null !!}>
                    <a href="{{ route('auth.register') }}" id="navbar-register-link">S'inscrire</a>
                </li>
                <li {!! in_array($section,['login', 'password']) ? 'class="active"' : null !!}>
                    <a href="{{ route('auth.login') }}" id="navbar-login-link" class="login-modal-toggle">Se connecter</a>
                </li>
            @endif
            </ul>
        </div>
    </div>
</nav>

<nav id="subnavbar">
    <div class="container-fluid">
        <div id="subnavbar-header">
            <span>Navigation</span>
            <button type="button" class="collapsed" data-toggle="collapse" data-target="#subnavbar-collapsable" aria-expanded="false">
                <span class="sr-only">Activer la sous-navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="subnavbar-collapsable">
        @yield('subnavbar')
        </div>
    </div>
</nav>
