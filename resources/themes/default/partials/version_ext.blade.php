<div class="version">
    <div class="collapsed" data-target="#version-{{ $version->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="version-{{ $version->id }}" role="button">
        @if($user = Sentinel::check())
        @if (($user->id == $version->user_id) || (Sentinel::hasAccess('version.delete')))
        <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#modal-delete-{{ $version->id }}">
            <i class="fa fa-times"></i>
            &nbsp; Supprimer
        </button>
        @endif
        @if (($user->id == $version->user_id) || (Sentinel::hasAccess('version.update')))
        <a href="{{ route('versions.edit', $version->id) }}">
            <i class="fa fa-pencil"></i>
            &nbsp; Modifier
        </a>
        @endif
        @endif
        <h3>
            @if($version->versionable_type == 'developpement')
                {{ $version->versionable->nom }}
            @else
                {{ $version->versionable->numero }} : {{ $version->versionable->nom }}
            @endif
            <span class="caret"></span>
        </h3>

    </div>
    <div class="collapse" id="version-{{ $version->id }}">
        <ul>
            <li>
                @if($version->versionable_type == 'developpement')
                    <strong>Développement :</strong>
                    <div>
                        <a href="{{ route('developpements.show', $version->versionable->id) }}">{{ $version->versionable->nom }}</a>
                    </div>
                @else
                    <strong>Leçon :</strong>
                    <div>
                        <a href="{{ route('lecons.show', $version->versionable->id) }}">{{ $version->versionable->nom }}</a>
                    </div>
                @endif                
            </li>

            @if(!empty($version->remarque))
            <li>
                <strong>Remarque :</strong> 
                <div>
                    {!! nl2br($version->remarque) !!}
                </div>
            </li>
            @endif

            @if(count($version->references) > 0)
            <li>
                <strong>Référence{{ (count($version->references) > 1)? 's' : null }} :</strong> 
                <ul>
                @foreach($version->references as $ref)
                    <li>
                        <a href="{{ route('references.show', $ref->id) }}">{{ $ref->titre }}, {{ $ref->auteurs }}</a>
                    </li>
                @endforeach
                </ul>
            </li>
            @endif


            @if(count($version->fichiers) > 0)
            <li>
                <strong>Fichier{{ (count($version->fichiers) > 1)? 's' : null }} :</strong> 
                <ul>
                @foreach($version->fichiers as $f)
                    <li>
                        <a href="{{ $version->url . $f->url }}">
                            {!! Html::fileIcon($version->url . $f->url, false) !!}&nbsp;
                            {{ $f->url }}
                        </a>
                    </li>
                @endforeach
                </ul>
            </li>
            @endif
        </ul>
    </div>
</div>
@if($user = Sentinel::check())
@if (($user->id == $version->user_id) || (Sentinel::hasAccess('version.delete')))
    @include('partials.confirm-delete', [
        'msgconfirm' => 'Voulez-vous vraiment supprimer cette version ?',
        'modalId'    => 'modal-delete-'.$version->id,
        'deleteRoute'=> ['versions.delete', $version->id]
    ])
@endif
@endif