<div class="modal fade bs-example-modal-sm" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="login-label">Connexion</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'login', 'method' => 'post', 'class' => 'form-horizontal'])}}
                    {!! Form::control('text', 'login', 'Login', 'Adresse email ou pseudo', $errors, 0) !!}
                    {!! Form::control('password', 'password', 'Mot de passe', 'Mot de passe', $errors, 0) !!}

                    <div class="form-group">
                        <div class="col-md-10">
                            {!! Form::bcheckbox('remember', 'Se souvenir de moi', 1, false) !!}
                        </div>
                        <div class="col-md-2">
                            <a class="btn btn-link pull-right" href="{{ url('/password/reset') }}">Mot de passe oublié ?</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            {!! Form::submit('Connexion', ['class' => 'btn btn-primary btn-lg center-block']) !!}
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer" style="text-align:center;">
                Vous n'avez pas de compte ? <a href="{{ url('/register') }}" class="btn-link">S'inscrire >></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="register-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="login-label">Inscription</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'register', 'method' => 'post', 'class' => 'form-horizontal'])}}

                    {!! Form::control('text', 'name', 'Nom', 'Nom ou pseudo', $errors, 0) !!}
                    {!! Form::control('email', 'email', 'Email', 'Adresse email', $errors, 0) !!}
                    {!! Form::control('password', 'password', 'Mot de passe', 'Mot de passe', $errors, 0) !!}
                    {!! Form::control('password', 'password_confirmation', 'Confirmer', 'Confirmer le mot de passe', $errors, 0) !!}
                    {!! Form::control('text', 'captcha', '82+9=', '', $errors, 0) !!}

                    <script src="https://www.google.com/recaptcha/api.js"></script>
                    <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}" data-callback="onRecaptchaSuccess"></div>

                    {!! Form::hidden('recaptcha_response', null, ['id' => 'recaptchaResponse']) !!}

                    <script>
                        function onRecaptchaSuccess(response) {
                            // The user has successfully completed the reCAPTCHA verification
                            // You can now proceed with your desired action
                            console.log('reCAPTCHA verification successful');
                            console.log('Response:', response);
                            document.getElementById('recaptchaResponse').value = response;
                            const button = document.getElementById("submitBtn");
                            button.style.display = "block";
                        }
                     </script>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button id="submitBtn" type="submit" class="btn btn-primary btn-lg center-block" style="display: none;">S'inscrire</button>
                            {{-- {!! Form::submit("S'inscrire", ['class' => 'btn btn-primary btn-lg center-block']) !!} --}}
                        </div>
                    </div>

                {{ Form::close() }}
            </div>
            <div class="modal-footer" style="text-align:center;">
                Vous avez déjà un compte ? <a href="{{ url('/login') }}" class="btn-link">Connectez-vous >></a>
            </div>
        </div>
    </div>
</div>