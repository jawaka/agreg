{{-- Confirm Delete --}}
<div class="modal fade" id="{{ isset($modalId)? $modalId : 'modal-delete' }}">
    <div class="modal-dialog">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    ×
                </button>

                <h4 class="modal-title">Confirmer la suppresion</h4>
            </div>
            <div class="modal-body">
                <p class="lead">
                    {{ $msgconfirm }}
                </p>
            </div>

            <div class="modal-footer">
            @if(isset($formDelete) && !$formDelete)
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-danger">Oui</button>
            @else
                {!!  Form::open([
                    'method' => 'DELETE',
                    'id'     => isset($deleteFormId)? $deleteFormId : '',
                    'route'  =>  (isset($deleteRoute)) ? $deleteRoute : ([
                        (Request::segment(1) .'.destroy'), 
                        Request::segment(2)
                    ])
                ]) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-danger">Oui</button>
                {!! Form::close() !!}
            @endif
            </div>
        </div>
    </div>
</div>