@if(isset($ok) || (session()->has('ok')))
<div class="alert alert-success alert-dismissible" role="alert" id="message">
    {!! $ok or session('ok') !!}
    
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if(isset($error) || (session()->has('error')))
<div class="alert alert-danger alert-dismissible" role="alert" id="message">
    {!! $error or session('error') !!}
    
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
