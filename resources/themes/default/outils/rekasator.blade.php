@extends('templates.full-page')

@section('title', 'Rekasator')
@section('subnavbar')
    @include('outils.subnavbar', ['subpage' => 'rekasator'])
@stop

@section('content')
<div class="white" id="content">
    <div class="jumbotron">
        <div class="container">
        <div class="col-md-offset-2">
            <h1 class="">Rekasator</h1>
            <p>
                Cet outil génère aléatoirement un <b>couplage</b> : c'est-à-dire un ensemble de développements qui couvrent les leçons demandées en cherchant à minimiser le nombre de développements.
                
                <br><br>

                Utilisation : 
                <ul>
                    <li><b>Qualité minimale</b> : les recasages proposés auront au moins cette qualité.</li>
                    <li><b>Réfèrences</b> : si cette case est cochée, Rekasator pourra choisir des développements sans réfèrences.</li>
                    <li><b>Extension couplage</b> : si cette case est cochée, que vous êtes connecté et que vous avez créé un couplage, Rekasator va conserver vos choix de développements et vos impasses et va vous proposer des développements à rajouter pour couvrir vos leçons.</li>
                    <li><b>Leçons</b> : choisissez les leçons que vous voulez couvrir si vous n'utilisez pas l'extension de couplage.</li>
                </ul>

            </p>
        </div>
        </div>
    </div>

    {!! Form::open(['url' => route('rekasator.result', $annee), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'rekasator-form']) !!}
        <div class="strip" style="overflow:auto">
        <div class="container">
            {!! Form::groupOpen('Qualité minimale', ['class' => 'form-group form-group-lg']) !!}
                <input type="range" min="0" max="5" name="qualite" class="form-control"/>
            {!! Form::groupClose() !!}

            {{-- {!! Form::groupOpen('Recasages', ['class' => 'form-group form-group-lg']) !!}
                <input type="number" value="4" min="0" max="5" name="recasages" class="form-control"/>
            {!! Form::groupClose() !!} --}}

            {!! Form::groupOpen('Utiliser les développements sans réfèrence', ['class' => 'form-group form-group-lg']) !!}
                <input type="checkbox" name="useUnreferenced" class="form-control" checked/>
            {!! Form::groupClose() !!}

            
            {!! Form::groupOpen('Extension de votre couplage', ['class' => 'form-group form-group-lg']) !!}
                <input type="checkbox" name="extendUserCouplage" class="form-control"/>
            {!! Form::groupClose() !!}


            {{-- {!! Form::groupOpen("Nombre d'essais", ['class' => 'form-group form-group-lg']) !!}
                <input type="number" min="500" max="2000" value="1000" step="100" name="nbr_essais" class="form-control"/>
            {!! Form::groupClose() !!} --}}

            {!! Form::groupOpen("Leçons", ['class' => 'form-group form-group-lg']) !!}
                <div class="input-group input-group-lg">
                    <input class="form-control" type="text" id="rekasator-choix" value="Toutes les leçons" readonly>

                    <div class="input-group-btn">
                        <button class="btn btn-default btn-lg btn-collapse collapsed" type="button" data-toggle="collapse" data-target="#rekasator-lecons" aria-expanded="false" aria-controls="rekasator-lecons">
                            Choisir... &nbsp;<span class="caret"></span>
                        </button>
                    </div>
                </div>
            {!! Form::groupClose() !!}
        </div>
        </div>

        <div class="strip llblue collapse" id="rekasator-lecons">
        <div class="container">
            <br />

            <div class="btn-group col-md-2">
                <button type="button" class="btn btn-default dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Sélection rapide <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" id="rekasator-toutes">Toutes les leçons</a></li>
                    <li><a href="#" id="rekasator-aucune">Aucune leçon</a></li>

                    <li role="separator" class="divider"></li>

                    <li><a href="#" id="rekasator-maths">Toutes les leçons de mathématiques</a></li>

                    <li role="separator" class="divider"></li>

                    <li><a href="#" id="rekasator-info">Toutes les leçons d'informatique</a></li>
                    <li><a href="#" id="rekasator-maths-info">Toutes les leçons de mathématiques option D</a></li>
                </ul>
            </div>
            <div class="col-md-10">
            {!! Form::groupOpen("", ['class' => 'columns form-group form-group-lg', 'col' => 0]) !!}
            @foreach($lecons as $lecon)
                <label class="checkbox-inline">
                    <input type="checkbox" name="lecons[{{ $lecon->id }}]" value="1"
                        class="{{ ($lecon->type->id == 3)? 'info' : (isset($lecon->options['D'])? 'maths maths-info' : 'maths')}}"/>
                    {{ $lecon->numero }}
                </label>
            @endforeach
            {!! Form::groupClose() !!}
            </div>
        </div>
        </div>

        <div class="strip">
        <div class="container">
            <br />
            {!! Form::submitbtn('Générer', ['class' => 'btn btn-primary btn-lg', 'disabled' => false]) !!}
        </div>
        </div>
    {!! Form::close() !!}

    <div class="strip llblue" id="rekasator-resultats">
    </div>

    <div class="strip llblue" id="rekasator-lol" style="height: 400px">
    </div>
</div>
@stop