            <ul>
                <li class="clickable {{ $subpage === 'rekasator' ? 'active' : null }}">
                    {{ Html::link(route('rekasator.index', config('app.annee')), 'Rekasator') }}
                </li>
                <li class="clickable {{ $subpage === 'couplages' ? 'active' : null }}">
                    {{ Html::link(route('couplages.index.user'), 'Couplages') }}
                </li>
            </ul>
