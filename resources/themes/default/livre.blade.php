@extends('templates.full-page')
@section('title', "Agreg-maths.fr : le couteau suisse de l'agrégatif de mathématiques")
@section('meta', "Ce site regroupe des plans de leçons, des développements, des rapports de jury, un éditeur de couplage et encore d'autres outils facilitant la préparation à l'agregation de mathématiques.")

@section('subnavbar')

@stop

@section('content')
<div id="index-page">

<div class="jumbotron">
  <div class="container">
    <h1>L'Oral a l'agrégation de mathématiques</h1>
<h2>Une sélection de développements</h2>
  </div>
</div>
<div class="strip">
<div class="container">
Quelques années après avoir passé l'agrégation, nous avons entrepris de mettre au propre une liste de développements en ayant comme but de centraliser l'informations des sources diverses (et en créeant des références pour les développements jusqu'alors sans référence).
Beaucoup de temps et de pages plus tard, nous sommes fier de vous présenter notre ouvrage, édité aux éditions Ellipses : "L'Oral à l'agrégation de mathématiques, une sélection de développements".
Le livre fait environ 400 pages et propose 65 développements, pour lesquels nous avons essayé de proposer plus qu'un simple schéma de preuve.
Nous avons également fait la part belle à quelques développements 100% originaux (ils ne sont même pas sur agreg-maths !), en essayant de varier les difficultés proposées.
<br /><br />
<div class="row">
  <div class="col-md-5">
                <img src="https://www.editions-ellipses.fr/34401-large_default/l-oral-a-l-agregation-de-mathematiques-une-selection-de-developpements.jpg" class="img-fluid" style="max-height:70vh"/>
  </div>
<div class="col-md-7">
<h2> </h2>
<p style="font-size:16px; line-height:24px">
Quels développements choisir pour préparer au mieux ses oraux d'agrégation de mathématiques ? C'est pour aider les candidats à résoudre ce casse-tête et leur éviter d'avoir à jongler entre de nombreuses références que ce livre a été rédigé. Les auteurs ont méticuleusement sélectionné 65 développements abordables et parfois originaux ou inédits pour couvrir la vaste étendue des leçons de mathématiques, en suivant les recommandations du jury ainsi que leur fraîche expérience à l'agrégation de 2015.
Au-delà d'une démonstration rigoureuse et éprouvée, chaque développement est précédé d'un récapitulatif détaillé des notions nécessaires à sa compréhension ainsi que de quelques éléments de contexte historique et/ou scientifique. De plus, pour répondre à certaines questions du jury ou pour parachever la compréhension du résultat démontré, chaque développement est prolongé par une section "approfondissements", dans laquelle les auteurs dressent des liens entre le résultat et d'autres notions du programme, ou illustrent certaines notions avec des algorithmes écrits en Sage.
Pour finir, des plans minimalistes de leçons sont proposés pour s'assurer que les développements choisis ne traitent pas d'un même aspect dans une leçon. 
<br />
</p>
</div>
</div>
<div class="text-center" style="font-style:italic">La table des matières peut être parcourue à l'adresse suivante : <a href="https://www.editions-ellipses.fr/index.php?controller=attachment&id_attachment=44038">Table des matières</a></div>
<br />
Nous tenons à remercier vivement toutes les personnes qui ont participé à la relecture de ce livre et nous espérons que cet ouvrage pourra vous être utile pour préparer l'agrégation.
Sachez que les profits réalisés (~4€ par livre) seront utilisés pour financer l'hébergement de ce site (cf page <a href="soutien">Nous soutenir</a>).
Ainsi, si le site a pu vous être utile et que vous souhaitez l'aider à vivre et à se développer tout en complétant votre bibliothèque, n'hésitez pas à acheter le livre chez votre revendeur favori !

</div>
</div>
<div class="strip" style="background-color:#e1ecf2">
<div class="container">
<h2>Qui sommes-nous ?</h2>
<p>
<a href="https://www.lirmm.fr/~isenmann/">Lucas Isenmann</a> : après avoir obtenu l'agrégation en 2015, il effectue une thèse en informatique à Montpellier dans l'équipe ALGCO (LIRMM). 
</p>
<p>
<a href="http://perso.ens-lyon.fr/timothee.pecatte/">Timothée Pecatte</a> : après avoir obtenu l'agrégation (option D) en 2015, il effectue une thèse en informatique à Lyon dans l'équipe MC2 (LIP) avant d'obtenir un poste de PRAG à l'INSA Lyon en septembre 2018.
</p>
</div>
</div>

</div>
@stop
