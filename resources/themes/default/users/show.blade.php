@extends('templates.full-page')

@section('title', 'Profil de '.$ruser->name)
@section('subnavbar')
    @include('users.subnavbar', ['subpage' => 'show'])
@stop

@section('content')
<section id="user">
    <div class="jumbotron">
    <div class="container">
        <h2>
            Profil de {{ $ruser->name }}
        </h2>
    </div>
    </div>


    <div class="strip" id="informations">
    <div class="container">
        <h3>Informations : </h3>

        <div class="row">
            <strong>Inscrit le : </strong>
            <div>
                {{ $ruser->created_at->format("d/m/Y") }}
            </div>
        </div>

        <div class="row">
            <strong>Dernière connexion :</strong>
            <div>
                {{ $ruser->last_login->format("d/m/Y") }}
            </div>
        </div>

        @if($ruser->montrer_email)
        <div class="row">
            <strong>Email :</strong>
            <div>
                {{ $ruser->email }}
            </div>
        </div>
        @endif

        @if(!empty($ruser->annee))
        <div class="row">
            <strong>Inscrit à l'agrégation :</strong>
            <div>
                {{ $ruser->annee }}, option {{ $ruser->foption }}
            </div>
        </div>
        @endif

        @if(!empty($ruser->resultat))
        <div class="row">
            <strong>Résultat :</strong>
            <div>
                {{ $ruser->fresultat }}
            </div>
        </div>
        @endif

    </div>
    </div>


    <div class="strip" id="developpements">
    <div class="container">
        <h3>
           Ses versions de développements :
        </h3>
        @foreach($ruser->versionsDeveloppements as $version)
            @include('partials.version_ext')
        @endforeach
    </div>
    </div>


    <div class="strip" id="lecons">
    <div class="container">
        <h3>
           Ses plans de leçons :
        </h3>
        @foreach($ruser->versionsLecons as $version)
            @include('partials.version_ext')
        @endforeach
    </div>
    </div>
</section>
@stop
