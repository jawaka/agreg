@extends('templates.full-page')

@section('title', 'Profil de '.$user->name)
@section('subnavbar')
    @include('users.subnavbar', ['subpage' => 'show'])
@stop

@section('content')
<section id="user">
    <div class="jumbotron">
    <div class="container">
        <h2>
            Votre profil
        </h2>
    </div>
    </div>


    <div class="strip" id="informations">
    <div class="container">
        <h3>Vos informations : </h3>
        <div class="row">
            <strong>Pseudonyme :</strong>
            <div>
                {{ $user->name }}            
            </div>
        </div>

        <div class="row">        
            <strong>Inscrit le : </strong>
            <div>
                {{ $user->created_at->format("d/m/Y") }}
            </div>
        </div>
        <div class="row">        
            <strong>Dernière connexion :</strong>
            <div>
                {{ $user->last_login->format("d/m/Y") }}
            </div>
        </div>

        {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('id', $user->id) !!}
            {!! Form::control('email', 'email', 'Email') !!}
            {!! Form::control('checkbox', 'montrer_email', 'Email public', ['checked' => $user->montrer_email]) !!}
            {!! Form::control('text', "annee", "Inscrit à l'agrégation") !!}
            {!! Form::control('select', 'option', 'Option', ['list' => [0 => 'Non renseigné', 1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'Docteur']]) !!}
            {!! Form::control('select', 'resultat', 'Résultat', ['list' => [0 => 'Non renseigné', 1 => 'Non-admissible', 2 => 'Admissible', 3 => 'Admis']]) !!}
            {!! Form::control('text', 'classement', 'Classement') !!}
            {!! Form::control('textarea', 'biographie', 'Quelques mots sur vous', ['attr' => ['size' => '50x5']]) !!}

            {!! Form::submitbtn('Mettre à jour') !!}
        {!! Form::close() !!}
    </div>
    </div>


    <div class="strip" id="developpements">
    <div class="container">
        <h3>
           Vos versions de développements : 
        </h3>
        @foreach($user->versionsDeveloppements as $version)
            @include('partials.version_ext')
        @endforeach
    </div>
    </div>


    <div class="strip" id="lecons">
    <div class="container">
        <h3>
           Vos plans de leçons : 
        </h3>
        @foreach($user->versionsLecons as $version)
            @include('partials.version_ext')
        @endforeach
    </div>
    </div>
</section>
@stop