@extends('templates.full-page')
@css('assets/css/fileinput.css')

@section('title', 'Version de '.$version->user->name)
@section('subnavbar')
    @if($type == 'developpement')
        @include('developpements.subnavbar', ['subpage' => 'version.edit'])
    @else
        @include('lecons.subnavbar', ['subpage' => 'version.edit'])
    @endif
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>
            @if($type == 'developpement')
            Modification d'une version
            @else
            Modification d'un plan
            @endif
        </h1>
    </div>
    </div>
</div>
<div class="container">
{!! Form::model($version, ['route' => ['versions.update', $version->id], 'method' => 'put', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
    {!! Form::control('textarea', 'remarque', 'Remarque', ['size' => 'lg', 'attr' => ['rows' => 4]]) !!}
    {!! Form::control('lSelect',  'references', 'Références',
        ['list' => $references, 'msg' => 'Choisir une référence...', 'val' => $version->references->toList(), 'size' => 'lg']) !!}


   <div class="form-group form-group-lg">
        <label class="col-md-2 control-label">Fichiers :</label> 
        <ul class="col-md-10">
        @foreach($version->fichiers as $f)
        <li class="list-group-item">
            <div class="input-group">
                {!! Form::text('fichiers['. $f->id .'][url]', $f->url, ['class' => 'form-control']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btn-lg file-delete" data-id="{{ $f->id }}">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </span>
            </div>
        </li>
        @endforeach
        
        <li class="list-group-item" id="versions-edit-file-input-li">
            <input type="file" name="version_fichier" id="versions-edit-file-input" data-upload-url="uploadFile" />
        </li>
        </ul>
    </div>


    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
@stop

@section('modal')
    @include('partials.confirm-delete', [
        'msgconfirm'   => 'Voulez-vous vraiment supprimer ce fichier ?', 
        'deleteRoute'  => ['versions.deleteFile', $version->id],
        'deleteFormId' => 'form-delete-file'
    ])
@stop
