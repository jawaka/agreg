@extends('templates.full-page')
@css('assets/css/fileinput.css')

@section('title', "Ajout d'une version")
@section('subnavbar')
    @if($type == 'developpement')
        @include('developpements.subnavbar', ['subpage' => 'version.create'])
    @else
        @include('lecons.subnavbar', ['subpage' => 'version.create'])
    @endif
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>
            @if($type == 'developpement')
            Ajout d'une version
            @else
            Ajout d'un plan
            @endif
        </h1>
    </div>
    </div>
</div>
<div class="container">
    @if($type == 'developpement')
        Si vous venez d'ajouter un développement, vous n'êtes pas obligés de rajouter une version. Vous pouvez juste retourner sur la page du dévelopemment ou alors rajouter une version sans pdf avec juste une référence.
        <br>
        <br>
    @endif

{!! Form::open(['route' => ['versions.store'], 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'files'=>true]) !!}
    {!! Form::hidden('versionable_id', $versionable->id) !!}
    {!! Form::hidden('versionable_type', $type) !!}

    {!! Form::control('textarea', 'remarque',  'Remarque', ['size' => 'lg', 'attr' => ['rows' => 4]]) !!}
    {!! Form::control('lSelect',  'references', 'Références',
        ['list' => $references, 'msg' => 'Choisir une référence...', 'val' => [], 'size' => 'lg']) !!}


   <div class="form-group form-group-lg">
        <label class="col-md-2 control-label">Fichiers :</label> 
        <div class="col-md-10">
            <input type="file" multiple="true" id="version-create-file-input" name="version_fichiers[]" class="file-loading"  />
        </div>
    </div>

{{--
   <div class="form-group form-group-lg">
        <div class="col-md-10 col-md-offset-2">
            <button type="submit" class="btn btn-danger btn-lg">Annuler</button>
        </div>
    </div>
--}}

    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
@stop