<div class="modal fade" id="modal-livre" tabindex="-2" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    ×
                </button>

                <h4 class="modal-title">Notre livre est édité !</h4>
            </div>
            <div class="modal-body">
<div class="row">
    <div class="col-sm-7">
		<img src="https://www.editions-ellipses.fr/34401-large_default/l-oral-a-l-agregation-de-mathematiques-une-selection-de-developpements.jpg" class="img-fluid" style="max-height:50vh"/>
    </div>
    <div class="col-sm-5">
		<br />
                Après plus d'un an et demi d'écriture, notre livre voit enfin le jour !<br />
                Cet ouvrage a été relu par des agrégatifs comme vous pour en faire un outil le plus utile possible !
		<br /><br />
		Cet ouvrage propose une liste de développements analysés finement, replacés dans un contexte global listant le plus exhaustivement possible les imbrications des résultats avec le reste du monde mathématique. Le lecteur trouvera dans cet ouvrage toute les techniques fondamentales de preuve ainsi que des entraînements complets et pédagogiques afin d’être préparé au mieux pour le concours de l’agrégation de mathématiques.
    </div>
</div>
            </div>
            <div class="modal-footer">
                <a href="https://agreg-maths.fr/livre" class="btn btn-lg btn-primary">En savoir plus !</a>
            </div>
        </div>
    </div>
</div>
