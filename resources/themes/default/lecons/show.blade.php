@extends('templates.full-page')

@section('title', 'Leçon '.$lecon->numero. (config('app.annee') != $lecon->annee? (' ('.$lecon->annee.')') : '') .' : '.$lecon->nom)
@section('meta', 'Rapports du jury, propositions de plans, suggestions de développements pour la leçon '.$lecon->numero. (config('app.annee') != $lecon->annee? (' ('.$lecon->annee.')') : ''))
@section('subnavbar')
    @include('lecons.subnavbar', ['subpage' => 'show'])
@stop


@section('content')
<section id="lecon">
    <div class="jumbotron">
    <div class="container">
        <h1>
            Leçon {{ $lecon->numero }}
            @if(!$lecon->isD)
            <!--
                <span data-toggle="tooltip" data-placement="bottom" title="Leçon au programme des options A,B,C uniquement.">*</span>
            -->
            @endif
            :
            {{ $lecon->nom }}
        </h1>
        <h2>
            @foreach($lecon->parents as $plecon)
                <a href="{{ route('lecons.show', $plecon->id) }}">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    ({{ $plecon->annee }}) {{$plecon->numero}}
                </a>
                <br />
            @endforeach

            @foreach($lecon->enfants as $plecon)
                <a href="{{ route('lecons.show', $plecon->id) }}" class="pull-right">
                    ({{ $plecon->annee }}) {{$plecon->numero}}
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
                <br />
            @endforeach
        </h2>
    </div>
    </div>

@if(isset($couplage))
    <div class="strip" id="couplage">
    <div class="container lecon state-{{ $couplage->getLeconStatus($lecon->id) }}" id="lecon-{{ $lecon->id }}" data-id="{{ $lecon->id }}">
        <form autocomplete="off">
        <div class="lecon-header">
            {!! Html::switcher('switch-lecon-'. $lecon->id, $couplage->getLeconStatus($lecon->id)) !!}
            <h3>
                <a href="{{ route('couplages.index.user') }}#lecon-{{ $lecon->id }}-anchor">
                    Cette leçon dans votre couplage
                </a>
            </h3>
        </div>
        <div id="commentaire">
            <button type="button" class="btn btn-success">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
            <textarea rows="1" data-id="{{ $lecon->id }}" placeholder="Pas de commentaires personnels pour cette leçon">{!!
                ($user->hasLCommentaire($lecon->id))?
                    $user->getLCommentaire($lecon->id)->commentaire :
                    ''
                !!}</textarea>
        </div>

        <div id="references">
            <select data-placeholder="Rajouter une référence personnelle pour cette leçon" class="chosen-select lecons-references" multiple data-id="{{ $lecon->id }}" tabindex="4">
                @foreach($references as $id => $elem)
                    <option value="{{$id}}"{!! $user->hasLReference($lecon->id, $id)? ' selected="selected"' : ''!!}>{{ $elem }}</option>
                @endforeach
            </select>
        </div>

        <ul id="couplage-developpements" class="couverture">
            @foreach($couplage->leconRecasages($lecon->id) as $id => $info)
            <li class="list-group-item couverture-rating couverture-rating-{{ $info['qualite']}} {{ $info['checked']? 'checked' : ''}} {{ $couplage->isLocked($id)? 'locked' : '' }}" data-id="{{ $id }}" data-lecon-id="{{ $lecon->id }}">
                <div class="couverture-checkbox {{ $info['checked']? 'checked' : ''}}" id="couverture_{{ $lecon->id }}_{{ $id }}"></div>
                {!! Form::rating('couverture_'.$lecon->id.'['.$id.'][qualite]', $info['qualite'], 5, false, $couplage->isLocked($id)) !!}
                <button type="button" class="delete">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
                <button type="button" class="developpement-edit">
                    <span class="glyphicon glyphicon-pencil"></span>
                </button>
                <button type="button" class="developpement-lock">
                    <i class="fa"></i>
                </button>
                <div>
                    <a href="{{ route('developpements.show', $id) }}">
                        {{ $developpements[$id] }}
                    </a>
                </div>
            </li>
            @endforeach

            <li class="list-group-item clearfix rating-add-container">
                <div class="input-group">
                    <div type="button" class="btn input-group-addon rating-add couverture-add-btn"><span class="glyphicon glyphicon-plus"></span></div>
                    <select data-placeholder="Choisir un développement..." class="form-control chosen-select">
                        <option value=""></option>
                        @foreach($developpements as $id => $elem)
                            <option value="{{$id}}"{!! in_array($id, $usedDevs)? ' disabled="disabled"' : ''!!}>{{ $elem }}</option>
                        @endforeach
                  </select>
                </div>
            </li>
        </ul>
        </form>
        <div id="new-developpement">
            <a href="{{ route("developpements.create") }}" class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Créer un nouveau développement
            </a>
        </div>
    </div>
    </div>
@endif


    <div class="strip" id="rapport">
    <div class="container">
        <h2>Dernier{{ (count($lecon->lastRapports()) > 1)? 's' : ''}} rapport{{ (count($lecon->lastRapports()) > 1)? 's' : ''}} du Jury : </h2>
        <p>
            @if(count($lecon->allRapports()) == 0)
                Pas de rapport concernant cette leçon.
            @else
                @foreach($lecon->lastRapports() as $r)
                    <b><a href="{{ route('lecons.show', $r->id) }}">({{ $r->annee }} : {{ $r->numero }} - {{ $r->nom }})</a></b>
                    {{ $r->rapport }}<br />
                @endforeach

                @if(count($lecon->otherRapports()) > 0)
                <h3 data-toggle="collapse" data-target="#other" class="collapsed">
                    Autre{{ (count($lecon->otherRapports()) > 1)? 's' : ''}} rapport{{ (count($lecon->otherRapports()) > 1)? 's' : ''}}
                    <span class="plus">+</span>
                </h3>
                <div id="other" class="collapse">
                @foreach($lecon->otherRapports() as $r)
                    <b><a href="{{ route('lecons.show', $r->id) }}">({{ $r->annee }} : {{ $r->numero }} - {{ $r->nom }})</a></b>
                    {{ $r->rapport }}<br />
                @endforeach
                </div>
                @endif
            @endif
        </p>
    </div>
    </div>


@if(!isset($couplage))
    <div class="strip" id="votes">
@if($user)
    @if($lecon->votesOfUser($user->id)->get()->isEmpty())
    <div class="container collapse in" id="show-votes">
        <p>
            <a href="#edit-votes" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="edit-votes">
                Vous n'êtes pas d'accord avec les recasages ci-dessous ? Proposez les vôtres ici !
            </a>
        </p>
    </div>
    <div class="container collapse" id="edit-votes">
    @else
    <div class="container" id="edit-votes">
    @endif

        <h2>Vos recasages : </h2>
        {!! Form::model($lecon, ['route' => ['lecons.votes.update', $lecon->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
            {!! Form::hidden('id', $lecon->id) !!}

            <div class="form-group form-group-lg">
                <ul id="list-votes">
                {!! Form::lRating('votes',
                    ['list' => $developpements, 'field' => 'qualite', 'msg' => 'Choisir un développement...',
                     'val' => $lecon->votesOfUser($user->id)->get()->toRatingArray('qualite')]) !!}
                </ul>
            </div>

            {!! Form::submitbtn('Mettre à jour', ['col' => 0]) !!}
        {!! Form::close() !!}
    </div>
@else
    <div class="container" id="show-votes">
        <p>
            <a href="http://www.agreg.fr/login" class="login-modal-toggle">
                Vous n'êtes pas d'accord avec les recasages ci-dessous ? Connectez-vous pour proposer les vôtres !
            </a>
        </p>
    </div>
@endif
    </div>
@endif

    <div class="strip" id="developpements">
    <div class="container">
        <h2>Développements : </h2>
        <ul>
        @foreach($recasages as $rdeveloppement)
            @php
                $nb_fichiers = 0;
                $nb_refs = 0;
            @endphp
            @foreach($rdeveloppement->versions as $version)
                @php
                    $nb_refs += count($version->references);
                    $nb_fichiers += count($version->fichiers);
                @endphp
            @endforeach 
            <li>
                <a href="{{ route('developpements.show', $rdeveloppement->id) }}">
                    {!! Html::rating($rdeveloppement->pivot->qualite) !!}
                    {{ $rdeveloppement->nom }}
                    @if($nb_refs == 0)
                    [<del>ref</del>]
                    @endif
                    @if($nb_fichiers == 0)
                    [<del>pdf</del>]
                    @endif
                </a>
            </li>
        @endforeach
        </ul>
    </div>
    </div>


    <div class="strip" id="versions">
    <div class="container">
        <h2>
            Plans/remarques :
            @if($user)
            <a href="{{ route('versions.create.lecon', $lecon->id) }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i>
                Ajouter votre plan
            </a>
            @endif
        </h2>
            <?php $c = 0; ?>
            @foreach($lecon->allParents() as $l)
            @if(count($l->versions) > 0)
                <h3><strong>{{ $l->annee }} :</strong> Leçon {{ $l->numero }} - {{ $l->nom }}</h3>
                @foreach($l->versions as $version)
                    <?php $c++; ?>
                    @include('partials.version')
                @endforeach
                <br />
            @endif
            @endforeach

            @if($c == 0)
            Pas de plans pour cette leçon.
            @endif
    </div>
    </div>


    <div class="strip" id="retours">
    <div class="container">
        <h2>
            Retours d'oraux :
            @if($user)
            <a href="{{ route('retours.create') }}" class="btn btn-success pull-right">
                <i class="fa fa-plus"></i>
                Ajouter votre retour d'oral
            </a>
            @endif
        </h2>
            <?php $c = 0; ?>
            @foreach($lecon->allParents() as $l)
            @if(count($l->retours) > 0)
                <h3><strong>{{ $l->annee }} :</strong> Leçon {{ $l->numero }} - {{ $l->nom }}</h3>
                @foreach($l->retours as $retour)
                    <?php $c++; ?>
                    @include('retours.partial')
                @endforeach
                <br />
            @endif
            @endforeach

            @if($c == 0)
            Pas de retours pour cette leçon.
            @endif
    </div>
    </div>

    <div class="strip" id="references">
        <div class="container">
            <h2>
            Références utilisées dans les versions de cette leçon :
            </h2>
            @foreach ($lecon->getReferences() as $ref)
                <a href="{{ route('references.show', [$ref->id]) }}">{{ $ref->titre }}</a>, {{$ref->auteurs}} (utilisée dans {{$ref->versionsDeveloppements()->count() + $ref->versionsLecons()->count()}} versions au total) <br>
            @endforeach

        </div>
    </div>

</section>
@stop


@section('scripts')
<script type="text/javascript">
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
var couplageAjaxUrl = "{{ route('couplages.ajax') }}";
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ItemList",
  "itemListElement": [
    <?php $i = 1; ?>
    @foreach($recasages as $rdeveloppement)
    {
        "@type": "ListItem",
        "position":"{{ $i++}}",
        "item":
        {
            "@type":"Rating",
            "ratingValue":"{{ $rdeveloppement->pivot->qualite }}",
            "name": "{{ str_replace("\\", "", $rdeveloppement->nom) }}",
            "url": "{{ route('developpements.show', $rdeveloppement->id) }}"
        }
    }
    @if($i == 10)
        @break
    @else
    ,
    @endif
    @endforeach
  ],
  "itemListOrder": "http://schema.org/ItemListOrderDescending",
  "name": "Les meilleurs développement pour cette leçon."
}
</script>
@stop

@section('modal')
<div class="modal fade" id="modal-developpement"></div>

{{-- Confirm Delete --}}
@include('partials.confirm-delete', ['formDelete' => false, 'msgconfirm' => 'Attention, ce développement est utilisé dans des leçons de votre couplage. Voulez-vous quand même le supprimer de votre couplage ?'])
@stop
