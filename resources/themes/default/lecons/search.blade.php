@extends('templates.page')

@section('title', "Recherche de leçons")
@section('subnavbar')
    <ul class="breadcrumbs">
        <li class="dropdown active clickable">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                {{ $anneeLecon }}
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                @foreach($leconsAnnees as $annee)
                <li><a href="{{ route('lecons.index.annee', $annee) }}">{{ $annee }}</a></li>
                @endforeach
            </ul>
        </li>
        <li class="separator"></li>
        <li class="active">
            Recherche
        </li>
        <li class="separator"></li>
        <li>{{ $search }}</li>
    </ul>

    {{ Form::open([
        'url' => route('lecons.search', $anneeLecon), 
        'method' => 'post', 
        'class' => 'navbar-form navbar-right', 
        'id' => 'search_form', 
        'role' => 'search'])
    }}
        <div class="form-group">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control" placeholder="Rechercher par mot-clef" name="search" id="search_field" />
                <div class="input-group-btn">
                    <button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
@stop


@section('content')
    @foreach($lecons as $l)
        <section id='{{ $l->numero }}' class='lecon'>
            <h3>
                <a href='{{ route("lecons.show", $l->id) }}'>
                    <span class="numero">{{ $l->numero }}</span>
                    {{ $l->nom }}
                </a>
            </h3>
            <div class="details">
                <ul>
                @foreach($l->recasagesOrdered as $r)
                    <li><span class='display-rating'>{!! Html::rating($r->pivot->qualite) !!}</span> {{ $r->nom }}</li>
                @endforeach
                </ul>
            </div>
        </section>
    @endforeach
@stop