@extends('templates.full-page')

@section('title', "Leçons de mathématiques proposées à l'agrégation en ". $anneeLecon)
@section('meta', "Liste des leçons pour l'année ".$anneeLecon." avec des suggestions de développements")
@section('subnavbar')
    @include('lecons.subnavbar', ['subpage' => 'list'])
@stop


@section('content')
<div id="lecons-container">
    <div class="jumbotron">
    <div class="container">
        <h1>Liste des leçons</h1>
        <p>
            Avec des suggestions de développements, des propositions de plans, des références, ...
        </p>
    </div>
    </div>
    <table class="table table-striped table-bordered" id="lecons-table">
        <thead>
            <tr>
                <th>
                    <div>
                    Numéro
                    </div>
                </th>
                <th>
                    Type
                </th>
                <th>
                    <div>
                    Option Docteur
                    </div>
                </th>
                <th>
                    <div>
                    Nom
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach($lecons as $lecon)
        <tr>
            <td>
                <div>
                    {{ $lecon->numero }}
                    @if(!$lecon->isD)
                    <!--
                    <span data-toggle="tooltip" data-placement="bottom" title="Leçon au programme des options A,B,C uniquement.">*</span>
                    -->
                    @endif
                </div>
            </td>
            <td>
                <div>
                    {{ $lecon->type_id  }}
                </div>
            </td>
            <td>
                <div>
                    {{ $lecon->getIsDocteurAttribute()  }}
                </div>
            </td>
            <td>
                <div>
                <a href="{{ route('lecons.show', [$lecon->id]) }}">
                    {{ $lecon->nom  }}
                </a>
                </div>
            </td>
        </tr>
        @endforeach
     </tbody>
    </table>
</div>
@stop

@section('scripts')
<script type="text/javascript">
// References datatable
$(document).ready(function() {
    function setSizes(){
        var ratio = 1;

        var total = $('#lecons-table').width();
        var container = $('.jumbotron .container').innerWidth();
        var margin = (total - container) / 2;

        var s1 =  margin + container * ratio / 12;
        var s2 =  margin + container * (12 - ratio) / 12;

        var th = $('#lecons-table th');
        th.first().width(s1);
        th.last().width(s2);
    }
    setSizes();
    $(window).resize(setSizes);
});
</script>
@stop

@section('contente')
<!--
<div id="lecons-container">
    <nav>
        <div id="lecons-nav-toggle">
            <div>
                <button type="button" id="lecons-nav-toggle-btn">
                    <i class="fa fa-sitemap"></i>
                    <span class="left glyphicon glyphicon-triangle-left"></span>
                    <span class="right glyphicon glyphicon-triangle-right"></span>
                </button>
            </div>
        </div>
        <div id="lecons-nav-links">
            <ul>
            @foreach($lecons as $l)
                <li><a href='#{{ $l->numero }}'> {{ $l->numero }} </a></li>
            @endforeach
            </ul>
            <div id="lecons-nav-scroller-container">
                <div id="lecons-nav-scroller"></div>
            </div>
        </div>
    </nav>


    <div id="lecons-content">
    @foreach($lecons as $l)
        <section id='{{ $l->numero }}' class='lecon'>
            <h3>
                <a href='{{ route("lecons.show", $l->id) }}'>
                    <span class="numero">{{ $l->numero . ($l->isD? '' : '*') }}</span>
                    {{ $l->nom }}
                </a>
            </h3>
            <div class="details">
                <ul>
                @foreach($l->recasages as $r)
                    <li>
                        <span class='display-rating'>{!! Html::rating($r->pivot->qualite) !!}</span> 
                        <a href="{{ route('developpements.show', $r->id) }}">{{ $r->nom }}</a>
                    </li>
                @endforeach
                </ul>
            </div>
        </section>
    @endforeach
    </div>
-->
</div>
@stop
