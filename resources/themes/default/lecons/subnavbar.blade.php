@if($subpage === 'list')

            <ul>
                <li class="dropdown active clickable">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ $anneeLecon }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($leconsAnnees as $annee)
                        <li><a href="{{ route('lecons.index.annee', [$annee]) }}">{{ $annee }}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>

            {{ Form::open([
                'url' => route('lecons.search', $anneeLecon), 
                'method' => 'post', 
                'class' => 'navbar-right', 
                'id' => 'search_form', 
                'role' => 'search'])
            }}
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Rechercher par mot-clef" name="search" id="search_field" />
                        <div class="input-group-btn">
                            <button class="btn" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}



@elseif($subpage === 'show')

            <ul>
                <li class="active clickable">
                    <a href="{{ route('lecons.index.annee', [$lecon->annee]) }}">
                        {{ $lecon->annee }}
                    </a>
                </li>
                <li class="separator"></li>
                <li class="active">
                    <a href="{{ route('lecons.show', $lecon->id) }}">
                        {{ $lecon->numero.' : '.$lecon->nom }}
                    </a>
                </li>
            </ul>



@elseif($subpage === 'version.create')

            <ul>
                <li class="active clickable">
                    <a href="{{ route('lecons.index.annee', [$versionable->annee,]) }}">
                        {{ $versionable->annee }}
                    </a>
                </li>
                <li class="separator"></li>
                <li class="active clickable">
                    <a href="{{ route('lecons.show', $versionable->id) }}">
                        {{ $versionable->numero.' : '.$versionable->nom }}
                    </a>
                </li>
                <li class="separator"></li>
                <li>
                    Nouveau plan
                </li>
            </ul>

@elseif($subpage === 'version.edit')
            <?php $versionable = $version->versionable ?>
            <ul>
                <li class="active clickable">
                    <a href="{{ route('lecons.index.annee', [$versionable->annee]) }}">
                        {{ $versionable->annee }}
                    </a>
                </li>
                <li class="separator"></li>
                <li class="active clickable">
                    <a href="{{ route('lecons.show', $versionable->id) }}">
                        {{ $versionable->numero.' : '.$versionable->nom }}
                    </a>
                </li>
                <li class="separator"></li>
                <li>
                    Plan de {{ $version->user->name }}
                </li>
                <li class="separator"></li>
                <li>
                Modification
                </li>
            </ul>

@endif