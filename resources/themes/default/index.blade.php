@extends('templates.full-page')
@section('title', "Agreg-maths.fr : le couteau suisse de l'agrégatif de mathématiques")
@section('meta', "Ce site regroupe des plans de leçons, des développements, des rapports de jury, un éditeur de couplage et encore d'autres outils facilitant la préparation à l'agregation de mathématiques.")

@section('subnavbar')

@stop

@section('content')
<div id="index-page">
<div class="jumbotron">
  <div class="container">
    <div class="alert alert-warning" role="alert">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        Ce site est encore en développement, n'hésitez pas à nous faire part de vos remarques, de vos suggestions à l'adresse mail : <b>admin@agreg-maths.fr</b>
    </div>
    <h1>Bienvenue sur le site agreg-maths.fr !</h1>
  </div>
</div>
<div class="strip">
<div class="container">

  <div class="alert alert-info" role="alert">
    <h3>(11/10/2024) Session 2025</h3>
    Les leçons pour la session 2025 ont été ajoutées au site.
    Remarquons la disparition des leçons <a href="lecons/1439">154 (Exemples de décompositions de matrices. Applications. )</a> et <a href="lecons/1477">244 (Exemples d'études et d'applications de fonctions usuelles et spéciales.)</a> par rapport à l'année dernière.
    
  </div>

  <br>

  <div class="alert alert-info" role="alert">
    <h3>(29/03/2024) 2ème édition de "L'oral à l'agrégation de mathématiques"</h3>
    Elle est sortie. Les plans contenus dans la première édition ont été enlevés dans le but que le livre soit à nouveau autorisé aux oraux. De plus 8 développements ont été rajoutés.
  </div>

  <br>

  <div class="alert alert-info" role="alert">
    <h3>(27/10/2023) Rekasator V2</h3>
    <a href="/outils/rekasator/2024">Rekasator</a> a été revu pour principalement apporter une nouvelle fonctionnalité : vous pouvez maintenant lui demander d'étendre votre <a href="/couplages/user">couplage</a> que vous aurez préalablement créé en vous proposant des développements pour couvrir toutes les leçons.
  </div>

  <br>

  <div class="alert alert-info" role="alert">
    <h3>(18/09/2023) Session 2024</h3>
      Les leçons pour la session 2024 ont été ajoutées au site.
      Par rapport à la session précédente il y a deux nouvelles leçons :
      <ul>
        <li><a href="lecons/1429">127 : Exemples de nombres remarquables. Exemples d'anneaux de nombres remarquables. Applications. </a></li>
        <li><a href="lecons/1461">218 : Formules de Taylor. Exemples et applications.</a></li>
      </ul>

      La leçon 127 est absolument nouvelle tandis que la 218 est une résurgence : des développements et des plans sont déjà disponibles pour cette dernière.
      Si vous voyez des développements qui se recasent dans la leçon 127, vous pouvez nous en faire part pour que nous changions les recasages de ces développements.

      <br/>
      <br/>

      Remarquons la disparition des leçons <a href="lecons/1416">267 (Exemples d'utilisation de courbes en dimension 2 ou supérieure)</a> et <a href="lecons/1359">126 (Exemples d'équations en arithmétique)</a> ainsi que la renumérotation de nombreuses leçons : 151 &rarr; 148, 152 &rarr; 149, 153 &rarr; 150, 154 &rarr; 151, 155 &rarr; 152, 149 &rarr; 153, 148 &rarr; 154, 156 &rarr; 155, 157 &rarr; 156, 158 &rarr; 157, 160 &rarr; 158, 265 &rarr; 244.
      
  </div>

  <div class="alert alert-info" role="alert">
    <h3>(07/06/2023) Recherche relecteurs</h3>
      La relecture est terminée.
      <br>
      <del>Le livre "L'oral à l'agrégation de mathématiques" étant interdit aux oraux à cause de la partie contenant des structures de plans de leçons, nous allons le rééditer en enlevant cette partie, en rajoutant des développements et en corrigeant les coquilles.
      Nous sommes donc à la recherche de relecteurs pour ces nouveaux développements.
      Si cela vous intéresse, envoyez nous un mail à <b>admin@agreg-maths.fr</b>. 
      Si vous voulez nous faire part d'erreurs dans ce livre, faites de même.
      Merci d'avance ! 
      </del>
  </div>

  <div class="alert alert-info" role="alert">
    <h3>(14/04/2023) Rekasator remarche</h3>
      L'outil <a href="outils/rekasator/2023">Rekasator</a> est de nouveau sur les rails. Cet outil génère aléatoirement un ensemble de développements pour couvrir au maximum possible les leçons demandées en cherchant à minimiser le nombre de développements et en maximisant la "qualité" (c'est-à-dire à quel point les développements passent bien dans les leçons).
  </div>

  <div class="alert alert-info" role="alert">
      <h3>(19/01/2023) Affichage automatique des [<del>ref</del>] [<del>pdf</del>] sur les développements</h3>
        Désormais un [<del>ref</del>] s'affichera automatiquement derrière le titre d'un développement lorsque aucune référence ne sera fournie dans les versions de ce développement. De même pour les pdfs associés.
    </div>

    <div class="alert alert-info" role="alert">
      <h3>(17/09/2022) Session 2023</h3>
        La liste des leçons pour la session 2023 a été ajoutée au site.
        Pour cette session, il y a <del>trois</del> deux nouvelles leçons par rapport à la précédente :
        <ul>
          <li><a href="lecons/1363">148 : Exemples de décompositions de matrices. Applications. </a></li>
          <li><a href="lecons/1386">206 : Exemples d’utilisation de la notion de dimension finie en analyse</a></li>
        </ul>

        Pour ces leçons, il n'y a donc dedans aucun développement pour l'instant.
        Des développements existants déjà sur le site pour des leçons proches devraient passer dans ces nouvelles leçons.
        Si vous en voyez, vous pouvez nous en faire part pour que nous changions les recasages de ces développements.

        <br/>
        <br/>

        La leçon <a href="lecons/1396">224 (Exemples de développements asymptotiques de suites et de fonctions)</a> n'existait pas en 2022 mais elle existait en 2019. 
        (Le problème technique lié à cette leçon est réglée au 26/09/2022).
    </div>




    <div class="alert alert-info" role="alert">
      <h3>(28/02/2022) Option Docteur</h3>

      On peut désormais mettre son couplage en option Docteur pour n'afficher que les leçons de l'option Docteur (liste officielle <a href="https://agreg.org/data/uploads/lecons/liste_lecons_doc.pdf">ici</a>).

    </div>

    <div class="alert alert-info" role="alert">
      <h3>(08/01/2022) Nouvelles leçons</h3>
      Pour la session 2022, une nouvelle leçon est apparue :
      <br>
      <br>
      Leçon 149 : Valeurs propres, vecteurs propres. Calculs exacts ou approchés d'éléments propres. Applications. 
      <br><br>
      Elle ne contient pour l'instant qu'un seul développement.
      Si vous repérez des développements qui passent dans cette leçon, merci de nous en faire part par mail pour qu'on puisse mettre à jour les développements qui passent dans cette leçon.

      <br><br>
      Pareil en informatique, il y en a beaucoup de nouvelles.
      Peut être que d'anciens développements passent dedans.
      
    </div>

    <div class="alert alert-info" role="alert">
    <h3>(08/01/2022) Gestionnaire de couplages</h3>
      Si vous avez déjà créé un couplage, il y a un bug pour changer l'année du couplage (les développements vont disparaitre du couplage car ils resteront sur l'ancienne année).
      Soit vous attendez qu'on règle ça. (Je sais pas combien de temps ça va prendre désolé).
      Soit vous supprimez tous les développements de votre couplage, vous changez l'année et puis vous remettez les développements (qui seront automatiquement mis pour l'année 2022).
      Comme l'export en pdf ne marche pas pour l'instant, je vous invite à faire "imprimer la page" à partir de votre navigateur et à l'imprimer dans un fichier.
    </div>

    <div class="alert alert-info" role="alert">
    <h3>(janvier 2022) Maintenance</h3>
Le site va être maintenu prochainement (la première étape des bonnes résolutions 2022 c'est fait).
Les prochaines étapes sont de régler les différents éléments suivants :
  <ul>
  <li> il y a un problème pour l'export pdf du couplage</li>
  <li> bug pour changer l'année du couplage</li>
  <li> rekasator bug</li>
  <li> <del>option Docteur</del> (créé le 28/02/22)</li>
  <li> <del>liste des leçons 2022</del> (réglé le 08/01/22)</li>
  <li> <del>liste des leçons 2021</del> (réglé le 07/01/22)</li>
  <li> <del>le lien pour les dons ne marche plus</del> (réglé le 06/01/22)</li>
  <li> et d'autres trucs que j'ai dû zappé (si vous voyez d'autres bugs, c'est le moment de nous avertir par mail)</li>
</ul>
</div>


    Ce site web 1.0 a été conçu par et pour les agreg de maths.
    Vous y trouverez un recueil de <a href="{{ route('developpements.index') }}">développements</a>,
        la liste de toutes les <a href="{{ route('lecons.index.annee', [config('app.annee'), 'algebre']) }}">leçons</a> avec les rapports du jury ainsi que des plans de leçons.
    De plus nous proposons quelques outils (<a href="{{route('developpements.kiviat', config('app.annee'))}}">diagramme de kiviat</a>, <a href="{{ route('rekasator.index', config('app.annee')) }}">Rekasator</a>, <a href="{{ route('couplages.index.user') }}">Couplages</a> (connexion nécessaire)) qui pourront se réveler fort utiles pour les agreg en quête d'optimalité !

    Le principe de base de ce site est la notion de "recasabilité".
    On en rappelle ici la définition tirée du dictionnaire Leroux 2016 :
    <blockquote>
        <p>
            La recasabilité d'un développement pour une leçon est une évaluation entre 0 et 5 de la cohérence dudit développement avec ladite leçon.
        </p>
        <footer>Leroux 2016</footer>
    </blockquote>

    Vous avez des remarques ? N'hésitez pas à nous en faire part en nous écrivant un mail à  admin@agreg-maths.fr.
    Vous voulez rajouter vos développements, vos plans de leçons ou proposer des modifications ?
    Il suffit de vous inscrire !
</div>
</div>
</div>
@stop
