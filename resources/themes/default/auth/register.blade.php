@extends('templates.page')

@section('title', 'Inscription')
@section('subnavbar')
    @include('auth.subnavbar', ['subpage' => 'register'])
@stop


@section('content')
    <br />

    {{ Form::open(['url' => 'register', 'method' => 'post', 'class' => 'form-horizontal'])}}

        {!! Form::control('text', 'name', 'Nom', 'Nom ou pseudo', $errors) !!}
        {!! Form::control('email', 'email', 'Email', 'Adresse email', $errors) !!}
        {!! Form::control('password', 'password', 'Mot de passe', 'Mot de passe', $errors) !!}
        {!! Form::control('password', 'password_confirmation', 'Confirmer', 'Confirmer le mot de passe', $errors) !!}

        {!! Form::submitbtn('Inscription') !!}

    {{ Form::close() }}
@endsection
