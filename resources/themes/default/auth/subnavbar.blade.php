@if($subpage != 'register')
            <ul>
                <li class="clickable {{ $subpage === 'login' ? 'active' : null }}">
                    {{ Html::link(route('auth.login'), 'Connexion') }}
                </li>
                <li class="clickable {{ $subpage === 'password' ? 'active' : null }}">
                    {{ Html::link(route('auth.password.form'), "Mot de passe oublié") }}
                </li>

            </ul>
@endif
