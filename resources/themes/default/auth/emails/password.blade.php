<body>
Une demande de réinitialisation de mot de passe a été effectuée récemment. <br />
Clickez sur le lien suivant pour continuer la procédure : 
{!! link_to_route('auth.password.reset.form', 'réinitialiser le mot de passe.', ['id' => $id, 'code' => $code]) !!}
</body>
