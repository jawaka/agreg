@extends('templates.page')

@section('title', 'Connexion')
@section('subnavbar')
    @include('auth.subnavbar', ['subpage' => 'login'])
@stop


@section('content')
    <br />
    {{ Form::open(['url' => 'login', 'method' => 'post', 'class' => 'form-horizontal'])}}

        {!! Form::control('text', 'login', 'Login', 'Adresse email ou pseudo', $errors) !!}
        {!! Form::control('password', 'password', 'Mot de passe', 'Mot de passe', $errors) !!}

        {!! Form::groupOpen() !!}
            {!! Form::bcheckbox('remember', 'Se souvenir de moi', 1, false) !!}
        {!! Form::groupClose() !!}

        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                {!! Form::submit('Connexion', ['class' => 'btn btn-primary']) !!}
                <a class="btn btn-link" href="{{ url('/password/reset') }}">Mot de passe oublié ?</a>
            </div>
        </div>
    {{ Form::close() }}
    </div>
</div>
@endsection
