@extends('templates.page')

@section('title', 'Réinitialiser le mot de passe')
@section('subnavbar')
    @include('auth.subnavbar', ['subpage' => 'password'])
@stop


@section('content')
    <br />
    {{ Form::open(['url' => ['/password/reset', $id, $code], 'method' => 'post', 'class' => 'form-horizontal'])}}
        {!! Form::control('password', 'password', 'Mot de passe', 'Nouveau mot de passe', $errors) !!}
        {!! Form::control('password', 'password_confirmation', 'Confirmation', 'Confirmer le nouveau mot de passe', $errors) !!}

        {!! Form::submitbtn('Enregistrer') !!}
    {{ Form::close() }}
@endsection
