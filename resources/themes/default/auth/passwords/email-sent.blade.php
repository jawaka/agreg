@extends('templates.page')

@section('title', 'Réinitialiser le mot de passe')
@section('subnavbar')
    @include('auth.subnavbar', ['subpage' => 'password'])
@stop


@section('content')
    <h3>Le lien pour réinitialiser le mot de passe a été envoyé à l'adresse correspondante.</h3>
@endsection