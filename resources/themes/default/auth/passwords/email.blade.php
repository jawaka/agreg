@extends('templates.page')

@section('title', 'Réinitialiser le mot de passe')
@section('subnavbar')
    @include('auth.subnavbar', ['subpage' => 'password'])
@stop


@section('content')
    <br />
    {{ Form::open(['url' => '/password/reset', 'method' => 'post', 'class' => 'form-horizontal'])}}
        {!! Form::control('text', 'login', 'Email/Pseudo', 'Adresse email ou pseudo', $errors) !!}
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-envelope"></i>
                    &nbsp;
                    M'envoyer un nouveau mot de passe
                </button>
            </div>
        </div>
    {{ Form::close() }}
@endsection