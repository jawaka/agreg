@extends('templates.full-page')

@section('title', 'Liste des développements')
@section('subnavbar')
    @include('developpements.subnavbar', ['subpage' => 'list'])
@stop

@section('content')
<div id="developpements-container">
    <div class="jumbotron">
    <div class="container">
        <h1>Liste des développements</h1>
        <p>
            Avec leurs recasages, leurs différentes versions, leurs références, ...
            @if(Sentinel::check())
            <a href="{{ route("developpements.create") }}" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-plus"></span>
                Ajouter le votre
            </a>
            @endif
        </p>
    </div>
    </div>
<!--
    <table class="table table-striped table-bordered" id="couplages-table">
        <thead>
            <tr>
                <th>
                    <div>
                    Nom
                    </div>
                </th>
                <th>
                    <div>
                    Année/option
                    </div>
                </th>
                <th>
                    <div>
                    Nombre de développements
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($couplages as $couplage)
        <tr class="clickable" data-href="{{ route('couplages.show', $couplage->id) }}">
            <td class="primary">
                <div>
                <a href="{{ route('couplages.show', $couplage->id) }}">
                    Couplage {{ $couplage->nom }}
                </a>
                </div>
            </td>
            <td>
                <div>
                {{ $couplage->annee }}, option {{ $couplage->foption }}
                </div>
            </td>
            <td>
                <div>
                {{ count($couplage->developpements) }}
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    -->
</div>
@stop


@section('scripts')
<script type="text/javascript">
// References datatable
$(document).ready(function() {
    function setSizes(){
        var ratio1 = 5;
        var ratio2 = 3;

        var total = $('#couplages-container').width();
        var container = $('.jumbotron .container').innerWidth();
        var margin = (total - container) / 2;

        var s1 =  margin + container * ratio1 / 12;
        var s2 =           container * ratio2 / 12;
        var s3 =  margin + container * (12 - ratio1 - ratio2) / 12;

        var th = $('#couplages-table th');
        th.first().width(s1);
        th.eq(1).width(s2);
        th.last().width(s3);
    }
    setSizes();
    $(window).resize(setSizes);
});
</script>
@stop