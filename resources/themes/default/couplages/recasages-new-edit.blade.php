@include('couplages.recasages', [
    'title'   => "Modification des recasages d'un développement",
    'message' => "Ce développement est déjà dans votre couplage. Pour l'ajouter dans cette leçon, il suffit d'ajuster ses recasages ci-dessous.",
    'button'  => "Modifier"
])