@css('assets/css/default.css')
@css('assets/css/cmun-serif.css')
@css('assets/css/debugbar.css')

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Couplage de {{ $couplage->user->name }}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Asset::styles() !!}
    <style>
    @page {
        margin: 1cm 3cm 1cm 2cm;
    }

    @page:first {
        margin: 0cm;
    }
    </style>
</head>
<body id="pdf-couplage" style="padding:0;margin:0;">
<form autocomplete="off">
    <div class="jumbotron">
    <div class="container">
        <h1>
            Couplage pour l'agrégation de Mathématiques
        </h1>
        <h3>
            {{ $couplage->user->name }} - Agreg-maths.fr
        </h3>
    </div>
    </div>

@if($opts['developpements'])
<div class="strip" id="developpements">
<div class="container">
    <h2>
        Développements (<span id="developpements-counter">{{ count($couplage->developpements) }}</span>) :
    </h2>

    <div id="list-developpements">
        @foreach($couplage->developpements as $d)
        <section class="developpement">
            <div class="developpement-header">
                <h3>
                    <a href="{{ route('developpements.show', $d->id) }}">
                        {{ $d->nom }}
                    </a>
                </h3>
            </div>
            @if($couplage->user->hasDCommentaire($d->id) && !empty($couplage->user->getDCommentaire($d->id)->commentaire) && $opts['developpementsCommentaires'])
            <div class="commentaire">
                {!! $couplage->user->getDCommentaire($d->id)->commentaire !!}
            </div>
            @endif

            @if($opts['developpementsReferences'])
            <ul class="references">
                @foreach($references as $id => $elem)
                    @if($couplage->user->hasDReference($d->id, $id))
                    <li>
                        <a href="{{ route('references.show', $id) }}">{{ $elem }}</a>
                    </li>
                    @endif
                @endforeach
            </ul>
            @endif

            @if($opts['developpementsRecasages'])
            <ul class="recasages">
                @foreach($invRecasages[$d->id] as $lid => $rating)
                    @if($couplage->couvertureHas($lid, $d->id) || !$opts['checkedOnly'])
                    <li class="list-group-item couverture-rating couverture-rating-{{ $rating['qualite']}}"
                        data-id="{{ $d->id }}" data-lecon-id="{{ $lid }}">
                        {!! Form::rating('couverture_'.$lid.'['.$d->id.'][qualite]', $rating['qualite'], 5, false, true) !!}
                        <div>
                            <a href="{{ route('lecons.show', $lid) }}">
                                {{ $lecons[$lid]->numero }} :
                            </a>
                            {{ $lecons[$lid]->nom }}
                        </div>
                    </li>
                    @endif
                @endforeach
            </ul>
            @endif
        </section>
        @endforeach
    </div>
</div>
</div>
@endif

@if($opts['lecons'])
<div class="strip" id="lecons">
<div class="container">
    <span id="lecons-anchor" class="anchor"></span>
    <h2>
        Couverture des leçons :
    </h2>
    @foreach($lecons as $lecon)
      @if($couplage->getLeconStatus($lecon->id) != 0 || $opts['impasse'])
        <section class="lecon state-{{ $couplage->getLeconStatus($lecon->id) }}" id="lecon-{{ $lecon->id }}" data-id="{{ $lecon->id }}">
            <div class="lecon-header">
                <h3>
                    <a href="{{ route('lecons.show', $lecon->id) }}">
                        {{ $lecon->numero }}
                    </a>
                    {{ $lecon->nom }}
                </h3>
            </div>
            @if($couplage->user->hasLCommentaire($lecon->id) && !empty($couplage->user->getLCommentaire($lecon->id)->commentaire) && $opts['leconsCommentaires'])
            <div class="commentaire">
                {!! $couplage->user->getLCommentaire($lecon->id)->commentaire !!}
            </div>
            @endif

            @if($opts['leconsReferences'])
            <ul class="references">
                  @foreach($references as $id => $elem)
                      @if($couplage->user->hasLReference($lecon->id, $id))
                      <li>
                          <a href="{{ route('references.show', $id) }}">{{ $elem }}</a>
                      </li>
                      @endif
                  @endforeach
            </ul>
            @endif

            <ul>
                @foreach($recasages[$lecon->id] as $id => $rating)
                @if($couplage->couvertureHas($lecon->id, $id) || !$opts['checkedOnly'])
                <li class="list-group-item couverture-rating couverture-rating-{{ $rating['qualite']}}"
                    data-id="{{ $id }}" data-lecon-id="{{ $lecon->id }}">
                    {!! Form::rating('couverture_'.$lecon->id.'['.$id.'][qualite]', $rating['qualite'], 5, false, true) !!}
                    <div>
                        <a href="{{ route('developpements.show', $id) }}">
                            {{ $developpements[$id] }}
                        </a>
                    </div>
                </li>
                @endif
                @endforeach
            </ul>
        </section>
      @endif
    @endforeach
</div>
</div>
@endif


    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>
</form>
</body>
