<div class="modal fade" id="modal-couplage-explications" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    ×
                </button>

                <h4 class="modal-title">Fonctionnement de l'outil couplage</h4>
            </div>

            <div id="carousel-couplage-explications" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_accueil.png ')}}" alt="Page d'accueil de l'éditeur de couplage.">
                        <div class="carousel-caption">
                            <h4>Paramétrez votre couplage</h4>
                            <p>
                                Ceci est la page de votre couplage.
                                Il y a trois paramètres généraux : <strong>l'année du couplage, l'option choisie et la visibilité</strong>.
                                L'année et l'option permettent à l'outil d'afficher seulement les leçons qui sont pertinentes pour vous.
                                Un couplage public sera accessible par tout le monde à travers la liste des couplages publics (bientôt disponible), alors qu'un couplage privé ne sera accessible que par son créateur.
                                <br />
                                Dans le champs en dessous vous pouvez insérer des <strong>commentaires</strong> sur votre couplage en général, sur son avancée, etc...
                                Ce champ peut notamment servir de <strong>mémo</strong> !
                            </p>
                        </div>
                    </div>


                    <div class="item">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_list_devs.png ')}}" alt="Liste des développements de votre couplage.">
                        <div class="carousel-caption">
                            <h4>Gérez vos développements ici...</h4>
                            <p>
                                Retrouvez ici les développements de votre couplage.
                                Vous pouvez <strong>modifier</strong> leurs recasages dans vos leçons, les <strong>verrouiller</strong> pour éviter des modifications involontaires,
                                    les <strong>retirer</strong> de votre couplage s'ils ne conviennent finalement pas,
                                    en <strong>ajouter</strong> de nouveaux, ...
                                <br/>
                                Si vous voulez rajouter un développement qui n'existe pas encore vous n'avez qu'à le <strong>créer</strong> !
                                N'hésitez pas à créer un nouveau développement même si vous n'êtes pas certain de son recasage : vous pourrez le corriger par la suite !
                                <br/>
                                En dessous vous trouverez la liste de tous les développements qui ont été <strong>commentés</strong> et qui ne sont pas inclus dans votre couplage.
                                Cela vous permet d'<strong>épingler</strong> des développements pour vous rappeler qu'ils sont intéressants et/ou à travailler.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_dev.png')}}" alt="Intégration dans la page d'une leçon.">
                        <div class="carousel-caption">
                            <h4>... ou sur la page d'un développement</h4>
                            <p>
                                Sur la page dédiée à un développement rajouté au couplage, vous pouvez rajouter des <strong>commentaires</strong>, changez le <strong>recasage</strong>
                                    du développement dans les leçons de votre couplage, et <strong>(dé)vérouiller</strong> le développement.
                            </p>
                        </div>
                    </div>


                    <div class="item">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_status_lecon.png ')}}" alt="Plusieurs status pour une leçon sont possibles : impasse, à faire, en cours, finie.">
                        <div class="carousel-caption">
                            <h4>Suivez l'avancement de votre travail</h4>
                            <p>
                                Pour chaque leçon, vous pouvez ajuster son état en fonction de l'avancé de votre travail en sélectionnant un des 4 états possibles:
                                <strong>impasse, à faire (par défaut), en cours, finie</strong>. <br />
                                Retrouvez en un coup d'oeil l'état de toutes vos leçons grace à l'<strong>aperçu</strong> présent en haut de la page de votre couplage.
                                Cet aperçu sert aussi d'<strong>accès rapide</strong> en cliquant sur le numéro de la leçon que vous voulez consulter.
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_lecon.png')}}" alt="Une leçon dans l'éditeur de couplage.">
                        <div class="carousel-caption">
                            <h4>Gérez le couplage d'une leçon sur l'éditeur...</h4>
                            <p>
                                Pour chaque leçon de votre couplage, vous pouvez <strong>ajuster</strong> les recasages des développements de votre couplage, les <strong>supprimer</strong> de cette leçon, ou en <strong>ajouter</strong> de nouveau.
                                <strong>Sélectionnez</strong> les développements que vous voulez finalement présenter dans cette leçon en les cochant.
                                <br />
                                Vous pouvez aussi ajouter des <strong>commentaires</strong> pour des références, des esquisses de plans, des résultats importants à intégrer, ...
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <img class="img-responsive" src="{{ asset('assets/images/couplage_explication_lecon2.png')}}" alt="Intégration dans la page d'une leçon.">
                        <div class="carousel-caption">
                            <h4>... ou sur la page d'une leçon</h4>
                            <p>
                                Sur la page dédiée à une leçon vous retrouverez les mêmes outils que sur l'éditeur de couplage.
                        </div>
                    </div>
                </div>

                <a class="left carousel-control" href="#carousel-couplage-explications" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-couplage-explications" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
