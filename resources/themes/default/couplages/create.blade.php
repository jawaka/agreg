@extends('templates.full-page')

@section('title', "Création d'un couplage")
@section('subnavbar')
    @include('couplages.subnavbar', ['subpage' => 'create'])
@stop

@section('content')
<div class="jumbotron yellow">
<div class="container">
    <h1>
        Votre couplage leçons/développements
    </h1>
    <p>
        Ne perdez plus jamais la sacrée-sainte feuille de votre couplage !
    </p>
</div>
</div>

<div class="container" id="couplage-activate-form">
    <div class="alert alert-info" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Outil non-activé</span>
        Cet outil n'est pas encore activé pour votre compte.
        Pour l'activer, veuillez d'abord remplir le formulaire ci-dessous.
    </div>

    <h2>Paramètres à renseigner : </h2>
    {!! Form::open(['route' => ['couplages.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}

        {!! Form::control('select',    'annee',     'Année',        ['list' => $annees, 'size' => 'lg', 'placeholder' => 'Pour afficher les leçons correspondantes...']) !!}
        {!! Form::control('select',    'option',    'Option',       ['list' => [1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'Docteur'], 'size' => 'lg', 'value' => ($user->option != 0? $user->option : 1)]) !!}
        {!! Form::control('bcheckbox', 'public',    'Public',       ['size' => 'lg', 'label' => '&nbsp;Rendre ce couplage visible pour les autres utilisateurs.', 'checked' => false, 'value' => 1]) !!}

        <br />
        {!! Form::submitbtn('Activer votre couplage', ['size' => 'lg']) !!}
    {!! Form::close() !!}
</div>
@stop


@section('scripts')
<script type="text/javascript">
    $('#couplage-activate-form form').formValidation({
        locale: 'fr_FR',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            annee: {
                validators: {
                    notEmpty: {},
                }
            },
            option: {
                validators: {
                    notEmpty: {},
                }
            },
        }
    });
</script>
@stop