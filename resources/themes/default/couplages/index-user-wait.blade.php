@extends('templates.full-page', ['nomathjax' => true])

@section('title', 'Couplages')
@section('subnavbar')
    @include('outils.subnavbar', ['subpage' => 'couplages'])
@stop

@section('content')
<section id="couplage">
<form autocomplete="off">
    <div class="jumbotron yellow">
    <div class="container">
        <h1>
            Votre couplage leçons/développements
        </h1>
        <p>
            Ne perdez plus jamais la sacrée-sainte feuille de votre couplage !
        </p>
    </div>
    </div>

    <div class="strip" id="parametres">
    <div class="container">
        <h2>
            En refonte totale ! (version béta en cours de test) <br />
        </h2>
            (N'ayez aucune crainte : aucune donnée ne sera perdue)
    </div>
    </div>
@stop
