@include('couplages.recasages', [
    'title'   => "Ajout d'un développement à votre couplage",
    'message' => "Pour finaliser l'ajout du développement <strong>". $developpement->nom ."</strong>, choisissez les leçons dans lesquelles vous pensez pouvoir recaser ce développement.
            Pour chacune de ces leçons, notez sur 5 étoiles la pertinence du recasage de ce développement dans cette leçon.
            <br />
            Ces notes pourront être modifiées par la suite.
            Par défaut, les notes proposées sont celles entrées par l'auteur du développement.",
    'button'  => "Ajouter"
])