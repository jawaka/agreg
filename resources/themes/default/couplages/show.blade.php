@extends('templates.full-page')

@section('title', 'Couplage de '.$couplage->user->name)
@section('subnavbar')
    @include('couplages.subnavbar', ['subpage' => 'show'])
@stop

@section('content')
<section id="couplage">

    @if($couplage->public == false)
        <div class="jumbotron">
            <div class="container">
                Erreur : ce couplage n'est pas public.
            </div>
        </div>
    @else


    <div class="jumbotron">
        <div class="container">
            <h2>
                Couplage de <a href="{{ route('users.show', $couplage->user_id) }}" class="auteur">{{ $couplage->user->name }}</a>
            </h2>
        </div>
    </div>

    


    <div class="strip" id="parametres">
    <div class="container">
        <h3>Paramètres : </h3>

        <div class="row">
            <strong>Année :</strong>
            <div>
                {{ $couplage->annee }}
            </div>
        </div>
        <div class="row">
            <strong>Option :</strong>
            <div>
                {{ $couplage->foption }}
            </div>
        </div>

       

        @if(!empty($couplage->commentaires))
        <div class="row">
            <strong>Commentaires :</strong>
            <div>
                {{ $couplage->commentaires }}
            </div>
        </div>
        @endif
    </div>
    </div>


    <div class="strip" id="apercu">
    <div class="container">
        <h3>Aperçu :</h3>
        <ul>
        @foreach($lecons as $lecon)
            <li>
                <a href="{{ route('lecons.show', $lecon->id) }}">{{ $lecon->numero }}</a>
                @if(count($leconsDevs[$lecon->id]))
                @endif
            </li>
        @endforeach
        </ul>
    </div>
    </div>


    <div class="strip" id="developpements">
    <div class="container">
        <h3>
            <a href="#lecons" class="pull-right">Couverture</a>
            Développements ({{ count($couplage->developpements) }}) : 
        </h3>

        <?php
            $developpementsArray = $couplage->developpementsMoreData()->sortByDesc('recasagesCount');
            $devsName = array();
            foreach($developpementsArray as $d){
                $devsName[$d->id] = $d->nom;
            }
        ?>
        
            <ul id="list-developpements">
            @foreach($developpementsArray as $d)
                <li class="list-group-item developpement-item" data-id="{{ $d->id }}">
                    
                    <div style="padding-left: 10px">
                        <a href="{{ route('developpements.show', $d->id) }}">
                            {{ $d->nom }}
                        </a>
                        
                        <div class="recasagesCount" style="position: relative; float: right; margin-right: 10px">
                            {{ $d->recasagesCount }}
                        </div>
                        <div class="superflu" style="position: relative; float: right; margin-right: 10px">
                            @if ($d->superflu)
                                superflu
                            @endif
                        </div>
                    </div>
                    
                </li>
            @endforeach
            </ul>
    </div>
    </div>


    <div class="strip" id="lecons">
    <div class="container">
        <h3>
            <a href="#" class="pull-right" data-spy="affix" data-offset-top="30" data-offset-bottom="30"><span class="glyphicon glyphicon-arrow-up"></span></a>
            Couverture des leçons : 
        </h3>
        <br>
        
        
        <?php
            $recasages = $couplage->recasages();
        ?>


        @foreach($lecons as $lecon)
        <section class="lecon state-{{ $couplage->getLeconStatus($lecon->id) }}" id="lecon-{{ $lecon->id }}" data-id="{{ $lecon->id }}">
            <span id="lecon-{{ $lecon->id }}-anchor" class="anchor"></span>
                <h4>
                    <a href="{{ route('lecons.show', $lecon->id) }}">
                        {{ $lecon->numero }} : {{ $lecon->nom }}
                    </a>
                </h4>

            <div class="commentaire">
                @if ($user->hasLCommentaire($lecon->id))
                {{ $user->getLCommentaire($lecon->id)->commentaire }}
                @endif
            </div>



            <ul class="couverture">
                @foreach($recasages[$lecon->id] as $id => $rating)
                <li class="list-group-item couverture-rating couverture-rating-{{ $rating['qualite']}} {{ $couplage->couvertureHas($lecon->id, $id)? 'checked' : ''}} {{ $couplage->isLocked($id)? 'locked' : '' }}"
                    data-id="{{ $id }}" data-lecon-id="{{ $lecon->id }}">
                    {!! Form::rating('couverture_'.$lecon->id.'['.$id.'][qualite]', $rating['qualite'], 5, false, $couplage->isLocked($id)) !!}
                    
                    <div>
                        <a href="{{ route('developpements.show', $id) }}">
                            {{ $devsName[$id] }}
                        </a>
                    </div>
                </li>
                @endforeach
            </ul>
        </section>
    @endforeach
    </div>
    </div>

    @endif
</section>
@stop

