@include('couplages.recasages', [
    'title'   => "Modification des recasages d'un développement",
    'message' => "Vous pouvez modifier les recasages du développement.",
    'button'  => "Modifier"
])