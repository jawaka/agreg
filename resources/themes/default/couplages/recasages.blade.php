<div class="modal-dialog">
    <div class="modal-content panel-primary">
        <div class="modal-header panel-heading">
            <button type="button" class="close" data-dismiss="modal">
                ×
            </button>

            <h4 class="modal-title">{!! $title !!}</h4>
        </div>
        <p class="modal-body developpement-lien">
            Page du développement : <a href="{{ route('developpements.show', $developpement->id) }}">{{ $developpement->nom }}</a>
        </p>
        <p class="modal-body">
        {!! $message !!}
        </p>
        <div class="modal-body" id="modal-developpement-recasages">
        <form action="post">
            <input type="hidden" name="id" value="{{ $developpement->id }}" />
            <div class="form-group form-group">
                <ul id="list-developpements">
                {!! Form::lRating('votes',
                    ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...',
                     'val' => $recasages->toRatingArray('qualite')]) !!}
                </ul>
            </div>
        </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
            <button type="submit" class="btn btn-primary">{{ $button }}</button>
        </div>
    </div>
</div>