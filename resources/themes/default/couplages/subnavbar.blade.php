            <ul>
                <li class="clickable active">
                    {{ Html::link(route('couplages.index.user'), 'Couplages') }}
                </li>

@if($subpage == 'create')
                <li class="separator"></li>
                <li>
                    Nouveau couplage
                </li>
@elseif($subpage == 'show')
                <li class="separator"></li>
                <li>
                    Couplage de {{ $couplage->user->name }}
                </li>
@elseif($subpage == 'user')
                <li class="separator"></li>
                <li>
                    Votre couplage
                </li>
@elseif($subpage == 'edit')
                <li class="separator"></li>
                <li>
                    Modification
                </li>
@endif
            </ul>
