@extends('templates.full-page')

@section('title', "Modification des paramètres d'un couplage")
@section('subnavbar')
    @include('couplages.subnavbar', ['subpage' => 'edit'])
@stop

@section('content')
<div class="jumbotron yellow">
    <div class="container">
    <div class="col-md-offset-2">
        <h1>Modification des paramètres d'un couplage</h1>
    </div>
    </div>
</div>
<div class="container" id="couplage-edit-form">
{!! Form::model($couplage, ['route' => ['couplages.update', $couplage->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
    {!! Form::hidden('id', $couplage->id) !!}

    {!! Form::control('select',    'annee',     'Année',        ['list' => $annees, 'size' => 'lg', 'placeholder' => 'Pour afficher les leçons correspondantes...']) !!}
    {!! Form::control('select',    'option',    'Option',       ['list' => [1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'Docteur'], 'size' => 'lg']) !!}
    {!! Form::control('bcheckbox', 'public',    'Public',       ['size' => 'lg', 'label' => '&nbsp;Rendre ce couplage visible pour les autres utilisateurs.', 'checked' => $couplage->public, 'value' => 1]) !!}

    {!! Form::submitbtn('Envoyer', ['size' => 'lg']) !!}
{!! Form::close() !!}
</div>
@stop

@section('scripts')
<script type="text/javascript">
    $('#couplage-edit-form form').formValidation({
        locale: 'fr_FR',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            annee: {
                validators: {
                    notEmpty: {},
                }
            },
            option: {
                validators: {
                    notEmpty: {},
                }
            },
        }
    });
</script>
@stop