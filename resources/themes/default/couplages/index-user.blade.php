@css('assets/css/magic.min.css')
@extends('templates.full-page', ['nomathjax' => true])

@section('title', 'Couplages')
@section('subnavbar')
    @include('couplages.subnavbar', ['subpage' => 'user'])
@stop

@section('content')
<style>
.rempla {
    display: none;
    padding: 10px;
    margin-top: 10px;
    background-color: white;
    border: 1px dashed #ccc;
}
</style>
<section id="couplage">
<form autocomplete="off">
    <div class="jumbotron yellow">
    <div class="container">
        <h1>
            Votre couplage leçons/développements

            <button type="button" id="exportPdf" class="btn-link pull-right" data-toggle="modal" data-target="#modal-export">
                <i class='fa fa-file-pdf-o'></i>
            </button>
        </h1>
        <p>
            Ne perdez plus jamais la sacrée-sainte feuille de votre couplage !
            <br>
            Le lien pour exporter en pdf ne marche plus pour l'instant.
            Une solution temporaire pour l'exporter facilement en noir est blanc est de l'<a href="pdf/{{$couplage->id}}/1/0/0/1/1/1/0/0">exporter en html</a>. 
        </p>
    </div>
    </div>
    <div class="strip" id="explications">
    <div class="container">
        <p>
            <a href="#modal-couplage-explications" role="button" data-toggle="modal" data-target="#modal-couplage-explications">
                Comment ça marche ?
            </a>
        </p>
    </div>
    </div>


    <div class="strip" id="parametres">
    <div class="container">
        <h2>
            Année {{ $couplage->annee }}, option {{ $couplage->foption }}, {!! $couplage->public? 'public' : 'privé' !!}
            <a href="{{ route('couplages.edit', $couplage->id) }}" class="btn btn-primary pull-right">
                <i class="fa fa-gear"></i>
                Modifier les paramètres
            </a>
        </h2>

        @if ($couplage->public)
            Lien pour que les autres visualisent votre couplage : <a href="{{ route('couplages.show', $couplage->id) }}">{{ route('couplages.show', $couplage->id) }}</a>
        @endif

        <h2>
            Commentaires :
        </h2>
        <button type="button" class="btn btn-success" id="couplage-commentaire-ok">
            <span class="glyphicon glyphicon-ok"></span>
        </button>
        <textarea id="couplage-commentaire" placeholder="Pas de commentaires">{!! $couplage->commentaires !!}</textarea>
    </div>
    </div>


    <div class="strip" id="apercu">
    <div class="container">
        <h2>Aperçu :</h2>
        <ul>
        @foreach($lecons as $lecon)
            <li class="state-{{ $couplage->getLeconStatus($lecon->id) }}" id="apercu-lecon-{{ $lecon->id }}">
                <a href="#lecon-{{ $lecon->id }}-anchor">{{ $lecon->numero }}</a>
            </li>
        @endforeach
        </ul>
    </div>
    </div>


    <div class="strip" id="developpements">
    <div class="container">
        <h2>
            <a href="#lecons-anchor" class="pull-right">Couverture</a>
            Développements (<span id="developpements-counter">{{ count($couplage->developpements) }}</span>) :
        </h2>
        Un développement est dit <i>superflu</i> si toutes les leçons dans lesquelles vous l'avez recasé sont couvertes par au moins 3 développements.
        Il est donc possible de le supprimer sans baisser la couverture de votre couplage.
        Si vous supprimez un tel développement, cela peut rendre utile un autre développement superflu. Ainsi la liste peut changer après une suppression ou un ajout d'un développement.

        <br>
        <br>
        Le premier nombre à droite d'un développement est le nombre de leçons dans lesquelles vous l'avez recasé.
        <br>
        Le deuxième nombre est le nombre de leçons dans lesquelles vous l'avez recasé et qui ont 2 développements ou moins.

        {{-- <button id="getPropoRempla">get</button> --}}

        <ul id="list-developpements" class="hover-list">
            @foreach($couplage->developpementsMoreData()->sortByDesc('recasagesCount') as $d)
            <li class="list-group-item developpement-item {{ $couplage->isLocked($d->id)? 'locked' : ''}}" data-id="{{ $d->id }}">
                <div class="developpement-checkbox"></div>
                <button type="button" class="developpement-delete">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
                <button type="button" class="developpement-edit">
                    <span class="glyphicon glyphicon-pencil"></span>
                </button>
                <div>
                    <a href="{{ route('developpements.show', $d->id) }}">
                        {{ $d->nom }}
                    </a>
                    
                    <div class="recaEffectif" data-id="{{ $d->id }}" style="position: relative; float: right; margin-right: 10px">
                        {{ $d->recaEffectif }}
                    </div>
                    <div class="recasagesCount" data-id="{{ $d->id }}" style="position: relative; float: right; margin-right: 10px">
                        {{ $d->recasagesCount }}
                    </div>
                    <div class="superflu" style="position: relative; float: right; margin-right: 10px">
                        @if ($d->superflu)
                            superflu
                        @endif
                    </div>
                    
                </div>
                <div class="rempla" id="rempla{{$d->id}}" data-id="{{ $d->id }}"></div>

            </li>
            @endforeach

            <li class="list-group-item separator"></li>

            <li class="list-group-item clearfix rating-add-container">
                <div class="input-group">
                    <div class="btn input-group-addon rating-add developpement-add-btn">
                        <span class="glyphicon glyphicon-plus"></span>
                    </div>
                    <select data-placeholder="Choisir un développement..." class="form-control chosen-select">
                        <option value=""></option>
                        @foreach($developpements as $id => $elem)
                            <option value="{{$id}}"{!! $couplage->developpements->keyBy('id')->has($id)? ' disabled' : ''!!}>{{ $elem }}</option>
                        @endforeach
                  </select>
                </div>
            </li>
        </ul>
        <div id="new-developpement">
            <a href="{{ route("developpements.create") }}" class="btn btn-success">
                <span class="glyphicon glyphicon-plus"></span>
                Créer un nouveau développement
            </a>
        </div>

        @if($dcommentaires->count())
        <h2>
            Développements commentés ⊈ votre couplage :
        </h2>
        <ul>
        @foreach($dcommentaires as $d)
            <li class="list-group-item">
                <a href="{{ route('developpements.show', $d->developpement->id) }}">
                    {{ $d->developpement->nom }}
                </a>
            </li>
        @endforeach
        @endif
        </ul>
    </div>
    </div>


    <div class="strip" id="lecons">
    <div class="container">
        <span id="lecons-anchor" class="anchor"></span>
        <h2>
            <a href="#" class="pull-right" data-spy="affix"><span class="glyphicon glyphicon-arrow-up"></span></a>
            Couverture des leçons :
        </h2>
        @foreach($lecons as $lecon)
            <section class="lecon state-{{ $couplage->getLeconStatus($lecon->id) }}" id="lecon-{{ $lecon->id }}" data-id="{{ $lecon->id }}">
                <span id="lecon-{{ $lecon->id }}-anchor" class="anchor"></span>
                <div class="lecon-header">
                    {!! Html::switcher('switch-lecon-'. $lecon->id, $couplage->getLeconStatus($lecon->id)) !!}
                    <h3>
                        <a href="{{ route('lecons.show', $lecon->id) }}">
                            {{ $lecon->numero }} : {{ $lecon->nom }}
                        </a>
                    </h3>
                </div>
                <div class="commentaire">
                    <button type="button" class="btn btn-success">
                        <span class="glyphicon glyphicon-ok"></span>
                    </button>
                    <textarea rows="1" data-id="{{ $lecon->id }}" placeholder="Pas de commentaires personnels pour cette leçon">{!!
                        ($user->hasLCommentaire($lecon->id))?
                            $user->getLCommentaire($lecon->id)->commentaire :
                            ''
                        !!}</textarea>
                </div>

                <div class="references">
                    <select data-placeholder="Rajouter une référence personnelle pour cette leçon" class="chosen-select lecons-references" multiple data-id="{{ $lecon->id }}">
                        @foreach($references as $id => $elem)
                        @if($user->hasLReference($lecon->id, $id))
                            <option value="{{$id}}" selected="selected">{{ $elem }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>


                <ul class="couverture">
                    @foreach($recasages[$lecon->id] as $id => $rating)
                    <li class="list-group-item couverture-rating couverture-rating-{{ $rating['qualite']}} {{ $couplage->couvertureHas($lecon->id, $id)? 'checked' : ''}} {{ $couplage->isLocked($id)? 'locked' : '' }}"
                        data-id="{{ $id }}" data-lecon-id="{{ $lecon->id }}">
                        <div class="couverture-checkbox {{ $couplage->couvertureHas($lecon->id, $id)? 'checked' : ''}}" id="couverture_{{ $lecon->id }}_{{ $id }}"></div>
                        {!! Form::rating('couverture_'.$lecon->id.'['.$id.'][qualite]', $rating['qualite'], 5, false, $couplage->isLocked($id)) !!}
                        <button type="button" class="delete">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                        <button type="button" class="developpement-edit">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                        <button type="button" class="developpement-lock">
                            <i class="fa"></i>
                        </button>
                        <div>
                            <a href="{{ route('developpements.show', $id) }}">
                                {{ $developpements[$id] }}
                            </a>
                        </div>
                    </li>
                    @endforeach

                    <li class="list-group-item clearfix rating-add-container">
                        <div class="input-group">
                            <div class="btn input-group-addon rating-add couverture-add-btn">
                                <span class="glyphicon glyphicon-plus"></span>
                            </div>
                            <select data-placeholder="Choisir un développement..." class="form-control chosen-select">
                                <option value=""></option>
                                @foreach($developpements as $id => $elem)
                                @if(array_key_exists($id, $recasages[$lecon->id]))
                                    <option value="{{$id}}" disabled>{{ $elem }}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </li>
                </ul>
            </section>
        @endforeach
    </div>
    </div>
</form>
</section>
@stop

@section('scripts')
<script type="text/javascript">
var couplageAjaxUrl = "{{ route('couplages.ajax') }}";

var dataRefs = [
    @foreach($references as $id => $elem)
    { id:{{$id}}, text:`{!! $elem !!}` },
    @endforeach
];


var dataDevs = [
    @foreach($developpements as $id => $elem)
    { id:{{$id}}, text:`{!! $elem !!}` },
    @endforeach
];

$(function(){
    $('#exportPdf').addClass('magictime tinUpIn');
});
</script>
@stop

@section('modal')
<div class="modal fade" id="modal-developpement"></div>
<div class="modal fade" id="modal-export">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="login-label">Export pdf</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'login', 'method' => 'post', 'class' => 'form-horizontal'])}}
                    <h5>Choisissez les options d'export en PDF :</h5>
                    {!! Form::control('checkbox', 'developpements', 'Liste des développements', ['col' => 9, 'checked' => true]) !!}
                    {!! Form::control('checkbox', 'developpementsCommentaires', 'Commentaires personnels des développements',  ['col' => 9, 'checked' => false]) !!}
                    {!! Form::control('checkbox', 'developpementsReferences', 'Références personnelles des développements',  ['col' => 9, 'checked' => false]) !!}
                    {!! Form::control('checkbox', 'developpementsRecasages', 'Recasages des développements',  ['col' => 9, 'checked' => false]) !!}

                    {!! Form::control('checkbox', 'lecons', 'Couverture des leçons', ['col' => 9, 'checked' => true]) !!}
                    {!! Form::control('checkbox', 'leconsCommentaires', 'Commentaires personels des leçons',  ['col' => 9, 'checked' => true]) !!}
                    {!! Form::control('checkbox', 'leconsReferences', 'Références personelles des leçons',  ['col' => 9, 'checked' => true]) !!}
                    {!! Form::control('checkbox', 'impasse', 'Afficher les impasses',  ['col' => 9, 'checked' => false]) !!}
                    {!! Form::control('checkbox', 'checkedOnly', 'Afficher seulement les recasages séléctionnés', ['col' => 9, 'checked' => false]) !!}

                    <br />
                    <div class="form-group">
                        <div class="col-md-12">
                            {!! Form::button('Exporter', ['class' => 'btn btn-primary btn-lg center-block', 'data-id' => $couplage->id]) !!}
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

{{-- Confirm Delete --}}
@include('partials.confirm-delete', ['formDelete' => false, 'msgconfirm' => 'Attention, ce développement est utilisé dans des leçons de votre couplage. Voulez-vous quand même le supprimer de votre couplage ?'])
@stop
