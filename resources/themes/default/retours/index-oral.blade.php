@extends('templates.full-page')
@section('title', 'Liste des oraux')

@section('subnavbar')
    @include('retours.subnavbar', ['subpage' => 'index-oral'])
@stop

@section('content')
<div class="jumbotron">
    <div class="container">
    <div class="col-md-offset-0">
        <h1>Retours d'oraux : {{ $oral->nom }}</h1>
    </div>
    </div>
</div>
<div class="strip">
    <div class="container">
        @each('retours.partial', $retours, 'retour')
    </div>
</div>
@stop