@extends('templates.full-page')
@section('title', "Modification d'un retour")

@section('subnavbar')
    @include('retours.subnavbar', ['subpage' => 'edit'])
@stop

@section('content')
<div class="jumbotron">
    <div class="container">
        <div class="col-md-offset-2">
            <h1>Modification d'un retour</h1>
        </div>
    </div>
</div>
{!! Form::model($retour, ['route' => ['retours.update', $retour->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'oral-form-edit']) !!}
    {!! Form::hidden('id', $retour->id) !!}

<div class="strip" id="retours-create-form-basic">
<div class="container">
    {!! Form::control('text',      'oraux_type',    'Oral',    ['size' => 'lg', 'value' => $retour->oral->nom,   'attr' => ['disabled']]) !!}
    {!! Form::control('text',      'annee',         'Année',   ['size' => 'lg', 'value' => $retour->annee, 'attr' => ['disabled']]) !!}
    {!! Form::control('bcheckbox', 'anonyme',       'Anonyme', ['size' => 'lg', 'checked' => $retour->anonyme, 'label' => '']) !!}
</div>
</div>

<div class="strip">
<div class="container">
    <div class="form-group">
        <label class="col-md-2 control-label control-label-lg">
            Questions :
        </label> 
        <div class="col-md-10" id="retours-create-form-questions">
            @foreach($retour->oral->questions as $question)
            <p class="form-control-static">
                <i><b>{{ $question->label }}</b></i>
            </p>
            {!! Form::reponse($question, $reponses, $ressources) !!}
            @endforeach
        </div>
    </div>
    {!! Form::submitbtn('Envoyer', ['class' => 'btn btn-primary btn-lg']) !!}
</div>
</div>
{!! Form::close() !!}
@stop
