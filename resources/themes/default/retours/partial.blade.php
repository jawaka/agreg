<?php if(isset($retour->oral)){?>
<div class="panel panel-default retour">
    <div class="panel-heading" data-target="#collapse_panel_{{ $retour->id }}" data-toggle="collapse" aria-expanded="false" aria-controls="collapse_panel_{{ $retour->id }}" role="button">
        <h3>
            <span class="label">{{$retour->annee }}</span>
            {{-- true a été rajouté parce que $retour->anonyme n'est pas un booléen mais un string --}}
            Retour {{ (true || $retour->anonyme || !isset($retour->user))? 'anonyme' : 'de '.$retour->user->name }} ({{ $retour->oral->nom }})
        </h3>
    </div>
    <div class="panel-collapse collapse" id="collapse_panel_{{ $retour->id }}">
        <ul class="list-group">
        @foreach($retour->oral->questions as $question)
            <li class="list-group-item">
                <span class="question">{{ $question->label }}</span>
                {!! Html::reponse($question, $retour->reponses->groupBy('question_id')) !!}
            </li>
        @endforeach
        </ul>
        @if($user = Sentinel::check())
        @if (($user->id == $retour->user_id) || (Sentinel::hasAccess('oral.retour.update')))
        <div class="panel-footer">
            <a href="{{ route('retours.edit', $retour->id) }}" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
                Modifier
            </a>
        </div>
        @endif
        @endif
    </div>
</div>
<?php }?>
