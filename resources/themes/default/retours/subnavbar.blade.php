@if($subpage == 'index')
            <ul>
                <li class="clickable active">
                    {{ Html::link(route('retours.index'), 'Retours') }}
                </li>
                <li class="clickable">
                    {{ Html::link(route('references.index'), 'Références') }}
                </li>

@else
            <ul>
                <li class="clickable active">
                    {{ Html::link(route('retours.index'), 'Retours') }}
                </li>
@if($subpage === 'index-oral')
                <li class="separator"></li>
                <li class="dropdown active clickable">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ $oral->nom }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    @foreach ($oraux as $o)
                        <li>
                            <a href="{{ route('retours.index-oral', [$o->fnom]) }}">
                            {{ $o->nom }}
                            </a>
                        </li>
                    @endforeach
                    </ul>
                </li>
@elseif($subpage == 'create')
                <li class="separator"></li>
                <li>
                    Nouveau retour
                </li>
@elseif($subpage == 'edit')
                <li class="separator"></li>
                <li>
                    Modification
                </li>

@endif
            </ul>
@endif