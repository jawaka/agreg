@extends('templates.full-page')
@section('title', "Création d'un retour")

@section('subnavbar')
    @include('retours.subnavbar', ['subpage' => 'create'])
@stop

@section('content')
<div class="jumbotron">
<div class="container">
<div class="col-md-offset-2">
    <h1>Création d'un retour (2/2)</h1>
</div>
</div>
</div>
{!! Form::open(['route' => ['retours.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="strip" id="retours-create-form-basic">
<div class="container">
    {!! Form::hidden('oraux_type_id', $oral->id) !!}
    {!! Form::control('text',      'oraux_type',    'Oral',    ['size' => 'lg', 'value' => $oral->nom,   'attr' => ['readonly']]) !!}
    {!! Form::control('text',      'annee',         'Année',   ['size' => 'lg', 'value' => $retourAnnee, 'attr' => ['readonly']]) !!}
    {!! Form::control('bcheckbox', 'anonyme',       'Anonyme', ['size' => 'lg', 'checked' => $retourIsAnonyme, 'label' => '']) !!}

</div>
</div>

<div class="strip">
<div class="container">
    <div class="form-group">
        <label class="col-md-2 control-label control-label-lg">
            Questions :
        </label> 
        <div class="col-md-10" id="retours-create-form-questions">
            @foreach($oral->questions as $question)
            <p class="form-control-static">
                <i><b>{{ $question->label }}</b></i>
            </p>
            {!! Form::reponse($question, [], $ressources) !!}
            @endforeach
        </div>
    </div>
    {!! Form::submitbtn('Envoyer', ['class' => 'btn btn-primary btn-lg']) !!}
</div>
</div>
{!! Form::close() !!}
</div>
@stop