@extends('templates.full-page')
@section('title', 'Liste des oraux')

@section('subnavbar')
    @include('retours.subnavbar', ['subpage' => 'index'])
@stop

@section('content')
<div class="jumbotron">
    <div class="container">
    <div class="col-md-offset-0">
        <h1>Retours d'oraux</h1>
        <p>
            Des retours d'expériences des années précédentes.
        </p>
    </div>
    </div>
</div>
<div class="strip">
    <div class="container">
        <div class="list-group">
            @foreach ($oraux as $oral)
            <a href="{{ route('retours.index-oral', [$oral->fnom]) }}" class="list-group-item col-md-3">
                {{ $oral->nom }}
            </a>
            @endforeach
        </div>
    </div>
    @if(Sentinel::check())
    <div class="container">
        <br/>
        <a href="{{ route('retours.create') }}" class="btn btn-success">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;
            Ajouter un retour
        </a>
    </div>
    @endif

    <div class="container">
        <h3>Liste des retours de l'année {{ config('app.annee') }} :</h3>
        @if(count($retours) > 0)
            @each('retours.partial', $retours, 'retour')
        @else
            Pas encore de retour ajouté pour cette session.
        @endif
    </div>
</div>
@stop