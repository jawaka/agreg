@extends('templates.full-page')
@section('title', "Création d'un retour")

@section('subnavbar')
    @include('retours.subnavbar', ['subpage' => 'create'])
@stop

@section('content')
<div class="jumbotron">
<div class="container">
<div class="col-md-offset-2">
    <h1>Création d'un retour (1/2)</h1>
</div>
</div>
</div>
<div class="container">
{!! Form::open(['route' => ['retours.create2'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
    {!! Form::control('cSelect',   'oraux_type_id', 'Oral',   ['size' => 'lg', 'msg' => "Choisir l'oral...",   'list' => $oraux, ]) !!}
    {!! Form::control('text',      'annee',         'Année',  ['size' => 'lg']) !!}
    {!! Form::control('bcheckbox', 'anonyme',       'Anonyme', ['size' => 'lg', 'checked' => false, 'label'=>'']) !!}
    {!! Form::submitbtn('Suite', ['class' => 'btn btn-default btn-lg']) !!}
{!! Form::close() !!}
</div>
@stop