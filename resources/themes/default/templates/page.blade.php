@extends('templates.layout')

@section('page')

@include('partials.navbar')


@section('content-container')
<div class="white" id="content">
    @include('partials.message')
    <div class="container">

        @section('content')
        <p>
          Contenu de la page
        </p>
        @show
    </div>
</div>
@show

@if(!Sentinel::check())
@include('partials.auth-modals')
@endif

@yield('modal')
@stop
