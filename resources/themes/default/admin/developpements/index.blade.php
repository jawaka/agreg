@extends('admin.templates.index')

@php
    $couplages_developpements = DB::table('couplages_developpements')->get();
    $counter = [];
    foreach( $couplages_developpements as $use){
        if (array_key_exists($use->developpement_id, $counter)){
            $counter[$use->developpement_id] ++ ;
        }else {
            $counter[$use->developpement_id] = 1;
        }
    }
@endphp

@section('title', 'Liste des développements')
@section('panel-title', 'Liste des développements')

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Nom</th>
            <th><span title="Nombre de couplages utilisant ce développement">
                Nb couplages
              </span></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($developpements as $developpement)
        <tr class='clickable' data-href="{{ route('admin.developpements.show', $developpement->id) }}">
            <td>{{ $developpement->id  }}</td>
            <td class="primary"><a href="{{ route('admin.developpements.show', $developpement->id) }}">{{ $developpement->nom }}</a></td>
            <td>
                @if(array_key_exists($developpement->id, $counter))
                    {{ $counter[ $developpement->id ] }}
                @else
                    0
                @endif

                </td>
        </tr>
        @endforeach
    </tbody>
@stop
