@extends('admin.templates.show')

@section('title', 'Développement '.$developpement->nom)
@section('panel-title', 'Détails du développement')

@section('display')



    {!! Html::displayText('Auteur',  $developpement->user->name)    !!}
    {!! Html::displayText('Nom',     $developpement->nom)    !!}
    {!! Html::displayText('Détails', $developpement->details) !!}

    {!! Html::displayListOpen('Recasages', 'div') !!}
    @foreach($recasagesByYear as $annee => $recasages)

        {!! Html::collapsePanelOpen('annee_'.$annee, $annee) !!}
            <ul class="list-group">
            @foreach($recasages as $rlecon)
                <li class="list-group-item clearfix">
                    {!! Html::rating($rlecon->pivot->qualite) !!}
                    <a href="{{ route('admin.lecons.show', $rlecon->id) }}">
                        {{ $rlecon->numero }} : 
                        {{ $rlecon->nom }}
                    </a>
                </li>
            @endforeach
            </ul>
        {!! Html::collapsePanelClose() !!}

    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Versions', 'div') !!}
        @each('admin.versions.partial', $developpement->versions, 'version')
    {!! Html::displayListClose() !!}

    {!! Html::displayText('Créé le',    $developpement->created_at) !!}
    {!! Html::displayText('Modifié le', $developpement->updated_at) !!}
    {!! Html::displayListOpen('Utilisation dans les couplages', 'div') !!}



    {{-- Affichage des diffs avec les autres développements.
        J'aimerai bien qu'il affiche les 5 dévs les plus proches en termes de recasages officiels.
        Aussi ceux en recasages non officiels --}}
    <?php
        // $recasages = $developpement->diffAutresDevs();
        // $leconsCouvertes = [];
        // foreach ($recasagesByYear as $annee => $recas) {
        //     if ($annee == 2017 ){
        //         // $leconsCouvertes = $recas;
        //         foreach ($recas as $rlecon) {
        //             echo $rlecon->id;
        //             echo "<br>";
        //             $leconsCouvertes[$rlecon->id] = [
        //                 'numero' => $rlecon->numero,
        //                 'nom' => $rlecon->nom
        //             ];
        //             // utilisation $leconsCouvertes[881]['numero']
        //         }
        //         break;
        //     }
        // }

        // $devsDiffIn = [];
        // $devsDiffOut = [];
        // foreach ($recasages as $reca){
        //     $devId = $reca->developpement_id;
        //     if ( array_key_exists($reca->lecon_id, $leconsCouvertes) ){
        //         $devsDiffIn[$devId] = ($devsDiffIn[$devId] ?? 0) + 1;
        //     } else {
        //         $devsDiffOut[$devId] = ($devsDiffOut[$devId] ?? 0) + 1;
        //     }
        // }
    ?>
    {{-- yo
    @foreach ($devsDiffIn as $devId => $diffIn)

        {{ $devId }} {{ $diffIn }} 
        @if ( array_key_exists($devId, $devsDiffOut) )
            {{ $devsDiffOut[$devId] }}
        @else 
            {{ 0 }}
        @endif
        <br>
    @endforeach --}}
    {{-- @if (count($recasages) === 0)
     <p>No developments found.</p>
    @endif
    @foreach($recasages as $reca)
            {{ $reca->qualite }}
    @endforeach --}}





    {{-- Affiche dans quels leçons les utilisateurs l'ont recasés --}}

    <table style="border: 1px dashed gray; margin: 5px; width: 100%">
        <tr >
            <th>Lecon num</th>
            <th>Titre</th>
            <th>Année</th>
            <th>Nb</th>
            <th>Q moy.</th>
            <th>Q off.</th>
        </tr>
        @foreach($developpement->recasagesUtilisateurs() as $leconId => $use)
        <tr>
            <td> <a href="/lecons/{{$use[2]}}">{{$use[3]}}</a> </td>
            <td> {{ str_limit($use[4], 30) }} </td>
            <td> {{$use[5]}} </td>
            <td> {{$use[1]}} </td>
            <td> {{$use[0]}} </td>
            <td> {{$use[6]}} </td>
        <br>
        </tr>
            @endforeach
    </table>



    {{-- Vielle version obsolète --}}
    {{-- 
    <table style="border: 1px dashed gray; margin: 5px; width: 50%">
        <tr >
            <th>Lecon num</th>
            <th>Average</th>
            <th>Count</th>
        </tr>
        @foreach($developpement->couvertures as $use)
        <tr>
            <td> {{$use->numero}} </td>
            <td> {{$use->avg}} </td>
            <td> {{$use->count}} </td>
        <br>
        </tr>
            @endforeach
    </table> --}}
    {!! Html::displayListClose() !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer ce développement ?'])
@stop

@section('panel-actions')
        <a href="{{ route('admin.developpements.versions.create', $developpement->id) }}" class="btn btn-success">
            <i class="fa fa-plus"></i>
            &nbsp; Ajouter une version
        </a>
@stop