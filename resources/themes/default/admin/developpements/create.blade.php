@extends('admin.templates.form')

@section('title', "Création d'un développement")
@section('panel-title', "Création d'un développement")

@section('form')
{!! Form::open(['route' => ['admin.developpements.store'], 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}

    {!! Form::control('cSelect',  'user_id',   'Auteur', ['list' => $users, 'msg' => "Choisir l'auteur..."]) !!}

    {!! Form::control('text',     'nom',       'Nom') !!}
    {!! Form::control('textarea', 'details',   'Details') !!}
    {!! Form::control('lRating',  'recasages', 'Recasages', 
        ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...']) !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop