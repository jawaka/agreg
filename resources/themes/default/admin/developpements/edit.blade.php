@extends('admin.templates.form')

@section('title', 'Développement '.$developpement->nom)
@section('panel-title', "Modification d'un développement")

@section('form')
{!! Form::model($developpement, ['route' => ['admin.developpements.update', $developpement->id], 'method' => 'put', 'class' => 'form-horizontal panel-body']) !!}
    {!! Form::hidden('id', $developpement->id) !!}

    {!! Form::control('cSelect',  'user_id',  'Auteur', ['list' => $users, 'msg' => "Choisir l'auteur..."]) !!}
    {!! Form::control('text',     'nom',       'Nom') !!}
    {!! Form::control('textarea', 'details',   'Details') !!}
    {!! Form::control('lRating',  'recasages', 'Recasages', 
        ['list' => $lecons, 'field' => 'qualite', 'msg' => 'Choisir une leçon...',
         'val' => $developpement->recasages->toRatingArray('qualite')]) !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
