@extends('admin.templates.form')

@section('title', 'Rôle '.$role->name)
@section('panel-title', "Modification d'un rôle")

@section('form')
{!! Form::model($role, ['route' => ['admin.roles.update', $role->id], 'method' => 'put', 'class' => 'form-horizontal panel-body']) !!}
    {!! Form::hidden('id', $role->id) !!}

    {!! Form::control('text', 'slug', 'Slug') !!}
    {!! Form::control('text', 'name', 'Nom')  !!}
    {!! Form::switcher_array('permissions', 'Permissions') !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop