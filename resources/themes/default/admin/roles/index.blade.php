@extends('admin.templates.index')

@section('title', 'Liste des rôles')
@section('panel-title', 'Liste des rôles')

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Slug</th>
            <th>Nom</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($roles as $role)
        <tr class='clickable' data-href="{{ route('admin.roles.show', [$role->id]) }}">
            <td>{{ $role->id }}</td>
            <td>{{ $role->slug }}</td>
            <td class="primary">{{ $role->name }}</td>
        </tr>
        @endforeach
    </tbody>
@stop