@extends('admin.templates.form')

@section('title', "Création d'un rôle")
@section('panel-title', "Création d'un rôle")

@section('form')
{!! Form::open(['route' => 'admin.roles.store', 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}

    {!! Form::control('text', 'slug', 'Slug')   !!}
    {!! Form::control('text', 'name', 'Nom')    !!}
    {!! Form::switcher_array('permissions', 'Permissions') !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
