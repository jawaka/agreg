@extends('admin.templates.show')

@section('title', 'Rôle '.$role->name)
@section('panel-title', 'Détails du rôle')

@section('display')
    {!! Html::displayText('Slug', $role->slug) !!}
    {!! Html::displayText('Nom',  $role->name) !!}

    {!! Html::displayListOpen('Permissions') !!}
        @foreach($role->permissions as $pname => $pvalue)
            <li class="{{ $pvalue? 'on' : 'off' }}">{{ $pname }}</li>
        @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayText('Créé le',    $role->created_at) !!}
    {!! Html::displayText('Modifié le', $role->updated_at) !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer ce rôle ?'])
@stop
