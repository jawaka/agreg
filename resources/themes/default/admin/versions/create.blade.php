@extends('admin.templates.form')

@section('title', "Création d'une version")
@section('panel-title', "Création d'une version")

@section('form')
{!! Form::open(['route' => ['admin.versions.store'], 'method' => 'post']) !!}
    {!! Form::hidden('versionable_id', $versionable->id) !!}
    {!! Form::hidden('versionable_type', (get_class($versionable) == 'Agreg\Models\Lecon\Lecon'? 'lecon' : 'developpement')) !!}

    {!! Form::control('cSelect',  'user_id',   'Auteur', ['list' => $users, 'msg' => "Choisir l'auteur..."]) !!}
    {!! Form::control('textarea', 'remarque',  'Remarque') !!}

    {!! Form::submitbtn('Suite', ['type' => 'default']) !!}
{!! Form::close() !!}
@stop
