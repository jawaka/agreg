@extends('admin.templates.index')

@section('title', 'Liste des versions')
@section('panel-title', 'Liste des versions')

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Type</th>
            <th>Versionable</th>
            <th>Auteur</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($versions as $version)
        <tr class='clickable' data-href="{{ route('admin.versions.show', $version->id) }}">
            <td>{{ $version->id     }}</td>
            <td>{{ ucfirst($version->versionable_type) }}</td>
            <td class="primary"><a href="{{ route('admin.versions.show', $version->id) }}">{{ $version->versionable->nom  }}</a></td>
            <td>{{ $version->user->name }} </td>
        </tr>
        @endforeach
    </tbody>
@stop
