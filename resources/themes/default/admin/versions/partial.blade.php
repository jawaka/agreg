{!! Html::collapsePanelOpen('version_'.$version->id,  'Version de '.$version->user->name) !!}
<ul class="display">
    {!! Html::displayText('Remarque', $version->remarque) !!}

    {!! Html::displayListOpen('Références') !!}
    @foreach($version->references as $ref)
        <li>
            <a href="{{ route('admin.references.show', $ref->id) }}">{{ $ref->titre }}</a>
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Fichiers') !!}
    @foreach($version->fichiers as $f)
        <li>
            <a href="{{ $version->url . $f->url }}">
                {!! Html::fileIcon($version->url . $f->url) !!}
                {{ $f->url }}
            </a>
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

</ul>
<div class="panel-footer clearfix display">
    <strong>Actions :</strong>
    <div>
        <a href="{{ route('admin.versions.show', $version->id) }}" class="btn btn-primary">
            <i class="fa fa-info"></i>
            &nbsp; Voir
        </a>
        <a href="{{ route('admin.versions.edit', $version->id) }}" class="btn btn-warning">
            <i class="fa fa-pencil"></i>
            &nbsp; Modifier
        </a>
    </div>
</div>

{!! Html::collapsePanelClose() !!}

