@extends('admin.templates.show')

@section('title', 'Version de '.$version->user->name)
@section('panel-title', 'Détails de la version')

@section('display')
    {!! Html::displayText('Version de', $version->versionable->nom) !!}
    {!! Html::displayText('Auteur',     $version->user->name)  !!}
    {!! Html::displayText('Remarque',   $version->remarque)   !!}

    {!! Html::displayListOpen('Références') !!}
    @foreach($version->references as $ref)
        <li>
            <a href="{{ route('admin.references.show', $ref->id) }}">
                {{ $ref->titre }}
            </a>
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Fichiers') !!}
    @foreach($version->fichiers as $f)
        <li>
            <a href="{{ $version->url . $f->url }}">
                {!! Html::fileIcon($version->url . $f->url) !!}
                {{ $f->url }}
            </a>
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayText('Créée le',    $version->created_at) !!}
    {!! Html::displayText('Modifiée le', $version->updated_at) !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cette version ?'])
@stop
