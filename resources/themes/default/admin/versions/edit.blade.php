@extends('admin.templates.form')
@css('css/fileinput.css')

@section('title', 'Version de '.$version->user->name)
@section('panel-title', "Modification d'une version")

@section('form')
{!! Form::model($version, ['route' => ['admin.versions.update', $version->id], 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}

    {!! Form::control('cSelect',  'user_id',  'Auteur', ['list' => $users, 'msg' => "Choisir l'auteur..."]) !!}
    {!! Form::control('textarea', 'remarque', 'Remarque') !!}
    {!! Form::control('lSelect',  'references', 'Références',
        ['list' => $references, 'msg' => 'Choisir une référence...', 'val' => $version->references->toList()]) !!}

    {!! Form::groupOpen('Fichiers', 'ul') !!}
        @foreach($version->fichiers as $f)
        <li class="list-group-item">
            <div class="input-group">
                {!! Form::text('fichiers['. $f->id .'][url]', $f->url, ['class' => 'form-control']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary file-delete" data-id="{{ $f->id }}">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </span>
            </div>
        </li>
        @endforeach
        
        <li class="list-group-item" id="version_file_input_li">
            <input type="file" name="version_fichier" id="version_file_input" data-language="fr" data-show-preview="false" data-upload-url="uploadFile" />
        </li>
    {!! Form::groupClose() !!}


    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', [
        'msgconfirm'   => 'Voulez-vous vraiment supprimer ce fichier ?', 
        'deleteRoute'  => 'admin.versions.deleteFile',
        'deleteFormId' => 'form-delete-file'
    ])
@stop
