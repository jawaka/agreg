<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home"></span></a>
            <a class="navbar-brand" href="/admin">Admin</a>
        </div>

        <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li @if(Request::is('admin')) class="active" @endif>
                {{ Html::link(route('admin.index'), 'Index') }}
            </li>
            <li @if(Request::is('admin/developpements*')) class="active" @endif>
                {{ Html::link(route('admin.developpements.index'), 'Développements')}}
            </li>
            <li @if(Request::is('admin/lecons*')) class="active" @endif>
                {{ Html::link(route('admin.lecons.index'), 'Leçons')}}
            </li>
            <li @if(Request::is('admin/versions*')) class="active" @endif>
                {{ Html::link(route('admin.versions.index'), 'Versions')}}
            </li>
            <li @if(Request::is('admin/references*')) class="active" @endif>
                {{ Html::link(route('admin.references.index'), 'Références')}}
            </li>
            <li @if(Request::is('admin/users*')) class="active" @endif>
                {{ Html::link(route('admin.users.index'), 'Membres')}}
            </li>
            <li @if(Request::is('admin/roles*')) class="active" @endif>
                {{ Html::link(route('admin.roles.index'), 'Roles')}}
            </li>
            <li @if(Request::is('admin/oraux*')) class="active" @endif>
                {{ Html::link(route('admin.oraux.index'), 'Oraux')}}
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
        @if($user = Sentinel::check())
            <li>{{ Html::link(route('admin.users.show', $user->id), $user->name) }}</li>
            <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span></a></li>
        @else
            <li>{{ Html::link('/login', 'Connexion') }}</li>
        @endif
        </ul>
      </div>
    </div>
</nav>
