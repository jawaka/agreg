<?php $route = Route::getCurrentRoute()->getName(); ?>

@if(isset($developpement))
    {!! BC::render($route, $developpement) !!}

@elseif(isset($anneeLecon))
    {!! BC::render($route, $anneeLecon) !!}
@elseif(isset($lecon))
    {!! BC::render($route, $lecon) !!}

@elseif(isset($version))
    {!! BC::render($route, $version) !!}

@elseif(isset($reference))
    {!! BC::render($route, $reference) !!}

@elseif(isset($User))
    {!! BC::render($route, $User) !!}

@elseif(isset($role))
    {!! BC::render($route, $role) !!}

@elseif(isset($versionable))
    {!! BC::render($route, $versionable) !!}

@elseif(isset($oral))
    {!! BC::render($route, $oral) !!}

@elseif(isset($retour))
    {!! BC::render($route, $retour) !!}

@else
    {!! BC::render() !!}
@endif
