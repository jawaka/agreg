@extends('admin.templates.show')

@section('title', 'Leçon '.$lecon->numero.' : '.$lecon->nom)

<div style="text-align: center">
    <div style="margin: 0 auto">
        <a href="/admin/lecons/{{$lecon->id-1}}"> <- Leçon précédente </a>
        ---
        <a href="/admin/lecons/{{$lecon->id+1}}"> Leçon suivante -></a>
    </div>
</div>


@section('panel-title', 'Détails de la leçon')

@section('display')
    {!! Html::displayText('Annee',   $lecon->annee)     !!}
    {!! Html::displayText('Numéro',  $lecon->numero)    !!}
    {!! Html::displayText('Type',    $lecon->type->nom) !!}
    {!! Html::displayText('Nom',     $lecon->nom)       !!}
    
    {!! Html::displayOpen('Options') !!}
        @foreach($lecon->options as $option => $value)
            {{ $value ? $option : '' }}
        @endforeach
    {!! Html::displayClose() !!}
    
    {!! Html::displayText('Rapport', $lecon->rapport)   !!}

    {!! Html::displayListOpen('Parents') !!}
    @foreach($lecon->parents as $plecon)
        <li>
            {{ $plecon->numero }} : {{ $plecon->nom }}
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Enfants') !!}
    @foreach($lecon->enfants as $plecon)
        <li>
            {{ $plecon->numero }} : {{ $plecon->nom }}
        </li>
    @endforeach
    {!! Html::displayListClose() !!}


    {!! Html::displayListOpen('Recasages') !!}
    @foreach($recasages as $rdeveloppement)
        <li>
            {!! Html::rating($rdeveloppement->pivot->qualite) !!}
            <a href="{{ route('admin.developpements.show', $rdeveloppement->id) }}">
                {{ $rdeveloppement->nom }}
            </a>
        </li>
    @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Versions', 'div') !!}
        @each('admin.versions.partial', $lecon->versions, 'version')
    {!! Html::displayListClose() !!}

    {!! Html::displayText('Créée le',    $lecon->created_at) !!}
    {!! Html::displayText('Modifiée le', $lecon->updated_at) !!}
    {!! Html::displayListOpen('Utilisation dans les couplages', 'div') !!}
        <table style="border: 1px dashed gray; margin: 5px; width: 100%">
            <tr>  
                <th>ID</th> 
                <th>Titre</th> 
                <th>Occurences</th>
                <th>Qualité moyenne</th> 
                <th>Qualité officielle</th>   
            </tr>  
            @foreach($lecon->stats() as $developpement_id => $stat)
                <tr>
                    <td> <a href="/admin/developpements/{{$stat['id']}}"> {{$stat["id"]}} </a> </td>
                    <td> {{$stat["nom"]}} </td>
                    <td> {{$stat["count"]}} </td>
                    <td> {{$stat["quality_avg"]}} </td>
                    <td> {{$stat["quality_reca"]}}</td>
                </tr>
            @endforeach
         </table>
        <br>
    {!! Html::displayListClose() !!}
        


@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cette leçon ?'])
@stop

@section('panel-actions')
        <a href="{{ route('admin.lecons.versions.create', $lecon->id) }}" class="btn btn-success">
            <i class="fa fa-plus"></i>
            &nbsp; Ajouter une version
        </a>
@stop