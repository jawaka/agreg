@extends('admin.templates.index')

@section('title', "Liste des leçons de l'année ". $anneeLecon)
@section('panel-title', "Liste des leçons de l'année ". $anneeLecon)

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Numéro</th>
            <th>Nom</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($lecons as $lecon)
        <tr class='clickable' data-href="{{ route('admin.lecons.show', [$lecon->id]) }}">
            <td>{{ $lecon->id     }}</td>
            <td>{{ $lecon->numero }}</td>
            <td class="primary">{{ $lecon->nom }}</td>
        </tr>
        @endforeach
    </tbody>
@stop