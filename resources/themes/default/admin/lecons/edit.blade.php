@extends('admin.templates.form')

@section('title', 'Leçon '.$lecon->numero.' : '.$lecon->nom)
@section('panel-title', "Modification d'une leçon")

@section('form')
{!! Form::model($lecon, ['route' => ['admin.lecons.update', $lecon->id], 'method' => 'put']) !!}
    {!! Form::hidden('id', $lecon->id) !!}

    {!! Form::control('text',     'annee',   'Année') !!}
    {!! Form::control('text',     'numero',  'Numéro') !!}
    {!! Form::control('lRadio',   'type_id', 'Type',    ['list' => $leconsTypes])  !!}
    {!! Form::control('text',     'nom',     'Nom') !!}
    {!! Form::control('lCheckbox','options', 'Options', ['list' => $leconsOptions, 'val' => $lecon->options]) !!}
    {!! Form::control('textarea', 'rapport', 'Rapport') !!}
    {!! Form::control('lSelect',  'parents', 'Parents',
        ['list' => $leconsParents, 'msg' => 'Choisir une leçon...', 'val' => $lecon->parents->toList()]) !!}
    {!! Form::control('lSelect',  'enfants', 'Enfants',
        ['list' => $leconsEnfants, 'msg' => 'Choisir une leçon...', 'val' => $lecon->enfants->toList()]) !!}
    {!! Form::control('lRating', 'recasages', 'Recasages', 
        ['list' => $developpements, 'field' => 'qualite', 'msg' => 'Choisir un développement...',
         'val' =>  $lecon->recasages->toRatingArray('qualite')]) !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
