@extends('admin.templates.index')

@section('title', 'Liste des leçons')
@section('panel-heading')
<div class="dropdown pull-right">
    <a href="#" class="dropdown-toggle" type="button" id="annees_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Afficher uniquement une année
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" aria-labelledby="annees_dropdown">
    @foreach($leconsAnnees as $annee)
        <li><a href="{{ route('admin.lecons.index.annee', $annee) }}">{{ $annee }}</a></li>
    @endforeach
    </ul>
</div>

<h1 class="panel-title">Liste des leçons</h1>
@stop

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Année</th>
            <th>Numéro</th>
            <th>Nom</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($lecons as $lecon)
        <tr class='clickable' data-href="{{ route('admin.lecons.show', [$lecon->id]) }}">
            <td>{{ $lecon->id     }}</td>
            <td>{{ $lecon->annee  }}</td>
            <td>{{ $lecon->numero }}</td>
            <td class="primary"><a href="{{ route('admin.lecons.show', [$lecon->id]) }}">{{ $lecon->nom }}</a></td>
        </tr>
        @endforeach
    </tbody>
@stop
