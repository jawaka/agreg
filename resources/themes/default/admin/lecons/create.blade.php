@extends('admin.templates.form')

@section('title', "Création d'une leçon")
@section('panel-title', "Création d'une leçon")

@section('form')
{!! Form::open(['route' => ['admin.lecons.store'], 'method' => 'post']) !!}

    {!! Form::control('text',     'annee',     'Année') !!}
    {!! Form::control('text',     'numero',    'Numéro') !!}
    {!! Form::control('lRadio',   'type_id',   'Type', ['list' => $leconsTypes]) !!}
    {!! Form::control('text',     'nom',       'Nom') !!}
    {!! Form::control('lCheckbox','options',   'Options', ['list' => $leconsOptions]) !!}
    {!! Form::control('textarea', 'rapport',   'Rapport') !!}
    {!! Form::control('lRating',  'recasages', 'Recasages', 
        ['list' => $developpements, 'field' => 'qualite', 'msg' => 'Choisir un développement...']) !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
