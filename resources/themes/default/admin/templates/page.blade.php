@extends('admin.templates.layout')

@section('page')
    @include('admin.partials.navbar')
    @include('admin.partials.breadcrumbs')

    <div class="container">
        <div class="col-sm-offset-1 col-sm-10">
        @include('admin.partials.message')

        @section('content')
        <p>
          Contenu de la page
        </p>
        @show
        </div>
    </div>

    @yield('modal')
@stop