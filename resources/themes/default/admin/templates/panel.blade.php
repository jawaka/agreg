@extends('admin.templates.page')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        @section('panel-heading')
        <h1 class="panel-title">@yield('panel-title')</h1>
        @show
    </div>
@yield('panel-content')
</div>

@yield('panel-links')
@stop