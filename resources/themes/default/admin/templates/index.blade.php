@extends('admin.templates.panel')

@section('panel-content')
<table class="table table-striped table-bordered" id="resourceTable">
    @yield('resource-table')
</table>
@stop
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#resourceTable').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ éléments",
            "search": "<span class='glyphicon glyphicon-search'></span>",
            "paginate": {
                "next":      "Suivant",
                "previous":  "Précédent"
            },
        },
        "info": false,
        "dom": '<"dataTables_header"lf>t<"dataTables_footer"<"dataTables_toolbar">ip>',
        "stateSave": true,
        {!! $tableorder or '' !!}
        {!! $tablecolumns or '' !!}
    });

    @if(!isset($tablenoadd))
    $("div.dataTables_toolbar").html(
        '<a href="{{ route("admin.". Request::segment(2) .".create") }}" class="btn btn-success">'
        +   '<span class="glyphicon glyphicon-plus"></span>'
        + ' Ajouter</a>');
    @endif
});
</script>
@stop