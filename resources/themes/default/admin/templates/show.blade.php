@extends('admin.templates.panel')

@section('panel-content')
<ul class="list-group">
    @yield('display')
</ul>
<div class="panel-footer clearfix display">
    <strong>Actions :</strong>
    <div>
        @yield('panel-actions')
        <a href="{{ route('admin.'. Request::segment(2) .'.edit', Request::segment(3)) }}" class="btn btn-warning">
            <i class="fa fa-pencil"></i>
            &nbsp; Modifier
        </a>

        <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-delete">
            <i class="fa fa-times"></i>
            &nbsp; Supprimer
        </button>
    </div>
</div>
@stop