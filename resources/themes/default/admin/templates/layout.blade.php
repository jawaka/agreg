@css('css/admin.css')
@js('js/admin.js', 'admin')

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Agreg.fr - @yield('title')</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! Asset::styles() !!}
</head>
<body>
    @section('page')
    <p>
      Contenu de la page
    </p>
    @show
    
    {!! Asset::scripts() !!}
    @yield('scripts')
</body>