@extends('admin.templates.form')

@section('title', "Gestionnaire des doublons")
@section('panel-title', "Gestionnaire des doublons")

@section('form')
    
    {!! Form::open(['url' => route('admin.outils.doublon.operate'), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'doublon-form']) !!}

    Développement à garder :

    <div class="input-group">
        <div type="button" class="btn input-group-addon rating-add couverture-add-btn"></div>
        <select name="to_keep" data-placeholder="Choisir un développement..." class="form-control chosen-select">
            <option value=""></option>
            @foreach($developpements as $id => $elem)
                <option value="{{$id}}">{{ $id}} {{ $elem }}</option>
            @endforeach
      </select>
    </div>

    Doublon à supprimer :

    <div class="input-group">
        <div type="button" class="btn input-group-addon rating-add couverture-add-btn"></div>
        <select name="to_delete" data-placeholder="Choisir un développement..." class="form-control chosen-select">
            <option value=""></option>
            @foreach($developpements as $id => $elem)
                <option value="{{$id}}">{{ $id}} {{ $elem }}</option>
            @endforeach
      </select>
    </div>

    {!! Form::submitbtn('Envoyer') !!}

    {!! Form::close() !!}

@stop
