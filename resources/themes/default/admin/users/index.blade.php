@extends('admin.templates.index')

@section('title', 'Liste des utilisateurs')
@section('panel-title', 'Liste des utilisateurs')

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Pseudo</th>
            <th>Email</th>
            <th>Rôles</th>
            <th>Permissions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr class='clickable' data-href="{{ route('admin.users.show', $user->id) }}">
            <td>{{ $user->id }}</td>
            <td class="primary"><a href="{{ route('admin.users.show', $user->id) }}">{{ $user->name }}</a></td>
            <td>{{ $user->email }}</td>
            <td>
                @foreach($user->roles as $role)
                    {{ $role->name }}
                @endforeach
            </td>
            <td>
                @foreach($user->permissions as $pname => $pvalue)
                    {{ $pname }}
                @endforeach
            </td>
        </tr>
        @endforeach
    </tbody>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#resourceTable').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ éléments",
            "search": "<span class='glyphicon glyphicon-search'></span>",
            "paginate": {
                "next":      "Suivant",
                "previous":  "Précédent"
            },
        },
        "info": false,
        "dom": '<"dataTables_header"lf>t<"dataTables_footer"<"dataTables_toolbar">ip>',
        "stateSave": true,
        "columnDefs": [
            {
                "targets": [ 3,4 ],
                "visible": false,
            }
        ]
    });
});
</script>
@stop
