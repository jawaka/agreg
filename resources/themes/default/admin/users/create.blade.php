@extends('admin.templates.form')

@section('title', "Création d'un utilisateur")
@section('panel-title', "Création d'un utilisateur")

@section('form')
{!! Form::open(['route' => 'admin.users.store', 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}

    {!! Form::control('text',  'name',  'Pseudo') !!}
    {!! Form::control('email', 'email', 'Email') !!}
    {!! Form::control('checkbox', 'montrer_email', 'Email public', ['checked' => false]) !!}
    {!! Form::control('password', 'password', 'Mot de passe') !!}
    {!! Form::control('password', 'password_confirmation', 'Confirmer', ['attr'=>['placeholder'=>'Mot de passe']]) !!}
    {!! Form::switcher_array('permissions', 'Permissions') !!}
    {!! Form::control('lCheckbox', 'roles', 'Rôles', ['list' => $roles]) !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop