@extends('admin.templates.form')

@section('title', 'Utilisateur '.$User->name)
@section('panel-title', "Modification d'un utilisateur")

@section('form')
{!! Form::model($User, ['route' => ['admin.users.update', $User->id], 'method' => 'put', 'class' => 'form-horizontal panel-body']) !!}
    {!! Form::hidden('id', $User->id) !!}

    {!! Form::control('text',  'name',  'Pseudo') !!}
    {!! Form::control('email', 'email', 'Email') !!}
    {!! Form::control('checkbox', 'montrer_email', 'Email public', ['checked' => $User->montrer_email]) !!}
    {!! Form::control('text', 'annee', "Inscrit à l'agrégation") !!}
    {!! Form::control('select', 'option', 'Option', ['list' => [0 => 'Non renseigné', 1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'Docteur']]) !!}
    {!! Form::control('select', 'resultat', 'Résultat', ['list' => [0 => 'Non renseigné', 1 => 'Non-admissible', 2 => 'Admissible', 3 => 'Admis']]) !!}
    {!! Form::control('text', 'classement', 'Classement') !!}
    {!! Form::control('textarea', 'biographie', 'Biographie', ['attr' => ['size' => '50x5']]) !!}

    {!! Form::switcher_array('permissions', 'Permissions') !!}

    {!! Form::groupOpen('Rôles') !!}
    @foreach($roles as $role)
        {!! Form::bcheckbox('roles['.$role->id.']', $role->name, 1, $User->inRole($role)) !!}
    @endforeach
    {!! Form::groupClose() !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
