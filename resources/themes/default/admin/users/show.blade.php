@extends('admin.templates.show')

@section('title', 'Utilisateur '.$User->name)
@section('panel-title', "Fiche d'utilisateur")

@section('display')
    {!! Html::displayText('Pseudo', $User->name) !!}
    {!! Html::displayText('Email',  $User->email) !!}
    {!! Html::displayText('Email public',  ($User->montrer_email? 'Oui' : 'Non')) !!}
    {!! Html::displayText("Inscrit à l'agrégation",  ($User->annee.", option ".$User->foption)) !!}
    {!! Html::displayText("Résultat",   $User->fresultat) !!}
    {!! Html::displayText('Biographie', $User->biographie) !!}


    {!! Html::displayListOpen('Permissions') !!}
        @foreach($User->permissions as $pname => $pvalue)
            <li class="list-group-item list-group-item-{{ $pvalue? 'success' : 'danger' }}">{{ $pname }}</li>
        @endforeach
    {!! Html::displayListClose() !!}


    {!! Html::displayListOpen('Rôles', 'div') !!}
        @foreach($User->roles as $role)
            {!! Html::collapsePanelOpen($role->slug, $role->name) !!}
                <ul class="list-group">
                @foreach($role->permissions as $pname => $pvalue)
                    <li class="list-group-item list-group-item-{{ $User->hasAccess($pname)? 'success' : 'danger' }}">{{ $pname }}</li>
                @endforeach
                </ul>
            {!! Html::collapsePanelClose() !!}
        @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayListOpen('Versions', 'div') !!}
        @foreach($User->versions as $uversion)
            <a href="{{ route('admin.versions.show', $uversion->id) }}">
                {{ $uversion->versionable->nom }}
            </a>
            <br />
        @endforeach
    {!! Html::displayListClose() !!}

    {!! Html::displayText('Créé le',  $User->created_at) !!}
    {!! Html::displayText('Modifié le',  $User->updated_at) !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cet utilisateur ?'])
@stop
