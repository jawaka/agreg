@extends('admin.templates.form')

@section('title', "Création d'un oral")
@section('panel-title', "Création d'un oral")

@section('form')
{!! Form::open(['route' => ['admin.oraux.store'], 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}
    {!! Form::control('text',     'nom',     'Nom',     'Nom',     $errors) !!}
    {!! Form::control('text',     'fnom',    'Slug',    'Slug',     $errors) !!}
    {!! Form::submitbtn('Suite', ['type' => 'default']) !!}
{!! Form::close() !!}
@stop