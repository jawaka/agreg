@extends('admin.templates.form')

@section('title', 'Oral '.$oral->nom)
@section('panel-title', "Modification d'un oral")

@section('form')
{!! Form::model($oral, ['route' => ['admin.oraux.update', $oral->id], 'method' => 'put', 'class' => 'form-horizontal panel-body', 'id' => 'oral-form-edit']) !!}
    {!! Form::hidden('id', $oral->id) !!}

    {!! Form::control('text',     'nom',     'Nom',     'Nom',     $errors) !!}
    {!! Form::control('text',    'fnom',    'Slug',    'Slug',     $errors) !!}

    @foreach($oral->questions as $question)
    <div class="form-group question-group">
        <label class="col-md-2 control-label">
            Q{{ $question->ordre }} :
        </label> 
        <div class="col-md-8 question">
            {!! Form::hidden('questionsIds[]', $question->id) !!}
            {!! Form::textarea('questions[]', $question->label, ['class' => 'form-control', 'placeholder' => 'Intitulé de la question', 'rows' => 4]) !!}
            {!! Form::cSelect('questionsTypes[]', $types, 'Type de question',  [$question->type->id]) !!}
        </div>
        <div class="btn-group col-md-2" role="group">
            <button type="button" class="btn btn-sm btn-default up"><span class="glyphicon glyphicon-chevron-up"></span></button>
            <button type="button" class="btn btn-sm btn-default down"><span class="glyphicon glyphicon-chevron-down"></span></button>
        </div>
    </div>
    @endforeach

    <div class="form-group hidden">
        <label class="col-md-2 control-label">
        </label> 
        <div class="col-md-8 question">
            {!! Form::hidden('__questionId', 0) !!}
            {!! Form::textarea('__question', '', ['class' => 'form-control', 'placeholder' => 'Intitulé de la question', 'rows' => 4]) !!}
            {!! Form::select('__questionType', $types, [], ['data-placeholder' => 'Type de question', 'class' => 'form-control']) !!}
        </div>
        <div class="btn-group col-md-2" role="group">
            <button type="button" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-up"></span></button>
            <button type="button" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-down"></span></button>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-10 col-md-offset-2">
            <button type="button" class="btn btn-success" id='oral-form-add-question'><span class="glyphicon glyphicon-plus"></span> Ajouter une question</button>
            <button type="button" class="btn btn-danger" id='oral-form-delete-question'><span class="glyphicon glyphicon-minus"></span> Supprimer une question</button>
            <button type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </div>
{!! Form::close() !!}
@stop
