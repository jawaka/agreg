@extends('admin.templates.show')

@section('title', 'Oral '.$oral->nom)
@section('panel-title', "Détails de l'oral")

@section('display')
    {!! Html::displayText('Nom',     $oral->nom)    !!}
    {!! Html::displayText('Slug',    $oral->fnom)    !!}


    @foreach($oral->questions as $question)
    {!! Html::displayOpen('Q'.$question->ordre) !!}
        <i><b>{{ $question->label }}</b></i>
        ({{ $question->type->nom }})
    {!! Html::displayClose() !!}
    @endforeach
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cet oral ?'])
@stop
