@extends('admin.templates.page')

@section('title', 'Liste des oraux')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title">Liste des oraux</h1>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            @foreach ($oraux as $oral)
            <li class="list-group-item col-md-6">
                <a href="{{ route('admin.oraux.show', [$oral->id]) }}">
                    {{ $oral->nom }}
                </a>
            </li>
            @endforeach
            <li class="list-group-item col-md-6">
                <a href="{{ route('admin.oraux.create') }}" >
                    <span class="glyphicon glyphicon-plus"></span>
                    Créer un nouvel oral
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title">Liste des retours d'oraux</h1>
    </div>
    <table class="table table-striped table-bordered" id="resourceTable">
        <thead>
            <tr>
                <th>#</th>
                <th>Oral</th>
                <th>User</th>
                <th>Année</th>
                <th>Anonyme</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($retours as $retour)
            <tr class="clickable" data-href="{{ route('admin.retours.show', $retour->id) }}">
                <td>{{ $retour->id }}</td>
                <td>{{ $retour->oral->nom }}</td>
                <td>{{ $retour->user->name }}</td>
                <td>{{ $retour->annee }}</td>
                <td>{{ $retour->anonyme? 'Oui' : 'Non' }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop


@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $('#resourceTable').DataTable({
        "language": {
            "lengthMenu": "Afficher _MENU_ éléments",
            "search": "<span class='glyphicon glyphicon-search'></span>",
            "paginate": {
                "next":      "Suivant",
                "previous":  "Précédent"
            },
        },
        "info": false,
        "dom": '<"dataTables_header"lf>t<"dataTables_footer"<"dataTables_toolbar">ip>',
        {!! $tableorder or '' !!}
        {!! $tablecolumns or '' !!}
    });

    @if(!isset($tablenoadd))
    $("div.dataTables_toolbar").html(
        '<a href="{{ route("admin.retours.create") }}" class="btn btn-success">'
        +   '<span class="glyphicon glyphicon-plus"></span>'
        + ' Ajouter</a>');
    @endif
});
</script>
@stop