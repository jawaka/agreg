@extends('admin.templates.index', ['tablenoadd' => true])

@section('title', "Index de l'administration")
@section('panel-title', "Index de l'administration")

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-2">Date</th>
            <th>Type</th>
            <th>Lien</th>
            <th>Utilisateur</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ressources as $r)
        <tr class='clickable' data-href="{{ $r['href'] }}">
            <td data-order={{ $r['date']->timestamp }}>{{ $r['date']->format('d/m/Y - H:i') }}</td>
            <td>{{ $r['type'] }}</td>
            <td class="primary"><a href="{{ $r['href'] }}">{{ $r['nom'] }}</a></td>
            <td>{{ $r['user'] }}</td>
            <td>
                @if($r['date'] == $r['cdate'])
                    <span class="glyphicon glyphicon-plus"></span>
                @else
                    <span class="glyphicon glyphicon-pencil"></span>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
@stop
