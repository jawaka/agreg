@extends('admin.templates.form')

@section('title', "Création d'une référence")
@section('panel-title', "Création d'une référence")

@section('form')
{!! Form::open(['route' => 'admin.references.store', 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}

    {!! Form::control('text', 'titre',   'Titre')   !!}
    {!! Form::control('text', 'auteurs', 'Auteurs') !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
