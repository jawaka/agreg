@extends('admin.templates.form')

@section('title', 'Référence '.$reference->titre)
@section('panel-title', "Modification d'une référence")

@section('form')
{!! Form::model($reference, ['route' => ['admin.references.update', $reference->id], 'method' => 'put', 'class' => 'form-horizontal panel-body']) !!}
    {!! Form::hidden('id', $reference->id) !!}

    {!! Form::control('text', 'titre',   'Titre')   !!}
    {!! Form::control('text', 'auteurs', 'Auteurs') !!}
    {!! Form::control('text', 'cote',    'Cote')    !!}
    {!! Form::control('text', 'ISBN',    'ISBN')    !!}

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop