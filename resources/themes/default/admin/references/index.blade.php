@extends('admin.templates.index')

@section('title', 'Liste des références')
@section('panel-title', 'Liste des références')

@section('resource-table')
    <thead>
        <tr>
            <th class="col-md-1">#</th>
            <th>Titre</th>
            <th>Auteurs</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($references as $reference)
        <tr class='clickable' data-href="{{ route('admin.references.show', [$reference->id]) }}">
            <td>{{ $reference->id }}</td>
            <td class="primary"><a href="{{ route('admin.references.show', [$reference->id]) }}">{{ $reference->titre }}</a></td>
            <td>{{ $reference->auteurs }}</td>
        </tr>
        @endforeach
    </tbody>
@stop
