@extends('admin.templates.show')

@section('title', 'Référence '.$reference->titre)
@section('panel-title', 'Détails de la référence')

@section('display')
    {!! Html::displayText('Titre',   $reference->titre)   !!}
    {!! Html::displayText('Auteurs', $reference->auteurs) !!}
    {!! Html::displayText('Cote',    $reference->cote)    !!}
    {!! Html::displayText('ISBN',    $reference->ISBN)    !!}

    {!! Html::displayOpen('Versions')    !!}
        @each('admin.versions.partial', $reference->versionsLecons, 'version')
    {!! Html::displayClose()    !!}

    {!! Html::displayText('Créée le',    $reference->created_at) !!}
    {!! Html::displayText('Modifiée le', $reference->updated_at) !!}
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer cette référence ?'])
@stop
