@extends('admin.templates.show')

@section('title', 'Retour de '.$retour->user->name)
@section('panel-title', "Détails du retour")

@section('display')
    {!! Html::displayText('Auteur',  $retour->user->name)   !!}
    {!! Html::displayText('Oral',    $retour->oral->nom)    !!}
    {!! Html::displayText('Année',   $retour->annee)        !!}
    {!! Html::displayText('Anonyme',($retour->anonyme? 'Oui' : 'Non')) !!}


    @foreach($retour->oral->questions as $question)
    {!! Html::displayOpen('Q'.$question->ordre) !!}
        <i><b>{{ $question->label }}</b></i>
        {!! Html::reponse($question, $reponses, 'admin.') !!}
    {!! Html::displayClose() !!}
    @endforeach
@stop

@section('modal')
    @include('admin.partials.confirm-delete', ['msgconfirm' => 'Voulez-vous vraiment supprimer ce retour ?'])
@stop
