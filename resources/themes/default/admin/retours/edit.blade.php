@extends('admin.templates.form')

@section('title', 'Retour de '.$retour->user->name)
@section('panel-title', "Modification d'un retour")

@section('form')
{!! Form::model($retour, ['route' => ['admin.retours.update', $retour->id], 'method' => 'put', 'class' => 'form-horizontal panel-body', 'id' => 'oral-form-edit']) !!}
    {!! Form::hidden('id', $retour->id) !!}

    <div class="form-group">
        <label class="col-sm-2 control-label">Oral :</label>
        <div class="col-sm-10">
            <input type="text" name="oraux_type_id" value="{{ $retour->oral->nom }}" class="form-control" disabled />
        </div>
    </div>

    {!! Form::control('cSelect',  'user_id',   'Auteur',  ['list' => $users, 'msg' => "Choisir l'auteur..."]) !!}
    {!! Form::control('text',     'annee',     'Année',   ['attr' => ['disabled']]) !!}
    {!! Form::control('bcheckbox','anonyme',   'Anonyme', ['checked' => $retour->anonyme, 'label' => '']) !!}

    @foreach($retour->oral->questions as $question)
    <div class="form-group">
        <label class="col-md-2 control-label">
            Q{{ $question->ordre }} :
        </label> 
        <div class="col-md-10">
            <p class="form-control-static">
                <i><b>{{ $question->label }}</b></i>
            </p>
            {!! Form::reponse($question, $reponses, $ressources) !!}
        </div>
    </div>
    @endforeach

    {!! Form::submitbtn('Envoyer') !!}
{!! Form::close() !!}
@stop
