@extends('admin.templates.form')

@section('title', "Création d'un retour")
@section('panel-title', "Création d'un retour")

@section('form')
{!! Form::open(['route' => ['admin.retours.store'], 'method' => 'post', 'class' => 'form-horizontal panel-body']) !!}

    {!! Form::control('cSelect',   'user_id',       'Auteur', ['msg' => "Choisir l'auteur...", 'list' => $users]) !!}
    {!! Form::control('cSelect',   'oraux_type_id', 'Oral',   ['msg' => "Choisir l'oral...",   'list' => $oraux]) !!}
    {!! Form::control('text',      'annee',         'Année') !!}
    {!! Form::control('bcheckbox', 'anonyme',       'Anonyme', ['checked' => false, 'label'=>'']) !!}

    {!! Form::submitbtn('Suite', ['type' => 'default']) !!}
{!! Form::close() !!}
@stop