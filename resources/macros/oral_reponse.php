<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;

FormBuilder::macro('reponse', function($question, $reponses, $ressources)
{
    if(is_array($reponses))
        $reponses = new \Illuminate\Database\Eloquent\Collection;

    $qname = 'questions['.$question->id.']';
    switch($question->type->fnom)
    {
        case 'texte':
            if(!$reponses->has($question->id))
                return '<textarea name="'.$qname.'" class="form-control" rows="4"></textarea>';

            $reponse = $reponses->get($question->id)[0];
            return sprintf('
                <textarea name="reponses[%s]" class="form-control" rows="4">%s</textarea>',
                $reponse->id,
                $reponse->reponse_texte
            );


        case 'note':
            if(!$reponses->has($question->id))
                return '<input type="number" name="'.$qname.'" class="form-control" placeholder="Note" step="0.25" min="0" max="20" />';

            $reponse = $reponses->get($question->id)[0];
            return sprintf('
                <input type="number" name="reponses[%s]" class="form-control" value="%s" step="0.25" min="0" max="20" />',
                $reponse->id,
                $reponse->reponse_texte
            );


        case 'lecon_analyse':
        case 'lecon_algebre':
        case 'lecon_info':
        case 'lecon_maths_info':
            if($question->type->fnom == 'lecon_analyse')    $liste = $ressources['lecons_analyse'];
            if($question->type->fnom == 'lecon_algebre')    $liste = $ressources['lecons_algebre'];
            if($question->type->fnom == 'lecon_info')       $liste = $ressources['lecons_info'];
            if($question->type->fnom == 'lecon_maths_info') $liste = $ressources['lecons_maths_info'];

            if(!$reponses->has($question->id))
                return Form::cSelect($qname, $liste, 'Choisir une leçon...');

            $reponse = $reponses->get($question->id)[0];
            $rname = 'reponses['.$reponse->id.']';
            return Form::cSelect($rname, $liste, 'Choisir une leçon...', [$reponse->reponsable_id]);


        case 'developpement':
            $liste = $ressources['developpements'];

            if(!$reponses->has($question->id))
                return Form::cSelect($qname, $liste, 'Choisir un developpement...');

            $reponse = $reponses->get($question->id)[0];
            $rname = 'reponses['.$reponse->id.']';
            return Form::cSelect($rname, $liste, 'Choisir un developpement...', [$reponse->reponsable_id]);


        case 'developpement_liste':
            $liste = $ressources['developpements'];
            $value = [];
            if($reponses->has($question->id))
                $value = $reponses->get($question->id)->keyBy('reponsable_id')->keys()->all();

            $result  = '<ul class="list-group">';
            $result .= Form::lSelect($qname, ['list' => $liste, 'msg' => 'Choisir un developpement...', 'val' => $value]);
            $result .= '</ul>';
            return $result;


        case 'reference_liste':
            $liste = $ressources['references'];
            $value = [];
            if($reponses->has($question->id))
                $value = $reponses->get($question->id)->keyBy('reponsable_id')->keys()->all();

            $result  = '<ul class="list-group">';
            $result .= Form::lSelect($qname, ['list' => $liste, 'msg' => 'Choisir une référence...', 'val' => $value]);
            $result .= '</ul>';
            return $result;
        default:
            return '';
    }
});


HtmlBuilder::macro('reponse', function($question, $reponses, $section = '')
{
    if(!$reponses->has($question->id))
        return '<p class="empty">Pas de réponse fournie.</p>';

    switch($question->type->fnom)
    {
        case 'texte':
        case 'note':
            $reponse = $reponses->get($question->id)[0];
            if(empty($reponse->reponse_texte))
                return '<p class="empty">Pas de réponse fournie.</p>';
            else
                return '<p>'.nl2br($reponse->reponse_texte).'</p>';

        case 'lecon_analyse':
        case 'lecon_algebre':
        case 'lecon_info':
        case 'lecon_maths_info':
        case 'developpement':
            $reponse = $reponses->get($question->id)[0];
            $r = $reponse->reponsable;
	    if(!isset($r))
		return '<p class="empty">Pas de réponse fournie</p>';

            if($reponse->reponsable_type == 'lecon')
                return sprintf('<p><a href="%s">%s : %s</a></p>',
                    route($section.'lecons.show', $r->id),
                    $r->numero,
                    $r->nom
                );
            else
                return sprintf('<p><a href="%s">%s</a></p>',
                    route($section.'developpements.show', $r->id),
                    $r->nom
                );


        case 'developpement_liste':
        case 'reference_liste':
            $result = '<ul>';
            foreach($reponses[$question->id] as $reponse)
            {
                $r = $reponse->reponsable;
                if($reponse->reponsable_type == 'developpement')
                    $result .= sprintf('<li><a href="%s">%s</a></li>',
                        route($section.'developpements.show', $r->id),
                        $r->nom
                    );
                else
                    $result .= sprintf('<li><a href="%s">%s : %s</a></li>',
                        route($section.'references.show', $r->id),
                        $r->titre,
                        $r->auteurs
                    );
            }
            $result .= '</ul>';
            return $result;
        default:
            return '';
    }
});
