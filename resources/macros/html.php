<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;

require "html_display.php";

// Collapsable panel
HtmlBuilder::macro('collapsePanelOpen', function($id, $heading, $h = 6)
{
    return sprintf('
            <div class="panel panel-default">
                <div class="panel-heading" data-target="#collapse_panel_%s" data-toggle="collapse" aria-expanded="false" aria-controls="collapse_panel_%s" role="button">
                    <h%s class="panel-title">%s</h%s>
                </div>
                <div class="panel-collapse collapse" id="collapse_panel_%s">',
            $id,
            $id,
            $h,
            $heading,
            $h,
            $id
    );
});

HtmlBuilder::macro('collapsePanelClose', function()
{
    return '
                </div>
            </div>';
});


// Button back
HtmlBuilder::macro('buttonBack', function()
{
    return '<a href="javascript:history.back()" class="btn btn-primary">
            <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </a>';
});


// Generate link with a glyphicon
HtmlBuilder::macro('linkIcon', function($view, $icon, $viewParams = [], $attributes = [])
{
    return sprintf('<a href="%s" %s><span class="glyphicon glyphicon-%s"></span></a>',
        route($view, $viewParams),
        HtmlBuilder::attributes($attributes),
        $icon
    );
});


// Generate stars rating
HtmlBuilder::macro('rating', function($rank, $scale = 5)
{
    $result = '<span class="display-rating">';
    for($i = 0; $i < $rank; $i++)
        $result .= '<span class="glyphicon glyphicon-star"></span>';
    for($i = $rank; $i < $scale; $i++)
        $result .= '<span class="glyphicon glyphicon-star-empty"></span>';
    $result .= '</span>';

    return $result;
});





/**
 * Get font awesome file icon class for specific MIME Type
 * @see https://gist.github.com/guedressel/0daa170c0fde65ce5551
 *
 */
function font_awesome_file_icon($mime_type)
{
    // List of official MIME Types: http://www.iana.org/assignments/media-types/media-types.xhtml
    static $font_awesome_file_icon_classes = array(
        // Images
        'image' => 'fa-file-image-o',
        // Audio
        'audio' => 'fa-file-audio-o',
        // Video
        'video' => 'fa-file-video-o',
        // Documents
        'application/pdf' => 'fa-file-pdf-o',
        'text/plain' => 'fa-file-text-o',
        'text/html' => 'fa-file-code-o',
        'application/json' => 'fa-file-code-o',
        // Archives
        'application/gzip' => 'fa-file-archive-o',
        'application/zip' => 'fa-file-archive-o',
        // Misc
        'application/octet-stream' => 'fa-file-o',
    );
    if (isset($font_awesome_file_icon_classes[ $mime_type ]))
        return $font_awesome_file_icon_classes[ $mime_type ];

    $mime_parts = explode('/', $mime_type, 2);
    $mime_group = $mime_parts[0];
    if (isset($font_awesome_file_icon_classes[ $mime_group ]))
        return $font_awesome_file_icon_classes[ $mime_group ];

    return "fa-file-o";
}

HtmlBuilder::macro('fileIcon', function($url, $stack = true)
{
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, base_path()."/public".$url);
    finfo_close($finfo);

    if($stack)
        return sprintf('
            <span class="fa-stack">
                <i class="fa fa-square fa-stack-2x"></i>
                <i class="fa fa-stack-1x fa-inverse %s"></i>
            </span>',
            font_awesome_file_icon($mime)
        );
    else
        return sprintf('
            <i class="fa fa-1x %s"></i>',
            font_awesome_file_icon($mime)
        );
});



// Switcher
HtmlBuilder::macro('switcher', function($id, $value = 1)
{
    return sprintf('
        <div class="switcher2 state-%d" id="%s">
            <div class="switcher-state" data-value="0">
                  <i class="fa fa-ban icon-0"></i>
            </div>
            <div class="switcher-state" data-value="1">
                  <i class="fa fa-circle-o icon-1"></i>
            </div>
            <div class="switcher-state" data-value="2">
                  <i class="fa fa-adjust icon-2"></i>
            </div>
            <div class="switcher-state" data-value="3">
                  <i class="fa fa-circle icon-3"></i>
            </div>
            <span class="switch"></span>
        </div>',
        $value,
        $id
    );
});
