<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;

require "form_switcher.php";
require "form_rating.php";
require "oral_reponse.php";

// Generic grid alignement
$macroGroupLastContainer = "";
FormBuilder::macro('groupOpen', function($label = null, $container = "div", $col = 2, $class = "form-group")
{
    global $macroGroupLastContainer;

    if(is_array($container))
    {
        $option = $container;
        $col   = array_key_exists('col',   $option)? $option['col']   : $col;
        $class = array_key_exists('class', $option)? $option['class'] : $class;
        $container = array_key_exists('container', $option)? $option['container'] : 'div';
    }


    $macroGroupLastContainer = $container;

    return sprintf('
        <div class="%s">
            <label class="col-md-%d control-label">%s</label> 
            <%s class="col-md-%d">',
        $class,
        $col,
        $label? $label.' :' : '',
        $container,
        12 - $col
    );
});

FormBuilder::macro('groupClose', function()
{
    global $macroGroupLastContainer;
    
    return sprintf('
            </%s>
        </div>',
        $macroGroupLastContainer
    );
});



 // Generic control input
FormBuilder::macro('control', function($type, $name, $label, $params = [])
{
    // Get errors associated to session
    $errors = Session::get('errors', new Illuminate\Support\MessageBag);

    /*## Get parameters ##*/
    $fields = array(
        'col'       => 2,
        'container' => [in_array($type, ['lRating', 'lSelect'])? 'ul' : 'div', ''],
        'attr'      => [],
        'class'     => 'form-group',
        'size'      => ''
    );

    foreach ($fields as $key => $default)
        $$key = isset($params[$key]) ? $params[$key] : $default;

    if($size == 'lg')
        $class .= ' form-group-lg';

    // Add default class to control
    if(!array_key_exists('class', $attr)) $attr['class'] = '';
    if(!in_array($type, ['bcheckbox', 'checkbox']))
        $attr['class'] .= 'form-control';

    // Add default placeholder
    if(!array_key_exists('placeholder', $attr) && $type != "select")
    {
        if(isset($params['placeholder']))
            $attr['placeholder'] = $params['placeholder'];            
        else
            $attr['placeholder'] = $label;
    }


    /*## Get the input html ##*/
    $value = \Request::old($name) ? \Request::old($name) : null;
    $value = Form::getValueAttribute($name, $value);
    if($value == null && isset($params['value']))
        $value = $params['value'];

    switch($type)
    {
        case 'bradio':      $input = Form::bradio($name, $params['label'], $value, $params['checked'], $attr);      break;
        case 'lRadio':      $input = Form::lRadio($name, $params['list'], $value, $attr);                           break;

        case 'bcheckbox':   $input = Form::bcheckbox($name, $params['label'], $value, $params['checked'], $attr);   break;
        case 'lCheckbox':   $value = isset($params['val'])? $params['val'] : $value;
                            $input = Form::lCheckbox($name, $params['list'], $value, $attr);                        break;

        case 'select':      $input = Form::select($name, $params['list'], $value, $attr);                           break;
        case 'cSelect':     $input = Form::cSelect($name, $params['list'], $params['msg'], $value, $attr);          break;

        case 'lRating':     $input = Form::lRating($name, $params);                                                 break;
        case 'lSelect':     $input = Form::lSelect($name, $params);                                                 break;

        case 'password':    $input = Form::password($name, $attr);                                                  break;
        case 'checkbox':    $input = Form::checkbox($name, $value, (isset($params['checked'])? $params['checked'] : false), $attr);                      break;

        default:            $input = call_user_func_array(['Form', $type], [$name, $value, $attr]);
    }

    /*## Return the formatted control group ##*/
    return sprintf('
        <div class="%s%s">
            <label class="col-md-%d control-label" for="%s">%s</label> 
            <%s class="%s col-md-%d">
                %s
                %s
            </%s>
        </div>',
        $class,         ($errors->has($name) ? ' has-error' : ''),
        $col,           $name,              (($label != '')? ($label.' :') : ''),
        $container[0],  $container[1],      12 - $col,
        $input,
        $errors->first($name, '<small class="help-block">:message</small>'),
        $container[0]
    );
});



// Submit button
FormBuilder::macro('submitbtn', function($text, $option = [])
{
    $type            = array_key_exists('type',  $option)? $option['type']  : 'primary';
    $option['class'] = array_key_exists('class', $option)? $option['class'] : 'btn btn-'.$type;
    $col             = array_key_exists('col',   $option)? $option['col']   : 2;
    $size            = array_key_exists('size',  $option)? $option['size']  : 0;

    if($size === 'lg')
        $option['class'] .= ' btn-lg';

    return sprintf('
        <div class="form-group">
            <div class="col-md-offset-%d col-md-%d">
                %s
            </div>
        </div>',
        $col,
        12 - $col,
        FormBuilder::submit($text, $option)
    );
});




 // Generic control input
FormBuilder::macro('staticText', function($label, $value, $params = [])
{
    /*## Get parameters ##*/
    $fields = array(
        'col'       => 2,
        'container' => ['div', ''],
        'attr'      => [],
        'class'     => 'form-group',
        'size'      => ''
    );

    foreach ($fields as $key => $default)
        $$key = isset($params[$key]) ? $params[$key] : $default;

    if($size == 'lg')
        $class .= ' form-group-lg';

    // Add default class to control
    if(!array_key_exists('class', $attr)) $attr['class'] = '';
        $attr['class'] .= 'form-control-static';

    /*## Return the formatted control group ##*/
    return sprintf('
        <div class="%s">
            <label class="col-md-%d control-label">%s</label> 
            <%s class="%s col-md-%d">
                <p %s>
                %s
                </p>
            </%s>
        </div>',
        $class,
        $col,           (($label != '')? ($label.' :') : ''),
        $container[0],  $container[1],      12 - $col,
        Html::attributes($attr),
        $value,
        $container[0]
    );
});


/*######################################
########### Custom controls ############
######################################*/

/*
 * cSelect
 */
FormBuilder::macro('cSelect', function($name, $list, $msg, $value = [], $attr = [])
{
    if(!isset($attr['class']))
        $attr['class'] = 'form-control';
    $attr['class'] .= " chosen-select";
    $attr['placeholder'] = "";
    $attr['data-placeholder'] = $msg;
    return Form::select($name, $list, $value, $attr);
});

/*
 * lSelect
 */
FormBuilder::macro('lSelect', function($name, $params = [])
{
    $params['scale'] = 0;
    return Form::lRating($name, $params);
});


/*
 * Radio
 */
FormBuilder::macro('bradio', function($name, $label, $value = 1, $checked = null, $attributes = [])
{
    return sprintf('
                <div class="radio">
                    <label>
                        %s
                        %s
                    </label>
                </div>',
        FormBuilder::radio($name, $value, $checked, $attributes),
        $label
    );
});


/*
 * List of radio
 */
FormBuilder::macro('lRadio', function($name, $list, $value = null, $attributes = [])
{
    $result = '';
    foreach($list as $cvalue => $clabel)
        $result .= Form::bradio($name, $clabel, $cvalue, ($cvalue == $value));

    return $result;
});



/*
 * Checkbox
 */
FormBuilder::macro('bcheckbox', function($name, $label, $value = 1, $checked = null, $attributes = [])
{
    return sprintf('
                <div class="checkbox">
                    <label>
                        %s
                        %s
                    </label>
                </div>',
        FormBuilder::checkbox($name, $value, $checked, $attributes),
        $label
    );
});


/*
 * List of checkbox
 */
FormBuilder::macro('lCheckbox', function($name, $list, $value = [], $attributes = [])
{
    if($value == null)
        $value = [];
    
    $result = '';
    foreach($list as $cid => $cname)
        $result .= Form::bcheckbox($name.'['.$cid.']', $cname, 1, array_key_exists($cid, $value));

    return $result;
});






/*
// Delete button
FormBuilder::macro('deletebtn', function($prefix, $id, $msg = "")
{
    return FormBuilder::open(['method' => 'DELETE', 'route' => [$prefix.'.destroy', $id], 'class' => 'form-inline pull-right VED'])
        .'<button type="submit" class="btn btn-danger" onclick="return confirm(\''. $msg .'\')"><span class="glyphicon glyphicon-remove"></span></button>'
        .FormBuilder::close();
});

/*
// Generate links view/edit/delete
FormBuilder::macro('VED', function($prefix, $id, $msg = "")
{
    return FormBuilder::open(['method' => 'DELETE', 'route' => [$prefix.'.destroy', $id], 'class' => 'form-inline pull-right VED']).
    '
    <div class="btn-group btn-group-sm" role="group">
        <a href="'. route($prefix.'.show', $id) .'" class="btn btn-success"><span class="glyphicon glyphicon-zoom-in"></span></a>
        <a href="'. route($prefix.'.edit', $id) .'" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
        <button type="submit" class="btn btn-danger" onclick="return confirm(\''. $msg .'\')"><span class="glyphicon glyphicon-remove"></span></button>
    </div>
    '.FormBuilder::close();
});
*/