<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;


// Open and close a group
HtmlBuilder::macro('displayOpen', function($label)
{
    return sprintf('
        <li class="list-group-item clearfix display">
            <strong>%s :</strong> 
            <div>',
        $label
    );
});

HtmlBuilder::macro('displayClose', function()
{
    return sprintf('
            </div>
        </li>'
    );
});


// Display an information
HtmlBuilder::macro('displayText', function($label, $text)
{
    return sprintf('
        <li class="list-group-item clearfix display">
            <strong>%s :</strong> 
            <div>
                %s
            </div>
        </li>',
        $label,
        $text
    );
});


// Display a list : open
$macroDisplayListContainers;
HtmlBuilder::macro('displayListOpen', function($label, $container = "ul")
{
    global $macroDisplayListContainers;
    if(!is_array($macroDisplayListContainers))
        $macroDisplayListContainers = [];

    array_push($macroDisplayListContainers, $container);

    return sprintf('
        <li class="list-group-item clearfix display">
            <strong>%s :</strong>
            <%s>',
        $label,
        $container
    );
});

// Display a list : close
HtmlBuilder::macro('displayListClose', function()
{
    global $macroDisplayListContainers;
    $container = array_pop($macroDisplayListContainers);

    return sprintf('
            </%s>
        </li>',
        $container
    );
});