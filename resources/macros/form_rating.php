<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;


// Rating control
FormBuilder::macro('rating', function($name, $value = 1, $scale = 5, $zero = false, $disabled = false, $attributes = [])
{
    $result = '
        <div class="rating rating-scale-'. ($scale + ($zero? 1 : 0)).' '.($disabled? 'disabled' : '').'"'. Html::attributes($attributes) .'>';

    for($i = $scale; $i > 0; $i--)
    {
        $result .= '
            <input type="radio" class="rating-input" id="'. $name .'-rating-'. $i .'" name="'. $name .'" value="'. $i .'" '. (($value == $i)? 'checked = "checked"' : '') .' '. ($disabled? 'disabled' : '') .'/>
            <label for="'. $name .'-rating-'. $i .'" class="rating-star"><span></span></label>
            ';
    }

    if($zero)
        $result .= '
            <input type="radio" class="rating-input" id="'. $name .'-rating-0" name="'. $name .'" value="0" '. (($value == 0)? 'checked = "true"' : '') .'/>
            <label for="'. $name .'-rating-0" class="rating-zero"><span>&#8709;</span></label>
            ';

    $result .= '
        </div>';

    return $result;
});


// List rating control
FormBuilder::macro('lRating', function($name, $params = [])
{
    $list        = $params['list'];
    $scale       = array_key_exists('scale',  $params)? $params['scale']  : 5;
    $defaultvalue= array_key_exists('val',    $params)? $params['val']    : [];
    $placeholder = array_key_exists('msg',    $params)? $params['msg']    : 'Choisir...';
    $attributes  = array_key_exists('attr',   $params)? $params['attr']   : [];
    $zero        = array_key_exists('zero',   $params)? $params['zero']   : false;
    $prefix      = array_key_exists('prefix', $params)? $params['prefix'] : 'rating';
    $display     = array_key_exists('display',$params)? $params['display']: (function($id, $value){ return $value; });

    $value = Form::getValueAttribute($name, $defaultvalue);
    $result = '';


    // List of select without rating
    if($scale == 0)
    {
        $lid = [];
        // Display the ratings
        foreach($value as $id)
        {
            if(!$list->has($id))
                continue;
            array_push($lid, $id);

            $result .= sprintf('
                <li class="list-group-item rating-item" data-id="%s">
                    %s
                    <button type="button" class="btn-link %s-delete"><span class="glyphicon glyphicon-trash"></span></button>
                    <div>
                        %s
                    </div>
                </li>',
                $id,
                Form::hidden($name .'['. $id .']', 1),
                $prefix,
                $display($id, $list[$id])
            );
        }


        // Button and select to add rating
        $result .= '
            <li class="list-group-item clearfix rating-add-container">
                <div class="input-group">
                    <div type="button" class="btn input-group-addon rating-add '.$prefix.'-add-btn" data-name="'. $name .'" data-scale="'. $scale .'"><span class="glyphicon glyphicon-plus"></span></div>
                    <select data-placeholder="'. $placeholder .'" class="form-control chosen-select">
                        <option value=""></option>';
        foreach($list as $id => $elem)
        {
            $result .= sprintf('
                        <option value="%s"%s>%s</option>
                ',
                $id,
                in_array($id, $lid)? ' disabled="disabled"' : '',
                $elem
            );
        }
        $result .= '
                      </select>
                </div>
            </li>';

    }
    // Real list of rating
    else
    {
        $ratingField = array_key_exists('field', $params)? $params['field'] : 'rating';

        // Display the ratings
        foreach($value as $id => $rating)
        {
            if(!$list->has($id))
                continue;

            $result .= sprintf('
                <li class="list-group-item rating-item" data-id="%s">
                    %s
                    <button type="button" class="btn-link %s-delete"><span class="glyphicon glyphicon-trash"></span></button>
                    <div>
                        %s
                    </div>
                </li>',
                $id,
                Form::rating($name .'['. $id .']['. $ratingField .']', $rating[$ratingField], $scale, $zero, $attributes),
                $prefix,
                $display($id,$list[$id])
            );
        }


        // Button and select to add rating
        $result .= '
            <li class="list-group-item clearfix rating-add-container">
                <div class="input-group">
                    <div type="button" class="btn input-group-addon rating-add '.$prefix.'-add-btn" data-name="'. $name .'" data-field="'. $ratingField .'" data-scale="'. $scale .'" data-zero="'.$zero.'"><span class="glyphicon glyphicon-plus"></span></div>
                    <select data-placeholder="'. $placeholder .'" class="form-control chosen-select">
                        <option value=""></option>';
        foreach($list as $id => $elem)
        {
            $result .= sprintf('
                        <option value="%s"%s>%s</option>
                ',
                $id,
                array_key_exists($id, $value)? ' disabled="disabled"' : '',
                $elem
            );
        }
        $result .= '
                      </select>
                </div>
            </li>';
    }

    return $result;
});
