<?php
use Collective\Html\HtmlBuilder;
use Collective\Html\FormBuilder;


// Switcher control
FormBuilder::macro('switcher', function($name, $attributes = [], $value = null)
{
    $value = ($value != null)? $value : Form::getValueAttribute($name);

    if(!isset($attributes['class']))
        $attributes['class'] = '';
    $attributes['class'] .= ' switcher';

    $id = "switcher_".$name;

    return sprintf('
                    <div %s>
                        <input type="checkbox" data-name="%s" class="switcher-checkbox" id="%s" %s>
                        <label class="switcher-label" for="%s">
                            <span class="switcher-inner">
                                <span class="switcher-on"><span class="glyphicon glyphicon-ok"></span></span>
                                <span class="switcher-off"><span class="glyphicon glyphicon-remove"></span></span>
                            </span>
                            <span class="switcher-switch"></span>
                        </label>
                    </div>
                    <input type="hidden" name="%s" id="%s" value="%s" />',
        Html::attributes($attributes),
        $name,
        $id,
        $value? 'checked':'',
        $id,
        $name, $name,
        $value? 1 : 0
    );
});


// Switcher array control
FormBuilder::macro('switcher_array', function($name, $label, $attributes = [], $value = null)
{
    if(!is_array($value))
    {
        $value = Form::getValueAttribute($name);
        if($value == null) $value = [];
    }

    $removable = isset($attributes['removable'])? $attributes['removable'] : true;
    $addable   = isset($attributes['addable']  )? $attributes['addable']   : true;


    $result = Form::groupOpen($label, 'ul');

    foreach($value as $sname => $svalue)
    {
        $result .= sprintf('
            <li class="list-group-item list-group-item-%s">
                %s
                <strong>%s</strong>
                '. ($removable? '<a href="#" onclick="this.parentNode.remove()" class="pull-right"><span class="glyphicon glyphicon-trash"></span></a>' : '') .'
            </li>
            ',
            $svalue? 'success' : 'danger',
            Form::switcher($name.'['.$sname.']', null, $svalue),
            $sname
        );
    }

    if($addable)
    {
        $result .= sprintf('
                <li class="hidden list-group-item list-group-item-success">
                    %s
                    <strong></strong>
                    <a href="#" onclick="this.parentNode.remove()" class="pull-right"><span class="glyphicon glyphicon-trash"></span></a>
                </li>

                <li class="list-group-item">
                    <div class="input-group">
                        <div type="button" class="btn input-group-addon add-switcher" data-name="%s"><span class="glyphicon glyphicon-plus"></span></div>
                        <input type="text" placeholder="Nouvelle permission" class="form-control" />
                    </div>
                </li>',
            Form::switcher('_add_switcher'),
            $name
        );
    }

    $result .= Form::groupClose();
    
    return $result;
});