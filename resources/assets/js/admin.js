// Clickable row
$(document).ready(function($){
    $(".clickable").click(function() {
        window.document.location = $(this).data("href");
    });
});

// Init chosen selects
$(".chosen-select").chosen({
    no_results_text: "Pas de résultats.",
    search_contains: true
});




// Ajax csrf token
$(function() {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
});


/*
 * Changing order of questions
 */
$(document).ready(function($){
    if(!$('#oral-form-edit').length)
        return;

    // Echange deux éléments
    function swapElements(elm1, elm2) {
        var parent1, next1,
            parent2, next2;

        parent1 = elm1.parentNode;
        next1   = elm1.nextSibling;
        parent2 = elm2.parentNode;
        next2   = elm2.nextSibling;

        parent1.insertBefore(elm2, next1);
        parent2.insertBefore(elm1, next2);
    }

    // Evénements de changement d'ordre
    var moveUp = function(){
        var question1 = $(this).parent().prev('.question');
        var question2 = question1.parent().prev('div').find('.question');
        swapElements(question1[0], question2[0]);
        return false;
    };
    var moveDown = function() {
        var question1 = $(this).parent().prev('.question');
        var question2 = question1.parent().next('div').find('.question');
        swapElements(question1[0], question2[0]);
        return false;
    };

    $('button.up').click(moveUp);
    $('button.down').click(moveDown);


    // Disabling stuff
    var refreshButton  = function(){
        $('button.up').prop('disabled', false);
        $('button.down').prop('disabled', false);

        $('button.up').first().prop('disabled', true);
        $('button.down').last().prop('disabled', true);
    };
    refreshButton();


    // Add a question
    $('#oral-form-add-question').click(function(){
        var template = $(this).parent().parent().prev('.hidden');
        var copy = template.clone().removeClass('hidden').addClass('question-group').insertBefore(template);

        // Number of the question
        var n = $('.question-group').length;
        copy.find('label').text('Q'+ n +' :');

        // Add class to buttons
        copy.find('button').first().addClass('up').click(moveUp);
        copy.find('button').last().addClass('down').click(moveDown);
        refreshButton();

        // Change names
        copy.find('[name="__questionId"]'  ).prop('name', 'questionsIds[]');
        copy.find('[name="__question"]'    ).prop('name', 'questions[]');
        copy.find('[name="__questionType"]').prop('name', 'questionsTypes[]');

        // Activate chosen select
        copy.find('select').chosen({
            no_results_text: "Pas de résultats.",
            search_contains: true
        });
    });

    // Remove a question
    $('#oral-form-delete-question').click(function(){
        $('.question-group').last().remove();
    });
});



/*
 * File input for version
 */
$(document).ready(function($){
    if(!$('#version_file_input').length)
        return;

    // Init the input file
    $('#version_file_input').fileinput('clear').fileinput('reset').fileinput('unlock');
    

    // Delete a file
    function deleteFile(){
        var id = this.getAttribute('data-id');
        var modal = $('#modal-delete');

        // Construct the hidden input if needed
        if(modal.find('[name="fichier_id"]').length == 0)
        {
            var input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'fichier_id');
            modal.find('form').append(input);
        }

        // Set the value of the input
        modal.find('[name="fichier_id"]').val(id);

        modal.modal('show');
    }

    $('.file-delete').on('click', deleteFile);

    $('#form-delete-file').ajaxForm(function(){
        var modal = $('#modal-delete');
        var id = modal.find('[name="fichier_id"]').val();

        $('[name="fichiers['+ id +'][url]').parent().parent().remove();
        modal.modal('hide');
    }); 


    // Upload a file
    $('#version_file_input').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;

        $(this).fileinput('clear').fileinput('reset').fileinput('unlock');

        var $li = $(' \
        <li class="list-group-item"> \
            <div class="input-group"> \
                <input type="text" name="fichiers['+ response.id +'][url]" value="'+ response.url +'" class="form-control" /> \
                <span class="input-group-btn"> \
                    <button type="button" class="btn btn-primary file-delete" data-id="'+ response.id +'"> \
                        <span class="glyphicon glyphicon-trash"></span> \
                    </button> \
                </span> \
            </div> \
        </li>');
        $li.find('.file-delete').on('click', deleteFile);
        $('#version_file_input_li').before($li);
    });
});

