(function(){

// Switcher function
function switcher() {
    var c = this.firstElementChild;
    if (c.checked) {
        document.getElementById(c.getAttribute("data-name")).value = 1;
        this.parentNode.className = 'list-group-item list-group-item-success';
    }
    else {
        document.getElementById(c.getAttribute("data-name")).value = 0;
        this.parentNode.className = 'list-group-item list-group-item-danger';
    }
}

$('.switcher').click(switcher);


// Switcher adder
$('.add-switcher').click(function() {
    var v = this.nextElementSibling.value;
    if(v == '')
    {
        alert("Champ vide.");
        return false;
    }
    var pref = this.getAttribute('data-name');
    var name = pref + '['+ v + ']';

    var id = "switcher_" + v;

    // Creating new element
    var li = this.parentNode.parentNode;
    var hli = li.previousElementSibling;

    var nli = hli.cloneNode(true);
    var div = nli.firstElementChild;
    nli.className = "list-group-item list-group-item-success";
    div.onclick = switcher;

    // Setting switcher
    var box = div.firstElementChild;
    box.setAttribute('data-name', name);
    box.setAttribute('id', id);
    box.setAttribute('checked', 'checked');

    box.nextElementSibling.setAttribute('for', id);

    // Setting input and text
    var hinput = div.nextElementSibling;
    hinput.setAttribute('name', name);
    hinput.setAttribute('id',   name);
    hinput.setAttribute('value', 1);
    
    var strong = hinput.nextElementSibling;
    strong.innerHTML = v;

    // Add the node to the list
    li.parentNode.insertBefore(nli, hli);
});


})();