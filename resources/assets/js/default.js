// Animation of the logo
var timelineOver = anime.timeline({ autoplay:false});
timelineOver.add({
  targets: '#outil2',
  rotate: '-45deg',
})
.add({
  targets: '#outil1',
  rotate: '-30deg',
  offset:'-=600'
})
.add({
  targets: '#outil3',
  rotate: '-70deg',
  offset:'-=800'
});
timelineOver.reverse();
document.getElementById('navbar').addEventListener('mouseenter', function(){ timelineOver.play(); timelineOver.reverse(); });
document.getElementById('navbar').addEventListener('mouseleave', function(){ timelineOver.play(); timelineOver.reverse(); });


// Modal for login and register
(function(){
    // Login
    $(".login-modal-toggle").click(function() {
        $("#login-modal").modal();
        return false;
    });

    $('#login-modal').on('show.bs.modal', function(){
        $("#navbar-login-link").parent().addClass('active');
    });

    $('#login-modal').on('hide.bs.modal', function(){
        $("#navbar-login-link").parent().removeClass('active');
    });


    $("#login-modal").find('a').last().click(function(){
        $("#login-modal").modal('hide');
        $("#register-modal").modal();
        return false;
    })


    // Register
    $("#navbar-register-link").click(function() {
        $("#register-modal").modal();
        return false;
    });

    $('#register-modal').on('show.bs.modal', function(){
        $("#navbar-register-link").parent().addClass('active');
    });

    $('#register-modal').on('hide.bs.modal', function(){
        $("#navbar-register-link").parent().removeClass('active');
    });


    $("#register-modal").find('a').last().click(function(){
        $("#register-modal").modal('hide');
        $("#login-modal").modal();
        return false;
    });
})();


// Alert message
$(function(){
    $("#message").css('marginTop', '0px');
    setTimeout(function(){
        $("#message").css('marginTop', '-42px');
    }, 5000);
});



// Ajax csrf token
$(function() {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
});

// Clickable element
$(function(){
    $("tr.clickable").click(function() {
        window.document.location = $(this).data("href");
    });
});


// Init chosen selects
$(".chosen-select").chosen({
    no_results_text: "Pas de résultats.",
    search_contains: true
}).on('chosen:showing_dropdown', function(){
    if(typeof MathJax != "undefined")
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
});



/*
 * Init file input for version (creation)
 */
$(function(){
    if(!$('#version-create-file-input').length)
        return;

    // Init the input file
    $('#version-create-file-input').fileinput({
        maxFileCount: 10,
        mainClass: "input-group-lg",
        maxFileSize: 5000,
        showUpload: false,
        language: "fr",
        showPreview: false,
        allowedFileExtensions: ["pdf", "eps", "tex", "txt"],
    }).fileinput('clear').fileinput('reset').fileinput('unlock');
});


/*
 * File input for version (edition)
 */
$(function(){
    if(!$('#versions-edit-file-input').length)
        return;

    // Init the input file
    $('#versions-edit-file-input').fileinput({
        mainClass: "input-group-lg",
        maxFileSize: 5000,
        language: "fr",
        showPreview: false,
        allowedFileExtensions: ["pdf", "eps", "tex", "txt"],
    }).fileinput('clear').fileinput('reset').fileinput('unlock');


    // Delete a file
    function deleteFile(){
        var id = this.getAttribute('data-id');
        var modal = $('#modal-delete');

        // Construct the hidden input if needed
        if(modal.find('[name="fichier_id"]').length == 0)
        {
            var input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'fichier_id');
            modal.find('form').append(input);
        }

        // Set the value of the input
        modal.find('[name="fichier_id"]').val(id);

        modal.modal('show');
    }

    $('.file-delete').on('click', deleteFile);

    $('#form-delete-file').ajaxForm(function(){
        var modal = $('#modal-delete');
        var id = modal.find('[name="fichier_id"]').val();

        $('[name="fichiers['+ id +'][url]').parent().parent().remove();
        modal.modal('hide');
    });


    // Upload a file
    $('#versions-edit-file-input').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;

        $(this).fileinput('clear').fileinput('reset').fileinput('unlock');

        var $li = $(' \
        <li class="list-group-item"> \
            <div class="input-group"> \
                <input type="text" name="fichiers['+ response.id +'][url]" value="'+ response.url +'" class="form-control" /> \
                <span class="input-group-btn"> \
                    <button type="button" class="btn btn-primary btn-lg file-delete" data-id="'+ response.id +'"> \
                        <span class="glyphicon glyphicon-trash"></span> \
                    </button> \
                </span> \
            </div> \
        </li>');
        $li.find('.file-delete').on('click', deleteFile);
        $('#versions-edit-file-input-li').before($li);
    });
});




/*
    Show votes menu
*/
$(function(){
    if(!$('#edit-votes').length)
        return;

    $('#edit-votes').on('show.bs.collapse', function(){
        $('#show-votes').collapse('hide');
    });
});



/*
    Go top
*/
$('.scrollTop').on('click', function(){
    $("body").animate({ scrollTop: 0 }, "slow");
});




/*!
 * JavaScript Cookie v2.1.4
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
	var registeredInModuleLoader = false;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function init (converter) {
		function api (key, value, attributes) {
			var result;
			if (typeof document === 'undefined') {
				return;
			}

			// Write

			if (arguments.length > 1) {
				attributes = extend({
					path: '/'
				}, api.defaults, attributes);

				if (typeof attributes.expires === 'number') {
					var expires = new Date();
					expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
					attributes.expires = expires;
				}

				// We're using "expires" because "max-age" is not supported by IE
				attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

				try {
					result = JSON.stringify(value);
					if (/^[\{\[]/.test(result)) {
						value = result;
					}
				} catch (e) {}

				if (!converter.write) {
					value = encodeURIComponent(String(value))
						.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
				} else {
					value = converter.write(value, key);
				}

				key = encodeURIComponent(String(key));
				key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
				key = key.replace(/[\(\)]/g, escape);

				var stringifiedAttributes = '';

				for (var attributeName in attributes) {
					if (!attributes[attributeName]) {
						continue;
					}
					stringifiedAttributes += '; ' + attributeName;
					if (attributes[attributeName] === true) {
						continue;
					}
					stringifiedAttributes += '=' + attributes[attributeName];
				}
				return (document.cookie = key + '=' + value + stringifiedAttributes);
			}

			// Read

			if (!key) {
				result = {};
			}

			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling "get()"
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var rdecode = /(%[0-9A-Z]{2})+/g;
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = parts[0].replace(rdecode, decodeURIComponent);
					cookie = converter.read ?
						converter.read(cookie, name) : converter(cookie, name) ||
						cookie.replace(rdecode, decodeURIComponent);

					if (this.json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					if (key === name) {
						result = cookie;
						break;
					}

					if (!key) {
						result[name] = cookie;
					}
				} catch (e) {}
			}

			return result;
		}

		api.set = api;
		api.get = function (key) {
			return api.call(api, key);
		};
		api.getJSON = function () {
			return api.apply({
				json: true
			}, [].slice.call(arguments));
		};
		api.defaults = {};

		api.remove = function (key, attributes) {
			api(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));
